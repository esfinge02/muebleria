Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuProAbrirOrdeneCompra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambiarVendedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProConsultarNotaCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProConsultaVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUtiVariables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatUsuariosAlmacen As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepSaldosGenerales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenIngresos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepIngresosxCobrador As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepIngresosxFormaPago As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatVendedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatPlanesCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiarioVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosMasVendidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiarioVentasDetallado As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiarioVentasExpo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiferenciaDePrecios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepNotasCanceladas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepFacturasCanceladas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatAutorizacionesVentasPedidoFabrica As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVentasDiarias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambiaBodegaSalidaVenta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenSalidas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenContabilidad As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEstadosCuentaClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSaldosVencidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCotizacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVentasxFechaEntrega As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProDescuentosEspecialesProgramados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAutorizacionesBonificacionesInteres As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProDescuentosEspecialesClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCancelacionesVentasSinRegistro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCancelacionesVentasConRegistro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSubCancelacionesVentas As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuCatClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosClienteMaestra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProConsultaNotaCargo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRecordatorioSaldo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuConfirmacionSaldoCobrador As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReporteEstadistico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVentasVendedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepUtilidadVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosMasVendidosGrupo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProVales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVentasCancelaciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambiarConvenio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHerrPermisosExtendidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepBonificaciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTimbrarFacturas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTimbrarComprobantesSimplificados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepFacturasPendientesTimbrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepRecibosCancelados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProReimpresionAbonosCFS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAnticipos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatOpcioonesCancelacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatTiposVales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAnticipos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProReimpresionAbonoMenos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMenos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepPedidosFabricaSurtidosFecha As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuProAbrirOrdeneCompra = New DevExpress.XtraBars.BarButtonItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambiarVendedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosCliente = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProConsultarNotaCredito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProConsultaVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuUtiVariables = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatUsuariosAlmacen = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepSaldosGenerales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenIngresos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepIngresosxCobrador = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepIngresosxFormaPago = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatVendedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatPlanesCredito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiarioVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosMasVendidos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiarioVentasDetallado = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiarioVentasExpo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiferenciaDePrecios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepNotasCanceladas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepFacturasCanceladas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatAutorizacionesVentasPedidoFabrica = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVentasDiarias = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambiaBodegaSalidaVenta = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenSalidas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenContabilidad = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEstadosCuentaClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSaldosVencidos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCotizacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVentasxFechaEntrega = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProDescuentosEspecialesProgramados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProAutorizacionesBonificacionesInteres = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProDescuentosEspecialesClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCancelacionesVentasSinRegistro = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCancelacionesVentasConRegistro = New DevExpress.XtraBars.BarButtonItem
        Me.mnuSubCancelacionesVentas = New DevExpress.XtraBars.BarSubItem
        Me.mnuCatClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosClienteMaestra = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProConsultaNotaCargo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRecordatorioSaldo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuConfirmacionSaldoCobrador = New DevExpress.XtraBars.BarButtonItem
        Me.mnuReporteEstadistico = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVentasVendedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepUtilidadVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosMasVendidosGrupo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProVales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVentasCancelaciones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambiarConvenio = New DevExpress.XtraBars.BarButtonItem
        Me.mnuHerrPermisosExtendidos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepBonificaciones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTimbrarFacturas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTimbrarComprobantesSimplificados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepFacturasPendientesTimbrar = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepRecibosCancelados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProReimpresionAbonosCFS = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProAnticipos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatOpcioonesCancelacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatTiposVales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAnticipos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProReimpresionAbonoMenos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMenos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepPedidosFabricaSurtidosFecha = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuProAbrirOrdeneCompra, Me.BarButtonItem1, Me.mnuProCambiarVendedor, Me.mnuProMovimientosArticulos, Me.mnuProMovimientosCliente, Me.mnuProConsultarNotaCredito, Me.mnuProConsultaVentas, Me.mnuUtiVariables, Me.mnuCatUsuariosAlmacen, Me.BarStaticItem1, Me.mnuRepSaldosGenerales, Me.mnuRepResumenIngresos, Me.mnuRepIngresosxCobrador, Me.mnuRepIngresosxFormaPago, Me.mnuCatVendedores, Me.mnuCatPlanesCredito, Me.mnuRepDiarioVentas, Me.mnuRepArticulosMasVendidos, Me.mnuRepDiarioVentasDetallado, Me.mnuRepDiarioVentasExpo, Me.mnuRepDiferenciaDePrecios, Me.mnuRepNotasCanceladas, Me.mnuRepFacturasCanceladas, Me.mnuCatAutorizacionesVentasPedidoFabrica, Me.mnuRepVentasDiarias, Me.mnuProCambiaBodegaSalidaVenta, Me.mnuRepResumenSalidas, Me.mnuRepResumenContabilidad, Me.mnuRepEstadosCuentaClientes, Me.mnuRepSaldosVencidos, Me.mnuProCotizacion, Me.mnuRepVentasxFechaEntrega, Me.mnuProDescuentosEspecialesProgramados, Me.mnuProAutorizacionesBonificacionesInteres, Me.mnuProDescuentosEspecialesClientes, Me.mnuProCancelacionesVentasSinRegistro, Me.mnuProCancelacionesVentasConRegistro, Me.mnuSubCancelacionesVentas, Me.mnuCatClientes, Me.mnuProMovimientosClienteMaestra, Me.mnuProConsultaNotaCargo, Me.mnuRecordatorioSaldo, Me.mnuConfirmacionSaldoCobrador, Me.mnuReporteEstadistico, Me.mnuRepVentasVendedor, Me.mnuRepUtilidadVentas, Me.mnuRepArticulosMasVendidosGrupo, Me.mnuProVales, Me.mnuRepVales, Me.mnuRepVentasCancelaciones, Me.mnuProCambiarConvenio, Me.mnuHerrPermisosExtendidos, Me.mnuRepBonificaciones, Me.mnuProTimbrarFacturas, Me.mnuProTimbrarComprobantesSimplificados, Me.mnuRepFacturasPendientesTimbrar, Me.mnuRepRecibosCancelados, Me.mnuProReimpresionAbonosCFS, Me.mnuProAnticipos, Me.mnuCatOpcioonesCancelacion, Me.mnuCatTiposVales, Me.mnuRepAnticipos, Me.mnuProReimpresionAbonoMenos, Me.mnuRepMenos, Me.mnuRepPedidosFabricaSurtidosFecha})
        Me.BarManager.MaxItemId = 176
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatUsuariosAlmacen), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerrPermisosExtendidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiVariables, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatVendedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatPlanesCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatOpcioonesCancelacion), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatTiposVales)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAbrirOrdeneCompra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambiarVendedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosCliente), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProConsultarNotaCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProConsultaVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambiaBodegaSalidaVenta), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatAutorizacionesVentasPedidoFabrica), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSubCancelacionesVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProConsultaNotaCargo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAutorizacionesBonificacionesInteres), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDescuentosEspecialesProgramados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDescuentosEspecialesClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCotizacion), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAnticipos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTimbrarFacturas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProReimpresionAbonoMenos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTimbrarComprobantesSimplificados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProReimpresionAbonosCFS), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosClienteMaestra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProVales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambiarConvenio)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRecordatorioSaldo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEstadosCuentaClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosVencidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasxFechaEntrega), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuConfirmacionSaldoCobrador), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReporteEstadistico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasVendedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepUtilidadVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosMasVendidosGrupo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasCancelaciones), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepBonificaciones), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepFacturasPendientesTimbrar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepRecibosCancelados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAnticipos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMenos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepPedidosFabricaSurtidosFecha), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosGenerales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenIngresos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepIngresosxCobrador), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepIngresosxFormaPago), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiarioVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiarioVentasDetallado), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiarioVentasExpo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosMasVendidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiferenciaDePrecios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepNotasCanceladas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepFacturasCanceladas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasDiarias), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenSalidas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenContabilidad)})
        '
        'mnuProAbrirOrdeneCompra
        '
        Me.mnuProAbrirOrdeneCompra.Caption = "Abrir &Orden de Compra"
        Me.mnuProAbrirOrdeneCompra.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProAbrirOrdeneCompra.Id = 105
        Me.mnuProAbrirOrdeneCompra.ImageIndex = 0
        Me.mnuProAbrirOrdeneCompra.Name = "mnuProAbrirOrdeneCompra"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 106
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'mnuProCambiarVendedor
        '
        Me.mnuProCambiarVendedor.Caption = "Cam&biar Vendedor"
        Me.mnuProCambiarVendedor.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambiarVendedor.Id = 107
        Me.mnuProCambiarVendedor.ImageIndex = 2
        Me.mnuProCambiarVendedor.Name = "mnuProCambiarVendedor"
        '
        'mnuProMovimientosArticulos
        '
        Me.mnuProMovimientosArticulos.Caption = "&Movimientos de Art�culos"
        Me.mnuProMovimientosArticulos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosArticulos.Id = 108
        Me.mnuProMovimientosArticulos.ImageIndex = 3
        Me.mnuProMovimientosArticulos.Name = "mnuProMovimientosArticulos"
        '
        'mnuProMovimientosCliente
        '
        Me.mnuProMovimientosCliente.Caption = "Movimientos de C&liente"
        Me.mnuProMovimientosCliente.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosCliente.Id = 109
        Me.mnuProMovimientosCliente.ImageIndex = 4
        Me.mnuProMovimientosCliente.Name = "mnuProMovimientosCliente"
        '
        'mnuProConsultarNotaCredito
        '
        Me.mnuProConsultarNotaCredito.Caption = "Consultar &Nota de Cr�dito"
        Me.mnuProConsultarNotaCredito.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProConsultarNotaCredito.Id = 110
        Me.mnuProConsultarNotaCredito.ImageIndex = 5
        Me.mnuProConsultarNotaCredito.Name = "mnuProConsultarNotaCredito"
        '
        'mnuProConsultaVentas
        '
        Me.mnuProConsultaVentas.Caption = "Consulta de Ven&tas"
        Me.mnuProConsultaVentas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProConsultaVentas.Id = 111
        Me.mnuProConsultaVentas.ImageIndex = 6
        Me.mnuProConsultaVentas.Name = "mnuProConsultaVentas"
        '
        'mnuUtiVariables
        '
        Me.mnuUtiVariables.Caption = "Variables del Sistema"
        Me.mnuUtiVariables.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuUtiVariables.Id = 115
        Me.mnuUtiVariables.ImageIndex = 14
        Me.mnuUtiVariables.Name = "mnuUtiVariables"
        '
        'mnuCatUsuariosAlmacen
        '
        Me.mnuCatUsuariosAlmacen.Caption = "Configuraci�n de Usuarios"
        Me.mnuCatUsuariosAlmacen.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuCatUsuariosAlmacen.Id = 116
        Me.mnuCatUsuariosAlmacen.ImageIndex = 13
        Me.mnuCatUsuariosAlmacen.Name = "mnuCatUsuariosAlmacen"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 12
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 117
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuRepSaldosGenerales
        '
        Me.mnuRepSaldosGenerales.Caption = "Saldos &Generales"
        Me.mnuRepSaldosGenerales.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSaldosGenerales.Id = 118
        Me.mnuRepSaldosGenerales.ImageIndex = 9
        Me.mnuRepSaldosGenerales.Name = "mnuRepSaldosGenerales"
        '
        'mnuRepResumenIngresos
        '
        Me.mnuRepResumenIngresos.Caption = "Res&umen de Ingresos"
        Me.mnuRepResumenIngresos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenIngresos.Id = 119
        Me.mnuRepResumenIngresos.ImageIndex = 10
        Me.mnuRepResumenIngresos.Name = "mnuRepResumenIngresos"
        '
        'mnuRepIngresosxCobrador
        '
        Me.mnuRepIngresosxCobrador.Caption = "&Ingresos por Cobrador"
        Me.mnuRepIngresosxCobrador.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepIngresosxCobrador.Id = 120
        Me.mnuRepIngresosxCobrador.ImageIndex = 12
        Me.mnuRepIngresosxCobrador.Name = "mnuRepIngresosxCobrador"
        '
        'mnuRepIngresosxFormaPago
        '
        Me.mnuRepIngresosxFormaPago.Caption = "Ingresos por &Forma de Pago"
        Me.mnuRepIngresosxFormaPago.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepIngresosxFormaPago.Id = 121
        Me.mnuRepIngresosxFormaPago.ImageIndex = 11
        Me.mnuRepIngresosxFormaPago.Name = "mnuRepIngresosxFormaPago"
        '
        'mnuCatVendedores
        '
        Me.mnuCatVendedores.Caption = "Vendedores"
        Me.mnuCatVendedores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatVendedores.Id = 124
        Me.mnuCatVendedores.ImageIndex = 17
        Me.mnuCatVendedores.Name = "mnuCatVendedores"
        '
        'mnuCatPlanesCredito
        '
        Me.mnuCatPlanesCredito.Caption = "Planes de Cr�dito"
        Me.mnuCatPlanesCredito.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatPlanesCredito.Id = 125
        Me.mnuCatPlanesCredito.ImageIndex = 16
        Me.mnuCatPlanesCredito.Name = "mnuCatPlanesCredito"
        '
        'mnuRepDiarioVentas
        '
        Me.mnuRepDiarioVentas.Caption = "Diario de Ventas"
        Me.mnuRepDiarioVentas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiarioVentas.Id = 126
        Me.mnuRepDiarioVentas.ImageIndex = 18
        Me.mnuRepDiarioVentas.Name = "mnuRepDiarioVentas"
        '
        'mnuRepArticulosMasVendidos
        '
        Me.mnuRepArticulosMasVendidos.Caption = "Art�culos Mas Vendidos"
        Me.mnuRepArticulosMasVendidos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosMasVendidos.Id = 127
        Me.mnuRepArticulosMasVendidos.ImageIndex = 21
        Me.mnuRepArticulosMasVendidos.Name = "mnuRepArticulosMasVendidos"
        '
        'mnuRepDiarioVentasDetallado
        '
        Me.mnuRepDiarioVentasDetallado.Caption = "Diario de Ventas Detallado"
        Me.mnuRepDiarioVentasDetallado.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiarioVentasDetallado.Id = 128
        Me.mnuRepDiarioVentasDetallado.ImageIndex = 19
        Me.mnuRepDiarioVentasDetallado.Name = "mnuRepDiarioVentasDetallado"
        '
        'mnuRepDiarioVentasExpo
        '
        Me.mnuRepDiarioVentasExpo.Caption = "Diario de Ventas Expo"
        Me.mnuRepDiarioVentasExpo.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiarioVentasExpo.Id = 129
        Me.mnuRepDiarioVentasExpo.ImageIndex = 20
        Me.mnuRepDiarioVentasExpo.Name = "mnuRepDiarioVentasExpo"
        '
        'mnuRepDiferenciaDePrecios
        '
        Me.mnuRepDiferenciaDePrecios.Caption = "Diferencia de Precios"
        Me.mnuRepDiferenciaDePrecios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiferenciaDePrecios.Id = 130
        Me.mnuRepDiferenciaDePrecios.ImageIndex = 22
        Me.mnuRepDiferenciaDePrecios.Name = "mnuRepDiferenciaDePrecios"
        '
        'mnuRepNotasCanceladas
        '
        Me.mnuRepNotasCanceladas.Caption = "Notas Canceladas"
        Me.mnuRepNotasCanceladas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepNotasCanceladas.Id = 131
        Me.mnuRepNotasCanceladas.ImageIndex = 23
        Me.mnuRepNotasCanceladas.Name = "mnuRepNotasCanceladas"
        '
        'mnuRepFacturasCanceladas
        '
        Me.mnuRepFacturasCanceladas.Caption = "Facturas Canceladas"
        Me.mnuRepFacturasCanceladas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepFacturasCanceladas.Id = 132
        Me.mnuRepFacturasCanceladas.ImageIndex = 24
        Me.mnuRepFacturasCanceladas.Name = "mnuRepFacturasCanceladas"
        '
        'mnuCatAutorizacionesVentasPedidoFabrica
        '
        Me.mnuCatAutorizacionesVentasPedidoFabrica.Caption = "Autorizaciones a Ventas Pedido F�brica"
        Me.mnuCatAutorizacionesVentasPedidoFabrica.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuCatAutorizacionesVentasPedidoFabrica.Id = 133
        Me.mnuCatAutorizacionesVentasPedidoFabrica.ImageIndex = 25
        Me.mnuCatAutorizacionesVentasPedidoFabrica.Name = "mnuCatAutorizacionesVentasPedidoFabrica"
        '
        'mnuRepVentasDiarias
        '
        Me.mnuRepVentasDiarias.Caption = "Ventas Diarias"
        Me.mnuRepVentasDiarias.Id = 134
        Me.mnuRepVentasDiarias.ImageIndex = 26
        Me.mnuRepVentasDiarias.Name = "mnuRepVentasDiarias"
        '
        'mnuProCambiaBodegaSalidaVenta
        '
        Me.mnuProCambiaBodegaSalidaVenta.Caption = "Cambiar Bodega de Salida a una Venta"
        Me.mnuProCambiaBodegaSalidaVenta.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambiaBodegaSalidaVenta.Id = 135
        Me.mnuProCambiaBodegaSalidaVenta.ImageIndex = 29
        Me.mnuProCambiaBodegaSalidaVenta.Name = "mnuProCambiaBodegaSalidaVenta"
        '
        'mnuRepResumenSalidas
        '
        Me.mnuRepResumenSalidas.Caption = "Resumen de Salidas"
        Me.mnuRepResumenSalidas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenSalidas.Id = 136
        Me.mnuRepResumenSalidas.ImageIndex = 28
        Me.mnuRepResumenSalidas.Name = "mnuRepResumenSalidas"
        '
        'mnuRepResumenContabilidad
        '
        Me.mnuRepResumenContabilidad.Caption = "R&esumen de Contabilidad"
        Me.mnuRepResumenContabilidad.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenContabilidad.Id = 137
        Me.mnuRepResumenContabilidad.ImageIndex = 27
        Me.mnuRepResumenContabilidad.Name = "mnuRepResumenContabilidad"
        '
        'mnuRepEstadosCuentaClientes
        '
        Me.mnuRepEstadosCuentaClientes.Caption = "Estado de Cuenta"
        Me.mnuRepEstadosCuentaClientes.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEstadosCuentaClientes.Id = 138
        Me.mnuRepEstadosCuentaClientes.Name = "mnuRepEstadosCuentaClientes"
        '
        'mnuRepSaldosVencidos
        '
        Me.mnuRepSaldosVencidos.Caption = "Saldos Vencidos"
        Me.mnuRepSaldosVencidos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSaldosVencidos.Id = 139
        Me.mnuRepSaldosVencidos.Name = "mnuRepSaldosVencidos"
        '
        'mnuProCotizacion
        '
        Me.mnuProCotizacion.Caption = "Cotizaciones"
        Me.mnuProCotizacion.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProCotizacion.Id = 140
        Me.mnuProCotizacion.Name = "mnuProCotizacion"
        '
        'mnuRepVentasxFechaEntrega
        '
        Me.mnuRepVentasxFechaEntrega.Caption = "Ventas por Fecha de Entrega"
        Me.mnuRepVentasxFechaEntrega.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVentasxFechaEntrega.Id = 141
        Me.mnuRepVentasxFechaEntrega.Name = "mnuRepVentasxFechaEntrega"
        '
        'mnuProDescuentosEspecialesProgramados
        '
        Me.mnuProDescuentosEspecialesProgramados.Caption = "Descuentos Especiales Programados"
        Me.mnuProDescuentosEspecialesProgramados.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDescuentosEspecialesProgramados.Id = 142
        Me.mnuProDescuentosEspecialesProgramados.Name = "mnuProDescuentosEspecialesProgramados"
        '
        'mnuProAutorizacionesBonificacionesInteres
        '
        Me.mnuProAutorizacionesBonificacionesInteres.Caption = "Autorizaciones a Bonificaciones de Interes"
        Me.mnuProAutorizacionesBonificacionesInteres.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProAutorizacionesBonificacionesInteres.Id = 143
        Me.mnuProAutorizacionesBonificacionesInteres.Name = "mnuProAutorizacionesBonificacionesInteres"
        '
        'mnuProDescuentosEspecialesClientes
        '
        Me.mnuProDescuentosEspecialesClientes.Caption = "Descuentos Especiales a Clientes"
        Me.mnuProDescuentosEspecialesClientes.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDescuentosEspecialesClientes.Id = 144
        Me.mnuProDescuentosEspecialesClientes.Name = "mnuProDescuentosEspecialesClientes"
        '
        'mnuProCancelacionesVentasSinRegistro
        '
        Me.mnuProCancelacionesVentasSinRegistro.Caption = "Cancelaciones de Ventas sin Registro"
        Me.mnuProCancelacionesVentasSinRegistro.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCancelacionesVentasSinRegistro.Id = 145
        Me.mnuProCancelacionesVentasSinRegistro.Name = "mnuProCancelacionesVentasSinRegistro"
        '
        'mnuProCancelacionesVentasConRegistro
        '
        Me.mnuProCancelacionesVentasConRegistro.Caption = "Cancelaciones de Ventas Con Registro"
        Me.mnuProCancelacionesVentasConRegistro.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCancelacionesVentasConRegistro.Id = 146
        Me.mnuProCancelacionesVentasConRegistro.Name = "mnuProCancelacionesVentasConRegistro"
        '
        'mnuSubCancelacionesVentas
        '
        Me.mnuSubCancelacionesVentas.Caption = "Cancelaciones de Ventas"
        Me.mnuSubCancelacionesVentas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuSubCancelacionesVentas.Id = 147
        Me.mnuSubCancelacionesVentas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCancelacionesVentasSinRegistro), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCancelacionesVentasConRegistro)})
        Me.mnuSubCancelacionesVentas.Name = "mnuSubCancelacionesVentas"
        '
        'mnuCatClientes
        '
        Me.mnuCatClientes.Caption = "Clientes"
        Me.mnuCatClientes.Id = 148
        Me.mnuCatClientes.Name = "mnuCatClientes"
        '
        'mnuProMovimientosClienteMaestra
        '
        Me.mnuProMovimientosClienteMaestra.Caption = "Consulta Maestra"
        Me.mnuProMovimientosClienteMaestra.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosClienteMaestra.Id = 149
        Me.mnuProMovimientosClienteMaestra.ImageIndex = 4
        Me.mnuProMovimientosClienteMaestra.Name = "mnuProMovimientosClienteMaestra"
        '
        'mnuProConsultaNotaCargo
        '
        Me.mnuProConsultaNotaCargo.Caption = "Consulta Nota de Cargo"
        Me.mnuProConsultaNotaCargo.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProConsultaNotaCargo.Id = 150
        Me.mnuProConsultaNotaCargo.Name = "mnuProConsultaNotaCargo"
        '
        'mnuRecordatorioSaldo
        '
        Me.mnuRecordatorioSaldo.Caption = "Recordatorio de Saldo"
        Me.mnuRecordatorioSaldo.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRecordatorioSaldo.Id = 151
        Me.mnuRecordatorioSaldo.Name = "mnuRecordatorioSaldo"
        '
        'mnuConfirmacionSaldoCobrador
        '
        Me.mnuConfirmacionSaldoCobrador.Caption = "Confirmaci�n de Saldo de Cobrador"
        Me.mnuConfirmacionSaldoCobrador.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuConfirmacionSaldoCobrador.Id = 152
        Me.mnuConfirmacionSaldoCobrador.Name = "mnuConfirmacionSaldoCobrador"
        '
        'mnuReporteEstadistico
        '
        Me.mnuReporteEstadistico.Caption = "Estadistico"
        Me.mnuReporteEstadistico.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuReporteEstadistico.Id = 153
        Me.mnuReporteEstadistico.Name = "mnuReporteEstadistico"
        '
        'mnuRepVentasVendedor
        '
        Me.mnuRepVentasVendedor.Caption = "Ventas por Vendedor"
        Me.mnuRepVentasVendedor.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVentasVendedor.Id = 154
        Me.mnuRepVentasVendedor.Name = "mnuRepVentasVendedor"
        '
        'mnuRepUtilidadVentas
        '
        Me.mnuRepUtilidadVentas.Caption = "Utilidad de Ventas"
        Me.mnuRepUtilidadVentas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepUtilidadVentas.Id = 155
        Me.mnuRepUtilidadVentas.Name = "mnuRepUtilidadVentas"
        '
        'mnuRepArticulosMasVendidosGrupo
        '
        Me.mnuRepArticulosMasVendidosGrupo.Caption = "Articulos Mas Vendidos x Grupo"
        Me.mnuRepArticulosMasVendidosGrupo.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosMasVendidosGrupo.Id = 156
        Me.mnuRepArticulosMasVendidosGrupo.Name = "mnuRepArticulosMasVendidosGrupo"
        '
        'mnuProVales
        '
        Me.mnuProVales.Caption = "Vales"
        Me.mnuProVales.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProVales.Id = 157
        Me.mnuProVales.Name = "mnuProVales"
        '
        'mnuRepVales
        '
        Me.mnuRepVales.Caption = "Vales"
        Me.mnuRepVales.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVales.Id = 158
        Me.mnuRepVales.Name = "mnuRepVales"
        '
        'mnuRepVentasCancelaciones
        '
        Me.mnuRepVentasCancelaciones.Caption = "Condonaci�n de Gastos Administrativos."
        Me.mnuRepVentasCancelaciones.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVentasCancelaciones.Id = 159
        Me.mnuRepVentasCancelaciones.Name = "mnuRepVentasCancelaciones"
        '
        'mnuProCambiarConvenio
        '
        Me.mnuProCambiarConvenio.Caption = "Cambiar Convenio"
        Me.mnuProCambiarConvenio.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambiarConvenio.Id = 160
        Me.mnuProCambiarConvenio.Name = "mnuProCambiarConvenio"
        '
        'mnuHerrPermisosExtendidos
        '
        Me.mnuHerrPermisosExtendidos.Caption = "Permisos Extendidos"
        Me.mnuHerrPermisosExtendidos.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuHerrPermisosExtendidos.Id = 161
        Me.mnuHerrPermisosExtendidos.Name = "mnuHerrPermisosExtendidos"
        '
        'mnuRepBonificaciones
        '
        Me.mnuRepBonificaciones.Caption = "Bonificaciones"
        Me.mnuRepBonificaciones.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepBonificaciones.Id = 162
        Me.mnuRepBonificaciones.Name = "mnuRepBonificaciones"
        '
        'mnuProTimbrarFacturas
        '
        Me.mnuProTimbrarFacturas.Caption = "Timbrar Facturas"
        Me.mnuProTimbrarFacturas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProTimbrarFacturas.Id = 163
        Me.mnuProTimbrarFacturas.Name = "mnuProTimbrarFacturas"
        '
        'mnuProTimbrarComprobantesSimplificados
        '
        Me.mnuProTimbrarComprobantesSimplificados.Caption = "Timbrar Comprobantes Simplificados"
        Me.mnuProTimbrarComprobantesSimplificados.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProTimbrarComprobantesSimplificados.Id = 164
        Me.mnuProTimbrarComprobantesSimplificados.Name = "mnuProTimbrarComprobantesSimplificados"
        '
        'mnuRepFacturasPendientesTimbrar
        '
        Me.mnuRepFacturasPendientesTimbrar.Caption = "Facturas Pendientes de Timbrar"
        Me.mnuRepFacturasPendientesTimbrar.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepFacturasPendientesTimbrar.Id = 165
        Me.mnuRepFacturasPendientesTimbrar.Name = "mnuRepFacturasPendientesTimbrar"
        '
        'mnuRepRecibosCancelados
        '
        Me.mnuRepRecibosCancelados.Caption = "Recibos Cancelados"
        Me.mnuRepRecibosCancelados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepRecibosCancelados.Id = 166
        Me.mnuRepRecibosCancelados.Name = "mnuRepRecibosCancelados"
        '
        'mnuProReimpresionAbonosCFS
        '
        Me.mnuProReimpresionAbonosCFS.Caption = "Reimpresiones Abonos y CFS"
        Me.mnuProReimpresionAbonosCFS.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProReimpresionAbonosCFS.Id = 167
        Me.mnuProReimpresionAbonosCFS.Name = "mnuProReimpresionAbonosCFS"
        '
        'mnuProAnticipos
        '
        Me.mnuProAnticipos.Caption = "Anticipos"
        Me.mnuProAnticipos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProAnticipos.Id = 168
        Me.mnuProAnticipos.Name = "mnuProAnticipos"
        '
        'mnuCatOpcioonesCancelacion
        '
        Me.mnuCatOpcioonesCancelacion.Caption = "Opciones de Cancelacion"
        Me.mnuCatOpcioonesCancelacion.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatOpcioonesCancelacion.Id = 169
        Me.mnuCatOpcioonesCancelacion.Name = "mnuCatOpcioonesCancelacion"
        '
        'mnuCatTiposVales
        '
        Me.mnuCatTiposVales.Caption = "Tipos de Vales"
        Me.mnuCatTiposVales.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatTiposVales.Id = 170
        Me.mnuCatTiposVales.Name = "mnuCatTiposVales"
        '
        'mnuRepAnticipos
        '
        Me.mnuRepAnticipos.Caption = "Anticipos"
        Me.mnuRepAnticipos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAnticipos.Id = 171
        Me.mnuRepAnticipos.Name = "mnuRepAnticipos"
        '
        'mnuProReimpresionAbonoMenos
        '
        Me.mnuProReimpresionAbonoMenos.Caption = "Reimpresion Abono Menos"
        Me.mnuProReimpresionAbonoMenos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProReimpresionAbonoMenos.Id = 172
        Me.mnuProReimpresionAbonoMenos.Name = "mnuProReimpresionAbonoMenos"
        '
        'mnuRepMenos
        '
        Me.mnuRepMenos.Caption = "Menos"
        Me.mnuRepMenos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMenos.Id = 173
        Me.mnuRepMenos.Name = "mnuRepMenos"
        '
        'mnuRepPedidosFabricaSurtidosFecha
        '
        Me.mnuRepPedidosFabricaSurtidosFecha.Caption = "Pedidos a Fabrica surtidos por fecha"
        Me.mnuRepPedidosFabricaSurtidosFecha.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepPedidosFabricaSurtidosFecha.Id = 174
        Me.mnuRepPedidosFabricaSurtidosFecha.Name = "mnuRepPedidosFabricaSurtidosFecha"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 374)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        Dim oPermisosEspeciales As New VillarrealBusiness.clsIdentificadoresPermisos

        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If

        response = oPermisosEspeciales.ListadoPermisos(TINApp.Connection.User, TINApp.Prefix)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim Row As DataRow
            Dim i As Long = 0

            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                ReDim Comunes.Common.Identificadores(odataset.Tables(0).Rows.Count() - 1)

                For Each Row In odataset.Tables(0).Rows
                    Comunes.Common.Identificadores.SetValue(Row("identificador"), i)
                    i = i + 1
                    'If i = 1 Then
                    '    Cadena_identificadores = Row("identificador")
                    'Else
                    '    Cadena_identificadores = Cadena_identificadores + "," + Row("identificador")
                    'End If
                Next
            Else
                'ShowMessage(MessageType.MsgInformation, "No hay registros de Permisos para este usuario.")
            End If
        End If
    End Sub

#End Region

#Region "Catalogos"
    Public Sub mnuCatVendedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatVendedores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oVendedores As New VillarrealBusiness.clsVendedores
        With oGrid
            .Title = "Vendedores"
            .UpdateTitle = "Vendedores"
            .Size = New Size(795, 643)
            .DataSource = AddressOf oVendedores.Listado
            .UpdateForm = New Comunes.frmVendedores
            .Format = AddressOf Comunes.clsFormato.for_vendedores_grs
            .MdiParent = Me
            .Show()
        End With
        oVendedores = Nothing
    End Sub

    Public Sub mnuCatPlanesCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatPlanesCredito.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
        With oGrid
            .Title = "Planes de Cr�dito"
            .UpdateTitle = "un Plan de Cr�dito"
            .Size = New Size(880, 643)
            .DataSource = AddressOf oPlanesCredito.Listado
            .UpdateForm = New frmPlanesCredito
            .Format = AddressOf for_planes_credito_grs
            .MdiParent = Me
            .Show()
        End With
        oPlanesCredito = Nothing
    End Sub

    Public Sub mnuCatAutorizacionesVentasPedidoFabrica_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatAutorizacionesVentasPedidoFabrica.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAutorizacionesVentasPedidoFabrica As New VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
        With oGrid
            .Title = "Autorizaciones de Ventas Pedido a Fabrica"
            .UpdateTitle = "Autorizaciones Ventas Pedido Fabrica"
            .Size = New Size(880, 643)
            .DataSource = AddressOf oAutorizacionesVentasPedidoFabrica.Listado
            .UpdateForm = New frmAutorizacionesVentasPedidoFabrica
            .Format = AddressOf for_autorizaciones_ventas_pedido_fabrica_grs
            .MdiParent = Me
            .Show()
        End With
        oAutorizacionesVentasPedidoFabrica = Nothing
    End Sub

    Public Sub mnuCatClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatClientes.ItemClick
        Dim oGrid As New Comunes.brwClientes

        With oGrid
            .MenuOption = e.Item
            .Title = "Clientes"
            .UpdateTitle = "un Cliente"
            .UpdateForm = New Comunes.frmClientes
            .Format = AddressOf Comunes.clsFormato.for_clientes_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuCatOpcioonesCancelacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatOpcioonesCancelacion.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oOpciones As New VillarrealBusiness.clsOpcionesCancelacion
        With oGrid
            .Title = "Opciones de Cancelaci�n"
            .UpdateTitle = "Opcion de Cancelaci�n"
            .DataSource = AddressOf oOpciones.Listado
            .UpdateForm = New frmOpcionesCancelacion
            .Format = AddressOf for_opciones_cancelacion_grs
            .MdiParent = Me
            .Show()
        End With
        oOpciones = Nothing
    End Sub

    Public Sub mnuCatTiposVales_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatTiposVales.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTiposVales As New VillarrealBusiness.clsTiposVales
        With oGrid
            .Title = "Tipos de Vales"
            .UpdateTitle = "Tipo de Vale"
            .DataSource = AddressOf oTiposVales.Listado
            .UpdateForm = New frmTiposVales
            .Format = AddressOf for_tipos_vales_grs
            .MdiParent = Me
            .Show()
        End With
        oTiposVales = Nothing
    End Sub

#End Region

#Region "Procesos"
    Public Sub mnuProAbrirOrdeneCompra_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAbrirOrdeneCompra.ItemClick
        Dim oForm As New frmAbrirOrdenCompra
        With oForm
            .MenuOption = e.Item
            .Title = "Abrir Orden de Compra"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProCambiarVendedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambiarVendedor.ItemClick
        Dim oForm As New frmCambiarVendedor
        With oForm
            .MenuOption = e.Item
            .Title = "Cambiar Vendedor"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProMovimientosArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosArticulos.ItemClick
        Dim oForm As New Comunes.frmMovimientosArticulos
        With oForm
            .MenuOption = e.Item
            .Title = "Movimientos del Art�culo"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProMovimientosCliente_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosCliente.ItemClick
        Dim oForm As New Comunes.frmMovimientosClientes
        With oForm
            .MenuOption = e.Item
            .Title = "Movimientos del Cliente"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProConsultarNotaCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProConsultarNotaCredito.ItemClick
        Dim oForm As New frmConsultaNotaCredito
        With oForm
            .MenuOption = e.Item
            .Title = "Consultar Nota de Cr�dito"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub mnuProConsultaNotaCargo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProConsultaNotaCargo.ItemClick
        Dim oForm As New frmConsultaNotaCargo
        With oForm
            .MenuOption = e.Item
            .Title = "Consultar Nota de Cargo"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProConsultaVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProConsultaVentas.ItemClick
        Dim oGrid As New brwVentas
        With oGrid
            .CanInsert = False
            .CanDelete = False
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Consulta de Ventas"
            .UpdateTitle = "una Venta"
            .UpdateForm = New Comunes.frmVentas
            .Size = New Size(880, 643)
            .Format = AddressOf for_ventas_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProCambiaBodegaSalidaVenta_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambiaBodegaSalidaVenta.ItemClick
        Dim oForm As New frmCambiaBodegaSalidaVenta
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Cambia Bodega de Salida de Art�culos de una Venta"
        oForm.Show()
    End Sub
    Public Sub mnuProCotizacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCotizacion.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCotizaciones As New VillarrealBusiness.clsCotizaciones
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Cotizaci�n"
            .DataSource = AddressOf oCotizaciones.Listado
            .UpdateTitle = "una Cotizaci�n"
            .UpdateForm = New frmCotizaciones
            .Format = AddressOf for_cotizaciones_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProDescuentosEspecialesProgramados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDescuentosEspecialesProgramados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Descuentos Especiales Programados"
            .DataSource = AddressOf oDescuentosEspecialesProgramados.Listado
            .UpdateTitle = "un Descuento Especial Programado"
            .UpdateForm = New Comunes.frmDescuentosEspecialesProgramados
            .Format = AddressOf Comunes.clsFormato.for_descuentos_especiales_programados_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProAutorizacionesBonificacionesInteres_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAutorizacionesBonificacionesInteres.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAutorizacionesBonificacionesInteres As New VillarrealBusiness.clsAutorizacionesBonificacionesInteres
        With oGrid
            .Title = "Autorizaciones para Bonificar Interes"
            .UpdateTitle = "Autorizaci�n "
            .DataSource = AddressOf oAutorizacionesBonificacionesInteres.Listado
            .UpdateForm = New frmAutorizacionesBonificacionesInteres
            .Format = AddressOf for_autorizaciones_bonificaciones_interes_grs
            .MdiParent = Me
            .Show()
        End With
        oAutorizacionesBonificacionesInteres = Nothing
    End Sub
    Public Sub mnuProDescuentosEspecialesClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDescuentosEspecialesClientes.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oDescuentosEspecialesClientes As New VillarrealBusiness.clsDescuentosEspecialesClientes
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Descuentos Especiales a Clientes"
            .DataSource = AddressOf oDescuentosEspecialesClientes.Listado
            .UpdateTitle = "un Descuento Especial a Clientes"
            .UpdateForm = New frmDescuentosEspecialesClientes
            .Format = AddressOf for_descuentos_especiales_clientes_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub mnuProCancelacionesVentasSinRegistro_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCancelacionesVentasSinRegistro.ItemClick
        Dim oForm As New frmCancelacionesVentas
        With oForm
            .ConRegistro = False
            .MenuOption = e.Item
            .Title = "Cancelar una Venta sin Registro"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Private Sub mnuProCancelacionesVentasConRegistro_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCancelacionesVentasConRegistro.ItemClick
        Dim oForm As New frmCancelacionesVentas
        With oForm
            .ConRegistro = True
            .MenuOption = e.Item
            .Title = "Cancelar una Venta con Registro"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuProMovimientosClienteMaestra_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosClienteMaestra.ItemClick
        Dim oForm As New Comunes.frmMovimientosClientes
        With oForm
            .MenuOption = e.Item
            .ConsultaMaestra = True
            .Title = "Movimientos del Cliente"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuProVales_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProVales.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oVales As New VillarrealBusiness.clsVales
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Vales"
            .DataSource = AddressOf oVales.Listado
            .UpdateTitle = "un Vale"
            .CanDelete = False
            .CanInsert = True
            .UpdateForm = New frmVales
            .Format = AddressOf Comunes.clsFormato.for_vales_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuProCambiarConvenio_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambiarConvenio.ItemClick
        Dim oForm As New frmCambioConvenio
        With oForm
            .MenuOption = e.Item
            .Title = "Cambiar Convenio de una Venta"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuProTimbrarFacturas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTimbrarFacturas.ItemClick
        Dim oForm As New Comunes.frmTimbrarFacturas
        With oForm
            .MenuOption = e.Item
            .Title = "Timbrar Factura"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProTimbrarComprobantesSimplificados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTimbrarComprobantesSimplificados.ItemClick
        Dim oForm As New Comunes.frmTimbrarComprobantesSimplificados
        With oForm
            .MenuOption = e.Item
            .Title = "Timbrar Comprobantes Simplificados"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProReimpresionAbonosCFS_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProReimpresionAbonosCFS.ItemClick
        Dim oForm As New Comunes.frmReimpresionRecibosCFS
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Reimpresi�n de Abonos y CFS"
            .Show()
        End With
    End Sub
    Public Sub mnuProAnticipos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAnticipos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAnticipos As New VillarrealBusiness.clsAnticipos
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Anticipos"
            .DataSource = AddressOf oAnticipos.Listado
            .UpdateTitle = "un Anticipo"
            .UpdateForm = New frmAnticipos
            .Format = AddressOf for_anticipos_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProReimpresionAbonoMenos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProReimpresionAbonoMenos.ItemClick
        Dim oForm As New Comunes.frmReimpresionAbonosMenos
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Reimpresi�n de Abonos Menos"
            .Show()
        End With
    End Sub
    

#End Region

#Region "Reportes"
    Public Sub mnuRepSaldosGenerales_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosGenerales.ItemClick
        Dim oForm As New Comunes.frmRepSaldosGenerales
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Generales"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepResumenIngresos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenIngresos.ItemClick
        Dim oForm As New Comunes.frmResumenIngresos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Ingresos"
        oForm.Action = Actions.Report
        oForm.Ingresos = True
        oForm.Show()
    End Sub
    Public Sub mnuRepIngresosxCobrador_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepIngresosxCobrador.ItemClick
        Dim oForm As New Comunes.frmIngresosxCobrador
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ingresos por Cobrador"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepIngresosxFormaPago_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepIngresosxFormaPago.ItemClick
        Dim oForm As New Comunes.frmIngresosxFormaPago
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ingresos por Forma de Pago"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepBonificaciones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepBonificaciones.ItemClick
        Dim oForm As New frmRepBonificaciones
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Bonificaciones"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDiarioVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiarioVentas.ItemClick
        Dim oForm As New Comunes.frmDiarioVentas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diario de Ventas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepArticulosMasVendidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosMasVendidos.ItemClick
        Dim oForm As New frmArticulosMasVendidos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Art�culos m�s Vendidos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepDiferenciaDePrecios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiferenciaDePrecios.ItemClick
        Dim oForm As New frmDiferenciaDePrecios
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diferencia de Precios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepNotasCanceladas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepNotasCanceladas.ItemClick
        Dim oForm As New frmNotasCanceladas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Notas Canceladas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepDiarioVentasExpo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiarioVentasExpo.ItemClick
        Dim oForm As New frmDiarioVentasExpo
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diario de Ventas Expo"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepDiarioVentasDetallado_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiarioVentasDetallado.ItemClick
        Dim oForm As New Comunes.frmDiarioVentasDetallado
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diario de Ventas Detallado"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepFacturasCanceladas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepFacturasCanceladas.ItemClick
        Dim oForm As New frmRepFacturasCanceladas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Facturas Canceladas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepVentasDiarias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasDiarias.ItemClick
        Dim oForm As New frmVentasDiarias
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ventas Diarias "
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepResumenSalidas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenSalidas.ItemClick
        Dim oForm As New Comunes.frmResumenSalidas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Salidas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepResumenContabilidad_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenContabilidad.ItemClick
        Dim oForm As New Comunes.frmResumenContabilidad
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Contabilidad"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepEstadosCuentaClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEstadosCuentaClientes.ItemClick
        Dim oForm As New Comunes.frmRepEstadoDeCuenta
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Estado de Cuenta"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepSaldosVencidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosVencidos.ItemClick
        Dim oForm As New Comunes.frmRepSaldosVencidos
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Vencidos"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepVentasxFechaEntrega_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasxFechaEntrega.ItemClick
        Dim oForm As New Comunes.frmRepVentasxFechaEntrega
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Ventas por Fecha de Entrega"
            .Action = Actions.Report
            .Show()
        End With
    End Sub

    Public Sub mnuRecordatorioSaldo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRecordatorioSaldo.ItemClick
        Dim oForm As New frmRecordatorioSaldo
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Recordatorio de Saldo"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuConfirmacionSaldoCobrador_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuConfirmacionSaldoCobrador.ItemClick
        Dim oForm As New frmConfirmacionSaldosCobrador
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Confirmaci�n de Saldo de Cobrador"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Private Sub mnuReporteEstadistico_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReporteEstadistico.ItemClick
        Dim oForm As New frmReporteEstadistico
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen Estadistico"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepVentasVendedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasVendedor.ItemClick
        Dim oForm As New Comunes.frmRepVentasVendedor
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ventas por Vendedor"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepUtilidadVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepUtilidadVentas.ItemClick
        Dim oForm As New frmRepUtilidadVenta
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Utilidad de Ventas"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepArticulosMasVendidosGrupo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosMasVendidosGrupo.ItemClick
        Dim oForm As New frmArticulosMasVendidosGrupo
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Articulos mas Vendidos por Grupos"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepVales_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVales.ItemClick
        Dim oForm As New frmReporteVales
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Vales"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepVentasCancelaciones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasCancelaciones.ItemClick
        Dim oForm As New frmRepVentasCancelaciones
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Condonaci�n de Gastos Administrativos."
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepFacturasPendientesTimbrar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepFacturasPendientesTimbrar.ItemClick
        Dim oForm As New frmRepFacturasPendientesTimbrar
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Facturas Pendientes de Timbrar"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepRecibosCancelados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepRecibosCancelados.ItemClick
        Dim oForm As New frmRepRecibosCancelados
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Recibos Cancelados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepAnticipos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAnticipos.ItemClick
        Dim oForm As New frmRepAnticipos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Anticipos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepMenos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMenos.ItemClick
        Dim oForm As New frmRepMenos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Menos"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub

    Public Sub mnuRepPedidosFabricaSurtidosFecha_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepPedidosFabricaSurtidosFecha.ItemClick
        Dim oForm As New frmRepPedidosFabricaSurtidosFecha
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Pedidos a fabrica surtidos por fecha"
        oForm.Action = Actions.Report

        oForm.Show()
    End Sub
#End Region

#Region "Herramientas"

    Public Sub mnuUtiVariables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUtiVariables.ItemClick
        Dim oForm As New Comunes.frmVariables
        oForm.MdiParent = Me
        oForm.Show()
    End Sub

    Private Sub mnuCatUsuariosAlmacen_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatUsuariosAlmacen.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oUsuarios As New VillarrealBusiness.clsUsuarios

        With oGrid
            .Title = "Configuraci�n de Usuarios"
            .UpdateTitle = ""
            .DataSource = AddressOf oUsuarios.ListadoUsuariosAlmacen
            .UpdateForm = New Comunes.frmUsuariosAlmacen
            .Format = AddressOf Comunes.clsFormato.for_usuarios_almacen_grs
            .MdiParent = Me
            .CanInsert = False
            .CanDelete = False
            .Width = 600
            .Show()
        End With
        oUsuarios = Nothing

    End Sub

    Public Sub mnuHerrPermisosExtendidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHerrPermisosExtendidos.ItemClick
        Dim oForm As New Comunes.frmPermisosIdentificadores
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Permisos Extendidos"
        oForm.Action = Actions.None
        oForm.Show()
    End Sub

#End Region


   
   
End Class
