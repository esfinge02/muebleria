Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class brwVentas
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.FilterPanel.SuspendLayout()
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        Me.tbrExtended.Size = New System.Drawing.Size(700, 22)
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 590)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(872, 8)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.lblSucursal)
        Me.FilterPanel.Controls.Add(Me.lblOrden)
        Me.FilterPanel.Controls.Add(Me.lkpSucursal)
        Me.FilterPanel.Controls.Add(Me.lkpSerie)
        Me.FilterPanel.Controls.Add(Me.dteFecha)
        Me.FilterPanel.Controls.Add(Me.Label4)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(872, 85)
        Me.FilterPanel.TabIndex = 0
        Me.FilterPanel.Controls.SetChildIndex(Me.Label4, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpSerie, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblSucursal, 0)
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 4, 8, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(96, 56)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(19, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 23)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Fecha:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(32, 8)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(44, 32)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 8)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(96, 32)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(136, 20)
        Me.lkpSerie.TabIndex = 3
        Me.lkpSerie.Tag = ""
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'brwVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(872, 598)
        Me.Name = "brwVentas"
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Declaraciones"
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oVentas As New VillarrealBusiness.clsVentas
#End Region

#Region "Propiedades"
    Private ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub brwVentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor).Date
        Me.FooterPanel.Visible = False
    End Sub

    Private Sub brwVentas_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = Comunes.clsUtilerias.ValidaFecha(Me.dteFecha.Text)
        If Response.ErrorFound Then Exit Sub
        Response = oVentas.ListadoEjecutivo(sucursal, serie, Me.dteFecha.DateTime.Date)
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lkpSerie.EditValue = Nothing
        lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        If sucursal > 0 And serie.Length > 0 And IsDate(Me.dteFecha.EditValue) Then
            Me.Reload(True)
        End If
    End Sub

    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
        If sucursal > 0 And serie.Length > 0 And IsDate(Me.dteFecha.EditValue) Then
            Me.Reload(True)
        End If
    End Sub

#End Region

   

End Class
