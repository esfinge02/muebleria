Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRecordatorioSaldo
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRecordatorioSaldo))
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(353, 28)
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = True
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(64, 48)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(296, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 60
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(8, 48)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 59
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmRecordatorioSaldo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 88)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Name = "frmRecordatorioSaldo"
        Me.Text = "frmRecordatorioSaldo"
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)

        End Get
    End Property


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim oClientes As New VillarrealBusiness.clsClientes
        Dim Response As New Events
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Me.lkpCliente.DataSource = CType(Response.Value, DataSet).Tables(0)
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
  
    Private Sub frmRecordatorioSaldo_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Me.Cliente = -1 Then
            Response.Message = "El cliente es requerido"
        End If

    End Sub

    Private Sub frmRecordatorioSaldo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim oReportes As New VillarrealBusiness.Reportes

        Response = oReportes.RecordatorioSaldo(Cliente)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet

                Dim oReport As New rptRecordatorioSaldo


                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)

                'TinApp.PrintReport(oreport, True, True)
                TINApp.ShowReport(Me.MdiParent, "Recordatorio de Saldo", oReport, , , , True)
                oReport = Nothing
            End If
        End If

    End Sub
End Class
