Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmReporteVales
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkVencidos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpTipoVale As Dipros.Editors.TINMultiLookup
    Public WithEvents lblTipoVale As System.Windows.Forms.Label
    Friend WithEvents dteFechaCorte As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReporteVales))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkVencidos = New DevExpress.XtraEditors.CheckEdit
        Me.lkpTipoVale = New Dipros.Editors.TINMultiLookup
        Me.lblTipoVale = New System.Windows.Forms.Label
        Me.dteFechaCorte = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkVencidos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(8, 120)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(320, 56)
        Me.gpbFechas.TabIndex = 7
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(70, 22)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Ini.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(216, 22)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Fin.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(176, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Has&ta: "
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(14, 96)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(50, 16)
        Me.lblTipoventa.TabIndex = 4
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "Estatus:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "T"
        Me.cboEstatus.Location = New System.Drawing.Point(64, 96)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Activo", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Canjeado", "J", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cambiado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", "C", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(127, 23)
        Me.cboEstatus.TabIndex = 5
        Me.cboEstatus.Tag = ""
        '
        'chkVencidos
        '
        Me.chkVencidos.Location = New System.Drawing.Point(232, 96)
        Me.chkVencidos.Name = "chkVencidos"
        '
        'chkVencidos.Properties
        '
        Me.chkVencidos.Properties.Caption = "Vencidos"
        Me.chkVencidos.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkVencidos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkVencidos.Size = New System.Drawing.Size(96, 22)
        Me.chkVencidos.TabIndex = 6
        '
        'lkpTipoVale
        '
        Me.lkpTipoVale.AllowAdd = False
        Me.lkpTipoVale.AutoReaload = False
        Me.lkpTipoVale.DataSource = Nothing
        Me.lkpTipoVale.DefaultSearchField = ""
        Me.lkpTipoVale.DisplayMember = "nombre"
        Me.lkpTipoVale.EditValue = Nothing
        Me.lkpTipoVale.Filtered = False
        Me.lkpTipoVale.InitValue = Nothing
        Me.lkpTipoVale.Location = New System.Drawing.Point(64, 40)
        Me.lkpTipoVale.MultiSelect = False
        Me.lkpTipoVale.Name = "lkpTipoVale"
        Me.lkpTipoVale.NullText = "(Todos)"
        Me.lkpTipoVale.PopupWidth = CType(520, Long)
        Me.lkpTipoVale.ReadOnlyControl = False
        Me.lkpTipoVale.Required = False
        Me.lkpTipoVale.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTipoVale.SearchMember = ""
        Me.lkpTipoVale.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTipoVale.SelectAll = False
        Me.lkpTipoVale.Size = New System.Drawing.Size(264, 20)
        Me.lkpTipoVale.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTipoVale.TabIndex = 1
        Me.lkpTipoVale.Tag = "tipo_vale"
        Me.lkpTipoVale.ToolTip = Nothing
        Me.lkpTipoVale.ValueMember = "tipo"
        '
        'lblTipoVale
        '
        Me.lblTipoVale.AutoSize = True
        Me.lblTipoVale.Location = New System.Drawing.Point(4, 40)
        Me.lblTipoVale.Name = "lblTipoVale"
        Me.lblTipoVale.Size = New System.Drawing.Size(60, 16)
        Me.lblTipoVale.TabIndex = 0
        Me.lblTipoVale.Tag = ""
        Me.lblTipoVale.Text = "Tipo Vale:"
        Me.lblTipoVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCorte
        '
        Me.dteFechaCorte.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFechaCorte.Location = New System.Drawing.Point(64, 65)
        Me.dteFechaCorte.Name = "dteFechaCorte"
        '
        'dteFechaCorte.Properties
        '
        Me.dteFechaCorte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCorte.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Size = New System.Drawing.Size(128, 23)
        Me.dteFechaCorte.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Corte:"
        '
        'frmReporteVales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(338, 188)
        Me.Controls.Add(Me.dteFechaCorte)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpTipoVale)
        Me.Controls.Add(Me.lblTipoVale)
        Me.Controls.Add(Me.chkVencidos)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboEstatus)
        Me.Controls.Add(Me.gpbFechas)
        Me.Name = "frmReporteVales"
        Me.Text = "frmReporteVales"
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.chkVencidos, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVale, 0)
        Me.Controls.SetChildIndex(Me.lkpTipoVale, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.dteFechaCorte, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkVencidos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oTiposVales As VillarrealBusiness.clsTiposVales
#End Region


    Public ReadOnly Property TipoVale() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTipoVale)
        End Get
    End Property

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmReporteVales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim oDataSet As DataSet

        Response = oReportes.ReporteVales(Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.chkVencidos.Checked, Me.cboEstatus.Value, Me.TipoVale, Me.dteFechaCorte.EditValue)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            oDataSet = CType(Response.Value, DataSet)
            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptVales
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Reporte de Vales", oReport, , , , True)

                oReport = Nothing
            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

        oDataSet = Nothing
    End Sub

    Private Sub frmReporteVales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oTiposVales = New VillarrealBusiness.clsTiposVales

        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))

    End Sub

#End Region


    Private Sub lkpTipoVale_LoadData(ByVal Initialize As Boolean) Handles lkpTipoVale.LoadData
        Dim Response As New Events

        Response = oTiposVales.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTipoVale.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpTipoVale_Format() Handles lkpTipoVale.Format
        Comunes.clsFormato.for_tipos_vales_grl(Me.lkpTipoVale)
    End Sub

End Class
