Imports System.Globalization
Imports System.Threading

Module modInicio
    Public TINApp As New Dipros.Windows.Application
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Ejecutivo"
        TINApp.Prefix = "EJE"

        ' ================================================================================
        Dim oStyle As New DevExpress.Utils.ViewStyle
        oStyle.BackColor = System.Drawing.Color.Beige
        oStyle.Options = oStyle.Options And (DevExpress.Utils.StyleOptions.UseBackColor Or DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseForeColor)
        oStyle.ForeColor = System.Drawing.Color.Black

        TINApp.Options.StyleDisabled = oStyle
        ' ================================================================================

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        Comunes.Common.Aplicacion = TINApp
        Comunes.Common.banEjecutivo = False

        'Inicia la aplicaci�n
        Dim oMain As New frmMain
        Dim blAvanza As Boolean = False

        'PEDIR LA SUCURSAL ACTUAL
        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Then
            If TINApp.Connection.User = "SUPER" Then
                Dim oForm As New Comunes.frmSucursalActual
                Application.Run(oForm)
                If oForm.EntrarMain = True Then
                    blAvanza = True
                Else
                    MsgBox("No hay una Sucursal Definida", MsgBoxStyle.Information, "Mensaje del Sistema")
                    blAvanza = False
                    Exit Sub
                End If
            Else
                MsgBox("No hay una Sucursal Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                blAvanza = False
                Exit Sub
            End If
        Else
            blAvanza = True
        End If

        'PEDIR LA CAJA ACTUAL
        If blAvanza = True Then
            If Comunes.clsUtilerias.AbrirArchivoCaja(Comunes.Common.Caja_Actual) = False Then
                If TINApp.Connection.User = "SUPER" Then
                    Dim oForm As New Comunes.frmCajaActual
                    Application.Run(oForm)
                    If oForm.EntrarMain = True Then
                        blAvanza = True
                    Else
                        MsgBox("No hay una Caja Definida", MsgBoxStyle.Information, "Mensaje del Sistema")
                        blAvanza = False
                        Exit Sub
                    End If
                Else
                    MsgBox("No hay una Caja Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                    blAvanza = False
                    Exit Sub
                End If
            Else
                blAvanza = True
            End If
        End If

        'CHECAR SI EL USUARIO TIENE UN CAJERO ASIGNADO, SOLO EL USUARIO SUPER PUEDE ENTRAR SIN CAJERO ASIGNADO
        If blAvanza = True Then
            If Comunes.clsUtilerias.uti_Usuariocajero(TINApp.Connection.User, Comunes.Common.Cajero) = False Then
                If TINApp.Connection.User = "SUPER" Then
                    If MsgBox("El Usuario No Tiene un Cajero Definido. �Desea continuar?", MsgBoxStyle.YesNo, "Mensaje del Sistema") = MsgBoxResult.Yes Then
                        'blAvanza = True
                        Application.Run(oMain)
                    Else
                        blAvanza = False
                        Exit Sub
                    End If
                Else
                    MsgBox("El Usuario No Tiene un Cajero Definido", MsgBoxStyle.Information, "Mensaje del Sistema")
                    Exit Sub
                End If
            Else
                Application.Run(oMain)
                'blAvanza = True
            End If
        End If


    End Sub
End Module
