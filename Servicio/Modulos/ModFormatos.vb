Imports Dipros.Windows.Forms
Module ModFormatos

#Region "Personal"

    Public Sub for_personal_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Personal", "personal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "CentrosServicio"

    Public Sub for_centros_servicio_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Centro", "centro", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Domicilio", "domicilio", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Contactos", "contactos", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Tel�fonos", "telefonos", 4, DevExpress.Utils.FormatType.None, "", 100)

    End Sub

#End Region

#Region "Garantias"

    Public Sub for_garantias_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Garant�a", "garantia", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "OrdenesServicio"

    Public Sub for_ordenes_servicio_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Orden", "orden_servicio", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Tipo De Servicio", "tipo_servicio_vista", 1, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Cliente", "orden_cliente", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre Cliente", "nombre_cliente", 4, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Proveedor", "nombre_proveedor", 5, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Modelo", "articulo", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "N�mero De Serie", "numero_serie", 7, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Clave Proveedor", "proveedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Garant�a", "garantia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Centro De Servicio", "nombre_centro_servicio", 8, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Falla", "falla", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Enviado Con", "enviado_con", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estatus", "estatus_vista", 9, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Recibe", "nombre_recibe", 10, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Jur�dico", "juridico", 11, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Factura", "folio_factura", 12, DevExpress.Utils.FormatType.None, "")

        AddColumns(oGrid, "Fecha Salida", "fecha_salida_reparacion", 13, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Fecha Entrada", "fecha_entrada_reparacion", 14, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Devoluci�n", "devolucion", 15, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Articulos"

    Public Sub for_articulos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Art�culo", "articulo", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Modelo", "modelo", 1, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "C�digo de Barras", "codigo_barras", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripci�n", "descripcion_corta", 3, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "Serie", "numero_serie", 2, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region

End Module
