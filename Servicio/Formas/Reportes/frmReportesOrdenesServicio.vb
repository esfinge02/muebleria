Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmReportesOrdenesServicio
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblReporte As System.Windows.Forms.Label
    Friend WithEvents cboReporte As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpClientes As System.Windows.Forms.GroupBox
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gpProveedor As System.Windows.Forms.GroupBox
    Friend WithEvents lblCentro_Servicio As System.Windows.Forms.Label
    Friend WithEvents lkpCentro_Servicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpCentroServicio As System.Windows.Forms.GroupBox
    Friend WithEvents gpRecibe As System.Windows.Forms.GroupBox
    Friend WithEvents lblRecibe As System.Windows.Forms.Label
    Friend WithEvents lkpRecibe As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTipo_Servicio As System.Windows.Forms.Label
    Friend WithEvents cboTipo_Servicio As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblEstatus As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents chkFechaPromesa As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReportesOrdenesServicio))
        Me.lblReporte = New System.Windows.Forms.Label
        Me.cboReporte = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.gpClientes = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.gpProveedor = New System.Windows.Forms.GroupBox
        Me.gpCentroServicio = New System.Windows.Forms.GroupBox
        Me.lblCentro_Servicio = New System.Windows.Forms.Label
        Me.lkpCentro_Servicio = New Dipros.Editors.TINMultiLookup
        Me.gpRecibe = New System.Windows.Forms.GroupBox
        Me.lblRecibe = New System.Windows.Forms.Label
        Me.lkpRecibe = New Dipros.Editors.TINMultiLookup
        Me.lblTipo_Servicio = New System.Windows.Forms.Label
        Me.cboTipo_Servicio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblEstatus = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.chkFechaPromesa = New DevExpress.XtraEditors.CheckEdit
        CType(Me.cboReporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpClientes.SuspendLayout()
        Me.gpProveedor.SuspendLayout()
        Me.gpCentroServicio.SuspendLayout()
        Me.gpRecibe.SuspendLayout()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFechaPromesa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblReporte
        '
        Me.lblReporte.AutoSize = True
        Me.lblReporte.Location = New System.Drawing.Point(16, 60)
        Me.lblReporte.Name = "lblReporte"
        Me.lblReporte.Size = New System.Drawing.Size(120, 16)
        Me.lblReporte.TabIndex = 2
        Me.lblReporte.Tag = ""
        Me.lblReporte.Text = "&Ordenes de Servicio:"
        Me.lblReporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboReporte
        '
        Me.cboReporte.EditValue = "FE"
        Me.cboReporte.Location = New System.Drawing.Point(144, 60)
        Me.cboReporte.Name = "cboReporte"
        '
        'cboReporte.Properties
        '
        Me.cboReporte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboReporte.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Fecha", "FE", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Cliente", "CL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Proveedor", "PR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Centro de Servicio", "CS", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Persona que Recibe", "RE", -1)})
        Me.cboReporte.Size = New System.Drawing.Size(208, 23)
        Me.cboReporte.TabIndex = 3
        Me.cboReporte.Tag = ""
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(16, 158)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(344, 40)
        Me.gpbFechas.TabIndex = 9
        Me.gpbFechas.TabStop = False
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(240, 14)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Fin.TabIndex = 10
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 14)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Ini.TabIndex = 8
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(33, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(198, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "&Hasta: "
        '
        'gpClientes
        '
        Me.gpClientes.Controls.Add(Me.Label2)
        Me.gpClientes.Controls.Add(Me.lkpCliente)
        Me.gpClientes.Enabled = False
        Me.gpClientes.Location = New System.Drawing.Point(16, 198)
        Me.gpClientes.Name = "gpClientes"
        Me.gpClientes.Size = New System.Drawing.Size(344, 40)
        Me.gpClientes.TabIndex = 62
        Me.gpClientes.TabStop = False
        Me.gpClientes.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cliente:"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(72, 14)
        Me.lkpCliente.MultiSelect = True
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = "(Todos)"
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(264, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 5
        Me.lkpCliente.ToolTip = "Cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(88, 14)
        Me.lkpProveedor.MultiSelect = True
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(400, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(240, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 13
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Pr&oveedor:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'gpProveedor
        '
        Me.gpProveedor.Controls.Add(Me.lkpProveedor)
        Me.gpProveedor.Controls.Add(Me.Label3)
        Me.gpProveedor.Enabled = False
        Me.gpProveedor.Location = New System.Drawing.Point(16, 198)
        Me.gpProveedor.Name = "gpProveedor"
        Me.gpProveedor.Size = New System.Drawing.Size(344, 40)
        Me.gpProveedor.TabIndex = 10
        Me.gpProveedor.TabStop = False
        Me.gpProveedor.Visible = False
        '
        'gpCentroServicio
        '
        Me.gpCentroServicio.Controls.Add(Me.lblCentro_Servicio)
        Me.gpCentroServicio.Controls.Add(Me.lkpCentro_Servicio)
        Me.gpCentroServicio.Enabled = False
        Me.gpCentroServicio.Location = New System.Drawing.Point(16, 198)
        Me.gpCentroServicio.Name = "gpCentroServicio"
        Me.gpCentroServicio.Size = New System.Drawing.Size(344, 40)
        Me.gpCentroServicio.TabIndex = 62
        Me.gpCentroServicio.TabStop = False
        Me.gpCentroServicio.Visible = False
        '
        'lblCentro_Servicio
        '
        Me.lblCentro_Servicio.AutoSize = True
        Me.lblCentro_Servicio.Location = New System.Drawing.Point(12, 16)
        Me.lblCentro_Servicio.Name = "lblCentro_Servicio"
        Me.lblCentro_Servicio.Size = New System.Drawing.Size(109, 16)
        Me.lblCentro_Servicio.TabIndex = 19
        Me.lblCentro_Servicio.Tag = ""
        Me.lblCentro_Servicio.Text = "&Centro de servicio:"
        Me.lblCentro_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCentro_Servicio
        '
        Me.lkpCentro_Servicio.AllowAdd = False
        Me.lkpCentro_Servicio.AutoReaload = False
        Me.lkpCentro_Servicio.DataSource = Nothing
        Me.lkpCentro_Servicio.DefaultSearchField = ""
        Me.lkpCentro_Servicio.DisplayMember = "descripcion"
        Me.lkpCentro_Servicio.EditValue = Nothing
        Me.lkpCentro_Servicio.Filtered = False
        Me.lkpCentro_Servicio.InitValue = Nothing
        Me.lkpCentro_Servicio.Location = New System.Drawing.Point(124, 14)
        Me.lkpCentro_Servicio.MultiSelect = True
        Me.lkpCentro_Servicio.Name = "lkpCentro_Servicio"
        Me.lkpCentro_Servicio.NullText = "(Todos)"
        Me.lkpCentro_Servicio.PopupWidth = CType(400, Long)
        Me.lkpCentro_Servicio.ReadOnlyControl = False
        Me.lkpCentro_Servicio.Required = False
        Me.lkpCentro_Servicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCentro_Servicio.SearchMember = ""
        Me.lkpCentro_Servicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCentro_Servicio.SelectAll = True
        Me.lkpCentro_Servicio.Size = New System.Drawing.Size(212, 20)
        Me.lkpCentro_Servicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCentro_Servicio.TabIndex = 20
        Me.lkpCentro_Servicio.Tag = "centro_servicio"
        Me.lkpCentro_Servicio.ToolTip = Nothing
        Me.lkpCentro_Servicio.ValueMember = "centro"
        '
        'gpRecibe
        '
        Me.gpRecibe.Controls.Add(Me.lblRecibe)
        Me.gpRecibe.Controls.Add(Me.lkpRecibe)
        Me.gpRecibe.Enabled = False
        Me.gpRecibe.Location = New System.Drawing.Point(16, 198)
        Me.gpRecibe.Name = "gpRecibe"
        Me.gpRecibe.Size = New System.Drawing.Size(344, 40)
        Me.gpRecibe.TabIndex = 62
        Me.gpRecibe.TabStop = False
        Me.gpRecibe.Visible = False
        '
        'lblRecibe
        '
        Me.lblRecibe.AutoSize = True
        Me.lblRecibe.Location = New System.Drawing.Point(16, 16)
        Me.lblRecibe.Name = "lblRecibe"
        Me.lblRecibe.Size = New System.Drawing.Size(48, 16)
        Me.lblRecibe.TabIndex = 17
        Me.lblRecibe.Tag = ""
        Me.lblRecibe.Text = "&Recibio:"
        Me.lblRecibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpRecibe
        '
        Me.lkpRecibe.AllowAdd = False
        Me.lkpRecibe.AutoReaload = False
        Me.lkpRecibe.DataSource = Nothing
        Me.lkpRecibe.DefaultSearchField = ""
        Me.lkpRecibe.DisplayMember = "nombre"
        Me.lkpRecibe.EditValue = Nothing
        Me.lkpRecibe.Filtered = False
        Me.lkpRecibe.InitValue = Nothing
        Me.lkpRecibe.Location = New System.Drawing.Point(72, 14)
        Me.lkpRecibe.MultiSelect = True
        Me.lkpRecibe.Name = "lkpRecibe"
        Me.lkpRecibe.NullText = "(Todos)"
        Me.lkpRecibe.PopupWidth = CType(400, Long)
        Me.lkpRecibe.ReadOnlyControl = False
        Me.lkpRecibe.Required = False
        Me.lkpRecibe.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpRecibe.SearchMember = ""
        Me.lkpRecibe.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpRecibe.SelectAll = True
        Me.lkpRecibe.Size = New System.Drawing.Size(264, 20)
        Me.lkpRecibe.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpRecibe.TabIndex = 18
        Me.lkpRecibe.Tag = "Recibe"
        Me.lkpRecibe.ToolTip = Nothing
        Me.lkpRecibe.ValueMember = "empleado"
        '
        'lblTipo_Servicio
        '
        Me.lblTipo_Servicio.AutoSize = True
        Me.lblTipo_Servicio.Location = New System.Drawing.Point(42, 86)
        Me.lblTipo_Servicio.Name = "lblTipo_Servicio"
        Me.lblTipo_Servicio.Size = New System.Drawing.Size(95, 16)
        Me.lblTipo_Servicio.TabIndex = 4
        Me.lblTipo_Servicio.Tag = ""
        Me.lblTipo_Servicio.Text = "&Tipo de servicio:"
        Me.lblTipo_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Servicio
        '
        Me.cboTipo_Servicio.EditValue = "T"
        Me.cboTipo_Servicio.Location = New System.Drawing.Point(144, 86)
        Me.cboTipo_Servicio.Name = "cboTipo_Servicio"
        '
        'cboTipo_Servicio.Properties
        '
        Me.cboTipo_Servicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Servicio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Garant�a", "G", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Armado", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Instalaci�n", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Reparaci�n", "R", -1)})
        Me.cboTipo_Servicio.Size = New System.Drawing.Size(120, 23)
        Me.cboTipo_Servicio.TabIndex = 5
        Me.cboTipo_Servicio.Tag = "tipo_servicio"
        '
        'lblEstatus
        '
        Me.lblEstatus.AutoSize = True
        Me.lblEstatus.Location = New System.Drawing.Point(92, 112)
        Me.lblEstatus.Name = "lblEstatus"
        Me.lblEstatus.Size = New System.Drawing.Size(50, 16)
        Me.lblEstatus.TabIndex = 6
        Me.lblEstatus.Tag = ""
        Me.lblEstatus.Text = "&Estatus:"
        Me.lblEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "Todos"
        Me.cboEstatus.Location = New System.Drawing.Point(144, 112)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", "X", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Proceso", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Terminada", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Devoluci�n", "D", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(120, 23)
        Me.cboEstatus.TabIndex = 7
        Me.cboEstatus.Tag = "estatus"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(144, 34)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(208, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(80, 34)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkFechaPromesa
        '
        Me.chkFechaPromesa.Location = New System.Drawing.Point(144, 136)
        Me.chkFechaPromesa.Name = "chkFechaPromesa"
        '
        'chkFechaPromesa.Properties
        '
        Me.chkFechaPromesa.Properties.Caption = "Fecha Promesa"
        Me.chkFechaPromesa.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkFechaPromesa.Size = New System.Drawing.Size(112, 19)
        Me.chkFechaPromesa.TabIndex = 8
        Me.chkFechaPromesa.Tag = ""
        '
        'frmReportesOrdenesServicio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(370, 200)
        Me.Controls.Add(Me.chkFechaPromesa)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.cboEstatus)
        Me.Controls.Add(Me.lblEstatus)
        Me.Controls.Add(Me.lblTipo_Servicio)
        Me.Controls.Add(Me.cboTipo_Servicio)
        Me.Controls.Add(Me.lblReporte)
        Me.Controls.Add(Me.cboReporte)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.gpProveedor)
        Me.Controls.Add(Me.gpCentroServicio)
        Me.Controls.Add(Me.gpClientes)
        Me.Controls.Add(Me.gpRecibe)
        Me.Name = "frmReportesOrdenesServicio"
        Me.Text = "frmReportesOrdenesServicio"
        Me.Controls.SetChildIndex(Me.gpRecibe, 0)
        Me.Controls.SetChildIndex(Me.gpClientes, 0)
        Me.Controls.SetChildIndex(Me.gpCentroServicio, 0)
        Me.Controls.SetChildIndex(Me.gpProveedor, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.cboReporte, 0)
        Me.Controls.SetChildIndex(Me.lblReporte, 0)
        Me.Controls.SetChildIndex(Me.cboTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblEstatus, 0)
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkFechaPromesa, 0)
        CType(Me.cboReporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpClientes.ResumeLayout(False)
        Me.gpProveedor.ResumeLayout(False)
        Me.gpCentroServicio.ResumeLayout(False)
        Me.gpRecibe.ResumeLayout(False)
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFechaPromesa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oClientes As VillarrealBusiness.clsClientes
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oCentrosServicio As VillarrealBusiness.clsCentrosServicio
    Private oEmpleados As VillarrealBusiness.clsEmpleados
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Cliente() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpCliente.ToXML, "Cliente")
        End Get
    End Property
    Private ReadOnly Property Proveedor() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpProveedor.ToXML, "Proveedor")
        End Get
    End Property
    Private ReadOnly Property CentroServicio() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpCentro_Servicio.ToXML, "Centro de Servicio")
        End Get
    End Property
    Private ReadOnly Property Empleado() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpRecibe.ToXML, "Persona que Recibe")
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmReportesOrdenesServicio_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.OrdenesServicio(Sucursal, Me.cboReporte.EditValue, Me.dteFecha_Ini.DateTime, Me.dteFecha_Fin.DateTime, Me.lkpCliente.ToXML, Me.lkpProveedor.ToXML, Me.lkpCentro_Servicio.ToXML, Me.lkpRecibe.ToXML, Me.cboTipo_Servicio.EditValue, Me.cboEstatus.EditValue, Me.chkFechaPromesa.Checked)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Repartos no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptOrdenesServicio
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Ordenes de Servicio", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub
    Private Sub frmReportesOrdenesServicio_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClientes = New VillarrealBusiness.clsClientes
        oProveedores = New VillarrealBusiness.clsProveedores
        oCentrosServicio = New VillarrealBusiness.clsCentrosServicio
        oEmpleados = New VillarrealBusiness.clsEmpleados
        oSucursales = New VillarrealBusiness.clsSucursales



        Me.cboEstatus.SelectedItem = 0 '"Todos"
        Me.cboEstatus.SelectedIndex = 0

        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmReportesOrdenesServicio_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        'dam 23/ene/08 - lo cambie de lookup a lookupcliente
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCentro_Servicio_Format() Handles lkpCentro_Servicio.Format
        Comunes.clsFormato.for_centros_servicio_grl(Me.lkpCentro_Servicio)
    End Sub
    Private Sub lkpCentro_Servicio_LoadData(ByVal Initialize As Boolean) Handles lkpCentro_Servicio.LoadData
        Dim Response As New Events
        Response = oCentrosServicio.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCentro_Servicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpRecibe_Format() Handles lkpRecibe.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpRecibe)
    End Sub
    Private Sub lkpRecibe_LoadData(ByVal Initialize As Boolean) Handles lkpRecibe.LoadData
        Dim response As Events
        response = oEmpleados.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpRecibe.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub cboReporte_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReporte.SelectedIndexChanged
        Select Case cboReporte.Value
            Case "FE"
                EsconderControles()
                CambiarTamanioForma(376, 232)

            Case "CL"
                EsconderControles()
                Me.gpClientes.Enabled = True
                Me.gpClientes.Visible = True
                CambiarTamanioForma(376, 272)

            Case "PR"
                EsconderControles()
                Me.gpProveedor.Enabled = True
                Me.gpProveedor.Visible = True
                CambiarTamanioForma(376, 272)
            Case "CS"
                EsconderControles()
                Me.gpCentroServicio.Enabled = True
                Me.gpCentroServicio.Visible = True
                CambiarTamanioForma(376, 272)
            Case "RE"
                EsconderControles()
                Me.gpRecibe.Enabled = True
                Me.gpRecibe.Visible = True
                CambiarTamanioForma(376, 272)
        End Select
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub EsconderControles()
        Me.gpCentroServicio.Enabled = False
        Me.gpCentroServicio.Visible = False
        Me.gpClientes.Enabled = False
        Me.gpClientes.Visible = False
        Me.gpProveedor.Enabled = False
        Me.gpProveedor.Visible = False
        Me.gpRecibe.Enabled = False
        Me.gpRecibe.Visible = False
    End Sub
    Private Sub CambiarTamanioForma(ByVal ancho As Integer, ByVal alto As Integer)
        Me.Width = ancho
        Me.Height = alto
    End Sub
#End Region

   
End Class
