Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmRepOrdenesServicioSinSolucion
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTipo_Servicio As System.Windows.Forms.Label
    Friend WithEvents cboTipo_Servicio As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblCentro_Servicio As System.Windows.Forms.Label
    Friend WithEvents lkpCentro_Servicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepOrdenesServicioSinSolucion))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTipo_Servicio = New System.Windows.Forms.Label
        Me.cboTipo_Servicio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCentro_Servicio = New System.Windows.Forms.Label
        Me.lkpCentro_Servicio = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(188, 28)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(25, 120)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(344, 40)
        Me.gpbFechas.TabIndex = 6
        Me.gpbFechas.TabStop = False
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(228, 12)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 3
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(68, 12)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(186, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "&Hasta: "
        '
        'lblTipo_Servicio
        '
        Me.lblTipo_Servicio.AutoSize = True
        Me.lblTipo_Servicio.Location = New System.Drawing.Point(39, 64)
        Me.lblTipo_Servicio.Name = "lblTipo_Servicio"
        Me.lblTipo_Servicio.Size = New System.Drawing.Size(95, 16)
        Me.lblTipo_Servicio.TabIndex = 2
        Me.lblTipo_Servicio.Tag = ""
        Me.lblTipo_Servicio.Text = "&Tipo de servicio:"
        Me.lblTipo_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Servicio
        '
        Me.cboTipo_Servicio.EditValue = "T"
        Me.cboTipo_Servicio.Location = New System.Drawing.Point(137, 64)
        Me.cboTipo_Servicio.Name = "cboTipo_Servicio"
        '
        'cboTipo_Servicio.Properties
        '
        Me.cboTipo_Servicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Servicio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Garant�a", "G", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Armado", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Instalaci�n", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Reparaci�n", "R", -1)})
        Me.cboTipo_Servicio.Size = New System.Drawing.Size(120, 20)
        Me.cboTipo_Servicio.TabIndex = 3
        Me.cboTipo_Servicio.Tag = "tipo_servicio"
        '
        'lblCentro_Servicio
        '
        Me.lblCentro_Servicio.AutoSize = True
        Me.lblCentro_Servicio.Location = New System.Drawing.Point(25, 88)
        Me.lblCentro_Servicio.Name = "lblCentro_Servicio"
        Me.lblCentro_Servicio.Size = New System.Drawing.Size(109, 16)
        Me.lblCentro_Servicio.TabIndex = 4
        Me.lblCentro_Servicio.Tag = ""
        Me.lblCentro_Servicio.Text = "&Centro de servicio:"
        Me.lblCentro_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCentro_Servicio
        '
        Me.lkpCentro_Servicio.AllowAdd = False
        Me.lkpCentro_Servicio.AutoReaload = False
        Me.lkpCentro_Servicio.DataSource = Nothing
        Me.lkpCentro_Servicio.DefaultSearchField = ""
        Me.lkpCentro_Servicio.DisplayMember = "descripcion"
        Me.lkpCentro_Servicio.EditValue = Nothing
        Me.lkpCentro_Servicio.Filtered = False
        Me.lkpCentro_Servicio.InitValue = Nothing
        Me.lkpCentro_Servicio.Location = New System.Drawing.Point(137, 88)
        Me.lkpCentro_Servicio.MultiSelect = False
        Me.lkpCentro_Servicio.Name = "lkpCentro_Servicio"
        Me.lkpCentro_Servicio.NullText = "Todos"
        Me.lkpCentro_Servicio.PopupWidth = CType(400, Long)
        Me.lkpCentro_Servicio.ReadOnlyControl = False
        Me.lkpCentro_Servicio.Required = False
        Me.lkpCentro_Servicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCentro_Servicio.SearchMember = ""
        Me.lkpCentro_Servicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCentro_Servicio.SelectAll = False
        Me.lkpCentro_Servicio.Size = New System.Drawing.Size(232, 20)
        Me.lkpCentro_Servicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCentro_Servicio.TabIndex = 5
        Me.lkpCentro_Servicio.Tag = "centro_servicio"
        Me.lkpCentro_Servicio.ToolTip = Nothing
        Me.lkpCentro_Servicio.ValueMember = "centro"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(136, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(72, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmRepOrdenesServicioSinSolucion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 184)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblCentro_Servicio)
        Me.Controls.Add(Me.lkpCentro_Servicio)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lblTipo_Servicio)
        Me.Controls.Add(Me.cboTipo_Servicio)
        Me.Name = "frmRepOrdenesServicioSinSolucion"
        Me.Text = "frmRepOrdenesServicioSinSolucion"
        Me.Controls.SetChildIndex(Me.cboTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lkpCentro_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblCentro_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oClientes As VillarrealBusiness.clsClientes
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oCentrosServicio As VillarrealBusiness.clsCentrosServicio
    Private oEmpleados As VillarrealBusiness.clsEmpleados
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property CentroServicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCentro_Servicio)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmReportesOrdenesServicioSinSolucion_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.OrdenesServicioSinSolucion(Me.dteFecha_Ini.DateTime, Me.dteFecha_Fin.DateTime, Me.cboTipo_Servicio.EditValue, CentroServicio, Sucursal)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Repartos no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptOrdenesServicioSinSolucion
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Ordenes de Servicio sin Soluci�n", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub
    Private Sub frmReportesOrdenesServicioSinSolucion_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClientes = New VillarrealBusiness.clsClientes
        oProveedores = New VillarrealBusiness.clsProveedores
        oCentrosServicio = New VillarrealBusiness.clsCentrosServicio
        oEmpleados = New VillarrealBusiness.clsEmpleados
        oSucursales = New VillarrealBusiness.clsSucursales


        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmReportesOrdenesServicioSinSolucion_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCentro_Servicio_Format() Handles lkpCentro_Servicio.Format
        Comunes.clsFormato.for_centros_servicio_grl(Me.lkpCentro_Servicio)
    End Sub
    Private Sub lkpCentro_Servicio_LoadData(ByVal Initialize As Boolean) Handles lkpCentro_Servicio.LoadData
        Dim response As Events
        response = oCentrosServicio.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCentro_Servicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    'Private Sub EsconderControles()
    '    Me.gpCentroServicio.Enabled = False
    '    Me.gpCentroServicio.Visible = False
    '    Me.gpClientes.Enabled = False
    '    Me.gpClientes.Visible = False
    '    Me.gpProveedor.Enabled = False
    '    Me.gpProveedor.Visible = False
    '    Me.gpRecibe.Enabled = False
    '    Me.gpRecibe.Visible = False
    'End Sub
    'Private Sub CambiarTamanioForma(ByVal ancho As Integer, ByVal alto As Integer)
    '    Me.Width = ancho
    '    Me.Height = alto
    'End Sub
#End Region


End Class
