Imports Dipros.Utils.Common

Public Class frmCentrosServicio
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblCentro As System.Windows.Forms.Label
    Friend WithEvents clcCentro As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents lblContactos As System.Windows.Forms.Label
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents txtTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtContactos As DevExpress.XtraEditors.MemoEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCentrosServicio))
        Me.lblCentro = New System.Windows.Forms.Label
        Me.clcCentro = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.lblContactos = New System.Windows.Forms.Label
        Me.lblTelefonos = New System.Windows.Forms.Label
        Me.txtTelefonos = New DevExpress.XtraEditors.TextEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.txtDomicilio = New DevExpress.XtraEditors.MemoEdit
        Me.txtContactos = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcCentro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1007, 28)
        '
        'lblCentro
        '
        Me.lblCentro.AutoSize = True
        Me.lblCentro.Location = New System.Drawing.Point(37, 40)
        Me.lblCentro.Name = "lblCentro"
        Me.lblCentro.Size = New System.Drawing.Size(46, 16)
        Me.lblCentro.TabIndex = 0
        Me.lblCentro.Tag = ""
        Me.lblCentro.Text = "&Centro:"
        Me.lblCentro.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCentro
        '
        Me.clcCentro.EditValue = "0"
        Me.clcCentro.Location = New System.Drawing.Point(88, 40)
        Me.clcCentro.MaxValue = 0
        Me.clcCentro.MinValue = 0
        Me.clcCentro.Name = "clcCentro"
        '
        'clcCentro.Properties
        '
        Me.clcCentro.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCentro.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCentro.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCentro.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCentro.Properties.Enabled = False
        Me.clcCentro.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCentro.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCentro.Size = New System.Drawing.Size(56, 21)
        Me.clcCentro.TabIndex = 1
        Me.clcCentro.Tag = "centro"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(11, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(24, 100)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 4
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblContactos
        '
        Me.lblContactos.AutoSize = True
        Me.lblContactos.Location = New System.Drawing.Point(19, 136)
        Me.lblContactos.Name = "lblContactos"
        Me.lblContactos.Size = New System.Drawing.Size(64, 16)
        Me.lblContactos.TabIndex = 6
        Me.lblContactos.Tag = ""
        Me.lblContactos.Text = "Co&ntactos:"
        Me.lblContactos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.Location = New System.Drawing.Point(21, 172)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(62, 16)
        Me.lblTelefonos.TabIndex = 8
        Me.lblTelefonos.Tag = ""
        Me.lblTelefonos.Text = "&Telefonos:"
        Me.lblTelefonos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefonos
        '
        Me.txtTelefonos.EditValue = ""
        Me.txtTelefonos.Location = New System.Drawing.Point(88, 172)
        Me.txtTelefonos.Name = "txtTelefonos"
        '
        'txtTelefonos.Properties
        '
        Me.txtTelefonos.Properties.MaxLength = 60
        Me.txtTelefonos.Size = New System.Drawing.Size(360, 22)
        Me.txtTelefonos.TabIndex = 9
        Me.txtTelefonos.Tag = "telefonos"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(88, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(360, 32)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(88, 100)
        Me.txtDomicilio.Name = "txtDomicilio"
        Me.txtDomicilio.Size = New System.Drawing.Size(360, 32)
        Me.txtDomicilio.TabIndex = 5
        Me.txtDomicilio.Tag = "domicilio"
        '
        'txtContactos
        '
        Me.txtContactos.EditValue = ""
        Me.txtContactos.Location = New System.Drawing.Point(88, 136)
        Me.txtContactos.Name = "txtContactos"
        Me.txtContactos.Size = New System.Drawing.Size(360, 32)
        Me.txtContactos.TabIndex = 7
        Me.txtContactos.Tag = "contactos"
        '
        'frmCentrosServicio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 199)
        Me.Controls.Add(Me.txtContactos)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblCentro)
        Me.Controls.Add(Me.clcCentro)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblDomicilio)
        Me.Controls.Add(Me.lblContactos)
        Me.Controls.Add(Me.lblTelefonos)
        Me.Controls.Add(Me.txtTelefonos)
        Me.Name = "frmCentrosServicio"
        Me.Controls.SetChildIndex(Me.txtTelefonos, 0)
        Me.Controls.SetChildIndex(Me.lblTelefonos, 0)
        Me.Controls.SetChildIndex(Me.lblContactos, 0)
        Me.Controls.SetChildIndex(Me.lblDomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcCentro, 0)
        Me.Controls.SetChildIndex(Me.lblCentro, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.txtContactos, 0)
        CType(Me.clcCentro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCentrosServicio As VillarrealBusiness.clsCentrosServicio
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCentrosServicio_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCentrosServicio.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oCentrosServicio.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oCentrosServicio.Eliminar(clcCentro.value)

        End Select
    End Sub

    Private Sub frmCentrosServicio_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oCentrosServicio.DespliegaDatos(OwnerForm.Value("centro"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmCentrosServicio_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCentrosServicio = New VillarrealBusiness.clsCentrosServicio

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCentrosServicio_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCentrosServicio.Validacion(Action, txtDescripcion.Text, txtDomicilio.Text)
    End Sub

    Private Sub frmCentrosServicio_Localize() Handles MyBase.Localize
        Find("centro", Me.clcCentro.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    'Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oCentrosServicio.ValidaDescripcion(txtDescripcion.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub
    'Private Sub txtDomicilio_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDomicilio.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oCentrosServicio.ValidaDomicilio(txtDomicilio.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
