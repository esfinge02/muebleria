Imports Dipros.Utils.Common

Public Class frmGarantias
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblGarantia As System.Windows.Forms.Label
    Friend WithEvents clcGarantia As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGarantias))
        Me.lblGarantia = New System.Windows.Forms.Label
        Me.clcGarantia = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(241, 28)
        '
        'lblGarantia
        '
        Me.lblGarantia.AutoSize = True
        Me.lblGarantia.Location = New System.Drawing.Point(28, 40)
        Me.lblGarantia.Name = "lblGarantia"
        Me.lblGarantia.Size = New System.Drawing.Size(56, 16)
        Me.lblGarantia.TabIndex = 0
        Me.lblGarantia.Tag = ""
        Me.lblGarantia.Text = "&Garant�a:"
        Me.lblGarantia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcGarantia
        '
        Me.clcGarantia.EditValue = "0"
        Me.clcGarantia.Location = New System.Drawing.Point(92, 40)
        Me.clcGarantia.MaxValue = 0
        Me.clcGarantia.MinValue = 0
        Me.clcGarantia.Name = "clcGarantia"
        '
        'clcGarantia.Properties
        '
        Me.clcGarantia.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcGarantia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGarantia.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcGarantia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGarantia.Properties.Enabled = False
        Me.clcGarantia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcGarantia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcGarantia.Size = New System.Drawing.Size(52, 19)
        Me.clcGarantia.TabIndex = 1
        Me.clcGarantia.Tag = "garantia"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(10, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 30
        Me.txtDescripcion.Size = New System.Drawing.Size(180, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmGarantias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(282, 96)
        Me.Controls.Add(Me.lblGarantia)
        Me.Controls.Add(Me.clcGarantia)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmGarantias"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcGarantia, 0)
        Me.Controls.SetChildIndex(Me.lblGarantia, 0)
        CType(Me.clcGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oGarantias As VillarrealBusiness.clsGarantias
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmGarantias_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oGarantias.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oGarantias.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oGarantias.Eliminar(clcGarantia.Value)

        End Select
    End Sub

    Private Sub frmGarantias_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oGarantias.DespliegaDatos(OwnerForm.Value("garantia"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmGarantias_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oGarantias = New VillarrealBusiness.clsGarantias

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmGarantias_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oGarantias.Validacion(Action, Me.txtDescripcion.Text)
    End Sub

    Private Sub frmGarantias_Localize() Handles MyBase.Localize
        Find("garantia", CType(Me.clcGarantia.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
