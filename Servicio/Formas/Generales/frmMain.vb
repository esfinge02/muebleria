Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatCentrosServicio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEmpleados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatGarantias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProOrdenesServicio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepOrdenesServicio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatMunicipios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosConMasFallas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUtiVariables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosConCambioFisico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepOrdenesServicioSinSolucion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatCentrosServicio = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEmpleados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatGarantias = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProOrdenesServicio = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepOrdenesServicio = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatMunicipios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosConMasFallas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuUtiVariables = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosConCambioFisico = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepOrdenesServicioSinSolucion = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatCentrosServicio, Me.mnuCatClientes, Me.mnuCatEmpleados, Me.mnuCatGarantias, Me.mnuProOrdenesServicio, Me.mnuRepOrdenesServicio, Me.mnuCatMunicipios, Me.mnuRepArticulosConMasFallas, Me.mnuUtiVariables, Me.mnuRepArticulosConCambioFisico, Me.mnuRepOrdenesServicioSinSolucion, Me.BarStaticItem1})
        Me.BarManager.MaxItemId = 118
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCentrosServicio), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatMunicipios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEmpleados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatGarantias)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProOrdenesServicio)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiVariables, True)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepOrdenesServicio), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepOrdenesServicioSinSolucion), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosConMasFallas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosConCambioFisico)})
        '
        'mnuCatCentrosServicio
        '
        Me.mnuCatCentrosServicio.Caption = "Centros de &Servicio"
        Me.mnuCatCentrosServicio.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCentrosServicio.Id = 106
        Me.mnuCatCentrosServicio.ImageIndex = 3
        Me.mnuCatCentrosServicio.Name = "mnuCatCentrosServicio"
        '
        'mnuCatClientes
        '
        Me.mnuCatClientes.Caption = "C&lientes"
        Me.mnuCatClientes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatClientes.Id = 107
        Me.mnuCatClientes.ImageIndex = 0
        Me.mnuCatClientes.Name = "mnuCatClientes"
        '
        'mnuCatEmpleados
        '
        Me.mnuCatEmpleados.Caption = "&Empleados"
        Me.mnuCatEmpleados.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEmpleados.Id = 108
        Me.mnuCatEmpleados.ImageIndex = 1
        Me.mnuCatEmpleados.Name = "mnuCatEmpleados"
        '
        'mnuCatGarantias
        '
        Me.mnuCatGarantias.Caption = "&Garant�as"
        Me.mnuCatGarantias.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatGarantias.Id = 109
        Me.mnuCatGarantias.ImageIndex = 6
        Me.mnuCatGarantias.Name = "mnuCatGarantias"
        '
        'mnuProOrdenesServicio
        '
        Me.mnuProOrdenesServicio.Caption = "&Ordenes de Servicio"
        Me.mnuProOrdenesServicio.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProOrdenesServicio.Id = 110
        Me.mnuProOrdenesServicio.ImageIndex = 4
        Me.mnuProOrdenesServicio.Name = "mnuProOrdenesServicio"
        '
        'mnuRepOrdenesServicio
        '
        Me.mnuRepOrdenesServicio.Caption = "Ordenes De Servicio"
        Me.mnuRepOrdenesServicio.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepOrdenesServicio.Id = 111
        Me.mnuRepOrdenesServicio.ImageIndex = 5
        Me.mnuRepOrdenesServicio.Name = "mnuRepOrdenesServicio"
        '
        'mnuCatMunicipios
        '
        Me.mnuCatMunicipios.Caption = "&Municipios"
        Me.mnuCatMunicipios.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatMunicipios.Id = 112
        Me.mnuCatMunicipios.ImageIndex = 2
        Me.mnuCatMunicipios.Name = "mnuCatMunicipios"
        '
        'mnuRepArticulosConMasFallas
        '
        Me.mnuRepArticulosConMasFallas.Caption = "Art�culos Con M�s Fallas"
        Me.mnuRepArticulosConMasFallas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosConMasFallas.Id = 113
        Me.mnuRepArticulosConMasFallas.ImageIndex = 8
        Me.mnuRepArticulosConMasFallas.Name = "mnuRepArticulosConMasFallas"
        '
        'mnuUtiVariables
        '
        Me.mnuUtiVariables.Caption = "Variables del Sistema"
        Me.mnuUtiVariables.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuUtiVariables.Id = 114
        Me.mnuUtiVariables.Name = "mnuUtiVariables"
        '
        'mnuRepArticulosConCambioFisico
        '
        Me.mnuRepArticulosConCambioFisico.Caption = "Art�culos Con Cambio F�sico"
        Me.mnuRepArticulosConCambioFisico.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosConCambioFisico.Id = 115
        Me.mnuRepArticulosConCambioFisico.ImageIndex = 9
        Me.mnuRepArticulosConCambioFisico.Name = "mnuRepArticulosConCambioFisico"
        '
        'mnuRepOrdenesServicioSinSolucion
        '
        Me.mnuRepOrdenesServicioSinSolucion.Caption = "Ordenes De Servicio Sin Soluci�n"
        Me.mnuRepOrdenesServicioSinSolucion.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepOrdenesServicioSinSolucion.Id = 116
        Me.mnuRepOrdenesServicioSinSolucion.ImageIndex = 7
        Me.mnuRepOrdenesServicioSinSolucion.Name = "mnuRepOrdenesServicioSinSolucion"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Id = 117
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim osucursales As New VillarrealBusiness.clsSucursales
        'Dim response As Dipros.Utils.Events
        'response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        'If Not response.ErrorFound Then
        '    Dim odataset As DataSet

        '    odataset = response.Value

        '    Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        'End If
    End Sub

#End Region

#Region " Catalogos "



    Public Sub mnuCatCentrosServicio_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCentrosServicio.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCentrosServicio As New VillarrealBusiness.clsCentrosServicio
        With oGrid
            .Title = "CentrosServicio"
            .UpdateTitle = "Centros de Servicio"
            .DataSource = AddressOf oCentrosServicio.Listado
            .UpdateForm = New frmCentrosServicio
            .Format = AddressOf for_centros_servicio_grs
            .MdiParent = Me
            .Show()
        End With
        oCentrosServicio = Nothing
    End Sub

    'Public Sub mnuCatClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatClientes.ItemClick
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oClientes As New VillarrealBusiness.clsClientes
    '    With oGrid
    '        .Title = "Clientes"
    '        .UpdateTitle = "un Cliente"
    '        .DataSource = AddressOf oClientes.Listado
    '        .UpdateForm = New Comunes.frmClientes
    '        .Format = AddressOf Comunes.clsFormato.for_clientes_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    '    oClientes = Nothing
    'End Sub

    Public Sub mnuCatMunicipios_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatMunicipios.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oMunicipios As New VillarrealBusiness.clsMunicipios
        With oGrid
            .Title = "Municipios"
            .UpdateTitle = "Municipio"
            .DataSource = AddressOf oMunicipios.Listado
            .UpdateForm = New Comunes.frmMunicipios
            .Format = AddressOf Comunes.clsFormato.for_municipios_grs
            .MdiParent = Me
            .Show()
        End With
        oMunicipios = Nothing
    End Sub

    Public Sub mnuCatEmpleados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEmpleados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEmpleados As New VillarrealBusiness.clsEmpleados
        With oGrid
            .Title = "Empleados"
            .UpdateTitle = "un Empleado"
            .DataSource = AddressOf oEmpleados.Listado
            .UpdateForm = New Comunes.frmEmpleados
            .Format = AddressOf Comunes.clsFormato.for_empleados_grs
            .MdiParent = Me
            .Show()
        End With
        oEmpleados = Nothing
    End Sub

    Public Sub mnuCatGarantias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatGarantias.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oGarantias As New VillarrealBusiness.clsGarantias
        With oGrid
            .Title = "Garantias"
            .UpdateTitle = "una Garantia"
            .DataSource = AddressOf oGarantias.Listado
            .UpdateForm = New frmGarantias
            .Format = AddressOf for_garantias_grs
            .MdiParent = Me
            .Show()
        End With
        oGarantias = Nothing
    End Sub

#End Region

#Region " Procesos "

    Public Sub mnuProOrdenesServicio_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProOrdenesServicio.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oOrdenesServicio As New VillarrealBusiness.clsOrdenesServicio
        With oGrid
            .Title = "Ordenes de Servicio"
            .UpdateTitle = "una Orden de Servicio"
            .DataSource = AddressOf oOrdenesServicio.Listado
            .UpdateForm = New frmOrdenesServicio
            .Format = AddressOf for_ordenes_servicio_grs
            .MdiParent = Me
            .Show()
        End With
        oOrdenesServicio = Nothing
    End Sub

#End Region

#Region " Reportes "

    Public Sub mnuRepOrdenesServicio_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepOrdenesServicio.ItemClick
        Dim oform As New frmReportesOrdenesServicio
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Ordenes de Servicio"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub
    Public Sub mnuRepArticulosConMasFallas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosConMasFallas.ItemClick
        Dim oform As New frmArticulosConMasFallas
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Art�culos Con M�s Fallas"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub
    Public Sub mnuRepArticulosConCambioFisico_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosConCambioFisico.ItemClick
        Dim oform As New frmArticulosCambioFisico
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Art�culos Con Cambio F�sico"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub
    Public Sub mnuRepOrdenesServicioSinSolucion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepOrdenesServicioSinSolucion.ItemClick
        Dim oform As New frmRepOrdenesServicioSinSolucion
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Ordenes de Servicio Sin Soluci�n"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub
#End Region

#Region " Herramientas "
    Public Sub mnuUtiVariables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUtiVariables.ItemClick
        Dim oForm As New Comunes.frmVariables
        oForm.MdiParent = Me
        oForm.Show()
    End Sub
#End Region


End Class
