Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmOrdenesServicio
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys

    Private serie_seleccionada As String = ""
    Private bandera As Boolean = False
    Private garantia_valida As Boolean
    Private folio_movimiento_salida As Long = 0
    Private folio_movimiento_entrada As Long = 0
    Private usuario_salida_reparacion As String = ""
    Private usuario_entrada_reparacion As String = ""
    Private Ultimo_Costo As Double = 0.0

#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblOrden_Servicio As System.Windows.Forms.Label
		Friend WithEvents clcOrden_Servicio As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblTipo_Servicio As System.Windows.Forms.Label
		Friend WithEvents cboTipo_Servicio As DevExpress.XtraEditors.ImageComboBoxEdit
		Friend WithEvents lblFecha As System.Windows.Forms.Label
		Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblArticulo As System.Windows.Forms.Label

		Friend WithEvents lblNumero_Serie As System.Windows.Forms.Label
		Friend WithEvents txtNumero_Serie As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblProveedor As System.Windows.Forms.Label

		Friend WithEvents lblGarantia As System.Windows.Forms.Label
    Friend WithEvents lblRecibe As System.Windows.Forms.Label
		Friend WithEvents lkpRecibe As Dipros.Editors.TINMultiLookup
		Friend WithEvents lblCentro_Servicio As System.Windows.Forms.Label
		Friend WithEvents lkpCentro_Servicio As Dipros.Editors.TINMultiLookup
		Friend WithEvents lblFalla As System.Windows.Forms.Label
		Friend WithEvents txtFalla As DevExpress.XtraEditors.MemoEdit
		Friend WithEvents lblEnviado_Con As System.Windows.Forms.Label
		Friend WithEvents txtEnviado_Con As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpCliente As System.Windows.Forms.GroupBox
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
    Friend WithEvents lkpFactura As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFechaFactura As System.Windows.Forms.Label
    Friend WithEvents dteFechaFactura As DevExpress.XtraEditors.DateEdit
    Friend WithEvents chkCliente As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grOrdenesServicio As DevExpress.XtraGrid.GridControl
    Friend WithEvents tmaOrdenesServicio As Dipros.Windows.TINMaster
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPersona As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCapturo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaPromesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkCambioAutorizado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSolucion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents tbReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents grSeries As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvSeries As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionCorta As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcGarantia As Dipros.Editors.TINCalcEdit
    Friend WithEvents dteFechaPromesa As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtVendedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelefonoCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grvDetalles As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkJuridico As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkMercancia_taller As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbImpresionSeguimientos As System.Windows.Forms.ToolBarButton
    Friend WithEvents tblEtiqueta As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblUsuarioFactura As System.Windows.Forms.Label

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesServicio))
        Me.lblOrden_Servicio = New System.Windows.Forms.Label
        Me.clcOrden_Servicio = New Dipros.Editors.TINCalcEdit
        Me.lblTipo_Servicio = New System.Windows.Forms.Label
        Me.cboTipo_Servicio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblNumero_Serie = New System.Windows.Forms.Label
        Me.txtNumero_Serie = New DevExpress.XtraEditors.TextEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lblGarantia = New System.Windows.Forms.Label
        Me.lblRecibe = New System.Windows.Forms.Label
        Me.lkpRecibe = New Dipros.Editors.TINMultiLookup
        Me.lblCentro_Servicio = New System.Windows.Forms.Label
        Me.lkpCentro_Servicio = New Dipros.Editors.TINMultiLookup
        Me.lblFalla = New System.Windows.Forms.Label
        Me.txtFalla = New DevExpress.XtraEditors.MemoEdit
        Me.lblEnviado_Con = New System.Windows.Forms.Label
        Me.txtEnviado_Con = New DevExpress.XtraEditors.TextEdit
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.gpCliente = New System.Windows.Forms.GroupBox
        Me.txtTelefonoCliente = New DevExpress.XtraEditors.TextEdit
        Me.txtVendedor = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblFechaFactura = New System.Windows.Forms.Label
        Me.dteFechaFactura = New DevExpress.XtraEditors.DateEdit
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.lkpFactura = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.chkCliente = New DevExpress.XtraEditors.CheckEdit
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.grOrdenesServicio = New DevExpress.XtraGrid.GridControl
        Me.grvDetalles = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPersona = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCapturo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaPromesa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaOrdenesServicio = New Dipros.Windows.TINMaster
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkCambioAutorizado = New DevExpress.XtraEditors.CheckEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtSolucion = New DevExpress.XtraEditors.MemoEdit
        Me.tbReimprimir = New System.Windows.Forms.ToolBarButton
        Me.grSeries = New DevExpress.XtraGrid.GridControl
        Me.grvSeries = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblDescripcionCorta = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFechaPromesa = New DevExpress.XtraEditors.DateEdit
        Me.clcGarantia = New Dipros.Editors.TINCalcEdit
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkJuridico = New DevExpress.XtraEditors.CheckEdit
        Me.chkMercancia_taller = New DevExpress.XtraEditors.CheckEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.tbImpresionSeguimientos = New System.Windows.Forms.ToolBarButton
        Me.tblEtiqueta = New System.Windows.Forms.ToolBarButton
        Me.lblUsuarioFactura = New System.Windows.Forms.Label
        CType(Me.clcOrden_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumero_Serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFalla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEnviado_Con.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpCliente.SuspendLayout()
        CType(Me.txtTelefonoCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grOrdenesServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCambioAutorizado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaPromesa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkJuridico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMercancia_taller.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbReimprimir, Me.tbImpresionSeguimientos, Me.tblEtiqueta})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(9075, 28)
        '
        'lblOrden_Servicio
        '
        Me.lblOrden_Servicio.AutoSize = True
        Me.lblOrden_Servicio.Location = New System.Drawing.Point(248, 40)
        Me.lblOrden_Servicio.Name = "lblOrden_Servicio"
        Me.lblOrden_Servicio.Size = New System.Drawing.Size(106, 16)
        Me.lblOrden_Servicio.TabIndex = 2
        Me.lblOrden_Servicio.Tag = ""
        Me.lblOrden_Servicio.Text = "&Orden de servicio:"
        Me.lblOrden_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOrden_Servicio
        '
        Me.clcOrden_Servicio.EditValue = "0"
        Me.clcOrden_Servicio.Location = New System.Drawing.Point(360, 39)
        Me.clcOrden_Servicio.MaxValue = 0
        Me.clcOrden_Servicio.MinValue = 0
        Me.clcOrden_Servicio.Name = "clcOrden_Servicio"
        '
        'clcOrden_Servicio.Properties
        '
        Me.clcOrden_Servicio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcOrden_Servicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden_Servicio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcOrden_Servicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden_Servicio.Properties.Enabled = False
        Me.clcOrden_Servicio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOrden_Servicio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcOrden_Servicio.Size = New System.Drawing.Size(56, 19)
        Me.clcOrden_Servicio.TabIndex = 3
        Me.clcOrden_Servicio.Tag = "orden_servicio"
        '
        'lblTipo_Servicio
        '
        Me.lblTipo_Servicio.AutoSize = True
        Me.lblTipo_Servicio.Location = New System.Drawing.Point(20, 40)
        Me.lblTipo_Servicio.Name = "lblTipo_Servicio"
        Me.lblTipo_Servicio.Size = New System.Drawing.Size(95, 16)
        Me.lblTipo_Servicio.TabIndex = 0
        Me.lblTipo_Servicio.Tag = ""
        Me.lblTipo_Servicio.Text = "&Tipo de servicio:"
        Me.lblTipo_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Servicio
        '
        Me.cboTipo_Servicio.EditValue = "G"
        Me.cboTipo_Servicio.Location = New System.Drawing.Point(120, 38)
        Me.cboTipo_Servicio.Name = "cboTipo_Servicio"
        '
        'cboTipo_Servicio.Properties
        '
        Me.cboTipo_Servicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Servicio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Garantía", "G", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Armado", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Instalación", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Reparación", "R", -1)})
        Me.cboTipo_Servicio.Size = New System.Drawing.Size(120, 23)
        Me.cboTipo_Servicio.TabIndex = 1
        Me.cboTipo_Servicio.Tag = "tipo_servicio"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(440, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "31/05/2006"
        Me.dteFecha.Location = New System.Drawing.Point(488, 38)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = "fecha"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(64, 262)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 13
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Artículo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumero_Serie
        '
        Me.lblNumero_Serie.AutoSize = True
        Me.lblNumero_Serie.Location = New System.Drawing.Point(368, 262)
        Me.lblNumero_Serie.Name = "lblNumero_Serie"
        Me.lblNumero_Serie.Size = New System.Drawing.Size(60, 16)
        Me.lblNumero_Serie.TabIndex = 15
        Me.lblNumero_Serie.Tag = ""
        Me.lblNumero_Serie.Text = "&No. Serie:"
        Me.lblNumero_Serie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumero_Serie
        '
        Me.txtNumero_Serie.EditValue = ""
        Me.txtNumero_Serie.Location = New System.Drawing.Point(432, 262)
        Me.txtNumero_Serie.Name = "txtNumero_Serie"
        '
        'txtNumero_Serie.Properties
        '
        Me.txtNumero_Serie.Properties.Enabled = False
        Me.txtNumero_Serie.Properties.MaxLength = 30
        Me.txtNumero_Serie.Size = New System.Drawing.Size(152, 20)
        Me.txtNumero_Serie.TabIndex = 16
        Me.txtNumero_Serie.Tag = "numero_serie"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(50, 312)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 20
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "&Proveedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblGarantia
        '
        Me.lblGarantia.AutoSize = True
        Me.lblGarantia.Location = New System.Drawing.Point(368, 238)
        Me.lblGarantia.Name = "lblGarantia"
        Me.lblGarantia.Size = New System.Drawing.Size(56, 16)
        Me.lblGarantia.TabIndex = 22
        Me.lblGarantia.Tag = ""
        Me.lblGarantia.Text = "&Garantía:"
        Me.lblGarantia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRecibe
        '
        Me.lblRecibe.AutoSize = True
        Me.lblRecibe.Location = New System.Drawing.Point(67, 337)
        Me.lblRecibe.Name = "lblRecibe"
        Me.lblRecibe.Size = New System.Drawing.Size(48, 16)
        Me.lblRecibe.TabIndex = 25
        Me.lblRecibe.Tag = ""
        Me.lblRecibe.Text = "&Realiza:"
        Me.lblRecibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpRecibe
        '
        Me.lkpRecibe.AllowAdd = False
        Me.lkpRecibe.AutoReaload = False
        Me.lkpRecibe.DataSource = Nothing
        Me.lkpRecibe.DefaultSearchField = ""
        Me.lkpRecibe.DisplayMember = "nombre"
        Me.lkpRecibe.EditValue = Nothing
        Me.lkpRecibe.Filtered = False
        Me.lkpRecibe.InitValue = Nothing
        Me.lkpRecibe.Location = New System.Drawing.Point(120, 337)
        Me.lkpRecibe.MultiSelect = False
        Me.lkpRecibe.Name = "lkpRecibe"
        Me.lkpRecibe.NullText = ""
        Me.lkpRecibe.PopupWidth = CType(400, Long)
        Me.lkpRecibe.ReadOnlyControl = False
        Me.lkpRecibe.Required = False
        Me.lkpRecibe.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpRecibe.SearchMember = ""
        Me.lkpRecibe.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpRecibe.SelectAll = False
        Me.lkpRecibe.Size = New System.Drawing.Size(240, 20)
        Me.lkpRecibe.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpRecibe.TabIndex = 26
        Me.lkpRecibe.Tag = "Recibe"
        Me.lkpRecibe.ToolTip = Nothing
        Me.lkpRecibe.ValueMember = "empleado"
        '
        'lblCentro_Servicio
        '
        Me.lblCentro_Servicio.AutoSize = True
        Me.lblCentro_Servicio.Location = New System.Drawing.Point(6, 362)
        Me.lblCentro_Servicio.Name = "lblCentro_Servicio"
        Me.lblCentro_Servicio.Size = New System.Drawing.Size(109, 16)
        Me.lblCentro_Servicio.TabIndex = 27
        Me.lblCentro_Servicio.Tag = ""
        Me.lblCentro_Servicio.Text = "&Centro de servicio:"
        Me.lblCentro_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCentro_Servicio
        '
        Me.lkpCentro_Servicio.AllowAdd = False
        Me.lkpCentro_Servicio.AutoReaload = False
        Me.lkpCentro_Servicio.DataSource = Nothing
        Me.lkpCentro_Servicio.DefaultSearchField = ""
        Me.lkpCentro_Servicio.DisplayMember = "descripcion"
        Me.lkpCentro_Servicio.EditValue = Nothing
        Me.lkpCentro_Servicio.Filtered = False
        Me.lkpCentro_Servicio.InitValue = Nothing
        Me.lkpCentro_Servicio.Location = New System.Drawing.Point(120, 362)
        Me.lkpCentro_Servicio.MultiSelect = False
        Me.lkpCentro_Servicio.Name = "lkpCentro_Servicio"
        Me.lkpCentro_Servicio.NullText = ""
        Me.lkpCentro_Servicio.PopupWidth = CType(400, Long)
        Me.lkpCentro_Servicio.ReadOnlyControl = False
        Me.lkpCentro_Servicio.Required = False
        Me.lkpCentro_Servicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCentro_Servicio.SearchMember = ""
        Me.lkpCentro_Servicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCentro_Servicio.SelectAll = False
        Me.lkpCentro_Servicio.Size = New System.Drawing.Size(240, 20)
        Me.lkpCentro_Servicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCentro_Servicio.TabIndex = 28
        Me.lkpCentro_Servicio.Tag = "centro_servicio"
        Me.lkpCentro_Servicio.ToolTip = Nothing
        Me.lkpCentro_Servicio.ValueMember = "centro"
        '
        'lblFalla
        '
        Me.lblFalla.AutoSize = True
        Me.lblFalla.Location = New System.Drawing.Point(80, 392)
        Me.lblFalla.Name = "lblFalla"
        Me.lblFalla.Size = New System.Drawing.Size(35, 16)
        Me.lblFalla.TabIndex = 29
        Me.lblFalla.Tag = ""
        Me.lblFalla.Text = "&Falla:"
        Me.lblFalla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFalla
        '
        Me.txtFalla.EditValue = ""
        Me.txtFalla.Location = New System.Drawing.Point(120, 390)
        Me.txtFalla.Name = "txtFalla"
        Me.txtFalla.Size = New System.Drawing.Size(240, 32)
        Me.txtFalla.TabIndex = 30
        Me.txtFalla.Tag = "falla"
        '
        'lblEnviado_Con
        '
        Me.lblEnviado_Con.AutoSize = True
        Me.lblEnviado_Con.Location = New System.Drawing.Point(39, 428)
        Me.lblEnviado_Con.Name = "lblEnviado_Con"
        Me.lblEnviado_Con.Size = New System.Drawing.Size(76, 16)
        Me.lblEnviado_Con.TabIndex = 31
        Me.lblEnviado_Con.Tag = ""
        Me.lblEnviado_Con.Text = "&Enviado con:"
        Me.lblEnviado_Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEnviado_Con
        '
        Me.txtEnviado_Con.EditValue = ""
        Me.txtEnviado_Con.Location = New System.Drawing.Point(120, 428)
        Me.txtEnviado_Con.Name = "txtEnviado_Con"
        '
        'txtEnviado_Con.Properties
        '
        Me.txtEnviado_Con.Properties.MaxLength = 50
        Me.txtEnviado_Con.Size = New System.Drawing.Size(240, 20)
        Me.txtEnviado_Con.TabIndex = 32
        Me.txtEnviado_Con.Tag = "enviado_con"
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(120, 262)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(240, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 14
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo_partida"
        '
        'gpCliente
        '
        Me.gpCliente.Controls.Add(Me.lblUsuarioFactura)
        Me.gpCliente.Controls.Add(Me.txtTelefonoCliente)
        Me.gpCliente.Controls.Add(Me.txtVendedor)
        Me.gpCliente.Controls.Add(Me.Label8)
        Me.gpCliente.Controls.Add(Me.Label7)
        Me.gpCliente.Controls.Add(Me.lblFechaFactura)
        Me.gpCliente.Controls.Add(Me.dteFechaFactura)
        Me.gpCliente.Controls.Add(Me.lblFolio_Factura)
        Me.gpCliente.Controls.Add(Me.lkpFactura)
        Me.gpCliente.Controls.Add(Me.lblCliente)
        Me.gpCliente.Controls.Add(Me.lkpCliente)
        Me.gpCliente.Controls.Add(Me.chkCliente)
        Me.gpCliente.Controls.Add(Me.lblDireccion)
        Me.gpCliente.Controls.Add(Me.txtDireccion)
        Me.gpCliente.Location = New System.Drawing.Point(16, 88)
        Me.gpCliente.Name = "gpCliente"
        Me.gpCliente.Size = New System.Drawing.Size(568, 144)
        Me.gpCliente.TabIndex = 10
        Me.gpCliente.TabStop = False
        '
        'txtTelefonoCliente
        '
        Me.txtTelefonoCliente.EditValue = ""
        Me.txtTelefonoCliente.Location = New System.Drawing.Point(104, 62)
        Me.txtTelefonoCliente.Name = "txtTelefonoCliente"
        '
        'txtTelefonoCliente.Properties
        '
        Me.txtTelefonoCliente.Properties.MaxLength = 50
        Me.txtTelefonoCliente.Size = New System.Drawing.Size(216, 20)
        Me.txtTelefonoCliente.TabIndex = 6
        Me.txtTelefonoCliente.Tag = "telefono_cliente"
        '
        'txtVendedor
        '
        Me.txtVendedor.EditValue = ""
        Me.txtVendedor.Location = New System.Drawing.Point(104, 112)
        Me.txtVendedor.Name = "txtVendedor"
        '
        'txtVendedor.Properties
        '
        Me.txtVendedor.Properties.Enabled = False
        Me.txtVendedor.Properties.MaxLength = 50
        Me.txtVendedor.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtVendedor.Size = New System.Drawing.Size(448, 20)
        Me.txtVendedor.TabIndex = 12
        Me.txtVendedor.Tag = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(40, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 16)
        Me.Label8.TabIndex = 11
        Me.Label8.Tag = ""
        Me.Label8.Text = "Vendedor:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(42, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 5
        Me.Label7.Tag = ""
        Me.Label7.Text = "Teléfono:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFechaFactura
        '
        Me.lblFechaFactura.AutoSize = True
        Me.lblFechaFactura.Location = New System.Drawing.Point(344, 88)
        Me.lblFechaFactura.Name = "lblFechaFactura"
        Me.lblFechaFactura.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaFactura.TabIndex = 9
        Me.lblFechaFactura.Tag = ""
        Me.lblFechaFactura.Text = "&Fecha de Factura:"
        Me.lblFechaFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaFactura
        '
        Me.dteFechaFactura.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaFactura.Location = New System.Drawing.Point(456, 88)
        Me.dteFechaFactura.Name = "dteFechaFactura"
        '
        'dteFechaFactura.Properties
        '
        Me.dteFechaFactura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFactura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.Enabled = False
        Me.dteFechaFactura.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaFactura.TabIndex = 10
        Me.dteFechaFactura.Tag = "fecha_factura"
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(48, 88)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(50, 16)
        Me.lblFolio_Factura.TabIndex = 7
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Factura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFactura
        '
        Me.lkpFactura.AllowAdd = False
        Me.lkpFactura.AutoReaload = False
        Me.lkpFactura.DataSource = Nothing
        Me.lkpFactura.DefaultSearchField = ""
        Me.lkpFactura.DisplayMember = "clave_compuesta"
        Me.lkpFactura.EditValue = Nothing
        Me.lkpFactura.Filtered = False
        Me.lkpFactura.InitValue = Nothing
        Me.lkpFactura.Location = New System.Drawing.Point(104, 88)
        Me.lkpFactura.MultiSelect = False
        Me.lkpFactura.Name = "lkpFactura"
        Me.lkpFactura.NullText = ""
        Me.lkpFactura.PopupWidth = CType(500, Long)
        Me.lkpFactura.ReadOnlyControl = False
        Me.lkpFactura.Required = False
        Me.lkpFactura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFactura.SearchMember = ""
        Me.lkpFactura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFactura.SelectAll = False
        Me.lkpFactura.Size = New System.Drawing.Size(216, 20)
        Me.lkpFactura.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFactura.TabIndex = 8
        Me.lkpFactura.Tag = ""
        Me.lkpFactura.ToolTip = Nothing
        Me.lkpFactura.ValueMember = "clave_compuesta"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(51, 16)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 1
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cl&iente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 14)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(448, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 2
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'chkCliente
        '
        Me.chkCliente.EditValue = True
        Me.chkCliente.Location = New System.Drawing.Point(16, -3)
        Me.chkCliente.Name = "chkCliente"
        '
        'chkCliente.Properties
        '
        Me.chkCliente.Properties.Caption = "Cliente"
        Me.chkCliente.Properties.Enabled = False
        Me.chkCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkCliente.Size = New System.Drawing.Size(75, 19)
        Me.chkCliente.TabIndex = 0
        Me.chkCliente.Tag = "orden_cliente"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(38, 40)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 3
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "Dirección:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(104, 38)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.MaxLength = 100
        Me.txtDireccion.Size = New System.Drawing.Size(448, 20)
        Me.txtDireccion.TabIndex = 4
        Me.txtDireccion.Tag = "direccion_cliente"
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(120, 312)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(450, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(240, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 21
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'grOrdenesServicio
        '
        '
        'grOrdenesServicio.EmbeddedNavigator
        '
        Me.grOrdenesServicio.EmbeddedNavigator.Name = ""
        Me.grOrdenesServicio.Location = New System.Drawing.Point(8, 520)
        Me.grOrdenesServicio.MainView = Me.grvDetalles
        Me.grOrdenesServicio.Name = "grOrdenesServicio"
        Me.grOrdenesServicio.Size = New System.Drawing.Size(576, 88)
        Me.grOrdenesServicio.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grOrdenesServicio.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesServicio.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesServicio.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesServicio.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesServicio.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesServicio.TabIndex = 40
        Me.grOrdenesServicio.TabStop = False
        Me.grOrdenesServicio.Text = "Detalle"
        '
        'grvDetalles
        '
        Me.grvDetalles.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcFecha, Me.grcPersona, Me.grcCapturo, Me.grcObservaciones, Me.grcFechaPromesa})
        Me.grvDetalles.GridControl = Me.grOrdenesServicio
        Me.grvDetalles.Name = "grvDetalles"
        Me.grvDetalles.OptionsBehavior.Editable = False
        Me.grvDetalles.OptionsCustomization.AllowFilter = False
        Me.grvDetalles.OptionsCustomization.AllowGroup = False
        Me.grvDetalles.OptionsCustomization.AllowSort = False
        Me.grvDetalles.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.VisibleIndex = 0
        Me.grcFecha.Width = 82
        '
        'grcPersona
        '
        Me.grcPersona.Caption = "Persona"
        Me.grcPersona.FieldName = "persona"
        Me.grcPersona.Name = "grcPersona"
        Me.grcPersona.VisibleIndex = 1
        Me.grcPersona.Width = 118
        '
        'grcCapturo
        '
        Me.grcCapturo.Caption = "Capturó"
        Me.grcCapturo.FieldName = "capturo"
        Me.grcCapturo.Name = "grcCapturo"
        Me.grcCapturo.VisibleIndex = 3
        Me.grcCapturo.Width = 118
        '
        'grcObservaciones
        '
        Me.grcObservaciones.Caption = "Observaciones"
        Me.grcObservaciones.FieldName = "observaciones"
        Me.grcObservaciones.Name = "grcObservaciones"
        Me.grcObservaciones.VisibleIndex = 2
        Me.grcObservaciones.Width = 130
        '
        'grcFechaPromesa
        '
        Me.grcFechaPromesa.Caption = "Fecha Promesa"
        Me.grcFechaPromesa.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPromesa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPromesa.FieldName = "fecha_promesa"
        Me.grcFechaPromesa.Name = "grcFechaPromesa"
        Me.grcFechaPromesa.VisibleIndex = 4
        Me.grcFechaPromesa.Width = 114
        '
        'tmaOrdenesServicio
        '
        Me.tmaOrdenesServicio.BackColor = System.Drawing.Color.White
        Me.tmaOrdenesServicio.CanDelete = True
        Me.tmaOrdenesServicio.CanInsert = True
        Me.tmaOrdenesServicio.CanUpdate = True
        Me.tmaOrdenesServicio.Grid = Me.grOrdenesServicio
        Me.tmaOrdenesServicio.Location = New System.Drawing.Point(8, 496)
        Me.tmaOrdenesServicio.Name = "tmaOrdenesServicio"
        Me.tmaOrdenesServicio.Size = New System.Drawing.Size(576, 23)
        Me.tmaOrdenesServicio.TabIndex = 39
        Me.tmaOrdenesServicio.TabStop = False
        Me.tmaOrdenesServicio.Title = "Detalle"
        Me.tmaOrdenesServicio.UpdateTitle = "un Registro"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(376, 368)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 34
        Me.Label2.Tag = ""
        Me.Label2.Text = "Estatus:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "En Proceso"
        Me.cboEstatus.Location = New System.Drawing.Point(432, 366)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Proceso", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Terminada", "T", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(150, 23)
        Me.cboEstatus.TabIndex = 35
        Me.cboEstatus.Tag = "estatus"
        '
        'chkCambioAutorizado
        '
        Me.chkCambioAutorizado.Location = New System.Drawing.Point(448, 428)
        Me.chkCambioAutorizado.Name = "chkCambioAutorizado"
        '
        'chkCambioAutorizado.Properties
        '
        Me.chkCambioAutorizado.Properties.Caption = "Cambio Autorizado"
        Me.chkCambioAutorizado.Properties.Enabled = False
        Me.chkCambioAutorizado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkCambioAutorizado.Size = New System.Drawing.Size(128, 19)
        Me.chkCambioAutorizado.TabIndex = 36
        Me.chkCambioAutorizado.Tag = "cambio_autorizado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(59, 456)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 37
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Solución:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSolucion
        '
        Me.txtSolucion.EditValue = ""
        Me.txtSolucion.Location = New System.Drawing.Point(120, 456)
        Me.txtSolucion.Name = "txtSolucion"
        '
        'txtSolucion.Properties
        '
        Me.txtSolucion.Properties.Enabled = False
        Me.txtSolucion.Properties.MaxLength = 1000
        Me.txtSolucion.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtSolucion.Size = New System.Drawing.Size(312, 32)
        Me.txtSolucion.TabIndex = 38
        Me.txtSolucion.Tag = "solucion"
        '
        'tbReimprimir
        '
        Me.tbReimprimir.Text = "Reimprimir"
        '
        'grSeries
        '
        '
        'grSeries.EmbeddedNavigator
        '
        Me.grSeries.EmbeddedNavigator.Name = ""
        Me.grSeries.Location = New System.Drawing.Point(368, 312)
        Me.grSeries.MainView = Me.grvSeries
        Me.grSeries.Name = "grSeries"
        Me.grSeries.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grSeries.Size = New System.Drawing.Size(216, 48)
        Me.grSeries.TabIndex = 19
        Me.grSeries.Text = "GridControl1"
        Me.grSeries.Visible = False
        '
        'grvSeries
        '
        Me.grvSeries.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcSerie})
        Me.grvSeries.GridControl = Me.grSeries
        Me.grvSeries.Name = "grvSeries"
        Me.grvSeries.OptionsCustomization.AllowGroup = False
        Me.grvSeries.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvSeries.OptionsView.ShowGroupPanel = False
        Me.grvSeries.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 39
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 1
        Me.grcSerie.Width = 175
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(43, 288)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 17
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Descripción:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcionCorta
        '
        Me.lblDescripcionCorta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDescripcionCorta.Enabled = False
        Me.lblDescripcionCorta.Location = New System.Drawing.Point(120, 288)
        Me.lblDescripcionCorta.Name = "lblDescripcionCorta"
        Me.lblDescripcionCorta.Size = New System.Drawing.Size(464, 20)
        Me.lblDescripcionCorta.TabIndex = 18
        Me.lblDescripcionCorta.Tag = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(59, 64)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 6
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(120, 62)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(240, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 7
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(388, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 16)
        Me.Label5.TabIndex = 8
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Fecha Promesa:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaPromesa
        '
        Me.dteFechaPromesa.EditValue = "31/05/2006"
        Me.dteFechaPromesa.Location = New System.Drawing.Point(488, 62)
        Me.dteFechaPromesa.Name = "dteFechaPromesa"
        '
        'dteFechaPromesa.Properties
        '
        Me.dteFechaPromesa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaPromesa.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaPromesa.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaPromesa.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaPromesa.TabIndex = 9
        Me.dteFechaPromesa.Tag = "fecha_promesa"
        '
        'clcGarantia
        '
        Me.clcGarantia.EditValue = "0"
        Me.clcGarantia.Location = New System.Drawing.Point(432, 238)
        Me.clcGarantia.MaxValue = 0
        Me.clcGarantia.MinValue = 0
        Me.clcGarantia.Name = "clcGarantia"
        '
        'clcGarantia.Properties
        '
        Me.clcGarantia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGarantia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGarantia.Properties.Enabled = False
        Me.clcGarantia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcGarantia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcGarantia.Size = New System.Drawing.Size(56, 19)
        Me.clcGarantia.TabIndex = 23
        Me.clcGarantia.Tag = "garantia"
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Enabled = False
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(120, 238)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = True
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(240, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 12
        Me.lkpBodega.Tag = "bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(65, 238)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 11
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(496, 238)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Tag = ""
        Me.Label6.Text = "Meses"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkJuridico
        '
        Me.chkJuridico.Location = New System.Drawing.Point(368, 428)
        Me.chkJuridico.Name = "chkJuridico"
        '
        'chkJuridico.Properties
        '
        Me.chkJuridico.Properties.Caption = "Jurídico"
        Me.chkJuridico.Properties.Enabled = False
        Me.chkJuridico.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlText)
        Me.chkJuridico.Size = New System.Drawing.Size(75, 19)
        Me.chkJuridico.TabIndex = 33
        Me.chkJuridico.Tag = "juridico"
        '
        'chkMercancia_taller
        '
        Me.chkMercancia_taller.Location = New System.Drawing.Point(448, 456)
        Me.chkMercancia_taller.Name = "chkMercancia_taller"
        '
        'chkMercancia_taller.Properties
        '
        Me.chkMercancia_taller.Properties.Caption = "Mercancia  en Taller"
        Me.chkMercancia_taller.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkMercancia_taller.Size = New System.Drawing.Size(128, 19)
        Me.chkMercancia_taller.TabIndex = 39
        Me.chkMercancia_taller.Tag = "mercancia_taller"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(376, 392)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 16)
        Me.Label9.TabIndex = 59
        Me.Label9.Tag = ""
        Me.Label9.Text = "Termino:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Location = New System.Drawing.Point(432, 390)
        Me.TextEdit1.Name = "TextEdit1"
        '
        'TextEdit1.Properties
        '
        Me.TextEdit1.Properties.Enabled = False
        Me.TextEdit1.Properties.MaxLength = 30
        Me.TextEdit1.Size = New System.Drawing.Size(152, 20)
        Me.TextEdit1.TabIndex = 60
        Me.TextEdit1.Tag = "usuario_termino"
        '
        'tbImpresionSeguimientos
        '
        Me.tbImpresionSeguimientos.Text = "Impresion Seguimientos"
        '
        'tblEtiqueta
        '
        Me.tblEtiqueta.Text = "Etiqueta"
        Me.tblEtiqueta.ToolTipText = "Imprime la Etiqueta de la Orden de Servicio"
        '
        'lblUsuarioFactura
        '
        Me.lblUsuarioFactura.AutoSize = True
        Me.lblUsuarioFactura.Location = New System.Drawing.Point(344, 66)
        Me.lblUsuarioFactura.Name = "lblUsuarioFactura"
        Me.lblUsuarioFactura.Size = New System.Drawing.Size(91, 16)
        Me.lblUsuarioFactura.TabIndex = 13
        Me.lblUsuarioFactura.Tag = ""
        Me.lblUsuarioFactura.Text = "Usuario Factura"
        Me.lblUsuarioFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmOrdenesServicio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(594, 616)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.chkMercancia_taller)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.chkJuridico)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.clcGarantia)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.grSeries)
        Me.Controls.Add(Me.lblDescripcionCorta)
        Me.Controls.Add(Me.chkCambioAutorizado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grOrdenesServicio)
        Me.Controls.Add(Me.cboEstatus)
        Me.Controls.Add(Me.tmaOrdenesServicio)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.gpCliente)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblOrden_Servicio)
        Me.Controls.Add(Me.clcOrden_Servicio)
        Me.Controls.Add(Me.lblTipo_Servicio)
        Me.Controls.Add(Me.cboTipo_Servicio)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblNumero_Serie)
        Me.Controls.Add(Me.txtNumero_Serie)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lblGarantia)
        Me.Controls.Add(Me.lblRecibe)
        Me.Controls.Add(Me.lkpRecibe)
        Me.Controls.Add(Me.lblCentro_Servicio)
        Me.Controls.Add(Me.lkpCentro_Servicio)
        Me.Controls.Add(Me.lblFalla)
        Me.Controls.Add(Me.txtFalla)
        Me.Controls.Add(Me.lblEnviado_Con)
        Me.Controls.Add(Me.txtEnviado_Con)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSolucion)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteFechaPromesa)
        Me.Controls.Add(Me.Label6)
        Me.Name = "frmOrdenesServicio"
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.dteFechaPromesa, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.txtSolucion, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.txtEnviado_Con, 0)
        Me.Controls.SetChildIndex(Me.lblEnviado_Con, 0)
        Me.Controls.SetChildIndex(Me.txtFalla, 0)
        Me.Controls.SetChildIndex(Me.lblFalla, 0)
        Me.Controls.SetChildIndex(Me.lkpCentro_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblCentro_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lkpRecibe, 0)
        Me.Controls.SetChildIndex(Me.lblRecibe, 0)
        Me.Controls.SetChildIndex(Me.lblGarantia, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.txtNumero_Serie, 0)
        Me.Controls.SetChildIndex(Me.lblNumero_Serie, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.cboTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Servicio, 0)
        Me.Controls.SetChildIndex(Me.clcOrden_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblOrden_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.gpCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.tmaOrdenesServicio, 0)
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        Me.Controls.SetChildIndex(Me.grOrdenesServicio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.chkCambioAutorizado, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionCorta, 0)
        Me.Controls.SetChildIndex(Me.grSeries, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.clcGarantia, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.chkJuridico, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkMercancia_taller, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.TextEdit1, 0)
        CType(Me.clcOrden_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumero_Serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFalla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEnviado_Con.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpCliente.ResumeLayout(False)
        CType(Me.txtTelefonoCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grOrdenesServicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCambioAutorizado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaPromesa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkJuridico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMercancia_taller.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oOrdenesServicio As VillarrealBusiness.clsOrdenesServicio
    Friend oOrdenesServicioDetalle As VillarrealBusiness.clsOrdenesServicioDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oEmpleados As VillarrealBusiness.clsEmpleados
    Private oCentrosServicio As VillarrealBusiness.clsCentrosServicio
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVariables As VillarrealBusiness.clsVariables
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oArticulosExistencias As VillarrealBusiness.clsArticulosExistencias
    Private oMovimientoInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientoInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle

    Private lPartidaFactura As Long
    Private articulo_lkp As Long
    Private Folio_Factura As Long = 0
    Private Serie_Factura As String = ""
    'Private TablaSeguimientos As DataTable

    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property SucursalFactura() As Long
        Get
            If Me.lkpFactura.EditValue = Nothing Then
                Return -1
            Else
                Return CLng(Me.lkpFactura.GetValue("sucursal"))
            End If

        End Get
    End Property
    Private Property SerieFactura() As String
        Get
            Return Serie_Factura
            'If Me.lkpFactura.EditValue = Nothing Then
            '    Return ""
            'Else
            '    Return CStr(Me.lkpFactura.GetValue("serie"))
            'End If
        End Get
        Set(ByVal value As String)
            Serie_Factura = value
        End Set
    End Property
    Private Property FolioFactura() As Long
        Get
            Return Folio_Factura
            'If Me.lkpFactura.EditValue = Nothing Then
            '    Return -1
            'Else
            '    Return CLng(Me.lkpFactura.GetValue("folio"))
            'End If
        End Get
        Set(ByVal value As Long)
            Folio_Factura = value
        End Set
    End Property
    Private Property PartidaFactura() As Long
        Get
            If Me.lkpFactura.EditValue = Nothing Then
                Return -1
            Else
                Return lPartidaFactura
            End If
        End Get
        Set(ByVal Value As Long)
            lPartidaFactura = Value

        End Set
    End Property

    Private ReadOnly Property Articulo() As Long
        Get
            Return articulo_lkp
            'Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property ManejaSeries() As Boolean
        Get
            If Me.lkpArticulo.EditValue = Nothing Then
                Return False
            Else
                Return CType(Me.lkpArticulo.GetValue("maneja_series"), Boolean)
            End If
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property CentroServicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCentro_Servicio)
        End Get
    End Property
    Private ReadOnly Property Recibe() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpRecibe)
        End Get
    End Property

    Private ReadOnly Property DiasArmado() As Long
        Get
            Return CLng(oVariables.TraeDatos("dias_armado", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property
    Private ReadOnly Property DiasInstalacion() As Long
        Get
            Return CLng(oVariables.TraeDatos("dias_instalacion", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    Private ReadOnly Property ConceptoSalidaReparacion() As String
        Get
            Return oVariables.TraeDatos("concepto_salida_reparacion", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property ConceptoEntradaReparacion() As String
        Get
            Return oVariables.TraeDatos("concepto_entrada_reparacion", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmOrdenesServicio_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmOrdenesServicio_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmOrdenesServicio_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        If Me.Action = Actions.Insert Then Me.ImprimeValeOrdenDeServicio()
    End Sub

    Private Sub frmOrdenesServicio_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oOrdenesServicio.Insertar(Me.DataSource, SucursalFactura, SerieFactura, FolioFactura, PartidaFactura, Articulo)
                If Not Response.ErrorFound Then
                    Response = GeneraMovimientosEntradasSalidas()

                    
                End If

            Case Actions.Update
                Response = oOrdenesServicio.Actualizar(Me.DataSource, Articulo)
                If Not Response.ErrorFound Then
                    Response = GeneraMovimientosEntradasSalidas()
                End If

                If Not Response.ErrorFound And (Me.cboEstatus.EditValue = "T" Or Me.cboEstatus.EditValue = "C") Then
                    Response = Me.oOrdenesServicio.ActualizaUsuarioTermino(Me.clcOrden_Servicio.Value)
                End If
            Case Actions.Delete
                Response = oOrdenesServicio.Eliminar(clcOrden_Servicio.Value)

        End Select
    End Sub
    Private Sub frmOrdenesServicio_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With tmaOrdenesServicio

            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oOrdenesServicioDetalle.Insertar(.SelectedRow, Me.clcOrden_Servicio.EditValue)
                    Case Actions.Update
                        Response = oOrdenesServicioDetalle.Actualizar(.SelectedRow, Me.clcOrden_Servicio.EditValue)
                    Case Actions.Delete
                        Response = oOrdenesServicioDetalle.Eliminar(Me.clcOrden_Servicio.Value, .Item("partida"))
                End Select
                .MoveNext()
            Loop
        End With

    End Sub

    Private Sub frmOrdenesServicio_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oOrdenesServicio.DespliegaDatos(OwnerForm.Value("orden_servicio"))

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet



            folio_movimiento_salida = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_salida"))
            folio_movimiento_entrada = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_entrada"))
            usuario_salida_reparacion = CType(oDataSet.Tables(0).Rows(0).Item("usuario_salida_reparacion"), String)
            usuario_entrada_reparacion = CType(oDataSet.Tables(0).Rows(0).Item("usuario_entrada_reparacion"), String)

            If Me.chkCliente.Checked = True Then
                Me.bandera = True
                Me.lkpFactura.EditValue = CStr(oDataSet.Tables(0).Rows(0).Item("clave_compuesta"))


            End If

            Me.lkpArticulo_LoadData(True)            
            Me.lkpArticulo.EditValue = oDataSet.Tables(0).Rows(0).Item("articulo_partida")
            Me.lblDescripcionCorta.Text = Me.lkpArticulo.GetValue("descripcion_corta")



            Me.lkpProveedor.EditValue = CInt(oDataSet.Tables(0).Rows(0).Item("proveedor"))
            oDataSet = Nothing
        End If

        Me.grSeries.Visible = False

        If Not Response.ErrorFound Then Response = oOrdenesServicioDetalle.Listado(Me.clcOrden_Servicio.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            odataset = Response.Value
            Me.tmaOrdenesServicio.DataSource = odataset
            'TablaSeguimientos = odataset.Tables(0)
            oDataSet = Nothing
        End If


        Select Case Me.cboEstatus.EditValue
            Case "P"
                Me.cboTipo_Servicio.Enabled = False
                Me.lkpArticulo.Enabled = False
                Me.chkCliente.Enabled = False
                Me.lkpCliente.Enabled = False
                Me.lkpFactura.Enabled = False
                Me.dteFechaFactura.Enabled = False
                Me.lkpBodega.Enabled = False
                Me.lblDescripcionCorta.Enabled = False
                Me.lkpProveedor.Enabled = True
                Me.lkpRecibe.Enabled = False
                Me.txtTelefonoCliente.Enabled = True
                'Me.lkpCentro_Servicio.Enabled = False

            Case "T", "C"
                Me.EnabledEdit(False)
                Me.tbrTools.Buttons(0).Enabled = False
                Me.tbrTools.Buttons(0).Visible = False
                Me.lkpProveedor.Enabled = False

                'SE HABILITO DEBIDO A PETICION DE LA GENTE DE GARANTIAS EL DIA 22 DE ABRIL 09
                Me.tmaOrdenesServicio.Enabled = True
                Me.grOrdenesServicio.Enabled = True
                Me.txtSolucion.Enabled = True

        End Select

        Select Case Action
            Case Actions.Update
                Me.txtDireccion.Enabled = True
            Case Actions.Delete
                Me.txtDireccion.Enabled = False
        End Select

    End Sub
    Private Sub frmOrdenesServicio_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oOrdenesServicio = New VillarrealBusiness.clsOrdenesServicio
        oOrdenesServicioDetalle = New VillarrealBusiness.clsOrdenesServicioDetalle
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oProveedores = New VillarrealBusiness.clsProveedores
        oArticulos = New VillarrealBusiness.clsArticulos

        oEmpleados = New VillarrealBusiness.clsEmpleados

        oCentrosServicio = New VillarrealBusiness.clsCentrosServicio
        oSucursales = New VillarrealBusiness.clsSucursales
        oVariables = New VillarrealBusiness.clsVariables
        oBodegas = New VillarrealBusiness.clsBodegas
        oArticulosExistencias = New VillarrealBusiness.clsArticulosExistencias
        oMovimientoInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientoInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle

        'Me.chkCliente.Checked = True
        Me.dteFechaFactura.EditValue = Nothing

        With Me.tmaOrdenesServicio
            .UpdateTitle = "un Registro"
            .UpdateForm = New frmOrdenesServicioDetalle
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("persona")
            .AddColumn("capturo")
            .AddColumn("observaciones")
            .AddColumn("fecha_promesa", "System.DateTime")
        End With

        Me.cboEstatus.SelectedIndex = 0

        Select Case Action
            Case Actions.Insert
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(2).Visible = False
                Me.tbrTools.Buttons(3).Enabled = False
                Me.tbrTools.Buttons(3).Visible = False
                Me.tbrTools.Buttons(4).Enabled = False
                Me.tbrTools.Buttons(4).Visible = False

                Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFechaPromesa.EditValue = CDate(TINApp.FechaServidor)
                Me.cboEstatus.Enabled = False

            Case Actions.Update
                Me.lkpSucursal.Enabled = False
                Me.lkpCentro_Servicio.Enabled = True

            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmOrdenesServicio_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oOrdenesServicio.Validacion(Action, dteFecha.Text, Me.dteFechaPromesa.Text, Articulo, Recibe, CentroServicio, txtFalla.Text, Me.ManejaSeries, Me.txtNumero_Serie.Text, Me.cboTipo_Servicio.EditValue)

        If Action = Actions.Insert And Me.cboTipo_Servicio.EditValue = "G" Then
            If Not Response.ErrorFound Then Response = oOrdenesServicio.ValidaGarantia(Me.dteFecha.DateTime, Me.dteFechaFactura.DateTime, Me.clcGarantia.EditValue, garantia_valida)

            If Not Response.ErrorFound And garantia_valida = False Then
                If ShowMessage(MessageType.MsgQuestion, "La garantía del Artículo no es Valida, ¿Desea Continuar?") <> Answer.MsgYes Then
                    Response.Message = "La Garantía no es Válida"
                End If
            End If
        End If


        If Action = Actions.Insert And Me.cboTipo_Servicio.EditValue = "R" Then
            If Not Response.ErrorFound Then Response = oOrdenesServicio.ValidaBodega(Bodega)
            If Not Response.ErrorFound Then Response = oArticulosExistencias.ValidaExistenciaArticuloBodega(Articulo, Bodega, 1)
        End If

        'DAM 03-Jun ...nuevo manejo de ordenes de tipo reparacion
        If Action = Actions.Update And Me.cboTipo_Servicio.EditValue = "R" Then
            If Me.cboEstatus.EditValue = "C" And Len(usuario_salida_reparacion) > 0 Then
                Response.Message = "No se permite cancelar la orden por que ya tiene generada una salida de reparación"
            End If

            ' SI SE MARCA TERMINADA Y HAY UN USUARIO DE SALIDA REPARACION Y NO TENGO UN USUARIO DE ENTRADA 
            ' DE(REPARACION)TENGO QUE TENER EL CHECK MARCADO DE CAMBIO FISICO

            If Me.cboEstatus.EditValue = "T" And (Len(usuario_salida_reparacion) > 0 And Len(usuario_entrada_reparacion) = 0 And chkCambioAutorizado.Checked = False) Then
                Response.Message = "No se permite Terminar la orden por que NO tiene generada una Entrada de reparación o NO tiene un cambio Autorizado"
            End If


        End If


        If CStr(Me.cboEstatus.EditValue).ToUpper <> "P" Then
            If Not Response.ErrorFound Then Response = oOrdenesServicio.ValidaSolucion(Me.txtSolucion.Text)
        End If

        If Action = Actions.Delete Then
            If Not Response.ErrorFound Then Response = oOrdenesServicio.ValidaDetalles(Me.grvDetalles.RowCount)
        End If
    End Sub

    Private Sub frmOrdenesServicio_Localize() Handles MyBase.Localize
        Find("orden_servicio", CType(Me.clcOrden_Servicio.EditValue, Object))

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button.Text = "Reimprimir" Then
            Dim response As Events
            response = Me.oOrdenesServicio.ActualizarImpresion(Me.clcOrden_Servicio.EditValue)
            If Not response.ErrorFound Then
                Me.ImprimeValeOrdenDeServicio()
            Else
                ShowMessage(MessageType.MsgInformation, "Error al actualizar la reimpresión")
            End If

        End If

        If e.Button Is Me.tbImpresionSeguimientos Then
            ImprimeSeguimientosOrdenServicio()
        End If

        If e.Button Is Me.tblEtiqueta Then
            ImprimeEtiqueta()
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "Lookups"


    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        Me.txtDireccion.Text = Me.lkpCliente.GetValue("direccion")
        Me.txtTelefonoCliente.Text = Me.lkpCliente.GetValue("telefono1")

        Me.lkpFactura.EditValue = Nothing
        Me.lkpFactura_LoadData(True)
    End Sub

    Private Sub lkpFactura_Format() Handles lkpFactura.Format
        Comunes.clsFormato.for_ventas_facturas_grl(Me.lkpFactura)
    End Sub
    Private Sub lkpFactura_LoadData(ByVal Initialize As Boolean) Handles lkpFactura.LoadData
        Dim response As Events
        response = oVentas.LookupFacturasPorCliente(Cliente)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFactura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpFactura_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFactura.EditValueChanged
        If IsLoading Then Exit Sub
        Me.dteFechaFactura.EditValue = Me.lkpFactura.GetValue("fecha")
        Me.txtVendedor.Text = Me.lkpFactura.GetValue("nombre_vendedor")
        Me.lkpArticulo.EditValue = Nothing
        SerieFactura = Me.lkpFactura.GetValue("serie")
        FolioFactura = Me.lkpFactura.GetValue("folio")
        Me.lblUsuarioFactura.Text = Me.lkpFactura.GetValue("usuario_factura")

        Me.lkpArticulo_LoadData(True)
    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events

        'La bandera se activa solo cuando es una actualizacion, porque no estaba cargando los articulos de la factura

        'Si el articulo se va seleccionar de la factura de un cliente
        If (Me.chkCliente.Checked = True And bandera = True) Or (Me.chkCliente.Checked = True And Me.FolioFactura > 0) Then
            Response = oArticulos.LookupArticulosPorFactura(SucursalFactura, SerieFactura, FolioFactura)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpArticulo.DataSource = oDataSet.Tables(0)
            End If

        Else
            'Si el articulo se va selccionar de la tienda
            Response = oArticulos.Lookup()
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpArticulo.DataSource = oDataSet.Tables(0)
            End If
            bandera = False
        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Me.lkpArticulo.EditValue Is Nothing Then
            Me.lblDescripcionCorta.Text = ""
            Me.txtNumero_Serie.Text = ""
            Me.lkpProveedor.EditValue = Nothing
            Me.articulo_lkp = 0

            Exit Sub
        End If

        'SE COMENTO ESTA PARTE POR QUE NO ESTAN UTILIZANDO SERIES
        'DAM - 22-OCT-2008
        If Me.chkCliente.Checked = True Then
            Me.txtNumero_Serie.Text = CStr(Me.lkpArticulo.GetValue("numero_serie"))
            'Else
            '    If ManejaSeries = True Then
            '        Me.grSeries.Visible = True
            '        Me.CargaSeries(Articulo)
            '        Me.grSeries.Focus()
            '    Else
            '        Me.grSeries.Visible = False
            '    End If
        End If

        articulo_lkp = Me.lkpArticulo.GetValue("articulo")
        Me.lPartidaFactura = Me.lkpArticulo.GetValue("partida_factura")
        Me.lblDescripcionCorta.Text = CStr(Me.lkpArticulo.GetValue("descripcion_corta"))
        Me.clcGarantia.EditValue = CLng(Me.lkpArticulo.GetValue("meses_garantia"))
        Me.Ultimo_Costo = CDbl(Me.lkpArticulo.GetValue("ultimo_costo"))
        If Action <> Actions.Update Then
            Me.lkpProveedor.EditValue = CLng(Me.lkpArticulo.GetValue("proveedor"))
        End If
    End Sub

    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim response As Events
        response = oProveedores.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub

    Private Sub lkpCentro_Servicio_Format() Handles lkpCentro_Servicio.Format
        Comunes.clsFormato.for_centros_servicio_grl(Me.lkpCentro_Servicio)
    End Sub
    Private Sub lkpCentro_Servicio_LoadData(ByVal Initialize As Boolean) Handles lkpCentro_Servicio.LoadData
        Dim response As Events
        response = oCentrosServicio.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCentro_Servicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub lkpRecibe_Format() Handles lkpRecibe.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpRecibe)
    End Sub
    Private Sub lkpRecibe_LoadData(ByVal Initialize As Boolean) Handles lkpRecibe.LoadData
        Dim response As Events
        response = oEmpleados.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpRecibe.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub


    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region


    Private Sub chkCliente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        If (Me.chkCliente.Checked = False) Then
            Me.lkpCliente.Enabled = False
            Me.lkpCliente.EditValue = Nothing
            Me.txtDireccion.Text = ""
            Me.txtDireccion.Enabled = False
            Me.lkpFactura.Enabled = False
            Me.lkpFactura.EditValue = Nothing
            Me.dteFechaFactura.EditValue = Nothing
            Me.lkpArticulo_LoadData(True)
            Me.lkpArticulo.EditValue = Nothing
            Me.txtTelefonoCliente.Enabled = False
            Me.txtTelefonoCliente.Text = ""
        Else
            Me.lkpCliente.Enabled = True
            Me.txtDireccion.Enabled = True
            Me.lkpFactura.Enabled = True
            Me.grSeries.Visible = False
            Me.lkpArticulo_LoadData(True)
            Me.lkpArticulo.EditValue = Nothing
            Me.txtTelefonoCliente.Enabled = True
        End If

    End Sub

    Private Sub cboTipo_Servicio_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipo_Servicio.EditValueChanged
        If Me.cboTipo_Servicio.IsLoading Then Exit Sub

        Select Case Me.cboTipo_Servicio.EditValue
            Case "G"
                Me.lkpRecibe.Visible = True
                Me.lblRecibe.Visible = True
                Me.lkpCentro_Servicio.Visible = True
                Me.lblCentro_Servicio.Visible = True
                Me.chkCliente.Checked = True
                Me.dteFechaPromesa.Enabled = True
                Me.lkpBodega.Enabled = False
                Me.txtFalla.Text = ""
                Me.txtFalla.Enabled = True
                Me.chkJuridico.CheckState = CheckState.Unchecked
                Me.chkJuridico.Enabled = False
                Me.lkpSucursal.Enabled = True


                'SE AGREGO ESTA PARTE POR QUE NO ESTAN UTILIZANDO SERIES
                'DAM - 22-OCT-2008
                Me.txtNumero_Serie.Enabled = True

            Case "A"
                Me.lkpRecibe.Visible = True
                Me.lblRecibe.Visible = True
                Me.lkpCentro_Servicio.Visible = True
                Me.lblCentro_Servicio.Visible = True
                Me.chkCliente.Checked = True
                CalcularFechaPromesa(Me.dteFecha.DateTime, Me.DiasArmado)
                Me.dteFechaPromesa.Enabled = False
                Me.lkpBodega.Enabled = False
                Me.txtFalla.Text = "Armado"
                Me.txtFalla.Enabled = False
                Me.chkJuridico.CheckState = CheckState.Unchecked
                Me.chkJuridico.Enabled = False
                Me.lkpSucursal.Enabled = False
                Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual

            Case "I"
                Me.lkpRecibe.Visible = True
                Me.lblRecibe.Visible = True
                Me.lkpCentro_Servicio.Visible = True
                Me.lblCentro_Servicio.Visible = True
                Me.chkCliente.Checked = True
                CalcularFechaPromesa(Me.dteFecha.DateTime, Me.DiasInstalacion)
                Me.dteFechaPromesa.Enabled = False
                Me.lkpBodega.Enabled = False
                Me.txtFalla.Text = "Instalación"
                Me.txtFalla.Enabled = False
                Me.chkJuridico.Enabled = False
                Me.lkpSucursal.Enabled = False
                Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual

            Case "R"
                Me.lkpRecibe.Visible = True
                Me.lblRecibe.Visible = True
                Me.lkpCentro_Servicio.Visible = True
                Me.lblCentro_Servicio.Visible = True
                Me.chkCliente.Checked = False
                Me.dteFechaPromesa.Enabled = True
                Me.lkpBodega.Enabled = True
                Me.txtFalla.Text = ""
                Me.txtFalla.Enabled = True
                Me.chkJuridico.CheckState = CheckState.Unchecked
                Me.chkJuridico.Enabled = True

                Me.txtNumero_Serie.Enabled = True
                Me.lkpSucursal.Enabled = False
                Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        End Select

    End Sub
    Private Sub cboEstatus_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEstatus.EditValueChanged
        If Me.cboEstatus.IsLoading Then Exit Sub

        Select Case Me.cboEstatus.EditValue
            Case "P"
                Me.txtSolucion.Enabled = False
                Me.txtSolucion.Text = ""
                Me.chkCambioAutorizado.Checked = False
                Me.chkCambioAutorizado.Enabled = False

            Case "T"
                Me.txtSolucion.Enabled = True
                Me.chkCambioAutorizado.Enabled = True

            Case "C"
                Me.txtSolucion.Enabled = True
                Me.chkCambioAutorizado.Checked = False
                Me.chkCambioAutorizado.Enabled = False
        End Select
    End Sub
    Private Sub grvSeries_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvSeries.CellValueChanging
        Dim j As Integer = Me.grvSeries.FocusedRowHandle
        Dim i As Integer = 0
        For i = 0 To Me.grvSeries.RowCount - 1
            If i <> j Then
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
            Else
                serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
                Me.txtNumero_Serie.Text = serie_seleccionada
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
            End If
        Next
        grvSeries.UpdateCurrentRow()
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub ImprimeValeOrdenDeServicio()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeDeOrdenDeServicio(Me.clcOrden_Servicio.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Vale de Orden de Servicio no puede Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptValeDeOrdenDeServicio

                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Vale de Orden de Servicio", oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If

            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub
    Private Sub ImprimeSeguimientosOrdenServicio()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ReporteSeguimientosOrdenServicio(Me.clcOrden_Servicio.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Los Seguimientos de la Orden de Servicio no puede Mostrarse")

            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptOrdenesServicioSeguimientos
                    oReport.lblTitulo.Text = "Reporte de Seguimientos de la Orden de Servicio: " + Me.clcOrden_Servicio.Value.ToString
                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Seguimientos de la Orden de Servicio: " + Me.clcOrden_Servicio.Value.ToString, oReport, , , , True)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If

            End If

        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Sub ImprimeEtiqueta()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.EtiquetaDeOrdenDeServicio(Me.clcOrden_Servicio.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La Etiqueta de la Orden de Servicio no puede Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptEtiquetaOrdenServicio

                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Etiqueta de la Orden de Servicio", oReport, , , , True)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If

            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

    Private Sub CargaSeries(ByVal articulo As Long)
        Dim response As Events

        'Trae las series de tipo entrada
        '-1 es para traer todas las series no importando la entrada
        response = oArticulos.SeriesArticulos(articulo, "E", Bodega, -1)
        If response.ErrorFound Then
            response.ShowMessage()
        Else
            Dim odataset As DataSet
            odataset = response.Value
            Me.grSeries.DataSource = odataset.Tables(0)

        End If

    End Sub
    Private Sub CalcularFechaPromesa(ByVal fecha As DateTime, ByVal dias As Long)
        Dim contador As Long = 0

        While contador < dias
            fecha = fecha.AddDays(1)
            If fecha.DayOfWeek <> DayOfWeek.Sunday Then
                contador = contador + 1
            End If
        End While

        Me.dteFechaPromesa.DateTime = fecha
    End Sub
    Private Function GeneraMovimientosEntradasSalidas() As Events
        Dim response As New Events

        If Me.cboTipo_Servicio.EditValue = "R" Then


            If folio_movimiento_salida = 0 Then
                response = Me.oMovimientoInventario.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoSalidaReparacion, Me.folio_movimiento_salida, TINApp.FechaServidor(), 0, "", Me.clcOrden_Servicio.Value, "")
                If Not response.ErrorFound Then
                    response = Me.oMovimientoInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoSalidaReparacion, folio_movimiento_salida, TINApp.FechaServidor(), 1, Articulo, 1, Ultimo_Costo, Ultimo_Costo, -1, 0)
                    If Not response.ErrorFound Then
                        response = Me.oOrdenesServicio.ActualizaFoliosMovimientos(Me.clcOrden_Servicio.Value, folio_movimiento_salida, False)

                        If Not response.ErrorFound Then
                            response = oArticulosExistencias.Actualizar(Me.Articulo, Me.Bodega, "FI", -1, 0)

                            If Not response.ErrorFound Then
                                response = oArticulosExistencias.Actualizar(Me.Articulo, Me.Bodega, "GA", 1, 0)
                            End If
                        End If
                    End If
                End If
            End If

            If folio_movimiento_salida > 0 And folio_movimiento_entrada = 0 And (Me.cboEstatus.EditValue = "C" Or (Me.cboEstatus.EditValue = "T" And chkCambioAutorizado.Checked = False)) Then
                response = Me.oMovimientoInventario.Insertar(Comunes.Common.Sucursal_Actual, Me.Bodega, Me.ConceptoEntradaReparacion, Me.folio_movimiento_entrada, TINApp.FechaServidor, 0, "", Me.clcOrden_Servicio.Value, "")
                If Not response.ErrorFound Then
                    response = Me.oMovimientoInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoEntradaReparacion, Me.folio_movimiento_entrada, TINApp.FechaServidor, 1, Me.Articulo, 1, Ultimo_Costo, Ultimo_Costo, -1, 0)
                    If Not response.ErrorFound Then
                        response = Me.oOrdenesServicio.ActualizaFoliosMovimientos(Me.clcOrden_Servicio.Value, folio_movimiento_entrada, True)

                        If Not response.ErrorFound And Me.cboEstatus.EditValue = "T" Then
                            response = oArticulosExistencias.Actualizar(Me.Articulo, Me.Bodega, "GA", -1, 0)

                            If Not response.ErrorFound And Me.chkCambioAutorizado.Checked = False Then
                                response = oArticulosExistencias.Actualizar(Me.Articulo, Me.Bodega, "FI", 1, 0)

                            End If
                        End If
                    End If

                End If
            End If
        End If

        Return response
    End Function


#End Region

End Class
