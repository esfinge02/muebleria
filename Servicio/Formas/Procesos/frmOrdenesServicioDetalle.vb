Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmOrdenesServicioDetalle
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
		Friend WithEvents lblFecha As System.Windows.Forms.Label
		Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblPersona As System.Windows.Forms.Label
		Friend WithEvents txtPersona As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblCapturo As System.Windows.Forms.Label
		Friend WithEvents txtCapturo As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblObservaciones As System.Windows.Forms.Label
		Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
		Friend WithEvents lblFecha_Promesa As System.Windows.Forms.Label
		Friend WithEvents dteFecha_Promesa As DevExpress.XtraEditors.DateEdit
    
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesServicioDetalle))
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblPersona = New System.Windows.Forms.Label
        Me.txtPersona = New DevExpress.XtraEditors.TextEdit
        Me.lblCapturo = New System.Windows.Forms.Label
        Me.txtCapturo = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblFecha_Promesa = New System.Windows.Forms.Label
        Me.dteFecha_Promesa = New DevExpress.XtraEditors.DateEdit
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Promesa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(679, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(456, 152)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(88, 19)
        Me.clcPartida.TabIndex = 3
        Me.clcPartida.Tag = "Partida"
        Me.clcPartida.Visible = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(64, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 6, 5, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(110, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = "fecha"
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(52, 64)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(53, 16)
        Me.lblPersona.TabIndex = 6
        Me.lblPersona.Tag = ""
        Me.lblPersona.Text = "Per&sona:"
        Me.lblPersona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersona
        '
        Me.txtPersona.EditValue = ""
        Me.txtPersona.Location = New System.Drawing.Point(110, 64)
        Me.txtPersona.Name = "txtPersona"
        '
        'txtPersona.Properties
        '
        Me.txtPersona.Properties.MaxLength = 100
        Me.txtPersona.Size = New System.Drawing.Size(600, 20)
        Me.txtPersona.TabIndex = 7
        Me.txtPersona.Tag = "persona"
        '
        'lblCapturo
        '
        Me.lblCapturo.AutoSize = True
        Me.lblCapturo.Location = New System.Drawing.Point(52, 88)
        Me.lblCapturo.Name = "lblCapturo"
        Me.lblCapturo.Size = New System.Drawing.Size(53, 16)
        Me.lblCapturo.TabIndex = 8
        Me.lblCapturo.Tag = ""
        Me.lblCapturo.Text = "Cap&turo:"
        Me.lblCapturo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCapturo
        '
        Me.txtCapturo.EditValue = ""
        Me.txtCapturo.Location = New System.Drawing.Point(110, 88)
        Me.txtCapturo.Name = "txtCapturo"
        '
        'txtCapturo.Properties
        '
        Me.txtCapturo.Properties.Enabled = False
        Me.txtCapturo.Properties.MaxLength = 100
        Me.txtCapturo.Size = New System.Drawing.Size(600, 20)
        Me.txtCapturo.TabIndex = 9
        Me.txtCapturo.Tag = "capturo"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(16, 112)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 10
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(110, 112)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(600, 38)
        Me.txtObservaciones.TabIndex = 11
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblFecha_Promesa
        '
        Me.lblFecha_Promesa.AutoSize = True
        Me.lblFecha_Promesa.Location = New System.Drawing.Point(16, 154)
        Me.lblFecha_Promesa.Name = "lblFecha_Promesa"
        Me.lblFecha_Promesa.Size = New System.Drawing.Size(93, 16)
        Me.lblFecha_Promesa.TabIndex = 12
        Me.lblFecha_Promesa.Tag = ""
        Me.lblFecha_Promesa.Text = "F&echa Promesa:"
        Me.lblFecha_Promesa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Promesa
        '
        Me.dteFecha_Promesa.EditValue = New Date(2006, 6, 5, 0, 0, 0, 0)
        Me.dteFecha_Promesa.Location = New System.Drawing.Point(110, 154)
        Me.dteFecha_Promesa.Name = "dteFecha_Promesa"
        '
        'dteFecha_Promesa.Properties
        '
        Me.dteFecha_Promesa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Promesa.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Promesa.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Promesa.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Promesa.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Promesa.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Promesa.TabIndex = 13
        Me.dteFecha_Promesa.Tag = "fecha_promesa"
        '
        'frmOrdenesServicioDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(720, 184)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblPersona)
        Me.Controls.Add(Me.txtPersona)
        Me.Controls.Add(Me.lblCapturo)
        Me.Controls.Add(Me.txtCapturo)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblFecha_Promesa)
        Me.Controls.Add(Me.dteFecha_Promesa)
        Me.Name = "frmOrdenesServicioDetalle"
        Me.Controls.SetChildIndex(Me.dteFecha_Promesa, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Promesa, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtCapturo, 0)
        Me.Controls.SetChildIndex(Me.lblCapturo, 0)
        Me.Controls.SetChildIndex(Me.txtPersona, 0)
        Me.Controls.SetChildIndex(Me.lblPersona, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Promesa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmOrdenesServicioDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub
    Private Sub frmOrdenesServicioDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        Me.clcPartida.EditValue = CLng(CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("Partida"))

        If Me.txtCapturo.Text <> CStr(TINApp.Connection.UserName) Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.tbrTools.Buttons(0).Visible = False
        End If

    End Sub
    Private Sub frmOrdenesServicioDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Select Case Action
            Case Actions.Insert
                Me.txtCapturo.Text = CStr(TINApp.Connection.UserName)
                Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFecha_Promesa.EditValue = CDate(TINApp.FechaServidor)
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmOrdenesServicioDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = CType(OwnerForm, frmOrdenesServicio).oOrdenesServicioDetalle.Validacion(Action, Me.dteFecha.Text, Me.dteFecha_Promesa.Text, Me.txtCapturo.Text, Me.txtPersona.Text, Me.txtObservaciones.Text)
    End Sub

    Private Sub frmOrdenesServicioDetalle_Localize() Handles MyBase.Localize
        Find("Partida", CType(Me.clcPartida.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
