Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptValeDeOrdenDeServicio
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtOrdenServicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txttipo_servicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private sucursal_elabora As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblNombreCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtdireccion_cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescricpionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFechaCompra As DataDynamics.ActiveReports.Label = Nothing
	Private txtFechaCompra As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblGarantia As DataDynamics.ActiveReports.Label = Nothing
	Private txtGarantia As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFalla As DataDynamics.ActiveReports.Label = Nothing
	Private txtFalla As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCentroServicio As DataDynamics.ActiveReports.Label = Nothing
	Private txtCentroServicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblPersona As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_centro_servicio1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEnviadoCon As DataDynamics.ActiveReports.Label = Nothing
	Private txtEnviadoCon As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtestatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtnombreproveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtnumeroserie As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private telefono_cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEtiquetaReimpresion As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Servicio.rptValeDeOrdenDeServicio.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtOrdenServicio = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txttipo_servicio = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.sucursal_elabora = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblNombreCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtNombreCliente = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblDomicilio = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtdireccion_cliente = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtDescricpionCorta = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lblFechaCompra = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtFechaCompra = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblFactura = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtFactura = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.lblGarantia = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtGarantia = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.lblFalla = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtFalla = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.lblCentroServicio = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.Label)
		Me.txtCentroServicio = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.lblPersona = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.Label)
		Me.nombre_centro_servicio1 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.lblEnviadoCon = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtEnviadoCon = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.Detail.Controls(23),DataDynamics.ActiveReports.Label)
		Me.txtestatus = CType(Me.Detail.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.Detail.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.Detail.Controls(26),DataDynamics.ActiveReports.Label)
		Me.txtnombreproveedor = CType(Me.Detail.Controls(27),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha = CType(Me.Detail.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.Label5 = CType(Me.Detail.Controls(29),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.Detail.Controls(30),DataDynamics.ActiveReports.Label)
		Me.txtnumeroserie = CType(Me.Detail.Controls(31),DataDynamics.ActiveReports.TextBox)
		Me.Label7 = CType(Me.Detail.Controls(32),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.Detail.Controls(33),DataDynamics.ActiveReports.Label)
		Me.telefono_cliente = CType(Me.Detail.Controls(34),DataDynamics.ActiveReports.TextBox)
		Me.txtEtiquetaReimpresion = CType(Me.Detail.Controls(35),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

  
End Class
