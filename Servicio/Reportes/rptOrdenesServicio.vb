Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptOrdenesServicio
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblOrdenado As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private Label38 As DataDynamics.ActiveReports.Label = Nothing
	Private Label39 As DataDynamics.ActiveReports.Label = Nothing
	Private Label40 As DataDynamics.ActiveReports.Label = Nothing
	Private Label41 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTipo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaPromesa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEstatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOrdenServicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFalla As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSeguimientos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaTermino As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDiasResolucion As DataDynamics.ActiveReports.TextBox = Nothing
	Private dias_resolucion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private centro_servicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private dias_resolucion2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPromedioResolucionDias As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Servicio.rptOrdenesServicio.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphTipo = CType(Me.Sections("gphTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTipo = CType(Me.Sections("gpfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Label37 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblOrdenado = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Line)
		Me.Label32 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label34 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label36 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label38 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Label39 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.Label40 = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.Label41 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.txtTipo = CType(Me.gphTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaPromesa = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtEstatus = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtOrdenServicio = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFalla = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSeguimientos = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaTermino = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtDiasResolucion = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.dias_resolucion1 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.centro_servicio = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.cliente = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.dias_resolucion2 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label35 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPromedioResolucionDias = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptOrdenesServicio_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
