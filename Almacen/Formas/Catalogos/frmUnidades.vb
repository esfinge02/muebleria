Imports Dipros.Utils.Common

Public Class frmUnidades
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblUnidad As System.Windows.Forms.Label
    Friend WithEvents txtUnidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUnidades))
        Me.lblUnidad = New System.Windows.Forms.Label
        Me.txtUnidad = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtUnidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(90, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblUnidad
        '
        Me.lblUnidad.AutoSize = True
        Me.lblUnidad.Location = New System.Drawing.Point(41, 43)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(47, 16)
        Me.lblUnidad.TabIndex = 0
        Me.lblUnidad.Tag = ""
        Me.lblUnidad.Text = "&Unidad:"
        Me.lblUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUnidad
        '
        Me.txtUnidad.EditValue = ""
        Me.txtUnidad.Location = New System.Drawing.Point(92, 40)
        Me.txtUnidad.Name = "txtUnidad"
        '
        'txtUnidad.Properties
        '
        Me.txtUnidad.Properties.MaxLength = 3
        Me.txtUnidad.Size = New System.Drawing.Size(28, 22)
        Me.txtUnidad.TabIndex = 1
        Me.txtUnidad.Tag = "unidad"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(16, 66)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 30
        Me.txtDescripcion.Size = New System.Drawing.Size(196, 22)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmUnidades
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(298, 96)
        Me.Controls.Add(Me.lblUnidad)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtUnidad)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmUnidades"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtUnidad, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblUnidad, 0)
        CType(Me.txtUnidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oUnidades As VillarrealBusiness.clsUnidades
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmUnidades_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oUnidades.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oUnidades.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oUnidades.Eliminar(txtUnidad.Text)

        End Select
    End Sub

    Private Sub frmUnidades_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oUnidades.DespliegaDatos(OwnerForm.Value("unidad"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmUnidades_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oUnidades = New VillarrealBusiness.clsUnidades

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmUnidades_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oUnidades.Validacion(Action, Me.txtUnidad.Text, txtDescripcion.Text)
    End Sub

    Private Sub frmUnidades_Localize() Handles MyBase.Localize
        Find("unidad", Me.txtUnidad.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
