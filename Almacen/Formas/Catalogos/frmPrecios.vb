Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmPrecios
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
    Friend WithEvents clcPrecio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcTerminacion As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkRecalcular As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPrecios))
        Me.lblPrecio = New System.Windows.Forms.Label
        Me.clcPrecio = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.clcTerminacion = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkRecalcular = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTerminacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRecalcular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(44, 40)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(42, 16)
        Me.lblPrecio.TabIndex = 0
        Me.lblPrecio.Tag = ""
        Me.lblPrecio.Text = "Pr&ecio:"
        Me.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecio
        '
        Me.clcPrecio.EditValue = "0"
        Me.clcPrecio.Location = New System.Drawing.Point(89, 40)
        Me.clcPrecio.MaxValue = 0
        Me.clcPrecio.MinValue = 0
        Me.clcPrecio.Name = "clcPrecio"
        '
        'clcPrecio.Properties
        '
        Me.clcPrecio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPrecio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPrecio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio.Properties.Enabled = False
        Me.clcPrecio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPrecio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecio.Size = New System.Drawing.Size(50, 19)
        Me.clcPrecio.TabIndex = 1
        Me.clcPrecio.Tag = "precio"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(33, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(89, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 20
        Me.txtNombre.Size = New System.Drawing.Size(192, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'clcTerminacion
        '
        Me.clcTerminacion.EditValue = "0"
        Me.clcTerminacion.Location = New System.Drawing.Point(89, 87)
        Me.clcTerminacion.MaxValue = 0
        Me.clcTerminacion.MinValue = 0
        Me.clcTerminacion.Name = "clcTerminacion"
        '
        'clcTerminacion.Properties
        '
        Me.clcTerminacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTerminacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTerminacion.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTerminacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTerminacion.Size = New System.Drawing.Size(50, 19)
        Me.clcTerminacion.TabIndex = 5
        Me.clcTerminacion.Tag = "terminacion_precio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Terminaci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkRecalcular
        '
        Me.chkRecalcular.Location = New System.Drawing.Point(88, 112)
        Me.chkRecalcular.Name = "chkRecalcular"
        '
        'chkRecalcular.Properties
        '
        Me.chkRecalcular.Properties.Caption = "Recalcular"
        Me.chkRecalcular.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkRecalcular.Size = New System.Drawing.Size(96, 19)
        Me.chkRecalcular.TabIndex = 6
        Me.chkRecalcular.Tag = "recalcular"
        '
        'frmPrecios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(290, 144)
        Me.Controls.Add(Me.chkRecalcular)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcTerminacion)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.clcPrecio)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "frmPrecios"
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcPrecio, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio, 0)
        Me.Controls.SetChildIndex(Me.clcTerminacion, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.chkRecalcular, 0)
        CType(Me.clcPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTerminacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRecalcular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oPrecios As VillarrealBusiness.clsPrecios
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmPrecios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oPrecios.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oPrecios.Actualizar(Me.DataSource)

            Case Actions.Delete
                Try
                    Response = oPrecios.Eliminar(clcPrecio.Value)
                Catch ex As Exception
                    ShowMessage(MessageType.MsgInformation, "No puede borrar el precio, ya que otros registros lo utilizan.")
                End Try


        End Select
    End Sub

    Private Sub frmPrecios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oPrecios.DespliegaDatos(OwnerForm.Value("precio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmPrecios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oPrecios = New VillarrealBusiness.clsPrecios

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmPrecios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oPrecios.Validacion(Action, Me.clcPrecio.Value, txtNombre.Text)
    End Sub

    Private Sub frmPrecios_Localize() Handles MyBase.Localize
        Find("precio", Me.clcPrecio.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPrecios.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
