
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms


Public Class frmUbicaciones
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUbicacion As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUbicaciones))
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblUbicacion = New System.Windows.Forms.Label
        Me.txtUbicacion = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtUbicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(466, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(34, 40)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 0
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Bodega"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(87, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(105, 20)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblUbicacion
        '
        Me.lblUbicacion.AutoSize = True
        Me.lblUbicacion.Location = New System.Drawing.Point(22, 63)
        Me.lblUbicacion.Name = "lblUbicacion"
        Me.lblUbicacion.Size = New System.Drawing.Size(62, 16)
        Me.lblUbicacion.TabIndex = 3
        Me.lblUbicacion.Tag = ""
        Me.lblUbicacion.Text = "&Ubicaci�n:"
        Me.lblUbicacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUbicacion
        '
        Me.txtUbicacion.EditValue = ""
        Me.txtUbicacion.Location = New System.Drawing.Point(87, 63)
        Me.txtUbicacion.Name = "txtUbicacion"
        '
        'txtUbicacion.Properties
        '
        Me.txtUbicacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUbicacion.Properties.MaxLength = 15
        Me.txtUbicacion.Size = New System.Drawing.Size(105, 20)
        Me.txtUbicacion.TabIndex = 4
        Me.txtUbicacion.Tag = "ubicacion"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 86)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 5
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(87, 86)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 60
        Me.txtDescripcion.Size = New System.Drawing.Size(360, 20)
        Me.txtDescripcion.TabIndex = 6
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmUbicaciones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 119)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblUbicacion)
        Me.Controls.Add(Me.txtUbicacion)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmUbicaciones"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtUbicacion, 0)
        Me.Controls.SetChildIndex(Me.lblUbicacion, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        CType(Me.txtUbicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oUbicaciones As VillarrealBusiness.clsUbicaciones
    Private oBodegas As New VillarrealBusiness.clsBodegas

    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmUbicaciones_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oUbicaciones.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oUbicaciones.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oUbicaciones.Eliminar(lkpBodega.editvalue, txtUbicacion.Text)

        End Select
    End Sub

    Private Sub frmUbicaciones_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oUbicaciones.DespliegaDatos(OwnerForm.Value("bodega"), OwnerForm.Value("ubicacion"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmUbicaciones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oUbicaciones = New VillarrealBusiness.clsUbicaciones

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpBodega.Enabled = False
                Me.txtUbicacion.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmUbicaciones_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oUbicaciones.Validacion(Action, Me.Bodega, Me.txtUbicacion.Text, txtDescripcion.Text)
    End Sub

    Private Sub frmUbicaciones_Localize() Handles MyBase.Localize
        Find("ubicacion", Me.txtUbicacion.Text)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
