Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmConceptosInventario
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkAfecta_saldos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkModificableMovimientosInv As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkNoAplicaCosteoInv As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConceptosInventario))
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblTipo = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.chkAfecta_saldos = New DevExpress.XtraEditors.CheckEdit
        Me.chkModificableMovimientosInv = New DevExpress.XtraEditors.CheckEdit
        Me.chkNoAplicaCosteoInv = New DevExpress.XtraEditors.CheckEdit
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAfecta_saldos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkModificableMovimientosInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoAplicaCosteoInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(25, 43)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 0
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "C&oncepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(89, 40)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConcepto.Properties.MaxLength = 5
        Me.txtConcepto.Size = New System.Drawing.Size(44, 20)
        Me.txtConcepto.TabIndex = 1
        Me.txtConcepto.Tag = "concepto"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(13, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(53, 121)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(32, 16)
        Me.lblTipo.TabIndex = 4
        Me.lblTipo.Tag = ""
        Me.lblTipo.Text = "&Tipo:"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "E"
        Me.cboTipo.Location = New System.Drawing.Point(89, 118)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Entrada", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Salida", "S", -1)})
        Me.cboTipo.Size = New System.Drawing.Size(87, 23)
        Me.cboTipo.TabIndex = 5
        Me.cboTipo.Tag = "tipo"
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(50, 144)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 6
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(89, 142)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(87, 19)
        Me.clcFolio.TabIndex = 7
        Me.clcFolio.Tag = "folio"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(89, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(260, 48)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'chkAfecta_saldos
        '
        Me.chkAfecta_saldos.Location = New System.Drawing.Point(88, 166)
        Me.chkAfecta_saldos.Name = "chkAfecta_saldos"
        '
        'chkAfecta_saldos.Properties
        '
        Me.chkAfecta_saldos.Properties.Caption = "Af&ecta Costos"
        Me.chkAfecta_saldos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkAfecta_saldos.Size = New System.Drawing.Size(116, 19)
        Me.chkAfecta_saldos.TabIndex = 8
        Me.chkAfecta_saldos.Tag = "afecta_saldos"
        '
        'chkModificableMovimientosInv
        '
        Me.chkModificableMovimientosInv.Location = New System.Drawing.Point(88, 187)
        Me.chkModificableMovimientosInv.Name = "chkModificableMovimientosInv"
        '
        'chkModificableMovimientosInv.Properties
        '
        Me.chkModificableMovimientosInv.Properties.Caption = "&Modificable en Movimientos al Inventario"
        Me.chkModificableMovimientosInv.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkModificableMovimientosInv.Size = New System.Drawing.Size(229, 19)
        Me.chkModificableMovimientosInv.TabIndex = 9
        Me.chkModificableMovimientosInv.Tag = "modificable_movimientosinv"
        '
        'chkNoAplicaCosteoInv
        '
        Me.chkNoAplicaCosteoInv.Location = New System.Drawing.Point(88, 208)
        Me.chkNoAplicaCosteoInv.Name = "chkNoAplicaCosteoInv"
        '
        'chkNoAplicaCosteoInv.Properties
        '
        Me.chkNoAplicaCosteoInv.Properties.Caption = "&No Aplica en Costeo de Inventario"
        Me.chkNoAplicaCosteoInv.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoAplicaCosteoInv.Size = New System.Drawing.Size(196, 19)
        Me.chkNoAplicaCosteoInv.TabIndex = 10
        Me.chkNoAplicaCosteoInv.Tag = "noaplica_costeoinv"
        '
        'chkActivo
        '
        Me.chkActivo.Location = New System.Drawing.Point(88, 229)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Size = New System.Drawing.Size(116, 19)
        Me.chkActivo.TabIndex = 11
        Me.chkActivo.Tag = "activo"
        '
        'frmConceptosInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 260)
        Me.Controls.Add(Me.chkActivo)
        Me.Controls.Add(Me.chkNoAplicaCosteoInv)
        Me.Controls.Add(Me.chkModificableMovimientosInv)
        Me.Controls.Add(Me.chkAfecta_saldos)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Name = "frmConceptosInventario"
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.lblTipo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.chkAfecta_saldos, 0)
        Me.Controls.SetChildIndex(Me.chkModificableMovimientosInv, 0)
        Me.Controls.SetChildIndex(Me.chkNoAplicaCosteoInv, 0)
        Me.Controls.SetChildIndex(Me.chkActivo, 0)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAfecta_saldos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkModificableMovimientosInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoAplicaCosteoInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmConceptosInventario_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oConceptosInventario.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oConceptosInventario.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oConceptosInventario.Eliminar(txtConcepto.Text)

        End Select
    End Sub

    Private Sub frmConceptosInventario_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oConceptosInventario.DespliegaDatos(OwnerForm.Value("concepto"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub

    Private Sub frmConceptosInventario_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.txtConcepto.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmConceptosInventario_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oConceptosInventario.Validacion(Action, Me.txtConcepto.Text, txtDescripcion.Text)
    End Sub

    Private Sub frmConceptosInventario_Localize() Handles MyBase.Localize
        Find("concepto", Me.txtConcepto.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
