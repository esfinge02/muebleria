Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmMovimientos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientos))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Location = New System.Drawing.Point(16, 136)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(376, 56)
        Me.gpbFechas.TabIndex = 10
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Ini.TabIndex = 12
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(33, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "D&esde:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(197, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Ha&sta:"
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(240, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 14
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(104, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.ToolTip = "Bodega"
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "&Bodega:"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(280, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(57, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "&Grupo:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 65)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "&Departamento:"
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.ToolTip = "Departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(49, 113)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Ar&t�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = "(Todos)"
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = True
        Me.lkpArticulo.Size = New System.Drawing.Size(280, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 7
        Me.lkpArticulo.Tag = ""
        Me.lkpArticulo.ToolTip = "Art�culo"
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'frmMovimientos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 200)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmMovimientos"
        Me.Text = "frmMovimientos"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGruposArticulos As VillarrealBusiness.clsGruposArticulos
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario


    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property

    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property

    Private ReadOnly Property Articulos() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property


    'Private ReadOnly Property Bodega() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpBodega.ToXML, "Bodega")
    '    End Get
    'End Property
    'Private ReadOnly Property Departamento() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpDepartamento.ToXML, "Departamento")
    '    End Get
    'End Property

    'Private ReadOnly Property Grupo() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpGrupo.ToXML, "Grupo")
    '    End Get
    'End Property
    'Private ReadOnly Property Articulos() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpArticulo.ToXML, "Articulo")
    '    End Get
    'End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMovimientos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.MovimientosNuevo(Me.Bodega, Me.Departamento, Me.Grupo, Me.Articulos, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Resumen de Movimientos no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptResumenMovimientos
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                oReport.oData = oDataSet
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Resumen de Movimientos", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub

    Private Sub frmMovimientos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oBodegas = New VillarrealBusiness.clsBodegas
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGruposArticulos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario

        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
    End Sub

    Private Sub frmMovimientos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.Validacion(Bodega, Grupo, Departamento, Articulos, dteFecha_Ini.Text, dteFecha_Fin.Text)
        Response = oReportes.Validacion(dteFecha_Ini.Text, dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasExistencias
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.LookupDepartamentosExistencias
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        lkpGrupo.EditValue = Nothing
        lkpGrupo_LoadData(True)

    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        'Response = oGruposArticulos.MultiLookup(Me.lkpDepartamento.ToXML)
        Response = oGruposArticulos.LookupGruposExistencias(Me.Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.SelectAll = True
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged
        lkpArticulo.EditValue = Nothing
        lkpArticulo_LoadData(True)

    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        'Response = oArticulos.MultiLookup(Me.lkpDepartamento.ToXML, Me.lkpGrupo.ToXML)
        Response = oArticulos.LookupArticulosMovtos(Me.Departamento, Me.Grupo)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.SelectAll = True
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
#End Region

End Class
