Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmMovimientosxReferencia
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConcepto_Salida As System.Windows.Forms.Label
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosxReferencia))
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lblConcepto_Salida = New System.Windows.Forms.Label
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(126, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "descripcion"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(96, 40)
        Me.lkpConcepto.MultiSelect = True
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = "(Todos)"
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = True
        Me.lkpConcepto.Size = New System.Drawing.Size(288, 20)
        Me.lkpConcepto.TabIndex = 1
        Me.lkpConcepto.Tag = ""
        Me.lkpConcepto.ToolTip = "Concepto"
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lblConcepto_Salida
        '
        Me.lblConcepto_Salida.AutoSize = True
        Me.lblConcepto_Salida.Location = New System.Drawing.Point(32, 40)
        Me.lblConcepto_Salida.Name = "lblConcepto_Salida"
        Me.lblConcepto_Salida.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto_Salida.TabIndex = 0
        Me.lblConcepto_Salida.Tag = ""
        Me.lblConcepto_Salida.Text = "Co&ncepto:"
        Me.lblConcepto_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(8, 64)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(376, 56)
        Me.gpbFechas.TabIndex = 2
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(33, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(198, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "&Hasta: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(240, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Fin.TabIndex = 14
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(144, 128)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Des&glosado"
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(112, 19)
        Me.chkDesglosado.TabIndex = 3
        Me.chkDesglosado.Tag = ""
        Me.chkDesglosado.ToolTip = "Movimientos Desglosados"
        '
        'frmMovimientosxReferencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 152)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lkpConcepto)
        Me.Controls.Add(Me.lblConcepto_Salida)
        Me.Controls.Add(Me.gpbFechas)
        Me.Name = "frmMovimientosxReferencia"
        Me.Text = "frmMovimientosxReferencia"
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto_Salida, 0)
        Me.Controls.SetChildIndex(Me.lkpConcepto, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oReportes As VillarrealBusiness.Reportes

    Private ReadOnly Property Concepto() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpConcepto.ToXML, "Concepto")
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmMovimientosxReferencia_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oReportes = New VillarrealBusiness.Reportes
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub

    Private Sub frmMovimientosxReferencia_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.MovimientosReferencia(Me.lkpConcepto.ToXML, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.chkDesglosado.EditValue)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Movimientos no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                If chkDesglosado.Checked = False Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptMovimientos

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                    TINApp.ShowReport(Me.MdiParent, "Movimientos por Referencia Concentrado", oReport)

                    oDataSet = Nothing
                    oReport = Nothing
                Else
                    Dim oDataSet As DataSet
                    Dim oReport1 As New rptMovimientosDesglosado

                    oDataSet = Response.Value
                    oReport1.DataSource = oDataSet.Tables(0)
                    'oReport1.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                    TINApp.ShowReport(Me.MdiParent, "Movimientos por Referencia Desglosado", oReport1)

                    oDataSet = Nothing
                    oReport1 = Nothing
                End If
            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub

    Private Sub frmMovimientosxReferencia_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Concepto, Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConcepto)
    End Sub
    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

End Class
