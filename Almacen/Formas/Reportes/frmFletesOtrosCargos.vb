Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmFletesOtrosCargos
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents chkIncluidoFactura As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents dteDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents gpoRangoFechas As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmFletesOtrosCargos))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblHasta = New System.Windows.Forms.Label
        Me.lblDesde = New System.Windows.Forms.Label
        Me.chkIncluidoFactura = New DevExpress.XtraEditors.CheckEdit
        Me.dteDesde = New DevExpress.XtraEditors.DateEdit
        Me.dteHasta = New DevExpress.XtraEditors.DateEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.gpoRangoFechas = New System.Windows.Forms.GroupBox
        CType(Me.chkIncluidoFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoRangoFechas.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(276, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "&Sucursal:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(216, 22)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(41, 16)
        Me.lblHasta.TabIndex = 2
        Me.lblHasta.Text = "Has&ta:"
        Me.lblHasta.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(24, 22)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(43, 16)
        Me.lblDesde.TabIndex = 0
        Me.lblDesde.Text = "&Desde:"
        Me.lblDesde.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'chkIncluidoFactura
        '
        Me.chkIncluidoFactura.Location = New System.Drawing.Point(80, 67)
        Me.chkIncluidoFactura.Name = "chkIncluidoFactura"
        '
        'chkIncluidoFactura.Properties
        '
        Me.chkIncluidoFactura.Properties.Caption = "&Incluido en Factura"
        Me.chkIncluidoFactura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkIncluidoFactura.Size = New System.Drawing.Size(128, 19)
        Me.chkIncluidoFactura.TabIndex = 2
        '
        'dteDesde
        '
        Me.dteDesde.EditValue = New Date(2007, 10, 30, 0, 0, 0, 0)
        Me.dteDesde.Location = New System.Drawing.Point(72, 22)
        Me.dteDesde.Name = "dteDesde"
        '
        'dteDesde.Properties
        '
        Me.dteDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDesde.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteDesde.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteDesde.Size = New System.Drawing.Size(96, 20)
        Me.dteDesde.TabIndex = 1
        '
        'dteHasta
        '
        Me.dteHasta.EditValue = New Date(2007, 10, 30, 0, 0, 0, 0)
        Me.dteHasta.Location = New System.Drawing.Point(264, 22)
        Me.dteHasta.Name = "dteHasta"
        '
        'dteHasta.Properties
        '
        Me.dteHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteHasta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteHasta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteHasta.Size = New System.Drawing.Size(96, 20)
        Me.dteHasta.TabIndex = 3
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Seleccione una Sucursal)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(312, 20)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'gpoRangoFechas
        '
        Me.gpoRangoFechas.Controls.Add(Me.lblHasta)
        Me.gpoRangoFechas.Controls.Add(Me.lblDesde)
        Me.gpoRangoFechas.Controls.Add(Me.dteDesde)
        Me.gpoRangoFechas.Controls.Add(Me.dteHasta)
        Me.gpoRangoFechas.Location = New System.Drawing.Point(16, 98)
        Me.gpoRangoFechas.Name = "gpoRangoFechas"
        Me.gpoRangoFechas.Size = New System.Drawing.Size(384, 56)
        Me.gpoRangoFechas.TabIndex = 3
        Me.gpoRangoFechas.TabStop = False
        Me.gpoRangoFechas.Text = "Rango de &fechas"
        '
        'frmFletesOtrosCargos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(418, 168)
        Me.Controls.Add(Me.gpoRangoFechas)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.chkIncluidoFactura)
        Me.Controls.Add(Me.lblSucursal)
        Me.Name = "frmFletesOtrosCargos"
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkIncluidoFactura, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.gpoRangoFechas, 0)
        CType(Me.chkIncluidoFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoRangoFechas.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oReportes As VillarrealBusiness.Reportes

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmFletesOtrosCargos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oReportes = New VillarrealBusiness.Reportes
        Dim oFecha As Date = Date.Parse(TINApp.FechaServidor)
        Me.dteHasta.EditValue = oFecha
        Me.dteDesde.EditValue = New Date(oFecha.Year, oFecha.Month, 1)
        oFecha = Nothing
    End Sub
    Private Sub frmFletesOtrosCargos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaSucursal(Me.lkpSucursal.EditValue)
        If Response.ErrorFound Then Exit Sub
        Response = ValidaRangoFechas(Me.dteDesde.EditValue, Me.dteHasta.EditValue)
    End Sub
    Private Sub frmFletesOtrosCargos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.FletesOtrosCargos(Sucursal, Me.chkIncluidoFactura.EditValue, Me.dteDesde.EditValue, Me.dteHasta.EditValue)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgError, "El reporte de Fletes y Otros Cargos no se puede mostrar.", , Response.Value)
        Else
            Dim oData As DataTable
            Dim oReport As rptFletesOtrosCargos
            Try
                oData = CType(Response.Value, DataSet).Tables(0)
                If oData.Rows.Count > 0 Then
                    oReport = New rptFletesOtrosCargos
                    oReport.DataSource = oData
                    TINApp.ShowReport(Me.MdiParent, "Fletes y Otros Cargos", oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El reporte de Fletes y Otros Cargos no tiene información.")
                End If
            Catch ex As Exception
                ShowMessage(MessageType.MsgError, "El reporte de Fletes y Otros Cargos no se puede mostrar.", , ex)
            Finally
                oData = Nothing
                oReport = Nothing
            End Try
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim oEvent As Events
        oEvent = oSucursales.Lookup()
        If Not oEvent.ErrorFound Then
            Me.lkpSucursal.DataSource = CType(oEvent.Value, DataSet).Tables(0)
        End If
        oEvent = Nothing
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function ValidaSucursal(ByRef sucursal As Object) As Events
        Dim oEvent As New Events
        If sucursal Is Nothing Then
            oEvent.Ex = Nothing
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            oEvent.Message = "Seleccione una sucursal"
            Return oEvent
        End If
        If CLng(sucursal) = -1 Then
            oEvent.Ex = Nothing
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            oEvent.Message = "Seleccione una sucursal"
        End If
        Return oEvent
    End Function
    Private Function ValidaRangoFechas(ByRef desde As Date, ByRef hasta As Date) As Events
        Dim oEvent As New Events
        If desde > hasta Then
            oEvent.Ex = Nothing
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            oEvent.Message = "El rango de fechas es incorrecto. La fecha inicial supera la fecha final"
        End If
        Return oEvent
    End Function
#End Region

End Class
