Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepDevolucionesProveedor
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepDevolucionesProveedor))
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(589, 28)
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(80, 65)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(420, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(352, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 3
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Pr&oveedor:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "Tipo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = -1
        Me.cboTipo.Location = New System.Drawing.Point(80, 90)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", -1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nota Aplicada", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente Nota Aplicada", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aplicada al Inventario", 3, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente Aplicada al Inventario ", 4, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enviado al Proveedor", 5, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendientes Enviado al Proveedor ", 6, -1)})
        Me.cboTipo.Size = New System.Drawing.Size(216, 23)
        Me.cboTipo.TabIndex = 5
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label3)
        Me.gpbFechas.Location = New System.Drawing.Point(37, 120)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(368, 56)
        Me.gpbFechas.TabIndex = 7
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(250, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Fin.TabIndex = 3
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(83, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Ini.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(202, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Has&ta: "
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(80, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(352, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.ToolTip = "Bodega"
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(24, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "&Bodega:"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(328, 94)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Desglosado"
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(96, 19)
        Me.chkDesglosado.TabIndex = 6
        '
        'frmRepDevolucionesProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 188)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmRepDevolucionesProveedor"
        Me.Text = "frmRepDevolucionesProveedor"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oReportes As VillarrealBusiness.Reportes
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oBodegas As VillarrealBusiness.clsBodegas


    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupBodega(Me.lkpBodega)
        End Get
    End Property

    Private Sub frmRepDevolucionesProveedor_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.DevolucionesProveedor(Me.Bodega, Me.Proveedor, Me.chkDesglosado.Checked, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.cboTipo.Value)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de las Devoluciones al Proveedor no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptDevolucionesProveedor
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)

                oReport.lblArticulo.Visible = Me.chkDesglosado.Checked
                oReport.lblModelo.Visible = Me.chkDesglosado.Checked
                oReport.lblDescripcion.Visible = Me.chkDesglosado.Checked
                oReport.lblOrdenServicio.Visible = Me.chkDesglosado.Checked
                oReport.lblCosto.Visible = Me.chkDesglosado.Checked
                oReport.lblImporte.Visible = Me.chkDesglosado.Checked

                oReport.Detail.Visible = Me.chkDesglosado.Checked


                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Devoluciones al Proveedor", oReport, , , , True)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub

    Private Sub frmRepDevolucionesProveedor_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oProveedores = New VillarrealBusiness.clsProveedores
        oBodegas = New VillarrealBusiness.clsBodegas


        Me.dteFecha_Ini.EditValue = Format(CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2)), "dd/MMM/yyyy")
        Me.dteFecha_Fin.EditValue = Format(CDate(TINApp.FechaServidor), "dd/MMM/yyyy")

    End Sub

    Private Sub frmRepDevolucionesProveedor_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(dteFecha_Ini.Text, dteFecha_Fin.Text)
    End Sub


    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasExistencias
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub



End Class
