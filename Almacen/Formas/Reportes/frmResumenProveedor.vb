Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmResumenProveedor
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboMercancia As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmResumenProveedor))
        Me.Label8 = New System.Windows.Forms.Label
        Me.cboMercancia = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        CType(Me.cboMercancia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(158, 28)
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(32, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "&Mercanc�a:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboMercancia
        '
        Me.cboMercancia.EditValue = 1
        Me.cboMercancia.Location = New System.Drawing.Point(96, 88)
        Me.cboMercancia.Name = "cboMercancia"
        '
        'cboMercancia.Properties
        '
        Me.cboMercancia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboMercancia.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Toda", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nacional", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Importaci�n", 3, -1)})
        Me.cboMercancia.Size = New System.Drawing.Size(88, 20)
        Me.cboMercancia.TabIndex = 5
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Location = New System.Drawing.Point(12, 120)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(340, 56)
        Me.gpbFechas.TabIndex = 7
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Ini.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "&Desde : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(184, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Ha&sta : "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(232, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Fin.TabIndex = 11
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(96, 64)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(400, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(232, 20)
        Me.lkpProveedor.TabIndex = 3
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Pr&oveedor:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(96, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(232, 20)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "&Bodega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(216, 88)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Des&glosado"
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(112, 19)
        Me.chkDesglosado.TabIndex = 6
        Me.chkDesglosado.Tag = ""
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(408, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        Me.lkpSucursal.TabIndex = 61
        Me.lkpSucursal.Tag = ""
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        Me.lkpSucursal.Visible = False
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(352, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 60
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSucursal.Visible = False
        '
        'frmResumenProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 192)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboMercancia)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmResumenProveedor"
        Me.Text = "frmResumenProveedor"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.cboMercancia, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        CType(Me.cboMercancia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Bodega_Actual_Usuario() As String
        Get
            Return Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)
        End Get
    End Property

    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

    Private ReadOnly Property Bodega() As String
        Get
            'Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpBodega.ToXML, "Bodega")
            Return Comunes.clsUtilerias.PreparaValorLookupBodega(Me.lkpBodega)
        End Get
    End Property

    Private strConceptoCompra As String = ""

    Public ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmResumenProveedor_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.ResumenProveedor(Bodega, Proveedor, Me.dteFecha_Ini.DateTime, Me.dteFecha_Fin.DateTime, Me.chkDesglosado.Checked, Me.cboMercancia.EditValue, strConceptoCompra)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Resumen de Compras al Proveedor no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                If chkDesglosado.Checked = False Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptResumenProveedor
                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Resumen de Compras al Proveedor", oReport)
                    oDataSet = Nothing
                    oReport = Nothing
                Else
                    Dim oDataSet As DataSet
                    Dim oReport1 As New rptResumenProveedorDesglosado
                    oDataSet = Response.Value
                    oReport1.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Resumen de Compras al Proveedor Desglosado", oReport1)
                    oDataSet = Nothing
                    oReport1 = Nothing
                End If
            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Sub frmResumenProveedor_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oProveedores = New VillarrealBusiness.clsProveedores
        oReportes = New VillarrealBusiness.Reportes
        oBodegas = New VillarrealBusiness.clsBodegas
        oSucursales = New VillarrealBusiness.clsSucursales
        Dim suc As Long
        suc = CType(CType(oBodegas.DespliegaDatos(Bodega_Actual_Usuario).Value, DataSet).Tables(0).Rows(0).Item("sucursal"), Long)
        Me.lkpSucursal.EditValue = suc

        Me.dteFecha_Fin.DateTime = CDate(TINApp.FechaServidor)
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        CargarConceptoCompra()
    End Sub

    Private Sub frmResumenProveedor_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.Validacion(Bodega, Proveedor, dteFecha_Ini.Text, dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega_LoadData(True)
    End Sub


    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup(sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarConceptoCompra()
        Dim oVariables As New VillarrealBusiness.clsVariables

        strConceptoCompra = oVariables.TraeDatos("concepto_compra", VillarrealBusiness.clsVariables.tipo_dato.Varchar)

        oVariables = Nothing

    End Sub
#End Region

End Class
