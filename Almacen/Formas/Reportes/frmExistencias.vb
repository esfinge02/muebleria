Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmExistencias
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents optTodos As System.Windows.Forms.RadioButton
    Friend WithEvents optConExistencia As System.Windows.Forms.RadioButton
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkCorteProveedor As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExistencias))
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.optTodos = New System.Windows.Forms.RadioButton
        Me.optConExistencia = New System.Windows.Forms.RadioButton
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkCorteProveedor = New DevExpress.XtraEditors.CheckEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        CType(Me.chkCorteProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(34, 50)
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 87)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(352, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "&Grupo: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "&Departamento: "
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 63)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(352, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'optTodos
        '
        Me.optTodos.Checked = True
        Me.optTodos.Location = New System.Drawing.Point(85, 144)
        Me.optTodos.Name = "optTodos"
        Me.optTodos.Size = New System.Drawing.Size(136, 24)
        Me.optTodos.TabIndex = 9
        Me.optTodos.TabStop = True
        Me.optTodos.Text = "&Todos los Art�culos"
        '
        'optConExistencia
        '
        Me.optConExistencia.Location = New System.Drawing.Point(245, 144)
        Me.optConExistencia.Name = "optConExistencia"
        Me.optConExistencia.Size = New System.Drawing.Size(136, 24)
        Me.optConExistencia.TabIndex = 10
        Me.optConExistencia.Text = "Con &Existencia"
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(104, 39)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(420, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(352, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 1
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Pr&oveedor:"
        '
        'chkCorteProveedor
        '
        Me.chkCorteProveedor.Location = New System.Drawing.Point(328, 112)
        Me.chkCorteProveedor.Name = "chkCorteProveedor"
        '
        'chkCorteProveedor.Properties
        '
        Me.chkCorteProveedor.Properties.Caption = "Corte por Proveedor"
        Me.chkCorteProveedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkCorteProveedor.Size = New System.Drawing.Size(128, 19)
        Me.chkCorteProveedor.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(64, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Tag = ""
        Me.Label5.Text = "Tipo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = 1
        Me.cboTipo.Location = New System.Drawing.Point(104, 112)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nacional", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Importaci�n", 3, -1)})
        Me.cboTipo.Size = New System.Drawing.Size(128, 20)
        Me.cboTipo.TabIndex = 7
        '
        'frmExistencias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(466, 176)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.chkCorteProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.optTodos)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.optConExistencia)
        Me.Name = "frmExistencias"
        Me.Text = "frmExistencias"
        Me.Controls.SetChildIndex(Me.optConExistencia, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.optTodos, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.chkCorteProveedor, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.chkCorteProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oGruposArticulos As VillarrealBusiness.clsGruposArticulos
    Private oProveedores As New VillarrealBusiness.clsProveedores

    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property

    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmEntradasAlmacen_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.ExistenciasNuevo(Departamento, Grupo, Me.optTodos.Checked, Proveedor, Me.cboTipo.Value, Me.chkCorteProveedor.Checked)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Existencias no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptExistencias
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Existencias", oReport, , , , True)
                oReport = Nothing

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub

    Private Sub frmEntradasAlmacen_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oReportes = New VillarrealBusiness.Reportes
        oBodegas = New VillarrealBusiness.clsBodegas
        oGruposArticulos = New VillarrealBusiness.clsGruposArticulos
    End Sub

    Private Sub frmEntradasAlmacen_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.Validacion(Action, Departamento, Grupo)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub

    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.LookupDepartamentosExistencias
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        Me.lkpGrupo.EditValue = Nothing
        lkpGrupo_LoadData(True)
    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGruposArticulos.LookupGruposExistencias(Me.Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

End Class
