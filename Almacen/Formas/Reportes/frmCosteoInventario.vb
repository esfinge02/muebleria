Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCosteoInventario
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cboMercancia As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkConcentrado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboOrdenamiento As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkSeries As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCosteoInventario))
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.chkSeries = New DevExpress.XtraEditors.CheckEdit
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboMercancia = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkConcentrado = New DevExpress.XtraEditors.CheckEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboOrdenamiento = New DevExpress.XtraEditors.ImageComboBoxEdit
        CType(Me.chkSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMercancia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkConcentrado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboOrdenamiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(120, 112)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(232, 20)
        'Me.lkpGrupo.StyleDisable = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
        '                Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 7
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(71, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "&Grupo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "&Departamento:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(120, 88)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(232, 20)
        'Me.lkpDepartamento.StyleDisable = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
        '                Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 5
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'chkSeries
        '
        Me.chkSeries.Location = New System.Drawing.Point(117, 208)
        Me.chkSeries.Name = "chkSeries"
        '
        'chkSeries.Properties
        '
        Me.chkSeries.Properties.Caption = "Mostrar &Series"
        Me.chkSeries.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSeries.Size = New System.Drawing.Size(99, 19)
        Me.chkSeries.TabIndex = 14
        Me.chkSeries.Tag = ""
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2007, 10, 12, 23, 58, 58, 0)
        Me.dteFecha.Location = New System.Drawing.Point(120, 136)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "E&xistencias al d�a:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboMercancia
        '
        Me.cboMercancia.EditValue = 1
        Me.cboMercancia.Location = New System.Drawing.Point(120, 160)
        Me.cboMercancia.Name = "cboMercancia"
        '
        'cboMercancia.Properties
        '
        Me.cboMercancia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboMercancia.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Toda", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nacional", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Importaci�n", 3, -1)})
        Me.cboMercancia.Size = New System.Drawing.Size(88, 20)
        Me.cboMercancia.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(51, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "&Mercanc�a:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkConcentrado
        '
        Me.chkConcentrado.Location = New System.Drawing.Point(117, 229)
        Me.chkConcentrado.Name = "chkConcentrado"
        '
        'chkConcentrado.Properties
        '
        Me.chkConcentrado.Properties.Caption = "Reporte Concentrado"
        Me.chkConcentrado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlText)
        Me.chkConcentrado.Size = New System.Drawing.Size(136, 19)
        Me.chkConcentrado.TabIndex = 15
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(120, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        'Me.lkpSucursal.StyleDisable = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
        'Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = ""
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(58, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(120, 64)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(232, 20)
        'Me.lkpBodega.StyleDisable = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
        '                Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 3
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(65, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Bodega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 186)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "&Ordenamiento:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboOrdenamiento
        '
        Me.cboOrdenamiento.EditValue = 1
        Me.cboOrdenamiento.Location = New System.Drawing.Point(120, 184)
        Me.cboOrdenamiento.Name = "cboOrdenamiento"
        '
        'cboOrdenamiento.Properties
        '
        Me.cboOrdenamiento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboOrdenamiento.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Departamento", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Grupo", 2, -1)})
        Me.cboOrdenamiento.Size = New System.Drawing.Size(104, 20)
        Me.cboOrdenamiento.TabIndex = 13
        '
        'frmCosteoInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(370, 256)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboOrdenamiento)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkConcentrado)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboMercancia)
        Me.Controls.Add(Me.chkSeries)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Name = "frmCosteoInventario"
        Me.Text = "frmCosteoInventario"
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.chkSeries, 0)
        Me.Controls.SetChildIndex(Me.cboMercancia, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.chkConcentrado, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboOrdenamiento, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.chkSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMercancia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkConcentrado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboOrdenamiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGruposArticulos As New VillarrealBusiness.clsGruposArticulos
    Private oSucursales As New VillarrealBusiness.clsSucursales

#End Region

#Region "Propiedades"

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Bodega() As String
        Get
            If Me.lkpBodega.EditValue Is Nothing Then
                Return "-1"
            Else
                Return CType(lkpBodega.EditValue, String)
            End If
        End Get
    End Property

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property

    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property

#End Region

#Region "Eventos de la Forma"
    Private Sub frmCosteoInventario_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.CosteoInventario(Me.Sucursal, Me.Bodega, Me.Departamento, Me.Grupo, Me.dteFecha.EditValue, Me.chkSeries.EditValue, Me.cboMercancia.EditValue, Me.cboOrdenamiento.Value)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Costeo de Inventario no se puede Mostrar")
        Else

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If Me.cboOrdenamiento.Value = 1 Then
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    If chkSeries.Checked = False Then
                        Dim oReport As New rptCosteoInventarioConcentrado
                        oReport.DataSource = oDataSet.Tables(0)
                        oReport.oData = oDataSet
                        oReport.blnConcentrado = chkConcentrado.CheckState

                        TINApp.ShowReport(Me.MdiParent, "Valor del Inventario", oReport)
                        oReport = Nothing
                    Else
                        Dim oReport1 As New rptCosteoInventarioDesglosado
                        oReport1.DataSource = oDataSet.Tables(0)
                        oReport1.oData = oDataSet
                        oReport1.blnConcentrado = chkConcentrado.CheckState

                        TINApp.ShowReport(Me.MdiParent, "Valor del Inventario Con Series", oReport1)
                        oReport1 = Nothing
                    End If
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            Else
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    If chkSeries.Checked = False Then
                        Dim oReport As New rptCosteoInventarioConcentradoGrupo
                        oReport.DataSource = oDataSet.Tables(0)
                        oReport.oData = oDataSet
                        oReport.blnConcentrado = chkConcentrado.CheckState

                        TINApp.ShowReport(Me.MdiParent, "Valor del Inventario", oReport)
                        oReport = Nothing
                    Else
                        Dim oReport1 As New rptCosteoInventarioDesglosadoGrupo
                        oReport1.DataSource = oDataSet.Tables(0)
                        oReport1.oData = oDataSet
                        oReport1.blnConcentrado = chkConcentrado.CheckState

                        TINApp.ShowReport(Me.MdiParent, "Valor del Inventario Con Series", oReport1)
                        oReport1 = Nothing
                    End If
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
            oDataSet = Nothing
        End If

    End Sub

    Private Sub frmCosteoInventario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dteFecha.EditValue = Now
        Me.lkpBodega.Enabled = False

    End Sub

    Private Sub frmCosteoInventario_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.ValidaFecha(Me.dteFecha.Text, True)
    End Sub
#End Region

#Region "Eventos de Controles"
#Region "Lookup"

#Region "Sucursal"
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega_LoadData(True)
        If Me.lkpSucursal.EditValue = Nothing Then
            Me.lkpBodega.Enabled = False
        Else
            Me.lkpBodega.Enabled = True
        End If
    End Sub

#End Region

#Region "Bodega"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup(sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
            Me.lkpBodega.SelectAll = True
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Departamento"
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub

    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        lkpGrupo_LoadData(True)
    End Sub
#End Region

#Region "Grupo"
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGruposArticulos.MultiLookup(Me.lkpDepartamento.ToXML)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            lkpGrupo.SelectAll = True
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region
#End Region

    Private Sub chkConcentrado_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkConcentrado.CheckedChanged
        If Me.chkConcentrado.Checked Then
            Me.chkSeries.Enabled = False
            Me.chkSeries.Checked = False
        Else
            Me.chkSeries.Enabled = True
        End If
    End Sub
#End Region



   
End Class
