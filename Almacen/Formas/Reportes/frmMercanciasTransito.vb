Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports DataDynamics.ActiveReports

Public Class frmMercanciasTransito
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMercanciasTransito))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lblTipo = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(17, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(16, 104)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(368, 56)
        Me.gpbFechas.TabIndex = 6
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(250, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Fin.TabIndex = 3
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(83, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Ini.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(202, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Has&ta: "
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(80, 72)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(232, 20)
        Me.lkpBodega.TabIndex = 5
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'lblBodega
        '
        Me.lblBodega.Location = New System.Drawing.Point(16, 72)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(54, 16)
        Me.lblBodega.TabIndex = 4
        Me.lblBodega.Text = "&Bodega: "
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(40, 48)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(32, 16)
        Me.lblTipo.TabIndex = 0
        Me.lblTipo.Text = "T&ipo:"
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = 1
        Me.cboTipo.Location = New System.Drawing.Point(80, 48)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mercanc�as en Tr�nsito", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mercanc�as por Entregar", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Vistas", 3, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Garant�as", 4, -1)})
        Me.cboTipo.Size = New System.Drawing.Size(160, 20)
        Me.cboTipo.TabIndex = 1
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 72)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.Visible = False
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 72)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        Me.lkpSucursal.TabIndex = 3
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        Me.lkpSucursal.Visible = False
        '
        'frmMercanciasTransito
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 176)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lkpBodega)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMercanciasTransito"
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.lblTipo, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupBodega(Me.lkpBodega)
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMercanciasTransito_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim sReporteNombre As String = String.Empty
        Select Case CInt(Me.cboTipo.Value)
            Case 1                  ' Mercancias en Tr�nsito
                Response = oReportes.MercanciasTransito(Bodega, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
                sReporteNombre = "Mercanc�as en Tr�nsito"
            Case 2                  ' Mercancias por Entregar
                Response = oReportes.MercanciaPendienteEntrega(Sucursal, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
                sReporteNombre = "Mercanc�as Pendientes de Entregar"
            Case 3                  ' Falta definir...
                ShowMessage(MessageType.MsgInformation, "El Reporte de Mercancias en Vistas no esta disponible todav�a.")
                sReporteNombre = "Mercanc�as en Vistas"
                Exit Sub
            Case 4                  ' Mercancias en Garant�a
                Response = oReportes.OrdenesServicio(Sucursal, "FE", Me.dteFecha_Ini.DateTime, Me.dteFecha_Fin.DateTime, String.Empty, String.Empty, String.Empty, String.Empty, "G", "X", False)
                sReporteNombre = "Mercanc�as en Garant�a"

        End Select

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de " & sReporteNombre & " no se puede Mostrar")
        Else
            Dim oReport As ActiveReport
            Try
                If CType(Response.Value, DataSet).Tables(0).Rows.Count > 0 Then

                    Select Case CInt(Me.cboTipo.EditValue)
                        Case 1          ' Mercancias en Tr�nsito
                            oReport = New rptMercanciaTransito
                            'Case 2           ' Mercancias por Entregar
                            '    oReport = New Comunes.rptMercanciaPendienteEntrega
                        Case 3          ' Falta definir...
                            oReport = New rptMercanciaTransito
                            'Case 4           ' Mercancias en Garant�a
                            '    oReport = New Comunes.rptOrdenesServicio
                    End Select
                    oReport.DataSource = CType(Response.Value, DataSet).Tables(0)
                    TINApp.ShowReport(Me.MdiParent, sReporteNombre, oReport)

                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

            Catch ex As Exception
                ShowMessage(MessageType.MsgError, "Error al intentar ejecutar el reporte de " & sReporteNombre, , ex)
            Finally
                oReport = Nothing
            End Try
        End If

    End Sub
    Private Sub frmMercanciasTransito_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaRangoFechas(Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
    End Sub
    Private Sub frmEntradasAlmacen_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oBodegas = New VillarrealBusiness.clsBodegas
        oSucursales = New VillarrealBusiness.clsSucursales
        Me.dteFecha_Ini.EditValue = Format(CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2)), "dd/MMM/yyyy")
        Me.dteFecha_Fin.EditValue = Format(CDate(TINApp.FechaServidor), "dd/MMM/yyyy")
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
#Region "Bodega"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Sucursal"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Tipo Reporte"
    Private Sub cboTipo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipo.EditValueChanged
        Select Case CInt(cboTipo.Value)
            Case 1              ' Mercancias en Tr�nsito
                Me.lblBodega.Visible = True
                Me.lkpBodega.Visible = True
                Me.lblSucursal.Visible = False
                Me.lkpSucursal.Visible = False
            Case 2              ' Mercancias por Entregar
                Me.lblBodega.Visible = False
                Me.lkpBodega.Visible = False
                Me.lblSucursal.Visible = True
                Me.lkpSucursal.Visible = True
            Case 3              ' Sin definir
                Me.lblBodega.Visible = False
                Me.lkpBodega.Visible = False
                Me.lblSucursal.Visible = False
                Me.lkpSucursal.Visible = False
            Case 4              ' Mercancias en Garant�a
                Me.lblBodega.Visible = False
                Me.lkpBodega.Visible = False
                Me.lblSucursal.Visible = False
                Me.lkpSucursal.Visible = False
        End Select
    End Sub
#End Region

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function ValidaRangoFechas(ByVal desde As DateTime, ByVal hasta As DateTime) As Events
        Dim oEvent As New Events
        If desde > hasta Then
            oEvent.Ex = Nothing
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            oEvent.Message = "El rango de fechas es incorrecto. La fecha inicial supera a la fecha final."
        End If
        Return oEvent
    End Function
#End Region

    
End Class
