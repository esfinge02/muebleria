Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmExistenciasMultiple
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents optTodos As System.Windows.Forms.RadioButton
    Friend WithEvents optConExistencia As System.Windows.Forms.RadioButton
    Friend WithEvents chkInlcuirPrecios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkInlcuirCostos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIncluirExistencias As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkInlcuirSeries As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lkpVendedor As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExistenciasMultiple))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.optTodos = New System.Windows.Forms.RadioButton
        Me.optConExistencia = New System.Windows.Forms.RadioButton
        Me.chkInlcuirPrecios = New DevExpress.XtraEditors.CheckEdit
        Me.chkInlcuirCostos = New DevExpress.XtraEditors.CheckEdit
        Me.chkIncluirExistencias = New DevExpress.XtraEditors.CheckEdit
        Me.chkInlcuirSeries = New DevExpress.XtraEditors.CheckEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lkpVendedor = New Dipros.Editors.TINMultiLookup
        CType(Me.chkInlcuirPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkInlcuirCostos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIncluirExistencias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkInlcuirSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(53, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(117, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(117, 112)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(232, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 7
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(66, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "&Grupo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(21, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "&Departamento:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(117, 88)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(232, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 5
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(117, 64)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(232, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 3
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Bodega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(117, 136)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(400, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(232, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 9
        Me.lkpProveedor.Tag = "proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(44, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Pr&oveedor:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'optTodos
        '
        Me.optTodos.Checked = True
        Me.optTodos.Location = New System.Drawing.Point(69, 294)
        Me.optTodos.Name = "optTodos"
        Me.optTodos.Size = New System.Drawing.Size(136, 24)
        Me.optTodos.TabIndex = 16
        Me.optTodos.TabStop = True
        Me.optTodos.Text = "&Todos los Art�culos"
        '
        'optConExistencia
        '
        Me.optConExistencia.Location = New System.Drawing.Point(221, 294)
        Me.optConExistencia.Name = "optConExistencia"
        Me.optConExistencia.Size = New System.Drawing.Size(112, 24)
        Me.optConExistencia.TabIndex = 17
        Me.optConExistencia.Text = "Con &Existencia"
        '
        'chkInlcuirPrecios
        '
        Me.chkInlcuirPrecios.EditValue = True
        Me.chkInlcuirPrecios.Location = New System.Drawing.Point(118, 190)
        Me.chkInlcuirPrecios.Name = "chkInlcuirPrecios"
        '
        'chkInlcuirPrecios.Properties
        '
        Me.chkInlcuirPrecios.Properties.Caption = "Inlcuir Precios"
        Me.chkInlcuirPrecios.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkInlcuirPrecios.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkInlcuirPrecios.Size = New System.Drawing.Size(160, 22)
        Me.chkInlcuirPrecios.TabIndex = 12
        '
        'chkInlcuirCostos
        '
        Me.chkInlcuirCostos.EditValue = True
        Me.chkInlcuirCostos.Location = New System.Drawing.Point(118, 214)
        Me.chkInlcuirCostos.Name = "chkInlcuirCostos"
        '
        'chkInlcuirCostos.Properties
        '
        Me.chkInlcuirCostos.Properties.Caption = "Inlcuir Costos"
        Me.chkInlcuirCostos.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkInlcuirCostos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkInlcuirCostos.Size = New System.Drawing.Size(160, 22)
        Me.chkInlcuirCostos.TabIndex = 13
        '
        'chkIncluirExistencias
        '
        Me.chkIncluirExistencias.EditValue = True
        Me.chkIncluirExistencias.Location = New System.Drawing.Point(118, 238)
        Me.chkIncluirExistencias.Name = "chkIncluirExistencias"
        '
        'chkIncluirExistencias.Properties
        '
        Me.chkIncluirExistencias.Properties.Caption = "Incluir Existencias"
        Me.chkIncluirExistencias.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkIncluirExistencias.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkIncluirExistencias.Size = New System.Drawing.Size(160, 22)
        Me.chkIncluirExistencias.TabIndex = 14
        '
        'chkInlcuirSeries
        '
        Me.chkInlcuirSeries.EditValue = True
        Me.chkInlcuirSeries.Location = New System.Drawing.Point(118, 262)
        Me.chkInlcuirSeries.Name = "chkInlcuirSeries"
        '
        'chkInlcuirSeries.Properties
        '
        Me.chkInlcuirSeries.Properties.Caption = "Incluir Series"
        Me.chkInlcuirSeries.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkInlcuirSeries.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkInlcuirSeries.Size = New System.Drawing.Size(160, 22)
        Me.chkInlcuirSeries.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 326)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(336, 16)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Este Proceso Puede Tardar Algunos Minutos"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(48, 160)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 10
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ve&ndedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVendedor
        '
        Me.lkpVendedor.AllowAdd = False
        Me.lkpVendedor.AutoReaload = False
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.DefaultSearchField = ""
        Me.lkpVendedor.DisplayMember = "nombre"
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor.Filtered = False
        Me.lkpVendedor.InitValue = Nothing
        Me.lkpVendedor.Location = New System.Drawing.Point(117, 160)
        Me.lkpVendedor.MultiSelect = False
        Me.lkpVendedor.Name = "lkpVendedor"
        Me.lkpVendedor.NullText = "(Todos)"
        Me.lkpVendedor.PopupWidth = CType(420, Long)
        Me.lkpVendedor.ReadOnlyControl = False
        Me.lkpVendedor.Required = False
        Me.lkpVendedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVendedor.SearchMember = ""
        Me.lkpVendedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVendedor.SelectAll = False
        Me.lkpVendedor.Size = New System.Drawing.Size(232, 20)
        Me.lkpVendedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVendedor.TabIndex = 11
        Me.lkpVendedor.Tag = "vendedor"
        Me.lkpVendedor.ToolTip = Nothing
        Me.lkpVendedor.ValueMember = "Vendedor"
        '
        'frmExistenciasMultiple
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(370, 352)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lkpVendedor)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkInlcuirSeries)
        Me.Controls.Add(Me.chkIncluirExistencias)
        Me.Controls.Add(Me.chkInlcuirCostos)
        Me.Controls.Add(Me.chkInlcuirPrecios)
        Me.Controls.Add(Me.optTodos)
        Me.Controls.Add(Me.optConExistencia)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmExistenciasMultiple"
        Me.Text = "frmExistenciasMultiple"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.optConExistencia, 0)
        Me.Controls.SetChildIndex(Me.optTodos, 0)
        Me.Controls.SetChildIndex(Me.chkInlcuirPrecios, 0)
        Me.Controls.SetChildIndex(Me.chkInlcuirCostos, 0)
        Me.Controls.SetChildIndex(Me.chkIncluirExistencias, 0)
        Me.Controls.SetChildIndex(Me.chkInlcuirSeries, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lkpVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        CType(Me.chkInlcuirPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkInlcuirCostos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIncluirExistencias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkInlcuirSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private oReportes As New VillarrealBusiness.Reportes
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGruposArticulos As New VillarrealBusiness.clsGruposArticulos
    Private oProveedores As New VillarrealBusiness.clsProveedores
    Private oVendedores As New VillarrealBusiness.clsVendedores
#End Region

#Region "Propiedades"
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupBodega(Me.lkpBodega)
        End Get
    End Property

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property

    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property

    'Private ReadOnly Property Sucursal() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpSucursal.ToXML, "Sucursal")
    '    End Get
    'End Property

    'Private ReadOnly Property Bodega() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpBodega.ToXML, "Bodega")
    '    End Get
    'End Property


    'Private ReadOnly Property Departamento() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpDepartamento.ToXML, "Departamento")
    '    End Get
    'End Property

    'Private ReadOnly Property Grupo() As Events
    '    Get
    '        Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpGrupo.ToXML, "Grupo")
    '    End Get
    'End Property

    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

    Private ReadOnly Property Vendedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpVendedor)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"

    Private Sub frmExistenciasMultiple_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.ExistenciasMultipleNuevo(Me.Sucursal, Me.Bodega, Me.Departamento, Me.Grupo, Me.Proveedor, Me.optTodos.Checked, Me.chkInlcuirPrecios.Checked, Me.chkInlcuirCostos.Checked, Me.chkIncluirExistencias.Checked, Me.chkInlcuirSeries.Checked, Vendedor)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Existencias no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptExistenciasMultiple
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)

               
                TINApp.ShowReport(Me.MdiParent, "Resumen de Existencias", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

#End Region

#Region "Eventos de Controles"
#Region "Sucursal"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.LookupExistenciasSucursales()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lkpBodega.EditValue = Nothing
        lkpBodega_LoadData(True)
    End Sub
#End Region

#Region "Bodega"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasExistencias(Me.Sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
            Me.lkpBodega.SelectAll = True
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Departamento"
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub

    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.LookupDepartamentosExistencias
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        Me.lkpGrupo.EditValue = Nothing
        lkpGrupo_LoadData(True)
    End Sub
#End Region

#Region "Grupo"
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        'Response = oGruposArticulos(Me.Departamento)
        Response = oGruposArticulos.LookupGruposExistencias(Me.Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
            Me.lkpGrupo.SelectAll = True
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Proveedores"
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub

    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.LookupExistenciasProveedores
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#Region "Vendedores"
    Private Sub lkpVendedor_Format() Handles lkpVendedor.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpVendedor)
    End Sub
    Private Sub lkpVendedor_LoadData(ByVal Initialize As Boolean) Handles lkpVendedor.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVendedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#End Region


End Class
