Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatDepartamentos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatGruposArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatConceptosInventario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatUnidades As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatProveedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEmpleados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProEntradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatSucursales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProOrdenesCompra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosInventarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepPendientesArribar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEntradasAlmacen As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTraspasos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProDevolucionesProveedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMovimientos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTraspasosEntradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepCosteoInventario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosQuedados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenSalidas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSalidasPorTienda As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMovimientosxReferencia As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenProveedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatUsuariosAlmacen As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatBodegas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProEntradasSurtimiento As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatUbicaciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProInventarioFisicoDepto As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuProInventarioFisicoUbicacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepStocksArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEstadisticaVenta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepTraspasos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenEntradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProSolicitudTraspasos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatPrecios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarMdiChildrenListItem1 As DevExpress.XtraBars.BarMdiChildrenListItem
    Friend WithEvents BarListItem1 As DevExpress.XtraBars.BarListItem
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepExistenciasAlmacen As DevExpress.XtraBars.BarButtonItem


    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem3 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuProCambioCancelacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepArticulosPedidosFabrica As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSeriesArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepExistencias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMercanciaTransito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepCambiosPrecios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepFletesOtrosCargos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProVistasSalidas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProVistasEntradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubVistas As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuRepMercanciaEtiquetada As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMercanciaVistas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProEntradasSalidasReparacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiferenciasPedidosFabrica As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiferenciaCostosDevProveedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDevolucionesProveedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatIdentificadoresPermisos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHerrPermisosExtendidos As DevExpress.XtraBars.BarButtonItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatDepartamentos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatGruposArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatConceptosInventario = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatUnidades = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatProveedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEmpleados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProEntradas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatSucursales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProOrdenesCompra = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosInventarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepPendientesArribar = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEntradasAlmacen = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTraspasos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProDevolucionesProveedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepExistenciasAlmacen = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMovimientos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTraspasosEntradas = New DevExpress.XtraBars.BarButtonItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepCosteoInventario = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosQuedados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenSalidas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSalidasPorTienda = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMovimientosxReferencia = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenProveedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatUsuariosAlmacen = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatBodegas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProEntradasSurtimiento = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatUbicaciones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProInventarioFisicoDepto = New DevExpress.XtraBars.BarButtonItem
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        Me.mnuProInventarioFisicoUbicacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepStocksArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEstadisticaVenta = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepTraspasos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenEntradas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProSolicitudTraspasos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatPrecios = New DevExpress.XtraBars.BarButtonItem
        Me.BarMdiChildrenListItem1 = New DevExpress.XtraBars.BarMdiChildrenListItem
        Me.BarListItem1 = New DevExpress.XtraBars.BarListItem
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.Bar2 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem3 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuProCambioCancelacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepArticulosPedidosFabrica = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSeriesArticulos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepExistencias = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMercanciaTransito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepCambiosPrecios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepFletesOtrosCargos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProVistasSalidas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProVistasEntradas = New DevExpress.XtraBars.BarButtonItem
        Me.BarSubVistas = New DevExpress.XtraBars.BarSubItem
        Me.mnuRepMercanciaEtiquetada = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMercanciaVistas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProEntradasSalidasReparacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiferenciasPedidosFabrica = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiferenciaCostosDevProveedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDevolucionesProveedor = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatIdentificadoresPermisos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuHerrPermisosExtendidos = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatDepartamentos, Me.mnuCatGruposArticulos, Me.mnuCatConceptosInventario, Me.mnuCatUnidades, Me.mnuCatProveedores, Me.mnuCatArticulos, Me.mnuCatEmpleados, Me.mnuProEntradas, Me.mnuCatSucursales, Me.mnuProOrdenesCompra, Me.mnuProMovimientosInventarios, Me.mnuRepPendientesArribar, Me.mnuRepEntradasAlmacen, Me.mnuProTraspasos, Me.mnuProDevolucionesProveedor, Me.mnuRepExistenciasAlmacen, Me.mnuRepMovimientos, Me.mnuProTraspasosEntradas, Me.BarButtonItem1, Me.mnuRepCosteoInventario, Me.mnuRepArticulosQuedados, Me.mnuRepResumenSalidas, Me.mnuRepResumenProveedor, Me.mnuCatUsuariosAlmacen, Me.mnuCatBodegas, Me.mnuProEntradasSurtimiento, Me.mnuProMovimientosArticulos, Me.mnuCatUbicaciones, Me.mnuProInventarioFisicoDepto, Me.BarSubItem1, Me.mnuProInventarioFisicoUbicacion, Me.mnuRepStocksArticulos, Me.mnuRepEstadisticaVenta, Me.mnuRepTraspasos, Me.mnuRepResumenEntradas, Me.mnuRepSalidasPorTienda, Me.mnuRepMovimientosxReferencia, Me.mnuProSolicitudTraspasos, Me.mnuCatPrecios, Me.BarMdiChildrenListItem1, Me.BarListItem1, Me.BarStaticItem1, Me.BarStaticItem3, Me.mnuProCambioCancelacion, Me.mnuRepArticulosPedidosFabrica, Me.mnuRepSeriesArticulos, Me.mnuRepExistencias, Me.mnuRepMercanciaTransito, Me.mnuRepCambiosPrecios, Me.mnuRepFletesOtrosCargos, Me.mnuProVistasSalidas, Me.mnuProVistasEntradas, Me.BarSubVistas, Me.mnuRepMercanciaEtiquetada, Me.mnuRepMercanciaVistas, Me.mnuProEntradasSalidasReparacion, Me.mnuRepDiferenciasPedidosFabrica, Me.mnuRepDiferenciaCostosDevProveedor, Me.mnuRepDevolucionesProveedor, Me.mnuCatIdentificadoresPermisos, Me.mnuHerrPermisosExtendidos})
        Me.BarManager.MaxItemId = 180
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerrPermisosExtendidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatSucursales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatBodegas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatUbicaciones), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatDepartamentos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatGruposArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConceptosInventario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatUnidades), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatProveedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEmpleados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatPrecios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatIdentificadoresPermisos)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProOrdenesCompra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProEntradas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosInventarios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProSolicitudTraspasos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTraspasos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTraspasosEntradas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDevolucionesProveedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProEntradasSurtimiento), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambioCancelacion), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubVistas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProEntradasSalidasReparacion)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMercanciaEtiquetada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepPendientesArribar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEntradasAlmacen), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepExistencias), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepExistenciasAlmacen), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMovimientos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepCosteoInventario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosQuedados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenSalidas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenProveedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepStocksArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEstadisticaVenta), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDevolucionesProveedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMercanciaVistas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiferenciaCostosDevProveedor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiferenciasPedidosFabrica), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepTraspasos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepCambiosPrecios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSeriesArticulos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMercanciaTransito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenEntradas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepArticulosPedidosFabrica), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepFletesOtrosCargos)})
        '
        'mnuCatDepartamentos
        '
        Me.mnuCatDepartamentos.Caption = "Depar&tamentos"
        Me.mnuCatDepartamentos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatDepartamentos.Id = 105
        Me.mnuCatDepartamentos.ImageIndex = 21
        Me.mnuCatDepartamentos.Name = "mnuCatDepartamentos"
        '
        'mnuCatGruposArticulos
        '
        Me.mnuCatGruposArticulos.Caption = "&Grupos de Art�culos"
        Me.mnuCatGruposArticulos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatGruposArticulos.Id = 106
        Me.mnuCatGruposArticulos.ImageIndex = 2
        Me.mnuCatGruposArticulos.Name = "mnuCatGruposArticulos"
        '
        'mnuCatConceptosInventario
        '
        Me.mnuCatConceptosInventario.Caption = "Conceptos de &Inventarios"
        Me.mnuCatConceptosInventario.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConceptosInventario.Id = 107
        Me.mnuCatConceptosInventario.ImageIndex = 1
        Me.mnuCatConceptosInventario.Name = "mnuCatConceptosInventario"
        '
        'mnuCatUnidades
        '
        Me.mnuCatUnidades.Caption = "U&nidades de Medida"
        Me.mnuCatUnidades.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatUnidades.Id = 108
        Me.mnuCatUnidades.ImageIndex = 3
        Me.mnuCatUnidades.Name = "mnuCatUnidades"
        '
        'mnuCatProveedores
        '
        Me.mnuCatProveedores.Caption = "Pr&oveedores"
        Me.mnuCatProveedores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatProveedores.Id = 109
        Me.mnuCatProveedores.ImageIndex = 4
        Me.mnuCatProveedores.Name = "mnuCatProveedores"
        '
        'mnuCatArticulos
        '
        Me.mnuCatArticulos.Caption = "Art�cu&los"
        Me.mnuCatArticulos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatArticulos.Id = 110
        Me.mnuCatArticulos.ImageIndex = 0
        Me.mnuCatArticulos.Name = "mnuCatArticulos"
        '
        'mnuCatEmpleados
        '
        Me.mnuCatEmpleados.Caption = "Emplea&dos"
        Me.mnuCatEmpleados.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEmpleados.Id = 111
        Me.mnuCatEmpleados.ImageIndex = 22
        Me.mnuCatEmpleados.Name = "mnuCatEmpleados"
        '
        'mnuProEntradas
        '
        Me.mnuProEntradas.Caption = "&Entradas al Almac�n"
        Me.mnuProEntradas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProEntradas.Id = 112
        Me.mnuProEntradas.ImageIndex = 19
        Me.mnuProEntradas.Name = "mnuProEntradas"
        '
        'mnuCatSucursales
        '
        Me.mnuCatSucursales.Caption = "&Sucursales"
        Me.mnuCatSucursales.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatSucursales.Id = 113
        Me.mnuCatSucursales.ImageIndex = 5
        Me.mnuCatSucursales.Name = "mnuCatSucursales"
        '
        'mnuProOrdenesCompra
        '
        Me.mnuProOrdenesCompra.Caption = "�rdenes de C&ompra"
        Me.mnuProOrdenesCompra.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProOrdenesCompra.Id = 114
        Me.mnuProOrdenesCompra.ImageIndex = 6
        Me.mnuProOrdenesCompra.Name = "mnuProOrdenesCompra"
        '
        'mnuProMovimientosInventarios
        '
        Me.mnuProMovimientosInventarios.Caption = "&Movimientos al Inventario"
        Me.mnuProMovimientosInventarios.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProMovimientosInventarios.Id = 116
        Me.mnuProMovimientosInventarios.ImageIndex = 8
        Me.mnuProMovimientosInventarios.Name = "mnuProMovimientosInventarios"
        '
        'mnuRepPendientesArribar
        '
        Me.mnuRepPendientesArribar.Caption = "Pend&ientes de Arribar"
        Me.mnuRepPendientesArribar.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepPendientesArribar.Id = 117
        Me.mnuRepPendientesArribar.ImageIndex = 10
        Me.mnuRepPendientesArribar.Name = "mnuRepPendientesArribar"
        '
        'mnuRepEntradasAlmacen
        '
        Me.mnuRepEntradasAlmacen.Caption = "Entra&das  al  Almac�n"
        Me.mnuRepEntradasAlmacen.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEntradasAlmacen.Id = 118
        Me.mnuRepEntradasAlmacen.ImageIndex = 11
        Me.mnuRepEntradasAlmacen.Name = "mnuRepEntradasAlmacen"
        '
        'mnuProTraspasos
        '
        Me.mnuProTraspasos.Caption = "Sa&lidas de Traspasos"
        Me.mnuProTraspasos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProTraspasos.Id = 119
        Me.mnuProTraspasos.ImageIndex = 9
        Me.mnuProTraspasos.Name = "mnuProTraspasos"
        '
        'mnuProDevolucionesProveedor
        '
        Me.mnuProDevolucionesProveedor.Caption = "&Devoluciones al Proveedor"
        Me.mnuProDevolucionesProveedor.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDevolucionesProveedor.Id = 120
        Me.mnuProDevolucionesProveedor.ImageIndex = 7
        Me.mnuProDevolucionesProveedor.Name = "mnuProDevolucionesProveedor"
        '
        'mnuRepExistenciasAlmacen
        '
        Me.mnuRepExistenciasAlmacen.Caption = "E&xistencias por Almac�n"
        Me.mnuRepExistenciasAlmacen.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepExistenciasAlmacen.Id = 123
        Me.mnuRepExistenciasAlmacen.ImageIndex = 12
        Me.mnuRepExistenciasAlmacen.Name = "mnuRepExistenciasAlmacen"
        '
        'mnuRepMovimientos
        '
        Me.mnuRepMovimientos.Caption = "&Movimientos"
        Me.mnuRepMovimientos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMovimientos.Id = 124
        Me.mnuRepMovimientos.ImageIndex = 13
        Me.mnuRepMovimientos.Name = "mnuRepMovimientos"
        '
        'mnuProTraspasosEntradas
        '
        Me.mnuProTraspasosEntradas.Caption = "E&ntradas de Traspasos"
        Me.mnuProTraspasosEntradas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProTraspasosEntradas.Id = 125
        Me.mnuProTraspasosEntradas.ImageIndex = 20
        Me.mnuProTraspasosEntradas.Name = "mnuProTraspasosEntradas"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 126
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'mnuRepCosteoInventario
        '
        Me.mnuRepCosteoInventario.Caption = "Va&lor del Inventario"
        Me.mnuRepCosteoInventario.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepCosteoInventario.Id = 127
        Me.mnuRepCosteoInventario.ImageIndex = 14
        Me.mnuRepCosteoInventario.Name = "mnuRepCosteoInventario"
        '
        'mnuRepArticulosQuedados
        '
        Me.mnuRepArticulosQuedados.Caption = "Art�culos &Quedados"
        Me.mnuRepArticulosQuedados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosQuedados.Id = 128
        Me.mnuRepArticulosQuedados.ImageIndex = 15
        Me.mnuRepArticulosQuedados.Name = "mnuRepArticulosQuedados"
        '
        'mnuRepResumenSalidas
        '
        Me.mnuRepResumenSalidas.Caption = "Resumen de &Salidas"
        Me.mnuRepResumenSalidas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenSalidas.Id = 129
        Me.mnuRepResumenSalidas.ImageIndex = 16
        Me.mnuRepResumenSalidas.Name = "mnuRepResumenSalidas"
        '
        'mnuRepSalidasPorTienda
        '
        Me.mnuRepSalidasPorTienda.Caption = "Sa&lidas por Tienda"
        Me.mnuRepSalidasPorTienda.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSalidasPorTienda.Id = 130
        Me.mnuRepSalidasPorTienda.ImageIndex = 17
        Me.mnuRepSalidasPorTienda.Name = "mnuRepSalidasPorTienda"
        '
        'mnuRepMovimientosxReferencia
        '
        Me.mnuRepMovimientosxReferencia.Caption = "Movimientos p&or Referencia"
        Me.mnuRepMovimientosxReferencia.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMovimientosxReferencia.Id = 131
        Me.mnuRepMovimientosxReferencia.ImageIndex = 23
        Me.mnuRepMovimientosxReferencia.Name = "mnuRepMovimientosxReferencia"
        '
        'mnuRepResumenProveedor
        '
        Me.mnuRepResumenProveedor.Caption = "Res&umen de Proveedor"
        Me.mnuRepResumenProveedor.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenProveedor.Id = 132
        Me.mnuRepResumenProveedor.ImageIndex = 18
        Me.mnuRepResumenProveedor.Name = "mnuRepResumenProveedor"
        '
        'mnuCatUsuariosAlmacen
        '
        Me.mnuCatUsuariosAlmacen.Caption = "Usuarios"
        Me.mnuCatUsuariosAlmacen.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatUsuariosAlmacen.Id = 135
        Me.mnuCatUsuariosAlmacen.Name = "mnuCatUsuariosAlmacen"
        '
        'mnuCatBodegas
        '
        Me.mnuCatBodegas.Caption = "&Bodegas"
        Me.mnuCatBodegas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatBodegas.Id = 136
        Me.mnuCatBodegas.ImageIndex = 24
        Me.mnuCatBodegas.Name = "mnuCatBodegas"
        '
        'mnuProEntradasSurtimiento
        '
        Me.mnuProEntradasSurtimiento.Caption = "Cap&tura de Series"
        Me.mnuProEntradasSurtimiento.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProEntradasSurtimiento.Id = 137
        Me.mnuProEntradasSurtimiento.ImageIndex = 25
        Me.mnuProEntradasSurtimiento.Name = "mnuProEntradasSurtimiento"
        '
        'mnuProMovimientosArticulos
        '
        Me.mnuProMovimientosArticulos.Caption = "&Movimientos de Art�culos"
        Me.mnuProMovimientosArticulos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosArticulos.Id = 139
        Me.mnuProMovimientosArticulos.ImageIndex = 27
        Me.mnuProMovimientosArticulos.Name = "mnuProMovimientosArticulos"
        '
        'mnuCatUbicaciones
        '
        Me.mnuCatUbicaciones.Caption = "&Ubicaciones"
        Me.mnuCatUbicaciones.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatUbicaciones.Id = 140
        Me.mnuCatUbicaciones.ImageIndex = 28
        Me.mnuCatUbicaciones.Name = "mnuCatUbicaciones"
        '
        'mnuProInventarioFisicoDepto
        '
        Me.mnuProInventarioFisicoDepto.Caption = "Por &Departamento"
        Me.mnuProInventarioFisicoDepto.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProInventarioFisicoDepto.Id = 141
        Me.mnuProInventarioFisicoDepto.ImageIndex = 29
        Me.mnuProInventarioFisicoDepto.Name = "mnuProInventarioFisicoDepto"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "&Inventario F�sico"
        Me.BarSubItem1.Id = 142
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProInventarioFisicoDepto), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProInventarioFisicoUbicacion)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'mnuProInventarioFisicoUbicacion
        '
        Me.mnuProInventarioFisicoUbicacion.Caption = "Por &Ubicaci�n"
        Me.mnuProInventarioFisicoUbicacion.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProInventarioFisicoUbicacion.Id = 143
        Me.mnuProInventarioFisicoUbicacion.ImageIndex = 30
        Me.mnuProInventarioFisicoUbicacion.Name = "mnuProInventarioFisicoUbicacion"
        '
        'mnuRepStocksArticulos
        '
        Me.mnuRepStocksArticulos.Caption = "Stoc&ks de Art�culos"
        Me.mnuRepStocksArticulos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepStocksArticulos.Id = 144
        Me.mnuRepStocksArticulos.ImageIndex = 31
        Me.mnuRepStocksArticulos.Name = "mnuRepStocksArticulos"
        '
        'mnuRepEstadisticaVenta
        '
        Me.mnuRepEstadisticaVenta.Caption = "Estad�sticas de Ve&nta"
        Me.mnuRepEstadisticaVenta.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEstadisticaVenta.Id = 145
        Me.mnuRepEstadisticaVenta.ImageIndex = 32
        Me.mnuRepEstadisticaVenta.Name = "mnuRepEstadisticaVenta"
        '
        'mnuRepTraspasos
        '
        Me.mnuRepTraspasos.Caption = "&Traspasos"
        Me.mnuRepTraspasos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepTraspasos.Id = 146
        Me.mnuRepTraspasos.ImageIndex = 33
        Me.mnuRepTraspasos.Name = "mnuRepTraspasos"
        '
        'mnuRepResumenEntradas
        '
        Me.mnuRepResumenEntradas.Caption = "Resumen de Entradas"
        Me.mnuRepResumenEntradas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenEntradas.Id = 147
        Me.mnuRepResumenEntradas.ImageIndex = 35
        Me.mnuRepResumenEntradas.Name = "mnuRepResumenEntradas"
        '
        'mnuProSolicitudTraspasos
        '
        Me.mnuProSolicitudTraspasos.Caption = "&Solicitud de Traspasos"
        Me.mnuProSolicitudTraspasos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProSolicitudTraspasos.Id = 148
        Me.mnuProSolicitudTraspasos.ImageIndex = 34
        Me.mnuProSolicitudTraspasos.Name = "mnuProSolicitudTraspasos"
        '
        'mnuCatPrecios
        '
        Me.mnuCatPrecios.Caption = "Pr&ecios"
        Me.mnuCatPrecios.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatPrecios.Id = 149
        Me.mnuCatPrecios.ImageIndex = 36
        Me.mnuCatPrecios.Name = "mnuCatPrecios"
        '
        'BarMdiChildrenListItem1
        '
        Me.BarMdiChildrenListItem1.Caption = "BarMdiChildrenListItem1"
        Me.BarMdiChildrenListItem1.Id = 150
        Me.BarMdiChildrenListItem1.Name = "BarMdiChildrenListItem1"
        '
        'BarListItem1
        '
        Me.BarListItem1.Caption = "BarListItem1"
        Me.BarListItem1.Id = 151
        Me.BarListItem1.Name = "BarListItem1"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 152
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'Bar2
        '
        Me.Bar2.BarName = "Sucursal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 1
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem3)})
        Me.Bar2.Offset = 1
        Me.Bar2.Text = "Sucursal"
        '
        'BarStaticItem3
        '
        Me.BarStaticItem3.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem3.Caption = "BarStaticItem3"
        Me.BarStaticItem3.Id = 156
        Me.BarStaticItem3.Name = "BarStaticItem3"
        Me.BarStaticItem3.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem3.Width = 32
        '
        'mnuProCambioCancelacion
        '
        Me.mnuProCambioCancelacion.Caption = "Cambios en Cancelaciones"
        Me.mnuProCambioCancelacion.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambioCancelacion.Id = 161
        Me.mnuProCambioCancelacion.ImageIndex = 38
        Me.mnuProCambioCancelacion.Name = "mnuProCambioCancelacion"
        '
        'mnuRepArticulosPedidosFabrica
        '
        Me.mnuRepArticulosPedidosFabrica.Caption = "Art�culos Pedidos a &F�brica"
        Me.mnuRepArticulosPedidosFabrica.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepArticulosPedidosFabrica.Id = 162
        Me.mnuRepArticulosPedidosFabrica.ImageIndex = 39
        Me.mnuRepArticulosPedidosFabrica.Name = "mnuRepArticulosPedidosFabrica"
        '
        'mnuRepSeriesArticulos
        '
        Me.mnuRepSeriesArticulos.Caption = "Series de Art�culos"
        Me.mnuRepSeriesArticulos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSeriesArticulos.Id = 163
        Me.mnuRepSeriesArticulos.ImageIndex = 40
        Me.mnuRepSeriesArticulos.Name = "mnuRepSeriesArticulos"
        '
        'mnuRepExistencias
        '
        Me.mnuRepExistencias.Caption = "&Existencias"
        Me.mnuRepExistencias.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepExistencias.Id = 164
        Me.mnuRepExistencias.ImageIndex = 12
        Me.mnuRepExistencias.Name = "mnuRepExistencias"
        '
        'mnuRepMercanciaTransito
        '
        Me.mnuRepMercanciaTransito.Caption = "Sucesos"
        Me.mnuRepMercanciaTransito.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMercanciaTransito.Id = 165
        Me.mnuRepMercanciaTransito.ImageIndex = 43
        Me.mnuRepMercanciaTransito.Name = "mnuRepMercanciaTransito"
        '
        'mnuRepCambiosPrecios
        '
        Me.mnuRepCambiosPrecios.Caption = "Cam&bios de Precios"
        Me.mnuRepCambiosPrecios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepCambiosPrecios.Id = 166
        Me.mnuRepCambiosPrecios.ImageIndex = 41
        Me.mnuRepCambiosPrecios.Name = "mnuRepCambiosPrecios"
        '
        'mnuRepFletesOtrosCargos
        '
        Me.mnuRepFletesOtrosCargos.Caption = "Fletes y &Otros Cargos"
        Me.mnuRepFletesOtrosCargos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepFletesOtrosCargos.Id = 167
        Me.mnuRepFletesOtrosCargos.ImageIndex = 42
        Me.mnuRepFletesOtrosCargos.Name = "mnuRepFletesOtrosCargos"
        '
        'mnuProVistasSalidas
        '
        Me.mnuProVistasSalidas.Caption = "Salida por Vistas"
        Me.mnuProVistasSalidas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProVistasSalidas.Id = 168
        Me.mnuProVistasSalidas.ImageIndex = 45
        Me.mnuProVistasSalidas.Name = "mnuProVistasSalidas"
        '
        'mnuProVistasEntradas
        '
        Me.mnuProVistasEntradas.Caption = "Entradas por Vistas"
        Me.mnuProVistasEntradas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProVistasEntradas.Id = 169
        Me.mnuProVistasEntradas.ImageIndex = 44
        Me.mnuProVistasEntradas.Name = "mnuProVistasEntradas"
        '
        'BarSubVistas
        '
        Me.BarSubVistas.Caption = "Mercancia en Vistas"
        Me.BarSubVistas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.BarSubVistas.Id = 170
        Me.BarSubVistas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProVistasSalidas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProVistasEntradas)})
        Me.BarSubVistas.Name = "BarSubVistas"
        '
        'mnuRepMercanciaEtiquetada
        '
        Me.mnuRepMercanciaEtiquetada.Caption = "Mercanc�a Etiquetada"
        Me.mnuRepMercanciaEtiquetada.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMercanciaEtiquetada.Id = 171
        Me.mnuRepMercanciaEtiquetada.Name = "mnuRepMercanciaEtiquetada"
        '
        'mnuRepMercanciaVistas
        '
        Me.mnuRepMercanciaVistas.Caption = "Mercancia en Vistas"
        Me.mnuRepMercanciaVistas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMercanciaVistas.Id = 172
        Me.mnuRepMercanciaVistas.Name = "mnuRepMercanciaVistas"
        '
        'mnuProEntradasSalidasReparacion
        '
        Me.mnuProEntradasSalidasReparacion.Caption = "Entradas y Salidas por Reparaci�n"
        Me.mnuProEntradasSalidasReparacion.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProEntradasSalidasReparacion.Id = 174
        Me.mnuProEntradasSalidasReparacion.Name = "mnuProEntradasSalidasReparacion"
        '
        'mnuRepDiferenciasPedidosFabrica
        '
        Me.mnuRepDiferenciasPedidosFabrica.Caption = "Periodo Asignaci�n de Costo"
        Me.mnuRepDiferenciasPedidosFabrica.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiferenciasPedidosFabrica.Id = 175
        Me.mnuRepDiferenciasPedidosFabrica.Name = "mnuRepDiferenciasPedidosFabrica"
        '
        'mnuRepDiferenciaCostosDevProveedor
        '
        Me.mnuRepDiferenciaCostosDevProveedor.Caption = "Diferencias en Costos x Dev. Proveedor"
        Me.mnuRepDiferenciaCostosDevProveedor.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiferenciaCostosDevProveedor.Id = 176
        Me.mnuRepDiferenciaCostosDevProveedor.Name = "mnuRepDiferenciaCostosDevProveedor"
        '
        'mnuRepDevolucionesProveedor
        '
        Me.mnuRepDevolucionesProveedor.Caption = "Devoluciones al Proveedor"
        Me.mnuRepDevolucionesProveedor.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDevolucionesProveedor.Id = 177
        Me.mnuRepDevolucionesProveedor.Name = "mnuRepDevolucionesProveedor"
        '
        'mnuCatIdentificadoresPermisos
        '
        Me.mnuCatIdentificadoresPermisos.Caption = "Identificadores para Permisos"
        Me.mnuCatIdentificadoresPermisos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatIdentificadoresPermisos.Id = 178
        Me.mnuCatIdentificadoresPermisos.Name = "mnuCatIdentificadoresPermisos"
        '
        'mnuHerrPermisosExtendidos
        '
        Me.mnuHerrPermisosExtendidos.Caption = "Permisos Extendidos"
        Me.mnuHerrPermisosExtendidos.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuHerrPermisosExtendidos.Id = 179
        Me.mnuHerrPermisosExtendidos.Name = "mnuHerrPermisosExtendidos"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(688, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    Private Sub frmMain_LoadOptions() Handles MyBase.LoadOptions
        With TINApp.Options.Security
            '---------------------------------------------------------------------------------------------
            .Options("mnuProOrdenesCompra").AddAction("FECHA_OC", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProEntradas").AddAction("FECHA_ENTRADAS", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProMovimientosInventarios").AddAction("FECHA_MOV_INV", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProTraspasos").AddAction("FECHA_TRA_SAL", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProSolicitudTraspasos").AddAction("FECHA_SOL_TRA", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProDevolucionesProveedor").AddAction("FECHA_DEV_PROV", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProCambioCancelacion").AddAction("FECHA_CAM_CANC", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

            '---------------------------------------------------------------------------------------------
            .Options("mnuProVistasSalidas").AddAction("FECHA_VIS_SAL", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

            '---------------------------------------------------------------------------------------------
            .Options("mnuProVistasEntradas").AddAction("FECHA_VIS_ENT", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

            .Options("mnuProDevolucionesProveedor").AddAction("BTNAPLICARNOTA", "Aplicar Nota", Dipros.Utils.MenuAction.ActionTypes.Extended)

        End With
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        Dim oPermisosEspeciales As New VillarrealBusiness.clsIdentificadoresPermisos


        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem3.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If

        response = oPermisosEspeciales.ListadoPermisos(TINApp.Connection.User, TINApp.Prefix)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim Row As DataRow
            Dim i As Long = 0
            Dim Cadena_identificadores As String

            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                ReDim Comunes.Common.Identificadores(odataset.Tables(0).Rows.Count() - 1)

                For Each Row In odataset.Tables(0).Rows
                    Comunes.Common.Identificadores.SetValue(Row("identificador"), i)
                    i = i + 1
                    'If i = 1 Then
                    '    Cadena_identificadores = Row("identificador")
                    'Else
                    '    Cadena_identificadores = Cadena_identificadores + "," + Row("identificador")
                    'End If
                Next
                'ShowMessage(MessageType.MsgInformation, "Ids : " + Cadena_identificadores, "Permisos de Devoluciones")

            Else
                'ShowMessage(MessageType.MsgInformation, "No hay registros de Permisos para este usuario.")
            End If
        End If

    End Sub

#End Region

#Region "Catalogos"
    Public Sub mnuCatBodegas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatBodegas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim obodegas As New VillarrealBusiness.clsBodegas
        With oGrid
            .Title = "Bodegas"
            .UpdateTitle = "Bodega"
            .DataSource = AddressOf obodegas.Listado
            .UpdateForm = New Comunes.frmBodegas
            .Format = AddressOf Comunes.clsFormato.for_bodegas_grs
            .MdiParent = Me
            .Width = 550
            .Show()
        End With
        obodegas = Nothing
    End Sub
    Public Sub mnuCatUbicaciones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatUbicaciones.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oUbicaciones As New VillarrealBusiness.clsUbicaciones
        With oGrid
            .Title = "Ubicaciones"
            .UpdateTitle = "Ubicaciones"
            .DataSource = AddressOf oUbicaciones.Listado
            .UpdateForm = New frmUbicaciones
            .Format = AddressOf for_ubicaciones_grs
            .MdiParent = Me
            .Width = 680
            .Show()
        End With
        oUbicaciones = Nothing
    End Sub

    Public Sub mnuCatDepartamentos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatDepartamentos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oDepartamentos As New VillarrealBusiness.clsDepartamentos
        With oGrid
            .Title = "Departamentos"
            .UpdateTitle = "Departamento"
            .DataSource = AddressOf oDepartamentos.Listado
            .UpdateForm = New Comunes.frmDepartamentos
            .Format = AddressOf Comunes.clsFormato.for_departamentos_grs
            .MdiParent = Me
            .Width = 540
            .Show()
        End With
        oDepartamentos = Nothing
    End Sub
    Public Sub mnuCatGruposArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatGruposArticulos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oGruposArticulos As New VillarrealBusiness.clsGruposArticulos
        With oGrid
            .Title = "Grupos de Art�culos"
            .UpdateTitle = "Grupo de Art�culos"
            .DataSource = AddressOf oGruposArticulos.Listado
            .UpdateForm = New Comunes.frmGruposArticulos
            .Format = AddressOf Comunes.clsFormato.for_grupos_articulos_grs
            .MdiParent = Me
            .Show()
        End With
        oGruposArticulos = Nothing
    End Sub
    Public Sub mnuCatConceptosInventario_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConceptosInventario.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oConceptosInventario As New VillarrealBusiness.clsConceptosInventario
        With oGrid
            .Title = "Conceptos de Inventario"
            .UpdateTitle = "Concepto de Inventario"
            .DataSource = AddressOf oConceptosInventario.Listado
            .UpdateForm = New frmConceptosInventario
            .Format = AddressOf for_conceptos_inventario_grs
            .MdiParent = Me
            .Width = 580
            .Show()
        End With
        oConceptosInventario = Nothing
    End Sub
    Public Sub mnuCatUnidades_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatUnidades.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oUnidades As New VillarrealBusiness.clsUnidades
        With oGrid
            .Title = "Unidades de Medida"
            .UpdateTitle = "Unidad de Medida"
            .DataSource = AddressOf oUnidades.Listado
            .UpdateForm = New frmUnidades
            .Format = AddressOf for_unidades_grs
            .MdiParent = Me
            .Width = 300
            .Show()
        End With
        oUnidades = Nothing
    End Sub
    Public Sub mnuCatProveedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatProveedores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oProveedores As New VillarrealBusiness.clsProveedores
        With oGrid
            .Title = "Proveedores"
            .UpdateTitle = "Proveedor"
            .DataSource = AddressOf oProveedores.Listado
            .UpdateForm = New Comunes.frmProveedores
            .Format = AddressOf Comunes.clsFormato.for_proveedores_grs
            .MdiParent = Me
            .Show()
        End With
        oProveedores = Nothing
    End Sub
    Public Sub mnuCatArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatArticulos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oArticulos As New VillarrealBusiness.clsArticulos
        With oGrid
            .Title = "Art�culos"
            .UpdateTitle = "Art�culo"
            .DataSource = AddressOf oArticulos.Listado
            .UpdateForm = New Comunes.frmArticulos
            .Format = AddressOf Comunes.clsFormato.for_articulos_grs
            .MdiParent = Me
            .Show()
        End With
        oArticulos = Nothing
    End Sub
    Public Sub mnuCatEmpleados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEmpleados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEmpleados As New VillarrealBusiness.clsEmpleados
        With oGrid
            .Title = "Empleados"
            .UpdateTitle = "un Empleado"
            .DataSource = AddressOf oEmpleados.Listado
            .UpdateForm = New Comunes.frmEmpleados
            .Format = AddressOf Comunes.clsFormato.for_empleados_grs
            .MdiParent = Me
            .Width = 480
            .Show()
        End With
        oEmpleados = Nothing
    End Sub
    Public Sub mnuCatSucursales_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatSucursales.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        With oGrid
            .Title = "Sucursales"
            .UpdateTitle = "Sucursales"
            .DataSource = AddressOf oSucursales.Listado
            .UpdateForm = New Comunes.frmSucursales
            .Format = AddressOf Comunes.clsFormato.for_sucursales_grs
            .MdiParent = Me
            .Show()
        End With
        oSucursales = Nothing
    End Sub
    Public Sub mnuProMovimientosArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosArticulos.ItemClick
        Dim oForm As New Comunes.frmMovimientosArticulos
        With oForm
            .MenuOption = e.Item
            .Title = "Movimientos del Art�culo"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuCatPrecios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatPrecios.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPrecios As New VillarrealBusiness.clsPrecios
        With oGrid
            .Title = "Tipos de Precios"
            .UpdateTitle = "Precio"
            .DataSource = AddressOf oPrecios.Listado
            .UpdateForm = New frmPrecios
            .Format = AddressOf for_precios_grs
            .MdiParent = Me
            .Width = 360
            .Show()
        End With
        oPrecios = Nothing
    End Sub
    Public Sub mnuCatIdentificadoresPermisos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatIdentificadoresPermisos.ItemClick
        Dim oGrid As New Comunes.brwIdentificadoresPermisos
        With oGrid
            .MenuOption = e.Item
            .Title = "Identificadores de Permisos"
            .UpdateTitle = "Identificador"
            .UpdateForm = New Comunes.frmIndentificadoresPermisos
            .Format = AddressOf Comunes.clsFormato.for_identificadores_permisos_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub


#End Region

#Region "Procesos"
    Public Sub mnuProOrdenesCompra_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProOrdenesCompra.ItemClick
        Dim oGrid As New brwOrdenesCompra
        With oGrid
            .MenuOption = e.Item
            .CanDelete = False
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "�rdenes de Compra"
            .UpdateTitle = "una �rden de Compra"
            .UpdateForm = New frmOrdenesCompra
            .Format = AddressOf for_ordenes_compra_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProEntradas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProEntradas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEntradas As New VillarrealBusiness.clsEntradas
        With oGrid
            .MenuOption = e.Item
            .Title = "Entradas"
            .UpdateTitle = "Entradas al Almac�n"
            .DataSource = AddressOf oEntradas.Listado
            .UpdateForm = New frmEntradas
            .Format = AddressOf for_entradas_grs
            .MdiParent = Me
            .Show()
        End With
        oEntradas = Nothing
    End Sub
    Public Sub mnuProMovimientosInventarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosInventarios.ItemClick
        Dim oGrid As New Comunes.brwMovimientosInventarios
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Movimientos al Inventario"
            .UpdateTitle = "un Movimiento al Inventario"
            .UpdateForm = New Comunes.frmMovimientosInventarios
            .Format = AddressOf Comunes.clsFormato.for_movimientos_inventarios_grs
            .MdiParent = Me
            .CanInsert = False
            .Show()
        End With
    End Sub
    Public Sub mnuProTraspasos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTraspasos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTraspasos As New VillarrealBusiness.clsTraspasos(Comunes.Common.Sucursal_Actual)
        With oGrid
            .Title = "Salida de Traspasos"
            .UpdateTitle = "Traspaso"
            .DataSource = AddressOf oTraspasos.Listado
            .UpdateForm = New frmTraspasosSalidas
            .Format = AddressOf for_traspasos_grs
            .MdiParent = Me
            .Show()
        End With
        oTraspasos = Nothing
    End Sub
    Public Sub mnuProTraspasosEntradas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTraspasosEntradas.ItemClick
        Dim oForm As New frmTraspasosEntradas
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Entradas de Traspasos"
        oForm.Show()
    End Sub
    Public Sub mnuProDevolucionesProveedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDevolucionesProveedor.ItemClick
        Dim oGrid As New brwDevolucionesProveedor
        With oGrid
            .MenuOption = e.Item
            .Title = "Devoluciones Proveedor"
            .UpdateTitle = "Devoluci�n Proveedor"
            .UpdateForm = New frmDevolucionesProveedor
            .Format = AddressOf for_devoluciones_proveedor_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProEntradasSurtimiento_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProEntradasSurtimiento.ItemClick
        Dim oForm As New frmEntradasSurtimiento
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Captura de Series"
        oForm.Show()
    End Sub
    Public Sub mnuProInventarioFisicoDepto_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProInventarioFisicoDepto.ItemClick
        Dim oGrid As New brwInventarioFisico
        With oGrid
            .MenuOption = e.Item
            .Title = "Inventario F�sico por Departamento"
            .UpdateTitle = "Inventario F�sico por Departamento"
            .TipoInventario = "D"
            .UpdateForm = New frmInventarioFisicoDepto
            .Format = AddressOf for_inventario_fisico_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProInventarioFisicoUbicacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProInventarioFisicoUbicacion.ItemClick
        Dim oGrid As New brwInventarioFisico
        With oGrid
            .MenuOption = e.Item
            .Title = "Inventario F�sico por Ubicaci�n"
            .UpdateTitle = "Inventario F�sico por Ubicaci�n"
            .TipoInventario = "U"
            .UpdateForm = New frmInventarioFisicoUbica
            .Format = AddressOf for_inventario_fisico_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProSolicitudTraspasos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProSolicitudTraspasos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oSolicitudTraspasos As New VillarrealBusiness.clsSolicitudTraspasos
        With oGrid
            .MenuOption = e.Item
            .Title = "Solicitud de Traspasos"
            .UpdateTitle = "una Solicitud de Traspasos"
            .DataSource = AddressOf oSolicitudTraspasos.Listado
            .UpdateForm = New frmSolicitudTraspasos
            .Format = AddressOf for_solicitud_traspasos_grs
            .MdiParent = Me
            .Show()
        End With
        oSolicitudTraspasos = Nothing
    End Sub
    Public Sub mnuProCambioCancelacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambioCancelacion.ItemClick
        Dim oForm As New frmCambioMercanciaCancelada
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Cambios de Mercancia Cancelada"
        oForm.Show()
    End Sub
    Public Sub mnuProVistasSalidas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProVistasSalidas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oVistasSalidas As New VillarrealBusiness.clsVistasSalidas
        With oGrid
            .MenuOption = e.Item
            .Title = "Salidas por Vistas"
            .UpdateTitle = "Salida por Vista"
            .DataSource = AddressOf oVistasSalidas.Listado
            .UpdateForm = New frmVistasSalidas
            .Format = AddressOf Comunes.clsFormato.for_vistas_salidas_grs
            .MdiParent = Me
            .Show()
        End With
        oVistasSalidas = Nothing
    End Sub
    Public Sub mnuProVistasEntradas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProVistasEntradas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oVistasEntradas As New VillarrealBusiness.clsVistasEntradas
        With oGrid
            .MenuOption = e.Item
            .Title = "Entradas por Vistas"
            .UpdateTitle = "Entrada por Vista"
            .DataSource = AddressOf oVistasEntradas.Listado
            .UpdateForm = New frmVistasEntradas
            .Format = AddressOf Comunes.clsFormato.for_vistas_entradas_grs
            .MdiParent = Me
            .CanDelete = False
            .Show()
        End With
        oVistasEntradas = Nothing
    End Sub
    Private Sub mnuProEntradasSalidasReparacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProEntradasSalidasReparacion.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEntradasSalidasReparacion As New VillarrealBusiness.clsEntradasSalidasReparacion
        With oGrid
            .MenuOption = e.Item
            .Title = "Entradas y Salidas por Reparaci�n"
            .UpdateTitle = "Entrada o Salida por Reparaci�n"
            .DataSource = AddressOf oEntradasSalidasReparacion.Listado
            .UpdateForm = New frmEntradasSalidasReparacion
            .Format = AddressOf Comunes.clsFormato.for_entradas_salidas_reparacion_grs
            .MdiParent = Me
            .CanInsert = False
            .CanUpdate = True
            .CanDelete = False
            .Show()
        End With
        oEntradasSalidasReparacion = Nothing
    End Sub
#End Region

#Region "Herramientas"

    Public Sub mnuHerrPermisosExtendidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHerrPermisosExtendidos.ItemClick
        Dim oForm As New Comunes.frmPermisosIdentificadores
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Permisos Extendidos"
        oForm.Action = Actions.None
        oForm.Show()
    End Sub
#End Region

#Region "Reportes"
    Public Sub mnuRepPendientesArribar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepPendientesArribar.ItemClick
        Dim oForm As New frmPendientesArribar
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Pendientes de Arribar"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepEntradasAlmacen_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEntradasAlmacen.ItemClick
        Dim oForm As New frmEntradasAlmacen
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Entradas al Almac�n"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    'Public Sub mnuRepExistencias_ItemClick1(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    
    'End Sub
    Public Sub mnuRepExistenciasAlmacen_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepExistenciasAlmacen.ItemClick
        Dim oForm As New frmExistencias
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Existencias por Almac�n"
        oForm.Action = Actions.Report
        oForm.Show()

    End Sub
    Public Sub mnuRepMovimientos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMovimientos.ItemClick
        Dim oForm As New frmMovimientos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Movimientos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepCosteoInventario_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepCosteoInventario.ItemClick
        Dim oForm As New frmCosteoInventario
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Valor del Inventario"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepArticulosQuedados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosQuedados.ItemClick
        Dim oForm As New frmArticulosQuedados
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Art�culos Quedados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepResumenSalidas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenSalidas.ItemClick
        Dim oForm As New Comunes.frmResumenSalidas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Salidas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepResumenProveedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenProveedor.ItemClick
        Dim oForm As New frmResumenProveedor
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Proveedor"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepStocksArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepStocksArticulos.ItemClick
        Dim oForm As New frmStocksDeArticulos
        oForm.MdiParent = Me
        oForm.Title = "Stocks de Art�culos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepEstadisticaVenta_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEstadisticaVenta.ItemClick
        Dim oForm As New frmEstadisticasVenta
        oForm.MdiParent = Me
        oForm.Title = "Estad�sticas de Ventas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepTraspasos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepTraspasos.ItemClick
        Dim oForm As New frmTraspasosEntradasSalidas
        oForm.MdiParent = Me
        oForm.Title = "Traspasos Entradas/Salidas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepResumenEntradas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenEntradas.ItemClick
        Dim oForm As New frmResumenEntradas
        oForm.MdiParent = Me
        oForm.Title = "Resumen de Entradas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    'Public Sub mnuRepResumenSalidasDetallado_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenSalidasDetallado.ItemClick
    '    Dim oForm As New frmResumenSalidasDetallado
    '    oForm.MdiParent = Me
    '    oForm.Title = "Resumen de Salidas Detallado"
    '    oForm.Action = Actions.Report
    '    oForm.Show()
    'End Sub
    Public Sub mnuRepArticulosPedidosFabrica_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepArticulosPedidosFabrica.ItemClick
        Dim oForm As New frmRepArticulosPedidosFabrica
        oForm.MdiParent = Me
        oForm.Title = "Art�culos Pedidos a F�brica"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepExistencias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepExistencias.ItemClick
        Dim oForm As New frmExistenciasMultiple
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Existencias"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepSeriesArticulos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSeriesArticulos.ItemClick
        Dim oForm As New frmSeriesArticulos
        oForm.MdiParent = Me
        oForm.Title = "Series de Art�culos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepMercanciaTransito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMercanciaTransito.ItemClick
        Dim oForm As New frmMercanciasTransito
        oForm.MdiParent = Me
        oForm.Title = "Sucesos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepCambiosPrecios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepCambiosPrecios.ItemClick
        Dim oForm As New Comunes.frmCambiosPrecios
        oForm.MdiParent = Me
        oForm.Title = "Cambios de Precios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepFletesOtrosCargos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepFletesOtrosCargos.ItemClick
        Dim oForm As New frmFletesOtrosCargos
        oForm.MdiParent = Me
        oForm.Title = "Fletes y Otros Cargos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepMercanciaEtiquetada_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMercanciaEtiquetada.ItemClick
        Dim oForm As New Comunes.frmMercanciaEtiquetada
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Mercanc�a Etiquetada"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepMercanciaVistas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMercanciaVistas.ItemClick
        Dim oForm As New frmMercanciaVistas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Mercanc�a en Vistas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDiferenciasPedidosFabrica_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiferenciasPedidosFabrica.ItemClick
        Dim oForm As New frmDiferenciasCostosPedidoFabrica
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Periodo Asignaci�n de Costo"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepDiferenciaCostosDevProveedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiferenciaCostosDevProveedor.ItemClick
        Dim oForm As New frmDiferenciasCostosDevProveedor
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diferencias en Costos por Devoluci�n de Mcia. a Proveedores"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepDevolucionesProveedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDevolucionesProveedor.ItemClick
        Dim oForm As New frmRepDevolucionesProveedor
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Devoluciones al Proveedor"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

#End Region

   
  
    
    
 
   
  
 
End Class
