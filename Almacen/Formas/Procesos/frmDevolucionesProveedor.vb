Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmDevolucionesProveedor
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Private folio_movimiento As Long = 0
    Private Guardado As Boolean = False
    Private nota_aplicada As Boolean = False
    Private aplicado_inventario As Boolean = False
    Private enviado_proveedor As Boolean = False
    Private _Impuesto As Double = 0



#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblDevolucion As System.Windows.Forms.Label
    Friend WithEvents clcDevolucion As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup

    Friend WithEvents txtConducto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents grDevolucionesProveedor As DevExpress.XtraGrid.GridControl
    Public WithEvents grvDevolucionesProveedor As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaDevolucionesProveedor As Dipros.Windows.TINMaster
    Public WithEvents gdcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcDescripcionCorta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents gdcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents gdcManejaSeries As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcFolioHisCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConcepto_Salida As System.Windows.Forms.Label
    Friend WithEvents grcEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblConducto As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcIva As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcSubTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDevolucionGuardada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCostoFlete As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcOrdenServicio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnNotaAplicada As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnAplicarInventario As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnEtiquetaDevolucion As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnEnviadoProveedor As System.Windows.Forms.ToolBarButton

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDevolucionesProveedor))
        Me.lblDevolucion = New System.Windows.Forms.Label
        Me.clcDevolucion = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblConducto = New System.Windows.Forms.Label
        Me.txtConducto = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.grDevolucionesProveedor = New DevExpress.XtraGrid.GridControl
        Me.grvDevolucionesProveedor = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gdcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcDescripcionCorta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcManejaSeries = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcFolioHisCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDevolucionGuardada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCostoFlete = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcOrdenServicio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaDevolucionesProveedor = New Dipros.Windows.TINMaster
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lblConcepto_Salida = New System.Windows.Forms.Label
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcIva = New DevExpress.XtraEditors.CalcEdit
        Me.clcTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcSubTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnNotaAplicada = New System.Windows.Forms.ToolBarButton
        Me.btnAplicarInventario = New System.Windows.Forms.ToolBarButton
        Me.btnEtiquetaDevolucion = New System.Windows.Forms.ToolBarButton
        Me.btnEnviadoProveedor = New System.Windows.Forms.ToolBarButton
        CType(Me.clcDevolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConducto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDevolucionesProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDevolucionesProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.btnNotaAplicada, Me.btnAplicarInventario, Me.btnEtiquetaDevolucion, Me.btnEnviadoProveedor})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(7708, 28)
        '
        'lblDevolucion
        '
        Me.lblDevolucion.AutoSize = True
        Me.lblDevolucion.Location = New System.Drawing.Point(30, 42)
        Me.lblDevolucion.Name = "lblDevolucion"
        Me.lblDevolucion.Size = New System.Drawing.Size(70, 16)
        Me.lblDevolucion.TabIndex = 0
        Me.lblDevolucion.Tag = ""
        Me.lblDevolucion.Text = "&Devolucion:"
        Me.lblDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDevolucion
        '
        Me.clcDevolucion.EditValue = "0"
        Me.clcDevolucion.Location = New System.Drawing.Point(104, 40)
        Me.clcDevolucion.MaxValue = 0
        Me.clcDevolucion.MinValue = 0
        Me.clcDevolucion.Name = "clcDevolucion"
        '
        'clcDevolucion.Properties
        '
        Me.clcDevolucion.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcDevolucion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDevolucion.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcDevolucion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDevolucion.Properties.Enabled = False
        Me.clcDevolucion.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcDevolucion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDevolucion.Size = New System.Drawing.Size(48, 19)
        Me.clcDevolucion.TabIndex = 1
        Me.clcDevolucion.Tag = "devolucion"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(59, 90)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "16/03/2006"
        Me.dteFecha.Location = New System.Drawing.Point(104, 87)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = "fecha"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(35, 114)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 8
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Prov&eedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(104, 111)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(380, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(312, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 9
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'lblConducto
        '
        Me.lblConducto.AutoSize = True
        Me.lblConducto.Location = New System.Drawing.Point(39, 138)
        Me.lblConducto.Name = "lblConducto"
        Me.lblConducto.Size = New System.Drawing.Size(61, 16)
        Me.lblConducto.TabIndex = 10
        Me.lblConducto.Tag = ""
        Me.lblConducto.Text = "Co&nducto:"
        Me.lblConducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConducto
        '
        Me.txtConducto.EditValue = ""
        Me.txtConducto.Location = New System.Drawing.Point(104, 135)
        Me.txtConducto.Name = "txtConducto"
        '
        'txtConducto.Properties
        '
        Me.txtConducto.Properties.MaxLength = 80
        Me.txtConducto.Size = New System.Drawing.Size(584, 20)
        Me.txtConducto.TabIndex = 11
        Me.txtConducto.Tag = "conducto"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(11, 159)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 12
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "Observac&iones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 159)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(584, 38)
        Me.txtObservaciones.TabIndex = 13
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(544, 66)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 14
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblStatus.Visible = False
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "S"
        Me.cboStatus.Location = New System.Drawing.Point(592, 63)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enviado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("No Enviado", "S", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 23)
        Me.cboStatus.TabIndex = 15
        Me.cboStatus.Tag = "status"
        Me.cboStatus.Visible = False
        '
        'grDevolucionesProveedor
        '
        '
        'grDevolucionesProveedor.EmbeddedNavigator
        '
        Me.grDevolucionesProveedor.EmbeddedNavigator.Name = ""
        Me.grDevolucionesProveedor.Location = New System.Drawing.Point(8, 232)
        Me.grDevolucionesProveedor.MainView = Me.grvDevolucionesProveedor
        Me.grDevolucionesProveedor.Name = "grDevolucionesProveedor"
        Me.grDevolucionesProveedor.Size = New System.Drawing.Size(680, 219)
        Me.grDevolucionesProveedor.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDevolucionesProveedor.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDevolucionesProveedor.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDevolucionesProveedor.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDevolucionesProveedor.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDevolucionesProveedor.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDevolucionesProveedor.TabIndex = 14
        Me.grDevolucionesProveedor.TabStop = False
        Me.grDevolucionesProveedor.Text = "DevolucionesProveedor"
        '
        'grvDevolucionesProveedor
        '
        Me.grvDevolucionesProveedor.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gdcArticulo, Me.gdcDescripcionCorta, Me.gdcCantidad, Me.gdcUnidad, Me.gdcSerie, Me.gdcManejaSeries, Me.gdcCosto, Me.gdcImporte, Me.gdcFolioHisCosto, Me.grcEntrada, Me.grcDepartamento, Me.grcGrupo, Me.grcDevolucionGuardada, Me.grcCostoFlete, Me.grcOrdenServicio})
        Me.grvDevolucionesProveedor.GridControl = Me.grDevolucionesProveedor
        Me.grvDevolucionesProveedor.Name = "grvDevolucionesProveedor"
        Me.grvDevolucionesProveedor.OptionsBehavior.Editable = False
        Me.grvDevolucionesProveedor.OptionsCustomization.AllowFilter = False
        Me.grvDevolucionesProveedor.OptionsCustomization.AllowGroup = False
        Me.grvDevolucionesProveedor.OptionsCustomization.AllowSort = False
        Me.grvDevolucionesProveedor.OptionsView.ShowGroupPanel = False
        '
        'gdcArticulo
        '
        Me.gdcArticulo.Caption = "Art�culo"
        Me.gdcArticulo.FieldName = "articulo"
        Me.gdcArticulo.Name = "gdcArticulo"
        Me.gdcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcArticulo.VisibleIndex = 0
        '
        'gdcDescripcionCorta
        '
        Me.gdcDescripcionCorta.Caption = "Descripci�n"
        Me.gdcDescripcionCorta.FieldName = "descripcion_corta"
        Me.gdcDescripcionCorta.Name = "gdcDescripcionCorta"
        Me.gdcDescripcionCorta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcDescripcionCorta.VisibleIndex = 1
        Me.gdcDescripcionCorta.Width = 200
        '
        'gdcCantidad
        '
        Me.gdcCantidad.Caption = "Cantidad"
        Me.gdcCantidad.FieldName = "cantidad"
        Me.gdcCantidad.Name = "gdcCantidad"
        Me.gdcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcCantidad.VisibleIndex = 2
        Me.gdcCantidad.Width = 77
        '
        'gdcUnidad
        '
        Me.gdcUnidad.Caption = "Unidad"
        Me.gdcUnidad.FieldName = "nombre_unidad"
        Me.gdcUnidad.Name = "gdcUnidad"
        Me.gdcUnidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcUnidad.VisibleIndex = 3
        Me.gdcUnidad.Width = 77
        '
        'gdcSerie
        '
        Me.gdcSerie.Caption = "Serie"
        Me.gdcSerie.FieldName = "numero_serie"
        Me.gdcSerie.Name = "gdcSerie"
        Me.gdcSerie.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcSerie.VisibleIndex = 4
        Me.gdcSerie.Width = 77
        '
        'gdcManejaSeries
        '
        Me.gdcManejaSeries.Caption = "Maneja Series"
        Me.gdcManejaSeries.FieldName = "maneja_series"
        Me.gdcManejaSeries.Name = "gdcManejaSeries"
        '
        'gdcCosto
        '
        Me.gdcCosto.Caption = "Costo"
        Me.gdcCosto.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcCosto.FieldName = "costo"
        Me.gdcCosto.Name = "gdcCosto"
        Me.gdcCosto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcCosto.VisibleIndex = 5
        Me.gdcCosto.Width = 66
        '
        'gdcImporte
        '
        Me.gdcImporte.Caption = "Importe"
        Me.gdcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcImporte.FieldName = "importe"
        Me.gdcImporte.Name = "gdcImporte"
        Me.gdcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.gdcImporte.VisibleIndex = 6
        Me.gdcImporte.Width = 94
        '
        'gdcFolioHisCosto
        '
        Me.gdcFolioHisCosto.Caption = "F�lio Hist�rico de Costo"
        Me.gdcFolioHisCosto.FieldName = "folio_historico_costo"
        Me.gdcFolioHisCosto.Name = "gdcFolioHisCosto"
        Me.gdcFolioHisCosto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcEntrada
        '
        Me.grcEntrada.Caption = "Entrada"
        Me.grcEntrada.FieldName = "entrada"
        Me.grcEntrada.Name = "grcEntrada"
        Me.grcEntrada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        '
        'grcDevolucionGuardada
        '
        Me.grcDevolucionGuardada.Caption = "Devoluci�n Guardada"
        Me.grcDevolucionGuardada.FieldName = "devolucion_guardada"
        Me.grcDevolucionGuardada.Name = "grcDevolucionGuardada"
        Me.grcDevolucionGuardada.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDevolucionGuardada.Width = 133
        '
        'grcCostoFlete
        '
        Me.grcCostoFlete.Caption = "Costo Flete"
        Me.grcCostoFlete.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcCostoFlete.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCostoFlete.FieldName = "costo_flete"
        Me.grcCostoFlete.Name = "grcCostoFlete"
        '
        'grcOrdenServicio
        '
        Me.grcOrdenServicio.Caption = "Orden Servicio"
        Me.grcOrdenServicio.FieldName = "orden_servicio"
        Me.grcOrdenServicio.Name = "grcOrdenServicio"
        Me.grcOrdenServicio.VisibleIndex = 7
        '
        'tmaDevolucionesProveedor
        '
        Me.tmaDevolucionesProveedor.BackColor = System.Drawing.Color.White
        Me.tmaDevolucionesProveedor.CanDelete = True
        Me.tmaDevolucionesProveedor.CanInsert = True
        Me.tmaDevolucionesProveedor.CanUpdate = True
        Me.tmaDevolucionesProveedor.Grid = Me.grDevolucionesProveedor
        Me.tmaDevolucionesProveedor.Location = New System.Drawing.Point(13, 208)
        Me.tmaDevolucionesProveedor.Name = "tmaDevolucionesProveedor"
        Me.tmaDevolucionesProveedor.Size = New System.Drawing.Size(675, 23)
        Me.tmaDevolucionesProveedor.TabIndex = 13
        Me.tmaDevolucionesProveedor.TabStop = False
        Me.tmaDevolucionesProveedor.Title = ""
        Me.tmaDevolucionesProveedor.UpdateTitle = "un Registro"
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "descripcion"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Enabled = False
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(400, 39)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(360, Long)
        Me.lkpConcepto.ReadOnlyControl = False
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(288, 20)
        Me.lkpConcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConcepto.TabIndex = 3
        Me.lkpConcepto.Tag = ""
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lblConcepto_Salida
        '
        Me.lblConcepto_Salida.AutoSize = True
        Me.lblConcepto_Salida.Location = New System.Drawing.Point(336, 42)
        Me.lblConcepto_Salida.Name = "lblConcepto_Salida"
        Me.lblConcepto_Salida.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto_Salida.TabIndex = 2
        Me.lblConcepto_Salida.Tag = ""
        Me.lblConcepto_Salida.Text = "C&oncepto:"
        Me.lblConcepto_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(50, 66)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 4
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(104, 63)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(410, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(312, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 5
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 6
        Me.ToolBarButton1.Text = "Reimprimir"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(544, 480)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 16)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "IVA:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'clcIva
        '
        Me.clcIva.Location = New System.Drawing.Point(576, 478)
        Me.clcIva.Name = "clcIva"
        '
        'clcIva.Properties
        '
        Me.clcIva.Properties.AllowFocused = False
        Me.clcIva.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.ReadOnly = True
        Me.clcIva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIva.Size = New System.Drawing.Size(112, 20)
        Me.clcIva.TabIndex = 18
        Me.clcIva.TabStop = False
        '
        'clcTotal
        '
        Me.clcTotal.Location = New System.Drawing.Point(576, 502)
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.AllowFocused = False
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.ReadOnly = True
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(112, 20)
        Me.clcTotal.TabIndex = 20
        Me.clcTotal.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(536, 504)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 16)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Total:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'clcSubTotal
        '
        Me.clcSubTotal.Location = New System.Drawing.Point(576, 454)
        Me.clcSubTotal.Name = "clcSubTotal"
        '
        'clcSubTotal.Properties
        '
        Me.clcSubTotal.Properties.AllowFocused = False
        Me.clcSubTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.ReadOnly = True
        Me.clcSubTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubTotal.Size = New System.Drawing.Size(112, 20)
        Me.clcSubTotal.TabIndex = 16
        Me.clcSubTotal.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(512, 456)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 16)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Sub Total:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnNotaAplicada
        '
        Me.btnNotaAplicada.Enabled = False
        Me.btnNotaAplicada.Tag = "BTNAPLICARNOTA"
        Me.btnNotaAplicada.Text = "Aplicar Nota"
        Me.btnNotaAplicada.ToolTipText = "BTNAPLICARNOTA"
        '
        'btnAplicarInventario
        '
        Me.btnAplicarInventario.Enabled = False
        Me.btnAplicarInventario.Tag = "BTNAPLICARINV"
        Me.btnAplicarInventario.Text = "Aplicar Inventario"
        Me.btnAplicarInventario.ToolTipText = "BTNAPLICARINV"
        '
        'btnEtiquetaDevolucion
        '
        Me.btnEtiquetaDevolucion.Text = "Etiqueta Devoluci�n"
        Me.btnEtiquetaDevolucion.ToolTipText = "Imprime la etiqueta de la devoluci�n"
        '
        'btnEnviadoProveedor
        '
        Me.btnEnviadoProveedor.Enabled = False
        Me.btnEnviadoProveedor.Tag = "BTNENVIADOPROV"
        Me.btnEnviadoProveedor.Text = "Enviado Proveedor"
        Me.btnEnviadoProveedor.ToolTipText = "BTNENVIADOPROV"
        '
        'frmDevolucionesProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(694, 528)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcIva)
        Me.Controls.Add(Me.clcTotal)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clcSubTotal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lkpConcepto)
        Me.Controls.Add(Me.lblConcepto_Salida)
        Me.Controls.Add(Me.lblDevolucion)
        Me.Controls.Add(Me.clcDevolucion)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.lblConducto)
        Me.Controls.Add(Me.txtConducto)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.grDevolucionesProveedor)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.tmaDevolucionesProveedor)
        Me.Name = "frmDevolucionesProveedor"
        Me.Controls.SetChildIndex(Me.tmaDevolucionesProveedor, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.grDevolucionesProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtConducto, 0)
        Me.Controls.SetChildIndex(Me.lblConducto, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcDevolucion, 0)
        Me.Controls.SetChildIndex(Me.lblDevolucion, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto_Salida, 0)
        Me.Controls.SetChildIndex(Me.lkpConcepto, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcSubTotal, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.clcTotal, 0)
        Me.Controls.SetChildIndex(Me.clcIva, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcDevolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConducto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDevolucionesProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDevolucionesProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDevolucionesProveedor As New VillarrealBusiness.clsDevolucionesProveedor
    Public oDevolucionesProveedordetalle As New VillarrealBusiness.clsDevolucionesProveedorDetalle
    Private oProveedores As New VillarrealBusiness.clsProveedores
    Private ovariables As New VillarrealBusiness.clsVariables
    Private oConceptosInventario As New VillarrealBusiness.clsConceptosInventario
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oReportes As New VillarrealBusiness.Reportes
    Private oHistoricoCostos As New VillarrealBusiness.clsHisCostos

    Private oArticulosExistencias As New VillarrealBusiness.clsArticulosExistencias

    Private oMovimientosInventario As New VillarrealBusiness.clsMovimientosInventarios
    Public oMovimientosInventarioDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As New VillarrealBusiness.clsMovimientosInventariosDetalleSeries

    Private oEntradasSalidasReparacion As New VillarrealBusiness.clsEntradasSalidasReparacion
    Private oPermisosEspeciales As New VillarrealBusiness.clsIdentificadoresPermisos


    Private ReadOnly Property ConceptoEntradaReparacion() As String
        Get
            Return oVariables.TraeDatos("concepto_entrada_reparacion", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property


    Public ReadOnly Property Bodega() As String
        Get
            Return PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
    Public ReadOnly Property Proveedor() As Long
        Get
            Return PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Private ReadOnly Property ConceptoDevolucionProveedor() As String
        Get
            Return ovariables.TraeDatos("concepto_devoluciones", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property ConceptoCancelacionDevolucionProveedor() As String
        Get
            Return ovariables.TraeDatos("concepto_cancelacion_devoluciones", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Public ReadOnly Property AfectaCostos() As Boolean
        Get
            Return Me.lkpConcepto.GetValue("afecta_saldos")
        End Get
    End Property
    Private ReadOnly Property Impuesto() As Double
        Get
            Return _Impuesto
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDevolucionesProveedor_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmDevolucionesProveedor_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Select Case Action
            Case Actions.Insert
                ImprimirReporte()
        End Select

    End Sub
    Private Sub frmDevolucionesProveedor_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
        Guardado = False
    End Sub

    Private Sub frmDevolucionesProveedor_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oDevolucionesProveedor.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oDevolucionesProveedor.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oDevolucionesProveedor.Eliminar(Bodega, clcDevolucion.Value)

        End Select
    End Sub
    Private Sub frmDevolucionesProveedor_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        Dim Costo_Detalle As Double = 0


        With Me.tmaDevolucionesProveedor
            .MoveFirst()
            Do While Not .EOF

                Select Case .CurrentAction
                    Case Actions.Insert

                        'INSERTO EL DETALLE DE LA DEVOLUCION 
                        Response = Me.oDevolucionesProveedordetalle.Insertar(Me.tmaDevolucionesProveedor.SelectedRow, Bodega, Me.clcDevolucion.Value)

                    Case Actions.Update


                        Response = Me.oDevolucionesProveedordetalle.Actualizar(Me.tmaDevolucionesProveedor.SelectedRow, Bodega, Me.clcDevolucion.Value)
                    Case Actions.Delete
                        Response = Me.oDevolucionesProveedordetalle.Eliminar(Bodega, Me.clcDevolucion.Value, Me.tmaDevolucionesProveedor.Item("partida"))


                End Select
                If Response.ErrorFound Then Exit Sub
                .MoveNext()
            Loop
        End With
    End Sub
    Private Sub frmDevolucionesProveedor_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oDevolucionesProveedor.DespliegaDatos(OwnerForm.Value("bodega"), OwnerForm.Value("devolucion"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet


            Me.folio_movimiento = oDataSet.Tables(0).Rows(0).Item("folio_movimiento")
            Me.nota_aplicada = CType(oDataSet.Tables(0).Rows(0).Item("nota_aplicada"), Boolean)
            Me.aplicado_inventario = CType(oDataSet.Tables(0).Rows(0).Item("aplicado_inventario"), Boolean)
            Me.enviado_proveedor = CType(oDataSet.Tables(0).Rows(0).Item("enviado_proveedor"), Boolean)


            Response = Me.oDevolucionesProveedordetalle.DespliegaDatos(OwnerForm.Value("bodega"), OwnerForm.Value("devolucion"))
            oDataSet = Response.Value
            Me.tmaDevolucionesProveedor.DataSource = oDataSet

            If OwnerForm.Value("status") = "E" Then
                Me.EnabledEdit(False)
                Me.tbrTools.Buttons.Item(0).Enabled = False
            End If
        End If

        CalculaTotales()

        'DAM 29/FEB/2012
        If Not Comunes.Common.Identificadores Is Nothing Then
            If Comunes.Common.Identificadores.Length > 0 Then

                

                Dim Validaciones As Boolean() = VerificaPermisosExtendidos(Me.tbrTools.Buttons, Comunes.Common.Identificadores, aplicado_inventario, "BTNAPLICARINV")

                If aplicado_inventario = True Then
                    Me.btnAplicarInventario.Enabled = False
                    Me.tbrTools.Buttons.Item(0).Enabled = False
                End If

                If enviado_proveedor = True Then
                    Me.btnEnviadoProveedor.Enabled = False
                End If

                If nota_aplicada = True Then
                    Me.btnNotaAplicada.Enabled = False
                End If


                'Dim cadenas As String = ""
                'Dim bool As Boolean

                'For Each bool In Validaciones

                '    cadenas = cadenas + "," + bool.ToString()

                'Next

                'ShowMessage(MessageType.MsgInformation, "Permisos Aplicados : " + cadenas, "Permisos de Devoluciones Aplicados")
            End If
        Else
            If TINApp.Connection.User.ToUpper() = "SUPER" Then
                'DAM 22/FEB/2012
                ' Los botones de Aplicar Nota y Enviado al Proveedor dependeran si ya esta aplicado al inventario
                Me.tbrTools.Buttons(3).Enabled = aplicado_inventario
                Me.tbrTools.Buttons(3).Visible = aplicado_inventario
                Me.tbrTools.Buttons(6).Enabled = aplicado_inventario
                Me.tbrTools.Buttons(6).Visible = aplicado_inventario
            End If
        End If

        If Me.nota_aplicada Or Me.aplicado_inventario Or Me.enviado_proveedor Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.tbrTools.Buttons(0).Visible = False
        End If


    End Sub
    Private Sub frmDevolucionesProveedor_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.dteFecha.DateTime = Now.Date

        _Impuesto = (CType(ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double) / 100)

        With Me.tmaDevolucionesProveedor
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmDevolucionesProveedorDetalle
            .AddColumn("articulo")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("nombre_unidad")
            .AddColumn("numero_serie")
            .AddColumn("maneja_series", "System.Boolean")
            .AddColumn("costo", "System.Double")
            .AddColumn("importe", "System.Double")
            .AddColumn("folio_historico_costo", "System.Int32")
            .AddColumn("entrada")
            .AddColumn("departamento", "System.Int32")
            .AddColumn("grupo", "System.Int32")
            .AddColumn("devolucion_guardada", "System.Boolean")
            .AddColumn("costo_flete", "System.Double")
            .AddColumn("orden_servicio", "System.Int32")
        End With

        Select Case Action
            Case Actions.Insert
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(5).Visible = False
            Case Actions.Update
                Me.lkpProveedor.Enabled = False
            Case Actions.Delete
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(5).Visible = False
        End Select

        Me.lkpConcepto.EditValue = Me.ConceptoDevolucionProveedor
        Me.lkpBodega.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)

        ActivaTMA()

    End Sub
    Private Sub frmDevolucionesProveedor_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDevolucionesProveedor.Validacion(Action, Bodega, Proveedor, Me.tmaDevolucionesProveedor.Count)
        If Not Response.ErrorFound Then
            Response = ValidaOrdenServicio()
        End If
    End Sub

    Private Sub frmDevolucionesProveedor_Localize() Handles MyBase.Localize
        Find("devolucion", Me.clcDevolucion.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConceptoSalida_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConcepto)
    End Sub
    Private Sub lkpConceptoSalida_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events

        response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        If Me.lkpBodega.DataSource Is Nothing Or Me.lkpBodega.EditValue Is Nothing Then Exit Sub

    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        Dim response As New Events

        If e.Button.Text = "Reimprimir" Then
            ImprimirReporte()
        End If

        If e.Button Is Me.btnEtiquetaDevolucion Then
            ImprimeEtiquetaDevolucion()
        End If

        If e.Button Is Me.btnNotaAplicada Then

            response = Me.oDevolucionesProveedor.ActualizaNotaAplicada(Me.Bodega, Me.clcDevolucion.Value, True)
        End If

        If e.Button Is Me.btnAplicarInventario Then

            ' ABRE LA TRANSACCION
            frmDevolucionesProveedor_BeginUpdate()

            GeneraMovimientoInventario(Action, response, Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, Me.dteFecha.DateTime, -1, "", Me.clcDevolucion.Value, "Movimiento generado desde Devoluciones al Proveedor")

            If Not response.ErrorFound Then
                response = GeneraMovimientoInventarioDetalles()

                If Not response.ErrorFound Then
                    response = Me.oDevolucionesProveedor.ActualizaAplicadoInventario(Me.Bodega, Me.clcDevolucion.Value, True)
                Else
                    frmDevolucionesProveedor_AbortUpdate()
                    Exit Sub
                End If
            Else
                frmDevolucionesProveedor_AbortUpdate()
                Exit Sub
            End If

            ''DAM 22/0FEB/2012
            '' Aqui se realiza la aplicacion de la nota
            'If Not response.ErrorFound Then
            '    response = Me.oDevolucionesProveedor.ActualizaNotaAplicada(Me.Bodega, Me.clcDevolucion.Value, True)
            'Else
            '    frmDevolucionesProveedor_AbortUpdate()
            '    Exit Sub
            'End If

            ' TERMINA LA TRANSACCION
            frmDevolucionesProveedor_EndUpdate()

        End If

        If e.Button Is Me.btnEnviadoProveedor Then
            response = Me.oDevolucionesProveedor.ActualizaEnviadoProveedor(Me.Bodega, Me.clcDevolucion.Value, True)
        End If

        If response.ErrorFound Then
            response.ShowError()
        Else
            Me.Close()
        End If

    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
        ActivaTMA()
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String)

        If Guardado = False Then
            response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)

            'Select Case accion
            '    Case Actions.Insert, Actions.Delete
            '        response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
            '        Guardado = True
            '    Case Actions.Update
            '        response = oMovimientosInventario.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
            '        Guardado = True
            'End Select

        End If
    End Sub
    Private Function ImprimirReporte()
        Dim response As New Events
        response = oReportes.devolucionMercancias(Bodega, clcDevolucion.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Devoluci�n de Mercanc�a no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptDevolucionMercancia

                oDataSet = response.Value

                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                TINApp.ShowReport(Me.MdiParent, "Devoluci�n de Mercanc�a", oReport, , , , True)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Function
    Public Sub CalculaTotales()
        clcSubTotal.Value = Me.gdcImporte.SummaryItem.SummaryValue
        clcIva.Value = CType(clcSubTotal.Value, Double) * Impuesto
        clcTotal.Value = CType(clcSubTotal.Value, Double) + CType(clcIva.Value, Double)

    End Sub
    Public Function ActivaLookup()
        If Me.tmaDevolucionesProveedor.DataSource Is Nothing Then
            Me.lkpProveedor.Enabled = True
            Me.lkpBodega.Enabled = True
        Else
            Me.lkpProveedor.Enabled = False
            Me.lkpBodega.Enabled = False
        End If
    End Function
    Public Function ActivaTMA()
        If Me.lkpProveedor.EditValue <= 0 Then
            Me.tmaDevolucionesProveedor.Enabled = False
        Else
            Me.tmaDevolucionesProveedor.Enabled = True
        End If
    End Function
    Public Function ValidaOrdenServicio() As Events
        Me.grvDevolucionesProveedor.CloseEditor()
        Dim response As Dipros.Utils.Events
        Dim oRow As DataRow
        Dim oData As DataSet

        'For Each oRow In CType(Me.grDevolucionesProveedor.DataSource, DataTable).Rows
        For Each oRow In CType(Me.tmaDevolucionesProveedor.DataSource, DataSet).Tables(0).Rows


            response = Me.oDevolucionesProveedor.ValidaOrdenServicio(CType(oRow("orden_servicio"), Integer))
            If Not response.ErrorFound Then
                oData = response.Value
                If (CType(oData.Tables(0).Rows(0).Item("utilizada"), Boolean) = True And (CType(oData.Tables(0).Rows(0).Item("devolucion"), Integer) > 0 And Me.clcDevolucion.EditValue <> CType(oData.Tables(0).Rows(0).Item("devolucion"), Integer))) Then
                    response.Ex = Nothing
                    response.Message = "Partida " + CType(oRow("partida"), String) + ". La orden de servicio " + CType(oRow("orden_servicio"), String).Trim + " ha sido asignada a otra devoluci�n."
                    Return response
                End If
            End If

        Next
        Return response

    End Function
    Private Function VerificaSalidaGeneraEntradaOrdenServicioReparacion(ByVal Orden_servicio As Long) As Events

        Dim response As Dipros.Utils.Events
        Dim oRow As DataRow
        Dim oDataSet As DataSet

        'For Each oRow In CType(Me.tmaDevolucionesProveedor.DataSource, DataSet).Tables(0).Rows
        'response = Me.oEntradasSalidasReparacion.DespliegaDatos(oRow("orden_servicio"))

        response = Me.oEntradasSalidasReparacion.DespliegaDatos(Orden_servicio)

        If Not response.ErrorFound Then
            oDataSet = response.Value

            Dim folio_movimiento_salida As Long = 0
            Dim folio_movimiento_entrada As Long = 0
            Dim Articulo As Long = 0
            Dim Ultimo_Costo As Double = 0.0
            Dim Bodega_Orden As String = ""

            Dim Tipo As String = ""

            Tipo = CStr(oDataSet.Tables(0).Rows(0).Item("tipo_servicio"))

            folio_movimiento_salida = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_salida"))
            folio_movimiento_entrada = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_entrada"))
            Articulo = CInt(oDataSet.Tables(0).Rows(0).Item("articulo"))
            Ultimo_Costo = CDbl(oDataSet.Tables(0).Rows(0).Item("ultimo_costo"))
            Bodega_Orden = CStr(oDataSet.Tables(0).Rows(0).Item("clave_bodega"))

            If Tipo = "R" And folio_movimiento_salida > 0 And folio_movimiento_entrada = 0 Then
                response = Me.oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, Bodega_Orden, Me.ConceptoEntradaReparacion, folio_movimiento_entrada, TINApp.FechaServidor, 0, "", Orden_servicio, "Entrada Generada por Devoluci�n")
                If Not response.ErrorFound Then
                    response = Me.oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega_Orden, Me.ConceptoEntradaReparacion, folio_movimiento_entrada, TINApp.FechaServidor, 1, Articulo, 1, Ultimo_Costo, Ultimo_Costo, -1, 0)
                    If Not response.ErrorFound Then
                        response = Me.oDevolucionesProveedor.ActualizaOrdenServicio(Orden_servicio, folio_movimiento_entrada)

                        If Not response.ErrorFound Then
                            response = oArticulosExistencias.Actualizar(Articulo, Bodega_Orden, "GA", -1, 0)
                        End If
                    End If

                End If
            End If

        Else
            ShowMessage(MessageType.MsgError, "Error al verificar la salida por reparacion", , response.Ex)
            'Exit For
        End If
        'Next
        Return response
    End Function
    Private Function GeneraMovimientoInventarioDetalles() As Events
        Dim Costo_Detalle As Double = 0
        Dim response As New Events
        Dim partida As Long = 0

        With Me.tmaDevolucionesProveedor
            .MoveFirst()
            Do While Not .EOF


                Select Case .CurrentAction
                    Case Actions.None
                        response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.SelectedRow, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, "I", "devoluciones")
                        If Not response.ErrorFound Then

                            ' =========================================================

                            Dim oDataSet As DataSet

                            If AfectaCostos Then
                                ' Traer costo del historial de costos
                                response = oHistoricoCostos.UltimoCostoArticulo(.Item("articulo"), Bodega)
                                If Not response.ErrorFound Then
                                    oDataSet = response.Value
                                    If oDataSet.Tables(0).Rows.Count > 0 Then
                                        Costo_Detalle = oDataSet.Tables(0).Rows(0).Item("costo")
                                        tmaDevolucionesProveedor.Item("folio_historico_costo") = oDataSet.Tables(0).Rows(0).Item("folio")
                                    End If
                                End If
                            End If

                            oDataSet = Nothing


                            ' =========================================================


                            ' DAM - COMENTADO EL DIA 27 DE ABRIL POR QUE TOMARA EL COSTO CON FLETE
                            ' DAM - SE VOLVIO A UTILIZAR ESTE PROCESO EL DIA 13 NOV 10, 

                            '' ACTUALIZO EL HISTORICO DEL ARTICULO y OBTENGO EL COSTO HISTORICO EN ESE MOMENTO
                            If tmaDevolucionesProveedor.Item("folio_historico_costo") > 0 Then
                                response = oHistoricoCostos.ActualizaSaldo(tmaDevolucionesProveedor.Item("folio_historico_costo"), -1)

                                response = oHistoricoCostos.DespliegaDatos(tmaDevolucionesProveedor.Item("folio_historico_costo"))
                                If CType(response.Value, DataSet).Tables(0).Rows.Count > 0 Then Costo_Detalle = CType(response.Value, DataSet).Tables(0).Rows(0).Item("costo")
                            Else
                                Costo_Detalle = Me.tmaDevolucionesProveedor.Item("costo")
                            End If


                            partida = partida + 1
                            ' INSERTO EL DETALLE DEL MOVIMIENTO CON EL COSTO ACTUALIZADO
                            response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, Date.Now.Date, .Item("partida"), .Item("articulo"), .Item("cantidad"), Costo_Detalle, Costo_Detalle * .Item("cantidad"), .Item("folio_historico_costo"), .Item("costo_flete"))


                            If tmaDevolucionesProveedor.Item("maneja_series") And Comunes.clsUtilerias.UsarSeries Then
                                ' INSERTO LAS SERIES 

                                response = oMovimientosInventarioDetalleSeries.Insertar(tmaDevolucionesProveedor.SelectedRow, Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, tmaDevolucionesProveedor.Item("Partida"), tmaDevolucionesProveedor.Item("articulo"))

                            End If

                            ''INSERTO EL DETALLE DE LA DEVOLUCION 
                            'Response = Me.oDevolucionesProveedordetalle.Insertar(Me.tmaDevolucionesProveedor.SelectedRow, Bodega, Me.clcDevolucion.Value)

                            Me.oDevolucionesProveedordetalle.Actualizar(Me.tmaDevolucionesProveedor.SelectedRow, Bodega, Me.clcDevolucion.Value)

                            'DAM PARA GENERAR ENTRADAS DE REPARACION
                            If Not response.ErrorFound Then
                                response = VerificaSalidaGeneraEntradaOrdenServicioReparacion(CType(.Item("orden_servicio"), Long))
                            End If
                        Else
                            Exit Do
                        End If
                        'Case Actions.Update
                        '    Response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.SelectedRow, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, "U", "devoluciones")
                        '    If Not Response.ErrorFound Then

                        '        If tmaDevolucionesProveedor.Item("maneja_series") And Comunes.clsUtilerias.UsarSeries Then
                        '            Response = oMovimientosInventarioDetalleSeries.Actualizar(tmaDevolucionesProveedor.SelectedRow, Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, tmaDevolucionesProveedor.Item("Partida"), tmaDevolucionesProveedor.Item("articulo"))
                        '        End If

                        '        Response = oMovimientosInventarioDetalle.Actualizar(tmaDevolucionesProveedor.SelectedRow, Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, Date.Now.Date)


                        '        If Not Response.ErrorFound Then Response = Me.oDevolucionesProveedordetalle.Actualizar(Me.tmaDevolucionesProveedor.SelectedRow, Bodega, Me.clcDevolucion.Value)
                        '    Else
                        '        Exit Do
                        '    End If
                    Case Actions.Delete


                        If Me.Action = Actions.Delete Then

                            ' INSERTA EL DETALLE DEL MOVIMIENTO DE LA CANCELACION DE DEVOLUCION
                            response = oMovimientosInventarioDetalle.Insertar(tmaDevolucionesProveedor.SelectedRow, Comunes.Common.Sucursal_Actual, Bodega, ConceptoCancelacionDevolucionProveedor, folio_movimiento, Date.Now.Date, System.DBNull.Value)

                            If tmaDevolucionesProveedor.Item("maneja_series") And Comunes.clsUtilerias.UsarSeries Then
                                ' INSERTA LAS SERIES DE LA CANCELACION DE DEVOLUCION
                                response = oMovimientosInventarioDetalleSeries.Insertar(tmaDevolucionesProveedor.SelectedRow, Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoCancelacionDevolucionProveedor, folio_movimiento, tmaDevolucionesProveedor.Item("Partida"), tmaDevolucionesProveedor.Item("articulo"))
                            End If


                        Else

                            If tmaDevolucionesProveedor.Item("maneja_series") And Comunes.clsUtilerias.UsarSeries Then
                                ' ELIMINA LAS SERIES
                                response = oMovimientosInventarioDetalleSeries.Eliminar(Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, tmaDevolucionesProveedor.Item("Partida"), tmaDevolucionesProveedor.Item("articulo"))
                            End If

                            ' ELIMINA EL DETALLE DEL MOVIMIENTO
                            If Not response.ErrorFound Then response = oMovimientosInventarioDetalle.Eliminar(Bodega, Me.ConceptoDevolucionProveedor, folio_movimiento, Me.tmaDevolucionesProveedor.Item("partida"))


                            ' ELIMINA EL DETALLE DE LA DEVOLUCION
                            If Not response.ErrorFound Then response = Me.oDevolucionesProveedordetalle.Eliminar(Bodega, Me.clcDevolucion.Value, Me.tmaDevolucionesProveedor.Item("partida"))

                        End If
                End Select
                If response.ErrorFound Then Exit Do
                .MoveNext()
            Loop
        End With

        Return response
    End Function
    Private Sub ImprimeEtiquetaDevolucion()
        Dim response As New Events
        response = oReportes.DevolucionEtiqueta(clcDevolucion.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Devoluci�n de Mercanc�a no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptImpresionEtiquetaDevolucion

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Etiqueta de la Devoluci�n", oReport, , , , True)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

#End Region

    Private Sub frmDevolucionesProveedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If VerificaPermisoExtendido(Me.OwnerForm.MenuOption.Name, "FECHA_DEV_PROV") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If

     

    End Sub


End Class
