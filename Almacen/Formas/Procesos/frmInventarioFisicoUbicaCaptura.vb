Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmInventarioFisicoUbicaCaptura
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grSeries As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvSeries As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblN_Articulo As System.Windows.Forms.Label
    Friend WithEvents clcConteo1 As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcConteo2 As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblN_Unidad As System.Windows.Forms.Label
    Friend WithEvents tmaSeries As Dipros.Windows.TINMaster
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpContador As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpSupervisor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpUbicacion As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblN_Ubicacion As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmInventarioFisicoUbicaCaptura))
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpUbicacion = New Dipros.Editors.TINMultiLookup
        Me.clcConteo1 = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.tmaSeries = New Dipros.Windows.TINMaster
        Me.grSeries = New DevExpress.XtraGrid.GridControl
        Me.grvSeries = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblN_Articulo = New System.Windows.Forms.Label
        Me.lblN_Unidad = New System.Windows.Forms.Label
        Me.clcConteo2 = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpContador = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpSupervisor = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkManejaSeries = New DevExpress.XtraEditors.CheckEdit
        Me.lblN_Ubicacion = New System.Windows.Forms.Label
        CType(Me.clcConteo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcConteo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(45, 64)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 2
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Ar&t�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "descripcion_corta"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 64)
        Me.lkpArticulo.MultiSelect = True
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = "(Todos)"
        Me.lkpArticulo.PopupWidth = CType(330, Long)
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = True
        Me.lkpArticulo.Size = New System.Drawing.Size(280, 20)
        Me.lkpArticulo.TabIndex = 3
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = "Art�culo"
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "&Ubicaci�n:"
        '
        'lkpUbicacion
        '
        Me.lkpUbicacion.AllowAdd = False
        Me.lkpUbicacion.AutoReaload = False
        Me.lkpUbicacion.DataSource = Nothing
        Me.lkpUbicacion.DefaultSearchField = ""
        Me.lkpUbicacion.DisplayMember = "descripcion"
        Me.lkpUbicacion.EditValue = Nothing
        Me.lkpUbicacion.InitValue = Nothing
        Me.lkpUbicacion.Location = New System.Drawing.Point(104, 40)
        Me.lkpUbicacion.MultiSelect = True
        Me.lkpUbicacion.Name = "lkpUbicacion"
        Me.lkpUbicacion.NullText = "(Todos)"
        Me.lkpUbicacion.PopupWidth = CType(350, Long)
        Me.lkpUbicacion.Required = False
        Me.lkpUbicacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUbicacion.SearchMember = ""
        Me.lkpUbicacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUbicacion.SelectAll = True
        Me.lkpUbicacion.Size = New System.Drawing.Size(280, 20)
        Me.lkpUbicacion.TabIndex = 1
        Me.lkpUbicacion.Tag = "ubicacion"
        Me.lkpUbicacion.ToolTip = "Ubicaci�n"
        Me.lkpUbicacion.ValueMember = "ubicacion"
        '
        'clcConteo1
        '
        Me.clcConteo1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcConteo1.Location = New System.Drawing.Point(104, 88)
        Me.clcConteo1.Name = "clcConteo1"
        Me.clcConteo1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcConteo1.Size = New System.Drawing.Size(88, 20)
        Me.clcConteo1.TabIndex = 5
        Me.clcConteo1.Tag = "conteo1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(37, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Co&nteo 1:"
        '
        'tmaSeries
        '
        Me.tmaSeries.BackColor = System.Drawing.Color.White
        Me.tmaSeries.CanDelete = True
        Me.tmaSeries.CanInsert = True
        Me.tmaSeries.CanUpdate = True
        Me.tmaSeries.Grid = Nothing
        Me.tmaSeries.Location = New System.Drawing.Point(8, 216)
        Me.tmaSeries.Name = "tmaSeries"
        Me.tmaSeries.Size = New System.Drawing.Size(376, 25)
        Me.tmaSeries.TabIndex = 13
        Me.tmaSeries.Title = ""
        Me.tmaSeries.UpdateTitle = "una Serie"
        Me.tmaSeries.Visible = False
        '
        'grSeries
        '
        '
        'grSeries.EmbeddedNavigator
        '
        Me.grSeries.EmbeddedNavigator.Name = ""
        Me.grSeries.Location = New System.Drawing.Point(8, 200)
        Me.grSeries.MainView = Me.grvSeries
        Me.grSeries.Name = "grSeries"
        Me.grSeries.Size = New System.Drawing.Size(376, 184)
        Me.grSeries.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSeries.TabIndex = 12
        Me.grSeries.Text = "GridControl1"
        '
        'grvSeries
        '
        Me.grvSeries.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSerie})
        Me.grvSeries.GridControl = Me.grSeries
        Me.grvSeries.Name = "grvSeries"
        Me.grvSeries.OptionsCustomization.AllowGroup = False
        Me.grvSeries.OptionsMenu.EnableFooterMenu = False
        Me.grvSeries.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvSeries.OptionsNavigation.AutoFocusNewRow = True
        Me.grvSeries.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvSeries.OptionsView.ShowGroupPanel = False
        Me.grvSeries.OptionsView.ShowIndicator = False
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.HeaderStyleName = "Style1"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        Me.grcSerie.VisibleIndex = 0
        '
        'lblN_Articulo
        '
        Me.lblN_Articulo.Location = New System.Drawing.Point(288, 88)
        Me.lblN_Articulo.Name = "lblN_Articulo"
        Me.lblN_Articulo.Size = New System.Drawing.Size(16, 23)
        Me.lblN_Articulo.TabIndex = 14
        Me.lblN_Articulo.Tag = "descripcion_corta"
        Me.lblN_Articulo.Text = "a"
        '
        'lblN_Unidad
        '
        Me.lblN_Unidad.Location = New System.Drawing.Point(344, 88)
        Me.lblN_Unidad.Name = "lblN_Unidad"
        Me.lblN_Unidad.Size = New System.Drawing.Size(16, 23)
        Me.lblN_Unidad.TabIndex = 16
        Me.lblN_Unidad.Tag = "unidad"
        Me.lblN_Unidad.Text = "u"
        '
        'clcConteo2
        '
        Me.clcConteo2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcConteo2.Location = New System.Drawing.Point(104, 112)
        Me.clcConteo2.Name = "clcConteo2"
        Me.clcConteo2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcConteo2.Size = New System.Drawing.Size(88, 20)
        Me.clcConteo2.TabIndex = 7
        Me.clcConteo2.Tag = "conteo2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(37, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Tag = ""
        Me.Label3.Text = "C&ontador:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpContador
        '
        Me.lkpContador.AllowAdd = False
        Me.lkpContador.AutoReaload = False
        Me.lkpContador.DataSource = Nothing
        Me.lkpContador.DefaultSearchField = ""
        Me.lkpContador.DisplayMember = "nombre"
        Me.lkpContador.EditValue = Nothing
        Me.lkpContador.InitValue = Nothing
        Me.lkpContador.Location = New System.Drawing.Point(104, 136)
        Me.lkpContador.MultiSelect = False
        Me.lkpContador.Name = "lkpContador"
        Me.lkpContador.NullText = ""
        Me.lkpContador.PopupWidth = CType(400, Long)
        Me.lkpContador.Required = False
        Me.lkpContador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpContador.SearchMember = ""
        Me.lkpContador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpContador.SelectAll = False
        Me.lkpContador.Size = New System.Drawing.Size(280, 20)
        Me.lkpContador.TabIndex = 9
        Me.lkpContador.Tag = "contador"
        Me.lkpContador.ToolTip = "Contador"
        Me.lkpContador.ValueMember = "empleado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Tag = ""
        Me.Label4.Text = "S&upervisor:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSupervisor
        '
        Me.lkpSupervisor.AllowAdd = False
        Me.lkpSupervisor.AutoReaload = False
        Me.lkpSupervisor.DataSource = Nothing
        Me.lkpSupervisor.DefaultSearchField = ""
        Me.lkpSupervisor.DisplayMember = "nombre"
        Me.lkpSupervisor.EditValue = Nothing
        Me.lkpSupervisor.InitValue = Nothing
        Me.lkpSupervisor.Location = New System.Drawing.Point(104, 160)
        Me.lkpSupervisor.MultiSelect = False
        Me.lkpSupervisor.Name = "lkpSupervisor"
        Me.lkpSupervisor.NullText = ""
        Me.lkpSupervisor.PopupWidth = CType(400, Long)
        Me.lkpSupervisor.Required = False
        Me.lkpSupervisor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSupervisor.SearchMember = ""
        Me.lkpSupervisor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSupervisor.SelectAll = False
        Me.lkpSupervisor.Size = New System.Drawing.Size(280, 20)
        Me.lkpSupervisor.TabIndex = 11
        Me.lkpSupervisor.Tag = "supervisor"
        Me.lkpSupervisor.ToolTip = "Supervisor"
        Me.lkpSupervisor.ValueMember = "empleado"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Con&teo 2:"
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.Location = New System.Drawing.Point(368, 88)
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'chkManejaSeries.Properties
        '
        Me.chkManejaSeries.Properties.Caption = "Maneja Series"
        Me.chkManejaSeries.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkManejaSeries.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkManejaSeries.Size = New System.Drawing.Size(16, 19)
        Me.chkManejaSeries.TabIndex = 17
        Me.chkManejaSeries.Tag = "maneja_series"
        '
        'lblN_Ubicacion
        '
        Me.lblN_Ubicacion.Location = New System.Drawing.Point(312, 88)
        Me.lblN_Ubicacion.Name = "lblN_Ubicacion"
        Me.lblN_Ubicacion.Size = New System.Drawing.Size(24, 23)
        Me.lblN_Ubicacion.TabIndex = 15
        Me.lblN_Ubicacion.Tag = "n_ubicacion"
        Me.lblN_Ubicacion.Text = "ub"
        '
        'frmInventarioFisicoUbicaCaptura
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 392)
        Me.Controls.Add(Me.lblN_Ubicacion)
        Me.Controls.Add(Me.chkManejaSeries)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lkpSupervisor)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.grSeries)
        Me.Controls.Add(Me.lkpContador)
        Me.Controls.Add(Me.clcConteo2)
        Me.Controls.Add(Me.lblN_Unidad)
        Me.Controls.Add(Me.lblN_Articulo)
        Me.Controls.Add(Me.tmaSeries)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcConteo1)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lkpUbicacion)
        Me.Name = "frmInventarioFisicoUbicaCaptura"
        Me.Text = "frmInventarioFisicoUbicaCaptura"
        Me.Controls.SetChildIndex(Me.lkpUbicacion, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcConteo1, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.tmaSeries, 0)
        Me.Controls.SetChildIndex(Me.lblN_Articulo, 0)
        Me.Controls.SetChildIndex(Me.lblN_Unidad, 0)
        Me.Controls.SetChildIndex(Me.clcConteo2, 0)
        Me.Controls.SetChildIndex(Me.lkpContador, 0)
        Me.Controls.SetChildIndex(Me.grSeries, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpSupervisor, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.chkManejaSeries, 0)
        Me.Controls.SetChildIndex(Me.lblN_Ubicacion, 0)
        CType(Me.clcConteo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcConteo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Private myDS As New Data.DataSet
    Private myArticulos As Data.DataTable = myDS.Tables.Add()
    Private myDr As Data.DataRow

#Region "Series"
    Private dsSeries As New DataSet
#End Region

#End Region

#Region "Declaraciones"
    'Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    'Private oGruposArticulos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oInventarioFisicoDetalle As New VillarrealBusiness.clsInventarioFisicoDetalle
    Private oInventarioFisicoDetalleSeries As New VillarrealBusiness.clsInventarioFisicoDetalleSeries
    Private oEmpleados As New VillarrealBusiness.clsEmpleados
    Private oUbicaciones As New VillarrealBusiness.clsUbicaciones

#Region "Propiedades"
    Private ReadOnly Property Contador() As Long
        Get
            Return PreparaValorLookup(Me.lkpContador)
        End Get
    End Property
    Private ReadOnly Property Supervisor() As Long
        Get
            Return PreparaValorLookup(Me.lkpSupervisor)
        End Get
    End Property
#End Region
#End Region

#Region "Eventos de la Forma"
    Private Sub frmInventarioFisicoUbicaCaptura_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                InsertaArticulos()
            Case Actions.Update
                ActualizaArticulo()
            Case Actions.Delete
                BorraArticulo()
        End Select
    End Sub

    Private Sub frmInventarioFisicoUbicaCaptura_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oInventarioFisicoDetalle.Validacion(Action, Me.lkpArticulo.SelectedCount, Contador, Supervisor)
    End Sub

    Private Sub frmInventarioFisicoUbicaCaptura_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        OcultaControles()

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmInventarioFisicoUbicaCaptura_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lkpUbicacion.EditValue = OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0).Item("ubicacion")
        Me.lkpArticulo.EditValue = OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0).Item("articulo")
    End Sub

    Private Sub frmInventarioFisicoUbicaCaptura_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oDataAux As DataSet
        Select Case Action
            Case Actions.Insert
                Me.clcConteo2.Enabled = False
                Me.grSeries.Enabled = False
                lkpUbicacion_EditValueChanged(Nothing, Nothing)
            Case Actions.Update
                InhabilitaControles()
                'si el art maneja series deshabilito conteo2
                If Me.chkManejaSeries.Checked = True Then
                    Me.clcConteo2.Enabled = False
                    'despliega series si tiene ya capturadas
                    oDataAux = CType(OwnerForm, frmInventarioFisicoUbica).pArticulosSeries(Me.lkpArticulo.EditValue).Series
                    If oDataAux Is Nothing Then
                        grSeries.DataSource = Nothing
                    Else
                        grSeries.DataSource = CType(OwnerForm, frmInventarioFisicoUbica).pArticulosSeries(Me.lkpArticulo.EditValue).Series.Tables(0)
                    End If
                    'agrega una fila al grid
                    InsertaNuevaFila()
                Else    'sino maneja series deshabilito el grid
                    Me.grSeries.Enabled = False
                End If
                'si el inventario ya fue aplicado
                If OwnerForm.banAplicado Then
                    DeshabilitaControles_InventarioAplicado()
                End If
            Case Actions.Delete
                InhabilitaControles()
                If Me.chkManejaSeries.Checked = True Then
                    'despliega series si tiene ya capturadas
                    oDataAux = CType(OwnerForm, frmInventarioFisicoUbica).pArticulosSeries(Me.lkpArticulo.EditValue).Series
                    If oDataAux Is Nothing Then
                        grSeries.DataSource = Nothing
                    Else
                        grSeries.DataSource = CType(OwnerForm, frmInventarioFisicoUbica).pArticulosSeries(Me.lkpArticulo.EditValue).Series.Tables(0)
                    End If
                    'agrega una fila al grid
                    InsertaNuevaFila()
                End If
        End Select
    End Sub
#End Region

#Region "Eventos de Controles"

#Region "Lookup"
    Private Sub lkpUbicacion_Format() Handles lkpUbicacion.Format
        Comunes.clsFormato.for_ubicaciones_grl(Me.lkpUbicacion)
    End Sub
    Private Sub lkpUbicacion_LoadData(ByVal Initialize As Boolean) Handles lkpUbicacion.LoadData
        Dim Response As New Events
        Response = oUbicaciones.Lookup(OwnerForm.Bodega)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpUbicacion.SelectAll = True
            Me.lkpUbicacion.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpUbicacion_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpUbicacion.EditValueChanged
        lkpArticulo.EditValue = Nothing
        lkpArticulo_LoadData(True)
    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        Response = oArticulos.MultiLookup_xUbicaciones(Me.lkpUbicacion.ToXML, OwnerForm.Bodega)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.SelectAll = True
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_ubicaciones_grl(Me.lkpArticulo)
    End Sub

    Private Sub lkpContador_LoadData(ByVal Initialize As Boolean) Handles lkpContador.LoadData
        Dim Response As New Events
        Response = oEmpleados.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpContador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpContador_Format() Handles lkpContador.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpContador)
    End Sub

    Private Sub lkpSupervisor_LoadData(ByVal Initialize As Boolean) Handles lkpSupervisor.LoadData
        Dim Response As New Events
        Response = oEmpleados.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSupervisor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpSupervisor_Format() Handles lkpSupervisor.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpSupervisor)
    End Sub

#End Region

    Private Sub grvSeries_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvSeries.KeyDown
        If e.KeyCode = e.KeyCode.Enter Then
            'Actualiza valores
            Me.grvSeries.CloseEditor()
            Me.grvSeries.UpdateCurrentRow()

            'Valida si a una fila se le borro su numero de serie y no es la ultima, esta se elimina
            If Me.grvSeries.GetRowCellValue(Me.grvSeries.FocusedRowHandle, Me.grcSerie) = "" And (Me.grvSeries.RowCount - 1 <> Me.grvSeries.FocusedRowHandle) Then
                Me.grvSeries.DeleteRow(Me.grvSeries.FocusedRowHandle)
                Me.clcConteo2.EditValue -= 1
                Exit Sub
            End If

            'Valida si la fila no tiene capturado un numero de serie no se permite agregar una nueva fila
            If Me.grvSeries.GetRowCellValue(Me.grvSeries.FocusedRowHandle, Me.grcSerie) = "" Then Exit Sub

            'Agrega nueva fila si estoy posicionado en la ultima fila
            If Me.grvSeries.RowCount - 1 = Me.grvSeries.FocusedRowHandle Then
                InsertaNuevaFila()
                Me.clcConteo2.EditValue += 1
            End If
        End If
    End Sub

#End Region

#Region "Funcionalidad"
    
    Private Sub OcultaControles()
        Me.chkManejaSeries.Visible = False
        Me.lblN_Ubicacion.Visible = False
        Me.lblN_Articulo.Visible = False
        Me.lblN_Unidad.Visible = False
    End Sub

    Private Sub InsertaNuevaFila()
        If Me.grvSeries.RowCount = 0 Then
            Me.grSeries.DataSource = Nothing
            Dim oEvents As New Events
            oEvents = oInventarioFisicoDetalleSeries.Listado(0, 0)
            Me.grSeries.DataSource = CType(oEvents.Value, DataSet).Tables(0)
        End If

        'si el grid ya tiene filas, valida si la ultima fila tiene capturado 
        'un numero de serie, sino no se permite agregar una nueva fila
        If Me.grvSeries.RowCount > 0 Then
            Me.grvSeries.MoveLast()
            If Me.grvSeries.GetRowCellValue(Me.grvSeries.FocusedRowHandle, Me.grcSerie) = "" Then Exit Sub
        End If

        Me.grvSeries.AddNewRow()
        Me.grvSeries.FocusedRowHandle = 0
        Me.grvSeries.MoveLast()
        Me.grvSeries.SetRowCellValue(Me.grvSeries.FocusedRowHandle, Me.grcSerie, "")
    End Sub

    Private Sub InhabilitaControles()
        Me.lkpUbicacion.Enabled = False
        Me.lkpArticulo.Enabled = False
    End Sub

#Region "Series"
    Private Sub ActualizaSeries()
        If Action <> Actions.Update Then Exit Sub

        Dim Aux As New ArticulosSeriesUbica
        Select Case Action
            Case Actions.Update
                Aux.Articulo = Me.lkpArticulo.EditValue
                Aux.Series = dsSeries
                CType(OwnerForm, frmInventarioFisicoUbica).pArticulosSeries(Aux.Articulo) = Aux
        End Select
    End Sub

    Private Sub RecuperaSeries()
        Me.grvSeries.UpdateCurrentRow()
        Me.grvSeries.CloseEditor()

        Dim i As Integer
        Dim cont As Integer = 0
        For i = 0 To Me.grvSeries.RowCount - 1
            If Me.grvSeries.GetRowCellValue(i, Me.grcSerie) <> "" Then cont += 1
        Next
        Me.clcConteo2.EditValue = cont

        dsSeries = CType(Me.grvSeries.DataSource, DataView).Table.DataSet
    End Sub

#End Region

    Private Sub InsertaArticulos()
        Dim response As Events
        Dim odataset As DataSet
        Dim i As Integer
        response = oInventarioFisicoDetalle.ListadoArticulosXUbicacion(Me.lkpArticulo.ToXML, Me.clcConteo1.Value, Me.clcConteo2.Value, Contador, Supervisor, Me.lkpUbicacion.ToXML, OwnerForm.Bodega)
        If Not response.ErrorFound Then
            odataset = response.Value
            CreaDatasetArticulos()
            With OwnerForm.MasterControl
                'si no hay articulos en el gridview introduce todos
                If .View.RowCount = 0 Then
                    For i = 0 To odataset.Tables(0).Rows.Count - 1
                        myDS.Tables(0).Rows(0).Item("ubicacion") = odataset.Tables(0).Rows(i).Item("ubicacion")
                        myDS.Tables(0).Rows(0).Item("n_ubicacion") = odataset.Tables(0).Rows(i).Item("n_ubicacion")
                        myDS.Tables(0).Rows(0).Item("articulo") = odataset.Tables(0).Rows(i).Item("articulo")
                        myDS.Tables(0).Rows(0).Item("descripcion_corta") = odataset.Tables(0).Rows(i).Item("descripcion_corta")
                        myDS.Tables(0).Rows(0).Item("unidad") = odataset.Tables(0).Rows(i).Item("unidad")
                        myDS.Tables(0).Rows(0).Item("conteo1") = odataset.Tables(0).Rows(i).Item("conteo1")
                        myDS.Tables(0).Rows(0).Item("conteo2") = odataset.Tables(0).Rows(i).Item("conteo2")
                        myDS.Tables(0).Rows(0).Item("contador") = odataset.Tables(0).Rows(i).Item("contador")
                        myDS.Tables(0).Rows(0).Item("supervisor") = odataset.Tables(0).Rows(i).Item("supervisor")
                        myDS.Tables(0).Rows(0).Item("maneja_series") = odataset.Tables(0).Rows(i).Item("maneja_series")
                        .AddRow(myDS)
                    Next
                Else    'si hay articulos en el grid introduce solo los que no existen
                    Dim j As Long
                    Dim banAgregar As Boolean
                    Dim articulo_tma As Long
                    Dim ubicacion_tma As String
                    Dim articulo_listado As Long
                    Dim ubicacion_listado As String
                    'recorre articulos del listado
                    For i = 0 To odataset.Tables(0).Rows.Count - 1
                        articulo_listado = odataset.Tables(0).Rows(i).Item("articulo")
                        ubicacion_listado = odataset.Tables(0).Rows(i).Item("ubicacion")
                        banAgregar = True
                        'recorre articulos del tma
                        For j = 0 To .View.RowCount - 1
                            articulo_tma = .View.GetDataRow(j).Item("articulo")
                            ubicacion_tma = .View.GetDataRow(j).Item("ubicacion")
                            'si el articulo del listado existe en el tma, este no se agrega 
                            If articulo_listado = articulo_tma And ubicacion_listado = ubicacion_tma Then
                                banAgregar = False
                                Exit For
                            End If
                        Next
                        If banAgregar Then
                            myDS.Tables(0).Rows(0).Item("ubicacion") = odataset.Tables(0).Rows(i).Item("ubicacion")
                            myDS.Tables(0).Rows(0).Item("n_ubicacion") = odataset.Tables(0).Rows(i).Item("n_ubicacion")
                            myDS.Tables(0).Rows(0).Item("articulo") = odataset.Tables(0).Rows(i).Item("articulo")
                            myDS.Tables(0).Rows(0).Item("descripcion_corta") = odataset.Tables(0).Rows(i).Item("descripcion_corta")
                            myDS.Tables(0).Rows(0).Item("unidad") = odataset.Tables(0).Rows(i).Item("unidad")
                            myDS.Tables(0).Rows(0).Item("conteo1") = odataset.Tables(0).Rows(i).Item("conteo1")
                            myDS.Tables(0).Rows(0).Item("conteo2") = odataset.Tables(0).Rows(i).Item("conteo2")
                            myDS.Tables(0).Rows(0).Item("contador") = odataset.Tables(0).Rows(i).Item("contador")
                            myDS.Tables(0).Rows(0).Item("supervisor") = odataset.Tables(0).Rows(i).Item("supervisor")
                            myDS.Tables(0).Rows(0).Item("maneja_series") = odataset.Tables(0).Rows(i).Item("maneja_series")
                            .AddRow(myDS)
                        End If
                    Next
                End If
            End With
        End If
    End Sub

    Private Sub ActualizaArticulo()
        If Me.chkManejaSeries.Checked Then
            RecuperaSeries()
        End If

        With OwnerForm.MasterControl
            .UpdateRow(Me.DataSource)
        End With

        If Me.chkManejaSeries.Checked Then
            ActualizaSeries()
        End If
    End Sub

    Private Sub BorraArticulo()
        If Me.chkManejaSeries.Checked Then
            RecuperaSeries()
        End If

        With OwnerForm.MasterControl
            .DeleteRow()
        End With

        If Me.chkManejaSeries.Checked Then
            ActualizaSeries()
        End If
    End Sub

    Private Sub CreaDatasetArticulos()
        With myArticulos
            .Columns.Add("ubicacion", Type.GetType("System.String"))
            .Columns.Add("n_ubicacion", Type.GetType("System.String"))
            .Columns.Add("articulo", Type.GetType("System.Int64"))
            .Columns.Add("descripcion_corta", Type.GetType("System.String"))
            .Columns.Add("unidad", Type.GetType("System.String"))
            .Columns.Add("conteo1", Type.GetType("System.Double"))
            .Columns.Add("conteo2", Type.GetType("System.Double"))
            .Columns.Add("contador", Type.GetType("System.Int64"))
            .Columns.Add("supervisor", Type.GetType("System.Int64"))
            .Columns.Add("maneja_series", Type.GetType("System.Boolean"))
        End With
        myDr = myArticulos.NewRow()
        myDr("ubicacion") = ""
        myDr("n_ubicacion") = ""
        myDr("articulo") = 0
        myDr("descripcion_corta") = ""
        myDr("unidad") = ""
        myDr("conteo1") = 0
        myDr("conteo2") = 0
        myDr("contador") = 0
        myDr("supervisor") = 0
        myDr("maneja_series") = 0
        myArticulos.Rows.Add(myDr)
    End Sub

    Private Sub DeshabilitaControles_InventarioAplicado()
        Me.tbrTools.Buttons.Item(0).Enabled = False
        Me.clcConteo1.Enabled = False
        Me.clcConteo2.Enabled = False
        Me.lkpContador.Enabled = False
        Me.lkpSupervisor.Enabled = False
        Me.grSeries.Enabled = False
    End Sub

#End Region

End Class
