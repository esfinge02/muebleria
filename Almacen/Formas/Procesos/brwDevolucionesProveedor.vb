Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class brwDevolucionesProveedor
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents cmdEnviar As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cmdEnviar = New DevExpress.XtraEditors.SimpleButton
        Me.FilterPanel.SuspendLayout()
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.cmdEnviar)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(768, 8)
        Me.FilterPanel.Controls.SetChildIndex(Me.cmdEnviar, 0)
        '
        'trbToolsData
        '
        Me.trbToolsData.Location = New System.Drawing.Point(584, 21)
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        Me.tbrExtended.Size = New System.Drawing.Size(562, 22)
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 397)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(768, 0)
        '
        'cmdEnviar
        '
        Me.cmdEnviar.Location = New System.Drawing.Point(640, 8)
        Me.cmdEnviar.Name = "cmdEnviar"
        Me.cmdEnviar.Size = New System.Drawing.Size(80, 23)
        Me.cmdEnviar.TabIndex = 1
        Me.cmdEnviar.Text = "Enviar"
        Me.cmdEnviar.Visible = False
        '
        'brwDevolucionesProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(768, 397)
        Me.Name = "brwDevolucionesProveedor"
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Declaraciones"
    Private oDevoluciones As New VillarrealBusiness.clsDevolucionesProveedor
#End Region

#Region "Eventos de la Forma"
    Private Sub brwDevolucionesProveedor_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oDevoluciones.Listado()
    End Sub

#End Region

#Region "Eventos de Controles"
    Private Sub cmdEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEnviar.Click
        If CType(Me.View.DataSource, DataView).Count <= 0 Then Exit Sub

        Dim response As New Events
        Dim bodega As String
        Dim devolucion As Long
        bodega = CType(Me.View.DataSource, DataView).Item(View.FocusedRowHandle).Item("bodega")
        devolucion = CType(Me.View.DataSource, DataView).Item(View.FocusedRowHandle).Item("devolucion")
        response = oDevoluciones.ActualizaEnviado(bodega, devolucion)
        If Not response.ErrorFound Then

            brwDevolucionesProveedor_LoadData(response)
            Me.Refresh()
        Else
            ShowMessage(MessageType.MsgError, response.Message, Me.Text, response.Ex)
        End If
    End Sub

#End Region

#Region "Funcionalidad"

#End Region

End Class
