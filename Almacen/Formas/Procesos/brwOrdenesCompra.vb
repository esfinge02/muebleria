Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class brwOrdenesCompra
    Inherits Dipros.Windows.frmTINGridNet

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.FilterPanel.SuspendLayout()
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.cboStatus)
        Me.FilterPanel.Controls.Add(Me.lblStatus)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.cboStatus, 0)
        '
        'FooterPanel
        '
        Me.FooterPanel.Name = "FooterPanel"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(16, 12)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 16)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "&Status:"
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = 0
        Me.cboStatus.Location = New System.Drawing.Point(56, 8)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por surtir y parcialmente surtidas", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Surtidas, Cerradas y Canceladas", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 2, -1)})
        Me.cboStatus.Size = New System.Drawing.Size(192, 20)
        Me.cboStatus.TabIndex = 1
        '
        'brwOrdenesCompra
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(770, 468)
        Me.Name = "brwOrdenesCompra"
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "DIPROS Systems - Declaraciones"
    Private oOrdenesCompra As New VillarrealBusiness.clsOrdenesCompra
#End Region

#Region "DIPROS Systems - Eventos de la Forma"
    Private Sub brwOrdenesCompra_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FooterPanel.Visible = False
    End Sub
    Private Sub brwOrdenesCompra_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oOrdenesCompra.Listado(CInt(Me.cboStatus.EditValue))
    End Sub
#End Region

#Region "DIPROS Systems - Eventos de Controles"
    Private Sub cboStatus_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.EditValueChanged

        Reload()
    End Sub
#End Region

#Region "DIPROS Systems - Funcionalidad"

#End Region


End Class
