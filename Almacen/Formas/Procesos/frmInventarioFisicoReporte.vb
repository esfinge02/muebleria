Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmInventarioFisicoReporte
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnConteo1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnConteo2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnConteo2 = New DevExpress.XtraEditors.SimpleButton
        Me.btnConteo1 = New DevExpress.XtraEditors.SimpleButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnConteo2
        '
        Me.btnConteo2.Location = New System.Drawing.Point(152, 40)
        Me.btnConteo2.Name = "btnConteo2"
        Me.btnConteo2.TabIndex = 7
        Me.btnConteo2.Text = "C&onteo 2"
        '
        'btnConteo1
        '
        Me.btnConteo1.Location = New System.Drawing.Point(64, 40)
        Me.btnConteo1.Name = "btnConteo1"
        Me.btnConteo1.TabIndex = 6
        Me.btnConteo1.Text = "&Conteo 1"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(272, 23)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Selecciona el formato de conteo a imprimir"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frmInventarioFisicoReporte
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(288, 70)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnConteo2)
        Me.Controls.Add(Me.btnConteo1)
        Me.Name = "frmInventarioFisicoReporte"
        Me.Text = "frmInventarioFisicoReporte"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Public OwnerForm As frmInventarioFisicoDepto
    Public OwnerFormUbica As frmInventarioFisicoUbica
    Public banUbica As Boolean
    Public inventario As Long

    Private oReportes As New VillarrealBusiness.Reportes
#End Region

#Region "Eventos de la Forma"
    Private Sub frmInventarioFisicoReporte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub frmInventarioFisicoReporte_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        If banUbica Then
            OwnerFormUbica.Enabled = True
        Else
            OwnerForm.Enabled = True
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnConteo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConteo1.Click
        'Imprime formato para conteo1
        Dim response As New Events
        Dim oDataSet As DataSet
        Dim oReport As Object

        If Not banUbica Then
            response = oReportes.InventarioFisicoConteo1(inventario)
            oReport = New rptInventarioFisicoConteo1
        Else
            response = oReportes.InventarioFisicoConteo1xUbicacion(inventario)
            oReport = New rptInventarioFisicoConteo1_xUbicacion
        End If

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte para Conteo 1 de Inventario F�sico no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Conteo 1 de Inventario F�sico", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

        Me.Close()
    End Sub

    Private Sub btnConteo2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConteo2.Click
        'imprime formato de conteo2
        Dim response As New Events
        Dim oDataSet As DataSet
        Dim oReport As Object

        If Not banUbica Then
            response = oReportes.InventarioFisicoConteo2(inventario)
            oReport = New rptInventarioFisicoConteo2
        Else
            response = oReportes.InventarioFisicoConteo2xUbicacion(inventario)
            oReport = New rptInventarioFisicoConteo2_xUbicacion
        End If

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte para Conteo 2 de Inventario F�sico no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Conteo 2 de Inventario F�sico", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

        Me.Close()
    End Sub
#End Region

#Region "Funcionalidad"

#End Region

End Class
