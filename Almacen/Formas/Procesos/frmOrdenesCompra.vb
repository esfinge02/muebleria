Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmOrdenesCompra
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents clcOrden As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha_Promesa As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Promesa As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFolio_Proveedor As System.Windows.Forms.Label
    Friend WithEvents txtFolio_Proveedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCondiciones As System.Windows.Forms.Label
    Friend WithEvents txtCondiciones As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAgente As System.Windows.Forms.Label
    Friend WithEvents txtAgente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents grOrdenesCompra As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaOrdenesCompra As Dipros.Windows.TINMaster
    Friend WithEvents gdcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gdcCantidadAnterior As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkSobrePedido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcSubTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcIva As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents grcCantidadRecibida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tlbCerrarOrden As System.Windows.Forms.ToolBarButton
    Friend WithEvents tlbReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbrTraerArticulos As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcCantidad_mes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad_anual As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcExistencias As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbrOCSobrePedido As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcFolios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblAnticipo As System.Windows.Forms.Label
    Friend WithEvents clcAnticipo As Dipros.Editors.TINCalcEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblCondicion1 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo5 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo4 As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCondicion4 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo3 As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcPlazo2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCondicion2 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion3 As System.Windows.Forms.Label
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrDatosPago As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbrSurtimientos As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcCantidadSugerida As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesCompra))
        Me.lblOrden = New System.Windows.Forms.Label
        Me.clcOrden = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha_Promesa = New System.Windows.Forms.Label
        Me.dteFecha_Promesa = New DevExpress.XtraEditors.DateEdit
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblFolio_Proveedor = New System.Windows.Forms.Label
        Me.txtFolio_Proveedor = New DevExpress.XtraEditors.TextEdit
        Me.lblCondiciones = New System.Windows.Forms.Label
        Me.txtCondiciones = New DevExpress.XtraEditors.TextEdit
        Me.lblAgente = New System.Windows.Forms.Label
        Me.txtAgente = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.grOrdenesCompra = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gdcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCantidadAnterior = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidadRecibida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad_mes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad_anual = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcExistencias = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidadSugerida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaOrdenesCompra = New Dipros.Windows.TINMaster
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkSobrePedido = New DevExpress.XtraEditors.CheckEdit
        Me.clcSubTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcIva = New DevExpress.XtraEditors.CalcEdit
        Me.clcTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.tlbCerrarOrden = New System.Windows.Forms.ToolBarButton
        Me.tlbReimprimir = New System.Windows.Forms.ToolBarButton
        Me.tbrTraerArticulos = New System.Windows.Forms.ToolBarButton
        Me.tbrOCSobrePedido = New System.Windows.Forms.ToolBarButton
        Me.lblAnticipo = New System.Windows.Forms.Label
        Me.clcAnticipo = New Dipros.Editors.TINCalcEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblCondicion1 = New System.Windows.Forms.Label
        Me.clcPlazo5 = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblCondicion5 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcPlazo4 = New Dipros.Editors.TINCalcEdit
        Me.lblCondicion4 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcPlazo3 = New Dipros.Editors.TINCalcEdit
        Me.clcPlazo2 = New Dipros.Editors.TINCalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcPlazo1 = New Dipros.Editors.TINCalcEdit
        Me.lblCondicion2 = New System.Windows.Forms.Label
        Me.lblCondicion3 = New System.Windows.Forms.Label
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosPago = New DevExpress.XtraNavBar.NavBarItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.tbrSurtimientos = New System.Windows.Forms.ToolBarButton
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Promesa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFolio_Proveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCondiciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSobrePedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcAnticipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcPlazo5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbCerrarOrden, Me.tlbReimprimir, Me.tbrTraerArticulos, Me.tbrOCSobrePedido, Me.tbrSurtimientos})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(19023, 28)
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(81, 10)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(43, 16)
        Me.lblOrden.TabIndex = 0
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "Or&den:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOrden
        '
        Me.clcOrden.EditValue = "0"
        Me.clcOrden.Location = New System.Drawing.Point(128, 10)
        Me.clcOrden.MaxValue = 0
        Me.clcOrden.MinValue = 0
        Me.clcOrden.Name = "clcOrden"
        '
        'clcOrden.Properties
        '
        Me.clcOrden.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcOrden.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcOrden.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden.Properties.Enabled = False
        Me.clcOrden.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOrden.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcOrden.Size = New System.Drawing.Size(40, 19)
        Me.clcOrden.TabIndex = 1
        Me.clcOrden.Tag = "orden"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(68, 34)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(128, 34)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(287, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(632, 8)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 6, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(680, 8)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblFecha_Promesa
        '
        Me.lblFecha_Promesa.AutoSize = True
        Me.lblFecha_Promesa.Location = New System.Drawing.Point(32, 56)
        Me.lblFecha_Promesa.Name = "lblFecha_Promesa"
        Me.lblFecha_Promesa.Size = New System.Drawing.Size(93, 16)
        Me.lblFecha_Promesa.TabIndex = 27
        Me.lblFecha_Promesa.Tag = ""
        Me.lblFecha_Promesa.Text = "F&echa promesa:"
        Me.lblFecha_Promesa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Promesa
        '
        Me.dteFecha_Promesa.EditValue = New Date(2006, 3, 6, 0, 0, 0, 0)
        Me.dteFecha_Promesa.Location = New System.Drawing.Point(128, 56)
        Me.dteFecha_Promesa.Name = "dteFecha_Promesa"
        '
        'dteFecha_Promesa.Properties
        '
        Me.dteFecha_Promesa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Promesa.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Promesa.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Promesa.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Promesa.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Promesa.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Promesa.TabIndex = 28
        Me.dteFecha_Promesa.Tag = "fecha_promesa"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(74, 58)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 6
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(128, 58)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = True
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(287, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 7
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(59, 84)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 8
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Pro&veedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(128, 82)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(400, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = True
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(287, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 9
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'lblFolio_Proveedor
        '
        Me.lblFolio_Proveedor.AutoSize = True
        Me.lblFolio_Proveedor.Location = New System.Drawing.Point(12, 108)
        Me.lblFolio_Proveedor.Name = "lblFolio_Proveedor"
        Me.lblFolio_Proveedor.Size = New System.Drawing.Size(112, 16)
        Me.lblFolio_Proveedor.TabIndex = 10
        Me.lblFolio_Proveedor.Tag = ""
        Me.lblFolio_Proveedor.Text = "Fo&lio de Proveedor:"
        Me.lblFolio_Proveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFolio_Proveedor
        '
        Me.txtFolio_Proveedor.EditValue = ""
        Me.txtFolio_Proveedor.Location = New System.Drawing.Point(128, 106)
        Me.txtFolio_Proveedor.Name = "txtFolio_Proveedor"
        '
        'txtFolio_Proveedor.Properties
        '
        Me.txtFolio_Proveedor.Properties.MaxLength = 50
        Me.txtFolio_Proveedor.Size = New System.Drawing.Size(287, 20)
        Me.txtFolio_Proveedor.TabIndex = 11
        Me.txtFolio_Proveedor.Tag = "folio_proveedor"
        '
        'lblCondiciones
        '
        Me.lblCondiciones.AutoSize = True
        Me.lblCondiciones.Location = New System.Drawing.Point(48, 104)
        Me.lblCondiciones.Name = "lblCondiciones"
        Me.lblCondiciones.Size = New System.Drawing.Size(75, 16)
        Me.lblCondiciones.TabIndex = 31
        Me.lblCondiciones.Tag = ""
        Me.lblCondiciones.Text = "Condicione&s:"
        Me.lblCondiciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCondiciones
        '
        Me.txtCondiciones.EditValue = ""
        Me.txtCondiciones.Location = New System.Drawing.Point(128, 104)
        Me.txtCondiciones.Name = "txtCondiciones"
        '
        'txtCondiciones.Properties
        '
        Me.txtCondiciones.Properties.MaxLength = 80
        Me.txtCondiciones.Size = New System.Drawing.Size(490, 20)
        Me.txtCondiciones.TabIndex = 32
        Me.txtCondiciones.Tag = "condiciones"
        '
        'lblAgente
        '
        Me.lblAgente.AutoSize = True
        Me.lblAgente.Location = New System.Drawing.Point(76, 132)
        Me.lblAgente.Name = "lblAgente"
        Me.lblAgente.Size = New System.Drawing.Size(48, 16)
        Me.lblAgente.TabIndex = 12
        Me.lblAgente.Tag = ""
        Me.lblAgente.Text = "A&gente:"
        Me.lblAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAgente
        '
        Me.txtAgente.EditValue = ""
        Me.txtAgente.Location = New System.Drawing.Point(128, 130)
        Me.txtAgente.Name = "txtAgente"
        '
        'txtAgente.Properties
        '
        Me.txtAgente.Properties.MaxLength = 50
        Me.txtAgente.Size = New System.Drawing.Size(287, 20)
        Me.txtAgente.TabIndex = 13
        Me.txtAgente.Tag = "agente"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(35, 154)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 14
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(128, 154)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(490, 38)
        Me.txtObservaciones.TabIndex = 15
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(80, 202)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 16
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "S&tatus:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grOrdenesCompra
        '
        '
        'grOrdenesCompra.EmbeddedNavigator
        '
        Me.grOrdenesCompra.EmbeddedNavigator.Name = ""
        Me.grOrdenesCompra.Location = New System.Drawing.Point(8, 258)
        Me.grOrdenesCompra.MainView = Me.grvOrdenesCompra
        Me.grOrdenesCompra.Name = "grOrdenesCompra"
        Me.grOrdenesCompra.Size = New System.Drawing.Size(776, 192)
        Me.grOrdenesCompra.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grOrdenesCompra.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesCompra.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesCompra.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesCompra.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesCompra.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesCompra.TabIndex = 20
        Me.grOrdenesCompra.TabStop = False
        Me.grOrdenesCompra.Text = "OrdenesCompra"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gdcPartida, Me.gdcCantidadAnterior, Me.gdcArticulo, Me.grcCantidadRecibida, Me.gdcDescripcion, Me.gdcCantidad, Me.gdcUnidad, Me.gdcPrecio, Me.gdcImporte, Me.grcCantidad_mes, Me.grcCantidad_anual, Me.grcExistencias, Me.grcFolios, Me.grcSobrePedido, Me.grcDepartamento, Me.grcGrupo, Me.grcModelo, Me.grcCantidadSugerida})
        Me.grvOrdenesCompra.GridControl = Me.grOrdenesCompra
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'gdcPartida
        '
        Me.gdcPartida.Caption = "Partida"
        Me.gdcPartida.FieldName = "Partida"
        Me.gdcPartida.Name = "gdcPartida"
        Me.gdcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcPartida.Width = 20
        '
        'gdcCantidadAnterior
        '
        Me.gdcCantidadAnterior.Caption = "Cantidad Anterior"
        Me.gdcCantidadAnterior.DisplayFormat.FormatString = "0"
        Me.gdcCantidadAnterior.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcCantidadAnterior.FieldName = "cantidad_anterior"
        Me.gdcCantidadAnterior.Name = "gdcCantidadAnterior"
        Me.gdcCantidadAnterior.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.gdcCantidadAnterior.Width = 20
        '
        'gdcArticulo
        '
        Me.gdcArticulo.Caption = "Artículo"
        Me.gdcArticulo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcArticulo.FieldName = "articulo"
        Me.gdcArticulo.Name = "gdcArticulo"
        Me.gdcArticulo.Width = 56
        '
        'grcCantidadRecibida
        '
        Me.grcCantidadRecibida.Caption = "Recibida"
        Me.grcCantidadRecibida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidadRecibida.FieldName = "cantidad_recibida"
        Me.grcCantidadRecibida.Name = "grcCantidadRecibida"
        Me.grcCantidadRecibida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidadRecibida.VisibleIndex = 8
        Me.grcCantidadRecibida.Width = 57
        '
        'gdcDescripcion
        '
        Me.gdcDescripcion.Caption = "Descripción"
        Me.gdcDescripcion.FieldName = "descripcion_corta"
        Me.gdcDescripcion.Name = "gdcDescripcion"
        Me.gdcDescripcion.VisibleIndex = 2
        Me.gdcDescripcion.Width = 116
        '
        'gdcCantidad
        '
        Me.gdcCantidad.Caption = "Pedida"
        Me.gdcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcCantidad.FieldName = "cantidad"
        Me.gdcCantidad.Name = "gdcCantidad"
        Me.gdcCantidad.VisibleIndex = 6
        Me.gdcCantidad.Width = 47
        '
        'gdcUnidad
        '
        Me.gdcUnidad.Caption = "Unidad"
        Me.gdcUnidad.FieldName = "nombre_unidad"
        Me.gdcUnidad.Name = "gdcUnidad"
        Me.gdcUnidad.VisibleIndex = 9
        Me.gdcUnidad.Width = 44
        '
        'gdcPrecio
        '
        Me.gdcPrecio.Caption = "Costo"
        Me.gdcPrecio.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcPrecio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcPrecio.FieldName = "costo"
        Me.gdcPrecio.Name = "gdcPrecio"
        Me.gdcPrecio.VisibleIndex = 10
        Me.gdcPrecio.Width = 64
        '
        'gdcImporte
        '
        Me.gdcImporte.Caption = "Importe"
        Me.gdcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcImporte.FieldName = "importe"
        Me.gdcImporte.Name = "gdcImporte"
        Me.gdcImporte.SummaryItem.DisplayFormat = "$###,###,##0.00"
        Me.gdcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.gdcImporte.VisibleIndex = 11
        Me.gdcImporte.Width = 100
        '
        'grcCantidad_mes
        '
        Me.grcCantidad_mes.Caption = "Vta Mes Anterior"
        Me.grcCantidad_mes.FieldName = "cantidad_mes"
        Me.grcCantidad_mes.Name = "grcCantidad_mes"
        Me.grcCantidad_mes.VisibleIndex = 4
        Me.grcCantidad_mes.Width = 95
        '
        'grcCantidad_anual
        '
        Me.grcCantidad_anual.Caption = "Vta Anual"
        Me.grcCantidad_anual.FieldName = "cantidad_anual"
        Me.grcCantidad_anual.Name = "grcCantidad_anual"
        Me.grcCantidad_anual.VisibleIndex = 5
        Me.grcCantidad_anual.Width = 63
        '
        'grcExistencias
        '
        Me.grcExistencias.Caption = "Exist."
        Me.grcExistencias.FieldName = "existencia"
        Me.grcExistencias.Name = "grcExistencias"
        Me.grcExistencias.VisibleIndex = 3
        Me.grcExistencias.Width = 43
        '
        'grcFolios
        '
        Me.grcFolios.Caption = "Folios de Ventas"
        Me.grcFolios.FieldName = "folios_ventas"
        Me.grcFolios.Name = "grcFolios"
        Me.grcFolios.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolios.VisibleIndex = 0
        Me.grcFolios.Width = 62
        '
        'grcSobrePedido
        '
        Me.grcSobrePedido.Caption = "Sobre Pedido"
        Me.grcSobrePedido.FieldName = "sobre_pedido"
        Me.grcSobrePedido.Name = "grcSobrePedido"
        Me.grcSobrePedido.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        Me.grcDepartamento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        Me.grcGrupo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.SortIndex = 0
        Me.grcModelo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 73
        '
        'grcCantidadSugerida
        '
        Me.grcCantidadSugerida.Caption = "Sugerida"
        Me.grcCantidadSugerida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidadSugerida.FieldName = "cantidad_sugerida"
        Me.grcCantidadSugerida.Name = "grcCantidadSugerida"
        Me.grcCantidadSugerida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidadSugerida.VisibleIndex = 7
        Me.grcCantidadSugerida.Width = 60
        '
        'tmaOrdenesCompra
        '
        Me.tmaOrdenesCompra.BackColor = System.Drawing.Color.White
        Me.tmaOrdenesCompra.CanDelete = True
        Me.tmaOrdenesCompra.CanInsert = True
        Me.tmaOrdenesCompra.CanUpdate = True
        Me.tmaOrdenesCompra.Grid = Me.grOrdenesCompra
        Me.tmaOrdenesCompra.Location = New System.Drawing.Point(24, 234)
        Me.tmaOrdenesCompra.Name = "tmaOrdenesCompra"
        Me.tmaOrdenesCompra.Size = New System.Drawing.Size(752, 23)
        Me.tmaOrdenesCompra.TabIndex = 19
        Me.tmaOrdenesCompra.TabStop = False
        Me.tmaOrdenesCompra.Title = "Artículos"
        Me.tmaOrdenesCompra.UpdateTitle = "un Registro"
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "PS"
        Me.cboStatus.Location = New System.Drawing.Point(128, 202)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Surtir", "PS", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Surtida", "SU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Parcialmente Surtida", "PA", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "CA", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cerrada", "CE", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(150, 20)
        Me.cboStatus.TabIndex = 17
        Me.cboStatus.Tag = "status"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(584, 460)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Sub Total :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkSobrePedido
        '
        Me.chkSobrePedido.Location = New System.Drawing.Point(304, 202)
        Me.chkSobrePedido.Name = "chkSobrePedido"
        '
        'chkSobrePedido.Properties
        '
        Me.chkSobrePedido.Properties.Caption = "Sobre Pedido"
        Me.chkSobrePedido.Properties.Enabled = False
        Me.chkSobrePedido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSobrePedido.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.InactiveCaption)
        Me.chkSobrePedido.Size = New System.Drawing.Size(103, 19)
        Me.chkSobrePedido.TabIndex = 18
        Me.chkSobrePedido.Tag = "sobre_pedido"
        '
        'clcSubTotal
        '
        Me.clcSubTotal.Location = New System.Drawing.Point(664, 458)
        Me.clcSubTotal.Name = "clcSubTotal"
        '
        'clcSubTotal.Properties
        '
        Me.clcSubTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.ReadOnly = True
        Me.clcSubTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubTotal.Size = New System.Drawing.Size(112, 20)
        Me.clcSubTotal.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(616, 484)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 16)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "IVA :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'clcIva
        '
        Me.clcIva.Location = New System.Drawing.Point(664, 482)
        Me.clcIva.Name = "clcIva"
        '
        'clcIva.Properties
        '
        Me.clcIva.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.ReadOnly = True
        Me.clcIva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIva.Size = New System.Drawing.Size(112, 20)
        Me.clcIva.TabIndex = 24
        '
        'clcTotal
        '
        Me.clcTotal.Location = New System.Drawing.Point(664, 506)
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.ReadOnly = True
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(112, 20)
        Me.clcTotal.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(609, 508)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 16)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Total :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'tlbCerrarOrden
        '
        Me.tlbCerrarOrden.Text = "Cerrar Orden"
        '
        'tlbReimprimir
        '
        Me.tlbReimprimir.Text = "Reimprimir"
        '
        'tbrTraerArticulos
        '
        Me.tbrTraerArticulos.ImageIndex = 6
        Me.tbrTraerArticulos.Text = "Traer Artículos"
        '
        'tbrOCSobrePedido
        '
        Me.tbrOCSobrePedido.ImageIndex = 7
        Me.tbrOCSobrePedido.Text = "Artículos Sobre Pedido"
        '
        'lblAnticipo
        '
        Me.lblAnticipo.AutoSize = True
        Me.lblAnticipo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnticipo.Location = New System.Drawing.Point(64, 80)
        Me.lblAnticipo.Name = "lblAnticipo"
        Me.lblAnticipo.Size = New System.Drawing.Size(59, 16)
        Me.lblAnticipo.TabIndex = 29
        Me.lblAnticipo.Tag = ""
        Me.lblAnticipo.Text = "A&nticipo:"
        Me.lblAnticipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnticipo
        '
        Me.clcAnticipo.EditValue = "0"
        Me.clcAnticipo.Location = New System.Drawing.Point(128, 80)
        Me.clcAnticipo.MaxValue = 0
        Me.clcAnticipo.MinValue = 0
        Me.clcAnticipo.Name = "clcAnticipo"
        '
        'clcAnticipo.Properties
        '
        Me.clcAnticipo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcAnticipo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnticipo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcAnticipo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnticipo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcAnticipo.Properties.Precision = 2
        Me.clcAnticipo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnticipo.Size = New System.Drawing.Size(95, 19)
        Me.clcAnticipo.TabIndex = 30
        Me.clcAnticipo.Tag = "anticipo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblCondicion1)
        Me.GroupBox2.Controls.Add(Me.clcPlazo5)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.lblCondicion5)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.clcPlazo4)
        Me.GroupBox2.Controls.Add(Me.lblCondicion4)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.clcPlazo3)
        Me.GroupBox2.Controls.Add(Me.clcPlazo2)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.clcPlazo1)
        Me.GroupBox2.Controls.Add(Me.lblCondicion2)
        Me.GroupBox2.Controls.Add(Me.lblCondicion3)
        Me.GroupBox2.Location = New System.Drawing.Point(128, 144)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(273, 168)
        Me.GroupBox2.TabIndex = 33
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Condiciones"
        '
        'lblCondicion1
        '
        Me.lblCondicion1.AutoSize = True
        Me.lblCondicion1.Location = New System.Drawing.Point(82, 27)
        Me.lblCondicion1.Name = "lblCondicion1"
        Me.lblCondicion1.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion1.TabIndex = 34
        Me.lblCondicion1.Tag = ""
        Me.lblCondicion1.Text = "1:"
        Me.lblCondicion1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo5
        '
        Me.clcPlazo5.EditValue = "0"
        Me.clcPlazo5.Location = New System.Drawing.Point(106, 123)
        Me.clcPlazo5.MaxValue = 0
        Me.clcPlazo5.MinValue = 0
        Me.clcPlazo5.Name = "clcPlazo5"
        '
        'clcPlazo5.Properties
        '
        Me.clcPlazo5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo5.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo5.Properties.MaxLength = 4
        Me.clcPlazo5.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo5.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo5.TabIndex = 47
        Me.clcPlazo5.Tag = "plazo_pago_5"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(162, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(28, 16)
        Me.Label5.TabIndex = 36
        Me.Label5.Tag = ""
        Me.Label5.Text = "Días"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCondicion5
        '
        Me.lblCondicion5.AutoSize = True
        Me.lblCondicion5.Location = New System.Drawing.Point(82, 123)
        Me.lblCondicion5.Name = "lblCondicion5"
        Me.lblCondicion5.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion5.TabIndex = 46
        Me.lblCondicion5.Tag = ""
        Me.lblCondicion5.Text = "5:"
        Me.lblCondicion5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(162, 99)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(28, 16)
        Me.Label9.TabIndex = 45
        Me.Label9.Tag = ""
        Me.Label9.Text = "Días"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo4
        '
        Me.clcPlazo4.EditValue = "0"
        Me.clcPlazo4.Location = New System.Drawing.Point(106, 99)
        Me.clcPlazo4.MaxValue = 0
        Me.clcPlazo4.MinValue = 0
        Me.clcPlazo4.Name = "clcPlazo4"
        '
        'clcPlazo4.Properties
        '
        Me.clcPlazo4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo4.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo4.Properties.MaxLength = 4
        Me.clcPlazo4.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo4.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo4.TabIndex = 44
        Me.clcPlazo4.Tag = "plazo_pago_4"
        '
        'lblCondicion4
        '
        Me.lblCondicion4.AutoSize = True
        Me.lblCondicion4.Location = New System.Drawing.Point(82, 99)
        Me.lblCondicion4.Name = "lblCondicion4"
        Me.lblCondicion4.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion4.TabIndex = 43
        Me.lblCondicion4.Tag = ""
        Me.lblCondicion4.Text = "4:"
        Me.lblCondicion4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(162, 123)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(28, 16)
        Me.Label11.TabIndex = 48
        Me.Label11.Tag = ""
        Me.Label11.Text = "Días"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(162, 75)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 16)
        Me.Label7.TabIndex = 42
        Me.Label7.Tag = ""
        Me.Label7.Text = "Días"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo3
        '
        Me.clcPlazo3.EditValue = "0"
        Me.clcPlazo3.Location = New System.Drawing.Point(106, 75)
        Me.clcPlazo3.MaxValue = 0
        Me.clcPlazo3.MinValue = 0
        Me.clcPlazo3.Name = "clcPlazo3"
        '
        'clcPlazo3.Properties
        '
        Me.clcPlazo3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo3.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo3.Properties.MaxLength = 4
        Me.clcPlazo3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo3.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo3.TabIndex = 41
        Me.clcPlazo3.Tag = "plazo_pago_3"
        '
        'clcPlazo2
        '
        Me.clcPlazo2.EditValue = "0"
        Me.clcPlazo2.Location = New System.Drawing.Point(106, 51)
        Me.clcPlazo2.MaxValue = 0
        Me.clcPlazo2.MinValue = 0
        Me.clcPlazo2.Name = "clcPlazo2"
        '
        'clcPlazo2.Properties
        '
        Me.clcPlazo2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo2.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo2.Properties.MaxLength = 4
        Me.clcPlazo2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo2.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo2.TabIndex = 38
        Me.clcPlazo2.Tag = "plazo_pago_2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(162, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(28, 16)
        Me.Label6.TabIndex = 39
        Me.Label6.Tag = ""
        Me.Label6.Text = "Días"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo1
        '
        Me.clcPlazo1.EditValue = "0"
        Me.clcPlazo1.Location = New System.Drawing.Point(106, 27)
        Me.clcPlazo1.MaxValue = 0
        Me.clcPlazo1.MinValue = 0
        Me.clcPlazo1.Name = "clcPlazo1"
        '
        'clcPlazo1.Properties
        '
        Me.clcPlazo1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo1.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo1.Properties.MaxLength = 4
        Me.clcPlazo1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo1.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo1.TabIndex = 35
        Me.clcPlazo1.Tag = "plazo_pago_1"
        '
        'lblCondicion2
        '
        Me.lblCondicion2.AutoSize = True
        Me.lblCondicion2.Location = New System.Drawing.Point(82, 51)
        Me.lblCondicion2.Name = "lblCondicion2"
        Me.lblCondicion2.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion2.TabIndex = 37
        Me.lblCondicion2.Tag = ""
        Me.lblCondicion2.Text = "2:"
        Me.lblCondicion2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCondicion3
        '
        Me.lblCondicion3.AutoSize = True
        Me.lblCondicion3.Location = New System.Drawing.Point(82, 75)
        Me.lblCondicion3.Name = "lblCondicion3"
        Me.lblCondicion3.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion3.TabIndex = 40
        Me.lblCondicion3.Tag = ""
        Me.lblCondicion3.Text = "3:"
        Me.lblCondicion3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosPago, Me.nvrDatosGenerales})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(72, 540)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 49
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosPago)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrDatosPago
        '
        Me.nvrDatosPago.Caption = "Condiciones de Pago"
        Me.nvrDatosPago.LargeImage = CType(resources.GetObject("nvrDatosPago.LargeImage"), System.Drawing.Image)
        Me.nvrDatosPago.LargeImageIndex = 1
        Me.nvrDatosPago.Name = "nvrDatosPago"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtAgente)
        Me.Panel1.Controls.Add(Me.lkpProveedor)
        Me.Panel1.Controls.Add(Me.lblAgente)
        Me.Panel1.Controls.Add(Me.txtFolio_Proveedor)
        Me.Panel1.Controls.Add(Me.lblFolio_Proveedor)
        Me.Panel1.Controls.Add(Me.cboStatus)
        Me.Panel1.Controls.Add(Me.lblObservaciones)
        Me.Panel1.Controls.Add(Me.chkSobrePedido)
        Me.Panel1.Controls.Add(Me.lblProveedor)
        Me.Panel1.Controls.Add(Me.lkpBodega)
        Me.Panel1.Controls.Add(Me.lblBodega)
        Me.Panel1.Controls.Add(Me.dteFecha)
        Me.Panel1.Controls.Add(Me.lblFecha)
        Me.Panel1.Controls.Add(Me.lkpSucursal)
        Me.Panel1.Controls.Add(Me.txtObservaciones)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.clcOrden)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.lblStatus)
        Me.Panel1.Controls.Add(Me.tmaOrdenesCompra)
        Me.Panel1.Controls.Add(Me.grOrdenesCompra)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.clcSubTotal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.clcIva)
        Me.Panel1.Controls.Add(Me.clcTotal)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(72, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(792, 536)
        Me.Panel1.TabIndex = 50
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.dteFecha_Promesa)
        Me.Panel2.Controls.Add(Me.lblFecha_Promesa)
        Me.Panel2.Controls.Add(Me.txtCondiciones)
        Me.Panel2.Controls.Add(Me.lblCondiciones)
        Me.Panel2.Controls.Add(Me.lblAnticipo)
        Me.Panel2.Controls.Add(Me.clcAnticipo)
        Me.Panel2.Location = New System.Drawing.Point(72, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(792, 536)
        Me.Panel2.TabIndex = 51
        '
        'tbrSurtimientos
        '
        Me.tbrSurtimientos.ImageIndex = 8
        Me.tbrSurtimientos.Text = "Surtimientos"
        '
        'frmOrdenesCompra
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 568)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.MasterControl = Me.tmaOrdenesCompra
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmOrdenesCompra"
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Promesa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFolio_Proveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCondiciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSobrePedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcAnticipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcPlazo5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oOrdenesCompra As VillarrealBusiness.clsOrdenesCompra
    Private oOrdenesCompraDetalle As VillarrealBusiness.clsOrdenesCompraDetalle
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVariables As VillarrealBusiness.clsVariables
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oMovimientosPagar As VillarrealBusiness.clsMovimientosPagar
    Private oUsuarios As VillarrealBusiness.clsUsuarios

    Private intBodega_Anterior As String
    Private Impuesto As Double = 0

    Private ReadOnly Property Sucursal() As Long
        Get
            Return PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Bodega() As String
        Get
            Return PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    Private ReadOnly Property Proveedor() As Long
        Get
            Return PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmOrdenesCompra_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmOrdenesCompra_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmOrdenesCompra_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmOrdenesCompra_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Me.cboStatus.Value = "PS"
                Response = oOrdenesCompra.Insertar(Me.DataSource)

                If Me.clcAnticipo.EditValue > 0 And Response.ErrorFound = False Then 'Genera un movimiento de tipo abono en doc_movimientos_pagar
                    Response = InsertaAbonoPorAnticipo()
                End If

            Case Actions.Update
                Response = oOrdenesCompra.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oOrdenesCompra.Eliminar(clcOrden.Value)

        End Select
    End Sub
    Private Sub frmOrdenesCompra_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With tmaOrdenesCompra

            If (Me.Action = Actions.Update And intBodega_Anterior <> Me.lkpBodega.EditValue) Then
                .MoveFirst()
                Do While Not .EOF
                    Response = oOrdenesCompraDetalle.CambiarBodega(intBodega_Anterior, Me.lkpBodega.EditValue, .Item("articulo"), .Item("cantidad_anterior"), Me.clcOrden.Text, Me.chkSobrePedido.Checked)
                    .MoveNext()
                Loop
            End If

            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert And .Item("cantidad") > 0
                        Response = oOrdenesCompraDetalle.Insertar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)

                        If Not Response.ErrorFound And .Item("sobre_pedido") = True Then
                           
                            Response = Me.oOrdenesCompraDetalle.ActualizarVentaDetalleSobrePedidoOC(clcOrden.EditValue, .Item("folios_ventas"))
                        End If
                    Case Actions.Update 'And .Item("cantidad") > 0
                        Response = oOrdenesCompraDetalle.Actualizar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)

                        'If Not Response.ErrorFound And .Item("sobre_pedido") = True Then

                        '    Response = Me.oOrdenesCompraDetalle.ActualizarVentaDetalleSobrePedidoOC(clcOrden.EditValue, .Item("folios_ventas"))
                        'End If
                    Case Actions.Delete
                        Response = oOrdenesCompraDetalle.Eliminar(Me.clcOrden.Value, .Item("Partida"), .Item("cantidad"), .Item("articulo"), lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                End Select
                .MoveNext()
            Loop
        End With

        If Me.Action = Actions.Insert And Not Response.ErrorFound Then
            ImprimeOrden(Me.clcOrden.Value)
        End If
    End Sub
    Private Sub frmOrdenesCompra_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        Response = oOrdenesCompra.DespliegaDatos(CType(OwnerForm.Value("orden"), Long))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)
            Me.DataSource = oDataSet
        End If
        intBodega_Anterior = Me.lkpBodega.EditValue

        Response = oOrdenesCompraDetalle.DespliegaDatos(CType(clcOrden.EditValue, Long))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)
            Me.tmaOrdenesCompra.DataSource = oDataSet
        End If

        'Deshabilita los controles que no se pueden modificar, dependiendo el status de la orden
        If Me.Action = Actions.Update Then
            Me.tbrTools.Buttons(3).Enabled = True
            Me.clcAnticipo.Enabled = False

            If Me.cboStatus.Value <> "PS" Then
                Me.dteFecha.Enabled = False
                Me.dteFecha_Promesa.Enabled = False
                Me.lkpSucursal.Enabled = False
                Me.lkpBodega.Enabled = False
                Me.lkpProveedor.Enabled = False
                Me.txtFolio_Proveedor.Enabled = False
                Me.txtCondiciones.Enabled = False
                Me.txtAgente.Enabled = False
                Me.txtObservaciones.Enabled = False
                Me.tmaOrdenesCompra.Enabled = False
                Me.grOrdenesCompra.Enabled = False
                Me.clcPlazo1.Enabled = False
                Me.clcPlazo2.Enabled = False
                Me.clcPlazo3.Enabled = False
                Me.clcPlazo4.Enabled = False
                Me.clcPlazo5.Enabled = False
            Else
                Me.tbrTools.Buttons(2).Enabled = True
                Me.clcAnticipo.Enabled = True
            End If

            If Me.cboStatus.Value = "PA" Then
                Me.dteFecha_Promesa.Enabled = True
                Me.txtCondiciones.Enabled = True
                Me.txtObservaciones.Enabled = True
                Me.tbrTools.Buttons(2).Enabled = True
                Me.txtFolio_Proveedor.Enabled = True
            End If
        End If

        If Me.cboStatus.Value = "CA" Then
            Me.tbrTools.Buttons(3).Enabled = True
        End If

        If Me.cboStatus.Value = "CE" Or Me.cboStatus.Value = "SU" Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.tbrTools.Buttons(0).Visible = False
            Me.tbrTools.Buttons(2).Enabled = False
            Me.tbrTools.Buttons(2).Visible = False
        End If
        Me.chkSobrePedido.Enabled = False
        CalculaTotal()

    End Sub
    Private Sub frmOrdenesCompra_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oOrdenesCompra = New VillarrealBusiness.clsOrdenesCompra
        oOrdenesCompraDetalle = New VillarrealBusiness.clsOrdenesCompraDetalle
        oSucursales = New VillarrealBusiness.clsSucursales
        oBodegas = New VillarrealBusiness.clsBodegas
        oMovimientosPagar = New VillarrealBusiness.clsMovimientosPagar
        oProveedores = New VillarrealBusiness.clsProveedores
        oVariables = New VillarrealBusiness.clsVariables
        oUsuarios = New VillarrealBusiness.clsUsuarios

        With Me.tmaOrdenesCompra
            .UpdateTitle = "un Artículo"
            .UpdateForm = New frmOrdenesCompraDetalle
            .AddColumn("articulo", "System.Int32")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("nombre_unidad")
            .AddColumn("costo", "System.Decimal")
            .AddColumn("importe", "System.Decimal")
            .AddColumn("cantidad_anterior")
            .AddColumn("cantidad_recibida", "System.Int32")
            .AddColumn("cantidad_mes", "System.Int32")
            .AddColumn("cantidad_anual", "System.Int32")
            .AddColumn("existencia", "System.Int32")
            .AddColumn("folios_ventas")
            .AddColumn("sobre_pedido", "System.Boolean")
            .AddColumn("departamento", "System.Int32")
            .AddColumn("grupo", "System.Int32")
            .AddColumn("modelo")
            .AddColumn("cantidad_sugerida", "System.Int32")
        End With

        Me.lkpBodega.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)

        Impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        Select Case Action
            Case Actions.Insert
                Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFecha_Promesa.EditValue = CDate(TINApp.FechaServidor)
                Me.grcCantidadRecibida.VisibleIndex = -1
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(3).Enabled = False
                Me.tbrTools.Buttons(6).Enabled = False

            Case Actions.Update
                'Me.tmaOrdenesCompra.CanDelete = FALSE
                Me.tbrTools.Buttons(4).Enabled = False
                Me.tbrOCSobrePedido.Enabled = False
                Me.tbrTools.Buttons(6).Enabled = True
                Me.grcCantidadSugerida.VisibleIndex = -1        'JMARTINEZ 28Oct2007: Sin datos historicos
            Case Actions.Delete
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(3).Enabled = False
                Me.tbrTools.Buttons(4).Enabled = False
                Me.tbrTools.Buttons(6).Enabled = False
                Me.tbrOCSobrePedido.Enabled = False
                Me.grcCantidadSugerida.VisibleIndex = -1        'JMARTINEZ 28Oct2007: Sin datos historicos
        End Select
    End Sub
    Private Sub frmOrdenesCompra_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oOrdenesCompra.Validacion(Action, lkpSucursal.EditValue, dteFecha.Text, dteFecha_Promesa.Text, lkpBodega.Text, lkpProveedor.EditValue, Me.txtFolio_Proveedor.Text, Me.cboStatus.Value, Me.tmaOrdenesCompra.Count, Me.tmaOrdenesCompra.DataSource)
    End Sub
    Private Sub frmOrdenesCompra_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.EditValue = Me.lkpBodega.GetValue("sucursal")
    End Sub

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        If Me.Action = Actions.Insert Then
            Response = oProveedores.Lookup(True, True)
        Else
            Response = oProveedores.Lookup()
        End If
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
        Me.txtAgente.Text = CType(Me.lkpProveedor.GetValue("contactos"), String)
    End Sub

    Private Sub dteFecha_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dteFecha.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oOrdenesCompra.ValidaFecha(dteFecha.EditValue)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub dteFecha_Promesa_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dteFecha_Promesa.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oOrdenesCompra.ValidaFecha_Promesa(dteFecha_Promesa.EditValue)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tlbReimprimir Then
            ImprimeOrden(Me.clcOrden.Value)
        ElseIf e.Button Is Me.tlbCerrarOrden Then
            If ShowMessage(MessageType.MsgQuestion, "¿Desea Cerrar esta Orden de Compra?") = Answer.MsgYes Then
                CerraOC()
                Me.Close()
            End If
        End If

        If e.Button Is Me.tbrTraerArticulos Then
            If Me.Proveedor > 0 Then
                TraeArticulos()
            Else
                ShowMessage(MessageType.MsgInformation, "Debe de Seleccionar un Proveedor")
            End If
        End If

        If e.Button Is Me.tbrOCSobrePedido Then
            If Me.Proveedor > 0 Then
                Me.ArticulosSobrePedido()
            Else
                ShowMessage(MessageType.MsgInformation, "Debe de Seleccionar un Proveedor")
            End If
        End If


        If e.Button Is Me.tbrSurtimientos Then
            DespliegaSurtimientos()
        End If
    End Sub

    Private Sub clcPlazos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcPlazo1.EditValueChanged, clcPlazo2.EditValueChanged, clcPlazo3.EditValueChanged, clcPlazo4.EditValueChanged, clcPlazo5.EditValueChanged
        If IsNumeric(CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue) Then 'And Not VerificaEditarCondicion(sender) Then
            If CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue > 0 Then
                'If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

                'If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                'If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                ' Me.LlenarGridPagos()
                'End If
                'End If
        'End If
        End If
        End If
    End Sub
    Private Sub clcPlazos_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcPlazo1.Validating, clcPlazo2.Validating, clcPlazo3.Validating, clcPlazo4.Validating, clcPlazo5.Validating

        Dim Control As DevExpress.XtraEditors.CalcEdit = CType(sender, DevExpress.XtraEditors.CalcEdit)
        Select Case Control.Name
            Case "clcPlazo2"
                If Control.Value <= Me.clcPlazo1.Value Then
                    Control.Value = 0
                End If
            Case "clcPlazo3"
                If Control.Value <= Me.clcPlazo2.Value Or Me.clcPlazo2.Value = 0 Then
                    Control.Value = 0
                End If

            Case "clcPlazo4"
                If Control.Value <= Me.clcPlazo3.Value Or Me.clcPlazo3.Value = 0 Then
                    Control.Value = 0
                End If
            Case "clcPlazo5"
                If Control.Value <= Me.clcPlazo4.Value Or Me.clcPlazo4.Value = 0 Then
                    Control.Value = 0
                End If
        End Select

        ''If CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue > 0 Then
        'If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then
        '    If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
        '        If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
        '            Me.LlenarGridPagos()
        '        End If
        '    End If
        'End If
    End Sub
    Private Sub clcPlazos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcPlazo1.Validated, clcPlazo2.Validated, clcPlazo3.Validated, clcPlazo4.Validated

        If clcPlazo1.Value = 0 Then
            clcPlazo2.EditValue = 0
        End If
        If clcPlazo2.Value = 0 Then
            clcPlazo3.EditValue = 0
        End If
        If clcPlazo3.Value = 0 Then
            clcPlazo4.EditValue = 0
        End If
        If clcPlazo4.Value = 0 Then
            clcPlazo5.EditValue = 0
        End If


        'If IsNumeric(clcPlazo1.Value) Then
        '    If clcPlazo1.Value > 0 Then
        '        'If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

        '        '    If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
        '        '        If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
        '        '            Me.LlenarGridPagos()
        '        '        End If
        '        '    End If
        '        'End If
        '        'Else
        '        '    borrarGridPagos()
        '    End If

        'Else
        '    borrarGridPagos()
        'End If

    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrDatosPago_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosPago.LinkPressed
        Me.Panel2.BringToFront()
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub ImprimeOrden(ByVal orden As Long)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.OrdenesCompra(orden)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La orden de compra no puede mostrarse")
            Else
                Dim oDataSet As DataSet
                Dim oReport As New rptOrdenesCompra

                oDataSet = CType(Response.Value, DataSet)
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Órden de Compra", oReport, , , , True)
                End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Public Sub CalculaTotal()
        clcSubTotal.Value = Me.gdcImporte.SummaryItem.SummaryValue
        clcIva.Value = CType(clcSubTotal.Value, Decimal) * (Impuesto / 100)
        clcTotal.Value = CType(clcSubTotal.Value, Decimal) + CType(clcIva.Value, Decimal)

    End Sub
    Private Sub TraeArticulos()
        Dim response As New Events

        response = Me.oOrdenesCompraDetalle.TraerArticulosOC(Me.Proveedor, Me.Sucursal)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)
            Me.tmaOrdenesCompra.DataSource = odataset
            CalculaTotal()
        End If
    End Sub
    Private Sub ArticulosSobrePedido()
        Dim response As New Events

        response = Me.oOrdenesCompraDetalle.ArticulosSobrePedidoOC(Me.Proveedor, Me.Sucursal)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)
            Me.tmaOrdenesCompra.DataSource = odataset
            If Me.tmaOrdenesCompra.Count > 0 Then
                Me.chkSobrePedido.Checked = True
            Else
                Me.chkSobrePedido.Checked = False
            End If
            CalculaTotal()
        End If
    End Sub
    Private Sub CerraOC()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oOrdenesCompra.CerrarOrden(Me.clcOrden.Value)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La orden de compra no puede mostrarse")
            Else
                'JMARTINEZ 27Oct2007 Si no trae datos no mostrar preview
                Dim oData As DataTable = CType(Response.Value, DataSet).Tables(0)
                If oData.Rows.Count > 0 Then
                    Dim oReport As New rptCerrarOrdenCompra
                    oReport.DataSource = oData
                    TINApp.ShowReport(Me.MdiParent, "Órden de Compra", oReport)
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub
    Private Function InsertaAbonoPorAnticipo() As Dipros.UTILS.Events
        Dim Response As New Dipros.Utils.Events
        Dim concepto_cxp_anticipo As String

        Dim Iva As Double
        Dim Subtotal As Double
        Dim Formulo As String

        Try
            concepto_cxp_anticipo = CType(oVariables.TraeDatos("concepto_cxp_abono_comision", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)

            Iva = (Me.clcAnticipo.EditValue * Impuesto / 100)
            Subtotal = Me.clcAnticipo.EditValue - Iva
            Formulo = CType(oUsuarios.DespliegaDatosUsuario().Value, DataSet).Tables(0).Rows(0).Item("nombre")

            Response = oMovimientosPagar.InsertarAbonoPorComision(concepto_cxp_anticipo, 0, Me.lkpProveedor.EditValue, 0, TINApp.FechaServidor, TINApp.FechaServidor, 0, "", 0, False, 0, Me.clcAnticipo.EditValue, Subtotal, Iva, Me.clcAnticipo.EditValue, 0, "Registro del Sistema por Anticipo", Formulo, "", "", "", False, 0)

        Catch ex As Exception

            ShowMessage(MessageType.MsgInformation, "No se Guardó el Anticipo")

        End Try
        Return Response
    End Function

    Private Sub DespliegaSurtimientos()
        Dim response As New Events
        Dim oDataSet As DataSet
        Dim frmModal As New frmSurtimientosOrdenesCompra

        With frmModal
            .OwnerForm = Me
            .Title = "Surtimientos"
            .MdiParent = Me.MdiParent
            response = oOrdenesCompra.DespliegaSurtimientos(Me.clcOrden.EditValue)
            If Not response.ErrorFound Then
                oDataSet = CType(response.Value, DataSet)
                .grSurtimientos.MainView = .grvSurtimientos
                .grSurtimientos.DataSource = oDataSet.Tables(0)
            End If
            .Show()
        End With

        Me.Enabled = False
    End Sub
#End Region



    Private Sub frmOrdenesCompra_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MenuOption.Name, "FECHA_OC") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub
End Class
