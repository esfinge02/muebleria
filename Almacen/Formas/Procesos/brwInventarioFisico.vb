Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class brwInventarioFisico
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents cmdAjustar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDiferencia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblFechaCierre As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblFechaCierre = New System.Windows.Forms.Label
        Me.cmdAjustar = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDiferencia = New DevExpress.XtraEditors.SimpleButton
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.FilterPanel.SuspendLayout()
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.lkpSucursal)
        Me.FilterPanel.Controls.Add(Me.lblSucursal)
        Me.FilterPanel.Controls.Add(Me.cmdDiferencia)
        Me.FilterPanel.Controls.Add(Me.cmdAjustar)
        Me.FilterPanel.Controls.Add(Me.lblFechaCierre)
        Me.FilterPanel.Controls.Add(Me.lkpBodega)
        Me.FilterPanel.Controls.Add(Me.Label3)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(770, 61)
        Me.FilterPanel.TabIndex = 0
        Me.FilterPanel.Controls.SetChildIndex(Me.Label3, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblFechaCierre, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.cmdAjustar, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.cmdDiferencia, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpSucursal, 0)
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 389)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(770, 8)
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(27, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Bodega:"
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(80, 32)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(410, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(224, 22)
        Me.lkpBodega.TabIndex = 3
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblFechaCierre
        '
        Me.lblFechaCierre.AutoSize = True
        Me.lblFechaCierre.Location = New System.Drawing.Point(408, 11)
        Me.lblFechaCierre.Name = "lblFechaCierre"
        Me.lblFechaCierre.Size = New System.Drawing.Size(85, 16)
        Me.lblFechaCierre.TabIndex = 4
        Me.lblFechaCierre.Text = "Fecha de Cierre"
        Me.lblFechaCierre.Visible = False
        '
        'cmdAjustar
        '
        Me.cmdAjustar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAjustar.Location = New System.Drawing.Point(680, 10)
        Me.cmdAjustar.Name = "cmdAjustar"
        Me.cmdAjustar.Size = New System.Drawing.Size(80, 23)
        Me.cmdAjustar.TabIndex = 6
        Me.cmdAjustar.Text = "Ajustar"
        '
        'cmdDiferencia
        '
        Me.cmdDiferencia.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDiferencia.Location = New System.Drawing.Point(597, 10)
        Me.cmdDiferencia.Name = "cmdDiferencia"
        Me.cmdDiferencia.Size = New System.Drawing.Size(80, 23)
        Me.cmdDiferencia.TabIndex = 5
        Me.cmdDiferencia.Text = "Diferencias"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 8)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(224, 22)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = ""
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(24, 11)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'brwInventarioFisico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(770, 397)
        Me.Name = "brwInventarioFisico"
        Me.Text = "brwInventarioFisico"
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Declaraciones"
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oInventarioFisico As New VillarrealBusiness.clsInventarioFisico
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oReportes As New VillarrealBusiness.Reportes

    Private tipo_inventario As Char

    Private ReadOnly Property Bodega_Actual_Usuario() As String
        Get
            Return Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)
        End Get
    End Property

    Public ReadOnly Property bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    Public ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Public WriteOnly Property TipoInventario() As Char
        Set(ByVal Value As Char)
            tipo_inventario = Value
        End Set
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub brwInventarioFisico_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oInventarioFisico.Listado(tipo_inventario, bodega)
    End Sub
    Private Sub brwInventarioFisico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'poner valor en lkpSucursal
        Dim suc As Long
        suc = CType(CType(oBodegas.DespliegaDatos(Bodega_Actual_Usuario).Value, DataSet).Tables(0).Rows(0).Item("sucursal"), Long)
        Me.lkpSucursal.EditValue = suc

        Me.lkpBodega.EditValue = Bodega_Actual_Usuario

        Me.FooterPanel.Visible = False
    End Sub
    Private Sub brwInventarioFisico_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oInventarioFisico.Validacion(Actions.None, True, Me.bodega, 0, Date.Now, Date.Now)
    End Sub
#End Region

#Region "Eventos de los Controles"
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega_LoadData(True)
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup(sucursal)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpBodega_Format() Handles lkpBodega.Format

        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub cmdAjustar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAjustar.Click
        'si no existen inventarios
        If Me.View.RowCount = 0 Then Exit Sub

        'si el inventario esta en estatus de Aplicado
        If Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("status")) = "A" Then
            ShowMessage(MessageType.MsgInformation, "El ajuste al inventario ya fue aplicado", "Inventario F�sico", Nothing, False)
            Exit Sub
        End If

        'valida si el inventario se puede ajustar
        'si el inventario tiene status de Aplicado no se puede ajustar
        If Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("status")) = "A" Then
            ShowMessage(MessageType.MsgInformation, "El inventario f�sico ya esta ajustado", "Inventario F�sico", Nothing, False)
        Else
            If ValidaFechaCierre(Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("fecha"))) Then
                ShowMessage(MessageType.MsgInformation, "No se puede aplicar el inventario con fecha anterior a la fecha de cierre", "Inventario F�sico", Nothing, False)
            Else
                'llama ventana de conceptos para realizar ajuste
                Dim frmModal As New frmInventarioFisicoAjustar
                With frmModal
                    .Text = "Conceptos de Ajuste"
                    .bodega = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("bodega"))
                    .inventario = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("inventario"))
                    .fecha = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("fecha"))
                    .OwnerForm = Me
                    .MdiParent = Me.MdiParent
                    .Show()
                End With
                Me.Enabled = False
            End If
        End If
    End Sub

    Private Sub cmdDiferencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDiferencia.Click
        'si no existen inventarios
        If Me.View.RowCount = 0 Then Exit Sub

        'si el inventario esta en estatus de Aplicado
        If Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("status")) = "A" Then
            ShowMessage(MessageType.MsgInformation, "El ajuste al inventario ya fue aplicado", "Inventario F�sico", Nothing, False)
            Exit Sub
        End If

        'imprime reporte de diferencias
        Dim response As New Events
        Dim oDataSet As DataSet
        Dim oReport As New rptInventarioFisicoDiferencias
        If Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("tipo_inventario")) = "D" Then
            'inventario x depto
            response = oReportes.InventarioFisicoDiferencias(Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("bodega")), Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("fecha")), Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("inventario")))
        Else
            'inventario x ubicacion
            response = oReportes.InventarioFisicoDiferenciasxUbicacion(Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("bodega")), Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("fecha")), Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("inventario")))
        End If

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Diferencias de Inventario F�sico no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Diferencias de Inventario F�sico", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub
#End Region

#Region "Funcionalidad"

    Private Function ValidaFechaCierre(ByVal fecha As DateTime) As Boolean
        ValidaFechaCierre = False
        Dim fecha_cierre As DateTime = uti_VariablesFechaCierre()
        If fecha_cierre = Nothing Then Exit Function
        If fecha <= fecha_cierre Then
            Me.lblFechaCierre.Visible = True
            Me.lblFechaCierre.Text = "Fecha Cierre: " & System.String.Format("dd/mmm/yyyy", fecha_cierre)
            ValidaFechaCierre = True
        End If
    End Function

#End Region

End Class
