Imports Comunes
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmEntradasDetalles
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Entrada As Long
    Dim KS As Keys
    Private dTablaPrecios As DataTable

    Private dsPrecios As New DataSet
    Private iArticulo As Integer
    Private ManejaSeries As Boolean = False
    Private bcosto As Boolean = False
    Private dcosto As Double
    Private brecalcular As Boolean = False
    Private bAsignado As Boolean
    Private bRecalcularPrecios As Boolean
    Private bRecalcula As Boolean = False

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents clcCosto As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcCantidadPedida As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcCostoPedido As Dipros.Editors.TINCalcEdit
    Friend WithEvents lbldescripcion As System.Windows.Forms.Label
    Friend WithEvents grPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcprecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptValor As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcNombrePrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUtilidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcOrden As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents chkSobrepedido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcfolioHistorico As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grcGanancia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGanancia_expo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcExpo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTerminacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcImporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcPrecioLista As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Public WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Public WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Public WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEntradasDetalles))
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.clcCosto = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcCantidadPedida = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcCostoPedido = New Dipros.Editors.TINCalcEdit
        Me.lbldescripcion = New System.Windows.Forms.Label
        Me.grPrecios = New DevExpress.XtraGrid.GridControl
        Me.grvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcprecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptValor = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcNombrePrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUtilidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGanancia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGanancia_expo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcIva = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcExpo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTerminacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcOrden = New DevExpress.XtraEditors.CalcEdit
        Me.chkSobrepedido = New DevExpress.XtraEditors.CheckEdit
        Me.clcfolioHistorico = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcImporte = New DevExpress.XtraEditors.CalcEdit
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.clcPrecioLista = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidadPedida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCostoPedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcfolioHistorico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1888, 28)
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(81, 40)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 0
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(133, 39)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(48, 19)
        Me.clcPartida.TabIndex = 1
        Me.clcPartida.Tag = "partida"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(78, 115)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Art�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(21, 202)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(108, 16)
        Me.lblCantidad.TabIndex = 12
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "&Cantidad Recibida:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(133, 200)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "n0"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "n0"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(72, 19)
        Me.clcCantidad.TabIndex = 13
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(288, 202)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(90, 16)
        Me.lblCosto.TabIndex = 16
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "C&osto Recibido:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCosto
        '
        Me.clcCosto.EditValue = "0"
        Me.clcCosto.Location = New System.Drawing.Point(384, 200)
        Me.clcCosto.MaxValue = 0
        Me.clcCosto.MinValue = 0
        Me.clcCosto.Name = "clcCosto"
        '
        'clcCosto.Properties
        '
        Me.clcCosto.Properties.DisplayFormat.FormatString = "c2"
        Me.clcCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.EditFormat.FormatString = "c2"
        Me.clcCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCosto.Properties.Precision = 2
        Me.clcCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCosto.Size = New System.Drawing.Size(72, 19)
        Me.clcCosto.TabIndex = 17
        Me.clcCosto.Tag = "costo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 178)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Tag = ""
        Me.Label2.Text = "Cantidad P&edida:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidadPedida
        '
        Me.clcCantidadPedida.EditValue = "0"
        Me.clcCantidadPedida.Location = New System.Drawing.Point(133, 176)
        Me.clcCantidadPedida.MaxValue = 0
        Me.clcCantidadPedida.MinValue = 0
        Me.clcCantidadPedida.Name = "clcCantidadPedida"
        '
        'clcCantidadPedida.Properties
        '
        Me.clcCantidadPedida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadPedida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadPedida.Properties.Enabled = False
        Me.clcCantidadPedida.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidadPedida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidadPedida.Size = New System.Drawing.Size(72, 19)
        Me.clcCantidadPedida.TabIndex = 11
        Me.clcCantidadPedida.Tag = "cantidad_pedida"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(297, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 16)
        Me.Label3.TabIndex = 14
        Me.Label3.Tag = ""
        Me.Label3.Text = "Costo P&edido:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCostoPedido
        '
        Me.clcCostoPedido.EditValue = "0"
        Me.clcCostoPedido.Location = New System.Drawing.Point(384, 176)
        Me.clcCostoPedido.MaxValue = 0
        Me.clcCostoPedido.MinValue = 0
        Me.clcCostoPedido.Name = "clcCostoPedido"
        '
        'clcCostoPedido.Properties
        '
        Me.clcCostoPedido.Properties.DisplayFormat.FormatString = "c2"
        Me.clcCostoPedido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoPedido.Properties.EditFormat.FormatString = "c2"
        Me.clcCostoPedido.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoPedido.Properties.Enabled = False
        Me.clcCostoPedido.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCostoPedido.Properties.Precision = 2
        Me.clcCostoPedido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCostoPedido.Size = New System.Drawing.Size(72, 19)
        Me.clcCostoPedido.TabIndex = 15
        Me.clcCostoPedido.Tag = "costo_pedido"
        '
        'lbldescripcion
        '
        Me.lbldescripcion.Location = New System.Drawing.Point(133, 136)
        Me.lbldescripcion.Name = "lbldescripcion"
        Me.lbldescripcion.Size = New System.Drawing.Size(312, 32)
        Me.lbldescripcion.TabIndex = 9
        Me.lbldescripcion.Tag = "descripcion"
        '
        'grPrecios
        '
        Me.grPrecios.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        'grPrecios.EmbeddedNavigator
        '
        Me.grPrecios.EmbeddedNavigator.Name = ""
        Me.grPrecios.Location = New System.Drawing.Point(7, 255)
        Me.grPrecios.MainView = Me.grvPrecios
        Me.grPrecios.Name = "grPrecios"
        Me.grPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptValor})
        Me.grPrecios.Size = New System.Drawing.Size(452, 136)
        Me.grPrecios.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPrecios.TabIndex = 18
        Me.grPrecios.TabStop = False
        Me.grPrecios.Text = "GridControl1"
        '
        'grvPrecios
        '
        Me.grvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcprecio, Me.grcNombrePrecio, Me.grcUtilidad, Me.grcPrecioVenta, Me.grcGanancia, Me.grcGanancia_expo, Me.grcIva, Me.grcExpo, Me.grcTerminacion})
        Me.grvPrecios.GridControl = Me.grPrecios
        Me.grvPrecios.GroupPanelText = "Precios"
        Me.grvPrecios.Name = "grvPrecios"
        Me.grvPrecios.OptionsView.ShowGroupPanel = False
        Me.grvPrecios.OptionsView.ShowIndicator = False
        '
        'grcprecio
        '
        Me.grcprecio.Caption = "Clave Precio"
        Me.grcprecio.ColumnEdit = Me.rptValor
        Me.grcprecio.FieldName = "precio"
        Me.grcprecio.Name = "grcprecio"
        Me.grcprecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'rptValor
        '
        Me.rptValor.AutoHeight = False
        Me.rptValor.Name = "rptValor"
        '
        'grcNombrePrecio
        '
        Me.grcNombrePrecio.Caption = "Precio"
        Me.grcNombrePrecio.FieldName = "nombre_precio"
        Me.grcNombrePrecio.Name = "grcNombrePrecio"
        Me.grcNombrePrecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombrePrecio.VisibleIndex = 0
        Me.grcNombrePrecio.Width = 149
        '
        'grcUtilidad
        '
        Me.grcUtilidad.Caption = "% Descuento"
        Me.grcUtilidad.ColumnEdit = Me.rptValor
        Me.grcUtilidad.DisplayFormat.FormatString = "n2"
        Me.grcUtilidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcUtilidad.FieldName = "porcentaje_descuento"
        Me.grcUtilidad.Name = "grcUtilidad"
        Me.grcUtilidad.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUtilidad.VisibleIndex = 1
        Me.grcUtilidad.Width = 91
        '
        'grcPrecioVenta
        '
        Me.grcPrecioVenta.Caption = "Precio Venta"
        Me.grcPrecioVenta.DisplayFormat.FormatString = "c2"
        Me.grcPrecioVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioVenta.FieldName = "precio_venta"
        Me.grcPrecioVenta.Name = "grcPrecioVenta"
        Me.grcPrecioVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioVenta.VisibleIndex = 2
        Me.grcPrecioVenta.Width = 87
        '
        'grcGanancia
        '
        Me.grcGanancia.Caption = "GridColumn1"
        Me.grcGanancia.FieldName = "factor_ganancia"
        Me.grcGanancia.Name = "grcGanancia"
        Me.grcGanancia.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcGanancia_expo
        '
        Me.grcGanancia_expo.Caption = "GridColumn3"
        Me.grcGanancia_expo.FieldName = "factor_ganancia_expo"
        Me.grcGanancia_expo.Name = "grcGanancia_expo"
        Me.grcGanancia_expo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcIva
        '
        Me.grcIva.Caption = "GridColumn4"
        Me.grcIva.FieldName = "impuesto"
        Me.grcIva.Name = "grcIva"
        Me.grcIva.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcExpo
        '
        Me.grcExpo.Caption = "GridColumn1"
        Me.grcExpo.FieldName = "precio_expo"
        Me.grcExpo.Name = "grcExpo"
        Me.grcExpo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcTerminacion
        '
        Me.grcTerminacion.Caption = "Terminacion precio"
        Me.grcTerminacion.FieldName = "terminacion_precio"
        Me.grcTerminacion.Name = "grcTerminacion"
        '
        'clcOrden
        '
        Me.clcOrden.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcOrden.Location = New System.Drawing.Point(248, 37)
        Me.clcOrden.Name = "clcOrden"
        '
        'clcOrden.Properties
        '
        Me.clcOrden.Properties.Enabled = False
        Me.clcOrden.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcOrden.Size = New System.Drawing.Size(75, 20)
        Me.clcOrden.TabIndex = 59
        Me.clcOrden.TabStop = False
        Me.clcOrden.Tag = "orden"
        Me.clcOrden.Visible = False
        '
        'chkSobrepedido
        '
        Me.chkSobrepedido.Location = New System.Drawing.Point(24, 40)
        Me.chkSobrepedido.Name = "chkSobrepedido"
        '
        'chkSobrepedido.Properties
        '
        Me.chkSobrepedido.Properties.Caption = ""
        Me.chkSobrepedido.Properties.Enabled = False
        Me.chkSobrepedido.Size = New System.Drawing.Size(16, 17)
        Me.chkSobrepedido.TabIndex = 60
        Me.chkSobrepedido.TabStop = False
        Me.chkSobrepedido.Tag = "sobre_pedido"
        Me.chkSobrepedido.Visible = False
        '
        'clcfolioHistorico
        '
        Me.clcfolioHistorico.EditValue = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.clcfolioHistorico.Location = New System.Drawing.Point(16, 88)
        Me.clcfolioHistorico.Name = "clcfolioHistorico"
        '
        'clcfolioHistorico.Properties
        '
        Me.clcfolioHistorico.Properties.Enabled = False
        Me.clcfolioHistorico.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcfolioHistorico.Size = New System.Drawing.Size(40, 20)
        Me.clcfolioHistorico.TabIndex = 61
        Me.clcfolioHistorico.TabStop = False
        Me.clcfolioHistorico.Tag = "folio_historico_costo"
        Me.clcfolioHistorico.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(57, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Tag = ""
        Me.Label4.Text = "De&scripci�n:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporte.Location = New System.Drawing.Point(344, 37)
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporte.Size = New System.Drawing.Size(75, 20)
        Me.clcImporte.TabIndex = 63
        Me.clcImporte.TabStop = False
        Me.clcImporte.Tag = "importe"
        Me.clcImporte.Visible = False
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(133, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(323, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 7
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo"
        '
        'clcPrecioLista
        '
        Me.clcPrecioLista.EditValue = "0"
        Me.clcPrecioLista.Location = New System.Drawing.Point(384, 224)
        Me.clcPrecioLista.MaxValue = 0
        Me.clcPrecioLista.MinValue = 0
        Me.clcPrecioLista.Name = "clcPrecioLista"
        '
        'clcPrecioLista.Properties
        '
        Me.clcPrecioLista.Properties.DisplayFormat.FormatString = "c2"
        Me.clcPrecioLista.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioLista.Properties.EditFormat.FormatString = "c2"
        Me.clcPrecioLista.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioLista.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioLista.Properties.Precision = 2
        Me.clcPrecioLista.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioLista.Size = New System.Drawing.Size(72, 19)
        Me.clcPrecioLista.TabIndex = 19
        Me.clcPrecioLista.Tag = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(306, 226)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 16)
        Me.Label5.TabIndex = 18
        Me.Label5.Tag = ""
        Me.Label5.Text = "Precio &Lista:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Enabled = False
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(133, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(323, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(41, 67)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 2
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Enabled = False
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(133, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(323, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(86, 91)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 4
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(8, 120)
        Me.txtmodelo.Name = "txtmodelo"
        Me.txtmodelo.Size = New System.Drawing.Size(48, 20)
        Me.txtmodelo.TabIndex = 65
        Me.txtmodelo.Tag = "modelo"
        Me.txtmodelo.Visible = False
        '
        'frmEntradasDetalles
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(466, 399)
        Me.Controls.Add(Me.txtmodelo)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.clcPrecioLista)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.grPrecios)
        Me.Controls.Add(Me.clcfolioHistorico)
        Me.Controls.Add(Me.chkSobrepedido)
        Me.Controls.Add(Me.clcOrden)
        Me.Controls.Add(Me.lbldescripcion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcCostoPedido)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcCantidadPedida)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.clcCosto)
        Me.Name = "frmEntradasDetalles"
        Me.Controls.SetChildIndex(Me.clcCosto, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.clcCantidadPedida, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcCostoPedido, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcOrden, 0)
        Me.Controls.SetChildIndex(Me.chkSobrepedido, 0)
        Me.Controls.SetChildIndex(Me.clcfolioHistorico, 0)
        Me.Controls.SetChildIndex(Me.grPrecios, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioLista, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.txtmodelo, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidadPedida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCostoPedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcfolioHistorico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oEntradasdetalle As New VillarrealBusiness.clsEntradasDetalles
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oArticulosPrecios As New VillarrealBusiness.clsArticulosPrecios
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property costo_pedido() As Double
        Get
            Return CType(Me.clcCostoPedido.EditValue, Double)
        End Get
    End Property
    Private ReadOnly Property costo_recibido() As Double
        Get
            Return CType(Me.clcCosto.EditValue, Double)
        End Get
    End Property
    Private Property Precio_lista() As Double
        Get
            Return CType(Me.clcPrecioLista.EditValue, Double)
        End Get
        Set(ByVal Value As Double)
            Me.clcPrecioLista.EditValue = Int(Value)
        End Set
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmEntradasDetalles_Accept(ByRef Response As Events) Handles MyBase.Accept
        Me.RecuperaPrecios()

        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .item.item("precio_lista") = Precio_lista
                    .item.item("modificado") = True
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .ITEM.item("precio_lista") = Precio_lista
                    .item.item("modificado") = True
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
        Me.ActualizaPrecios()
        OwnerForm.CalculaTotales()
        OwnerForm.LlenarGrid()
    End Sub

    Private Sub frmEntradasDetalles_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Dim valor As Double = 0

        Me.bcosto = True
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("departamento")) Then
            Me.lkpDepartamento.EditValue = -1
        Else
            Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        End If


        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("grupo")) Then
            Me.lkpGrupo.EditValue = -1
        Else
            Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        End If


        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo")) Then
            Me.lkpArticulo.EditValue = -1
        Else
            Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)
            If Me.lkpArticulo.EditValue > 0 Then
                If Departamento <= 0 Or Grupo <= 0 Then
                    Actualiza_Lookups()
                    
                End If
            End If
        End If

        If Me.clcPrecioLista.EditValue = 0 Then
            Me.Precio_lista = 0
        Else

            If Not CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("precio_lista") Is System.DBNull.Value Then
                Me.Precio_lista = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("precio_lista")), Double)
            End If

        End If




        Me.bcosto = False
        Me.clcOrden.EditValue = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("orden")
        Me.chkSobrepedido.Checked = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("sobre_pedido")
        Me.clcfolioHistorico.Value = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("folio_historico_costo")
        Me.txtmodelo.Text = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("modelo")

        ObtenerPrecios()


        If OwnerForm.bcosteado Then
            Me.tbrTools.Buttons(0).Visible = False
        End If

        'If Me.clcPrecioLista.EditValue = 0 Then
        '    CalculaBasePrecioLista()
        'End If

        dcosto = Me.clcCosto.EditValue

        'If Me.chkPrecioLista.Checked = False Then
        '    Me.clcPrecioLista.Enabled = False
        'Else
        '    Me.clcPrecioLista.Enabled = True
        'End If


    End Sub

    Private Sub frmEntradasDetalles_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Select Case Action
            Case Actions.Insert
                Me.clcCosto.EditValue = Me.clcCostoPedido.EditValue
            Case Actions.Update
                Me.clcCosto.EditValue = Me.clcCostoPedido.EditValue
                Me.lkpArticulo.Enabled = False
            Case Actions.Delete
        End Select



        Response = oArticulos.DespliegaDatos(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo"))
        Me.clcPrecioLista.EditValue = Response.Value.tables(0).rows(0).item("precio_lista")


    End Sub

    Private Sub frmEntradasDetalles_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        If CType(OwnerForm, frmEntradas).Action = Actions.Update Then
            Response = oEntradasdetalle.Validacion(Action, Articulo, Me.clcCosto.EditValue, Me.clcOrden.Value, Me.clcCantidadPedida.EditValue)
        Else
            If CType(OwnerForm, frmEntradas).Action = Actions.Insert Then
                Response = oEntradasdetalle.Validacion(Action, Articulo, Me.clcCosto.EditValue, Me.clcOrden.Value, Me.clcCantidadPedida.EditValue, Me.clcCantidad.EditValue)
            End If
        End If
    End Sub



#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        If (Me.lkpArticulo.EditValue > 0 And (Action.Update Or Actions.Delete)) Then
            '    Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Response = oDepartamentos.LookupFijo(Me.lkpArticulo.GetValue("departamento"))
        Else
            Response = oDepartamentos.LookupFijo(Departamento)
        End If

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub

    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events

        If (Me.lkpArticulo.EditValue > 0 And (Action.Update Or Actions.Delete)) Then
            Response = oGrupos.Lookup(Me.lkpArticulo.GetValue("departamento"))
        Else
            Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
        End If

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If
    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events

        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Me.lkpArticulo.DataSource Is Nothing Or Me.lkpArticulo.EditValue Is Nothing Or Not IsNumeric(Me.lkpArticulo.EditValue) Then Exit Sub

        Me.lbldescripcion.Text = lkpArticulo.GetValue("descripcion")
        If ((Action.Update Or Actions.Delete) And Me.lkpArticulo.EditValue > 0) Then
            'Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpDepartamento_LoadData(True)
            'Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("descripcion")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpGrupo_LoadData(True)
        End If

        ManejaSeries = Me.lkpArticulo.GetValue("maneja_series")

        If Action <> Actions.Insert Then Exit Sub

        Dim response As Events
        DespliegaPrecios(Articulo, response)
        If response.ErrorFound Then
            response.ShowMessage()
        End If

        If Me.lkpArticulo.EditValue > 0 Then
            If Me.lkpDepartamento.EditValue <= 0 Or Me.lkpGrupo.EditValue <= 0 Then
                Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
                Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            End If

        End If
    End Sub
    Private Sub lkpArticulo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.Validated
        If Me.lkpArticulo.Text = "" Then
            Me.lkpArticulo.Focus()
        End If
    End Sub

    Private Sub clcCosto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcCosto.EditValueChanged
        'If Me.chkPrecioLista.Checked = False Then
        '    CalculaBasePrecioLista()
        '    bRecalcularPrecios = True
        '    RecalcularPrecios(costo_pedido, costo_recibido, Precio_lista)
        '    bRecalcularPrecios = False
        'End If

        If IsNumeric(clcCosto.EditValue) = True Then
            Me.clcImporte.EditValue = CType(Me.clcCantidad.EditValue, Double) * CType(Me.clcCosto.EditValue, Double)
        End If

    End Sub

    Private Sub clcCantidad_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCantidad.EditValueChanged
        If IsNumeric(Me.clcCantidad.EditValue) = False Then Exit Sub
        Me.clcImporte.EditValue = CType(Me.clcCantidad.EditValue, Double) * CType(Me.clcCosto.EditValue, Double)
    End Sub
    Private Sub clcCantidad_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCantidad.GotFocus
        Me.clcCantidad.SelectAll()
    End Sub
    Private Sub clcCosto_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCosto.GotFocus
        Me.dcosto = Me.clcCosto.EditValue
    End Sub

    Private Sub grvPrecios_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvPrecios.CellValueChanged

        If brecalcular = False Then

            If ((Action.Update Or Actions.Delete) And (IsNumeric(grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcExpo)) = False)) Then
                grvPrecios.SetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcExpo, 0)
            End If
            If grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcUtilidad) > 100 Or grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcUtilidad) < 0 Then
                ShowMessage(MessageType.MsgInformation, "Porcentaje de Descuento Fuera del Rango V�lido")
                grvPrecios.SetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcUtilidad, 0)
            End If

            If e.Column.ColumnHandle = 5 And Me.grvPrecios.FocusedRowHandle <> (grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcExpo) - 1) Then 'descuento
                RecalcularPrecios(costo_pedido, costo_recibido, Precio_lista)
            ElseIf e.Column.ColumnHandle = 5 And Me.grvPrecios.FocusedRowHandle = (grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcExpo) - 1) And e.Value <> 0 Then
                grvPrecios.SetRowCellValue(grvPrecios.FocusedRowHandle, Me.grcUtilidad, 0)
            ElseIf e.Column.ColumnHandle = 4 Then
                RecalcularDescuentos(costo_pedido, costo_recibido)
            End If

        End If
    End Sub

    'Private Sub btnRecalcularPrecios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    bRecalcularPrecios = True
    '    RecalcularPrecios(costo_pedido, costo_recibido, Precio_lista)
    '    bRecalcularPrecios = False
    'End Sub

    'Private Sub chkPrecioLista_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    If CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("modificado") = False Then

    '        Dim oEvent As Events
    '        If Me.chkPrecioLista.Checked = False Then
    '            bRecalcula = True
    '            CalculaBasePrecioLista()
    '            bRecalcularPrecios = True
    '            RecalcularPrecios(costo_pedido, costo_recibido, Precio_lista)
    '            bRecalcularPrecios = False
    '            bRecalcula = False
    '        Else

    '            oEvent = oArticulos.DespliegaDatos(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo"))
    '            Me.clcPrecioLista.EditValue = oEvent.Value.tables(0).rows(0).item("precio_lista")
    '            ObtenerPrecios()
    '            'bRecalcularPrecios = True
    '            'RecalcularPrecios(0, 0, Precio_lista)
    '            'bRecalcularPrecios = False
    '        End If
    '    End If


    '    If Me.chkPrecioLista.Checked = False Then
    '        Me.clcPrecioLista.Enabled = False
    '        CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("modificado") = False
    '    Else
    '        Me.clcPrecioLista.Enabled = True
    '        CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("modificado") = True
    '    End If
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function CalculaImporteDetalle(ByVal ganancia As Double, ByVal ganancia_expo As Double, ByVal descuento As Double, ByVal iva As Double, ByVal costo As Double, ByVal expo As Boolean, ByVal terminacion As Long, ByVal bRecalculaPrecioLista As Boolean, ByRef precio_lista As Double) As Double
        Dim oEvent As New Dipros.Utils.Events

        oEvent = oArticulos.CalculaImporte(ganancia, ganancia_expo, descuento, iva, costo, expo, terminacion, bRecalculaPrecioLista, precio_lista)
        Return CType(oEvent.Value, Double)
    End Function
    Private Function CalculaDescuentoDetalle(ByVal ganancia As Double, ByVal ganancia_expo As Double, ByVal precio_venta As Double, ByVal iva As Double, ByVal costo As Double, ByVal expo As Boolean) As Double
        Dim oEvent As New Dipros.Utils.Events

        oEvent = oArticulos.CalculaDescuento(ganancia, ganancia_expo, precio_venta, iva, costo, expo)
        Return CType(oEvent.Value, Double)
    End Function
    Private Sub RecuperaPrecios()
        Me.grvPrecios.UpdateCurrentRow()
        dsPrecios = CType(Me.grvPrecios.DataSource, DataView).Table.DataSet
    End Sub
    Private Sub ObtenerPrecios()
        Dim Indice As Integer
        Indice = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("partida")
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                grPrecios.DataSource = CType(OwnerForm, frmEntradas).ArticulosPrecios(Indice).Precios.Tables(0)

            Case Actions.Delete
                grPrecios.DataSource = CType(OwnerForm, frmEntradas).ArticulosPrecios(Indice).Precios.Tables(0)
        End Select
    End Sub
    Private Sub ActualizaPrecios()
        Dim Aux As New ArticulosPreciosCostos

        Select Case Action
            Case Actions.Insert
                Aux.Indice = MaximaPartida()
                Aux.Precios = dsPrecios
                CType(OwnerForm, frmEntradas).ArticulosPrecios(Aux.Indice) = Aux
            Case Actions.Update
                Aux.Indice = Me.clcPartida.Value
                Aux.Precios = dsPrecios
                CType(OwnerForm, frmEntradas).ArticulosPrecios(Aux.Indice) = Aux
            Case Actions.Delete
        End Select
    End Sub
    Private Sub DespliegaPrecios(ByVal articulo As String, ByRef response As Events)
        Dim odataset As DataSet

        ' Despliega Datos de Precios
        response = oArticulosPrecios.Listado(articulo)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.grPrecios.DataSource = odataset.Tables(0)

        End If
        odataset = Nothing
    End Sub

    Private Sub RecalcularPrecios(ByVal costo_pedido As Double, ByVal costo_recibido As Double, ByRef precio_lista As Double)
        brecalcular = True
        Dim i As Long = 0
        Dim bexpo As Boolean
        bexpo = False
        Me.grvPrecios.UpdateCurrentRow()
        With Me.grvPrecios
            While i <= .RowCount - 1
                Dim ganancia As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcGanancia)
                Dim ganancia_expo As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcGanancia_expo)
                Dim descuento As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcUtilidad)
                Dim iva As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcIva)
                Dim lexpo As Long
                If IsDBNull(Me.grvPrecios.GetRowCellValue(i, Me.grcExpo)) Then
                    lexpo = -1
                Else
                    lexpo = Me.grvPrecios.GetRowCellValue(i, Me.grcExpo)
                End If

                Dim lterminacion As Long = Me.grvPrecios.GetRowCellValue(i, Me.grcTerminacion)
                If i + 1 = lexpo Then
                    bexpo = True
                Else
                    bexpo = False
                End If
                If bRecalcularPrecios Then
                    Me.grvPrecios.SetRowCellValue(i, Me.grcPrecioVenta, CalculaImporteDetalle(ganancia, ganancia_expo, descuento, iva, costo_recibido, bexpo, lterminacion, False, precio_lista))
                End If
                If (Not bRecalcularPrecios) And (dcosto <> Me.clcCosto.EditValue Or bRecalcula = True) Then
                    Me.Precio_lista = CalculaImporteDetalle(ganancia, ganancia_expo, descuento, iva, costo_recibido, bexpo, lterminacion, True, precio_lista)
                End If
                i = i + 1
            End While
        End With
        brecalcular = False
    End Sub
    Private Sub RecalcularDescuentos(ByVal costo_pedido As Double, ByVal costo_recibido As Double)
        brecalcular = True
        Dim i As Long = 0
        Dim bexpo As Boolean
        bexpo = False
        Me.grvPrecios.UpdateCurrentRow()
        With Me.grvPrecios
            While i <= .RowCount - 1
                Dim precio_venta As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcPrecioVenta)
                Dim ganancia As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcGanancia)
                Dim ganancia_expo As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcGanancia_expo)
                Dim descuento As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcUtilidad)
                Dim iva As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcIva)
                Dim lexpo As Long = Me.grvPrecios.GetRowCellValue(i, Me.grcExpo)
                If i + 1 = lexpo Then
                    bexpo = True
                Else
                    bexpo = False
                End If
                Me.grvPrecios.SetRowCellValue(i, Me.grcUtilidad, CalculaDescuentoDetalle(ganancia, ganancia_expo, precio_venta, iva, costo_recibido, bexpo))
                i = i + 1
            End While
        End With
        brecalcular = False
    End Sub
    Private Function MaximaPartida() As Long
        Dim maximo As Long = 0
        Dim rows() As DataRow
        Dim row As DataRow

        rows = CType(OwnerForm, frmEntradas).tmaEntradas.DataSource.Tables(0).Select("control <> 3")

        For Each row In rows
            If row.Item("partida") > maximo Then
                maximo = row.Item("partida")
            End If
        Next
        Return maximo

    End Function
    Private Sub Actualiza_Lookups()
        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpDepartamento_LoadData(True)
        If Me.lkpDepartamento.EditValue > 0 Then

            Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        End If

        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")

    End Sub

    'Private Sub CalculaBasePrecioLista()
    '    If Me.clcCosto.IsLoading = True Or Not IsNumeric(Me.clcCosto.EditValue) Then Exit Sub
    '    Me.clcImporte.EditValue = CType(Me.clcCantidad.EditValue, Double) * CType(Me.clcCosto.EditValue, Double)

    '    If IsNumeric(Me.clcCosto.EditValue) And bcosto = False And (dcosto <> Me.clcCosto.EditValue Or bRecalcula = True) Then
    '        bRecalcularPrecios = False
    '        RecalcularPrecios(costo_pedido, costo_recibido, Precio_lista)
    '        Me.dcosto = Me.clcCosto.EditValue
    '    End If
    'End Sub

#End Region

    
    
End Class
