Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmInventarioFisicoAjustar
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpConceptoEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConceptoEntrada As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConceptoSalida As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmInventarioFisicoAjustar))
        Me.lkpConceptoEntrada = New Dipros.Editors.TINMultiLookup
        Me.lblConceptoEntrada = New System.Windows.Forms.Label
        Me.lkpConceptoSalida = New Dipros.Editors.TINMultiLookup
        Me.lblConceptoSalida = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(75, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lkpConceptoEntrada
        '
        Me.lkpConceptoEntrada.AllowAdd = False
        Me.lkpConceptoEntrada.AutoReaload = False
        Me.lkpConceptoEntrada.DataSource = Nothing
        Me.lkpConceptoEntrada.DefaultSearchField = ""
        Me.lkpConceptoEntrada.DisplayMember = "descripcion"
        Me.lkpConceptoEntrada.EditValue = Nothing
        Me.lkpConceptoEntrada.Filtered = False
        Me.lkpConceptoEntrada.InitValue = Nothing
        Me.lkpConceptoEntrada.Location = New System.Drawing.Point(130, 40)
        Me.lkpConceptoEntrada.MultiSelect = False
        Me.lkpConceptoEntrada.Name = "lkpConceptoEntrada"
        Me.lkpConceptoEntrada.NullText = ""
        Me.lkpConceptoEntrada.PopupWidth = CType(400, Long)
        Me.lkpConceptoEntrada.Required = False
        Me.lkpConceptoEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoEntrada.SearchMember = ""
        Me.lkpConceptoEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoEntrada.SelectAll = False
        Me.lkpConceptoEntrada.Size = New System.Drawing.Size(192, 22)
        Me.lkpConceptoEntrada.TabIndex = 1
        Me.lkpConceptoEntrada.Tag = ""
        Me.lkpConceptoEntrada.ToolTip = Nothing
        Me.lkpConceptoEntrada.ValueMember = "concepto"
        '
        'lblConceptoEntrada
        '
        Me.lblConceptoEntrada.AutoSize = True
        Me.lblConceptoEntrada.Location = New System.Drawing.Point(17, 40)
        Me.lblConceptoEntrada.Name = "lblConceptoEntrada"
        Me.lblConceptoEntrada.Size = New System.Drawing.Size(107, 16)
        Me.lblConceptoEntrada.TabIndex = 0
        Me.lblConceptoEntrada.Tag = ""
        Me.lblConceptoEntrada.Text = "Co&ncepto Entrada:"
        Me.lblConceptoEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoSalida
        '
        Me.lkpConceptoSalida.AllowAdd = False
        Me.lkpConceptoSalida.AutoReaload = False
        Me.lkpConceptoSalida.DataSource = Nothing
        Me.lkpConceptoSalida.DefaultSearchField = ""
        Me.lkpConceptoSalida.DisplayMember = "descripcion"
        Me.lkpConceptoSalida.EditValue = Nothing
        Me.lkpConceptoSalida.Filtered = False
        Me.lkpConceptoSalida.InitValue = Nothing
        Me.lkpConceptoSalida.Location = New System.Drawing.Point(130, 64)
        Me.lkpConceptoSalida.MultiSelect = False
        Me.lkpConceptoSalida.Name = "lkpConceptoSalida"
        Me.lkpConceptoSalida.NullText = ""
        Me.lkpConceptoSalida.PopupWidth = CType(400, Long)
        Me.lkpConceptoSalida.Required = False
        Me.lkpConceptoSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoSalida.SearchMember = ""
        Me.lkpConceptoSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoSalida.SelectAll = False
        Me.lkpConceptoSalida.Size = New System.Drawing.Size(192, 22)
        Me.lkpConceptoSalida.TabIndex = 3
        Me.lkpConceptoSalida.Tag = ""
        Me.lkpConceptoSalida.ToolTip = Nothing
        Me.lkpConceptoSalida.ValueMember = "concepto"
        '
        'lblConceptoSalida
        '
        Me.lblConceptoSalida.AutoSize = True
        Me.lblConceptoSalida.Location = New System.Drawing.Point(26, 64)
        Me.lblConceptoSalida.Name = "lblConceptoSalida"
        Me.lblConceptoSalida.Size = New System.Drawing.Size(98, 16)
        Me.lblConceptoSalida.TabIndex = 2
        Me.lblConceptoSalida.Tag = ""
        Me.lblConceptoSalida.Text = "Conc&epto Salida:"
        Me.lblConceptoSalida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmInventarioFisicoAjustar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(338, 96)
        Me.Controls.Add(Me.lkpConceptoSalida)
        Me.Controls.Add(Me.lblConceptoSalida)
        Me.Controls.Add(Me.lkpConceptoEntrada)
        Me.Controls.Add(Me.lblConceptoEntrada)
        Me.Name = "frmInventarioFisicoAjustar"
        Me.Text = "frmInventarioFisicoAjustar"
        Me.Controls.SetChildIndex(Me.lblConceptoEntrada, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoEntrada, 0)
        Me.Controls.SetChildIndex(Me.lblConceptoSalida, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoSalida, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Public bodega As String
    Public inventario As Long
    Public fecha As DateTime
#End Region

#Region "Declaraciones"
    Private oConceptosInventario As New VillarrealBusiness.clsConceptosInventario
    Private oInventarioFisico As New VillarrealBusiness.clsInventarioFisico

    Private ReadOnly Property ConceptoEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoEntrada)
        End Get
    End Property

    Private ReadOnly Property ConceptoSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoSalida)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmInventarioFisicoAjustar_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oInventarioFisico.Ajustar(Comunes.Common.Sucursal_Actual, bodega, inventario, fecha, ConceptoEntrada, ConceptoSalida)
    End Sub

    Private Sub frmInventarioFisicoAjustar_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        OwnerForm.Enabled = True
    End Sub

    Private Sub frmInventarioFisicoAjustar_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oInventarioFisico.ValidacionAjustar(ConceptoEntrada, ConceptoSalida)
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub lkpConceptoEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoEntrada.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConceptoEntrada_Format() Handles lkpConceptoEntrada.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoEntrada)
    End Sub

    Private Sub lkpConceptoSalida_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoSalida.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConceptoSalida_Format() Handles lkpConceptoSalida.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoSalida)
    End Sub
#End Region

#Region "Funcionalidad"

#End Region

End Class
