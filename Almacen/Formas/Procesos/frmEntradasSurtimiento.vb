Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmEntradasSurtimiento
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents lkpEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grvArticulosEntrada As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArt As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRecibida As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcDescr As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grArticulosEntrada As DevExpress.XtraGrid.GridControl
    Friend WithEvents txtSerie As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMod As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEntradasSurtimiento))
        Me.lblEntrada = New System.Windows.Forms.Label
        Me.lkpEntrada = New Dipros.Editors.TINMultiLookup
        Me.grArticulosEntrada = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSerie = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.grvArticulosEntrada = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArt = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMod = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescr = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCant = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRecibida = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        CType(Me.grArticulosEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulosEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRecibida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3021, 28)
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Location = New System.Drawing.Point(32, 51)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(52, 16)
        Me.lblEntrada.TabIndex = 0
        Me.lblEntrada.Tag = ""
        Me.lblEntrada.Text = "Entrada:"
        Me.lblEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEntrada
        '
        Me.lkpEntrada.AllowAdd = False
        Me.lkpEntrada.AutoReaload = False
        Me.lkpEntrada.DataSource = Nothing
        Me.lkpEntrada.DefaultSearchField = ""
        Me.lkpEntrada.DisplayMember = "entrada"
        Me.lkpEntrada.EditValue = Nothing
        Me.lkpEntrada.Filtered = False
        Me.lkpEntrada.InitValue = Nothing
        Me.lkpEntrada.Location = New System.Drawing.Point(88, 48)
        Me.lkpEntrada.MultiSelect = False
        Me.lkpEntrada.Name = "lkpEntrada"
        Me.lkpEntrada.NullText = ""
        Me.lkpEntrada.PopupWidth = CType(400, Long)
        Me.lkpEntrada.Required = True
        Me.lkpEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEntrada.SearchMember = ""
        Me.lkpEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEntrada.SelectAll = False
        Me.lkpEntrada.Size = New System.Drawing.Size(208, 20)
        Me.lkpEntrada.TabIndex = 1
        Me.lkpEntrada.Tag = ""
        Me.lkpEntrada.ToolTip = Nothing
        Me.lkpEntrada.ValueMember = "entrada"
        '
        'grArticulosEntrada
        '
        '
        'grArticulosEntrada.EmbeddedNavigator
        '
        Me.grArticulosEntrada.EmbeddedNavigator.Name = ""
        Me.grArticulosEntrada.LevelDefaults.Add("Detalle", Me.grvArticulos)
        Me.grArticulosEntrada.Location = New System.Drawing.Point(8, 88)
        Me.grArticulosEntrada.MainView = Me.grvArticulosEntrada
        Me.grArticulosEntrada.Name = "grArticulosEntrada"
        Me.grArticulosEntrada.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRecibida, Me.txtSerie})
        Me.grArticulosEntrada.Size = New System.Drawing.Size(632, 336)
        Me.grArticulosEntrada.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grArticulosEntrada.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulosEntrada.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulosEntrada.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulosEntrada.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulosEntrada.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulosEntrada.TabIndex = 2
        Me.grArticulosEntrada.Text = "ArticulosEntrada"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcCantidad, Me.grcUnidad, Me.grcSerie})
        Me.grvArticulos.GridControl = Me.grArticulosEntrada
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsCustomization.AllowFilter = False
        Me.grvArticulos.OptionsCustomization.AllowGroup = False
        Me.grvArticulos.OptionsCustomization.AllowSort = False
        Me.grvArticulos.OptionsDetail.ShowDetailTabs = True
        Me.grvArticulos.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvArticulos.OptionsNavigation.UseTabKey = False
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        Me.grvArticulos.OptionsView.ShowIndicator = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Articulo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 0
        Me.grcArticulo.Width = 78
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 91
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripcion"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 2
        Me.grcDescripcion.Width = 290
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.DisplayFormat.FormatString = "0"
        Me.grcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 60
        '
        'grcUnidad
        '
        Me.grcUnidad.Caption = "Unidad"
        Me.grcUnidad.FieldName = "nombre_unidad"
        Me.grcUnidad.Name = "grcUnidad"
        Me.grcUnidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.ColumnEdit = Me.txtSerie
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.VisibleIndex = 4
        Me.grcSerie.Width = 111
        '
        'txtSerie
        '
        Me.txtSerie.AutoHeight = False
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.ValidateOnEnterKey = True
        '
        'grvArticulosEntrada
        '
        Me.grvArticulosEntrada.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArt, Me.grcMod, Me.grcDescr, Me.grcCant})
        Me.grvArticulosEntrada.GridControl = Me.grArticulosEntrada
        Me.grvArticulosEntrada.Name = "grvArticulosEntrada"
        Me.grvArticulosEntrada.OptionsBehavior.Editable = False
        Me.grvArticulosEntrada.OptionsCustomization.AllowFilter = False
        Me.grvArticulosEntrada.OptionsCustomization.AllowGroup = False
        Me.grvArticulosEntrada.OptionsCustomization.AllowSort = False
        Me.grvArticulosEntrada.OptionsDetail.ShowDetailTabs = True
        Me.grvArticulosEntrada.OptionsDetail.SmartDetailHeight = True
        Me.grvArticulosEntrada.OptionsView.ShowGroupPanel = False
        Me.grvArticulosEntrada.OptionsView.ShowIndicator = False
        '
        'grcArt
        '
        Me.grcArt.Caption = "Art�culo"
        Me.grcArt.FieldName = "articulo"
        Me.grcArt.Name = "grcArt"
        Me.grcArt.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArt.VisibleIndex = 0
        Me.grcArt.Width = 110
        '
        'grcMod
        '
        Me.grcMod.Caption = "Modelo"
        Me.grcMod.FieldName = "modelo"
        Me.grcMod.Name = "grcMod"
        Me.grcMod.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMod.VisibleIndex = 1
        Me.grcMod.Width = 102
        '
        'grcDescr
        '
        Me.grcDescr.Caption = "Descripci�n"
        Me.grcDescr.FieldName = "descripcion_corta"
        Me.grcDescr.Name = "grcDescr"
        Me.grcDescr.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescr.VisibleIndex = 2
        Me.grcDescr.Width = 333
        '
        'grcCant
        '
        Me.grcCant.Caption = "Cantidad"
        Me.grcCant.DisplayFormat.FormatString = "0"
        Me.grcCant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCant.FieldName = "cantidad"
        Me.grcCant.Name = "grcCant"
        Me.grcCant.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCant.VisibleIndex = 3
        Me.grcCant.Width = 85
        '
        'chkRecibida
        '
        Me.chkRecibida.AutoHeight = False
        Me.chkRecibida.Name = "chkRecibida"
        '
        'frmEntradasSurtimiento
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(650, 432)
        Me.Controls.Add(Me.grArticulosEntrada)
        Me.Controls.Add(Me.lblEntrada)
        Me.Controls.Add(Me.lkpEntrada)
        Me.Name = "frmEntradasSurtimiento"
        Me.Text = "frmEntradasSurtimiento"
        Me.Controls.SetChildIndex(Me.lkpEntrada, 0)
        Me.Controls.SetChildIndex(Me.lblEntrada, 0)
        Me.Controls.SetChildIndex(Me.grArticulosEntrada, 0)
        CType(Me.grArticulosEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulosEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRecibida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Dim KS As Keys

    Private oEntradas As VillarrealBusiness.clsEntradas
    Private oOrdenesCompra As VillarrealBusiness.clsOrdenesCompra
    Private oEntradasDetallesSeries As VillarrealBusiness.clsEntradasDetallesSeries

    Private ReadOnly Property Entrada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEntrada)
        End Get
    End Property
    Private ReadOnly Property EntradaCosteada() As Boolean
        Get
            Return Me.lkpEntrada.GetValue("costeado")
        End Get
    End Property

    Private Data_Grid As DataSet

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmEntradasSurtimiento_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmEntradasSurtimiento_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmEntradasSurtimiento_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmEntradasSurtimiento_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEntradas = New VillarrealBusiness.clsEntradas
        oOrdenesCompra = New VillarrealBusiness.clsOrdenesCompra
        oEntradasDetallesSeries = New VillarrealBusiness.clsEntradasDetallesSeries
    End Sub

    Private Sub frmEntradasSurtimiento_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim orow As DataRow
        Dim oRowHijo As DataRow
        Dim arrRows() As DataRow

        'VMMR: 26/06/2006: CUANDO SE USAN AGRUPAMIENTOS EN UN GRID DE DEVEXPRESS, ESTE CREA VISTAS RELACIONADAS (RELATEDVIEWS)
        'Y TODAS HAY QUE HACER QUE SE ACTUALICEN O CIERREN SUS EDITORES SINO LOS CAMBIOS NO SE GUARDAN
        For i As Integer = 0 To grArticulosEntrada.Views.Count - 1
            grArticulosEntrada.Views(i).UpdateCurrentRow()
            grArticulosEntrada.Views(i).CloseEditor()
        Next

        For Each orow In CType(Me.grArticulosEntrada.DataSource, DataTable).Rows

            If Not Response.ErrorFound Then Response = oEntradasDetallesSeries.Eliminar(Entrada, orow("partida"), orow("articulo"))

            arrRows = orow.GetChildRows("ARTICULOS_DE_ENTRADA")
            For Each oRowHijo In arrRows
                If Not Response.ErrorFound And oRowHijo("numero_serie") <> "" Then Response = oEntradasDetallesSeries.Insertar(oRowHijo("entrada"), oRowHijo("partida"), oRowHijo("articulo"), oRowHijo("numero_serie"))
            Next

        Next

    End Sub

    Private Sub frmEntradasSurtimiento_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim orow As DataRow
        Dim oRowHijo As DataRow
        Dim arrRows() As DataRow

        Response = oEntradasDetallesSeries.Validacion(Entrada)

        If Not Response.ErrorFound Then Response = Me.ValidaNumerosSerie

    End Sub

    Private Sub frmEntradasSurtimiento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub frmEntradasSurtimiento_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Validating
        
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "lkpEntrada"
    Private Sub lkpEntrada_Format() Handles lkpEntrada.Format
        modFormatos.for_entradas_grl(Me.lkpEntrada)
    End Sub
    Private Sub lkpEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpEntrada.LoadData
        Dim response As Events
        Me.lkpEntrada.EditValue = Nothing
        response = oEntradas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpEntrada.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpEntrada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEntrada.EditValueChanged
        ActualizaGrid()

        Me.grvArticulosEntrada.ExpandAllGroups()
        Me.grvArticulos.ExpandAllGroups()

        If EntradaCosteada = True Then
            Comunes.clsUtilerias.InhabilitaColumnas(Me.grcSerie)
            Me.tbrTools.Buttons.Item(0).Enabled = False
        Else
            Comunes.clsUtilerias.HablilitaColumnas(Me.grcSerie)
            Me.tbrTools.Buttons.Item(0).Enabled = True
        End If
    End Sub
#End Region

    Private Sub grvArticulos_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles grvArticulos.ValidateRow
        Dim oDataView As DataView
        oDataView = sender.datasource

        For i As Integer = 0 To oDataView.Table.Rows.Count - 1
            For j As Integer = 0 To oDataView.Table.Rows.Count - 1
                If i <> j Then
                    If oDataView.Table.Rows(i).Item("numero_serie") = oDataView.Table.Rows(j).Item("numero_serie") And oDataView.Table.Rows(i).Item("numero_serie") <> "" Then
                        e.ErrorText = "El numero de serie " + oDataView.Table.Rows(i).Item("numero_serie") + " se encuentra repetido"
                        e.Valid = False
                        Exit Sub
                    End If
                End If
            Next
        Next
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Public Sub ActualizaGrid()
        Dim Response As Dipros.Utils.Events

        Response = oEntradas.DespliegaArticulosParaSurtimiento(Entrada)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Data_Grid = oDataSet

            oDataSet.Relations.Add( _
             "ARTICULOS_DE_ENTRADA", _
             New DataColumn() {oDataSet.Tables(0).Columns("entrada"), _
                               oDataSet.Tables(0).Columns("partida"), _
                               oDataSet.Tables(0).Columns("articulo")}, _
             New DataColumn() {oDataSet.Tables(1).Columns("entrada"), _
                               oDataSet.Tables(1).Columns("partida"), _
                               oDataSet.Tables(1).Columns("articulo")}, False)

            grArticulosEntrada.LevelDefaults.Add("ARTICULOS_DE_ENTRADA", grvArticulos)
            grvArticulosEntrada.OptionsDetail.EnableMasterViewMode = True
            grvArticulosEntrada.OptionsView.ShowChildrenInGroupPanel = True
            grArticulosEntrada.DataSource = oDataSet.Tables(0)
            Me.grvArticulosEntrada.ExpandAllGroups()
        End If
    End Sub
    Public Function ValidaNumerosSerie() As Events
        Dim oEvents As New Events
        Dim i As Integer = 0

        If Me.Data_Grid.Tables(1).Rows.Count > 0 Then Me.grvArticulos.SelectRange(0, 0)

        Dim condicion As String
        While i < Me.Data_Grid.Tables(1).Rows.Count
            condicion = "partida = " + CType(Me.Data_Grid.Tables(1).Rows(i).Item("partida"), String) + _
                        " and articulo = " + CType(Me.Data_Grid.Tables(1).Rows(i).Item("articulo"), String) + _
                        " and numero_serie = '" + CType(Me.Data_Grid.Tables(1).Rows(i).Item("numero_serie"), String) + "' "
            If Me.Data_Grid.Tables(1).Select(condicion).Length > 1 And CType(Me.Data_Grid.Tables(1).Rows(i).Item("numero_serie"), String).Trim.Length > 0 Then
                'termina ciclo y regresas un true para decir que no se puede gtuardar
                oEvents.Ex = Nothing
                oEvents.Message = "El N�mero de Serie no se Puede Repetir"
                Exit While
            End If
            i = i + 1
        End While

        Return oEvents

    End Function

#End Region



End Class
