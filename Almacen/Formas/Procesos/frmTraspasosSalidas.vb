Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports Comunes.clsUtilerias

Public Class frmTraspasosSalidas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys

    Private folio_movimiento As Long = 0
    Private Guardado As Boolean = False
    Private folio_solicitud As Long
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblTraspaso As System.Windows.Forms.Label
    Friend WithEvents clcTraspaso As Dipros.Editors.TINCalcEdit

    Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Friend WithEvents lblConcepto_Salida As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Salida As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Salida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblObservaciones_Salida As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones_Salida As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblSucursal_Entrada As System.Windows.Forms.Label

    Friend WithEvents lblBodega_Entrada As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConceptoSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursalEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents grTraspasos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvTraspasos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaTraspasos As Dipros.Windows.TINMaster
    Friend WithEvents gdcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcDescripcionCorta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcManejaSeries As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcFolioHisCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tlbReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents lkpChofer As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpSolicitudTraspaso As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gdcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpBodeguero As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodeguero As System.Windows.Forms.Label


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTraspasosSalidas))
        Me.lblTraspaso = New System.Windows.Forms.Label
        Me.clcTraspaso = New Dipros.Editors.TINCalcEdit
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.lblConcepto_Salida = New System.Windows.Forms.Label
        Me.lblFolio_Salida = New System.Windows.Forms.Label
        Me.clcFolio_Salida = New Dipros.Editors.TINCalcEdit
        Me.lblObservaciones_Salida = New System.Windows.Forms.Label
        Me.txtObservaciones_Salida = New DevExpress.XtraEditors.MemoEdit
        Me.lblSucursal_Entrada = New System.Windows.Forms.Label
        Me.lblBodega_Entrada = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpConceptoSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaEntrada = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursalEntrada = New Dipros.Editors.TINMultiLookup
        Me.grTraspasos = New DevExpress.XtraGrid.GridControl
        Me.grvTraspasos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gdcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcDescripcionCorta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcManejaSeries = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gdcFolioHisCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaTraspasos = New Dipros.Windows.TINMaster
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.tlbReimprimir = New System.Windows.Forms.ToolBarButton
        Me.lkpChofer = New Dipros.Editors.TINMultiLookup
        Me.lblChofer = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpSolicitudTraspaso = New Dipros.Editors.TINMultiLookup
        Me.lkpBodeguero = New Dipros.Editors.TINMultiLookup
        Me.lblBodeguero = New System.Windows.Forms.Label
        CType(Me.clcTraspaso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Salida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones_Salida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(8446, 28)
        '
        'lblTraspaso
        '
        Me.lblTraspaso.AutoSize = True
        Me.lblTraspaso.Location = New System.Drawing.Point(110, 40)
        Me.lblTraspaso.Name = "lblTraspaso"
        Me.lblTraspaso.Size = New System.Drawing.Size(59, 16)
        Me.lblTraspaso.TabIndex = 0
        Me.lblTraspaso.Tag = ""
        Me.lblTraspaso.Text = "&Traspaso:"
        Me.lblTraspaso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTraspaso
        '
        Me.clcTraspaso.EditValue = "0"
        Me.clcTraspaso.Location = New System.Drawing.Point(176, 39)
        Me.clcTraspaso.MaxValue = 0
        Me.clcTraspaso.MinValue = 0
        Me.clcTraspaso.Name = "clcTraspaso"
        '
        'clcTraspaso.Properties
        '
        Me.clcTraspaso.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcTraspaso.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTraspaso.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcTraspaso.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTraspaso.Properties.Enabled = False
        Me.clcTraspaso.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTraspaso.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTraspaso.Size = New System.Drawing.Size(48, 19)
        Me.clcTraspaso.TabIndex = 1
        Me.clcTraspaso.Tag = "traspaso"
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(78, 64)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(91, 16)
        Me.lblBodega_Salida.TabIndex = 4
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "&Bodega Origen:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblConcepto_Salida
        '
        Me.lblConcepto_Salida.AutoSize = True
        Me.lblConcepto_Salida.Location = New System.Drawing.Point(109, 88)
        Me.lblConcepto_Salida.Name = "lblConcepto_Salida"
        Me.lblConcepto_Salida.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto_Salida.TabIndex = 6
        Me.lblConcepto_Salida.Tag = ""
        Me.lblConcepto_Salida.Text = "Co&ncepto:"
        Me.lblConcepto_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Salida
        '
        Me.lblFolio_Salida.AutoSize = True
        Me.lblFolio_Salida.Location = New System.Drawing.Point(134, 110)
        Me.lblFolio_Salida.Name = "lblFolio_Salida"
        Me.lblFolio_Salida.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Salida.TabIndex = 8
        Me.lblFolio_Salida.Tag = ""
        Me.lblFolio_Salida.Text = "&Folio:"
        Me.lblFolio_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Salida
        '
        Me.clcFolio_Salida.EditValue = "0"
        Me.clcFolio_Salida.Location = New System.Drawing.Point(176, 109)
        Me.clcFolio_Salida.MaxValue = 0
        Me.clcFolio_Salida.MinValue = 0
        Me.clcFolio_Salida.Name = "clcFolio_Salida"
        '
        'clcFolio_Salida.Properties
        '
        Me.clcFolio_Salida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Salida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Salida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Salida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Salida.Properties.Enabled = False
        Me.clcFolio_Salida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Salida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Salida.Size = New System.Drawing.Size(104, 19)
        Me.clcFolio_Salida.TabIndex = 9
        Me.clcFolio_Salida.Tag = "folio_salida"
        '
        'lblObservaciones_Salida
        '
        Me.lblObservaciones_Salida.AutoSize = True
        Me.lblObservaciones_Salida.Location = New System.Drawing.Point(22, 132)
        Me.lblObservaciones_Salida.Name = "lblObservaciones_Salida"
        Me.lblObservaciones_Salida.Size = New System.Drawing.Size(147, 16)
        Me.lblObservaciones_Salida.TabIndex = 10
        Me.lblObservaciones_Salida.Tag = ""
        Me.lblObservaciones_Salida.Text = "&Observaciones de Origen:"
        Me.lblObservaciones_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones_Salida
        '
        Me.txtObservaciones_Salida.EditValue = ""
        Me.txtObservaciones_Salida.Location = New System.Drawing.Point(176, 132)
        Me.txtObservaciones_Salida.Name = "txtObservaciones_Salida"
        Me.txtObservaciones_Salida.Size = New System.Drawing.Size(414, 38)
        Me.txtObservaciones_Salida.TabIndex = 11
        Me.txtObservaciones_Salida.Tag = "observaciones_salida"
        '
        'lblSucursal_Entrada
        '
        Me.lblSucursal_Entrada.AutoSize = True
        Me.lblSucursal_Entrada.Location = New System.Drawing.Point(67, 199)
        Me.lblSucursal_Entrada.Name = "lblSucursal_Entrada"
        Me.lblSucursal_Entrada.Size = New System.Drawing.Size(102, 16)
        Me.lblSucursal_Entrada.TabIndex = 14
        Me.lblSucursal_Entrada.Tag = ""
        Me.lblSucursal_Entrada.Text = "&Sucursal Destino:"
        Me.lblSucursal_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega_Entrada
        '
        Me.lblBodega_Entrada.AutoSize = True
        Me.lblBodega_Entrada.Location = New System.Drawing.Point(73, 176)
        Me.lblBodega_Entrada.Name = "lblBodega_Entrada"
        Me.lblBodega_Entrada.Size = New System.Drawing.Size(96, 16)
        Me.lblBodega_Entrada.TabIndex = 12
        Me.lblBodega_Entrada.Tag = ""
        Me.lblBodega_Entrada.Text = "Bodega &Destino:"
        Me.lblBodega_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(645, 64)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 21
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "Stat&us:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblStatus.Visible = False
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "P"
        Me.cboStatus.Location = New System.Drawing.Point(698, 62)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Recibido", "R", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 20)
        Me.cboStatus.TabIndex = 22
        Me.cboStatus.Tag = "status"
        Me.cboStatus.Visible = False
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(176, 62)
        Me.lkpBodegaSalida.MultiSelect = False
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = ""
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = True
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(415, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 5
        Me.lkpBodegaSalida.Tag = "bodega_salida"
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lkpConceptoSalida
        '
        Me.lkpConceptoSalida.AllowAdd = False
        Me.lkpConceptoSalida.AutoReaload = False
        Me.lkpConceptoSalida.DataSource = Nothing
        Me.lkpConceptoSalida.DefaultSearchField = ""
        Me.lkpConceptoSalida.DisplayMember = "descripcion"
        Me.lkpConceptoSalida.EditValue = Nothing
        Me.lkpConceptoSalida.Enabled = False
        Me.lkpConceptoSalida.Filtered = False
        Me.lkpConceptoSalida.InitValue = Nothing
        Me.lkpConceptoSalida.Location = New System.Drawing.Point(176, 86)
        Me.lkpConceptoSalida.MultiSelect = False
        Me.lkpConceptoSalida.Name = "lkpConceptoSalida"
        Me.lkpConceptoSalida.NullText = ""
        Me.lkpConceptoSalida.PopupWidth = CType(400, Long)
        Me.lkpConceptoSalida.ReadOnlyControl = False
        Me.lkpConceptoSalida.Required = False
        Me.lkpConceptoSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoSalida.SearchMember = ""
        Me.lkpConceptoSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoSalida.SelectAll = False
        Me.lkpConceptoSalida.Size = New System.Drawing.Size(415, 20)
        Me.lkpConceptoSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoSalida.TabIndex = 7
        Me.lkpConceptoSalida.Tag = "concepto_salida"
        Me.lkpConceptoSalida.ToolTip = Nothing
        Me.lkpConceptoSalida.ValueMember = "concepto"
        '
        'lkpBodegaEntrada
        '
        Me.lkpBodegaEntrada.AllowAdd = False
        Me.lkpBodegaEntrada.AutoReaload = False
        Me.lkpBodegaEntrada.DataSource = Nothing
        Me.lkpBodegaEntrada.DefaultSearchField = ""
        Me.lkpBodegaEntrada.DisplayMember = "Descripcion"
        Me.lkpBodegaEntrada.EditValue = Nothing
        Me.lkpBodegaEntrada.Filtered = False
        Me.lkpBodegaEntrada.InitValue = Nothing
        Me.lkpBodegaEntrada.Location = New System.Drawing.Point(176, 174)
        Me.lkpBodegaEntrada.MultiSelect = False
        Me.lkpBodegaEntrada.Name = "lkpBodegaEntrada"
        Me.lkpBodegaEntrada.NullText = ""
        Me.lkpBodegaEntrada.PopupWidth = CType(400, Long)
        Me.lkpBodegaEntrada.ReadOnlyControl = False
        Me.lkpBodegaEntrada.Required = False
        Me.lkpBodegaEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaEntrada.SearchMember = ""
        Me.lkpBodegaEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaEntrada.SelectAll = False
        Me.lkpBodegaEntrada.Size = New System.Drawing.Size(414, 20)
        Me.lkpBodegaEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaEntrada.TabIndex = 13
        Me.lkpBodegaEntrada.Tag = "bodega_entrada"
        Me.lkpBodegaEntrada.ToolTip = Nothing
        Me.lkpBodegaEntrada.ValueMember = "Bodega"
        '
        'lkpSucursalEntrada
        '
        Me.lkpSucursalEntrada.AllowAdd = False
        Me.lkpSucursalEntrada.AutoReaload = False
        Me.lkpSucursalEntrada.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursalEntrada.DataSource = Nothing
        Me.lkpSucursalEntrada.DefaultSearchField = ""
        Me.lkpSucursalEntrada.DisplayMember = "nombre"
        Me.lkpSucursalEntrada.EditValue = Nothing
        Me.lkpSucursalEntrada.Enabled = False
        Me.lkpSucursalEntrada.Filtered = False
        Me.lkpSucursalEntrada.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursalEntrada.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursalEntrada.InitValue = Nothing
        Me.lkpSucursalEntrada.Location = New System.Drawing.Point(176, 197)
        Me.lkpSucursalEntrada.MultiSelect = False
        Me.lkpSucursalEntrada.Name = "lkpSucursalEntrada"
        Me.lkpSucursalEntrada.NullText = ""
        Me.lkpSucursalEntrada.PopupWidth = CType(400, Long)
        Me.lkpSucursalEntrada.ReadOnlyControl = False
        Me.lkpSucursalEntrada.Required = False
        Me.lkpSucursalEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursalEntrada.SearchMember = ""
        Me.lkpSucursalEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursalEntrada.SelectAll = False
        Me.lkpSucursalEntrada.Size = New System.Drawing.Size(413, 20)
        Me.lkpSucursalEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursalEntrada.TabIndex = 15
        Me.lkpSucursalEntrada.Tag = "sucursal_entrada"
        Me.lkpSucursalEntrada.ToolTip = Nothing
        Me.lkpSucursalEntrada.ValueMember = "Sucursal"
        '
        'grTraspasos
        '
        '
        'grTraspasos.EmbeddedNavigator
        '
        Me.grTraspasos.EmbeddedNavigator.Name = ""
        Me.grTraspasos.Location = New System.Drawing.Point(9, 297)
        Me.grTraspasos.MainView = Me.grvTraspasos
        Me.grTraspasos.Name = "grTraspasos"
        Me.grTraspasos.Size = New System.Drawing.Size(784, 232)
        Me.grTraspasos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grTraspasos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.TabIndex = 21
        Me.grTraspasos.TabStop = False
        Me.grTraspasos.Text = "Traspasos"
        '
        'grvTraspasos
        '
        Me.grvTraspasos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gdcArticulo, Me.gdcModelo, Me.gdcDescripcionCorta, Me.gdcCantidad, Me.gdcUnidad, Me.gdcSerie, Me.gdcManejaSeries, Me.gdcCosto, Me.gdcImporte, Me.gdcFolioHisCosto, Me.grcDepartamento, Me.grcGrupo})
        Me.grvTraspasos.GridControl = Me.grTraspasos
        Me.grvTraspasos.Name = "grvTraspasos"
        Me.grvTraspasos.OptionsBehavior.Editable = False
        Me.grvTraspasos.OptionsCustomization.AllowFilter = False
        Me.grvTraspasos.OptionsCustomization.AllowGroup = False
        Me.grvTraspasos.OptionsCustomization.AllowSort = False
        Me.grvTraspasos.OptionsView.ShowGroupPanel = False
        '
        'gdcArticulo
        '
        Me.gdcArticulo.Caption = "Artículo"
        Me.gdcArticulo.FieldName = "articulo"
        Me.gdcArticulo.Name = "gdcArticulo"
        Me.gdcArticulo.VisibleIndex = 0
        Me.gdcArticulo.Width = 67
        '
        'gdcModelo
        '
        Me.gdcModelo.Caption = "Modelo"
        Me.gdcModelo.FieldName = "modelo"
        Me.gdcModelo.Name = "gdcModelo"
        Me.gdcModelo.VisibleIndex = 1
        Me.gdcModelo.Width = 100
        '
        'gdcDescripcionCorta
        '
        Me.gdcDescripcionCorta.Caption = "Descripción"
        Me.gdcDescripcionCorta.FieldName = "descripcion_corta"
        Me.gdcDescripcionCorta.Name = "gdcDescripcionCorta"
        Me.gdcDescripcionCorta.VisibleIndex = 2
        Me.gdcDescripcionCorta.Width = 135
        '
        'gdcCantidad
        '
        Me.gdcCantidad.Caption = "Cantidad"
        Me.gdcCantidad.FieldName = "cantidad"
        Me.gdcCantidad.Name = "gdcCantidad"
        Me.gdcCantidad.VisibleIndex = 3
        Me.gdcCantidad.Width = 65
        '
        'gdcUnidad
        '
        Me.gdcUnidad.Caption = "Unidad"
        Me.gdcUnidad.FieldName = "nombre_unidad"
        Me.gdcUnidad.Name = "gdcUnidad"
        Me.gdcUnidad.VisibleIndex = 4
        Me.gdcUnidad.Width = 52
        '
        'gdcSerie
        '
        Me.gdcSerie.Caption = "Serie"
        Me.gdcSerie.FieldName = "numero_serie"
        Me.gdcSerie.Name = "gdcSerie"
        Me.gdcSerie.VisibleIndex = 5
        Me.gdcSerie.Width = 99
        '
        'gdcManejaSeries
        '
        Me.gdcManejaSeries.Caption = "Maneja Series"
        Me.gdcManejaSeries.FieldName = "maneja_series"
        Me.gdcManejaSeries.Name = "gdcManejaSeries"
        '
        'gdcCosto
        '
        Me.gdcCosto.Caption = "Costo"
        Me.gdcCosto.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcCosto.FieldName = "costo"
        Me.gdcCosto.Name = "gdcCosto"
        Me.gdcCosto.VisibleIndex = 6
        Me.gdcCosto.Width = 97
        '
        'gdcImporte
        '
        Me.gdcImporte.Caption = "Importe"
        Me.gdcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.gdcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gdcImporte.FieldName = "importe"
        Me.gdcImporte.Name = "gdcImporte"
        Me.gdcImporte.VisibleIndex = 7
        Me.gdcImporte.Width = 120
        '
        'gdcFolioHisCosto
        '
        Me.gdcFolioHisCosto.Caption = "FolioHisCosto"
        Me.gdcFolioHisCosto.FieldName = "folio_historico_costo"
        Me.gdcFolioHisCosto.Name = "gdcFolioHisCosto"
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        '
        'tmaTraspasos
        '
        Me.tmaTraspasos.BackColor = System.Drawing.Color.White
        Me.tmaTraspasos.CanDelete = True
        Me.tmaTraspasos.CanInsert = True
        Me.tmaTraspasos.CanUpdate = True
        Me.tmaTraspasos.Grid = Me.grTraspasos
        Me.tmaTraspasos.Location = New System.Drawing.Point(9, 273)
        Me.tmaTraspasos.Name = "tmaTraspasos"
        Me.tmaTraspasos.Size = New System.Drawing.Size(592, 23)
        Me.tmaTraspasos.TabIndex = 20
        Me.tmaTraspasos.TabStop = False
        Me.tmaTraspasos.Title = ""
        Me.tmaTraspasos.UpdateTitle = "un Registro"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 22, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(698, 38)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.ShowClear = False
        Me.dteFecha.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(648, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "F&echa:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlbReimprimir
        '
        Me.tlbReimprimir.Enabled = False
        Me.tlbReimprimir.ImageIndex = 6
        Me.tlbReimprimir.Text = "Reimprimir"
        '
        'lkpChofer
        '
        Me.lkpChofer.AllowAdd = False
        Me.lkpChofer.AutoReaload = False
        Me.lkpChofer.DataSource = Nothing
        Me.lkpChofer.DefaultSearchField = ""
        Me.lkpChofer.DisplayMember = "nombre"
        Me.lkpChofer.EditValue = Nothing
        Me.lkpChofer.Filtered = False
        Me.lkpChofer.InitValue = Nothing
        Me.lkpChofer.Location = New System.Drawing.Point(176, 221)
        Me.lkpChofer.MultiSelect = False
        Me.lkpChofer.Name = "lkpChofer"
        Me.lkpChofer.NullText = ""
        Me.lkpChofer.PopupWidth = CType(400, Long)
        Me.lkpChofer.ReadOnlyControl = False
        Me.lkpChofer.Required = True
        Me.lkpChofer.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChofer.SearchMember = ""
        Me.lkpChofer.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChofer.SelectAll = False
        Me.lkpChofer.Size = New System.Drawing.Size(414, 20)
        Me.lkpChofer.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpChofer.TabIndex = 17
        Me.lkpChofer.Tag = "chofer"
        Me.lkpChofer.ToolTip = Nothing
        Me.lkpChofer.ValueMember = "chofer"
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(124, 224)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(45, 16)
        Me.lblChofer.TabIndex = 16
        Me.lblChofer.Tag = ""
        Me.lblChofer.Text = "Ch&ofer:"
        Me.lblChofer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(391, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "So&licitud:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSolicitudTraspaso
        '
        Me.lkpSolicitudTraspaso.AllowAdd = False
        Me.lkpSolicitudTraspaso.AutoReaload = False
        Me.lkpSolicitudTraspaso.DataSource = Nothing
        Me.lkpSolicitudTraspaso.DefaultSearchField = ""
        Me.lkpSolicitudTraspaso.DisplayMember = "solicitud"
        Me.lkpSolicitudTraspaso.EditValue = Nothing
        Me.lkpSolicitudTraspaso.Filtered = False
        Me.lkpSolicitudTraspaso.InitValue = Nothing
        Me.lkpSolicitudTraspaso.Location = New System.Drawing.Point(454, 38)
        Me.lkpSolicitudTraspaso.MultiSelect = False
        Me.lkpSolicitudTraspaso.Name = "lkpSolicitudTraspaso"
        Me.lkpSolicitudTraspaso.NullText = ""
        Me.lkpSolicitudTraspaso.PopupWidth = CType(400, Long)
        Me.lkpSolicitudTraspaso.ReadOnlyControl = False
        Me.lkpSolicitudTraspaso.Required = False
        Me.lkpSolicitudTraspaso.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSolicitudTraspaso.SearchMember = ""
        Me.lkpSolicitudTraspaso.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSolicitudTraspaso.SelectAll = False
        Me.lkpSolicitudTraspaso.Size = New System.Drawing.Size(137, 20)
        Me.lkpSolicitudTraspaso.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSolicitudTraspaso.TabIndex = 1
        Me.lkpSolicitudTraspaso.Tag = "solicitud"
        Me.lkpSolicitudTraspaso.ToolTip = Nothing
        Me.lkpSolicitudTraspaso.ValueMember = "solicitud"
        '
        'lkpBodeguero
        '
        Me.lkpBodeguero.AllowAdd = False
        Me.lkpBodeguero.AutoReaload = True
        Me.lkpBodeguero.DataSource = Nothing
        Me.lkpBodeguero.DefaultSearchField = ""
        Me.lkpBodeguero.DisplayMember = "nombre"
        Me.lkpBodeguero.EditValue = Nothing
        Me.lkpBodeguero.Filtered = False
        Me.lkpBodeguero.InitValue = Nothing
        Me.lkpBodeguero.Location = New System.Drawing.Point(176, 245)
        Me.lkpBodeguero.MultiSelect = False
        Me.lkpBodeguero.Name = "lkpBodeguero"
        Me.lkpBodeguero.NullText = ""
        Me.lkpBodeguero.PopupWidth = CType(400, Long)
        Me.lkpBodeguero.ReadOnlyControl = False
        Me.lkpBodeguero.Required = False
        Me.lkpBodeguero.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodeguero.SearchMember = ""
        Me.lkpBodeguero.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodeguero.SelectAll = False
        Me.lkpBodeguero.Size = New System.Drawing.Size(414, 20)
        Me.lkpBodeguero.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodeguero.TabIndex = 19
        Me.lkpBodeguero.Tag = "bodeguero"
        Me.lkpBodeguero.ToolTip = Nothing
        Me.lkpBodeguero.ValueMember = "bodeguero"
        '
        'lblBodeguero
        '
        Me.lblBodeguero.AutoSize = True
        Me.lblBodeguero.Location = New System.Drawing.Point(101, 246)
        Me.lblBodeguero.Name = "lblBodeguero"
        Me.lblBodeguero.Size = New System.Drawing.Size(68, 16)
        Me.lblBodeguero.TabIndex = 18
        Me.lblBodeguero.Tag = ""
        Me.lblBodeguero.Text = "&Bodeguero:"
        Me.lblBodeguero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmTraspasosSalidas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(799, 535)
        Me.Controls.Add(Me.lkpBodeguero)
        Me.Controls.Add(Me.lblBodeguero)
        Me.Controls.Add(Me.lkpChofer)
        Me.Controls.Add(Me.lblChofer)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpBodegaEntrada)
        Me.Controls.Add(Me.lkpSucursalEntrada)
        Me.Controls.Add(Me.lkpConceptoSalida)
        Me.Controls.Add(Me.lkpBodegaSalida)
        Me.Controls.Add(Me.lblTraspaso)
        Me.Controls.Add(Me.clcTraspaso)
        Me.Controls.Add(Me.lblBodega_Salida)
        Me.Controls.Add(Me.lblConcepto_Salida)
        Me.Controls.Add(Me.lblFolio_Salida)
        Me.Controls.Add(Me.clcFolio_Salida)
        Me.Controls.Add(Me.lblObservaciones_Salida)
        Me.Controls.Add(Me.txtObservaciones_Salida)
        Me.Controls.Add(Me.lblSucursal_Entrada)
        Me.Controls.Add(Me.lblBodega_Entrada)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.grTraspasos)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.tmaTraspasos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpSolicitudTraspaso)
        Me.Name = "frmTraspasosSalidas"
        Me.Controls.SetChildIndex(Me.lkpSolicitudTraspaso, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.tmaTraspasos, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.grTraspasos, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal_Entrada, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones_Salida, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Salida, 0)
        Me.Controls.SetChildIndex(Me.clcTraspaso, 0)
        Me.Controls.SetChildIndex(Me.lblTraspaso, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaSalida, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoSalida, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursalEntrada, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaEntrada, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblChofer, 0)
        Me.Controls.SetChildIndex(Me.lkpChofer, 0)
        Me.Controls.SetChildIndex(Me.lblBodeguero, 0)
        Me.Controls.SetChildIndex(Me.lkpBodeguero, 0)
        CType(Me.clcTraspaso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Salida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones_Salida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oTraspasos As VillarrealBusiness.clsTraspasos
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario

    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oHistorialCostos As VillarrealBusiness.clsHisCostos

    Private oVariables As VillarrealBusiness.clsVariables
    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private ochoferes As VillarrealBusiness.clsChoferes
    Private oBodegueros As VillarrealBusiness.clsBodegueros

    Private oSolicitudTraspasos As VillarrealBusiness.clsSolicitudTraspasos

    Private SucursalSalida As Long = -1

    Private ReadOnly Property ConceptoCancelacionTraspaso() As String
        Get
            Return oVariables.TraeDatos("concepto_cancelacion_traspaso", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Friend ReadOnly Property BodegaSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get
    End Property
    Private ReadOnly Property BodegaEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaEntrada)
        End Get
    End Property
    Private ReadOnly Property SucursalEntrada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursalEntrada)
        End Get
    End Property
    Private ReadOnly Property ConceptoSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoSalida)
        End Get
    End Property
    Private ReadOnly Property FolioSalida() As Long
        Get
            Return Me.clcFolio_Salida.Value
        End Get
    End Property
    Public ReadOnly Property AfectaCostos() As Boolean
        Get
            Return Me.lkpConceptoSalida.GetValue("afecta_saldos")
        End Get
    End Property
    Private ReadOnly Property Chofer() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpChofer)
        End Get
    End Property
    Private ReadOnly Property SolicitudTraspaso() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSolicitudTraspaso)
        End Get
    End Property
    Private ReadOnly Property Bodeguero() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBodeguero)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmTraspasosSalidas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
        Guardado = False
    End Sub
    Private Sub frmTraspasosSalidas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmTraspasosSalidas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        If Me.Action = Actions.Insert Then
            Me.ImprimeTraspasoSalida(Me.clcTraspaso.Value)
        End If
    End Sub

    Private Sub frmTraspasosSalidas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oTraspasos.Insertar(Me.DataSource, Comunes.Common.Sucursal_Actual, folio_movimiento, False)

                If Not Response.ErrorFound Then GeneraMovimientoInventario(Response, folio_movimiento, ConceptoSalida)

                If Not Response.ErrorFound Then Response = oTraspasos.ActualizarFolioSalida(Me.clcTraspaso.Value, folio_movimiento)

                If SolicitudTraspaso > 0 Then
                    If Not Response.ErrorFound Then Response = oSolicitudTraspasos.ActualizarEstatus(SolicitudTraspaso, "SU", Me.clcTraspaso.EditValue)
                End If

            Case Actions.Update
                Response = oTraspasos.Actualizar(Me.DataSource, SucursalSalida)

                If Not Response.ErrorFound Then ActualizaMovimientoInventario(Response, Me.clcFolio_Salida.Value)

            Case Actions.Delete
                Response = oTraspasos.Eliminar(clcTraspaso.Value)

                If folio_solicitud > 0 Then
                    If Not Response.ErrorFound Then Response = oSolicitudTraspasos.ActualizarEstatus(folio_solicitud, "PS", 0)
                End If


        End Select
    End Sub
    Private Sub frmTraspasosSalidas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        If Action = Actions.Delete Then
            'si es una cancelacion se genera el movimiento de cancelacion de traspaso
            GeneraMovimientoInventario(Response, folio_movimiento, ConceptoCancelacionTraspaso)
        End If

        Dim partida_actual As Long = 0

        With tmaTraspasos
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.SelectedRow, BodegaSalida, ConceptoSalida, folio_movimiento, "I", "traspasos")
                        If Not Response.ErrorFound Then
                            Dim i As Long
                            For i = 1 To .Item("cantidad")

                                Dim articulo_activo As Long = .Item("articulo")

                                partida_actual = partida_actual + 1

                                CargarCostosArticulosGuardar(articulo_activo, 1, .Item("costo"), .Item("importe"), .Item("folio_historico_costo"))

                                Response = oHistorialCostos.ActualizaSaldo(.Item("folio_historico_costo"), -1)

                                'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, BodegaSalida, ConceptoSalida, folio_movimiento, dteFecha.EditValue, partida_actual, .Item("articulo"), 1, .Item("costo"), .Item("importe"), .Item("folio_historico_costo"), 0, System.DBNull.Value)

                                If .Item("maneja_series") And .Item("validanumeroserie") = True Then

                                    If .Item("entro_pantalla_validaserie") = True Then
                                        Response = Me.oMovimientosInventarioDetalleSeries.EliminarSerieMVC(.Item("articulo"), .Item("numero_serie"))
                                        ' CODIGO PARA MOSTRAR UN MESAJE PERSONALIZADO SI HUBO UN ERROR 
                                        If Response.ErrorFound Then
                                            If CType(CType(CType(Response.Ex, System.Exception).InnerException, System.Exception), System.Data.SqlClient.SqlException).Number = 515 Then
                                                Response.Message = "NO HAY UNA SERIE VALIDA DEL INVENTARIO INICIAL PARA EL ARTICULO:" & CType(.Item("articulo"), String)
                                            End If
                                        End If
                                    End If


                                    'AQUI SE INSERTA Y CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES, SE MANDA LA SERIE A LA BODEGA DE ENTRADA
                                    If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, BodegaSalida, ConceptoSalida, folio_movimiento, .Item("Partida"), .Item("articulo"))

                                    If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), BodegaEntrada)
                                End If

                                If Not Response.ErrorFound Then Response = oTraspasos.TraspasosSalidas_ActualizaExistencias_Ins(BodegaEntrada, .Item("articulo"), 1)


                            Next
                        Else
                            Exit Do
                        End If

                    Case Actions.Delete

                        If Me.Action = Actions.Delete Then
                            'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE POR LA CANCELACION DEL TRASPASO
                            If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, BodegaSalida, ConceptoCancelacionTraspaso, folio_movimiento, dteFecha.EditValue, System.DBNull.Value)
                            If .Item("maneja_series") Then
                                'AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES, SE REGRESA A LA BODEGA DE SALIDA
                                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), BodegaSalida)

                                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, BodegaSalida, ConceptoCancelacionTraspaso, folio_movimiento, .Item("Partida"), .Item("articulo"))
                            End If

                            If Not Response.ErrorFound Then Response = oTraspasos.TraspasosSalidas_ActualizaExistencias_Del(BodegaEntrada, .Item("articulo"), .Item("cantidad"))
                        End If

                        If Not Response.ErrorFound And .Item("folio_historico_costo") > 0 Then Response = oHistorialCostos.ActualizaSaldo(.Item("folio_historico_costo"), 1)

                End Select
                .MoveNext()
            Loop
        End With

    End Sub

    Private Sub frmTraspasosSalidas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim oDataSet As DataSet
        Response = oTraspasos.DespliegaDatos(OwnerForm.Value("traspaso"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count = 0 Then
                ShowMessage(MessageType.MsgInformation, "El usuario no tiene asignada la bodega de salida o no tiene permisos de ver TODAS las bodegas", "Traspasos de Salidas")
                Exit Sub
            End If

            Me.DataSource = oDataSet
            Me.lkpBodegaEntrada.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_entrada")
            SucursalSalida = oDataSet.Tables(0).Rows(0).Item("sucursal_salida")
        End If
        folio_solicitud = oDataSet.Tables(0).Rows(0).Item("solicitud")
        Me.lkpSolicitudTraspaso.Enabled = False

        Response = oMovimientosInventarioDetalle.Listado(Me.clcFolio_Salida.Value, Me.lkpBodegaSalida.EditValue, Me.lkpConceptoSalida.EditValue)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaTraspasos.DataSource = oDataSet
        End If

        folio_movimiento = Me.clcFolio_Salida.Value

        oDataSet = Nothing
    End Sub
    Private Sub frmTraspasosSalidas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oTraspasos = New VillarrealBusiness.clsTraspasos
        oSucursales = New VillarrealBusiness.clsSucursales
        oBodegas = New VillarrealBusiness.clsBodegas
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario

        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries

        oArticulos = New VillarrealBusiness.clsArticulos
        oHistorialCostos = New VillarrealBusiness.clsHisCostos
        oVariables = New VillarrealBusiness.clsVariables
        oUsuarios = New VillarrealBusiness.clsUsuarios
        oChoferes = New VillarrealBusiness.clsChoferes
        oBodegueros = New VillarrealBusiness.clsBodegueros

        oSolicitudTraspasos = New VillarrealBusiness.clsSolicitudTraspasos

        With tmaTraspasos
            .UpdateTitle = "un Artículo"
            .UpdateForm = New frmTraspasosDetalles
            .AddColumn("articulo")
            .AddColumn("modelo")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("nombre_unidad")
            .AddColumn("numero_serie")
            .AddColumn("maneja_series", "System.Boolean")
            .AddColumn("costo", "System.Decimal")
            .AddColumn("importe", "System.Decimal")
            .AddColumn("folio_historico_costo")
            .AddColumn("departamento", "System.Int32")
            .AddColumn("grupo", "System.Int32")
            .AddColumn("validanumeroserie", "System.Boolean")
            .AddColumn("entro_pantalla_validaserie", "System.Boolean")

        End With

        Me.lkpBodegaSalida.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)

        Select Case Action
            Case Actions.Insert
                Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
            Case Actions.Update
                Me.lkpBodegaSalida.Enabled = False
                Me.lkpBodegaEntrada.Enabled = False
                Me.tmaTraspasos.Enabled = False
                Me.grTraspasos.Enabled = False
                Me.tbrTools.Buttons(2).Enabled = True

                If OwnerForm.Value("status") = "R" Then
                    Me.txtObservaciones_Salida.Enabled = False
                    Me.lkpChofer.Enabled = False
                    Me.tbrTools.Buttons(0).Enabled = False
                    Me.tbrTools.Buttons(0).Visible = False
                End If

            Case Actions.Delete
                If OwnerForm.Value("status") = "R" Then
                    Me.tbrTools.Buttons(0).Enabled = False
                End If

                Me.tbrTools.Buttons(2).Enabled = False
        End Select
    End Sub
    Private Sub frmTraspasosSalidas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oTraspasos.Validacion(Action, Comunes.Common.Sucursal_Actual, BodegaSalida, ConceptoSalida, SucursalEntrada, BodegaEntrada, Chofer, cboStatus.EditValue, CType(Me.grTraspasos.DataSource, DataView), Bodeguero, Comunes.clsUtilerias.UsarSeries)
    End Sub
    Private Sub frmTraspasosSalidas_Localize() Handles MyBase.Localize
        Find("traspaso", CType(Me.clcTraspaso.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged
        Me.lkpBodegaEntrada.EditValue = Nothing
        CargarBodegasDestino()
        CargaConceptoSalida()
    End Sub

    Private Sub lkpBodegaEntrada_Format() Handles lkpBodegaEntrada.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaEntrada)
    End Sub
    Private Sub lkpBodegaEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaEntrada.LoadData
    End Sub
    Private Sub lkpBodegaEntrada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaEntrada.EditValueChanged
        Me.lkpSucursalEntrada.EditValue = Me.lkpBodegaEntrada.GetValue("sucursal")
    End Sub

    Private Sub lkpConceptoSalida_Format() Handles lkpConceptoSalida.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoSalida)
    End Sub
    Private Sub lkpConceptoSalida_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoSalida.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursalEntrada_Format() Handles lkpSucursalEntrada.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursalEntrada)
    End Sub
    Private Sub lkpSucursalEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpSucursalEntrada.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursalEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpChofer_Format() Handles lkpChofer.Format
        Comunes.clsFormato.for_choferes_grl(Me.lkpChofer)
    End Sub
    Private Sub lkpChofer_LoadData(ByVal Initialize As Boolean) Handles lkpChofer.LoadData
        Dim response As Events
        response = ochoferes.Lookup(1)   ''verificar el  parametro
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChofer.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpSolicitudTraspaso_Format() Handles lkpSolicitudTraspaso.Format
        Comunes.clsFormato.for_solicitud_traspasos_grl(Me.lkpSolicitudTraspaso)
    End Sub
    Private Sub lkpSolicitudTraspaso_LoadData(ByVal Initialize As Boolean) Handles lkpSolicitudTraspaso.LoadData
        Dim response As Events
        response = oSolicitudTraspasos.Lookup(TINApp.Connection.User)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSolicitudTraspaso.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpSolicitudTraspaso_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSolicitudTraspaso.EditValueChanged
        CargarSolicitudTraspasos(Me.lkpSolicitudTraspaso.EditValue)
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Reimprimir" Then
            ImprimeTraspasoSalida(Me.clcTraspaso.Value)
        End If
    End Sub


    Private Sub lkpBodeguero_Format() Handles lkpBodeguero.Format
        Comunes.clsFormato.for_bodegueros_grl(Me.lkpBodeguero)
    End Sub
    Private Sub lkpBodeguero_LoadData(ByVal Initialize As Boolean) Handles lkpBodeguero.LoadData
        Dim response As Events
        response = oBodegueros.Lookup(Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodeguero.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarBodegasDestino()
        Dim Response As New Events
        Response = oBodegas.BodegasDestino(BodegaSalida)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Public Sub CargarCostosArticulosGuardar(ByVal articulo As Long, ByVal cantidad_articulo As Long, ByRef costo_articulo As Object, ByRef importe_articulo As Object, ByRef folio_historico_costo As Object)
        Dim Response As Events
        Dim oDataSet As DataSet

        If AfectaCostos Then
            ' Traer costo del historial de costos
            Response = oHistorialCostos.UltimoCostoArticulo(articulo, Me.BodegaSalida)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    costo_articulo = oDataSet.Tables(0).Rows(0).Item("costo")
                    importe_articulo = costo_articulo * cantidad_articulo
                    folio_historico_costo = oDataSet.Tables(0).Rows(0).Item("folio")
                End If
            End If
        Else
            'Traer ultimo costo de Articulos
            Response = oArticulos.DespliegaDatos(articulo)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    costo_articulo = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
                    importe_articulo = costo_articulo * cantidad_articulo
                    folio_historico_costo = -1
                End If
            End If
        End If

        oDataSet = Nothing
    End Sub
    Private Function GeneraMovimientoInventario(ByRef response As Events, ByRef folio As Long, ByVal concepto As String)

        If Guardado = False Then
            response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, BodegaSalida, concepto, folio, _
                        dteFecha.EditValue, Comunes.Common.Sucursal_Actual, ConceptoSalida, Me.clcTraspaso.Value, "Movimiento generado desde salidas de traspasos")
            Guardado = True
        End If

    End Function
    Private Function ActualizaMovimientoInventario(ByRef response As Events, ByRef folio As Long)
        If Guardado = False Then
            response = oMovimientosInventario.Actualizar(SucursalSalida, BodegaSalida, ConceptoSalida, folio, _
                        dteFecha.EditValue, Comunes.Common.Sucursal_Actual, ConceptoSalida, Me.clcTraspaso.Value, "Movimiento generado desde salidas de traspasos")
            Guardado = True
        End If

    End Function
    Private Sub ActualizaHistorial(ByRef Response As Events, ByVal folio_historial As Long, ByVal cantidad As Long)
        Response = oHistorialCostos.ActualizaSaldo(folio_historial, cantidad)
    End Sub
    Private Function CargaConceptoSalida() As Events
        Dim Response As Events

        Response = oBodegas.BodegasSucursalesSel(BodegaSalida)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.lkpConceptoSalida.EditValue = CType(oDataSet.Tables(0).Rows(0).Item("concepto_salida_traspaso"), String)
            End If
            oDataSet = Nothing
        End If

        Return Response
    End Function
    Private Sub ImprimeTraspasoSalida(ByVal traspaso As Long)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeDeSalidaDeTraspasos(traspaso)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El vale de salida de traspaso no puede mostrarse")
            Else
                Dim oDataSet As DataSet
                Dim oReport As New rptTraspasosSalidas

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                ' oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Salida de Traspaso", oReport, , , , True)
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

    Private Sub CargarSolicitudTraspasos(ByVal solicitud As Long)
        Dim Response As New Events

        Dim oDataSet As DataSet
        Response = oSolicitudTraspasos.DespliegaDatos(solicitud)

        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.lkpBodegaSalida.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_salida")
            Me.lkpBodegaEntrada.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_entrada")
            Me.txtObservaciones_Salida.Text = oDataSet.Tables(0).Rows(0).Item("Observaciones")
        End If

        If Not Response.ErrorFound Then Response = oSolicitudTraspasos.ArticulosDeSolicitud(solicitud)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaTraspasos.DataSource = oDataSet
        End If

        oDataSet = Nothing

        Me.lkpBodegaEntrada.Enabled = False
        Me.lkpBodegaSalida.Enabled = False

        'CargarCostosArticulos()
    End Sub

#End Region

    Private Sub frmTraspasosSalidas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MenuOption.Name, "FECHA_TRA_SAL") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub
End Class
