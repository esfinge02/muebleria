Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmOrdenesCompraDetalle
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
    Friend WithEvents clcPrecio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblunidad As System.Windows.Forms.Label
    Friend WithEvents lbldescripcion_corta As System.Windows.Forms.Label
    Friend WithEvents clcCantidad_Anterior As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcCantidadMes As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcCantidadAnual As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcExistencia As Dipros.Editors.TINCalcEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCantidadSugeridaTexto As System.Windows.Forms.Label
    Friend WithEvents lblCantidadSugeridaValor As System.Windows.Forms.Label
    Friend WithEvents clcCantidadSugerida As DevExpress.XtraEditors.CalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesCompraDetalle))
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblPrecio = New System.Windows.Forms.Label
        Me.clcPrecio = New Dipros.Editors.TINCalcEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblunidad = New System.Windows.Forms.Label
        Me.lbldescripcion_corta = New System.Windows.Forms.Label
        Me.clcCantidad_Anterior = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcCantidadMes = New Dipros.Editors.TINCalcEdit
        Me.clcCantidadAnual = New Dipros.Editors.TINCalcEdit
        Me.clcExistencia = New Dipros.Editors.TINCalcEdit
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblCantidadSugeridaTexto = New System.Windows.Forms.Label
        Me.lblCantidadSugeridaValor = New System.Windows.Forms.Label
        Me.clcCantidadSugerida = New DevExpress.XtraEditors.CalcEdit
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad_Anterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidadMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidadAnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcExistencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidadSugerida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(661, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(67, 99)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 4
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Ar&tículo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(60, 144)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 8
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Ca&ntidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(123, 144)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "0"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "0"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.NullText = "0"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad.TabIndex = 9
        Me.clcCantidad.Tag = "cantidad"
        Me.clcCantidad.ToolTip = "cantidad"
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(78, 168)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(40, 16)
        Me.lblPrecio.TabIndex = 14
        Me.lblPrecio.Tag = ""
        Me.lblPrecio.Text = "C&osto:"
        Me.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecio
        '
        Me.clcPrecio.EditValue = "0"
        Me.clcPrecio.Location = New System.Drawing.Point(123, 168)
        Me.clcPrecio.MaxValue = 0
        Me.clcPrecio.MinValue = 0
        Me.clcPrecio.Name = "clcPrecio"
        '
        'clcPrecio.Properties
        '
        Me.clcPrecio.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecio.Properties.Precision = 2
        Me.clcPrecio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecio.Size = New System.Drawing.Size(88, 19)
        Me.clcPrecio.TabIndex = 15
        Me.clcPrecio.Tag = "costo"
        Me.clcPrecio.ToolTip = "costo"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(16, 312)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 12
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblImporte.Visible = False
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(80, 312)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(88, 19)
        Me.clcImporte.TabIndex = 13
        Me.clcImporte.Tag = "importe"
        Me.clcImporte.ToolTip = "importe"
        Me.clcImporte.Visible = False
        '
        'lblunidad
        '
        Me.lblunidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblunidad.Location = New System.Drawing.Point(288, 248)
        Me.lblunidad.Name = "lblunidad"
        Me.lblunidad.Size = New System.Drawing.Size(88, 16)
        Me.lblunidad.TabIndex = 15
        Me.lblunidad.Tag = "nombre_unidad"
        Me.lblunidad.Visible = False
        '
        'lbldescripcion_corta
        '
        Me.lbldescripcion_corta.Location = New System.Drawing.Point(123, 120)
        Me.lbldescripcion_corta.Name = "lbldescripcion_corta"
        Me.lbldescripcion_corta.Size = New System.Drawing.Size(268, 20)
        Me.lbldescripcion_corta.TabIndex = 7
        Me.lbldescripcion_corta.Tag = "descripcion_corta"
        Me.lbldescripcion_corta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clcCantidad_Anterior
        '
        Me.clcCantidad_Anterior.EditValue = "0"
        Me.clcCantidad_Anterior.Location = New System.Drawing.Point(216, 248)
        Me.clcCantidad_Anterior.MaxValue = 0
        Me.clcCantidad_Anterior.MinValue = 0
        Me.clcCantidad_Anterior.Name = "clcCantidad_Anterior"
        '
        'clcCantidad_Anterior.Properties
        '
        Me.clcCantidad_Anterior.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad_Anterior.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad_Anterior.Properties.Enabled = False
        Me.clcCantidad_Anterior.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad_Anterior.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad_Anterior.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad_Anterior.TabIndex = 714
        Me.clcCantidad_Anterior.TabStop = False
        Me.clcCantidad_Anterior.Tag = "cantidad_anterior"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripción:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidadMes
        '
        Me.clcCantidadMes.EditValue = "0"
        Me.clcCantidadMes.Location = New System.Drawing.Point(205, 280)
        Me.clcCantidadMes.MaxValue = 0
        Me.clcCantidadMes.MinValue = 0
        Me.clcCantidadMes.Name = "clcCantidadMes"
        '
        'clcCantidadMes.Properties
        '
        Me.clcCantidadMes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadMes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadMes.Properties.Enabled = False
        Me.clcCantidadMes.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidadMes.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidadMes.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidadMes.TabIndex = 61
        Me.clcCantidadMes.TabStop = False
        Me.clcCantidadMes.Tag = "cantidad_mes"
        Me.clcCantidadMes.Visible = False
        '
        'clcCantidadAnual
        '
        Me.clcCantidadAnual.EditValue = "0"
        Me.clcCantidadAnual.Location = New System.Drawing.Point(254, 280)
        Me.clcCantidadAnual.MaxValue = 0
        Me.clcCantidadAnual.MinValue = 0
        Me.clcCantidadAnual.Name = "clcCantidadAnual"
        '
        'clcCantidadAnual.Properties
        '
        Me.clcCantidadAnual.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadAnual.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadAnual.Properties.Enabled = False
        Me.clcCantidadAnual.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidadAnual.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidadAnual.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidadAnual.TabIndex = 62
        Me.clcCantidadAnual.TabStop = False
        Me.clcCantidadAnual.Tag = "cantidad_anual"
        Me.clcCantidadAnual.Visible = False
        '
        'clcExistencia
        '
        Me.clcExistencia.EditValue = "0"
        Me.clcExistencia.Location = New System.Drawing.Point(303, 280)
        Me.clcExistencia.MaxValue = 0
        Me.clcExistencia.MinValue = 0
        Me.clcExistencia.Name = "clcExistencia"
        '
        'clcExistencia.Properties
        '
        Me.clcExistencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExistencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExistencia.Properties.Enabled = False
        Me.clcExistencia.Properties.MaskData.EditMask = "########0.00"
        Me.clcExistencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcExistencia.Size = New System.Drawing.Size(48, 19)
        Me.clcExistencia.TabIndex = 63
        Me.clcExistencia.TabStop = False
        Me.clcExistencia.Tag = "existencia"
        Me.clcExistencia.Visible = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(128, 280)
        Me.CheckEdit1.Name = "CheckEdit1"
        '
        'CheckEdit1.Properties
        '
        Me.CheckEdit1.Properties.Caption = ""
        Me.CheckEdit1.Size = New System.Drawing.Size(75, 17)
        Me.CheckEdit1.TabIndex = 64
        Me.CheckEdit1.TabStop = False
        Me.CheckEdit1.Tag = "sobre_pedido"
        Me.CheckEdit1.Visible = False
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(123, 72)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(274, 20)
        Me.lkpGrupo.TabIndex = 3
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(75, 75)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 2
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(123, 48)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(274, 20)
        Me.lkpDepartamento.TabIndex = 1
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(30, 51)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 0
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(56, 248)
        Me.txtmodelo.Name = "txtmodelo"
        Me.txtmodelo.Size = New System.Drawing.Size(112, 20)
        Me.txtmodelo.TabIndex = 715
        Me.txtmodelo.TabStop = False
        Me.txtmodelo.Tag = "modelo"
        Me.txtmodelo.Visible = False
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(123, 96)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(750, Long)
        Me.lkpArticulo.Required = True
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(173, 20)
        Me.lkpArticulo.TabIndex = 5
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'lblCantidadSugeridaTexto
        '
        Me.lblCantidadSugeridaTexto.AutoSize = True
        Me.lblCantidadSugeridaTexto.Location = New System.Drawing.Point(240, 144)
        Me.lblCantidadSugeridaTexto.Name = "lblCantidadSugeridaTexto"
        Me.lblCantidadSugeridaTexto.Size = New System.Drawing.Size(111, 16)
        Me.lblCantidadSugeridaTexto.TabIndex = 10
        Me.lblCantidadSugeridaTexto.Text = "Cantidad Sugerida:"
        '
        'lblCantidadSugeridaValor
        '
        Me.lblCantidadSugeridaValor.AutoSize = True
        Me.lblCantidadSugeridaValor.BackColor = System.Drawing.SystemColors.Window
        Me.lblCantidadSugeridaValor.Location = New System.Drawing.Point(360, 144)
        Me.lblCantidadSugeridaValor.Name = "lblCantidadSugeridaValor"
        Me.lblCantidadSugeridaValor.Size = New System.Drawing.Size(11, 16)
        Me.lblCantidadSugeridaValor.TabIndex = 11
        Me.lblCantidadSugeridaValor.Text = "0"
        '
        'clcCantidadSugerida
        '
        Me.clcCantidadSugerida.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCantidadSugerida.Location = New System.Drawing.Point(40, 280)
        Me.clcCantidadSugerida.Name = "clcCantidadSugerida"
        Me.clcCantidadSugerida.Size = New System.Drawing.Size(75, 20)
        Me.clcCantidadSugerida.TabIndex = 718
        Me.clcCantidadSugerida.Tag = "cantidad_sugerida"
        '
        'frmOrdenesCompraDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(410, 208)
        Me.Controls.Add(Me.clcCantidadSugerida)
        Me.Controls.Add(Me.lblCantidadSugeridaValor)
        Me.Controls.Add(Me.lblCantidadSugeridaTexto)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.txtmodelo)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.CheckEdit1)
        Me.Controls.Add(Me.clcExistencia)
        Me.Controls.Add(Me.clcCantidadAnual)
        Me.Controls.Add(Me.clcCantidadMes)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblunidad)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.clcPrecio)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lbldescripcion_corta)
        Me.Controls.Add(Me.clcCantidad_Anterior)
        Me.Name = "frmOrdenesCompraDetalle"
        Me.Controls.SetChildIndex(Me.clcCantidad_Anterior, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion_corta, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.clcPrecio, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblunidad, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcCantidadMes, 0)
        Me.Controls.SetChildIndex(Me.clcCantidadAnual, 0)
        Me.Controls.SetChildIndex(Me.clcExistencia, 0)
        Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.txtmodelo, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblCantidadSugeridaTexto, 0)
        Me.Controls.SetChildIndex(Me.lblCantidadSugeridaValor, 0)
        Me.Controls.SetChildIndex(Me.clcCantidadSugerida, 0)
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad_Anterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidadMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidadAnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcExistencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidadSugerida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oOrdenesCompraDetalle As VillarrealBusiness.clsOrdenesCompraDetalle
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As VillarrealBusiness.clsArticulos
    'Private ReadOnly Property Banco() As Long
    '    Get
    '        If lkpBanco.EditValue = Nothing Then
    '            Return -1
    '        Else
    '            Return lkpBanco.EditValue
    '        End If
    '    End Get
    'End Property

    'Public Property Chequera() As String
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpChequera)
    '    End Get
    '    Set(ByVal Value As String)
    '        Me.lkpChequera.EditValue = Value
    '    End Set
    'End Property
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property

    Private articulo_temporal As Long = 0

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmOrdenesCompraDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
            
        End With
        
        OwnerForm.calculatotal()
    End Sub
    Private Sub frmOrdenesCompraDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        lkpDepartamento_LoadData(True)
        'Me.lkpGrupo_FilterData(-1)
        'Me.lkpGrupo_InitData(-1)
        lkpArticulo_LoadData(True)


        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lblCantidadSugeridaValor.Text = Me.clcCantidadSugerida.EditValue
    End Sub
    Private Sub frmOrdenesCompraDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oOrdenesCompraDetalle = New VillarrealBusiness.clsOrdenesCompraDetalle
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGrupos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpDepartamento.Enabled = False
                Me.lkpGrupo.Enabled = False
                Me.lkpArticulo.Enabled = False
                Me.lblCantidadSugeridaTexto.Visible = False
                Me.lblCantidadSugeridaValor.Visible = False
            Case Actions.Delete
                Me.lblCantidadSugeridaTexto.Visible = False
                Me.lblCantidadSugeridaValor.Visible = False
        End Select
    End Sub
    Private Sub frmOrdenesCompraDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Me.lkpArticulo.Focus()
        If Action = Actions.Delete Then Exit Sub
        Response = oOrdenesCompraDetalle.Validacion(Action, Departamento, Grupo, Articulo, Me.clcCantidad.EditValue)

        If Response.ErrorFound Then Exit Sub

        If Action = Actions.Insert Then Response = oOrdenesCompraDetalle.ValidaExisteArticulo(Articulo, "articulo", CType(OwnerForm, frmOrdenesCompra).tmaOrdenesCompra.DataSource)
    End Sub
    Private Sub frmOrdenesCompraDetalle_Localize() Handles MyBase.Localize

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    'Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
    '    Dim Response As New Events
    '    If ((Me.lkpArticulo.EditValue > 0) And (Action.Update Or Action.Delete)) Then
    '        Response = oDepartamentos.LookupFijo(Me.lkpArticulo.GetValue("departamento"))
    '    Else
    '        Response = oDepartamentos.Lookup
    '    End If

    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
    '    End If

    'End Sub
    'Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
    '    Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    'End Sub
    'Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

    '    Me.lkpGrupo.EditValue = Nothing
    '    Me.lkpGrupo_FilterData(-1)
    '    Me.lkpGrupo_InitData(-1)
    '    Me.lbldescripcion_corta.Text = ""

    'End Sub


    'Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
    '    Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    'End Sub
    'Private Sub lkpGrupo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

    '    If Departamento < 0 Then
    '        Me.lkpDepartamento_LoadData(True)
    '    Else
    '        If ((Action.Update Or Action.Delete) And Me.lkpArticulo.EditValue > 0) Then
    '            Exit Sub
    '        Else
    '            Me.lkpArticulo.EditValue = Nothing
    '            '@ACH-29/06/07: Agregar formato al Lookup articulo para que acepte los datos
    '            Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    '            '/@ACH-29/06/07
    '            Me.lkpArticulo_LoadData(True)
    '            'Me.lkpArticulo_InitData(-1)
    '            Me.lbldescripcion_corta.Text = ""
    '        End If
    '    End If

    'End Sub
    'Private Sub lkpGrupo_FilterData(ByVal Value As Object) Handles lkpGrupo.FilterData
    '    Dim Response As New Events
    '    ''Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
    '    '@ACH-29/06/07: Modificación para que en caso de que Departamento = Nothing, no haga filtro por departamento.
    '    Response = oGrupos.Lookup(Departamento)
    '    '/@ACH-29/06/07
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpGrupo.DataSource = oDataSet.Tables(0)
    '    End If
    'End Sub
    'Private Sub lkpGrupo_InitData(ByVal Value As Object) Handles lkpGrupo.InitData
    '    Dim Response As New Events

    '    Response = oGrupos.Lookup(Departamento)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then
    '            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
    '        End If

    '    End If
    'End Sub
    'Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
    '    Dim Response As New Events

    '    If ((Action.Update Or Action.Delete) And Me.lkpArticulo.EditValue > 0) Then
    '        Response = oGrupos.LookupFijo(Departamento, Grupo)
    '    Else
    '        Response = oGrupos.Lookup(Departamento)
    '    End If
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then
    '            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
    '        End If

    '    End If
    'End Sub

    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData

        Dim Response As New Events

        Response = oDepartamentos.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub

    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData

        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub

    'Private Sub lkpArticulo_Format()
    '    Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    'End Sub
    'Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    '@ACH-29/06/07: Modificación para que en caso de que Departamento = Nothing, no haga filtro por departamento.
    '    'Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue, 0)
    '    Response = oArticulos.Lookup(Departamento, Grupo, 0)
    '    '/@ACH-29/06/07
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '    End If
    'End Sub

    'Private Sub lkpArticulo_FilterData(ByVal Value As Object) Handles lkpArticulo.FilterData
    '    Dim Response As New Events
    '    Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue, 0, Value)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '    End If
    'End Sub
    'Private Sub lkpArticulo_InitData(ByVal Value As Object) Handles lkpArticulo.InitData
    '    Dim Response As New Events

    '    Response = oArticulos.Lookup(Departamento, Grupo, 0, , Value)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then
    '            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '        End If

    '    End If
    'End Sub

    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Not (lkpArticulo.EditValue Is Nothing) Then


            If articulo_temporal <> Articulo Then articulo_temporal = Articulo
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpArticulo.EditValue = articulo_temporal

            lbldescripcion_corta.Text = lkpArticulo.GetValue("descripcion_corta")
            lblunidad.Text = lkpArticulo.GetValue("nombre_unidad")
            clcPrecio.Value = lkpArticulo.GetValue("ultimo_costo_sin_flete")
            Me.txtmodelo.Text = lkpArticulo.GetValue("modelo")
            AsignarValoresVentasArticulo(Me.Articulo)
        End If
        If ((Action.Update Or Action.Delete) And Me.lkpArticulo.EditValue > 0) Then
            Me.lkpDepartamento_LoadData(True)
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo_LoadData(True)
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
        End If
    End Sub

    Private Sub clcCantidad_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcCantidad.EditValueChanged
        If Not IsNumeric(clcCantidad.EditValue) Or Not IsNumeric(clcPrecio.EditValue) Then
            clcImporte.EditValue = 0
            Exit Sub
        End If

        If (clcPrecio.EditValue Is Nothing Or clcPrecio.EditValue <= 0) Or (clcCantidad.EditValue Is Nothing Or clcCantidad.EditValue <= 0) Then
            clcImporte.EditValue = 0
        Else
            clcImporte.Value = clcPrecio.Value * clcCantidad.Value
        End If
    End Sub
    Private Sub clcPrecio_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcPrecio.EditValueChanged

        If Not IsNumeric(clcCantidad.EditValue) Or Not IsNumeric(clcPrecio.EditValue) Then
            clcImporte.EditValue = 0
            Exit Sub
        End If

        If (clcPrecio.EditValue Is Nothing Or clcPrecio.EditValue <= 0) Or (clcCantidad.EditValue Is Nothing Or clcCantidad.EditValue <= 0) Or Not IsNumeric(clcCantidad.EditValue) Then
            clcImporte.EditValue = 0
        Else
            clcImporte.Value = clcPrecio.Value * clcCantidad.Value
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub AsignarValoresVentasArticulo(ByVal articulo As Long)
        Dim response As Events

        response = oOrdenesCompraDetalle.TraerDatosVentaArticulo(articulo)
        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                Me.clcCantidadAnual.EditValue = odataset.Tables(0).Rows(0).Item("CANTIDAD_ANUAL")
                Me.clcCantidadMes.EditValue = odataset.Tables(0).Rows(0).Item("CANTIDAD_MES")
                Me.clcExistencia.EditValue = odataset.Tables(0).Rows(0).Item("EXISTENCIA")
            End If

        End If

    End Sub
#End Region

End Class
