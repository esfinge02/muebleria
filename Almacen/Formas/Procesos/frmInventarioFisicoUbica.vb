Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Structure ArticulosSeriesUbica
    Dim Articulo As Long
    Dim Series As DataSet
End Structure

Public Class frmInventarioFisicoUbica
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblCierre As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblPendiente As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConteo1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConteo2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaArticulos As Dipros.Windows.TINMaster
    Friend WithEvents clcConteo1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcClaveUbicacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUbicacion As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmInventarioFisicoUbica))
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblCierre = New System.Windows.Forms.Label
        Me.lblPendiente = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveUbicacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUbicacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConteo1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcConteo1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcConteo2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label6 = New System.Windows.Forms.Label
        Me.tmaArticulos = New Dipros.Windows.TINMaster
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcConteo1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(6813, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(104, 40)
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(75, 20)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "inventario"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Folio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCierre
        '
        Me.lblCierre.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCierre.Location = New System.Drawing.Point(608, 64)
        Me.lblCierre.Name = "lblCierre"
        Me.lblCierre.Size = New System.Drawing.Size(128, 19)
        Me.lblCierre.TabIndex = 5
        Me.lblCierre.Text = "Fecha de Cierre"
        Me.lblCierre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCierre.Visible = False
        '
        'lblPendiente
        '
        Me.lblPendiente.AutoSize = True
        Me.lblPendiente.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendiente.ForeColor = System.Drawing.Color.Green
        Me.lblPendiente.Location = New System.Drawing.Point(424, 37)
        Me.lblPendiente.Name = "lblPendiente"
        Me.lblPendiente.Size = New System.Drawing.Size(135, 27)
        Me.lblPendiente.TabIndex = 2
        Me.lblPendiente.Text = "PENDIENTE"
        Me.lblPendiente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 6, 8, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(648, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 4
        Me.dteFecha.Tag = "fecha"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(600, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 536)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(640, 56)
        Me.txtObservaciones.TabIndex = 9
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grArticulos
        '
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Location = New System.Drawing.Point(8, 112)
        Me.grArticulos.MainView = Me.grvArticulos
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.clcConteo1})
        Me.grArticulos.Size = New System.Drawing.Size(736, 416)
        Me.grArticulos.TabIndex = 7
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveUbicacion, Me.grcUbicacion, Me.grcClaveArticulo, Me.grcArticulo, Me.grcClaveUnidad, Me.grcConteo1, Me.grcConteo2})
        Me.grvArticulos.GridControl = Me.grArticulos
        Me.grvArticulos.GroupPanelText = "Arrastrar el encabezado de la columna para agrupar"
        Me.grvArticulos.Name = "grvArticulos"
        '
        'grcClaveUbicacion
        '
        Me.grcClaveUbicacion.Caption = "Ubicaci�n"
        Me.grcClaveUbicacion.FieldName = "ubicacion"
        Me.grcClaveUbicacion.Name = "grcClaveUbicacion"
        Me.grcClaveUbicacion.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        '
        'grcUbicacion
        '
        Me.grcUbicacion.Caption = "Ubicaci�n"
        Me.grcUbicacion.FieldName = "n_ubicacion"
        Me.grcUbicacion.Name = "grcUbicacion"
        Me.grcUbicacion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUbicacion.VisibleIndex = 0
        Me.grcUbicacion.Width = 100
        '
        'grcClaveArticulo
        '
        Me.grcClaveArticulo.Caption = "Art�culo"
        Me.grcClaveArticulo.FieldName = "articulo"
        Me.grcClaveArticulo.Name = "grcClaveArticulo"
        Me.grcClaveArticulo.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcClaveArticulo.VisibleIndex = 1
        Me.grcClaveArticulo.Width = 100
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Descripci�n"
        Me.grcArticulo.FieldName = "descripcion_corta"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcArticulo.VisibleIndex = 2
        Me.grcArticulo.Width = 220
        '
        'grcClaveUnidad
        '
        Me.grcClaveUnidad.Caption = "Unidad"
        Me.grcClaveUnidad.FieldName = "unidad"
        Me.grcClaveUnidad.Name = "grcClaveUnidad"
        Me.grcClaveUnidad.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcClaveUnidad.VisibleIndex = 3
        Me.grcClaveUnidad.Width = 100
        '
        'grcConteo1
        '
        Me.grcConteo1.Caption = "Conteo 1"
        Me.grcConteo1.ColumnEdit = Me.clcConteo1
        Me.grcConteo1.FieldName = "conteo1"
        Me.grcConteo1.Name = "grcConteo1"
        Me.grcConteo1.Options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        Me.grcConteo1.VisibleIndex = 4
        Me.grcConteo1.Width = 100
        '
        'clcConteo1
        '
        Me.clcConteo1.AutoHeight = False
        Me.clcConteo1.Name = "clcConteo1"
        '
        'grcConteo2
        '
        Me.grcConteo2.Caption = "Conteo 2"
        Me.grcConteo2.FieldName = "conteo2"
        Me.grcConteo2.Name = "grcConteo2"
        Me.grcConteo2.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcConteo2.VisibleIndex = 5
        Me.grcConteo2.Width = 100
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 536)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Observaciones:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmaArticulos
        '
        Me.tmaArticulos.BackColor = System.Drawing.Color.White
        Me.tmaArticulos.CanDelete = True
        Me.tmaArticulos.CanInsert = True
        Me.tmaArticulos.CanUpdate = True
        Me.tmaArticulos.Grid = Me.grArticulos
        Me.tmaArticulos.Location = New System.Drawing.Point(8, 87)
        Me.tmaArticulos.Name = "tmaArticulos"
        Me.tmaArticulos.Size = New System.Drawing.Size(736, 25)
        Me.tmaArticulos.TabIndex = 6
        Me.tmaArticulos.Title = ""
        Me.tmaArticulos.UpdateTitle = "un Art�culo"
        '
        'frmInventarioFisicoUbica
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(754, 600)
        Me.Controls.Add(Me.tmaArticulos)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.grArticulos)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblPendiente)
        Me.Controls.Add(Me.lblCierre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcFolio)
        Me.Name = "frmInventarioFisicoUbica"
        Me.Text = "frmInventarioFisicoUbica"
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblCierre, 0)
        Me.Controls.SetChildIndex(Me.lblPendiente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.grArticulos, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.tmaArticulos, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcConteo1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Transacciones"
    Private Sub frmInventarioFisicoUbica_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmInventarioFisicoUbica_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmInventarioFisicoUbica_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        If Action = Actions.Insert Or Action = Actions.Update Then
            'preguntar q formato desea imprimir: conteo1 o conteo2
            'llama a ventana de seleccion reporte
            Dim frmModal As New frmInventarioFisicoReporte
            With frmModal
                .Text = "Formato de Conteo"
                .banUbica = True
                .OwnerFormUbica = Me
                .inventario = Me.clcFolio.Value
                .MdiParent = Me.MdiParent
                .Show()
            End With
            Me.Enabled = False
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Dim KS As Keys
    Dim sBodega As String
    Public banAplicado As Boolean = False

#Region "Series"
    Private EstructuraSeries As ArrayList
#End Region

    Private oInventarioFisico As VillarrealBusiness.clsInventarioFisico
    Private oInventarioFisicoDetalle As VillarrealBusiness.clsInventarioFisicoDetalle
    Private oInventarioFisicoDetalleSeries As VillarrealBusiness.clsInventarioFisicoDetalleSeries

#Region "Propiedades"
    Public Property Inventario() As Long
        Get
            Return Me.clcFolio.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcFolio.Value = Value
        End Set
    End Property
    Public Property Bodega() As String
        Get
            Return sBodega
        End Get
        Set(ByVal Value As String)
            sBodega = Value
        End Set
    End Property

#Region "Series"
    Public Property pArticulosSeries(ByVal Articulo As Integer) As ArticulosSeriesUbica
        Get
            Dim AuxArticulos As ArticulosSeriesUbica
            For i As Integer = 0 To EstructuraSeries.Count - 1
                AuxArticulos = EstructuraSeries(i)
                If AuxArticulos.Articulo = Articulo Then
                    Return AuxArticulos
                    Exit For
                End If
            Next
        End Get
        Set(ByVal Value As ArticulosSeriesUbica)
            If Not ExisteArticulo(Value.Articulo) Then
                EstructuraSeries.Add(Value)
            Else
                EstructuraSeries(IndexArticulo(Value.Articulo)) = Value
            End If
        End Set
    End Property
#End Region

#End Region

#End Region

#Region "Eventos de la Forma"
    Private Sub frmInventarioFisicoUbica_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateCurrentRow()

        Select Case Action
            Case Actions.Insert
                Response = oInventarioFisico.Insertar(Inventario, Bodega, Me.dteFecha.DateTime, "P", Me.txtObservaciones.Text, "U")

            Case Actions.Update
                'actualizo el inv fisico, y borro sus art y series para volverlos a ins
                Response = oInventarioFisico.Actualizar(Inventario, Bodega, Me.dteFecha.DateTime, "P", Me.txtObservaciones.Text, "U")

            Case Actions.Delete
                'borro el inv fisico, y sus articulos y sus series en cascada
                Response = oInventarioFisico.Eliminar(Inventario)

        End Select
    End Sub

    Private Sub frmInventarioFisicoUbica_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oInventarioFisico.DespliegaDatos(OwnerForm.Value("inventario"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            If oDataSet.Tables(0).Rows(0).Item("status") = "A" Then banAplicado = True

            'despliega articulos
            Response = oInventarioFisicoDetalle.ListadoxUbicacion(OwnerForm.Value("inventario"))
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.tmaArticulos.DataSource = oDataSet
            End If

            'despliega series
            If Not Response.ErrorFound Then Response = DespliegaSeries()

            'si el inv ya fue aplicado, deshabilitar todo
            If banAplicado Then Me.DeshabilitaControles_InventarioAjustado()
        End If
    End Sub

    Private Sub frmInventarioFisicoUbica_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oInventarioFisico = New VillarrealBusiness.clsInventarioFisico
        oInventarioFisicoDetalle = New VillarrealBusiness.clsInventarioFisicoDetalle
        oInventarioFisicoDetalleSeries = New VillarrealBusiness.clsInventarioFisicoDetalleSeries

        EstructuraSeries = New ArrayList

        Me.Bodega = CType(OwnerForm, brwInventarioFisico).bodega

        Select Case Action
            Case Actions.Insert
                Me.dteFecha.DateTime = CType(TINApp.FechaServidor, DateTime)
            Case Actions.Update
            Case Actions.Delete
        End Select

        ConfiguraMaster()
    End Sub

    Private Sub frmInventarioFisicoUbica_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                ValidaFechaCierre()
            Case Actions.Delete
                ValidaFechaCierre()
        End Select
    End Sub

    Private Sub frmInventarioFisicoUbica_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oInventarioFisico.Validacion(Action, False, Me.Bodega, Me.grvArticulos.RowCount, Me.dteFecha.DateTime, uti_VariablesFechaCierre)
    End Sub

    Private Sub frmInventarioFisicoUbica_Localize() Handles MyBase.Localize
        Find("inventario", Me.clcFolio.Value)
    End Sub

    Private Sub frmInventarioFisicoUbica_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        If Action = Actions.Delete Then Exit Sub

        Me.tmaArticulos.MoveFirst()
        Do While Not Me.tmaArticulos.EOF
            If Me.tmaArticulos.CurrentAction <> Actions.Delete Then
                Response = Me.oInventarioFisicoDetalle.Insertar(Me.clcFolio.EditValue, Me.tmaArticulos.Item("articulo"), Me.tmaArticulos.Item("conteo1"), Me.tmaArticulos.Item("conteo2"), Me.tmaArticulos.Item("contador"), Me.tmaArticulos.Item("supervisor"), Me.tmaArticulos.Item("ubicacion"))
                'Inserto Series
                If Not Response.ErrorFound Then
                    Dim Aux As ArticulosSeriesUbica
                    Aux = pArticulosSeries(Me.tmaArticulos.Item("articulo"))
                    If Not Aux.Series Is Nothing Then
                        For Each oRow As DataRow In Aux.Series.Tables(0).Rows
                            If oRow.RowState <> DataRowState.Deleted Then
                                If oRow.Item("numero_serie") <> "" Then
                                    Response = Me.oInventarioFisicoDetalleSeries.Insertar(Me.clcFolio.EditValue, Me.tmaArticulos.Item("articulo"), oRow.Item("numero_serie"))
                                End If
                            End If
                        Next
                    End If
                End If
            End If
            If Response.ErrorFound Then Exit Sub
            Me.tmaArticulos.MoveNext()
        Loop
    End Sub
#End Region

#Region "Eventos de Controles"
  
#End Region

#Region "Funcionalidad"
    Private Sub ConfiguraMaster()
        With Me.tmaArticulos
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmInventarioFisicoUbicaCaptura
            .AddColumn("ubicacion")
            .AddColumn("n_ubicacion")
            .AddColumn("articulo", "System.Int64")
            .AddColumn("descripcion_corta")
            .AddColumn("unidad")
            .AddColumn("conteo1", "System.Double")
            .AddColumn("conteo2", "System.Double")
            .AddColumn("contador", "System.Int64")
            .AddColumn("supervisor", "System.Int64")
            .AddColumn("maneja_series", "System.Boolean")
        End With
    End Sub

    Private Sub ValidaFechaCierre()
        Dim fecha_cierre As DateTime = uti_VariablesFechaCierre()

        If fecha_cierre = Nothing Then Exit Sub

        If dteFecha.DateTime <= fecha_cierre Then
            lblCierre.Visible = True
            lblCierre.Text = "Fecha Cierre: " & Format(fecha_cierre, "dd/mmm/yyyy")

            DeshabilitaControles()

            'deshabilita boton guardar
            Me.tbrTools.Buttons.Item(0).Enabled = False
        End If
    End Sub

    Private Sub DeshabilitaControles()
        Me.clcFolio.Enabled = False
        Me.dteFecha.Enabled = False
        Me.tmaArticulos.Enabled = False
        Me.grArticulos.Enabled = False
        Me.txtObservaciones.Enabled = False
    End Sub

    Public Function ExisteArticuloEnInventario(ByVal articulo As Long) As Boolean
        ExisteArticuloEnInventario = False

        Dim i As Integer
        For i = 0 To Me.grvArticulos.RowCount - 1
            If articulo = Me.grvArticulos.GetRowCellValue(i, Me.grcClaveArticulo) Then
                ExisteArticuloEnInventario = True
                Exit For
            End If
        Next

    End Function

    Private Sub DeshabilitaControles_InventarioAjustado()
        Me.tbrTools.Buttons.Item(0).Enabled = False         'deshabilita el boton de guardar cambios
        Me.clcFolio.Enabled = False
        Me.dteFecha.Enabled = False
        Me.txtObservaciones.Enabled = False
        'deshabilita la opcion de insertar y borrar articulos
        Me.tmaArticulos.CanInsert = False
        Me.tmaArticulos.CanDelete = False
        'deshabilita la edicion de la columna conteo1
        Me.grvArticulos.Columns("conteo1").Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
    End Sub

#Region "Series"
    Private Function ExisteArticulo(ByVal Articulo As Integer) As Boolean
        Dim AuxArticulosSeries As ArticulosSeriesUbica
        For i As Integer = 0 To EstructuraSeries.Count - 1
            AuxArticulosSeries = EstructuraSeries(i)
            If AuxArticulosSeries.Articulo = Articulo Then
                ExisteArticulo = True
                Exit Function
            End If
        Next
    End Function
    Private Function IndexArticulo(ByVal Articulo As Integer) As Integer
        Dim AuxArticulosSeries As ArticulosSeriesUbica
        For i As Integer = 0 To EstructuraSeries.Count - 1
            AuxArticulosSeries = EstructuraSeries(i)
            If AuxArticulosSeries.Articulo = Articulo Then
                IndexArticulo = i
                Exit Function
            End If
        Next
    End Function
    Private Function DespliegaSeries() As Events
        Dim odataset As DataSet
        Dim response As Events
        Dim AuxSeries As ArticulosSeriesUbica

        Me.tmaArticulos.MoveFirst()
        Do While Not Me.tmaArticulos.EOF
            AuxSeries = New ArticulosSeriesUbica
            response = Me.oInventarioFisicoDetalleSeries.Listado(Me.clcFolio.Value, Me.tmaArticulos.Item("articulo"))
            If Not response.ErrorFound Then
                odataset = response.Value
                AuxSeries.Articulo = Me.tmaArticulos.Item("articulo")
                AuxSeries.Series = odataset
                Me.EstructuraSeries.Add(AuxSeries)
            End If
            Me.tmaArticulos.MoveNext()
        Loop
        Return response
    End Function

#End Region

#End Region

End Class
