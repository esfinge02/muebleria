Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmTraspasosDetalles
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblunidad As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lbldescripcion_corta As System.Windows.Forms.Label
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblFolioHistorial As System.Windows.Forms.Label
    Friend WithEvents grvSeries As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grSeries As DevExpress.XtraGrid.GridControl
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents chkvalidanumeroserie As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkentro_pantalla_validaserie As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTraspasosDetalles))
        Me.lblunidad = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lbldescripcion_corta = New System.Windows.Forms.Label
        Me.chkManejaSeries = New DevExpress.XtraEditors.CheckEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.lblImporte = New System.Windows.Forms.Label
        Me.lblFolioHistorial = New System.Windows.Forms.Label
        Me.grSeries = New DevExpress.XtraGrid.GridControl
        Me.grvSeries = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblSerie = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.chkentro_pantalla_validaserie = New DevExpress.XtraEditors.CheckEdit
        Me.chkvalidanumeroserie = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkentro_pantalla_validaserie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkvalidanumeroserie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(542, 28)
        '
        'lblunidad
        '
        Me.lblunidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblunidad.Location = New System.Drawing.Point(24, 264)
        Me.lblunidad.Name = "lblunidad"
        Me.lblunidad.Size = New System.Drawing.Size(80, 16)
        Me.lblunidad.TabIndex = 65
        Me.lblunidad.Tag = "nombre_unidad"
        Me.lblunidad.Visible = False
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(43, 91)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(55, 16)
        Me.lblArticulo.TabIndex = 4
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Ar&t�culo :"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(36, 138)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(62, 16)
        Me.lblCantidad.TabIndex = 8
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Ca&ntidad :"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 88)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = True
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(108, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 5
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(104, 136)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "D"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "D"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.NullText = "0"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad.TabIndex = 9
        Me.clcCantidad.Tag = "cantidad"
        '
        'lbldescripcion_corta
        '
        Me.lbldescripcion_corta.Location = New System.Drawing.Point(104, 112)
        Me.lbldescripcion_corta.Name = "lbldescripcion_corta"
        Me.lbldescripcion_corta.Size = New System.Drawing.Size(328, 22)
        Me.lbldescripcion_corta.TabIndex = 7
        Me.lbldescripcion_corta.Tag = "descripcion_corta"
        Me.lbldescripcion_corta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.Location = New System.Drawing.Point(24, 280)
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'chkManejaSeries.Properties
        '
        Me.chkManejaSeries.Properties.Caption = "ManejaSeries"
        Me.chkManejaSeries.Size = New System.Drawing.Size(75, 19)
        Me.chkManejaSeries.TabIndex = 67
        Me.chkManejaSeries.TabStop = False
        Me.chkManejaSeries.Tag = "maneja_series"
        Me.chkManejaSeries.Visible = False
        '
        'lblCosto
        '
        Me.lblCosto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCosto.Location = New System.Drawing.Point(24, 248)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(80, 16)
        Me.lblCosto.TabIndex = 64
        Me.lblCosto.Tag = "costo"
        Me.lblCosto.Visible = False
        '
        'lblImporte
        '
        Me.lblImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblImporte.Location = New System.Drawing.Point(24, 232)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(80, 16)
        Me.lblImporte.TabIndex = 64
        Me.lblImporte.Tag = "importe"
        Me.lblImporte.Visible = False
        '
        'lblFolioHistorial
        '
        Me.lblFolioHistorial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFolioHistorial.Location = New System.Drawing.Point(24, 216)
        Me.lblFolioHistorial.Name = "lblFolioHistorial"
        Me.lblFolioHistorial.Size = New System.Drawing.Size(80, 16)
        Me.lblFolioHistorial.TabIndex = 64
        Me.lblFolioHistorial.Tag = "folio_historico_costo"
        Me.lblFolioHistorial.Visible = False
        '
        'grSeries
        '
        '
        'grSeries.EmbeddedNavigator
        '
        Me.grSeries.EmbeddedNavigator.Name = ""
        Me.grSeries.Location = New System.Drawing.Point(160, 161)
        Me.grSeries.MainView = Me.grvSeries
        Me.grSeries.Name = "grSeries"
        Me.grSeries.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grSeries.Size = New System.Drawing.Size(272, 120)
        Me.grSeries.TabIndex = 68
        Me.grSeries.Text = "GridControl1"
        Me.grSeries.Visible = False
        '
        'grvSeries
        '
        Me.grvSeries.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcSerie})
        Me.grvSeries.GridControl = Me.grSeries
        Me.grvSeries.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvSeries.Name = "grvSeries"
        Me.grvSeries.OptionsCustomization.AllowGroup = False
        Me.grvSeries.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvSeries.OptionsView.ShowGroupPanel = False
        Me.grvSeries.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 86
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 1
        Me.grcSerie.Width = 184
        '
        'lblSerie
        '
        Me.lblSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSerie.Location = New System.Drawing.Point(160, 136)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(224, 19)
        Me.lblSerie.TabIndex = 69
        Me.lblSerie.Tag = "numero_serie"
        Me.lblSerie.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "D&escripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 64)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(328, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 3
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(55, 67)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 2
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 40)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(328, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 1
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "Departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(10, 43)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 0
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkentro_pantalla_validaserie
        '
        Me.chkentro_pantalla_validaserie.Location = New System.Drawing.Point(328, 200)
        Me.chkentro_pantalla_validaserie.Name = "chkentro_pantalla_validaserie"
        '
        'chkentro_pantalla_validaserie.Properties
        '
        Me.chkentro_pantalla_validaserie.Properties.Caption = "entro_pantalla_validaserie"
        Me.chkentro_pantalla_validaserie.Properties.Enabled = False
        Me.chkentro_pantalla_validaserie.Size = New System.Drawing.Size(75, 19)
        Me.chkentro_pantalla_validaserie.TabIndex = 70
        Me.chkentro_pantalla_validaserie.TabStop = False
        Me.chkentro_pantalla_validaserie.Tag = "entro_pantalla_validaserie"
        '
        'chkvalidanumeroserie
        '
        Me.chkvalidanumeroserie.Location = New System.Drawing.Point(336, 232)
        Me.chkvalidanumeroserie.Name = "chkvalidanumeroserie"
        '
        'chkvalidanumeroserie.Properties
        '
        Me.chkvalidanumeroserie.Properties.Caption = "validanumeroserie"
        Me.chkvalidanumeroserie.Properties.Enabled = False
        Me.chkvalidanumeroserie.Size = New System.Drawing.Size(75, 19)
        Me.chkvalidanumeroserie.TabIndex = 71
        Me.chkvalidanumeroserie.TabStop = False
        Me.chkvalidanumeroserie.Tag = "validanumeroserie"
        '
        'frmTraspasosDetalles
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(778, 427)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grSeries)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.chkManejaSeries)
        Me.Controls.Add(Me.lblunidad)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.lbldescripcion_corta)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblFolioHistorial)
        Me.Controls.Add(Me.chkvalidanumeroserie)
        Me.Controls.Add(Me.chkentro_pantalla_validaserie)
        Me.Name = "frmTraspasosDetalles"
        Me.Text = "frmTraspasosDetalles"
        Me.Controls.SetChildIndex(Me.chkentro_pantalla_validaserie, 0)
        Me.Controls.SetChildIndex(Me.chkvalidanumeroserie, 0)
        Me.Controls.SetChildIndex(Me.lblFolioHistorial, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion_corta, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblunidad, 0)
        Me.Controls.SetChildIndex(Me.chkManejaSeries, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.grSeries, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkentro_pantalla_validaserie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkvalidanumeroserie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oHistorialCostos As VillarrealBusiness.clsHisCostos
    Private oMovimientosInventariosDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle

    Private modelo As String

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property Maneja_series() As Boolean
        Get
            If Articulo = -1 Then
                Return False
            Else
                Return Me.lkpArticulo.GetValue("maneja_series")
            End If
        End Get
    End Property
    ReadOnly Property AfectaCostos() As Boolean
        Get
            Return OwnerForm.AfectaCostos
        End Get
    End Property
    Private serie_seleccionada As String = ""

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmOrdenesCompraDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept

        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    CType(OwnerForm, frmTraspasosSalidas).lkpBodegaSalida.Enabled = False
                    'CargarCostosArticulos()
                    .item.item("modelo") = Me.modelo
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    'CargarCostosArticulos()
                    .item.item("modelo") = Me.modelo
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmOrdenesCompraDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)
        Me.modelo = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("modelo")), String)

        Me.lblSerie.Text = CType(CType(CType(OwnerForm.MasterControl.SelectedRow, Object), System.Data.DataSet).Tables(0), System.Data.DataTable).Rows(0).Item("numero_serie")


        If Me.Action = Actions.Delete Then
            Me.lkpDepartamento.Enabled = False
            Me.lkpGrupo.Enabled = False
            Me.lkpArticulo.Enabled = False
            Me.clcCantidad.Enabled = False
        End If


        'MarcarGridSerie()
    End Sub

    Private Sub frmTraspasosDetalles_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGrupos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos
        oHistorialCostos = New VillarrealBusiness.clsHisCostos
        oMovimientosInventariosDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle

        lblCosto.Text = "0"
        Me.Size = New Size(448, 192)

        Select Case Action
            Case Actions.Update
                Me.lkpDepartamento.Enabled = False
                Me.lkpGrupo.Enabled = False
                Me.lkpArticulo.Enabled = False
        End Select
    End Sub

    Private Sub frmTraspasosDetalles_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.lkpDepartamento.Select()
        Me.SelectNextControl(Me.lkpDepartamento, True, False, False, False)
        clcCantidad.Value = CType(clcCantidad.Value, Integer)

        If Action <> Actions.Delete Then
            'DAM REVISA MANEJO SERIES
            If Me.Maneja_series And Comunes.clsUtilerias.UsarSeries Then
                'Response = Me.oMovimientosInventariosDetalle.Validacion(Action, Articulo, Me.clcCantidad.Value, Me.lkpArticulo.GetValue("maneja_series"), serie_seleccionada, Comunes.clsUtilerias.uti_hisSeries(Articulo, serie_seleccionada, "S", CType(OwnerForm, frmTraspasosSalidas).BodegaSalida))
                'If Not Response.ErrorFound Then
                Response = oMovimientosInventariosDetalle.ValidaExisteSerie(serie_seleccionada, "numero_serie", CType(Me.OwnerForm, frmTraspasosSalidas).tmaTraspasos.DataSource)
                'End If

                If Not Response.ErrorFound And Me.chkvalidanumeroserie.Checked = False Then
                    Response.Message = "El Articulo no Corresponde con el Numero de Serie"
                End If
            Else
                Dim CADENA As Char = "S"
                Response = Me.oMovimientosInventariosDetalle.Validacion(Articulo, Me.clcCantidad.Value, False, "", CType(lblCosto.Text, Double), CADENA)

            End If
        End If
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo_FilterData(-1)
        Me.lkpGrupo_InitData(-1)
        Me.lbldescripcion_corta.Text = ""

    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpArticulo.EditValue = Nothing
        'Me.lkpArticulo_FilterData(-1)
        'Me.lkpArticulo_InitData(-1)
        lkpArticulo_LoadData(True)
        Me.lbldescripcion_corta.Text = ""

    End Sub
    Private Sub lkpGrupo_FilterData(ByVal Value As Object) Handles lkpGrupo.FilterData
        Dim Response As New Events
        Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpGrupo_InitData(ByVal Value As Object) Handles lkpGrupo.InitData
        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            End If

        End If
    End Sub
    'Private Sub lkpArticulo_InitData(ByVal Value As Object) Handles lkpArticulo.InitData
    '    Dim Response As New Events

    '    Response = oArticulos.Lookup(Departamento, Grupo, 0, , Value)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then
    '            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '        End If

    '    End If
    'End Sub
    'Private Sub lkpArticulo_FilterData(ByVal Value As Object) Handles lkpArticulo.FilterData
    '    Dim Response As New Events
    '    Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue, 0, Value)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '    End If
    'End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        Response = oArticulos.Lookup(Me.Departamento, Me.Grupo, 0)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        Dim valor_anterior_articulo As Long
        valor_anterior_articulo = Me.lkpArticulo.EditValue

        'lbldescripcion_corta.Text = lkpArticulo.GetValue("descripcion_corta")
        'Me.lkpDepartamento.EditValue = lkpArticulo.GetValue("departamento")
        'Me.lkpGrupo.EditValue = lkpArticulo.GetValue("grupo")


        If valor_anterior_articulo <> Articulo Then valor_anterior_articulo = Articulo
        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
        Me.lkpArticulo.EditValue = valor_anterior_articulo
        Me.lbldescripcion_corta.Text = lkpArticulo.GetValue("descripcion_corta")

        lblunidad.Text = lkpArticulo.GetValue("nombre_unidad")


        chkManejaSeries.Checked = lkpArticulo.GetValue("maneja_series")

        modelo = lkpArticulo.GetValue("modelo")


        Select Case AfectaCostos
            Case True
                clcCantidad.Value = 1
                clcCantidad.Enabled = False
                'si maneja series
                'DAM -- REVISA MANEJO SERIES OCT-08
                If chkManejaSeries.Checked And Comunes.clsUtilerias.UsarSeries = True Then
                    CargaSeries(Me.Articulo)
                    grSeries.Visible = True
                    Me.Size = New Size(448, 325) '248
                    Me.lblSerie.Visible = True
                Else
                    grSeries.Visible = False
                    Me.Size = New Size(448, 192)
                    Me.lblSerie.Visible = False
                End If
            Case False
                'si maneja series
                'DAM -- REVISA MANEJO SERIES OCT-08
                If chkManejaSeries.Checked And Comunes.clsUtilerias.UsarSeries = True Then
                    clcCantidad.Value = 1
                    clcCantidad.Enabled = False
                    CargaSeries(Me.Articulo)
                    grSeries.Visible = True
                    Me.Size = New Size(448, 325)
                    Me.lblSerie.Visible = True
                Else
                    clcCantidad.Enabled = True
                    grSeries.Visible = False
                    Me.Size = New Size(448, 192)
                    Me.lblSerie.Visible = False
                End If
        End Select


    End Sub
    Private Sub grvSeries_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvSeries.CellValueChanging
        Dim j As Integer = Me.grvSeries.FocusedRowHandle
        Dim i As Integer = 0
        For i = 0 To Me.grvSeries.RowCount - 1
            If i <> j Then
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
            Else
                serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
                Me.lblSerie.Text = serie_seleccionada
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
            End If
        Next
        grvSeries.UpdateCurrentRow()

        If Me.lblSerie.Text.Substring(0, 3).ToUpper = "MVC" Then
            Me.chkvalidanumeroserie.Checked = False
            ShowMessage(MessageType.MsgInformation, "El N�mero de Serie del inventario inicial no es V�lido para Traspasos", "Traspasos")
            ValidaSerieCorrecta(modelo, lblSerie.Text)
        Else
            Me.chkvalidanumeroserie.Checked = True
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Public Sub CargarCostosArticulos()
        Dim Response As Events
        Dim oDataSet As DataSet

        lblCosto.Text = "0"
        If AfectaCostos Then
            ' Traer costo del historial de costos
            Response = oHistorialCostos.UltimoCostoArticulo(Me.lkpArticulo.GetValue("articulo"), CType(OwnerForm, frmTraspasosSalidas).BodegaSalida)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    lblCosto.Text = oDataSet.Tables(0).Rows(0).Item("costo")
                    lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
                    lblFolioHistorial.Text = oDataSet.Tables(0).Rows(0).Item("folio")
                End If
            End If
        Else
            'Traer ultimo costo de Articulos
            Response = oArticulos.DespliegaDatos(Me.lkpArticulo.GetValue("articulo"))
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    lblCosto.Text = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
                    lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.EditValue
                    lblFolioHistorial.Text = -1
                End If
            End If
        End If

        oDataSet = Nothing
    End Sub
    Private Sub CargaSeries(ByVal articulo As Long)
        Dim response As Events

        'Trae las series de tipo entrada
        '-1 es para traer todas las series no importando la entrada
        response = oArticulos.SeriesArticulos(articulo, "E", CType(OwnerForm, frmTraspasosSalidas).BodegaSalida, -1)
        If response.ErrorFound Then
            response.ShowMessage()
        Else
            Dim odataset As DataSet
            odataset = response.Value
            Me.grSeries.DataSource = odataset.Tables(0)

        End If

    End Sub

    Private Sub ValidaSerieCorrecta(ByVal modelo_serie As String, ByVal numero_serie As String)

        Dim response As Events
        Dim bodega As String
        Dim uti_hisSeries As Boolean

        bodega = CType(OwnerForm, frmTraspasosSalidas).BodegaSalida

        If Me.chkManejaSeries.Checked = True And Me.chkvalidanumeroserie.Checked = False Then
            'uti_hisSeries = Comunes.clsUtilerias.uti_hisSeries(Articulo, numero_serie, "S", bodega)
            'response = oMovimientosInventariosDetalle.Validacion(Action, Articulo, Me.clcCantidad.Value, Me.chkManejaSeries.Checked, numero_serie, uti_hisSeries)

            'If response.ErrorFound Then
            'response.ShowMessage()
            Dim OFORM As New Comunes.frmRepartosValidaNumeroSerie
            OFORM.Articulo = Articulo
            OFORM.modelo = modelo
            OFORM.Fila = -1
            OFORM.OwnerForm = Me
            OFORM.MdiParent = Me.MdiParent
            Me.Enabled = False
            OFORM.Show()
            'Else
            '    'MARCO COMO VALIDO SI EL NUMERO DE SERIE PASO LA REVISION EN LA TABLA
            '    Me.chkvalidanumeroserie.Checked = True
            'End If

        End If


    End Sub
    Public Sub LLenarNumeroSerieValido(ByVal fila As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal validanumeroserie As Boolean)

        Me.chkvalidanumeroserie.Checked = validanumeroserie
        If validanumeroserie Then
            Me.lblSerie.Text = numero_serie
            Me.serie_seleccionada = Me.lblSerie.Text
            Me.chkentro_pantalla_validaserie.Checked = True
        End If

    End Sub

    'Private Sub MarcarGridSerie()
    '    Dim valor_buscar As String
    '    Dim i As Long = 0
    '    valor_buscar = Me.lblSerie.Text
    '    For i = 0 To Me.grvSeries.RowCount - 1
    '        If valor_buscar <> Me.grvSeries.GetRowCellValue(i, Me.grcSerie) Then
    '            Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
    '        Else
    '            'serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
    '            'Me.lblSerie.Text = serie_seleccionada
    '            Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
    '        End If
    '    Next
    '    grvSeries.UpdateCurrentRow()
    'End Sub
#End Region

   
End Class
