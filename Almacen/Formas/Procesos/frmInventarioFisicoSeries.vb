Public Class frmInventarioFisicoSeries
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNumero_serie As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmInventarioFisicoSeries))
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNumero_serie = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtNumero_serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(229, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 16)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Numero de Serie:"
        '
        'txtNumero_serie
        '
        Me.txtNumero_serie.EditValue = ""
        Me.txtNumero_serie.Location = New System.Drawing.Point(128, 40)
        Me.txtNumero_serie.Name = "txtNumero_serie"
        '
        'txtNumero_serie.Properties
        '
        Me.txtNumero_serie.Properties.MaxLength = 30
        Me.txtNumero_serie.Size = New System.Drawing.Size(152, 20)
        Me.txtNumero_serie.TabIndex = 60
        Me.txtNumero_serie.Tag = "numero_serie"
        '
        'frmInventarioFisicoSeries
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(306, 72)
        Me.Controls.Add(Me.txtNumero_serie)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmInventarioFisicoSeries"
        Me.Text = "frmInventarioFisicoSeries"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtNumero_serie, 0)
        CType(Me.txtNumero_serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub frmInventarioFisicoSeries_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmInventarioFisicoSeries_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

    End Sub

    Private Sub frmInventarioFisicoSeries_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

    End Sub
End Class
