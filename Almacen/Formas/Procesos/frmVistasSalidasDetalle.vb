Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmVistasSalidasDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Folio_Vista_Salida As Long
    Dim Facturada As Boolean
    Dim Sucursal_Factura As Long
    Dim Serie_Factura As String
    Dim Folio_Factura As Long
    Dim Concepto_Inventario_Salida As String
    Dim Folio_Invetario_Salida As Long
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNumero_Serie As System.Windows.Forms.Label
    Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Public WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Public WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Public WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbldescripcion As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents grSeries As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvSeries As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDescripcionBodega As System.Windows.Forms.Label
    Friend WithEvents lblModelo As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents clccantidad As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblFolioHistorial As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSeries As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVistasSalidasDetalle))
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblNumero_Serie = New System.Windows.Forms.Label
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.lbldescripcion = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.grSeries = New DevExpress.XtraGrid.GridControl
        Me.grvSeries = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkManejaSeries = New DevExpress.XtraEditors.CheckEdit
        Me.lblDescripcionBodega = New System.Windows.Forms.Label
        Me.lblModelo = New System.Windows.Forms.Label
        Me.clccantidad = New DevExpress.XtraEditors.CalcEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.lblCosto = New System.Windows.Forms.Label
        Me.lblFolioHistorial = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSeries = New DevExpress.XtraEditors.MemoEdit
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clccantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(643, 28)
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(56, 40)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 0
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(112, 40)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(64, 19)
        Me.clcPartida.TabIndex = 1
        Me.clcPartida.Tag = "partida"
        '
        'lblNumero_Serie
        '
        Me.lblNumero_Serie.AutoSize = True
        Me.lblNumero_Serie.Location = New System.Drawing.Point(24, 256)
        Me.lblNumero_Serie.Name = "lblNumero_Serie"
        Me.lblNumero_Serie.Size = New System.Drawing.Size(85, 16)
        Me.lblNumero_Serie.TabIndex = 12
        Me.lblNumero_Serie.Tag = ""
        Me.lblNumero_Serie.Text = "&Numero Serie:"
        Me.lblNumero_Serie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(56, 170)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega_Salida.TabIndex = 10
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "&Bodega:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(112, 171)
        Me.lkpBodegaSalida.MultiSelect = False
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = ""
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = False
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(323, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 11
        Me.lkpBodegaSalida.Tag = "Bodega"
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Enabled = False
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(112, 87)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(323, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(64, 88)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 4
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Enabled = False
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(112, 63)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(323, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(16, 64)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 2
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(112, 111)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(323, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 7
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Tag = ""
        Me.Label4.Text = "De&scripci�n:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbldescripcion
        '
        Me.lbldescripcion.Location = New System.Drawing.Point(112, 135)
        Me.lbldescripcion.Name = "lbldescripcion"
        Me.lbldescripcion.Size = New System.Drawing.Size(312, 32)
        Me.lbldescripcion.TabIndex = 9
        Me.lbldescripcion.Tag = "descripcion"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(56, 112)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Art�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grSeries
        '
        '
        'grSeries.EmbeddedNavigator
        '
        Me.grSeries.EmbeddedNavigator.Name = ""
        Me.grSeries.Location = New System.Drawing.Point(112, 248)
        Me.grSeries.MainView = Me.grvSeries
        Me.grSeries.Name = "grSeries"
        Me.grSeries.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grSeries.Size = New System.Drawing.Size(320, 128)
        Me.grSeries.TabIndex = 15
        Me.grSeries.TabStop = False
        Me.grSeries.Text = "GridControl1"
        Me.grSeries.Visible = False
        '
        'grvSeries
        '
        Me.grvSeries.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcSerie})
        Me.grvSeries.GridControl = Me.grSeries
        Me.grvSeries.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvSeries.Name = "grvSeries"
        Me.grvSeries.OptionsCustomization.AllowGroup = False
        Me.grvSeries.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvSeries.OptionsView.ShowGroupPanel = False
        Me.grvSeries.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 86
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.SortIndex = 0
        Me.grcSerie.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcSerie.VisibleIndex = 1
        Me.grcSerie.Width = 184
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.Location = New System.Drawing.Point(0, 328)
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'chkManejaSeries.Properties
        '
        Me.chkManejaSeries.Properties.Caption = "ManejaSeries"
        Me.chkManejaSeries.Size = New System.Drawing.Size(96, 19)
        Me.chkManejaSeries.TabIndex = 16
        Me.chkManejaSeries.TabStop = False
        Me.chkManejaSeries.Tag = "maneja_series"
        Me.chkManejaSeries.Visible = False
        '
        'lblDescripcionBodega
        '
        Me.lblDescripcionBodega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDescripcionBodega.Location = New System.Drawing.Point(288, 248)
        Me.lblDescripcionBodega.Name = "lblDescripcionBodega"
        Me.lblDescripcionBodega.Size = New System.Drawing.Size(140, 20)
        Me.lblDescripcionBodega.TabIndex = 14
        Me.lblDescripcionBodega.Tag = "descripcion_bodega"
        Me.lblDescripcionBodega.Visible = False
        '
        'lblModelo
        '
        Me.lblModelo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblModelo.Location = New System.Drawing.Point(24, 264)
        Me.lblModelo.Name = "lblModelo"
        Me.lblModelo.Size = New System.Drawing.Size(72, 20)
        Me.lblModelo.TabIndex = 59
        Me.lblModelo.Tag = "modelo"
        Me.lblModelo.Visible = False
        '
        'clccantidad
        '
        Me.clccantidad.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.clccantidad.Location = New System.Drawing.Point(112, 196)
        Me.clccantidad.Name = "clccantidad"
        Me.clccantidad.Size = New System.Drawing.Size(48, 20)
        Me.clccantidad.TabIndex = 13
        Me.clccantidad.Tag = "cantidad"
        '
        'lblImporte
        '
        Me.lblImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblImporte.Location = New System.Drawing.Point(24, 240)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(80, 16)
        Me.lblImporte.TabIndex = 72
        Me.lblImporte.Tag = "importe"
        Me.lblImporte.Visible = False
        '
        'lblCosto
        '
        Me.lblCosto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCosto.Location = New System.Drawing.Point(24, 216)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 71
        Me.lblCosto.Tag = "costo"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCosto.Visible = False
        '
        'lblFolioHistorial
        '
        Me.lblFolioHistorial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFolioHistorial.Location = New System.Drawing.Point(16, 311)
        Me.lblFolioHistorial.Name = "lblFolioHistorial"
        Me.lblFolioHistorial.Size = New System.Drawing.Size(80, 16)
        Me.lblFolioHistorial.TabIndex = 73
        Me.lblFolioHistorial.Tag = "folio_historico_costo"
        Me.lblFolioHistorial.Text = "-1"
        Me.lblFolioHistorial.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(48, 196)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Tag = ""
        Me.Label2.Text = "Cantidad:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSeries
        '
        Me.txtSeries.EditValue = ""
        Me.txtSeries.Location = New System.Drawing.Point(216, 196)
        Me.txtSeries.Name = "txtSeries"
        Me.txtSeries.Size = New System.Drawing.Size(216, 46)
        Me.txtSeries.TabIndex = 74
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(168, 196)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 16)
        Me.Label3.TabIndex = 75
        Me.Label3.Tag = ""
        Me.Label3.Text = "Series:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmVistasSalidasDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 252)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSeries)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grSeries)
        Me.Controls.Add(Me.lblFolioHistorial)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.clccantidad)
        Me.Controls.Add(Me.chkManejaSeries)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbldescripcion)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblNumero_Serie)
        Me.Controls.Add(Me.lblBodega_Salida)
        Me.Controls.Add(Me.lkpBodegaSalida)
        Me.Controls.Add(Me.lblDescripcionBodega)
        Me.Controls.Add(Me.lblModelo)
        Me.Name = "frmVistasSalidasDetalle"
        Me.Controls.SetChildIndex(Me.lblModelo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionBodega, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaSalida, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblNumero_Serie, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.chkManejaSeries, 0)
        Me.Controls.SetChildIndex(Me.clccantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblFolioHistorial, 0)
        Me.Controls.SetChildIndex(Me.grSeries, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtSeries, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clccantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oVistasSalidasDetalle As New VillarrealBusiness.clsVistasSalidasDetalle

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get

    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get

    End Property
    
    Private serie_seleccionada As String = ""
    Private CostoSinFlete As Double = 0
    Private Series() As String
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmVistasSalidasDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    CargarCostosArticulos()
                    .AddRow(Me.DataSource)
                    'If Me.Series.Length > 0 Then
                    '    CType(OwnerForm, frmVistasSalidas).Series = Me.Series
                    'End If
                    ActualizaSeries()
                Case Actions.Update
                    CargarCostosArticulos()
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmVistasSalidasDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmVistasSalidasDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmVistasSalidasDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        'DAM REVISA MANEJO SERIES 
        Dim CantidadSeries As Long

        Series = Me.txtSeries.Text.Split(",")
        If Series(0) = "" Then
            CantidadSeries = 0
        Else
            CantidadSeries = Series.Length
        End If
        Response = oVistasSalidasDetalle.Validacion(Action, Articulo, Me.clccantidad.EditValue, "", Bodega, Me.chkManejaSeries.Checked, Comunes.clsUtilerias.UsarSeries, CantidadSeries)
    End Sub

    Private Sub frmVistasSalidasDetalle_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        If (Me.lkpArticulo.EditValue > 0 And (Action.Update Or Actions.Delete)) Then
            '    Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Response = oDepartamentos.LookupFijo(Me.lkpArticulo.GetValue("departamento"))
        Else
            Response = oDepartamentos.LookupFijo(Departamento)
        End If

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub

    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events

        If (Me.lkpArticulo.EditValue > 0 And (Action.Update Or Actions.Delete)) Then
            Response = oGrupos.Lookup(Me.lkpArticulo.GetValue("departamento"))
        Else
            Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
        End If

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If
    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events

        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        'Me.lblSerie.Text = ""
        Me.txtSeries.Text = ""
        Me.lblModelo.Text = ""

        If Me.lkpArticulo.DataSource Is Nothing Or Me.lkpArticulo.EditValue Is Nothing Or Not IsNumeric(Me.lkpArticulo.EditValue) Then Exit Sub

        Me.lbldescripcion.Text = lkpArticulo.GetValue("descripcion")
        If ((Action.Update Or Actions.Delete) And Me.lkpArticulo.EditValue > 0) Then
            'Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpDepartamento_LoadData(True)
            'Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("descripcion")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpGrupo_LoadData(True)
        End If


        Me.chkManejaSeries.Checked = Me.lkpArticulo.GetValue("maneja_series")
        Me.lblModelo.Text = Me.lkpArticulo.GetValue("modelo")
        Me.CostoSinFlete = lkpArticulo.GetValue("costo")

        CargaSeries(Me.Articulo)

        If Action <> Actions.Insert Then Exit Sub

        'Dim response As Events
        'DespliegaPrecios(Articulo, response)
        'If response.ErrorFound Then
        '    response.ShowMessage()
        'End If

        If Me.lkpArticulo.EditValue > 0 Then
            If Me.lkpDepartamento.EditValue <= 0 Or Me.lkpGrupo.EditValue <= 0 Then
                Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
                Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
                'grSeries.Visible = True
            End If
        End If
    End Sub
    Private Sub lkpArticulo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.Validated

        If Me.lkpArticulo.Text = "" Then
            Me.lkpArticulo.Focus()
        End If
    End Sub

    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged
        'Me.lblSerie.Text = ""
        Me.txtSeries.Text = ""
        If Bodega.Trim.Length > 0 Then
            Me.lblDescripcionBodega.Text = Me.Bodega.ToString + "-" + Me.lkpBodegaSalida.DisplayText
            CargaSeries(Me.Articulo)
        End If
    End Sub

    Private Sub grvSeries_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvSeries.CellValueChanging
        Dim j As Integer = Me.grvSeries.FocusedRowHandle
        Dim i As Integer = 0
        For i = 0 To Me.grvSeries.RowCount - 1
            If i <> j Then
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
            Else
                serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
                'Me.lblSerie.Text = serie_seleccionada
                Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
            End If
        Next
        grvSeries.UpdateCurrentRow()
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Public Sub CargarCostosArticulos()
        '    Dim Response As Events
        '    Dim oDataSet As DataSet
        'Dim costo_normal As Double


        lblCosto.Text = CostoSinFlete '- (CostoSinFlete * DescuentoPorProntoPagoDeEntrada())
        lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value


        '    If AfectaCostos Then
        '        ' Traer costo del historial de costos
        '        Response = oHistorialCostos.UltimoCostoArticulo(Me.lkpArticulo.GetValue("articulo"))
        '        If Not Response.ErrorFound Then
        '            oDataSet = Response.Value
        '            If oDataSet.Tables(0).Rows.Count > 0 Then
        '                costo_normal = oDataSet.Tables(0).Rows(0).Item("costo")

        '                lblCosto.Text = costo_normal - (costo_normal * DescuentoPorProntoPagoDeEntrada())

        '                lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
        '                lblFolioHistorial.Text = oDataSet.Tables(0).Rows(0).Item("folio")
        '            End If
        '        End If
        '    Else
        '        'Traer ultimo costo de Articulos
        '        Response = oArticulos.DespliegaDatos(Me.lkpArticulo.GetValue("articulo"))
        '        If Not Response.ErrorFound Then
        '            oDataSet = Response.Value
        '            If oDataSet.Tables(0).Rows.Count > 0 Then
        '                costo_normal = oDataSet.Tables(0).Rows(0).Item("costo")

        '                lblCosto.Text = costo_normal - (costo_normal * DescuentoPorProntoPagoDeEntrada())
        '                'lblCosto.Text = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
        '                lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
        '                lblFolioHistorial.Text = folio_historico_costo_entrada
        '            End If
        '        End If
        '    End If

        '    oDataSet = Nothing
    End Sub
    Private Sub CargaSeries(ByVal articulo As Long)

        'DAM REVISA MANEJO SERIES
        If Me.chkManejaSeries.Checked = False Or Comunes.clsUtilerias.UsarSeries = False Then Exit Sub
        Dim response As Events
        'Trae las series de tipo entrada
        '-1 es para traer todas las series no importando la entrada
        response = oArticulos.SeriesArticulos(articulo, "E", Bodega, -1)
        If response.ErrorFound Then
            response.ShowMessage()
        Else
            Dim odataset As DataSet
            odataset = response.Value
            Me.grSeries.DataSource = odataset.Tables(0)

        End If
        response = Nothing
    End Sub
#End Region

    Private Sub ActualizaSeries()
        Dim Aux As New ArticuloSeriesCapturadas

        Select Case Action
            Case Actions.Insert
                Aux.Indice = MaximaPartida()
                Aux.Series = Me.Series
                CType(OwnerForm, frmVistasSalidas).ArticuloSeries(Aux.Indice) = Aux
            Case Actions.Update
                Aux.Indice = Me.clcPartida.Value
                Aux.Series = Me.Series
                CType(OwnerForm, frmVistasSalidas).ArticuloSeries(Aux.Indice) = Aux
            Case Actions.Delete
        End Select
    End Sub

    Private Function MaximaPartida() As Long
        Dim maximo As Long = 0
        Dim rows() As DataRow
        Dim row As DataRow

        rows = CType(OwnerForm, frmVistasSalidas).tmaVistasSalidas.DataSource.Tables(0).Select("control <> 3")

        For Each row In rows
            If row.Item("partida") > maximo Then
                maximo = row.Item("partida")
            End If
        Next
        Return maximo

    End Function

End Class
