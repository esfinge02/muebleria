Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmTraspasosEntradas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Private folio_movimiento As Long = 0
    Private ConceptoEntrada As String = ""
    Private traspasos_imprimir As String = "-1"
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega_Entrada As System.Windows.Forms.Label
    Friend WithEvents grTraspasos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvTraspasos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRecibida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRecibida As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio_Historico As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTraspaso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodegaDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTraspasosEntradas))
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaEntrada = New Dipros.Editors.TINMultiLookup
        Me.lblBodega_Entrada = New System.Windows.Forms.Label
        Me.grTraspasos = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUnidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio_Historico = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvTraspasos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcRecibida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRecibida = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcTraspaso = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodegaDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.grTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRecibida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3529, 28)
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(47, 64)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(91, 16)
        Me.lblBodega_Salida.TabIndex = 2
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "Bodega &Origen:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(144, 64)
        Me.lkpBodegaSalida.MultiSelect = True
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = "(Todos)"
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.Required = True
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = True
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaSalida.TabIndex = 3
        Me.lkpBodegaSalida.Tag = "bodega_salida"
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lkpBodegaEntrada
        '
        Me.lkpBodegaEntrada.AllowAdd = False
        Me.lkpBodegaEntrada.AutoReaload = False
        Me.lkpBodegaEntrada.DataSource = Nothing
        Me.lkpBodegaEntrada.DefaultSearchField = ""
        Me.lkpBodegaEntrada.DisplayMember = "Descripcion"
        Me.lkpBodegaEntrada.EditValue = Nothing
        Me.lkpBodegaEntrada.Filtered = False
        Me.lkpBodegaEntrada.InitValue = Nothing
        Me.lkpBodegaEntrada.Location = New System.Drawing.Point(144, 40)
        Me.lkpBodegaEntrada.MultiSelect = False
        Me.lkpBodegaEntrada.Name = "lkpBodegaEntrada"
        Me.lkpBodegaEntrada.NullText = ""
        Me.lkpBodegaEntrada.PopupWidth = CType(400, Long)
        Me.lkpBodegaEntrada.Required = True
        Me.lkpBodegaEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaEntrada.SearchMember = ""
        Me.lkpBodegaEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaEntrada.SelectAll = False
        Me.lkpBodegaEntrada.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaEntrada.TabIndex = 1
        Me.lkpBodegaEntrada.Tag = "bodega_entrada"
        Me.lkpBodegaEntrada.ToolTip = Nothing
        Me.lkpBodegaEntrada.ValueMember = "Bodega"
        '
        'lblBodega_Entrada
        '
        Me.lblBodega_Entrada.AutoSize = True
        Me.lblBodega_Entrada.Location = New System.Drawing.Point(42, 40)
        Me.lblBodega_Entrada.Name = "lblBodega_Entrada"
        Me.lblBodega_Entrada.Size = New System.Drawing.Size(96, 16)
        Me.lblBodega_Entrada.TabIndex = 0
        Me.lblBodega_Entrada.Tag = ""
        Me.lblBodega_Entrada.Text = "Bodega &Destino:"
        Me.lblBodega_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grTraspasos
        '
        '
        'grTraspasos.EmbeddedNavigator
        '
        Me.grTraspasos.EmbeddedNavigator.Name = ""
        Me.grTraspasos.LevelDefaults.Add("Detalle", Me.grvArticulos)
        Me.grTraspasos.Location = New System.Drawing.Point(12, 96)
        Me.grTraspasos.MainView = Me.grvTraspasos
        Me.grTraspasos.Name = "grTraspasos"
        Me.grTraspasos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRecibida})
        Me.grTraspasos.Size = New System.Drawing.Size(596, 288)
        Me.grTraspasos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grTraspasos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grTraspasos.TabIndex = 4
        Me.grTraspasos.Text = "Traspasos"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcCantidad, Me.grcUnidad, Me.grcSerie, Me.grcCosto, Me.grcImporte, Me.grcPartida, Me.grcFolio_Historico})
        Me.grvArticulos.GridControl = Me.grTraspasos
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsBehavior.Editable = False
        Me.grvArticulos.OptionsCustomization.AllowFilter = False
        Me.grvArticulos.OptionsCustomization.AllowGroup = False
        Me.grvArticulos.OptionsCustomization.AllowSort = False
        Me.grvArticulos.OptionsDetail.ShowDetailTabs = True
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        Me.grvArticulos.OptionsView.ShowIndicator = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Articulo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 0
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripcion"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 2
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.DisplayFormat.FormatString = "0"
        Me.grcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        '
        'grcUnidad
        '
        Me.grcUnidad.Caption = "Unidad"
        Me.grcUnidad.FieldName = "nombre_unidad"
        Me.grcUnidad.Name = "grcUnidad"
        Me.grcUnidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUnidad.VisibleIndex = 4
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "numero_serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 5
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto.VisibleIndex = 6
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 7
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFolio_Historico
        '
        Me.grcFolio_Historico.Caption = "FolioHistorico"
        Me.grcFolio_Historico.FieldName = "folio_historico"
        Me.grcFolio_Historico.Name = "grcFolio_Historico"
        Me.grcFolio_Historico.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grvTraspasos
        '
        Me.grvTraspasos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcRecibida, Me.grcTraspaso, Me.grcFecha, Me.grcObservaciones, Me.grcBodega, Me.grcBodegaDescripcion})
        Me.grvTraspasos.GridControl = Me.grTraspasos
        Me.grvTraspasos.Name = "grvTraspasos"
        Me.grvTraspasos.OptionsCustomization.AllowFilter = False
        Me.grvTraspasos.OptionsCustomization.AllowGroup = False
        Me.grvTraspasos.OptionsCustomization.AllowSort = False
        Me.grvTraspasos.OptionsDetail.ShowDetailTabs = True
        Me.grvTraspasos.OptionsDetail.SmartDetailHeight = True
        Me.grvTraspasos.OptionsView.ShowGroupPanel = False
        Me.grvTraspasos.OptionsView.ShowIndicator = False
        '
        'grcRecibida
        '
        Me.grcRecibida.Caption = "Recibida"
        Me.grcRecibida.ColumnEdit = Me.chkRecibida
        Me.grcRecibida.FieldName = "recibida"
        Me.grcRecibida.Name = "grcRecibida"
        Me.grcRecibida.VisibleIndex = 0
        Me.grcRecibida.Width = 57
        '
        'chkRecibida
        '
        Me.chkRecibida.AutoHeight = False
        Me.chkRecibida.Name = "chkRecibida"
        '
        'grcTraspaso
        '
        Me.grcTraspaso.Caption = "Folio"
        Me.grcTraspaso.FieldName = "traspaso"
        Me.grcTraspaso.Name = "grcTraspaso"
        Me.grcTraspaso.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTraspaso.VisibleIndex = 1
        Me.grcTraspaso.Width = 51
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 2
        Me.grcFecha.Width = 79
        '
        'grcObservaciones
        '
        Me.grcObservaciones.Caption = "Observaciones"
        Me.grcObservaciones.FieldName = "observaciones_entrada"
        Me.grcObservaciones.Name = "grcObservaciones"
        Me.grcObservaciones.VisibleIndex = 3
        Me.grcObservaciones.Width = 316
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega Origen"
        Me.grcBodega.FieldName = "bodega_salida"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Width = 91
        '
        'grcBodegaDescripcion
        '
        Me.grcBodegaDescripcion.Caption = "Bodega Origen"
        Me.grcBodegaDescripcion.Name = "grcBodegaDescripcion"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 6
        Me.ToolBarButton1.Text = "Reimprimir"
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        '
        'frmTraspasosEntradas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(618, 392)
        Me.Controls.Add(Me.grTraspasos)
        Me.Controls.Add(Me.lkpBodegaEntrada)
        Me.Controls.Add(Me.lblBodega_Entrada)
        Me.Controls.Add(Me.lkpBodegaSalida)
        Me.Controls.Add(Me.lblBodega_Salida)
        Me.Name = "frmTraspasosEntradas"
        Me.Text = "frmTraspasosEntradas"
        Me.Controls.SetChildIndex(Me.lblBodega_Salida, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaSalida, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaEntrada, 0)
        Me.Controls.SetChildIndex(Me.grTraspasos, 0)
        CType(Me.grTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRecibida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oTraspasos As VillarrealBusiness.clsTraspasos
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario

    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oHistorialCostos As VillarrealBusiness.clsHisCostos

    Private oVariables As VillarrealBusiness.clsVariables


    Private ReadOnly Property SucursalSalida() As Long
        Get
            Return lkpBodegaSalida.GetValue("sucursal")
        End Get
    End Property

    Private ReadOnly Property BodegaSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get
    End Property

    Private ReadOnly Property BodegaEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaEntrada)
        End Get
    End Property

    Private ReadOnly Property ObservacionesEntrada(ByVal traspaso As Long) As String
        Get
            If grvTraspasos.GetRowCellValue(traspaso, Me.grcObservaciones) = Nothing Then
                Return ""
            Else
                Return grvTraspasos.GetRowCellValue(traspaso, Me.grcObservaciones)
            End If
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmTraspasosEntradas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmTraspasosEntradas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmTraspasosEntradas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Me.ImprimeTraspasoEntrada(traspasos_imprimir)
    End Sub

    Private Sub frmTraspasosEntradas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If ShowMessage(MessageType.MsgQuestion, "�Est� seguro de dar entrada a los traspasos seleccionados?", "Mensaje del Sistema") = Answer.MsgYes Then

            Dim i As Long = 0
            Dim j As Long = 0
            Dim orow As DataRow
            Dim oRowHijo As DataRow
            Dim arrRows() As DataRow

            Me.grvTraspasos.CloseEditor()
            Me.grvTraspasos.UpdateCurrentRow()

            'Se recorre todas las filas del grid y si el check de recibido esta encendido se genera la entrada
            For Each orow In CType(Me.grTraspasos.DataSource, DataTable).Rows
                If orow("recibida") = True Then

                    'Se genera el movimiento de inventario de la entrada
                    GeneraMovimientoInventario(Response, folio_movimiento, ConceptoEntrada, orow("concepto_salida"), orow("folio_salida"))

                    arrRows = orow.GetChildRows("TRASPASOS")
                    For Each oRowHijo In arrRows

                        Response = oHistorialCostos.DespliegaDatos(oRowHijo("folio_historico"))
                        Dim ODATASET As DataSet
                        Dim fecha_historico As DateTime
                        Dim costo_historico As Double
                        Dim folio_nuevo_historico As Double

                        ODATASET = Response.Value
                        fecha_historico = ODATASET.Tables(0).Rows(0).Item("fecha")
                        costo_historico = ODATASET.Tables(0).Rows(0).Item("costo")

                        If Not Response.ErrorFound Then Response = oHistorialCostos.Insertar(folio_nuevo_historico, fecha_historico, oRowHijo("articulo"), BodegaEntrada, oRowHijo("cantidad"), costo_historico, oRowHijo("cantidad"))


                        'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                        If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, BodegaEntrada, ConceptoEntrada, folio_movimiento, CDate(TINApp.FechaServidor), _
                                                                               oRowHijo("partida"), oRowHijo("articulo"), oRowHijo("cantidad"), oRowHijo("costo"), oRowHijo("importe"), folio_nuevo_historico, 0, System.DBNull.Value)

                        'DAM -- REVISA MANEJO SERIES OCT-08
                        If oRowHijo("maneja_series") And Comunes.clsUtilerias.UsarSeries Then
                            If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, BodegaEntrada, ConceptoEntrada, folio_movimiento, oRowHijo("partida"), oRowHijo("numero_serie"), oRowHijo("articulo"))
                        End If

                        If Not Response.ErrorFound Then Response = oTraspasos.TraspasosSalidas_ActualizaExistencias_Del(BodegaEntrada, oRowHijo("articulo"), oRowHijo("cantidad"))
                    Next

                    If Not Response.ErrorFound Then Response = oTraspasos.ActualizarEntrada(orow("traspaso"), ConceptoEntrada, folio_movimiento, orow("observaciones_entrada"))

                    traspasos_imprimir = traspasos_imprimir + ", " + CStr(CLng(orow("traspaso")))

                End If
            Next

        Else

            Response.Message = "Datos no Guardados"

        End If

    End Sub

    Private Sub frmTraspasosEntradas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oTraspasos = New VillarrealBusiness.clsTraspasos
        oSucursales = New VillarrealBusiness.clsSucursales
        oBodegas = New VillarrealBusiness.clsBodegas
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario

        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries

        oArticulos = New VillarrealBusiness.clsArticulos
        oHistorialCostos = New VillarrealBusiness.clsHisCostos
        oVariables = New VillarrealBusiness.clsVariables

        Me.lkpBodegaEntrada.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)
    End Sub

    Private Sub frmTraspasosEntradas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = New Events

        If Me.grvTraspasos.RowCount = 0 Then
            Response.Message = "No hay traspasos en la lista"
            Response.Value = Nothing
        End If


        If Not Response.ErrorFound Then
            Response = oTraspasos.ValidaEntradaTraspaso(CType(Me.grvTraspasos.DataSource, DataView).Table)
        End If






    End Sub

    Private Sub frmTraspasosEntradas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

   
    'Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
    '   If e.Button.Text = "Reimprimir" Then
    '        ImprimeTraspasoEntrada(Me.folio_movimiento) 'nuevo
    '    End If
    'End Sub 'nuevo es el bot�n de reimprimir, est� desactivado por que no existe 
    'un grid que tenga las entradas de traspasos 

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.BodegasDestino(Me.lkpBodegaEntrada.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            Me.lkpBodegaSalida.SelectAll = True
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged
        ActualizaGrid()
    End Sub

    Private Sub lkpBodegaEntrada_Format() Handles lkpBodegaEntrada.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaEntrada)
    End Sub
    Private Sub lkpBodegaEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaEntrada.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaEntrada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaEntrada.EditValueChanged
        Me.lkpBodegaSalida_LoadData(True)
        ConceptoEntrada = CargaConceptoEntrada()
        ActualizaGrid()
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Public Sub ActualizaGrid()
        Dim Response As Dipros.Utils.Events

        Response = oTraspasos.ListadoTraspasosDetalles(Me.lkpBodegaSalida.ToXML, BodegaEntrada)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            oDataSet.Relations.Add( _
             "TRASPASOS", _
             New DataColumn() {oDataSet.Tables(0).Columns("sucursal_salida"), _
                               oDataSet.Tables(0).Columns("bodega_salida"), _
                               oDataSet.Tables(0).Columns("concepto_salida"), _
                               oDataSet.Tables(0).Columns("folio_salida")}, _
             New DataColumn() {oDataSet.Tables(1).Columns("sucursal"), _
                               oDataSet.Tables(1).Columns("bodega"), _
                               oDataSet.Tables(1).Columns("concepto"), _
                               oDataSet.Tables(1).Columns("folio")}, False)

            grTraspasos.LevelDefaults.Add("TRASPASOS", grvArticulos)
            grvTraspasos.OptionsDetail.EnableMasterViewMode = True
            grvTraspasos.OptionsView.ShowChildrenInGroupPanel = True
            grTraspasos.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Function GeneraMovimientoInventario(ByRef response As Events, ByRef folio As Long, ByVal concepto As String, ByVal concepto_referencia As String, ByVal folio_referencia As Long)
        response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, BodegaEntrada, concepto, folio, _
                    CDate(TINApp.FechaServidor), Comunes.Common.Sucursal_Actual, concepto_referencia, folio_referencia, "Movimiento generado desde entradas de traspasos")

    End Function
    Private Function CargaConceptoEntrada() As String
        Dim Response As Events

        Response = oBodegas.BodegasSucursalesSel(BodegaEntrada)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Return oDataSet.Tables(0).Rows(0).Item("concepto_entrada_traspaso")
            End If
            oDataSet = Nothing
        End If

        Return ""
    End Function

    Private Sub ImprimeTraspasoEntrada(ByVal traspasos As String)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeDeEntradaDeTraspasos(traspasos)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El vale de entrada de traspaso no puede mostrarse")
            Else
                Dim oDataSet As DataSet
                Dim oReport As New rptTraspasosEntradas

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Entrada de Traspaso", oReport, , , , True)
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

#End Region

End Class
