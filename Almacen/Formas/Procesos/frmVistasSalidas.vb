Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Structure ArticuloSeriesCapturadas
    Dim Indice As Integer
    Dim Series() As String
End Structure

Public Class frmVistasSalidas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Vista_Salida As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Vista_Salida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha_Devolucion As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Devolucion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPersona_Recibe As System.Windows.Forms.Label
    Friend WithEvents txtPersona_Recibe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPlacas As System.Windows.Forms.Label
    Friend WithEvents txtPlacas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblBodeguero As System.Windows.Forms.Label
    Friend WithEvents lkpBodeguero As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUsuario_Autorizo As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grVistasSalidas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVistasSalidas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaVistasSalidas As Dipros.Windows.TINMaster
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpUsuarioAutorizo As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkVistaEntregada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbrImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVistasSalidas))
        Me.lblFolio_Vista_Salida = New System.Windows.Forms.Label
        Me.clcFolio_Vista_Salida = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha_Devolucion = New System.Windows.Forms.Label
        Me.dteFecha_Devolucion = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblPersona_Recibe = New System.Windows.Forms.Label
        Me.txtPersona_Recibe = New DevExpress.XtraEditors.TextEdit
        Me.lblPlacas = New System.Windows.Forms.Label
        Me.txtPlacas = New DevExpress.XtraEditors.TextEdit
        Me.lblBodeguero = New System.Windows.Forms.Label
        Me.lkpBodeguero = New Dipros.Editors.TINMultiLookup
        Me.lblUsuario_Autorizo = New System.Windows.Forms.Label
        Me.lkpUsuarioAutorizo = New Dipros.Editors.TINMultiLookup
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grVistasSalidas = New DevExpress.XtraGrid.GridControl
        Me.grvVistasSalidas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaVistasSalidas = New Dipros.Windows.TINMaster
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.chkVistaEntregada = New DevExpress.XtraEditors.CheckEdit
        Me.tbrImprimir = New System.Windows.Forms.ToolBarButton
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.clcFolio_Vista_Salida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Devolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersona_Recibe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlacas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grVistasSalidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVistasSalidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkVistaEntregada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrImprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3141, 28)
        '
        'lblFolio_Vista_Salida
        '
        Me.lblFolio_Vista_Salida.AutoSize = True
        Me.lblFolio_Vista_Salida.Location = New System.Drawing.Point(91, 40)
        Me.lblFolio_Vista_Salida.Name = "lblFolio_Vista_Salida"
        Me.lblFolio_Vista_Salida.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Vista_Salida.TabIndex = 0
        Me.lblFolio_Vista_Salida.Tag = ""
        Me.lblFolio_Vista_Salida.Text = "&Folio:"
        Me.lblFolio_Vista_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Vista_Salida
        '
        Me.clcFolio_Vista_Salida.EditValue = "0"
        Me.clcFolio_Vista_Salida.Location = New System.Drawing.Point(134, 40)
        Me.clcFolio_Vista_Salida.MaxValue = 0
        Me.clcFolio_Vista_Salida.MinValue = 0
        Me.clcFolio_Vista_Salida.Name = "clcFolio_Vista_Salida"
        '
        'clcFolio_Vista_Salida.Properties
        '
        Me.clcFolio_Vista_Salida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Vista_Salida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Vista_Salida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Vista_Salida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Vista_Salida.Properties.Enabled = False
        Me.clcFolio_Vista_Salida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Vista_Salida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Vista_Salida.Size = New System.Drawing.Size(74, 19)
        Me.clcFolio_Vista_Salida.TabIndex = 1
        Me.clcFolio_Vista_Salida.Tag = "folio_vista_salida"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(70, 63)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(512, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "20/12/2007"
        Me.dteFecha.Location = New System.Drawing.Point(560, 39)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblFecha_Devolucion
        '
        Me.lblFecha_Devolucion.AutoSize = True
        Me.lblFecha_Devolucion.Location = New System.Drawing.Point(448, 63)
        Me.lblFecha_Devolucion.Name = "lblFecha_Devolucion"
        Me.lblFecha_Devolucion.Size = New System.Drawing.Size(106, 16)
        Me.lblFecha_Devolucion.TabIndex = 6
        Me.lblFecha_Devolucion.Tag = ""
        Me.lblFecha_Devolucion.Text = "&Fecha Devoluci�n:"
        Me.lblFecha_Devolucion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Devolucion
        '
        Me.dteFecha_Devolucion.EditValue = "20/12/2007"
        Me.dteFecha_Devolucion.Location = New System.Drawing.Point(560, 63)
        Me.dteFecha_Devolucion.Name = "dteFecha_Devolucion"
        '
        'dteFecha_Devolucion.Properties
        '
        Me.dteFecha_Devolucion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Devolucion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Devolucion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Devolucion.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Devolucion.TabIndex = 7
        Me.dteFecha_Devolucion.Tag = "fecha_devolucion"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(76, 88)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 8
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(134, 87)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(287, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 9
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblPersona_Recibe
        '
        Me.lblPersona_Recibe.AutoSize = True
        Me.lblPersona_Recibe.Location = New System.Drawing.Point(34, 112)
        Me.lblPersona_Recibe.Name = "lblPersona_Recibe"
        Me.lblPersona_Recibe.Size = New System.Drawing.Size(93, 16)
        Me.lblPersona_Recibe.TabIndex = 10
        Me.lblPersona_Recibe.Tag = ""
        Me.lblPersona_Recibe.Text = "&Persona Recibe:"
        Me.lblPersona_Recibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersona_Recibe
        '
        Me.txtPersona_Recibe.EditValue = ""
        Me.txtPersona_Recibe.Location = New System.Drawing.Point(134, 111)
        Me.txtPersona_Recibe.Name = "txtPersona_Recibe"
        '
        'txtPersona_Recibe.Properties
        '
        Me.txtPersona_Recibe.Properties.MaxLength = 50
        Me.txtPersona_Recibe.Size = New System.Drawing.Size(287, 20)
        Me.txtPersona_Recibe.TabIndex = 11
        Me.txtPersona_Recibe.Tag = "persona_recibe"
        '
        'lblPlacas
        '
        Me.lblPlacas.AutoSize = True
        Me.lblPlacas.Location = New System.Drawing.Point(82, 136)
        Me.lblPlacas.Name = "lblPlacas"
        Me.lblPlacas.Size = New System.Drawing.Size(43, 16)
        Me.lblPlacas.TabIndex = 12
        Me.lblPlacas.Tag = ""
        Me.lblPlacas.Text = "&Placas:"
        Me.lblPlacas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPlacas
        '
        Me.txtPlacas.EditValue = ""
        Me.txtPlacas.Location = New System.Drawing.Point(134, 135)
        Me.txtPlacas.Name = "txtPlacas"
        '
        'txtPlacas.Properties
        '
        Me.txtPlacas.Properties.MaxLength = 10
        Me.txtPlacas.Size = New System.Drawing.Size(74, 20)
        Me.txtPlacas.TabIndex = 13
        Me.txtPlacas.Tag = "placas"
        '
        'lblBodeguero
        '
        Me.lblBodeguero.AutoSize = True
        Me.lblBodeguero.Location = New System.Drawing.Point(64, 160)
        Me.lblBodeguero.Name = "lblBodeguero"
        Me.lblBodeguero.Size = New System.Drawing.Size(68, 16)
        Me.lblBodeguero.TabIndex = 14
        Me.lblBodeguero.Tag = ""
        Me.lblBodeguero.Text = "&Bodeguero:"
        Me.lblBodeguero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodeguero
        '
        Me.lkpBodeguero.AllowAdd = False
        Me.lkpBodeguero.AutoReaload = False
        Me.lkpBodeguero.DataSource = Nothing
        Me.lkpBodeguero.DefaultSearchField = ""
        Me.lkpBodeguero.DisplayMember = "nombre"
        Me.lkpBodeguero.EditValue = Nothing
        Me.lkpBodeguero.Filtered = False
        Me.lkpBodeguero.InitValue = Nothing
        Me.lkpBodeguero.Location = New System.Drawing.Point(134, 159)
        Me.lkpBodeguero.MultiSelect = False
        Me.lkpBodeguero.Name = "lkpBodeguero"
        Me.lkpBodeguero.NullText = ""
        Me.lkpBodeguero.PopupWidth = CType(400, Long)
        Me.lkpBodeguero.ReadOnlyControl = False
        Me.lkpBodeguero.Required = False
        Me.lkpBodeguero.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodeguero.SearchMember = ""
        Me.lkpBodeguero.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodeguero.SelectAll = False
        Me.lkpBodeguero.Size = New System.Drawing.Size(287, 20)
        Me.lkpBodeguero.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodeguero.TabIndex = 15
        Me.lkpBodeguero.Tag = "Bodeguero"
        Me.lkpBodeguero.ToolTip = Nothing
        Me.lkpBodeguero.ValueMember = "Bodeguero"
        '
        'lblUsuario_Autorizo
        '
        Me.lblUsuario_Autorizo.AutoSize = True
        Me.lblUsuario_Autorizo.Location = New System.Drawing.Point(77, 184)
        Me.lblUsuario_Autorizo.Name = "lblUsuario_Autorizo"
        Me.lblUsuario_Autorizo.Size = New System.Drawing.Size(55, 16)
        Me.lblUsuario_Autorizo.TabIndex = 16
        Me.lblUsuario_Autorizo.Tag = ""
        Me.lblUsuario_Autorizo.Text = "Autorizo:"
        Me.lblUsuario_Autorizo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpUsuarioAutorizo
        '
        Me.lkpUsuarioAutorizo.AllowAdd = False
        Me.lkpUsuarioAutorizo.AutoReaload = False
        Me.lkpUsuarioAutorizo.DataSource = Nothing
        Me.lkpUsuarioAutorizo.DefaultSearchField = ""
        Me.lkpUsuarioAutorizo.DisplayMember = "nombre"
        Me.lkpUsuarioAutorizo.EditValue = Nothing
        Me.lkpUsuarioAutorizo.Filtered = False
        Me.lkpUsuarioAutorizo.InitValue = Nothing
        Me.lkpUsuarioAutorizo.Location = New System.Drawing.Point(134, 183)
        Me.lkpUsuarioAutorizo.MultiSelect = False
        Me.lkpUsuarioAutorizo.Name = "lkpUsuarioAutorizo"
        Me.lkpUsuarioAutorizo.NullText = ""
        Me.lkpUsuarioAutorizo.PopupWidth = CType(400, Long)
        Me.lkpUsuarioAutorizo.ReadOnlyControl = False
        Me.lkpUsuarioAutorizo.Required = False
        Me.lkpUsuarioAutorizo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUsuarioAutorizo.SearchMember = ""
        Me.lkpUsuarioAutorizo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUsuarioAutorizo.SelectAll = False
        Me.lkpUsuarioAutorizo.Size = New System.Drawing.Size(287, 20)
        Me.lkpUsuarioAutorizo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpUsuarioAutorizo.TabIndex = 17
        Me.lkpUsuarioAutorizo.Tag = "Usuario_Autorizo"
        Me.lkpUsuarioAutorizo.ToolTip = Nothing
        Me.lkpUsuarioAutorizo.ValueMember = "usuario"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(40, 208)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 18
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(134, 207)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(514, 38)
        Me.txtObservaciones.TabIndex = 19
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grVistasSalidas
        '
        '
        'grVistasSalidas.EmbeddedNavigator
        '
        Me.grVistasSalidas.EmbeddedNavigator.Name = ""
        Me.grVistasSalidas.Location = New System.Drawing.Point(17, 280)
        Me.grVistasSalidas.MainView = Me.grvVistasSalidas
        Me.grVistasSalidas.Name = "grVistasSalidas"
        Me.grVistasSalidas.Size = New System.Drawing.Size(632, 240)
        Me.grVistasSalidas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grVistasSalidas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasSalidas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasSalidas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasSalidas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasSalidas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasSalidas.TabIndex = 21
        Me.grVistasSalidas.Text = "VistasSalidas"
        '
        'grvVistasSalidas
        '
        Me.grvVistasSalidas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcCantidad, Me.grcNumSerie, Me.grcBodega, Me.grcNombreBodega})
        Me.grvVistasSalidas.GridControl = Me.grVistasSalidas
        Me.grvVistasSalidas.Name = "grvVistasSalidas"
        Me.grvVistasSalidas.OptionsCustomization.AllowFilter = False
        Me.grvVistasSalidas.OptionsCustomization.AllowGroup = False
        Me.grvVistasSalidas.OptionsCustomization.AllowSort = False
        Me.grvVistasSalidas.OptionsView.ShowGroupPanel = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Articulo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 0
        Me.grcArticulo.Width = 66
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 136
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 2
        Me.grcDescripcion.Width = 184
        '
        'grcNumSerie
        '
        Me.grcNumSerie.Caption = "Num. Serie"
        Me.grcNumSerie.FieldName = "numero_serie"
        Me.grcNumSerie.Name = "grcNumSerie"
        Me.grcNumSerie.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumSerie.VisibleIndex = 4
        Me.grcNumSerie.Width = 111
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Clave Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreBodega
        '
        Me.grcNombreBodega.Caption = "Bodega"
        Me.grcNombreBodega.FieldName = "descripcion_bodega"
        Me.grcNombreBodega.Name = "grcNombreBodega"
        Me.grcNombreBodega.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreBodega.VisibleIndex = 5
        Me.grcNombreBodega.Width = 121
        '
        'tmaVistasSalidas
        '
        Me.tmaVistasSalidas.BackColor = System.Drawing.Color.White
        Me.tmaVistasSalidas.CanDelete = True
        Me.tmaVistasSalidas.CanInsert = True
        Me.tmaVistasSalidas.CanUpdate = False
        Me.tmaVistasSalidas.Grid = Me.grVistasSalidas
        Me.tmaVistasSalidas.Location = New System.Drawing.Point(17, 256)
        Me.tmaVistasSalidas.Name = "tmaVistasSalidas"
        Me.tmaVistasSalidas.Size = New System.Drawing.Size(632, 23)
        Me.tmaVistasSalidas.TabIndex = 20
        Me.tmaVistasSalidas.Title = ""
        Me.tmaVistasSalidas.UpdateTitle = "un Registro"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(134, 63)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(287, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'chkVistaEntregada
        '
        Me.chkVistaEntregada.Location = New System.Drawing.Point(536, 184)
        Me.chkVistaEntregada.Name = "chkVistaEntregada"
        '
        'chkVistaEntregada.Properties
        '
        Me.chkVistaEntregada.Properties.Caption = "Vista Entregada"
        Me.chkVistaEntregada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkVistaEntregada.Size = New System.Drawing.Size(112, 19)
        Me.chkVistaEntregada.TabIndex = 59
        Me.chkVistaEntregada.Tag = "vista_entregada"
        '
        'tbrImprimir
        '
        Me.tbrImprimir.ImageIndex = 6
        Me.tbrImprimir.Text = "Imprimir"
        Me.tbrImprimir.ToolTipText = "Impresi�n de la Salida por Vistas"
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        '
        'frmVistasSalidas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(666, 528)
        Me.Controls.Add(Me.chkVistaEntregada)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblFolio_Vista_Salida)
        Me.Controls.Add(Me.clcFolio_Vista_Salida)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha_Devolucion)
        Me.Controls.Add(Me.dteFecha_Devolucion)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblPersona_Recibe)
        Me.Controls.Add(Me.txtPersona_Recibe)
        Me.Controls.Add(Me.lblPlacas)
        Me.Controls.Add(Me.txtPlacas)
        Me.Controls.Add(Me.lblBodeguero)
        Me.Controls.Add(Me.lkpBodeguero)
        Me.Controls.Add(Me.lblUsuario_Autorizo)
        Me.Controls.Add(Me.lkpUsuarioAutorizo)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.grVistasSalidas)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.tmaVistasSalidas)
        Me.Name = "frmVistasSalidas"
        Me.Controls.SetChildIndex(Me.tmaVistasSalidas, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.grVistasSalidas, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lkpUsuarioAutorizo, 0)
        Me.Controls.SetChildIndex(Me.lblUsuario_Autorizo, 0)
        Me.Controls.SetChildIndex(Me.lkpBodeguero, 0)
        Me.Controls.SetChildIndex(Me.lblBodeguero, 0)
        Me.Controls.SetChildIndex(Me.txtPlacas, 0)
        Me.Controls.SetChildIndex(Me.lblPlacas, 0)
        Me.Controls.SetChildIndex(Me.txtPersona_Recibe, 0)
        Me.Controls.SetChildIndex(Me.lblPersona_Recibe, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Devolucion, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Devolucion, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Vista_Salida, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Vista_Salida, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkVistaEntregada, 0)
        CType(Me.clcFolio_Vista_Salida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Devolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersona_Recibe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlacas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grVistasSalidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVistasSalidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkVistaEntregada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oVistasSalidas As VillarrealBusiness.clsVistasSalidas
    Private oVistasSalidasDetalle As VillarrealBusiness.clsVistasSalidasDetalle
    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private oVariables As VillarrealBusiness.clsVariables
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oCliente As VillarrealBusiness.clsClientes
    Private oBodegueros As VillarrealBusiness.clsBodegueros

    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario

    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oReportes As VillarrealBusiness.Reportes
    'Private oHistorialCostos As VillarrealBusiness.clsHisCostos

    Private folio_movimiento As Long = 0
    Private sSeries() As String
    'Private Guardado As Boolean = False
    Private EstructuraSeries As ArrayList

    'Public Property Series() As String()
    '    Get
    '        Return sSeries
    '    End Get
    '    Set(ByVal Value As String())
    '        sSeries = Value
    '    End Set
    'End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property Bodeguero() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBodeguero)
        End Get
    End Property
    Private ReadOnly Property UsuarioAutorizo() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpUsuarioAutorizo)
        End Get
    End Property

    Private ReadOnly Property ConceptoVistasSalidas() As String
        Get
            Return oVariables.TraeDatos("concepto_vistas_salida", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Public Property ArticuloSeries(ByVal Indice As Integer) As ArticuloSeriesCapturadas
        Get
            Dim AuxArticulos As ArticuloSeriesCapturadas
            For i As Integer = 0 To EstructuraSeries.Count - 1
                AuxArticulos = EstructuraSeries(i)
                'revisa si existe
                If AuxArticulos.Indice = Indice Then
                    Return AuxArticulos
                    Exit For
                End If
            Next
        End Get
        Set(ByVal Value As ArticuloSeriesCapturadas)
            If Not ExisteArticulo(Value.Indice) Then
                EstructuraSeries.Add(Value)
            Else
                EstructuraSeries(IndexArticulo(Value.Indice)) = Value
            End If
        End Set
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmVistasSalidas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmVistasSalidas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Select Case Action
            Case Actions.Insert
                ImprimirReporte()
        End Select

    End Sub
    Private Sub frmVistasSalidas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
        'Guardado = False
    End Sub
    Private Sub frmVistasSalidas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oVistasSalidas.Insertar(Me.DataSource)


            Case Actions.Update
                Response = oVistasSalidas.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oVistasSalidas.Eliminar(clcFolio_Vista_Salida.Value)

        End Select
    End Sub
    Private Sub frmVistasSalidas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MENUOPTION.NAME, "FECHA_VIS_SAL") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If

    End Sub
    Private Sub frmVistasSalidas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oVistasSalidas.DespliegaDatos(OwnerForm.Value("folio_vista_salida"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            Response = Me.oVistasSalidasDetalle.Listado(Me.clcFolio_Vista_Salida.Value)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.tmaVistasSalidas.DataSource = oDataSet
            End If

            If Me.chkVistaEntregada.Checked = True Then
                Me.tbrTools.Buttons(0).Enabled = Not Me.chkVistaEntregada.Checked
                Me.chkVistaEntregada.Enabled = Not Me.chkVistaEntregada.Checked
            End If
        End If

    End Sub
    Private Sub frmVistasSalidas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVistasSalidas = New VillarrealBusiness.clsVistasSalidas
        oVistasSalidasDetalle = New VillarrealBusiness.clsVistasSalidasDetalle
        oUsuarios = New VillarrealBusiness.clsUsuarios
        oVariables = New VillarrealBusiness.clsVariables
        oSucursales = New VillarrealBusiness.clsSucursales
        oCliente = New VillarrealBusiness.clsClientes
        oBodegueros = New VillarrealBusiness.clsBodegueros

        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries
        oArticulos = New VillarrealBusiness.clsArticulos
        oReportes = New VillarrealBusiness.Reportes

        EstructuraSeries = New ArrayList


        Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
        Me.dteFecha_Devolucion.DateTime = Me.dteFecha.DateTime.AddDays(5)
        Select Case Action
            Case Actions.Insert
                Me.tbrImprimir.Enabled = False
                Me.tbrImprimir.Visible = False
                Me.chkVistaEntregada.Checked = False
            Case Actions.Update
                Me.tbrImprimir.Enabled = True
                Me.tbrImprimir.Visible = True
                Me.chkVistaEntregada.Checked = True
            Case Actions.Delete
                Me.tbrImprimir.Enabled = False
                Me.tbrImprimir.Visible = False
        End Select


        With Me.tmaVistasSalidas
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmVistasSalidasDetalle

            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("modelo")
            .AddColumn("descripcion")
            .AddColumn("numero_serie")
            .AddColumn("bodega")
            .AddColumn("descripcion_bodega")
            .AddColumn("maneja_series", "System.Boolean")
            .AddColumn("cantidad")
            .AddColumn("costo", "System.Double")
            .AddColumn("importe", "System.Double")
            .AddColumn("folio_historico_costo")

        End With
    End Sub
    Private Sub frmVistasSalidas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVistasSalidas.Validacion(Action, Sucursal, Cliente, Me.txtPersona_Recibe.Text, Me.txtPlacas.Text, Bodeguero, UsuarioAutorizo)
        If Not Response.ErrorFound Then
            If Me.chkVistaEntregada.Checked Then
                If ShowMessage(MessageType.MsgQuestion, "Estas Seguro de Entregar la Mercancia a Vista", "Salida por Vistas") <> Answer.MsgYes Then
                    Response.Message = "Mercancia no Entregada"
                End If
            End If
        End If
    End Sub
    Private Sub frmVistasSalidas_Localize() Handles MyBase.Localize
        Find("folio_vista_salida", Me.clcFolio_Vista_Salida.Value)

    End Sub
    Private Sub frmVistasSalidas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        Dim lpartida As Long = 0

        With Me.tmaVistasSalidas
            .MoveFirst()
            Do While Not .EOF
                lpartida = lpartida + 1
                Select Case .CurrentAction
                    Case Actions.Insert, Actions.Update, Actions.None



                        'DAM 27/dic/07 Verifico si Marco la Salida Mercancia
                        If Me.chkVistaEntregada.Checked Then

                            'DAM 27/dic/07 Primero Valido Existencias y genero los Movimientos al Inventario
                            Response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.SelectedRow, .Item("bodega"), ConceptoVistasSalidas, folio_movimiento, "I", "Salidas Vistas")

                            If Not Response.ErrorFound Then
                                '    'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                                GeneraMovimientoInventario(.CurrentAction, Response, Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistasSalidas, folio_movimiento, Me.dteFecha.DateTime, -1, "", Me.clcFolio_Vista_Salida.Value, "Movimiento generado desde Salida a Vistas")


                                Response = oMovimientosInventarioDetalle.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistasSalidas, folio_movimiento, dteFecha.EditValue, System.DBNull.Value)


                                'DAM REVISA MANEJO SERIES 
                                If Response.ErrorFound = False And .Item("maneja_series") = True And Comunes.clsUtilerias.UsarSeries Then
                                    'AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES
                                    If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoVistasSalidas, folio_movimiento, .Item("Partida"), .Item("articulo"))

                                    ' SE MANDA LA SERIE A LA BODEGA DE ENTRADA
                                    If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), .Item("bodega"))
                                End If
                            End If
                        End If

                        'DAM (27 / dic / 7)
                        If .CurrentAction = Actions.Insert Then
                            Dim i As Long
                            For i = 1 To .Item("cantidad")
                                If Not Response.ErrorFound Then


                                    Dim Aux As ArticuloSeriesCapturadas
                                    Dim iRow As Integer
                                    Aux = ArticuloSeries(tmaVistasSalidas.Item("partida"))

                                    If i <= Aux.Series.Length Then
                                        Response = Me.oVistasSalidasDetalle.Insertar(.SelectedRow, Me.clcFolio_Vista_Salida.Value, False, -1, "", -1, Me.ConceptoVistasSalidas, folio_movimiento, tmaVistasSalidas.Item("partida"), Aux.Series(i - 1))
                                    Else
                                        Response = Me.oVistasSalidasDetalle.Insertar(.SelectedRow, Me.clcFolio_Vista_Salida.Value, False, -1, "", -1, Me.ConceptoVistasSalidas, folio_movimiento, tmaVistasSalidas.Item("partida"), "")
                                    End If


                                End If
                            Next
                        End If
                        If .CurrentAction = Actions.Update Then
                            If Not Response.ErrorFound Then Response = Me.oVistasSalidasDetalle.Actualizar(.SelectedRow, Me.clcFolio_Vista_Salida.Value, False, -1, "", -1, Me.ConceptoVistasSalidas, folio_movimiento)
                        End If


                    Case Actions.Delete

                        If Not Me.chkVistaEntregada.Checked Then
                            Response = Me.oVistasSalidasDetalle.Eliminar(Me.clcFolio_Vista_Salida.Value, .Item("partida"))
                        End If


                End Select
                If Response.ErrorFound Then Exit Sub
                .MoveNext()
            Loop
        End With

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oCliente.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub lkpBodeguero_Format() Handles lkpBodeguero.Format
        Comunes.clsFormato.for_bodegueros_grl(Me.lkpBodeguero)
    End Sub
    Private Sub lkpBodeguero_LoadData(ByVal Initialize As Boolean) Handles lkpBodeguero.LoadData
        Dim response As Events
        response = oBodegueros.Lookup(Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodeguero.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpUsuarioAutorizo_LoadData(ByVal Initialize As Boolean) Handles lkpUsuarioAutorizo.LoadData
        Dim Response As New Events
        Response = oUsuarios.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpUsuarioAutorizo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpUsuarioAutorizo_Format() Handles lkpUsuarioAutorizo.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpUsuarioAutorizo)
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbrImprimir Then
            ImprimirReporte()
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String)
        'If Guardado = False Then
        Select Case accion
            Case Actions.Insert, Actions.Delete, Actions.None
                response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                'Guardado = True
            Case Actions.Update
                response = oMovimientosInventario.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                'Guardado = True
        End Select

        ' End If
    End Function
    Private Function ImprimirReporte()
        Dim response As New Events
        response = oReportes.SalidasVistas(Me.clcFolio_Vista_Salida.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Salida por Vista no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptSalidaVistas

                oDataSet = response.Value

                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                TINApp.ShowReport(Me.MdiParent, "Salida por Vistas", oReport)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Function
    Private Function ExisteArticulo(ByVal Indice As Integer) As Boolean
        Dim AuxArticuloSeriesCapturadas As ArticuloSeriesCapturadas
        For i As Integer = 0 To EstructuraSeries.Count - 1
            AuxArticuloSeriesCapturadas = EstructuraSeries(i)
            'revisa si existe el articulo
            If AuxArticuloSeriesCapturadas.Indice = Indice Then
                ExisteArticulo = True
                Exit Function
            End If
        Next
    End Function
    Private Function IndexArticulo(ByVal Indice As Integer) As Integer
        Dim AuxArticuloSeriesCapturadas As ArticuloSeriesCapturadas
        For i As Integer = 0 To EstructuraSeries.Count - 1
            AuxArticuloSeriesCapturadas = EstructuraSeries(i)
            'revisa si existe la pregunta
            If AuxArticuloSeriesCapturadas.Indice = Indice Then
                IndexArticulo = i
                Exit Function
            End If
        Next
    End Function
#End Region


End Class
