Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmCambioMercanciaCancelada
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grArticuloEntrada As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvArticuloEntrada As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lkpConceptoSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConcepto_Salida As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblObservaciones_Salida As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones_Salida As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpConceptoEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents tmaArticulosEntrada As Dipros.Windows.TINMaster
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSucursal_Factura As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
    Friend WithEvents lblSerie_Factura As System.Windows.Forms.Label
    Friend WithEvents txtSerie_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFechaFactura As System.Windows.Forms.Label
    Friend WithEvents lkpFactura As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFechaFactura As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtSucursal_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambioMercanciaCancelada))
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.grArticuloEntrada = New DevExpress.XtraGrid.GridControl
        Me.grvArticuloEntrada = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaArticulosEntrada = New Dipros.Windows.TINMaster
        Me.lkpConceptoSalida = New Dipros.Editors.TINMultiLookup
        Me.lblConcepto_Salida = New System.Windows.Forms.Label
        Me.lkpConceptoEntrada = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblObservaciones_Salida = New System.Windows.Forms.Label
        Me.txtObservaciones_Salida = New DevExpress.XtraEditors.MemoEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblSucursal_Factura = New System.Windows.Forms.Label
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.lblSerie_Factura = New System.Windows.Forms.Label
        Me.txtSerie_Factura = New DevExpress.XtraEditors.TextEdit
        Me.lblFechaFactura = New System.Windows.Forms.Label
        Me.lkpFactura = New Dipros.Editors.TINMultiLookup
        Me.dteFechaFactura = New DevExpress.XtraEditors.DateEdit
        Me.txtSucursal_Factura = New DevExpress.XtraEditors.TextEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grArticuloEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticuloEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones_Salida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursal_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2440, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 22, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(613, 36)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.ShowClear = False
        Me.dteFecha.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 60
        Me.dteFecha.Tag = "fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(566, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 59
        Me.Label2.Tag = ""
        Me.Label2.Text = "F&echa:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grArticuloEntrada
        '
        '
        'grArticuloEntrada.EmbeddedNavigator
        '
        Me.grArticuloEntrada.EmbeddedNavigator.Name = ""
        Me.grArticuloEntrada.Location = New System.Drawing.Point(2, 264)
        Me.grArticuloEntrada.MainView = Me.grvArticuloEntrada
        Me.grArticuloEntrada.Name = "grArticuloEntrada"
        Me.grArticuloEntrada.Size = New System.Drawing.Size(710, 232)
        Me.grArticuloEntrada.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grArticuloEntrada.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticuloEntrada.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticuloEntrada.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticuloEntrada.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticuloEntrada.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticuloEntrada.TabIndex = 62
        Me.grArticuloEntrada.TabStop = False
        Me.grArticuloEntrada.Text = "Traspasos"
        '
        'grvArticuloEntrada
        '
        Me.grvArticuloEntrada.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn8, Me.GridColumn9, Me.GridColumn6, Me.GridColumn7})
        Me.grvArticuloEntrada.GridControl = Me.grArticuloEntrada
        Me.grvArticuloEntrada.Name = "grvArticuloEntrada"
        Me.grvArticuloEntrada.OptionsCustomization.AllowFilter = False
        Me.grvArticuloEntrada.OptionsCustomization.AllowGroup = False
        Me.grvArticuloEntrada.OptionsCustomization.AllowSort = False
        Me.grvArticuloEntrada.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Partida"
        Me.GridColumn1.FieldName = "Partida"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 52
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Descripci�n"
        Me.GridColumn2.FieldName = "n_articulo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 97
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Art�culo"
        Me.GridColumn3.FieldName = "articulo"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.VisibleIndex = 1
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Nombre Bodega"
        Me.GridColumn4.FieldName = "n_bodega"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 4
        Me.GridColumn4.Width = 97
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Bodega"
        Me.GridColumn5.FieldName = "bodega"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 3
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Cantidad"
        Me.GridColumn8.FieldName = "cantidad"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.VisibleIndex = 5
        Me.GridColumn8.Width = 58
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Precio"
        Me.GridColumn9.DisplayFormat.FormatString = "c2"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "preciounitario"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.VisibleIndex = 6
        Me.GridColumn9.Width = 68
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Art�culo Destino "
        Me.GridColumn6.FieldName = "articulo_destino"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.VisibleIndex = 7
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Bodega Destino"
        Me.GridColumn7.FieldName = "bodega_destino"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.VisibleIndex = 8
        '
        'tmaArticulosEntrada
        '
        Me.tmaArticulosEntrada.BackColor = System.Drawing.Color.White
        Me.tmaArticulosEntrada.CanDelete = False
        Me.tmaArticulosEntrada.CanInsert = False
        Me.tmaArticulosEntrada.CanUpdate = True
        Me.tmaArticulosEntrada.Grid = Me.grArticuloEntrada
        Me.tmaArticulosEntrada.Location = New System.Drawing.Point(2, 240)
        Me.tmaArticulosEntrada.Name = "tmaArticulosEntrada"
        Me.tmaArticulosEntrada.Size = New System.Drawing.Size(710, 23)
        Me.tmaArticulosEntrada.TabIndex = 61
        Me.tmaArticulosEntrada.TabStop = False
        Me.tmaArticulosEntrada.Title = ""
        Me.tmaArticulosEntrada.UpdateTitle = "un Registro"
        '
        'lkpConceptoSalida
        '
        Me.lkpConceptoSalida.AllowAdd = False
        Me.lkpConceptoSalida.AutoReaload = False
        Me.lkpConceptoSalida.DataSource = Nothing
        Me.lkpConceptoSalida.DefaultSearchField = ""
        Me.lkpConceptoSalida.DisplayMember = "descripcion"
        Me.lkpConceptoSalida.EditValue = Nothing
        Me.lkpConceptoSalida.Filtered = False
        Me.lkpConceptoSalida.InitValue = Nothing
        Me.lkpConceptoSalida.Location = New System.Drawing.Point(120, 24)
        Me.lkpConceptoSalida.MultiSelect = False
        Me.lkpConceptoSalida.Name = "lkpConceptoSalida"
        Me.lkpConceptoSalida.NullText = ""
        Me.lkpConceptoSalida.PopupWidth = CType(400, Long)
        Me.lkpConceptoSalida.Required = False
        Me.lkpConceptoSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoSalida.SearchMember = ""
        Me.lkpConceptoSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoSalida.SelectAll = False
        Me.lkpConceptoSalida.Size = New System.Drawing.Size(216, 20)
        Me.lkpConceptoSalida.TabIndex = 64
        Me.lkpConceptoSalida.Tag = "concepto_salida"
        Me.lkpConceptoSalida.ToolTip = Nothing
        Me.lkpConceptoSalida.ValueMember = "concepto"
        '
        'lblConcepto_Salida
        '
        Me.lblConcepto_Salida.AutoSize = True
        Me.lblConcepto_Salida.Location = New System.Drawing.Point(16, 26)
        Me.lblConcepto_Salida.Name = "lblConcepto_Salida"
        Me.lblConcepto_Salida.Size = New System.Drawing.Size(98, 16)
        Me.lblConcepto_Salida.TabIndex = 63
        Me.lblConcepto_Salida.Tag = ""
        Me.lblConcepto_Salida.Text = "Concepto Sali&da:"
        Me.lblConcepto_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoEntrada
        '
        Me.lkpConceptoEntrada.AllowAdd = False
        Me.lkpConceptoEntrada.AutoReaload = False
        Me.lkpConceptoEntrada.DataSource = Nothing
        Me.lkpConceptoEntrada.DefaultSearchField = ""
        Me.lkpConceptoEntrada.DisplayMember = "descripcion"
        Me.lkpConceptoEntrada.EditValue = Nothing
        Me.lkpConceptoEntrada.Filtered = False
        Me.lkpConceptoEntrada.InitValue = Nothing
        Me.lkpConceptoEntrada.Location = New System.Drawing.Point(120, 48)
        Me.lkpConceptoEntrada.MultiSelect = False
        Me.lkpConceptoEntrada.Name = "lkpConceptoEntrada"
        Me.lkpConceptoEntrada.NullText = ""
        Me.lkpConceptoEntrada.PopupWidth = CType(400, Long)
        Me.lkpConceptoEntrada.Required = False
        Me.lkpConceptoEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoEntrada.SearchMember = ""
        Me.lkpConceptoEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoEntrada.SelectAll = False
        Me.lkpConceptoEntrada.Size = New System.Drawing.Size(216, 20)
        Me.lkpConceptoEntrada.TabIndex = 66
        Me.lkpConceptoEntrada.Tag = ""
        Me.lkpConceptoEntrada.ToolTip = Nothing
        Me.lkpConceptoEntrada.ValueMember = "concepto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(107, 16)
        Me.Label3.TabIndex = 65
        Me.Label3.Tag = ""
        Me.Label3.Text = "Co&ncepto Entrada:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones_Salida
        '
        Me.lblObservaciones_Salida.AutoSize = True
        Me.lblObservaciones_Salida.Location = New System.Drawing.Point(24, 176)
        Me.lblObservaciones_Salida.Name = "lblObservaciones_Salida"
        Me.lblObservaciones_Salida.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones_Salida.TabIndex = 67
        Me.lblObservaciones_Salida.Tag = ""
        Me.lblObservaciones_Salida.Text = "&Observaciones:"
        Me.lblObservaciones_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones_Salida
        '
        Me.txtObservaciones_Salida.EditValue = ""
        Me.txtObservaciones_Salida.Location = New System.Drawing.Point(120, 176)
        Me.txtObservaciones_Salida.Name = "txtObservaciones_Salida"
        Me.txtObservaciones_Salida.Size = New System.Drawing.Size(592, 48)
        Me.txtObservaciones_Salida.TabIndex = 68
        Me.txtObservaciones_Salida.Tag = "observaciones"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSucursal_Factura)
        Me.GroupBox2.Controls.Add(Me.lblFolio_Factura)
        Me.GroupBox2.Controls.Add(Me.lblSerie_Factura)
        Me.GroupBox2.Controls.Add(Me.txtSerie_Factura)
        Me.GroupBox2.Controls.Add(Me.lblFechaFactura)
        Me.GroupBox2.Controls.Add(Me.lkpFactura)
        Me.GroupBox2.Controls.Add(Me.dteFechaFactura)
        Me.GroupBox2.Controls.Add(Me.txtSucursal_Factura)
        Me.GroupBox2.Location = New System.Drawing.Point(4, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(352, 128)
        Me.GroupBox2.TabIndex = 69
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Factura"
        '
        'lblSucursal_Factura
        '
        Me.lblSucursal_Factura.AutoSize = True
        Me.lblSucursal_Factura.Location = New System.Drawing.Point(6, 48)
        Me.lblSucursal_Factura.Name = "lblSucursal_Factura"
        Me.lblSucursal_Factura.Size = New System.Drawing.Size(116, 16)
        Me.lblSucursal_Factura.TabIndex = 8
        Me.lblSucursal_Factura.Tag = ""
        Me.lblSucursal_Factura.Text = "S&ucursal de factura:"
        Me.lblSucursal_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(72, 24)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(50, 16)
        Me.lblFolio_Factura.TabIndex = 6
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "Fac&tura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerie_Factura
        '
        Me.lblSerie_Factura.AutoSize = True
        Me.lblSerie_Factura.Location = New System.Drawing.Point(25, 72)
        Me.lblSerie_Factura.Name = "lblSerie_Factura"
        Me.lblSerie_Factura.Size = New System.Drawing.Size(97, 16)
        Me.lblSerie_Factura.TabIndex = 12
        Me.lblSerie_Factura.Tag = ""
        Me.lblSerie_Factura.Text = "&Serie de factura:"
        Me.lblSerie_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie_Factura
        '
        Me.txtSerie_Factura.EditValue = ""
        Me.txtSerie_Factura.Location = New System.Drawing.Point(128, 70)
        Me.txtSerie_Factura.Name = "txtSerie_Factura"
        '
        'txtSerie_Factura.Properties
        '
        Me.txtSerie_Factura.Properties.Enabled = False
        Me.txtSerie_Factura.Properties.MaxLength = 3
        Me.txtSerie_Factura.Size = New System.Drawing.Size(96, 20)
        Me.txtSerie_Factura.TabIndex = 13
        Me.txtSerie_Factura.Tag = "serie_factura"
        '
        'lblFechaFactura
        '
        Me.lblFechaFactura.AutoSize = True
        Me.lblFechaFactura.Location = New System.Drawing.Point(18, 96)
        Me.lblFechaFactura.Name = "lblFechaFactura"
        Me.lblFechaFactura.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaFactura.TabIndex = 10
        Me.lblFechaFactura.Tag = ""
        Me.lblFechaFactura.Text = "&Fecha de Factura:"
        Me.lblFechaFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFactura
        '
        Me.lkpFactura.AllowAdd = False
        Me.lkpFactura.AutoReaload = False
        Me.lkpFactura.DataSource = Nothing
        Me.lkpFactura.DefaultSearchField = ""
        Me.lkpFactura.DisplayMember = "folio"
        Me.lkpFactura.EditValue = Nothing
        Me.lkpFactura.Filtered = False
        Me.lkpFactura.InitValue = Nothing
        Me.lkpFactura.Location = New System.Drawing.Point(128, 22)
        Me.lkpFactura.MultiSelect = False
        Me.lkpFactura.Name = "lkpFactura"
        Me.lkpFactura.NullText = ""
        Me.lkpFactura.PopupWidth = CType(400, Long)
        Me.lkpFactura.Required = True
        Me.lkpFactura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFactura.SearchMember = ""
        Me.lkpFactura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFactura.SelectAll = False
        Me.lkpFactura.Size = New System.Drawing.Size(216, 20)
        Me.lkpFactura.TabIndex = 7
        Me.lkpFactura.Tag = "folio_factura"
        Me.lkpFactura.ToolTip = Nothing
        Me.lkpFactura.ValueMember = "clave_compuesta"
        '
        'dteFechaFactura
        '
        Me.dteFechaFactura.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaFactura.Location = New System.Drawing.Point(128, 94)
        Me.dteFechaFactura.Name = "dteFechaFactura"
        '
        'dteFechaFactura.Properties
        '
        Me.dteFechaFactura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFactura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.Enabled = False
        Me.dteFechaFactura.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaFactura.TabIndex = 11
        Me.dteFechaFactura.Tag = "fecha_factura"
        '
        'txtSucursal_Factura
        '
        Me.txtSucursal_Factura.EditValue = ""
        Me.txtSucursal_Factura.Location = New System.Drawing.Point(128, 46)
        Me.txtSucursal_Factura.Name = "txtSucursal_Factura"
        '
        'txtSucursal_Factura.Properties
        '
        Me.txtSucursal_Factura.Properties.Enabled = False
        Me.txtSucursal_Factura.Properties.MaxLength = 3
        Me.txtSucursal_Factura.Size = New System.Drawing.Size(216, 20)
        Me.txtSucursal_Factura.TabIndex = 13
        Me.txtSucursal_Factura.Tag = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lkpConceptoSalida)
        Me.GroupBox1.Controls.Add(Me.lblConcepto_Salida)
        Me.GroupBox1.Controls.Add(Me.lkpConceptoEntrada)
        Me.GroupBox1.Location = New System.Drawing.Point(361, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 104)
        Me.GroupBox1.TabIndex = 70
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Conceptos"
        '
        'frmCambioMercanciaCancelada
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(714, 496)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblObservaciones_Salida)
        Me.Controls.Add(Me.grArticuloEntrada)
        Me.Controls.Add(Me.txtObservaciones_Salida)
        Me.Controls.Add(Me.tmaArticulosEntrada)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmCambioMercanciaCancelada"
        Me.Text = "frmCambioMercanciaCancelada"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.tmaArticulosEntrada, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones_Salida, 0)
        Me.Controls.SetChildIndex(Me.grArticuloEntrada, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones_Salida, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grArticuloEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticuloEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones_Salida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursal_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Dipros Systems, Declaraciones"


    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oVentasCambios As VillarrealBusiness.clsVentasCambios


    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private lsucursalfactura As Long
    Private lfoliofactura As Long

    Private ReadOnly Property Factura() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpFactura)
        End Get
    End Property
    Private ReadOnly Property ConceptoSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoSalida)
        End Get
    End Property
    Private ReadOnly Property ConceptoEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoEntrada)
        End Get
    End Property
    Private ReadOnly Property SucursalFactura() As Long
        Get
            Return Comunes.Common.Sucursal_Actual
        End Get
    End Property


#End Region

#Region "Dipros Systems, Eventos de la Forma"
    Private Sub frmCambioMercanciaCancelada_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCambioMercanciaCancelada_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCambioMercanciaCancelada_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmCambioMercanciaCancelada_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oVentasCambios = New VillarrealBusiness.clsVentasCambios
        With Me.tmaArticulosEntrada
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New Comunes.frmVentasDetalleCambios
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("preciounitario", "System.Double")
            .AddColumn("bodega")
            .AddColumn("n_bodega")

            .AddColumn("departamentodestino")
            .AddColumn("grupodestino")
            .AddColumn("articulo_destino")
            .AddColumn("bodega_destino")

        End With


    End Sub
    Private Sub frmCambioMercanciaCancelada_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Dim folio As Long
        Dim folio_movimiento As Long

        Response = oVentasCambios.EliminarxFactura(lsucursalfactura, Me.txtSerie_Factura.Text, Me.lfoliofactura)

        If Response.ErrorFound Then Exit Sub

        Do While Not Me.tmaArticulosEntrada.EOF
            tmaArticulosEntrada.MoveFirst()

            If (tmaArticulosEntrada.Item("articulo") <> tmaArticulosEntrada.Item("articulo_destino")) Or _
                    (tmaArticulosEntrada.Item("bodega") <> tmaArticulosEntrada.Item("bodega_destino")) Then

                'inserto en la tabla de ventas cambios los articulos q fueron cambiados de bodega o por articulos LU
                Response = oVentasCambios.Insertar(Me.tmaArticulosEntrada.SelectedRow, lsucursalfactura, Me.txtSerie_Factura.Text, Me.lfoliofactura)


                
                '' aki va el codigo que insertara los movimientos de entradas y salidas de los articulos

                'Movimiento de salida
                Response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, Me.tmaArticulosEntrada.Item("bodega"), ConceptoSalida, folio, _
                                     dteFecha.EditValue, Comunes.Common.Sucursal_Actual, ConceptoSalida, folio, "Movimiento de Salida generado desde Cambio de Articulos Cancelados")

                folio_movimiento = folio

                If Response.ErrorFound Then Exit Sub
                Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Me.tmaArticulosEntrada.Item("bodega"), ConceptoSalida, folio_movimiento, dteFecha.EditValue, Me.tmaArticulosEntrada.Item("partida"), Me.tmaArticulosEntrada.Item("articulo"), Me.tmaArticulosEntrada.Item("cantidad"), Me.tmaArticulosEntrada.Item("preciounitario "), Me.tmaArticulosEntrada.Item("importe"), -1, 0, System.DBNull.Value)


                'Movimiento de entrada
                If Response.ErrorFound Then Exit Sub
                Response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, Me.tmaArticulosEntrada.Item("bodega_destino"), ConceptoEntrada, folio, _
                                     dteFecha.EditValue, Comunes.Common.Sucursal_Actual, ConceptoSalida, folio, "Movimiento de Entrada generado desde Cambio de Articulos Cancelados")

                If Response.ErrorFound Then Exit Sub
                'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Me.tmaArticulosEntrada.Item("bodega_destino"), ConceptoSalida, folio_movimiento, dteFecha.EditValue, Me.tmaArticulosEntrada.Item("partida"), Me.tmaArticulosEntrada.Item("articulo_destino"), Me.tmaArticulosEntrada.Item("cantidad"), Me.tmaArticulosEntrada.Item("preciounitario "), Me.tmaArticulosEntrada.Item("importe"), -1, 0, System.DBNull.Value)

            End If

            tmaArticulosEntrada.MoveNext()
        Loop


        'Actualiza la Factura como ya modificada
        'If Response.ErrorFound Then Exit Sub
        ' Response = oVentasCambios.ActualizaFacturaCambioFisicoArticulos(lsucursalfactura, Me.txtSerie_Factura.Text, Me.lfoliofactura)


    End Sub
    Private Sub frmCambioMercanciaCancelada_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVentasCambios.Validacion(Factura, Me.ConceptoSalida, Me.ConceptoEntrada)
    End Sub

#End Region

#Region "Dipros Systems, Eventos de los Controles"

    Private Sub lkpConceptoSalida_Format() Handles lkpConceptoSalida.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoSalida)
    End Sub
    Private Sub lkpConceptoSalida_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoSalida.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConceptoEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoEntrada.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConceptoEntrada_Format() Handles lkpConceptoEntrada.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoEntrada)
    End Sub

    Private Sub lkpFactura_Format() Handles lkpFactura.Format
        Comunes.clsFormato.for_ventas_facturas_grl(Me.lkpFactura)
    End Sub
    Private Sub lkpFactura_LoadData(ByVal Initialize As Boolean) Handles lkpFactura.LoadData
        Dim response As Events
        response = oVentas.LookupFacturasCanceladas(SucursalFactura)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFactura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpFactura_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFactura.EditValueChanged
        Me.txtSerie_Factura.Text = Me.lkpFactura.GetValue("serie")
        Me.dteFechaFactura.EditValue = Me.lkpFactura.GetValue("fecha")
        Me.txtSucursal_Factura.Text = Me.lkpFactura.GetValue("nombre_sucursal")
        lsucursalfactura = Me.lkpFactura.GetValue("sucursal")
        lfoliofactura = Me.lkpFactura.GetValue("folio")

        DespliegaArticulos(lsucursalfactura, Me.txtSerie_Factura.Text, lfoliofactura)
    End Sub
#End Region

#Region "Dipros Systems, Funcionalidad"

    Private Sub DespliegaArticulos(ByVal sucursalfactura As Long, ByVal seriefactura As String, ByVal folioFactura As Long)
        Dim response As Events
        Dim odataset As DataSet
        Dim i As Long

        response = oVentasDetalle.Listado(sucursalfactura, seriefactura, folioFactura)

        If Not response.ErrorFound Then

            odataset = response.Value
            Me.tmaArticulosEntrada.DataSource = odataset
            For i = 0 To odataset.Tables(0).Rows.Count - 1
                tmaArticulosEntrada.DataSource.Tables(0).Rows(i).Item("articulo_destino") = odataset.Tables(0).Rows(i).Item("articulo")
                tmaArticulosEntrada.DataSource.Tables(0).Rows(i).Item("bodega_destino") = odataset.Tables(0).Rows(i).Item("bodega")
            Next
        End If


    End Sub

#End Region


    Private Sub frmCambioMercanciaCancelada_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.MenuOption.Name, "FECHA_CAM_CANC") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub
End Class
