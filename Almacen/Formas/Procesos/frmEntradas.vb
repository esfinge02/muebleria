Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Structure ArticulosPreciosCostos
    Dim Indice As Integer
    Dim Precios As DataSet
End Structure

Public Class frmEntradas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Private EstructuraPrecios As ArrayList

    Private concepto_nulo As String = ""
    Private SucursalOC As Long = 0

    Private folio_movimiento As Long = 0
    Private Guardado As Boolean = False
    Public bcosteado As Boolean = False
    Private bdespliega As Boolean = False



#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpOrden As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFolio_Proveedor As System.Windows.Forms.Label
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grEntradas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvEntradas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaEntradas As Dipros.Windows.TINMaster
    Friend WithEvents lkpFolioProveedor As Dipros.Editors.TINMultiLookup
   
    Friend WithEvents lblPedimento As System.Windows.Forms.Label
    Friend WithEvents txtPedimento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAduana As System.Windows.Forms.Label
    Friend WithEvents txtAduana As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFecha_Importacion As System.Windows.Forms.Label
    'Friend WithEvents dteFecha_Importacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox

    Friend WithEvents chkImportacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCosteado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcSubTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcIva As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label

    Friend WithEvents ChkAbonoCuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents clcPlazo1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCondicion1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion2 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion3 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo3 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion4 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo4 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcPlazo5 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblCondicion5 As System.Windows.Forms.Label

    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup


    Friend WithEvents grPagos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPagos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents nvrDatosPago As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents tbrReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbrCostear As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteCondicion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents nvDatosFlete As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnFlete As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lkpAcreedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkFleteEnFactura As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcOtrosGastos As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMonto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCostoProrrateo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcTotalCostoUnitario As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcSeguro As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblSeguro As System.Windows.Forms.Label
    Friend WithEvents clcManiobras As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblManiobras As System.Windows.Forms.Label
    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
    Friend WithEvents lblIva As System.Windows.Forms.Label
    Friend WithEvents lblIvaRemoto As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents grpFlete As System.Windows.Forms.GroupBox
    Friend WithEvents clcSubtotalFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcIVAFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcIvaRetenido As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotalFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcFolioFactura As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents dteFechaFactura As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaFactura As System.Windows.Forms.Label
    Friend WithEvents lblFolioFactura As System.Windows.Forms.Label
    Friend WithEvents tmaDescuentosProveedores As Dipros.Windows.TINMaster
    Friend WithEvents grDescuentosProveedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDescuentosProveedor As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorcentaje As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAntesIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProntoPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dteFecha_Importacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcImporteDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label

    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad_Pedida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto_Pedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto_recibido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCostoFlete As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioLista As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCostoConFlete As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcSubTotalconFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotalconFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcIvaconFlete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents grcImporteConFlete As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbrEtiquetas As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcbPrecioLista As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtcondiciones_pago As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEntradas))
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpOrden = New Dipros.Editors.TINMultiLookup
        Me.lblFolio_Proveedor = New System.Windows.Forms.Label
        Me.lblReferencia = New System.Windows.Forms.Label
        Me.txtReferencia = New DevExpress.XtraEditors.TextEdit
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grEntradas = New DevExpress.XtraGrid.GridControl
        Me.grvEntradas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad_Pedida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto_Pedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto_recibido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCostoConFlete = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteConFlete = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCostoFlete = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioLista = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcbPrecioLista = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaEntradas = New Dipros.Windows.TINMaster
        Me.lkpFolioProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblPedimento = New System.Windows.Forms.Label
        Me.txtPedimento = New DevExpress.XtraEditors.TextEdit
        Me.lblAduana = New System.Windows.Forms.Label
        Me.txtAduana = New DevExpress.XtraEditors.TextEdit
        Me.lblFecha_Importacion = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dteFecha_Importacion = New DevExpress.XtraEditors.DateEdit
        Me.chkImportacion = New DevExpress.XtraEditors.CheckEdit
        Me.tbrReimprimir = New System.Windows.Forms.ToolBarButton
        Me.tbrCostear = New System.Windows.Forms.ToolBarButton
        Me.chkCosteado = New DevExpress.XtraEditors.CheckEdit
        Me.clcSubTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcIva = New DevExpress.XtraEditors.CalcEdit
        Me.clcTotal = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.ChkAbonoCuenta = New DevExpress.XtraEditors.CheckEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblCondicion1 = New System.Windows.Forms.Label
        Me.clcPlazo5 = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblCondicion5 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcPlazo4 = New Dipros.Editors.TINCalcEdit
        Me.lblCondicion4 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcPlazo3 = New Dipros.Editors.TINCalcEdit
        Me.clcPlazo2 = New Dipros.Editors.TINCalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcPlazo1 = New Dipros.Editors.TINCalcEdit
        Me.lblCondicion2 = New System.Windows.Forms.Label
        Me.lblCondicion3 = New System.Windows.Forms.Label
        Me.grPagos = New DevExpress.XtraGrid.GridControl
        Me.grvPagos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMonto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteCondicion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosPago = New DevExpress.XtraNavBar.NavBarItem
        Me.nvDatosFlete = New DevExpress.XtraNavBar.NavBarItem
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.clcTotalconFlete = New DevExpress.XtraEditors.CalcEdit
        Me.clcIvaconFlete = New DevExpress.XtraEditors.CalcEdit
        Me.clcSubTotalconFlete = New DevExpress.XtraEditors.CalcEdit
        Me.clcTotalCostoUnitario = New DevExpress.XtraEditors.CalcEdit
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.txtcondiciones_pago = New DevExpress.XtraEditors.MemoEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.tmaDescuentosProveedores = New Dipros.Windows.TINMaster
        Me.grDescuentosProveedores = New DevExpress.XtraGrid.GridControl
        Me.grvDescuentosProveedor = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorcentaje = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAntesIVA = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProntoPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnFlete = New System.Windows.Forms.Panel
        Me.grpFlete = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.clcTotalFlete = New DevExpress.XtraEditors.CalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.clcIvaRetenido = New DevExpress.XtraEditors.CalcEdit
        Me.lblIvaRemoto = New System.Windows.Forms.Label
        Me.clcIVAFlete = New DevExpress.XtraEditors.CalcEdit
        Me.lblIva = New System.Windows.Forms.Label
        Me.clcSubtotalFlete = New DevExpress.XtraEditors.CalcEdit
        Me.lblSubtotal = New System.Windows.Forms.Label
        Me.clcManiobras = New DevExpress.XtraEditors.CalcEdit
        Me.lblManiobras = New System.Windows.Forms.Label
        Me.clcSeguro = New DevExpress.XtraEditors.CalcEdit
        Me.lblSeguro = New System.Windows.Forms.Label
        Me.clcFolioFactura = New DevExpress.XtraEditors.CalcEdit
        Me.dteFechaFactura = New DevExpress.XtraEditors.DateEdit
        Me.lblFechaFactura = New System.Windows.Forms.Label
        Me.lblFolioFactura = New System.Windows.Forms.Label
        Me.clcOtrosGastos = New DevExpress.XtraEditors.CalcEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.clcFlete = New DevExpress.XtraEditors.CalcEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.chkFleteEnFactura = New DevExpress.XtraEditors.CheckEdit
        Me.lkpAcreedor = New Dipros.Editors.TINMultiLookup
        Me.tbrEtiquetas = New System.Windows.Forms.ToolBarButton
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPedimento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAduana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFecha_Importacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkImportacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCosteado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChkAbonoCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcPlazo5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.clcTotalconFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIvaconFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubTotalconFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalCostoUnitario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.txtcondiciones_pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnFlete.SuspendLayout()
        Me.grpFlete.SuspendLayout()
        CType(Me.clcTotalFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIvaRetenido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIVAFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubtotalFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcManiobras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSeguro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcOtrosGastos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFleteEnFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrReimprimir, Me.tbrCostear, Me.tbrEtiquetas})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(33184, 28)
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(88, 18)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 0
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(128, 16)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(64, 19)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "entrada"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(584, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "F&echa:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(632, 16)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(104, 23)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(58, 43)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 4
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Provee&dor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(128, 40)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(450, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = True
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(608, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 5
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(16, 67)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(107, 16)
        Me.lblOrden.TabIndex = 6
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "Orden de Co&mpra:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpOrden
        '
        Me.lkpOrden.AllowAdd = False
        Me.lkpOrden.AutoReaload = False
        Me.lkpOrden.DataSource = Nothing
        Me.lkpOrden.DefaultSearchField = ""
        Me.lkpOrden.DisplayMember = "orden"
        Me.lkpOrden.EditValue = Nothing
        Me.lkpOrden.Filtered = False
        Me.lkpOrden.InitValue = Nothing
        Me.lkpOrden.Location = New System.Drawing.Point(128, 64)
        Me.lkpOrden.MultiSelect = False
        Me.lkpOrden.Name = "lkpOrden"
        Me.lkpOrden.NullText = ""
        Me.lkpOrden.PopupWidth = CType(350, Long)
        Me.lkpOrden.ReadOnlyControl = False
        Me.lkpOrden.Required = False
        Me.lkpOrden.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpOrden.SearchMember = ""
        Me.lkpOrden.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpOrden.SelectAll = False
        Me.lkpOrden.Size = New System.Drawing.Size(104, 20)
        Me.lkpOrden.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpOrden.TabIndex = 7
        Me.lkpOrden.Tag = "orden"
        Me.lkpOrden.ToolTip = Nothing
        Me.lkpOrden.ValueMember = "orden"
        '
        'lblFolio_Proveedor
        '
        Me.lblFolio_Proveedor.AutoSize = True
        Me.lblFolio_Proveedor.Location = New System.Drawing.Point(351, 67)
        Me.lblFolio_Proveedor.Name = "lblFolio_Proveedor"
        Me.lblFolio_Proveedor.Size = New System.Drawing.Size(115, 16)
        Me.lblFolio_Proveedor.TabIndex = 8
        Me.lblFolio_Proveedor.Tag = ""
        Me.lblFolio_Proveedor.Text = "Fo&lio del Proveedor:"
        Me.lblFolio_Proveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(56, 91)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(67, 16)
        Me.lblReferencia.TabIndex = 10
        Me.lblReferencia.Tag = ""
        Me.lblReferencia.Text = "Refere&ncia:"
        Me.lblReferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtReferencia
        '
        Me.txtReferencia.EditValue = ""
        Me.txtReferencia.Location = New System.Drawing.Point(128, 88)
        Me.txtReferencia.Name = "txtReferencia"
        '
        'txtReferencia.Properties
        '
        Me.txtReferencia.Properties.MaxLength = 15
        Me.txtReferencia.Size = New System.Drawing.Size(160, 20)
        Me.txtReferencia.TabIndex = 11
        Me.txtReferencia.Tag = "referencia"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(416, 91)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 12
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(472, 88)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(420, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(264, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 13
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(34, 112)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 14
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(128, 112)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(608, 48)
        Me.txtObservaciones.TabIndex = 15
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grEntradas
        '
        '
        'grEntradas.EmbeddedNavigator
        '
        Me.grEntradas.EmbeddedNavigator.Name = ""
        Me.grEntradas.Location = New System.Drawing.Point(15, 304)
        Me.grEntradas.MainView = Me.grvEntradas
        Me.grEntradas.Name = "grEntradas"
        Me.grEntradas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grEntradas.Size = New System.Drawing.Size(721, 176)
        Me.grEntradas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style10", New DevExpress.Utils.ViewStyleEx("Style10", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grEntradas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style4", New DevExpress.Utils.ViewStyleEx("Style4", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style5", New DevExpress.Utils.ViewStyleEx("Style5", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style9", New DevExpress.Utils.ViewStyleEx("Style9", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style8", New DevExpress.Utils.ViewStyleEx("Style8", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style3", New DevExpress.Utils.ViewStyleEx("Style3", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style7", New DevExpress.Utils.ViewStyleEx("Style7", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style6", New DevExpress.Utils.ViewStyleEx("Style6", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEntradas.TabIndex = 11
        Me.grEntradas.TabStop = False
        Me.grEntradas.Text = "Entradas"
        '
        'grvEntradas
        '
        Me.grvEntradas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcDescripcion, Me.grcCantidad_Pedida, Me.grcCantidad, Me.grcCosto_Pedido, Me.grcCosto_recibido, Me.grcCostoConFlete, Me.grcImporte, Me.grcImporteConFlete, Me.GridColumn1, Me.grcDepartamento, Me.grcGrupo, Me.grcCostoFlete, Me.grcPrecioLista, Me.grcbPrecioLista, Me.grcModelo})
        Me.grvEntradas.GridControl = Me.grEntradas
        Me.grvEntradas.Name = "grvEntradas"
        Me.grvEntradas.OptionsBehavior.Editable = False
        Me.grvEntradas.OptionsCustomization.AllowFilter = False
        Me.grvEntradas.OptionsCustomization.AllowGroup = False
        Me.grvEntradas.OptionsCustomization.AllowSort = False
        Me.grvEntradas.OptionsView.ShowGroupPanel = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Artículo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.Width = 50
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripción"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 1
        Me.grcDescripcion.Width = 194
        '
        'grcCantidad_Pedida
        '
        Me.grcCantidad_Pedida.Caption = "Pedida"
        Me.grcCantidad_Pedida.FieldName = "cantidad_pedida"
        Me.grcCantidad_Pedida.Name = "grcCantidad_Pedida"
        Me.grcCantidad_Pedida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad_Pedida.VisibleIndex = 2
        Me.grcCantidad_Pedida.Width = 49
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Recibida"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 57
        '
        'grcCosto_Pedido
        '
        Me.grcCosto_Pedido.Caption = "Costo Pedido"
        Me.grcCosto_Pedido.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcCosto_Pedido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCosto_Pedido.FieldName = "costo_pedido"
        Me.grcCosto_Pedido.Name = "grcCosto_Pedido"
        Me.grcCosto_Pedido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto_Pedido.Width = 83
        '
        'grcCosto_recibido
        '
        Me.grcCosto_recibido.Caption = "Costo s/Flete"
        Me.grcCosto_recibido.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcCosto_recibido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCosto_recibido.FieldName = "costo"
        Me.grcCosto_recibido.Name = "grcCosto_recibido"
        Me.grcCosto_recibido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto_recibido.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCosto_recibido.VisibleIndex = 5
        Me.grcCosto_recibido.Width = 85
        '
        'grcCostoConFlete
        '
        Me.grcCostoConFlete.Caption = "Costo c/Flete"
        Me.grcCostoConFlete.DisplayFormat.FormatString = "$ ###,##0.00"
        Me.grcCostoConFlete.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCostoConFlete.FieldName = "costo_flete"
        Me.grcCostoConFlete.Name = "grcCostoConFlete"
        Me.grcCostoConFlete.VisibleIndex = 4
        Me.grcCostoConFlete.Width = 84
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe s/Flete"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.VisibleIndex = 7
        Me.grcImporte.Width = 89
        '
        'grcImporteConFlete
        '
        Me.grcImporteConFlete.Caption = "Importe c/Flete"
        Me.grcImporteConFlete.DisplayFormat.FormatString = "$ ###,##0.00"
        Me.grcImporteConFlete.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteConFlete.FieldName = "importe_con_flete"
        Me.grcImporteConFlete.Name = "grcImporteConFlete"
        Me.grcImporteConFlete.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteConFlete.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporteConFlete.VisibleIndex = 6
        Me.grcImporteConFlete.Width = 99
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "folio_historico_costo"
        Me.GridColumn1.FieldName = "folio_historico_costo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.Width = 25
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        Me.grcDepartamento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        Me.grcGrupo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCostoFlete
        '
        Me.grcCostoFlete.Caption = "Costo Flete"
        Me.grcCostoFlete.FieldName = "costo_flete"
        Me.grcCostoFlete.Name = "grcCostoFlete"
        Me.grcCostoFlete.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPrecioLista
        '
        Me.grcPrecioLista.Caption = "Precio Lista"
        Me.grcPrecioLista.FieldName = "precio_lista"
        Me.grcPrecioLista.Name = "grcPrecioLista"
        Me.grcPrecioLista.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcbPrecioLista
        '
        Me.grcbPrecioLista.Caption = "bPrecio Lista"
        Me.grcbPrecioLista.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcbPrecioLista.FieldName = "bprecio_lista"
        Me.grcbPrecioLista.Name = "grcbPrecioLista"
        Me.grcbPrecioLista.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 0
        '
        'tmaEntradas
        '
        Me.tmaEntradas.BackColor = System.Drawing.Color.White
        Me.tmaEntradas.CanDelete = False
        Me.tmaEntradas.CanInsert = False
        Me.tmaEntradas.CanUpdate = True
        Me.tmaEntradas.Grid = Me.grEntradas
        Me.tmaEntradas.Location = New System.Drawing.Point(16, 280)
        Me.tmaEntradas.Name = "tmaEntradas"
        Me.tmaEntradas.Size = New System.Drawing.Size(720, 23)
        Me.tmaEntradas.TabIndex = 10
        Me.tmaEntradas.TabStop = False
        Me.tmaEntradas.Title = "Artículos"
        Me.tmaEntradas.UpdateTitle = "un Registro"
        '
        'lkpFolioProveedor
        '
        Me.lkpFolioProveedor.AllowAdd = False
        Me.lkpFolioProveedor.AutoReaload = False
        Me.lkpFolioProveedor.DataSource = Nothing
        Me.lkpFolioProveedor.DefaultSearchField = ""
        Me.lkpFolioProveedor.DisplayMember = "folio_proveedor"
        Me.lkpFolioProveedor.EditValue = Nothing
        Me.lkpFolioProveedor.Filtered = False
        Me.lkpFolioProveedor.InitValue = Nothing
        Me.lkpFolioProveedor.Location = New System.Drawing.Point(472, 64)
        Me.lkpFolioProveedor.MultiSelect = False
        Me.lkpFolioProveedor.Name = "lkpFolioProveedor"
        Me.lkpFolioProveedor.NullText = ""
        Me.lkpFolioProveedor.PopupWidth = CType(280, Long)
        Me.lkpFolioProveedor.ReadOnlyControl = False
        Me.lkpFolioProveedor.Required = False
        Me.lkpFolioProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioProveedor.SearchMember = ""
        Me.lkpFolioProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioProveedor.SelectAll = False
        Me.lkpFolioProveedor.Size = New System.Drawing.Size(264, 20)
        Me.lkpFolioProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolioProveedor.TabIndex = 9
        Me.lkpFolioProveedor.Tag = ""
        Me.lkpFolioProveedor.ToolTip = "folio"
        Me.lkpFolioProveedor.ValueMember = "folio_proveedor"
        '
        'lblPedimento
        '
        Me.lblPedimento.AutoSize = True
        Me.lblPedimento.Location = New System.Drawing.Point(94, 27)
        Me.lblPedimento.Name = "lblPedimento"
        Me.lblPedimento.Size = New System.Drawing.Size(67, 16)
        Me.lblPedimento.TabIndex = 1
        Me.lblPedimento.Tag = ""
        Me.lblPedimento.Text = "Pedimen&to:"
        Me.lblPedimento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPedimento
        '
        Me.txtPedimento.EditValue = ""
        Me.txtPedimento.Location = New System.Drawing.Point(168, 24)
        Me.txtPedimento.Name = "txtPedimento"
        '
        'txtPedimento.Properties
        '
        Me.txtPedimento.Properties.Enabled = False
        Me.txtPedimento.Properties.MaxLength = 30
        Me.txtPedimento.Size = New System.Drawing.Size(180, 20)
        Me.txtPedimento.TabIndex = 17
        Me.txtPedimento.Tag = "pedimento"
        '
        'lblAduana
        '
        Me.lblAduana.AutoSize = True
        Me.lblAduana.Location = New System.Drawing.Point(110, 51)
        Me.lblAduana.Name = "lblAduana"
        Me.lblAduana.Size = New System.Drawing.Size(51, 16)
        Me.lblAduana.TabIndex = 3
        Me.lblAduana.Tag = ""
        Me.lblAduana.Text = "Ad&uana:"
        Me.lblAduana.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAduana
        '
        Me.txtAduana.EditValue = ""
        Me.txtAduana.Location = New System.Drawing.Point(168, 48)
        Me.txtAduana.Name = "txtAduana"
        '
        'txtAduana.Properties
        '
        Me.txtAduana.Properties.Enabled = False
        Me.txtAduana.Properties.MaxLength = 50
        Me.txtAduana.Size = New System.Drawing.Size(512, 20)
        Me.txtAduana.TabIndex = 18
        Me.txtAduana.Tag = "aduana"
        '
        'lblFecha_Importacion
        '
        Me.lblFecha_Importacion.AutoSize = True
        Me.lblFecha_Importacion.Location = New System.Drawing.Point(32, 75)
        Me.lblFecha_Importacion.Name = "lblFecha_Importacion"
        Me.lblFecha_Importacion.Size = New System.Drawing.Size(129, 16)
        Me.lblFecha_Importacion.TabIndex = 5
        Me.lblFecha_Importacion.Tag = ""
        Me.lblFecha_Importacion.Text = "Fecha de &Importación:"
        Me.lblFecha_Importacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dteFecha_Importacion)
        Me.GroupBox1.Controls.Add(Me.chkImportacion)
        Me.GroupBox1.Controls.Add(Me.lblAduana)
        Me.GroupBox1.Controls.Add(Me.lblFecha_Importacion)
        Me.GroupBox1.Controls.Add(Me.lblPedimento)
        Me.GroupBox1.Controls.Add(Me.txtAduana)
        Me.GroupBox1.Controls.Add(Me.txtPedimento)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 168)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(720, 104)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        '
        'dteFecha_Importacion
        '
        Me.dteFecha_Importacion.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha_Importacion.Location = New System.Drawing.Point(168, 72)
        Me.dteFecha_Importacion.Name = "dteFecha_Importacion"
        '
        'dteFecha_Importacion.Properties
        '
        Me.dteFecha_Importacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Importacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Importacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Importacion.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Importacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Importacion.Properties.Enabled = False
        Me.dteFecha_Importacion.Size = New System.Drawing.Size(104, 23)
        Me.dteFecha_Importacion.TabIndex = 19
        Me.dteFecha_Importacion.Tag = "fecha_importacion"
        '
        'chkImportacion
        '
        Me.chkImportacion.Location = New System.Drawing.Point(16, 0)
        Me.chkImportacion.Name = "chkImportacion"
        '
        'chkImportacion.Properties
        '
        Me.chkImportacion.Properties.Caption = "Entrada de Importación"
        Me.chkImportacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkImportacion.Size = New System.Drawing.Size(144, 19)
        Me.chkImportacion.TabIndex = 16
        Me.chkImportacion.Tag = ""
        Me.chkImportacion.ToolTip = "Entrada de Importación"
        '
        'tbrReimprimir
        '
        Me.tbrReimprimir.ImageIndex = 6
        Me.tbrReimprimir.Text = "Reimprimir"
        '
        'tbrCostear
        '
        Me.tbrCostear.ImageIndex = 7
        Me.tbrCostear.Text = "Costear"
        '
        'chkCosteado
        '
        Me.chkCosteado.Location = New System.Drawing.Point(16, 134)
        Me.chkCosteado.Name = "chkCosteado"
        '
        'chkCosteado.Properties
        '
        Me.chkCosteado.Properties.Caption = "Costeado"
        Me.chkCosteado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkCosteado.Size = New System.Drawing.Size(88, 19)
        Me.chkCosteado.TabIndex = 7
        Me.chkCosteado.Tag = "costeado"
        Me.chkCosteado.Visible = False
        '
        'clcSubTotal
        '
        Me.clcSubTotal.Location = New System.Drawing.Point(640, 488)
        Me.clcSubTotal.Name = "clcSubTotal"
        '
        'clcSubTotal.Properties
        '
        Me.clcSubTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotal.Properties.ReadOnly = True
        Me.clcSubTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubTotal.Size = New System.Drawing.Size(96, 20)
        Me.clcSubTotal.TabIndex = 19
        Me.clcSubTotal.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(466, 488)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 16)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "SubTotal:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(495, 512)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "IVA:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'clcIva
        '
        Me.clcIva.Location = New System.Drawing.Point(640, 512)
        Me.clcIva.Name = "clcIva"
        '
        'clcIva.Properties
        '
        Me.clcIva.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.ReadOnly = True
        Me.clcIva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIva.Size = New System.Drawing.Size(96, 20)
        Me.clcIva.TabIndex = 22
        Me.clcIva.TabStop = False
        '
        'clcTotal
        '
        Me.clcTotal.Location = New System.Drawing.Point(640, 536)
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.ReadOnly = True
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(96, 20)
        Me.clcTotal.TabIndex = 24
        Me.clcTotal.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(488, 536)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 16)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Total:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ChkAbonoCuenta
        '
        Me.ChkAbonoCuenta.Location = New System.Drawing.Point(16, 150)
        Me.ChkAbonoCuenta.Name = "ChkAbonoCuenta"
        '
        'ChkAbonoCuenta.Properties
        '
        Me.ChkAbonoCuenta.Properties.Caption = "Abono a Cuenta"
        Me.ChkAbonoCuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.ChkAbonoCuenta.Size = New System.Drawing.Size(112, 19)
        Me.ChkAbonoCuenta.TabIndex = 8
        Me.ChkAbonoCuenta.Tag = "abono_cuenta"
        Me.ChkAbonoCuenta.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblCondicion1)
        Me.GroupBox2.Controls.Add(Me.clcPlazo5)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.lblCondicion5)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.clcPlazo4)
        Me.GroupBox2.Controls.Add(Me.lblCondicion4)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.clcPlazo3)
        Me.GroupBox2.Controls.Add(Me.clcPlazo2)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.clcPlazo1)
        Me.GroupBox2.Controls.Add(Me.lblCondicion2)
        Me.GroupBox2.Controls.Add(Me.lblCondicion3)
        Me.GroupBox2.Location = New System.Drawing.Point(24, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(204, 152)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Condiciones"
        '
        'lblCondicion1
        '
        Me.lblCondicion1.AutoSize = True
        Me.lblCondicion1.Location = New System.Drawing.Point(32, 24)
        Me.lblCondicion1.Name = "lblCondicion1"
        Me.lblCondicion1.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion1.TabIndex = 1
        Me.lblCondicion1.Tag = ""
        Me.lblCondicion1.Text = "1:"
        Me.lblCondicion1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo5
        '
        Me.clcPlazo5.EditValue = "0"
        Me.clcPlazo5.Location = New System.Drawing.Point(56, 120)
        Me.clcPlazo5.MaxValue = 0
        Me.clcPlazo5.MinValue = 0
        Me.clcPlazo5.Name = "clcPlazo5"
        '
        'clcPlazo5.Properties
        '
        Me.clcPlazo5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo5.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo5.Properties.MaxLength = 4
        Me.clcPlazo5.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo5.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo5.TabIndex = 14
        Me.clcPlazo5.Tag = "plazo_pago_5"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(112, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(28, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Tag = ""
        Me.Label5.Text = "Días"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCondicion5
        '
        Me.lblCondicion5.AutoSize = True
        Me.lblCondicion5.Location = New System.Drawing.Point(32, 120)
        Me.lblCondicion5.Name = "lblCondicion5"
        Me.lblCondicion5.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion5.TabIndex = 13
        Me.lblCondicion5.Tag = ""
        Me.lblCondicion5.Text = "5:"
        Me.lblCondicion5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(112, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(28, 16)
        Me.Label9.TabIndex = 12
        Me.Label9.Tag = ""
        Me.Label9.Text = "Días"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo4
        '
        Me.clcPlazo4.EditValue = "0"
        Me.clcPlazo4.Location = New System.Drawing.Point(56, 96)
        Me.clcPlazo4.MaxValue = 0
        Me.clcPlazo4.MinValue = 0
        Me.clcPlazo4.Name = "clcPlazo4"
        '
        'clcPlazo4.Properties
        '
        Me.clcPlazo4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo4.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo4.Properties.MaxLength = 4
        Me.clcPlazo4.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo4.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo4.TabIndex = 11
        Me.clcPlazo4.Tag = "plazo_pago_4"
        '
        'lblCondicion4
        '
        Me.lblCondicion4.AutoSize = True
        Me.lblCondicion4.Location = New System.Drawing.Point(32, 96)
        Me.lblCondicion4.Name = "lblCondicion4"
        Me.lblCondicion4.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion4.TabIndex = 10
        Me.lblCondicion4.Tag = ""
        Me.lblCondicion4.Text = "4:"
        Me.lblCondicion4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(112, 120)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(28, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Tag = ""
        Me.Label11.Text = "Días"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(112, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Tag = ""
        Me.Label7.Text = "Días"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo3
        '
        Me.clcPlazo3.EditValue = "0"
        Me.clcPlazo3.Location = New System.Drawing.Point(56, 72)
        Me.clcPlazo3.MaxValue = 0
        Me.clcPlazo3.MinValue = 0
        Me.clcPlazo3.Name = "clcPlazo3"
        '
        'clcPlazo3.Properties
        '
        Me.clcPlazo3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo3.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo3.Properties.MaxLength = 4
        Me.clcPlazo3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo3.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo3.TabIndex = 8
        Me.clcPlazo3.Tag = "plazo_pago_3"
        '
        'clcPlazo2
        '
        Me.clcPlazo2.EditValue = "0"
        Me.clcPlazo2.Location = New System.Drawing.Point(56, 48)
        Me.clcPlazo2.MaxValue = 0
        Me.clcPlazo2.MinValue = 0
        Me.clcPlazo2.Name = "clcPlazo2"
        '
        'clcPlazo2.Properties
        '
        Me.clcPlazo2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo2.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo2.Properties.MaxLength = 4
        Me.clcPlazo2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo2.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo2.TabIndex = 5
        Me.clcPlazo2.Tag = "plazo_pago_2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(112, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(28, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Tag = ""
        Me.Label6.Text = "Días"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo1
        '
        Me.clcPlazo1.EditValue = "0"
        Me.clcPlazo1.Location = New System.Drawing.Point(56, 24)
        Me.clcPlazo1.MaxValue = 0
        Me.clcPlazo1.MinValue = 0
        Me.clcPlazo1.Name = "clcPlazo1"
        '
        'clcPlazo1.Properties
        '
        Me.clcPlazo1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo1.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo1.Properties.MaxLength = 4
        Me.clcPlazo1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo1.Size = New System.Drawing.Size(48, 19)
        Me.clcPlazo1.TabIndex = 2
        Me.clcPlazo1.Tag = "plazo_pago_1"
        '
        'lblCondicion2
        '
        Me.lblCondicion2.AutoSize = True
        Me.lblCondicion2.Location = New System.Drawing.Point(32, 48)
        Me.lblCondicion2.Name = "lblCondicion2"
        Me.lblCondicion2.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion2.TabIndex = 4
        Me.lblCondicion2.Tag = ""
        Me.lblCondicion2.Text = "2:"
        Me.lblCondicion2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCondicion3
        '
        Me.lblCondicion3.AutoSize = True
        Me.lblCondicion3.Location = New System.Drawing.Point(32, 72)
        Me.lblCondicion3.Name = "lblCondicion3"
        Me.lblCondicion3.Size = New System.Drawing.Size(16, 16)
        Me.lblCondicion3.TabIndex = 7
        Me.lblCondicion3.Tag = ""
        Me.lblCondicion3.Text = "3:"
        Me.lblCondicion3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grPagos
        '
        '
        'grPagos.EmbeddedNavigator
        '
        Me.grPagos.EmbeddedNavigator.Name = ""
        Me.grPagos.Location = New System.Drawing.Point(15, 400)
        Me.grPagos.MainView = Me.grvPagos
        Me.grPagos.Name = "grPagos"
        Me.grPagos.Size = New System.Drawing.Size(643, 152)
        Me.grPagos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grPagos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagos.TabIndex = 2
        Me.grPagos.TabStop = False
        Me.grPagos.Text = "MMM/yyyy"
        '
        'grvPagos
        '
        Me.grvPagos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcMonto, Me.grcImporteCondicion, Me.grcFechaPago, Me.grcDescuento})
        Me.grvPagos.GridControl = Me.grPagos
        Me.grvPagos.Name = "grvPagos"
        Me.grvPagos.OptionsBehavior.Editable = False
        Me.grvPagos.OptionsCustomization.AllowFilter = False
        Me.grvPagos.OptionsCustomization.AllowGroup = False
        Me.grvPagos.OptionsCustomization.AllowSort = False
        Me.grvPagos.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.SortIndex = 0
        Me.grcPartida.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcPartida.VisibleIndex = 0
        Me.grcPartida.Width = 112
        '
        'grcMonto
        '
        Me.grcMonto.Caption = "Total"
        Me.grcMonto.DisplayFormat.FormatString = "c2"
        Me.grcMonto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcMonto.FieldName = "total"
        Me.grcMonto.Name = "grcMonto"
        Me.grcMonto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMonto.VisibleIndex = 2
        Me.grcMonto.Width = 102
        '
        'grcImporteCondicion
        '
        Me.grcImporteCondicion.Caption = "Importe"
        Me.grcImporteCondicion.DisplayFormat.FormatString = "c2"
        Me.grcImporteCondicion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteCondicion.FieldName = "totalxcondicion"
        Me.grcImporteCondicion.Name = "grcImporteCondicion"
        Me.grcImporteCondicion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteCondicion.VisibleIndex = 1
        Me.grcImporteCondicion.Width = 107
        '
        'grcFechaPago
        '
        Me.grcFechaPago.Caption = "Fecha de Pago"
        Me.grcFechaPago.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPago.FieldName = "fecha_pago"
        Me.grcFechaPago.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.grcFechaPago.Name = "grcFechaPago"
        Me.grcFechaPago.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaPago.VisibleIndex = 3
        Me.grcFechaPago.Width = 179
        '
        'grcDescuento
        '
        Me.grcDescuento.Caption = "Descuento"
        Me.grcDescuento.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcDescuento.FieldName = "descuento"
        Me.grcDescuento.Name = "grcDescuento"
        Me.grcDescuento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescuento.Width = 129
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.EditFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosPago, Me.nvrDatosGenerales, Me.nvDatosFlete})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 592)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 93
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosPago), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvDatosFlete)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrDatosPago
        '
        Me.nvrDatosPago.Caption = "Condiciones de Pago"
        Me.nvrDatosPago.LargeImage = CType(resources.GetObject("nvrDatosPago.LargeImage"), System.Drawing.Image)
        Me.nvrDatosPago.LargeImageIndex = 1
        Me.nvrDatosPago.Name = "nvrDatosPago"
        '
        'nvDatosFlete
        '
        Me.nvDatosFlete.Caption = "Datos del Flete"
        Me.nvDatosFlete.LargeImage = CType(resources.GetObject("nvDatosFlete.LargeImage"), System.Drawing.Image)
        Me.nvDatosFlete.Name = "nvDatosFlete"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.VisibleIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.clcTotalconFlete)
        Me.Panel1.Controls.Add(Me.clcIvaconFlete)
        Me.Panel1.Controls.Add(Me.clcSubTotalconFlete)
        Me.Panel1.Controls.Add(Me.clcTotalCostoUnitario)
        Me.Panel1.Controls.Add(Me.tmaEntradas)
        Me.Panel1.Controls.Add(Me.clcTotal)
        Me.Panel1.Controls.Add(Me.lkpOrden)
        Me.Panel1.Controls.Add(Me.lblFolio_Proveedor)
        Me.Panel1.Controls.Add(Me.lblReferencia)
        Me.Panel1.Controls.Add(Me.lblFolio)
        Me.Panel1.Controls.Add(Me.lblBodega)
        Me.Panel1.Controls.Add(Me.clcFolio)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.clcIva)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.txtObservaciones)
        Me.Panel1.Controls.Add(Me.chkCosteado)
        Me.Panel1.Controls.Add(Me.clcSubTotal)
        Me.Panel1.Controls.Add(Me.dteFecha)
        Me.Panel1.Controls.Add(Me.lblProveedor)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lkpBodega)
        Me.Panel1.Controls.Add(Me.lblObservaciones)
        Me.Panel1.Controls.Add(Me.lblFecha)
        Me.Panel1.Controls.Add(Me.ChkAbonoCuenta)
        Me.Panel1.Controls.Add(Me.grEntradas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.lkpProveedor)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.lkpFolioProveedor)
        Me.Panel1.Location = New System.Drawing.Point(120, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(968, 681)
        Me.Panel1.TabIndex = 1
        '
        'clcTotalconFlete
        '
        Me.clcTotalconFlete.Location = New System.Drawing.Point(536, 536)
        Me.clcTotalconFlete.Name = "clcTotalconFlete"
        '
        'clcTotalconFlete.Properties
        '
        Me.clcTotalconFlete.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotalconFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalconFlete.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotalconFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalconFlete.Properties.ReadOnly = True
        Me.clcTotalconFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalconFlete.Size = New System.Drawing.Size(96, 20)
        Me.clcTotalconFlete.TabIndex = 27
        Me.clcTotalconFlete.TabStop = False
        '
        'clcIvaconFlete
        '
        Me.clcIvaconFlete.Location = New System.Drawing.Point(536, 512)
        Me.clcIvaconFlete.Name = "clcIvaconFlete"
        '
        'clcIvaconFlete.Properties
        '
        Me.clcIvaconFlete.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIvaconFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIvaconFlete.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIvaconFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIvaconFlete.Properties.ReadOnly = True
        Me.clcIvaconFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIvaconFlete.Size = New System.Drawing.Size(96, 20)
        Me.clcIvaconFlete.TabIndex = 26
        Me.clcIvaconFlete.TabStop = False
        '
        'clcSubTotalconFlete
        '
        Me.clcSubTotalconFlete.Location = New System.Drawing.Point(536, 488)
        Me.clcSubTotalconFlete.Name = "clcSubTotalconFlete"
        '
        'clcSubTotalconFlete.Properties
        '
        Me.clcSubTotalconFlete.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotalconFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotalconFlete.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubTotalconFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubTotalconFlete.Properties.ReadOnly = True
        Me.clcSubTotalconFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubTotalconFlete.Size = New System.Drawing.Size(96, 20)
        Me.clcSubTotalconFlete.TabIndex = 25
        Me.clcSubTotalconFlete.TabStop = False
        '
        'clcTotalCostoUnitario
        '
        Me.clcTotalCostoUnitario.Location = New System.Drawing.Point(312, 504)
        Me.clcTotalCostoUnitario.Name = "clcTotalCostoUnitario"
        '
        'clcTotalCostoUnitario.Properties
        '
        Me.clcTotalCostoUnitario.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCostoUnitario.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCostoUnitario.Properties.Enabled = False
        Me.clcTotalCostoUnitario.Properties.ReadOnly = True
        Me.clcTotalCostoUnitario.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalCostoUnitario.Size = New System.Drawing.Size(112, 20)
        Me.clcTotalCostoUnitario.TabIndex = 20
        Me.clcTotalCostoUnitario.TabStop = False
        Me.clcTotalCostoUnitario.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtcondiciones_pago)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.tmaDescuentosProveedores)
        Me.Panel2.Controls.Add(Me.grDescuentosProveedores)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.grPagos)
        Me.Panel2.Location = New System.Drawing.Point(120, 28)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(968, 681)
        Me.Panel2.TabIndex = 1
        '
        'txtcondiciones_pago
        '
        Me.txtcondiciones_pago.EditValue = ""
        Me.txtcondiciones_pago.Location = New System.Drawing.Point(248, 48)
        Me.txtcondiciones_pago.Name = "txtcondiciones_pago"
        '
        'txtcondiciones_pago.Properties
        '
        Me.txtcondiciones_pago.Properties.MaxLength = 80
        Me.txtcondiciones_pago.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtcondiciones_pago.Size = New System.Drawing.Size(408, 48)
        Me.txtcondiciones_pago.TabIndex = 17
        Me.txtcondiciones_pago.Tag = "condiciones_pago"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(240, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(89, 16)
        Me.Label13.TabIndex = 16
        Me.Label13.Tag = ""
        Me.Label13.Text = "&Observaciones:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmaDescuentosProveedores
        '
        Me.tmaDescuentosProveedores.BackColor = System.Drawing.Color.White
        Me.tmaDescuentosProveedores.CanDelete = True
        Me.tmaDescuentosProveedores.CanInsert = True
        Me.tmaDescuentosProveedores.CanUpdate = True
        Me.tmaDescuentosProveedores.Grid = Me.grDescuentosProveedores
        Me.tmaDescuentosProveedores.Location = New System.Drawing.Point(15, 192)
        Me.tmaDescuentosProveedores.Name = "tmaDescuentosProveedores"
        Me.tmaDescuentosProveedores.Size = New System.Drawing.Size(643, 23)
        Me.tmaDescuentosProveedores.TabIndex = 0
        Me.tmaDescuentosProveedores.TabStop = False
        Me.tmaDescuentosProveedores.Title = "Descuentos"
        Me.tmaDescuentosProveedores.UpdateTitle = "un Descuento"
        '
        'grDescuentosProveedores
        '
        '
        'grDescuentosProveedores.EmbeddedNavigator
        '
        Me.grDescuentosProveedores.EmbeddedNavigator.Name = ""
        Me.grDescuentosProveedores.Location = New System.Drawing.Point(15, 216)
        Me.grDescuentosProveedores.MainView = Me.grvDescuentosProveedor
        Me.grDescuentosProveedores.Name = "grDescuentosProveedores"
        Me.grDescuentosProveedores.Size = New System.Drawing.Size(643, 152)
        Me.grDescuentosProveedores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDescuentosProveedores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.TabIndex = 1
        Me.grDescuentosProveedores.TabStop = False
        Me.grDescuentosProveedores.Text = "Cuentas Bancarias"
        '
        'grvDescuentosProveedor
        '
        Me.grvDescuentosProveedor.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveDescuento, Me.grcNombreDescuento, Me.grcPorcentaje, Me.grcAntesIVA, Me.grcProntoPago, Me.grcImporteDescuento})
        Me.grvDescuentosProveedor.GridControl = Me.grDescuentosProveedores
        Me.grvDescuentosProveedor.Name = "grvDescuentosProveedor"
        Me.grvDescuentosProveedor.OptionsBehavior.Editable = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowFilter = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowGroup = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowSort = False
        Me.grvDescuentosProveedor.OptionsView.ShowGroupPanel = False
        '
        'grcClaveDescuento
        '
        Me.grcClaveDescuento.Caption = "Descuento"
        Me.grcClaveDescuento.FieldName = "descuento"
        Me.grcClaveDescuento.Name = "grcClaveDescuento"
        Me.grcClaveDescuento.Width = 89
        '
        'grcNombreDescuento
        '
        Me.grcNombreDescuento.Caption = "Descripción"
        Me.grcNombreDescuento.FieldName = "nombre"
        Me.grcNombreDescuento.Name = "grcNombreDescuento"
        Me.grcNombreDescuento.SortIndex = 0
        Me.grcNombreDescuento.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcNombreDescuento.VisibleIndex = 0
        Me.grcNombreDescuento.Width = 246
        '
        'grcPorcentaje
        '
        Me.grcPorcentaje.Caption = "Porcentaje"
        Me.grcPorcentaje.DisplayFormat.FormatString = "n2"
        Me.grcPorcentaje.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorcentaje.FieldName = "porcentaje"
        Me.grcPorcentaje.Name = "grcPorcentaje"
        Me.grcPorcentaje.VisibleIndex = 3
        Me.grcPorcentaje.Width = 89
        '
        'grcAntesIVA
        '
        Me.grcAntesIVA.Caption = "Antes de IVA"
        Me.grcAntesIVA.FieldName = "antes_iva"
        Me.grcAntesIVA.Name = "grcAntesIVA"
        Me.grcAntesIVA.VisibleIndex = 1
        Me.grcAntesIVA.Width = 92
        '
        'grcProntoPago
        '
        Me.grcProntoPago.Caption = "Pronto Pago"
        Me.grcProntoPago.FieldName = "pronto_pago"
        Me.grcProntoPago.Name = "grcProntoPago"
        Me.grcProntoPago.VisibleIndex = 2
        Me.grcProntoPago.Width = 89
        '
        'grcImporteDescuento
        '
        Me.grcImporteDescuento.Caption = "Importe Descuento"
        Me.grcImporteDescuento.DisplayFormat.FormatString = "c2"
        Me.grcImporteDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteDescuento.FieldName = "importe_descuento"
        Me.grcImporteDescuento.Name = "grcImporteDescuento"
        Me.grcImporteDescuento.VisibleIndex = 4
        Me.grcImporteDescuento.Width = 113
        '
        'pnFlete
        '
        Me.pnFlete.Controls.Add(Me.grpFlete)
        Me.pnFlete.Location = New System.Drawing.Point(120, 28)
        Me.pnFlete.Name = "pnFlete"
        Me.pnFlete.Size = New System.Drawing.Size(968, 569)
        Me.pnFlete.TabIndex = 0
        '
        'grpFlete
        '
        Me.grpFlete.Controls.Add(Me.Label10)
        Me.grpFlete.Controls.Add(Me.clcTotalFlete)
        Me.grpFlete.Controls.Add(Me.lblTotal)
        Me.grpFlete.Controls.Add(Me.clcIvaRetenido)
        Me.grpFlete.Controls.Add(Me.lblIvaRemoto)
        Me.grpFlete.Controls.Add(Me.clcIVAFlete)
        Me.grpFlete.Controls.Add(Me.lblIva)
        Me.grpFlete.Controls.Add(Me.clcSubtotalFlete)
        Me.grpFlete.Controls.Add(Me.lblSubtotal)
        Me.grpFlete.Controls.Add(Me.clcManiobras)
        Me.grpFlete.Controls.Add(Me.lblManiobras)
        Me.grpFlete.Controls.Add(Me.clcSeguro)
        Me.grpFlete.Controls.Add(Me.lblSeguro)
        Me.grpFlete.Controls.Add(Me.clcFolioFactura)
        Me.grpFlete.Controls.Add(Me.dteFechaFactura)
        Me.grpFlete.Controls.Add(Me.lblFechaFactura)
        Me.grpFlete.Controls.Add(Me.lblFolioFactura)
        Me.grpFlete.Controls.Add(Me.clcOtrosGastos)
        Me.grpFlete.Controls.Add(Me.Label14)
        Me.grpFlete.Controls.Add(Me.clcFlete)
        Me.grpFlete.Controls.Add(Me.Label12)
        Me.grpFlete.Controls.Add(Me.chkFleteEnFactura)
        Me.grpFlete.Controls.Add(Me.lkpAcreedor)
        Me.grpFlete.Location = New System.Drawing.Point(16, 16)
        Me.grpFlete.Name = "grpFlete"
        Me.grpFlete.Size = New System.Drawing.Size(712, 536)
        Me.grpFlete.TabIndex = 0
        Me.grpFlete.TabStop = False
        Me.grpFlete.Tag = "total"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(93, 90)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 16)
        Me.Label10.TabIndex = 23
        Me.Label10.Tag = ""
        Me.Label10.Text = "Fletero:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotalFlete
        '
        Me.clcTotalFlete.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcTotalFlete.Location = New System.Drawing.Point(150, 280)
        Me.clcTotalFlete.Name = "clcTotalFlete"
        '
        'clcTotalFlete.Properties
        '
        Me.clcTotalFlete.Properties.DisplayFormat.FormatString = "c2"
        Me.clcTotalFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalFlete.Properties.Enabled = False
        Me.clcTotalFlete.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTotalFlete.Properties.MaxLength = 13
        Me.clcTotalFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcTotalFlete.Size = New System.Drawing.Size(98, 20)
        Me.clcTotalFlete.TabIndex = 20
        Me.clcTotalFlete.Tag = "total"
        Me.clcTotalFlete.ToolTip = "Total"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(104, 282)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 19
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIvaRetenido
        '
        Me.clcIvaRetenido.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcIvaRetenido.Location = New System.Drawing.Point(150, 256)
        Me.clcIvaRetenido.Name = "clcIvaRetenido"
        '
        'clcIvaRetenido.Properties
        '
        Me.clcIvaRetenido.Properties.DisplayFormat.FormatString = "c2"
        Me.clcIvaRetenido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIvaRetenido.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIvaRetenido.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcIvaRetenido.Properties.MaxLength = 13
        Me.clcIvaRetenido.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcIvaRetenido.Size = New System.Drawing.Size(98, 20)
        Me.clcIvaRetenido.TabIndex = 18
        Me.clcIvaRetenido.Tag = "iva_retenido"
        Me.clcIvaRetenido.ToolTip = "IVA Retenido"
        '
        'lblIvaRemoto
        '
        Me.lblIvaRemoto.AutoSize = True
        Me.lblIvaRemoto.Location = New System.Drawing.Point(59, 258)
        Me.lblIvaRemoto.Name = "lblIvaRemoto"
        Me.lblIvaRemoto.Size = New System.Drawing.Size(81, 16)
        Me.lblIvaRemoto.TabIndex = 17
        Me.lblIvaRemoto.Tag = ""
        Me.lblIvaRemoto.Text = "IVA Retenido:"
        Me.lblIvaRemoto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIVAFlete
        '
        Me.clcIVAFlete.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcIVAFlete.Location = New System.Drawing.Point(150, 232)
        Me.clcIVAFlete.Name = "clcIVAFlete"
        '
        'clcIVAFlete.Properties
        '
        Me.clcIVAFlete.Properties.DisplayFormat.FormatString = "c2"
        Me.clcIVAFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIVAFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIVAFlete.Properties.Enabled = False
        Me.clcIVAFlete.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcIVAFlete.Properties.MaxLength = 13
        Me.clcIVAFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcIVAFlete.Size = New System.Drawing.Size(98, 20)
        Me.clcIVAFlete.TabIndex = 16
        Me.clcIVAFlete.Tag = "iva"
        Me.clcIVAFlete.ToolTip = "IVA"
        '
        'lblIva
        '
        Me.lblIva.AutoSize = True
        Me.lblIva.Location = New System.Drawing.Point(111, 234)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(29, 16)
        Me.lblIva.TabIndex = 15
        Me.lblIva.Tag = ""
        Me.lblIva.Text = "IVA:"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSubtotalFlete
        '
        Me.clcSubtotalFlete.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSubtotalFlete.Location = New System.Drawing.Point(150, 208)
        Me.clcSubtotalFlete.Name = "clcSubtotalFlete"
        '
        'clcSubtotalFlete.Properties
        '
        Me.clcSubtotalFlete.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSubtotalFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotalFlete.Properties.EditFormat.FormatString = "n2"
        Me.clcSubtotalFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotalFlete.Properties.Enabled = False
        Me.clcSubtotalFlete.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSubtotalFlete.Properties.MaxLength = 13
        Me.clcSubtotalFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSubtotalFlete.Size = New System.Drawing.Size(98, 20)
        Me.clcSubtotalFlete.TabIndex = 14
        Me.clcSubtotalFlete.Tag = "subtotal"
        Me.clcSubtotalFlete.ToolTip = "Subtotal"
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(85, 210)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(55, 16)
        Me.lblSubtotal.TabIndex = 13
        Me.lblSubtotal.Tag = ""
        Me.lblSubtotal.Text = "Subtotal:"
        Me.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcManiobras
        '
        Me.clcManiobras.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcManiobras.Location = New System.Drawing.Point(150, 160)
        Me.clcManiobras.Name = "clcManiobras"
        '
        'clcManiobras.Properties
        '
        Me.clcManiobras.Properties.DisplayFormat.FormatString = "c2"
        Me.clcManiobras.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcManiobras.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcManiobras.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcManiobras.Properties.MaxLength = 13
        Me.clcManiobras.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcManiobras.Size = New System.Drawing.Size(98, 20)
        Me.clcManiobras.TabIndex = 10
        Me.clcManiobras.Tag = "maniobras"
        Me.clcManiobras.ToolTip = "Maniobras"
        '
        'lblManiobras
        '
        Me.lblManiobras.AutoSize = True
        Me.lblManiobras.Location = New System.Drawing.Point(75, 162)
        Me.lblManiobras.Name = "lblManiobras"
        Me.lblManiobras.Size = New System.Drawing.Size(65, 16)
        Me.lblManiobras.TabIndex = 9
        Me.lblManiobras.Tag = ""
        Me.lblManiobras.Text = "Maniobras:"
        Me.lblManiobras.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSeguro
        '
        Me.clcSeguro.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSeguro.Location = New System.Drawing.Point(150, 136)
        Me.clcSeguro.Name = "clcSeguro"
        '
        'clcSeguro.Properties
        '
        Me.clcSeguro.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSeguro.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSeguro.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSeguro.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSeguro.Properties.MaxLength = 13
        Me.clcSeguro.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSeguro.Size = New System.Drawing.Size(98, 20)
        Me.clcSeguro.TabIndex = 8
        Me.clcSeguro.Tag = "seguro"
        Me.clcSeguro.ToolTip = "Seguro"
        '
        'lblSeguro
        '
        Me.lblSeguro.AutoSize = True
        Me.lblSeguro.Location = New System.Drawing.Point(92, 138)
        Me.lblSeguro.Name = "lblSeguro"
        Me.lblSeguro.Size = New System.Drawing.Size(48, 16)
        Me.lblSeguro.TabIndex = 7
        Me.lblSeguro.Tag = ""
        Me.lblSeguro.Text = "Seguro:"
        Me.lblSeguro.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioFactura
        '
        Me.clcFolioFactura.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioFactura.Location = New System.Drawing.Point(150, 40)
        Me.clcFolioFactura.Name = "clcFolioFactura"
        '
        'clcFolioFactura.Properties
        '
        Me.clcFolioFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioFactura.Properties.MaskData.EditMask = "#########0"
        Me.clcFolioFactura.Properties.MaxLength = 13
        Me.clcFolioFactura.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioFactura.Size = New System.Drawing.Size(98, 20)
        Me.clcFolioFactura.TabIndex = 2
        Me.clcFolioFactura.Tag = "folio_factura"
        Me.clcFolioFactura.ToolTip = "Folio de la Factura"
        '
        'dteFechaFactura
        '
        Me.dteFechaFactura.EditValue = New Date(2007, 4, 10, 0, 0, 0, 0)
        Me.dteFechaFactura.Location = New System.Drawing.Point(150, 64)
        Me.dteFechaFactura.Name = "dteFechaFactura"
        '
        'dteFechaFactura.Properties
        '
        Me.dteFechaFactura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFactura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Size = New System.Drawing.Size(98, 23)
        Me.dteFechaFactura.TabIndex = 2
        Me.dteFechaFactura.Tag = "fecha_factura"
        Me.dteFechaFactura.ToolTip = "Fecha de la Factura"
        '
        'lblFechaFactura
        '
        Me.lblFechaFactura.AutoSize = True
        Me.lblFechaFactura.Location = New System.Drawing.Point(54, 66)
        Me.lblFechaFactura.Name = "lblFechaFactura"
        Me.lblFechaFactura.Size = New System.Drawing.Size(86, 16)
        Me.lblFechaFactura.TabIndex = 1
        Me.lblFechaFactura.Tag = ""
        Me.lblFechaFactura.Text = "Fecha Factura:"
        Me.lblFechaFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolioFactura
        '
        Me.lblFolioFactura.AutoSize = True
        Me.lblFolioFactura.Location = New System.Drawing.Point(60, 42)
        Me.lblFolioFactura.Name = "lblFolioFactura"
        Me.lblFolioFactura.Size = New System.Drawing.Size(80, 16)
        Me.lblFolioFactura.TabIndex = 0
        Me.lblFolioFactura.Tag = ""
        Me.lblFolioFactura.Text = "Folio Factura:"
        Me.lblFolioFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOtrosGastos
        '
        Me.clcOtrosGastos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcOtrosGastos.Location = New System.Drawing.Point(150, 184)
        Me.clcOtrosGastos.Name = "clcOtrosGastos"
        '
        'clcOtrosGastos.Properties
        '
        Me.clcOtrosGastos.Properties.DisplayFormat.FormatString = "c2"
        Me.clcOtrosGastos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOtrosGastos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOtrosGastos.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOtrosGastos.Properties.MaxLength = 13
        Me.clcOtrosGastos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcOtrosGastos.Size = New System.Drawing.Size(98, 20)
        Me.clcOtrosGastos.TabIndex = 12
        Me.clcOtrosGastos.Tag = "otros_gastos"
        Me.clcOtrosGastos.ToolTip = "Otros Gastos"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(59, 186)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 16)
        Me.Label14.TabIndex = 11
        Me.Label14.Tag = ""
        Me.Label14.Text = "Otros Gastos:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFlete
        '
        Me.clcFlete.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFlete.Location = New System.Drawing.Point(150, 112)
        Me.clcFlete.Name = "clcFlete"
        '
        'clcFlete.Properties
        '
        Me.clcFlete.Properties.DisplayFormat.FormatString = "c2"
        Me.clcFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFlete.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFlete.Properties.MaxLength = 13
        Me.clcFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFlete.Size = New System.Drawing.Size(98, 20)
        Me.clcFlete.TabIndex = 6
        Me.clcFlete.Tag = "flete"
        Me.clcFlete.ToolTip = "Flete"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(104, 114)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(36, 16)
        Me.Label12.TabIndex = 5
        Me.Label12.Tag = ""
        Me.Label12.Text = "Flete:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkFleteEnFactura
        '
        Me.chkFleteEnFactura.Location = New System.Drawing.Point(150, 304)
        Me.chkFleteEnFactura.Name = "chkFleteEnFactura"
        '
        'chkFleteEnFactura.Properties
        '
        Me.chkFleteEnFactura.Properties.Caption = "FLETE INCLUIDO EN LA FACTURA"
        Me.chkFleteEnFactura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkFleteEnFactura.Size = New System.Drawing.Size(224, 19)
        Me.chkFleteEnFactura.TabIndex = 21
        Me.chkFleteEnFactura.Tag = "flete_incluido"
        Me.chkFleteEnFactura.ToolTip = "Flete Incluido en la Factura"
        '
        'lkpAcreedor
        '
        Me.lkpAcreedor.AllowAdd = False
        Me.lkpAcreedor.AutoReaload = False
        Me.lkpAcreedor.DataSource = Nothing
        Me.lkpAcreedor.DefaultSearchField = ""
        Me.lkpAcreedor.DisplayMember = "nombre"
        Me.lkpAcreedor.EditValue = Nothing
        Me.lkpAcreedor.Filtered = False
        Me.lkpAcreedor.InitValue = Nothing
        Me.lkpAcreedor.Location = New System.Drawing.Point(150, 88)
        Me.lkpAcreedor.MultiSelect = False
        Me.lkpAcreedor.Name = "lkpAcreedor"
        Me.lkpAcreedor.NullText = ""
        Me.lkpAcreedor.PopupWidth = CType(400, Long)
        Me.lkpAcreedor.ReadOnlyControl = False
        Me.lkpAcreedor.Required = False
        Me.lkpAcreedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAcreedor.SearchMember = ""
        Me.lkpAcreedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAcreedor.SelectAll = False
        Me.lkpAcreedor.Size = New System.Drawing.Size(264, 20)
        Me.lkpAcreedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAcreedor.TabIndex = 4
        Me.lkpAcreedor.Tag = "acreedor"
        Me.lkpAcreedor.ToolTip = "Numero de Acreedor"
        Me.lkpAcreedor.ValueMember = "acreedor"
        '
        'tbrEtiquetas
        '
        Me.tbrEtiquetas.Text = "Etiquetas"
        '
        'frmEntradas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 600)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.pnFlete)
        Me.MasterControl = Me.tmaEntradas
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmEntradas"
        Me.Controls.SetChildIndex(Me.pnFlete, 0)
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPedimento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAduana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFecha_Importacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkImportacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCosteado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChkAbonoCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcPlazo5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPagos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPagos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.clcTotalconFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIvaconFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubTotalconFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalCostoUnitario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.txtcondiciones_pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnFlete.ResumeLayout(False)
        Me.grpFlete.ResumeLayout(False)
        CType(Me.clcTotalFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIvaRetenido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIVAFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubtotalFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcManiobras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSeguro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcOtrosGastos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFleteEnFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGrupos As VillarrealBusiness.clsGruposArticulos
    Private oEntradas As VillarrealBusiness.clsEntradas
    Private oEntradasdetalle As New VillarrealBusiness.clsEntradasDetalles
    Private oEntradasProgramacionPagos As VillarrealBusiness.clsEntradasProgramacionPagos

    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oBodegas As VillarrealBusiness.clsBodegas

    Private oOrdenesCompra As VillarrealBusiness.clsOrdenesCompra
    Private oOrdenesCompraDetalle As New VillarrealBusiness.clsOrdenesCompraDetalle

    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oArticulosPrecios As New VillarrealBusiness.clsArticulosPrecios
    Private oHistoricoArticulosPrecios As New VillarrealBusiness.clsHisArticulosPrecios
    Private ovariables As New VillarrealBusiness.clsVariables

    Private oMovimientos As New VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosDetalleSeries As New VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oHistoricoCostos As New VillarrealBusiness.clsHisCostos
    Private oReportes As New VillarrealBusiness.Reportes

    Private oVentas As New VillarrealBusiness.clsVentas
    Private oAcreedores As VillarrealBusiness.clsAcreedores
    Private oAcreedoresDiversos As VillarrealBusiness.clsAcreedoresDiversos
    Private oProveedoresdescuentos As VillarrealBusiness.clsProveedoresDescuentos
    Private oEntradasProveedorDescuentos As VillarrealBusiness.clsEntradasProveedorDescuentos
    Private oEntradasDetalleprecios As VillarrealBusiness.clsEntradasDetallePrecios

    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques

    Private dTotalConsiderandoDescuentos As Double
    Private dSubtotalConsiderandoDescuentos As Double
    Private dIvaConsiderandoDescuentos As Double
    Private bReciboImpreso As Boolean

    Private ImpuestoGuardado As Double = 0



    Public ReadOnly Property Proveedor() As Long
        Get
            Return PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Private ReadOnly Property Orden() As Long
        Get
            Return PreparaValorLookup(Me.lkpOrden)
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get

            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
    Private ReadOnly Property ConceptoEntrada() As String
        Get
            Return ovariables.TraeDatos("concepto_compra", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property ConceptoCancelacionEntrada() As String
        Get
            Return ovariables.TraeDatos("concepto_cancelacion_compra", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Concepto_cxp() As String
        Get
            Return ovariables.TraeDatos("concepto_cxp_nota_entrada", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Impuesto() As Double
        Get

            If Me.ImpuestoGuardado = -1 Then
                Return 0
            Else
                Return (ImpuestoGuardado / 100)
            End If


            'If ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float) = -1 Then
            '    Return 0
            'Else
            '    Return (CType(ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double) / 100)
            'End If


        End Get
    End Property
    Private ReadOnly Property MaximoPlazoCondicion() As Long
        Get
            Return ObtenerMaximoPlazoCondicion()
        End Get
    End Property

    Private ReadOnly Property Acreedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAcreedor)
        End Get
    End Property
    Public ReadOnly Property Vista_Descuentos() As DataView
        Get
            Return Me.grvDescuentosProveedor.DataSource
        End Get
    End Property
    Private Property TotalConsiderandoDescuentos() As Double
        Get
            Return dTotalConsiderandoDescuentos
        End Get
        Set(ByVal Value As Double)
            dTotalConsiderandoDescuentos = Value
        End Set
    End Property
    Private Property SubtotalConsiderandoDescuentos() As Double
        Get
            Return dSubtotalConsiderandoDescuentos
        End Get
        Set(ByVal Value As Double)
            dSubtotalConsiderandoDescuentos = Value
        End Set
    End Property
    Private Property IvaConsiderandoDescuentos() As Double
        Get
            Return dIvaConsiderandoDescuentos

        End Get
        Set(ByVal Value As Double)
            dIvaConsiderandoDescuentos = Value
        End Set
    End Property

    Private OC_Sobre_pedido As Boolean = False
    Private Folio_Poliza_Cheque As Long = 0

    Public Property ArticulosPrecios(ByVal Indice As Integer) As ArticulosPreciosCostos
        Get
            Dim AuxArticulos As ArticulosPreciosCostos
            For i As Integer = 0 To EstructuraPrecios.Count - 1
                AuxArticulos = EstructuraPrecios(i)
                'revisa si existe
                If AuxArticulos.Indice = Indice Then
                    Return AuxArticulos
                    Exit For
                End If
            Next
        End Get
        Set(ByVal Value As ArticulosPreciosCostos)
            If Not ExisteArticulo(Value.Indice) Then
                EstructuraPrecios.Add(Value)
            Else
                EstructuraPrecios(IndexArticulo(Value.Indice)) = Value
            End If
        End Set
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEntradas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmEntradas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        Select Case Action
            Case Actions.Insert
                ImprimirReporte()
        End Select
    End Sub
    Private Sub frmEntradas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmEntradas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Try


            oEntradas = New VillarrealBusiness.clsEntradas
            oProveedores = New VillarrealBusiness.clsProveedores
            oBodegas = New VillarrealBusiness.clsBodegas
            oOrdenesCompra = New VillarrealBusiness.clsOrdenesCompra
            oAcreedores = New VillarrealBusiness.clsAcreedores
            oAcreedoresDiversos = New VillarrealBusiness.clsAcreedoresDiversos
            oProveedoresdescuentos = New VillarrealBusiness.clsProveedoresDescuentos
            oEntradasProveedorDescuentos = New VillarrealBusiness.clsEntradasProveedorDescuentos
            oEntradasProgramacionPagos = New VillarrealBusiness.clsEntradasProgramacionPagos
            oEntradasDetalleprecios = New VillarrealBusiness.clsEntradasDetallePrecios
            oDepartamentos = New VillarrealBusiness.clsDepartamentos
            oGrupos = New VillarrealBusiness.clsGruposArticulos
            oPolizasCheques = New VillarrealBusiness.clsPolizasCheques

            EstructuraPrecios = New ArrayList

            Me.ImpuestoGuardado = ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float)

            With Me.tmaEntradas
                .UpdateTitle = "un Artículo"
                .UpdateForm = New frmEntradasDetalles
                .AddColumn("articulo", "System.Int32")
                .AddColumn("descripcion")
                .AddColumn("cantidad", "System.Int32")
                .AddColumn("cantidad_pedida", "System.Int32")
                .AddColumn("costo_pedido", "System.Double")
                .AddColumn("costo", "System.Double")
                .AddColumn("folio_historico_costo", "System.Int32")
                .AddColumn("cantidad_anterior", "System.Int32")
                .AddColumn("saldo", "System.Int32")
                .AddColumn("orden", "System.Int32")
                .AddColumn("sobre_pedido")
                .AddColumn("importe", "System.Double")
                .AddColumn("departamento", "System.Double")
                .AddColumn("grupo", "System.Double")
                .AddColumn("costo_flete", "System.Double")
                .AddColumn("precio_lista", "System.Double")
                .AddColumn("importe_con_flete", "System.Double")
                .AddColumn("bprecio_lista", "System.Boolean")
                .AddColumn("modelo")
                .AddColumn("modificado", "System.Boolean")
            End With

            With Me.tmaDescuentosProveedores
                .UpdateTitle = " un Descuento"
                .UpdateForm = New Comunes.frmProveedoresDescuentos ' frmProveedoresDescuentos
                .AddColumn("descuento", "System.Int32")
                .AddColumn("nombre", "System.String")
                .AddColumn("porcentaje", "System.Double")
                .AddColumn("antes_iva", "System.Boolean")
                .AddColumn("pronto_pago", "System.Boolean")
                .AddColumn("importe_descuento", "System.Double")
            End With


            Select Case Action
                Case Actions.Insert
                    Me.tbrTools.Buttons(2).Visible = False
                    Me.tbrTools.Buttons(3).Visible = False
                    Me.tbrTools.Buttons(2).Enabled = False
                    Me.tbrTools.Buttons(3).Enabled = False
                Case Actions.Update
                    Me.lkpBodega.Enabled = False
                    Me.lkpOrden.Enabled = False
                    Me.lkpFolioProveedor.Enabled = False
                    Me.lkpProveedor.Enabled = False
                Case Actions.Delete
                    Me.tbrTools.Buttons(2).Enabled = False
                    Me.tbrTools.Buttons(3).Enabled = False

            End Select
            Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
            Me.dteFecha_Importacion.EditValue = ""


        Catch ex As Exception

        End Try


        dteFecha.SelectAll()
    End Sub
    Private Sub frmEntradas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oEntradas.Insertar(Me.DataSource)
            Case Actions.Update
                Response = oEntradas.Actualizar(Me.DataSource)

            Case Actions.Delete
                If Me.chkCosteado.Checked Then

                    Response = oEntradas.EliminarCosteada(clcFolio.Value)
                    If Not Response.ErrorFound Then
                        Dim resultado As String = CType(Response.Value, DataSet).Tables(0).Rows(0).Item("resultado")
                        If resultado.Trim.Length > 0 Then
                            Response.Message = resultado

                        End If
                    End If
                Else
                    Response = oEntradas.Eliminar(clcFolio.Value)
                    If Not Response.ErrorFound And Orden > 0 Then
                        'Cambio el Status a la orden de compra
                        Response = Me.oOrdenesCompra.CambiarStatus(Orden)
                    End If
                End If

        End Select
    End Sub
    Private Sub frmEntradas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim oDataSet As DataSet
        Dim data_articulos As DataSet
        bdespliega = True
        Response = oEntradas.DespliegaDatos(OwnerForm.Value("entrada"))

        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.lkpOrden.EditValue = oDataSet.Tables(0).Rows(0).Item("orden")
            Me.DataSource = oDataSet
            Me.folio_movimiento = oDataSet.Tables(0).Rows(0).Item("folio_movimiento")
            OC_Sobre_pedido = oDataSet.Tables(0).Rows(0).Item("sobre_pedido")

            bcosteado = oDataSet.Tables(0).Rows(0).Item("costeado")
            Me.chkCosteado.Checked = bcosteado
            If bcosteado Then
                Select Case Action
                    Case Actions.Update
                        Me.tbrTools.Buttons(3).Enabled = False
                        Me.tbrTools.Buttons(0).Enabled = False
                        ' Desactivo los controles de Condiciones y DPP si ya esta costeada la entrada
                        DesactivarControles()

                        'DAM - Se comento el Delete para usar el boton de eliminar por peticion de Laura Gomez 03/03/2013
                        'Case Actions.Delete
                        '    Me.tbrTools.Buttons(0).Enabled = False
                End Select

            End If
        End If

        If Not Response.ErrorFound Then
            Response = Me.oEntradasdetalle.Listado(OwnerForm.Value("entrada"))
            data_articulos = Response.Value
        End If

        If Not Response.ErrorFound Then Response = DespliegaPrecios(data_articulos)
        bdespliega = False
        If Me.txtAduana.Text <> "" Or Me.txtPedimento.Text <> "" Then
            Me.chkImportacion.Checked = True
        End If

        CalculaTotales()
        CambiaForma()


        If Not Response.ErrorFound Then
            Response = oEntradasProveedorDescuentos.DespliegaDatos(Me.clcFolio.EditValue)
            oDataSet = Response.Value
            Me.tmaDescuentosProveedores.DataSource = oDataSet

        End If

        If Not Response.ErrorFound Then
            Response = Me.oEntradasProgramacionPagos.DespliegaDatos(Me.clcFolio.EditValue)   'oEntradasdetalle.Listado(OwnerForm.Value("entrada"))
            Me.grPagos.DataSource = CType(Response.Value, DataSet).Tables(0)
            'data_articulos = Response.Value
        End If

        oDataSet = Nothing
    End Sub
    Private Sub frmEntradas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        Me.tmaEntradas.MoveFirst()

        If Action = Actions.Delete And bcosteado = True Then Exit Sub
        '    'si es una cancelacion primero tengo q agregar el encabezado del movimiento
        '    'Inserta movimiento de Cancelación
        '    GeneraMovimientoInventario(Action, Response, Comunes.Common.Sucursal_Actual, Bodega, ConceptoCancelacionEntrada, folio_movimiento, Me.dteFecha.DateTime, SucursalOC, concepto_nulo, Orden, "Movimiento de Cancelaciòn generado desde entradas al almacen")
        'End If

        Do While Not tmaEntradas.EOF
            If tmaEntradas.Item("cantidad") > 0 Then

                Select Case tmaEntradas.CurrentAction
                    Case Actions.Insert

                        If Not Response.ErrorFound Then
                            ' Inserto el Detalle de Entradas
                            Response = Me.oEntradasdetalle.Insertar(Me.tmaEntradas.SelectedRow, Me.clcFolio.Value, Bodega)
                        End If

                        If Not Response.ErrorFound Then
                            Dim Aux As ArticulosPreciosCostos
                            Dim iRow As Integer
                            ' Inserto el Detalle de Precios de las Entradas
                            Aux = ArticulosPrecios(tmaEntradas.Item("partida"))
                            For Each oRow As DataRow In Aux.Precios.Tables(0).Rows
                                Response = oEntradasDetalleprecios.Insertar(oRow, Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"))
                            Next
                        End If

                    Case Actions.Update
                        ' Actualizo el Detalle de Entradas
                        Response = oEntradasdetalle.Actualizar(Me.tmaEntradas.SelectedRow, Me.clcFolio.Value, Bodega, False)
                        If Not Response.ErrorFound Then
                            Dim Aux As ArticulosPreciosCostos
                            Dim iRow As Integer
                            ' Inserto el Detalle de Precios de las Entradas
                            Aux = ArticulosPrecios(tmaEntradas.Item("partida"))
                            For Each oRow As DataRow In Aux.Precios.Tables(0).Rows
                                Response = oEntradasDetalleprecios.Actualizar(oRow, Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"))
                            Next
                        End If
                    Case Actions.Delete
                        'If Me.chkCosteado.EditValue Then
                        '    If Me.Action = Actions.Delete Then
                        '        ' Inserto el Detalle del Movimiento de la Cancelacion de la Entrada
                        '        Response = Me.oMovimientosDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Me.ConceptoCancelacionEntrada, folio_movimiento, Me.dteFecha.DateTime, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"), tmaEntradas.Item("cantidad"), tmaEntradas.Item("costo"), CType(tmaEntradas.Item("cantidad") * tmaEntradas.Item("costo"), Double), tmaEntradas.Item("folio_historico_costo"), 0, System.DBNull.Value)

                        '        If Not Response.ErrorFound Then Response = Me.oMovimientosDetalleSeries.InsertarSeriesEntradas(Comunes.Common.Sucursal_Actual, Bodega, ConceptoCancelacionEntrada, folio_movimiento, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"), Me.clcFolio.Value, tmaEntradas.Item("partida"))

                        '    End If

                        '    ' ACTUALIZO EL HISTORICO DEL ARTICULO A CERO COMO CANCELACION DE SU ENTRADA
                        '    If Not Response.ErrorFound Then Response = Me.oHistoricoCostos.Actualizar(tmaEntradas.Item("folio_historico_costo"), FechaconHoras, tmaEntradas.Item("articulo"), Bodega, tmaEntradas.Item("cantidad"), tmaEntradas.Item("costo_flete"), 0)
                        'End If
                End Select
                If Response.ErrorFound Then Exit Sub
            End If

            tmaEntradas.MoveNext()
        Loop

        If Not Response.ErrorFound Then
            Try
                With Me.tmaDescuentosProveedores
                    .MoveFirst()
                    Do While Not .EOF
                        Select Case .CurrentAction
                            Case Actions.Insert
                                Response = oEntradasProveedorDescuentos.Insertar(Me.clcFolio.EditValue, .SelectedRow)
                            Case Actions.Update
                                Response = oEntradasProveedorDescuentos.Actualizar(Me.clcFolio.EditValue, .SelectedRow)
                            Case Actions.Delete
                                Response = oEntradasProveedorDescuentos.Eliminar(Me.clcFolio.EditValue, .SelectedRow)

                        End Select
                        .MoveNext()
                    Loop
                End With
            Catch ex As Exception
            End Try
        End If

        GuardaProgramacionPagos()

    End Sub
    Private Sub frmEntradas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oEntradas.Validacion(Action, Proveedor, Me.Orden, Bodega, Me.tmaEntradas.Count, Me.tmaEntradas.DataSource, Me.dteFecha.EditValue, Me.txtReferencia.Text, Me.clcFolioFactura.EditValue, Me.dteFechaFactura.Text, Me.clcFlete.EditValue, Me.clcSeguro.EditValue, Me.clcManiobras.EditValue, Me.clcOtrosGastos.EditValue, Me.clcSubtotalFlete.EditValue, Me.clcIVAFlete.EditValue, Me.clcIvaRetenido.EditValue, Me.clcTotalFlete.EditValue)
        If Response.ErrorFound Then Exit Sub

        If Me.Impuesto = 0 Then
            If ShowMessage(MessageType.MsgQuestion, "El impuesto no esta definido o es igual a 0 (cero), ¿Deseas Continuar ?", "Atención") = Answer.MsgNo Then
                Response.Message = "Impuesto No definido"
            End If
        End If
    End Sub
    Private Sub frmEntradas_Localize() Handles MyBase.Localize
        Find("entrada", CType(Me.clcFolio.Value, Object))
    End Sub
    Private Sub frmEntradas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If Me.Action = Actions.Delete Then
            Me.tmaDescuentosProveedores.Refresh()
            'me.grDescuentosProveedores.RefreshDataSource()

        End If

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim response As Events
        If Me.Action = Actions.Insert Then
            response = oProveedores.Lookup(True, True)
        Else
            response = oProveedores.Lookup()
        End If
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
        If Me.lkpProveedor.DataSource Is Nothing Or Me.lkpProveedor.EditValue Is Nothing Then Exit Sub
        lkpOrden_LoadData(True)
        lkpFolioProveedor_LoadData(True)
        If Me.lkpProveedor.EditValue > 0 Then

            LlenarGrid()
        End If
    End Sub

    Private Sub lkpOrden_LoadData(ByVal Initialize As Boolean) Handles lkpOrden.LoadData
        Dim response As Events
        response = oOrdenesCompra.Lookup(Proveedor)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpOrden.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpOrden_Format() Handles lkpOrden.Format
        modFormatos.for_ordenes_compra_grl(Me.lkpOrden)
    End Sub
    Private Sub lkpOrden_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpOrden.EditValueChanged
        If Me.lkpOrden.DataSource Is Nothing Or Me.lkpOrden.EditValue Is Nothing Then Exit Sub
        Me.lkpProveedor.EditValue = Me.lkpOrden.GetValue("proveedor")
        Me.lkpFolioProveedor.EditValue = Me.lkpOrden.GetValue("folio_proveedor")
        Me.lkpBodega.EditValue = CType(Me.lkpOrden.GetValue("bodega"), String)
        Me.SucursalOC = Me.lkpOrden.GetValue("sucursal")
        OC_Sobre_pedido = Me.lkpOrden.GetValue("sobre_pedido")

        Me.clcPlazo1.EditValue = Me.lkpOrden.GetValue("plazo_pago_1")
        Me.clcPlazo2.EditValue = Me.lkpOrden.GetValue("plazo_pago_2")
        Me.clcPlazo3.EditValue = Me.lkpOrden.GetValue("plazo_pago_3")
        Me.clcPlazo4.EditValue = Me.lkpOrden.GetValue("plazo_pago_4")
        Me.clcPlazo5.EditValue = Me.lkpOrden.GetValue("plazo_pago_5")

        LlenarGridPagos()

        If bdespliega = False Then
            Me.txtcondiciones_pago.Text = Me.lkpOrden.GetValue("condiciones")
            ObtenerDetalleOrden(Orden)
        End If

        Me.CalculaTotales()
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events

        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        If Me.lkpBodega.DataSource Is Nothing Or Me.lkpBodega.EditValue Is Nothing Then Exit Sub
    End Sub

    Private Sub lkpFolioProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpFolioProveedor.LoadData
        Dim response As Events
        Me.lkpFolioProveedor.EditValue = Nothing
        response = oOrdenesCompra.Lookup(Proveedor)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolioProveedor.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpFolioProveedor_Format() Handles lkpFolioProveedor.Format
        modFormatos.for_folio_proveeedor_ordenes_compra_grl(Me.lkpFolioProveedor)
    End Sub
    Private Sub lkpFolioProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolioProveedor.EditValueChanged
        If Me.lkpFolioProveedor.DataSource Is Nothing Or Me.lkpFolioProveedor.EditValue Is Nothing Then Exit Sub
        lkpOrden_LoadData(True)
        Me.lkpOrden.EditValue = Me.lkpFolioProveedor.GetValue("orden")
    End Sub

    Private Sub dteFecha_Importacion_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha_Importacion.EditValueChanged
        If Me.dteFecha_Importacion.Text.Length = 0 Then
            Me.dteFecha_Importacion.EditValue = System.DBNull.Value
        End If
    End Sub
    Private Sub chkImportacion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImportacion.CheckedChanged
        If Action <> Actions.Delete Then
            If chkImportacion.Checked = True Then
                Me.txtPedimento.Enabled = True
                Me.txtAduana.Enabled = True
                Me.dteFecha_Importacion.Enabled = True
                Me.dteFecha_Importacion.EditValue = CDate(TINApp.FechaServidor)
            Else
                Me.txtPedimento.Text = ""
                Me.txtAduana.Text = ""
                Me.dteFecha_Importacion.EditValue = ""
                Me.txtPedimento.Enabled = False
                Me.txtAduana.Enabled = False
                Me.dteFecha_Importacion.Enabled = False
            End If
        End If
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbrReimprimir Then
            ImprimirReporte()



        ElseIf e.Button Is Me.tbrCostear Then
            Dim Response As New Events
            Costear(Response)

            If Not Response.ErrorFound Then
                Me.Close()
            Else
                Response.ShowMessage()
            End If
        ElseIf e.Button Is Me.tbrEtiquetas Then
            Dim Response As Events
            Dim campos(1) As String
            campos(0) = "articulo"
            campos(1) = "cantidad"

            Response = oReportes.ImpresionEtiquetasPrecios2(Me.clcFolio.Value)
            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New Comunes.rptEtiquetas
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Impresión de Etiquetas de Precios", oReport, , , , True)
                    oReport = Nothing
                End If
                oDataSet = Nothing
            End If
        End If
    End Sub
    Private Sub txtReferencia_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtReferencia.KeyPress
        If e.KeyChar = " " Then
            e.Handled = True
        End If
    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrDatosPago_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosPago.LinkPressed
        Me.Panel2.BringToFront()
    End Sub
    Private Sub nvDatosFlete_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvDatosFlete.LinkPressed
        Me.pnFlete.BringToFront()
    End Sub

    Private Sub clcPlazos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcPlazo1.EditValueChanged, clcPlazo2.EditValueChanged, clcPlazo3.EditValueChanged, clcPlazo4.EditValueChanged, clcPlazo5.EditValueChanged
        If IsNumeric(CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue) Then 'And Not VerificaEditarCondicion(sender) Then
            If CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue > 0 Then
                If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

                    If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                        If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                            Me.LlenarGridPagos()
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub clcPlazos_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcPlazo1.Validating, clcPlazo2.Validating, clcPlazo3.Validating, clcPlazo4.Validating, clcPlazo5.Validating

        Dim Control As DevExpress.XtraEditors.CalcEdit = CType(sender, DevExpress.XtraEditors.CalcEdit)
        Select Case Control.Name
            Case "clcPlazo2"
                If Control.Value <= Me.clcPlazo1.Value Then
                    Control.Value = 0
                End If
            Case "clcPlazo3"
                If Control.Value <= Me.clcPlazo2.Value Or Me.clcPlazo2.Value = 0 Then
                    Control.Value = 0
                End If

            Case "clcPlazo4"
                If Control.Value <= Me.clcPlazo3.Value Or Me.clcPlazo3.Value = 0 Then
                    Control.Value = 0
                End If
            Case "clcPlazo5"
                If Control.Value <= Me.clcPlazo4.Value Or Me.clcPlazo4.Value = 0 Then
                    Control.Value = 0
                End If
        End Select

        'If CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue > 0 Then
        If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then
            If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                    Me.LlenarGridPagos()
                End If
            End If
        End If
    End Sub
    Private Sub clcPlazos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcPlazo1.Validated, clcPlazo2.Validated, clcPlazo3.Validated, clcPlazo4.Validated

        If clcPlazo1.Value = 0 Then
            clcPlazo2.EditValue = 0
        End If
        If clcPlazo2.Value = 0 Then
            clcPlazo3.EditValue = 0
        End If
        If clcPlazo3.Value = 0 Then
            clcPlazo4.EditValue = 0
        End If
        If clcPlazo4.Value = 0 Then
            clcPlazo5.EditValue = 0
        End If


        If IsNumeric(clcPlazo1.Value) Then
            If clcPlazo1.Value > 0 Then
                If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

                    If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                        If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                            Me.LlenarGridPagos()
                        End If
                    End If
                End If
            Else
                borrarGridPagos()
            End If

        Else
            borrarGridPagos()
        End If

    End Sub

    Private Sub lkpAcreedor_Format() Handles lkpAcreedor.Format
        Comunes.clsFormato.for_acreedores_grl(Me.lkpAcreedor)
    End Sub
    Private Sub lkpAcreedor_LoadData(ByVal Initialize As Boolean) Handles lkpAcreedor.LoadData
        Dim response As Events
        response = Me.oAcreedores.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpAcreedor.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub chkFleteEnFactura_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFleteEnFactura.CheckedChanged
        If Me.chkFleteEnFactura.Checked = True Then
            Me.lkpAcreedor.EditValue = Nothing
            Me.lkpAcreedor.Enabled = False
        Else
            Me.lkpAcreedor.Enabled = True
        End If
    End Sub

    Private Sub clcFletes_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcFlete.EditValueChanged, clcSeguro.EditValueChanged, clcManiobras.EditValueChanged, clcOtrosGastos.EditValueChanged, clcIvaRetenido.EditValueChanged
        If IsNumeric(CType(sender, DevExpress.XtraEditors.CalcEdit).EditValue) Then
            CalculaTotalesFlete()
        End If
    End Sub

    'Private Sub clcFletes_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcFlete.Validating, clcSeguro.Validating, clcManiobras.Validating, clcOtrosGastos.Validating, clcIvaRetenido.Validating
    '    With CType(sender, DevExpress.XtraEditors.CalcEdit)
    '        If IsNumeric(.EditValue) Then
    '            'If .EditValue > 100 Then
    '            '    .EditValue = 100
    '            '    e.Cancel = True
    '            'End If
    '        Else
    '            .EditValue = 0
    '        End If
    '    End With
    'End Sub

    Private Sub clcTotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcTotal.EditValueChanged
        If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(Me.clcTotal.EditValue) Then

            Select Case Action

                Case Actions.Insert
                    If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 Then
                        Me.LlenarGridPagos()

                        CalculaProntoPago()
                    End If


                Case Actions.Update
                    If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 Then
                        Me.LlenarGridPagos()

                        CalculaProntoPago()
                    End If
                Case Actions.Delete

            End Select
        End If

    End Sub

    Private Sub tmaDescuentosProveedores_DataSourceChanged() Handles tmaDescuentosProveedores.DataSourceChanged
        If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

            If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                    Me.LlenarGridPagos()
                End If
            End If
        End If

        CalculaProntoPago()

    End Sub

    Private Sub tmaDescuentosProveedores_RowAdded(ByVal RowHandle As Long) Handles tmaDescuentosProveedores.RowAdded
        If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

            If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                    Me.LlenarGridPagos()
                End If
            End If
        End If

        CalculaProntoPago()

    End Sub

    Private Sub tmaDescuentosProveedores_RowDeleted() Handles tmaDescuentosProveedores.RowDeleted
        If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

            If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                    Me.LlenarGridPagos()
                End If
            End If
        End If

        CalculaProntoPago()

    End Sub

    Private Sub tmaDescuentosProveedores_RowUpdated(ByVal RowHandle As Long) Handles tmaDescuentosProveedores.RowUpdated
        If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

            If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                    Me.LlenarGridPagos()
                End If
            End If
        End If

        CalculaProntoPago()
    End Sub



#End Region

#Region "DIPROS Systems, Funcionalidad"

    'DAM 22/07/2007 - FUNCION PARA CREAR LAS FILAS DEL GRID DE LOS PAGOS PARA CXP EN BASE A LAS CONDICIONES DE PAGO
    Public Function CrearRow(ByVal oData As DataSet, ByVal partida As Long, ByVal Total As Double, ByVal ImportePagos As Double, ByVal plazo As Long, ByVal descuento As Double) As DataRow
        Try
            Dim oRow As DataRow

            Dim midatarow As System.Data.DataRow
            midatarow = oData.Tables("Generales").NewRow()
            Try
                midatarow("control") = "1"
                midatarow("partida") = CType(partida, Integer)
                midatarow("total") = CType(Total, Double)
                midatarow("totalxcondicion") = CType(ImportePagos, Double)
                midatarow("fecha_pago") = CType(TINApp.FechaServidor, DateTime).AddDays(plazo)
                midatarow("descuento") = CType(descuento, Double)
                Return midatarow

            Catch ex As Exception
            End Try
        Catch ex As Exception

        End Try



    End Function
    Public Function GeneraCuentasPorPagar(ByVal entrada As Long, ByVal fecha As Date, ByVal subtotal As Double, ByVal iva As Double, ByVal total As Double) As Events
        Dim oEvents As Events
        Dim oMovimientosPagar As New VillarrealBusiness.clsMovimientosPagar

        Dim folio_cxp As Long
        Dim fecha_vencimiento As Date
        Dim plazo As Long
        Dim oDataSet As DataSet

        plazo = MaximoPlazoCondicion
        fecha_vencimiento = fecha.AddDays(plazo)

        'Me.ObtenerFolioPolizaCheque(oEvents)

        'If Not oEvents.ErrorFound Then
        oEvents = oMovimientosPagar.Insertar(Concepto_cxp, folio_cxp, Proveedor, entrada, fecha, fecha_vencimiento, plazo, "", _
                                     0, True, total, 0, subtotal, iva, total, total, _
                                     "P", 0, System.DBNull.Value, 0, Me.txtObservaciones.Text, CStr(TINApp.Connection.UserName), "", "", Me.txtReferencia.Text, Me.ChkAbonoCuenta.EditValue, 0, Me.Folio_Poliza_Cheque)            'Lleva un 0 al final por que akino inserto el pago a proveedores
        'End If
        Return oEvents
    End Function
    Public Sub CalculaTotales()



        'Calculando totales sin cobro de flete
        clcSubTotal.Value = Me.grcImporte.SummaryItem.SummaryValue
        clcIva.Value = CType(clcSubTotal.Value, Double) * Impuesto
        clcTotal.Value = CType(clcSubTotal.Value, Double) + CType(clcIva.Value, Double)

        'Calculando totales con FLETE

        clcSubTotalconFlete.Value = Me.grcImporteConFlete.SummaryItem.SummaryValue
        clcIvaconFlete.Value = CType(clcSubTotalconFlete.Value, Double) * Impuesto
        clcTotalconFlete.Value = CType(clcSubTotalconFlete.Value, Double) + CType(clcIvaconFlete.Value, Double)
    End Sub
    Public Sub CambiaForma()
        If Me.clcSubTotalconFlete.EditValue = 0 Then
            Me.clcSubTotalconFlete.Visible = False
            Me.clcIvaconFlete.Visible = False
            Me.clcTotalconFlete.Visible = False
            Me.grcImporteConFlete.VisibleIndex = -1
            Me.grcCostoConFlete.VisibleIndex = -1
            Me.grcImporte.Caption = "Importe"
            Me.grcCosto_recibido.Caption = "Costo"
        End If
    End Sub
    Public Sub DesactivarControles()

        Me.clcPlazo1.Enabled = False
        Me.clcPlazo2.Enabled = False
        Me.clcPlazo3.Enabled = False
        Me.clcPlazo4.Enabled = False
        Me.clcPlazo5.Enabled = False
        Me.clcFolioFactura.Enabled = False
        Me.dteFechaFactura.Enabled = False
        Me.lkpAcreedor.Enabled = False
        Me.clcFlete.Enabled = False
        Me.clcSeguro.Enabled = False
        Me.clcManiobras.Enabled = False
        Me.clcOtrosGastos.Enabled = False
        Me.clcSubtotalFlete.Enabled = False
        Me.clcIVAFlete.Enabled = False
        Me.clcIvaRetenido.Enabled = False
        Me.clcTotalFlete.Enabled = False
        Me.chkFleteEnFactura.Enabled = False
        Me.tmaDescuentosProveedores.Enabled = False

    End Sub
    Public Sub LlenarGrid()

        Select Case Action
            Case Actions.Insert

                Dim response As New Events

                response = Me.oProveedoresdescuentos.DespliegaDatosParaInsercionDeEntradas(Me.Proveedor)
                If Not response.ErrorFound Then
                    Dim odataset As DataSet
                    odataset = response.Value

                    Me.tmaDescuentosProveedores.DataSource = odataset
                    If Not Me.tmaDescuentosProveedores.DataSource Is Nothing Then

                        If IsNumeric(Me.lkpProveedor.EditValue) And IsNumeric(clcTotal.EditValue) And IsNumeric(clcPlazo1.EditValue) Then
                            If Me.lkpProveedor.EditValue > 0 And Me.clcTotal.EditValue > 0 And clcPlazo1.EditValue > 0 Then
                                Me.LlenarGridPagos()
                            End If
                        End If
                    End If

                End If

        End Select

        'If Me.clcTotal.Value > 0 Then
        '    Dim response As New Events

        '    response = Me.oEntradasProveedorDescuentos.Listado
        '    If Not response.ErrorFound Then
        '        Dim odataset As DataSet
        '        odataset = response.Value
        '        Me.grPagos.DataSource = odataset.Tables(0)
        '    Else
        '        response.ShowMessage()

        '    End If
        'End If
    End Sub

    Private Sub LlenarGridPagos()
        Try
            If ObtenerMaximoPlazoCondicion() > 0 Then
                Dim Total As Double
                Dim TotalProrrateado As Double
                Dim ImportePagos As Double
                Dim Plazo1 As Integer
                Dim Plazo2 As Integer
                Dim Plazo3 As Integer
                Dim Plazo4 As Integer
                Dim Plazo5 As Integer
                Dim PlazosConValor As Integer = 0
                Dim NumeroDescuentos As Integer
                Dim i As Integer = 0
                Dim oData As New DataSet

                Plazo1 = Me.clcPlazo1.Value
                Plazo2 = Me.clcPlazo2.Value
                Plazo3 = Me.clcPlazo3.Value
                Plazo4 = Me.clcPlazo4.Value
                Plazo5 = Me.clcPlazo5.Value



                If Plazo1 > 0 Then PlazosConValor = PlazosConValor + 1
                If Plazo2 > 0 Then PlazosConValor = PlazosConValor + 1
                If Plazo3 > 0 Then PlazosConValor = PlazosConValor + 1
                If Plazo4 > 0 Then PlazosConValor = PlazosConValor + 1
                If Plazo5 > 0 Then PlazosConValor = PlazosConValor + 1
                If Plazo1 + Plazo2 + Plazo3 + Plazo4 + Plazo5 Then

                End If
                Total = TotalOSubtotal()
                NumeroDescuentos = Me.grvDescuentosProveedor.RowCount
                TotalProrrateado = Total

                For i = 0 To Me.grvDescuentosProveedor.RowCount - 1
                    TotalProrrateado = TotalProrrateado - (TotalProrrateado * Me.grvDescuentosProveedor.GetRowCellValue(i, Me.grcPorcentaje) / 100)
                Next

                If Me.grvDescuentosProveedor.RowCount > 0 Then
                    ImportePagos = TotalProrrateado / PlazosConValor
                    Total = TotalProrrateado
                Else
                    ImportePagos = Total / PlazosConValor
                End If

                TotalConsiderandoDescuentos = Total
                IvaConsiderandoDescuentos = TotalConsiderandoDescuentos * Impuesto
                SubtotalConsiderandoDescuentos = TotalConsiderandoDescuentos - IvaConsiderandoDescuentos


                oData.Tables.Add("Generales")
                oData.Tables(0).Columns.Add("control")

                oData.Tables(0).Columns.Add("partida")
                oData.Tables(0).Columns.Add("total").DataType = System.Type.GetType("System.Double")
                oData.Tables(0).Columns.Add("totalxcondicion").DataType = System.Type.GetType("System.Double")
                oData.Tables(0).Columns.Add("fecha_pago").DataType = System.Type.GetType("System.DateTime")


                oData.Tables(0).Columns.Add("descuento")

                Dim descuento As Double = 0
                Dim TotalMenosDescuento As Double = Total
                For i = 0 To PlazosConValor - 1
                    Dim plazo As Integer

                    Select Case i
                        Case 0
                            plazo = Plazo1
                            descuento = 0
                        Case 1
                            plazo = Plazo2
                            descuento = 0
                        Case 2
                            plazo = Plazo3
                            descuento = 0
                        Case 3
                            plazo = Plazo4
                            descuento = 0
                        Case 4
                            plazo = Plazo5
                            descuento = 0
                    End Select

                    oData.Tables(0).Rows.Add(CrearRow(oData, (i + 1), Total, ImportePagos, plazo, descuento))
                Next

                Me.grPagos.DataSource = oData.Tables(0)

            End If

        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, "Error al llenar el grid de pagos")
        End Try

    End Sub
    Private Sub GuardaHistorial(ByRef response As Events, ByVal articulo As Long, ByVal clave_precio As Long, ByVal precio_venta As Double)
        Dim folio As Long = 0
        response = Me.oHistoricoArticulosPrecios.Insertar(folio, articulo, clave_precio, precio_venta)
    End Sub
    Private Sub ObtenerDetalleOrden(ByVal entrada As Long)
        Dim response As Events
        Dim odata As DataSet

        response = Me.oEntradasdetalle.DetalleOC(entrada)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            response = DespliegaPrecios(oDataSet)
        End If

    End Sub
    Private Sub CalculaTotalesFlete()
        Try
            Me.clcSubtotalFlete.EditValue = CType(Me.clcFlete.EditValue, Double) + CType(Me.clcSeguro.EditValue, Double) + CType(Me.clcManiobras.EditValue, Double) + CType(Me.clcOtrosGastos.EditValue, Double)
            Me.clcIVAFlete.EditValue = CType(Me.clcSubtotalFlete.EditValue, Double) * CType(Me.Impuesto, Double)
            Me.clcTotalFlete.EditValue = CType(Me.clcSubtotalFlete.EditValue, Double) + CType(Me.clcIVAFlete.EditValue, Double) - CType(Me.clcIvaRetenido.EditValue, Double)
        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, "Revise el formato de los datos")
        End Try

    End Sub
    Private Sub CalculaProntoPago()
        Try
            If Me.tmaDescuentosProveedores.View.RowCount > 0 And Me.clcTotal.Value > 0 Then

                Dim i = 0
                Dim total As Double
                Dim total_Prorrateado As Double
                Dim Descuento As Double
                Dim Porcentaje As Double
                Dim ImporteCondicion As Double
                Dim PlazosConValor As Integer = 0

                total = TotalOSubtotal() 'Me.clcTotal.Value
                total_Prorrateado = total
                Porcentaje = 0
                Descuento = 0

                If Me.clcPlazo1.Value > 0 Then PlazosConValor = PlazosConValor + 1
                If Me.clcPlazo2.Value > 0 Then PlazosConValor = PlazosConValor + 1
                If Me.clcPlazo3.Value > 0 Then PlazosConValor = PlazosConValor + 1
                If Me.clcPlazo4.Value > 0 Then PlazosConValor = PlazosConValor + 1
                If Me.clcPlazo5.Value > 0 Then PlazosConValor = PlazosConValor + 1

                For i = 0 To Me.tmaDescuentosProveedores.View.RowCount - 1

                    Porcentaje = Me.tmaDescuentosProveedores.DataSource.Tables(0).Rows(i).Item("porcentaje")
                    'Porcentaje = Me.grvDescuentosProveedor.GetRowCellValue(i, Me.grcPorcentaje)
                    If Porcentaje > 0 Then
                        Descuento = total_Prorrateado * Porcentaje / 100
                        total_Prorrateado = total_Prorrateado - Descuento

                        'Me.grvDescuentosProveedor.SetRowCellValue(i, Me.grcImporteDescuento, Descuento)
                        Me.tmaDescuentosProveedores.DataSource.Tables(0).Rows(i).Item("importe_descuento") = Descuento
                        Me.tmaDescuentosProveedores.Refresh()

                    End If

                    Porcentaje = 0
                    Descuento = 0

                Next

                ImporteCondicion = total_Prorrateado / PlazosConValor
                total = total_Prorrateado

                TotalConsiderandoDescuentos = total
                IvaConsiderandoDescuentos = TotalConsiderandoDescuentos * Impuesto
                SubtotalConsiderandoDescuentos = TotalConsiderandoDescuentos - IvaConsiderandoDescuentos

                For i = 0 To Me.grvPagos.RowCount - 1

                    Me.grvPagos.SetRowCellValue(i, Me.grcImporteCondicion, ImporteCondicion)

                Next

            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub borrarGridPagos()
        Dim i As Integer
        For i = 0 To Me.grvPagos.RowCount - 1
            Me.grvPagos.DeleteRow(i)
        Next
    End Sub

    Private Function GuardaProgramacionPagos() As Events
        Me.grvPagos.CloseEditor()
        Me.grvPagos.UpdateCurrentRow()
        Dim response As New Events


        response = Me.oEntradasProgramacionPagos.Eliminar(Me.clcFolio.EditValue, -1)
        If Not response.ErrorFound Then
            Try

                Dim i As Integer = 0
                With Me.grvPagos
                    For i = 0 To .RowCount - 1
                        'Select Case Action
                        'Case Actions.Insert
                        response = Me.oEntradasProgramacionPagos.Insertar(Me.clcFolio.EditValue, .GetRowCellValue(i, Me.grcPartida), .GetRowCellValue(i, Me.grcFechaPago), .GetRowCellValue(i, Me.grcImporteCondicion), .GetRowCellValue(i, Me.grcDescuento), .GetRowCellValue(i, Me.grcMonto))
                        'Case Actions.Update
                        'response = Me.oEntradasProgramacionPagos.Actualizar(Me.clcFolio.EditValue, .GetRowCellValue(i, Me.grcPartida), .GetRowCellValue(i, Me.grcFechaPago), .GetRowCellValue(i, Me.grcImporteCondicion), .GetRowCellValue(i, Me.grcDescuento), .GetRowCellValue(i, Me.grcMonto))
                        'Case Actions.Delete
                        ' response = Me.oEntradasProgramacionPagos.Eliminar(Me.clcFolio.EditValue, .GetRowCellValue(i, Me.grcPartida))
                        'End Select

                    Next
                End With
            Catch ex As Exception

            End Try
        End If
        'código anterior
        'Me.grvPagos.CloseEditor()
        'Me.grvPagos.UpdateCurrentRow()

        'Dim response As New Events
        'Dim i As Long
        'Dim lpartida As Long
        'Dim fecha_pago As Date

        'Dim dImporte As Double
        'Dim dDescuento As Double
        'Dim dtotal As Double

        'oEntradasProgramacionPagos = New VillarrealBusiness.clsEntradasProgramacionPagos

        'For i = 0 To Me.grvPagos.RowCount - 1
        '    lpartida = Me.grvPagos.GetRowCellValue(i, Me.grcPartida)
        '    fecha_pago = Me.grvPagos.GetRowCellValue(i, Me.grcFechaPago)

        '    dImporte = Me.grvPagos.GetRowCellValue(i, Me.grcImporteCondicion)
        '    dDescuento = Me.grvPagos.GetRowCellValue(i, Me.grcDescuento)
        '    dtotal = Me.grvPagos.GetRowCellValue(i, Me.grcMonto)


        '    response = Me.oEntradasProgramacionPagos.Insertar(Me.clcFolio.EditValue, lpartida, fecha_pago, dImporte, dDescuento, dtotal)
        'Next


        Return response
    End Function
    Private Function contarCondiciones() As Long
        Dim counter As Long
        counter = 0

        If (Me.clcPlazo1.Value > 0) Then counter = counter + 1
        If (Me.clcPlazo2.Value > 0) Then counter = counter + 1
        If (Me.clcPlazo3.Value > 0) Then counter = counter + 1
        If (Me.clcPlazo4.Value > 0) Then counter = counter + 1
        If (Me.clcPlazo5.Value > 0) Then counter = counter + 1

        contarCondiciones = counter
    End Function
    Private Function ValidarValoresClc()
        If Not IsNumeric(Me.clcPlazo1.EditValue) Then
            Me.clcPlazo1.EditValue = 0
        End If
        If Not IsNumeric(Me.clcPlazo2.EditValue) Then
            Me.clcPlazo2.EditValue = 0
        End If
        If Not IsNumeric(Me.clcPlazo3.EditValue) Then
            Me.clcPlazo3.EditValue = 0
        End If
        If Not IsNumeric(Me.clcPlazo4.EditValue) Then
            Me.clcPlazo4.EditValue = 0
        End If
        If Not IsNumeric(Me.clcPlazo5.EditValue) Then
            Me.clcPlazo5.EditValue = 0
        End If
    End Function
    Private Function ObtenerMaximoPlazoCondicion() As Long

        Dim maxima_condicion As Long = 0
        If (Me.clcPlazo1.Value > 0) Then maxima_condicion = Me.clcPlazo1.Value
        If (Me.clcPlazo2.Value > 0) Then maxima_condicion = Me.clcPlazo2.Value
        If (Me.clcPlazo3.Value > 0) Then maxima_condicion = Me.clcPlazo3.Value
        If (Me.clcPlazo4.Value > 0) Then maxima_condicion = Me.clcPlazo4.Value
        If (Me.clcPlazo5.Value > 0) Then maxima_condicion = Me.clcPlazo5.Value

        Return maxima_condicion
    End Function
    Private Function GeneraCargoAcreedores() As Dipros.Utils.Events
        Dim oEvents As New Dipros.Utils.Events
        Dim IVA As Double
        Dim Concepto As String

        IVA = Me.clcIVAFlete.EditValue - Me.clcIvaRetenido.EditValue
        Concepto = Me.ovariables.TraeDatos("concepto_cxp_cargo_por_flete", VillarrealBusiness.clsVariables.tipo_dato.Varchar)

        oEvents = oAcreedoresDiversos.Insertar(0, Me.dteFechaFactura.EditValue, Me.clcFolioFactura.EditValue, Me.dteFechaFactura.EditValue, Me.clcTotalFlete.EditValue, Me.lkpAcreedor.EditValue, Me.lkpAcreedor.GetValue("nombre"), "P", 0, 0, Me.dteFechaFactura.EditValue, "", 0, Me.dteFechaFactura.EditValue, False, Concepto, False, Me.clcSubtotalFlete.EditValue, IVA, Me.lkpAcreedor.GetValue("rfc"), 0)

        Return oEvents
    End Function
    Private Function TotalOSubtotal() As Double

        If IsNumeric(Me.clcTotal.Value) And IsNumeric(Me.clcSubTotal.Value) Then
            If Me.tmaDescuentosProveedores.View.RowCount > 0 Then
                If CType(Me.grvDescuentosProveedor.GetRowCellValue(0, Me.grcAntesIVA), Boolean) Then
                    TotalOSubtotal = Me.clcSubTotal.Value
                Else
                    TotalOSubtotal = Me.clcTotal.Value
                End If
            Else
                TotalOSubtotal = Me.clcTotal.Value
            End If
        Else
            TotalOSubtotal = 0
        End If

    End Function
    Private Function Costear(ByRef response As Events) As Events

        Me.tmaEntradas.MoveFirst()

        'Validaciones Necesarios de Conceptos para el Costeo
        If contarCondiciones() <= 0 Then
            response.Message = "Al menos una condición es Requerida"
            Exit Function
        End If

        If Me.Concepto_cxp.Trim.Length <= 0 Then
            response.Message = "El Concepto de CXP es Requerido"
            Exit Function
        End If
        If Me.ConceptoCancelacionEntrada.Trim.Length <= 0 Then
            response.Message = "El Concepto de Cancelacion  de Entrada es Requerido"
            Exit Function
        End If
        If Me.ConceptoEntrada.Trim.Length <= 0 Then
            response.Message = "El Concepto Entrada es Requerido"
            Exit Function
        End If

        'DAM - 12 / 02 /2007
        'Verifica si las Series de los articulos ya fueron capturadas
        Do While Not tmaEntradas.EOF

            'DAM -- REVISA MANEJO SERIES OCT-08
            If Comunes.clsUtilerias.UsarSeries = True Then
                response = oEntradasdetalle.ChecarSeries(Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"), tmaEntradas.Item("cantidad"))
                If Not response.Value Then
                    response.Message = "Articulo " + CType(tmaEntradas.Item("articulo"), String) + " todavía no se incluyen todas sus series"
                    Exit Function
                End If
            End If

            Me.tmaEntradas.MoveNext()
        Loop

        'DAM - 25-05-2007 Etapa Beta
        'Si tiene Flete y no esta en la factura dicho costo se valida que se tenga un acreedor
        If Me.clcTotalFlete.Value > 0 Then
            If Me.chkFleteEnFactura.Checked = False Then
                If IsNumeric(Me.lkpAcreedor.EditValue) = True Then
                    If Me.lkpAcreedor.EditValue <= 0 Then
                        response.Message = "Debe seleccionar un Acreedor si el Costo del flete es mayor que cero"
                        Exit Function
                    End If
                Else
                    response.Message = "Debe seleccionar un Acreedor si el Costo del flete es mayor que cero"
                    Exit Function
                End If
            End If
        End If

        'DAM - inicia transaccion
        TINApp.Connection.Begin()

        Me.chkCosteado.EditValue = True

        response = oEntradas.Actualizar(Me.DataSource)
        If response.ErrorFound Then
            TINApp.Connection.Rollback()
            response.ShowMessage()
            Exit Function
        End If

        'Guarda el Movimiento al Inventario
        GeneraMovimientoInventario(Actions.Insert, response, Comunes.Common.Sucursal_Actual, Bodega, ConceptoEntrada, folio_movimiento, Me.dteFecha.DateTime, SucursalOC, concepto_nulo, Me.clcFolio.Value, "Movimiento generado desde entradas al almacen")
        If response.ErrorFound Then
            TINApp.Connection.Rollback()
            response.ShowMessage()
            Exit Function
        End If


        ' =================================================
        ' DAM - 25-04-2007 Codigo para prorrateo de costos con el flete
        ' =================================================

        Me.tmaEntradas.MoveFirst()
        Do While Not Me.tmaEntradas.EOF

            Me.tmaEntradas.Item("costo_flete") = Me.Prorratear_costo_articulo(Me.tmaEntradas.Item("costo"))

            tmaEntradas.MoveNext()
        Loop

        ' =================================================

        Me.tmaEntradas.MoveFirst()

        Do While Not tmaEntradas.EOF
            Dim PrecioLista As Double = 0

            If tmaEntradas.Item("cantidad") > 0 Then

                PrecioLista = tmaEntradas.Item("precio_lista")
                ' Inserto el Historico de costos 
                'DAM - 13/abr/07 - se modifico para que inserte el costo con el flete prorrateado y no el costo del proveedor puro
                response = Me.oHistoricoCostos.Insertar(tmaEntradas.Item("folio_historico_costo"), FechaconHoras, tmaEntradas.Item("articulo"), Bodega, tmaEntradas.Item("cantidad"), tmaEntradas.Item("costo_flete"), tmaEntradas.Item("cantidad"))
                If response.ErrorFound Then
                    TINApp.Connection.Rollback()
                    response.ShowMessage()
                    Exit Function
                End If

                'Actualizo la Ultima Fecha de Compra, el Ultimo Costo y el Precio de Lista en el catalogo de Articulos
                response = oArticulos.ActualizaUltimoCosto(tmaEntradas.Item("articulo"), Me.dteFecha.Text, tmaEntradas.Item("costo_flete"), tmaEntradas.Item("costo"), PrecioLista)
                If response.ErrorFound Then
                    TINApp.Connection.Rollback()
                    ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                    Exit Function
                End If

                'Actualizo el Pedimento de los Articulos
                response = Me.oArticulos.ActualizaPedimento(Me.tmaEntradas.Item("articulo"), Me.txtPedimento.Text, Me.txtAduana.Text, Me.dteFecha_Importacion.EditValue)
                If response.ErrorFound Then
                    TINApp.Connection.Rollback()
                    response.ShowMessage()
                    Exit Function
                End If

                'Actualizo el detalle de la entrada (articulos)
                If tmaEntradas.CurrentAction = Actions.Update Or tmaEntradas.CurrentAction = Actions.None Then
                    response = oEntradasdetalle.Actualizar(Me.tmaEntradas.SelectedRow, Me.clcFolio.Value, Bodega, True)
                    If response.ErrorFound Then
                        TINApp.Connection.Rollback()
                        response.ShowMessage()
                        Exit Function
                    End If
                End If

                ' =================================================
                ' Inserto el Detalle de Precios de las Entradas
                ' =================================================

                If Not response.ErrorFound Then
                    Dim Aux As ArticulosPreciosCostos
                    Dim iRow As Integer
                    Dim descuento_precio As Double = 0

                    Aux = ArticulosPrecios(tmaEntradas.Item("partida"))
                    For Each oRow As DataRow In Aux.Precios.Tables(0).Rows


                        If oRow("utilidad") = 0 Then
                            oRow("precio_venta") = PrecioLista
                            'Else
                            '    descuento_precio = oRow("utilidad") / 100
                            '    oRow("precio_venta") = PrecioLista - (PrecioLista * descuento_precio)
                        End If

                        Dim precio_venta_validado As Double

                        'If oRow("precio_expo") = oRow("precio") Then
                        If oRow("precio") <> 4 Then
                            'precio_venta_validado = (Me.tmaEntradas.Item("costo_flete") * (1 + Impuesto)) * oRow("factor_ganancia_expo")
                            precio_venta_validado = oRow("precio_venta")


                            response = VerificaPrecios(tmaEntradas.Item("articulo"), oRow("precio"), oRow("porcentaje_descuento"), precio_venta_validado)
                            If response.ErrorFound Then
                                TINApp.Connection.Rollback()
                                response.ShowMessage()
                                Exit Function
                            End If


                            If tmaEntradas.CurrentAction = Actions.Insert Then
                                response = oEntradasDetalleprecios.Insertar(oRow, Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"))
                                If response.ErrorFound Then
                                    TINApp.Connection.Rollback()
                                    response.ShowMessage()
                                    Exit Function
                                End If

                            ElseIf tmaEntradas.CurrentAction = Actions.Update Or tmaEntradas.CurrentAction = Actions.None Then
                                response = oEntradasDetalleprecios.Actualizar(oRow, Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"))
                                If response.ErrorFound Then
                                    TINApp.Connection.Rollback()
                                    response.ShowMessage()
                                    Exit Function
                                End If
                            End If
                        End If

                    Next
                End If
                ' =================================================


                ' Inserto el Detalle del Movimiento de la Entrada
                response = Me.oMovimientosDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega, ConceptoEntrada, folio_movimiento, Me.dteFecha.DateTime, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"), tmaEntradas.Item("cantidad"), tmaEntradas.Item("costo_flete"), CType(tmaEntradas.Item("cantidad") * tmaEntradas.Item("costo_flete"), Double), tmaEntradas.Item("folio_historico_costo"), 0, tmaEntradas.Item("costo_flete"))
                If response.ErrorFound Then
                    TINApp.Connection.Rollback()
                    response.ShowMessage()
                    Exit Function
                End If

                'DAM -- REVISA MANEJO SERIES OCT-08
                If Comunes.clsUtilerias.UsarSeries = True Then
                    'Inserto las Series de las entradas
                    response = Me.oMovimientosDetalleSeries.InsertarSeriesEntradas(Comunes.Common.Sucursal_Actual, Bodega, ConceptoEntrada, folio_movimiento, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"), Me.clcFolio.Value, tmaEntradas.Item("partida"))
                    If response.ErrorFound Then
                        TINApp.Connection.Rollback()
                        ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                        Exit Function
                    End If
                End If

                'Actualizo las ventas sobre pedido que tengan la misma orden de compra de la entrada
                'Aqui tambien se actualiza el saldo de la capa del articulo si ya fue vendido sobre pedido
                response = oVentas.ActualizaEntradaEnVentasSobrePedido(Orden, Me.clcFolio.EditValue, tmaEntradas.Item("articulo"), Bodega, tmaEntradas.Item("cantidad"), tmaEntradas.Item("costo"), tmaEntradas.Item("folio_historico_costo"))
                If response.ErrorFound Then
                    TINApp.Connection.Rollback()
                    ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                    Exit Function
                End If
            End If


            tmaEntradas.MoveNext()
        Loop


        If Not response.ErrorFound And Orden > 0 Then
            'Cambio el Status a la orden de compra
            response = Me.oOrdenesCompra.CambiarStatus(Orden)
            If response.ErrorFound Then
                TINApp.Connection.Rollback()
                ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                Exit Function
            End If
        End If

        If Not response.ErrorFound Then
            'Se genera la cuenta por pagar 
            'Se agrego para que genere el cargo por el importe ya con los descuentos si es que los tuviera
            response = GeneraCuentasPorPagar(Me.clcFolio.EditValue, Me.dteFecha.DateTime, Me.SubtotalConsiderandoDescuentos, Me.IvaConsiderandoDescuentos, Me.TotalConsiderandoDescuentos)
            If response.ErrorFound Then
                TINApp.Connection.Rollback()
                ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                Exit Function
            End If
        End If


        'Aki guardar en la tabla de doc_entradas_programacion_pagos
        If Not response.ErrorFound Then

            response = GuardaProgramacionPagos()
            If response.ErrorFound Then
                TINApp.Connection.Rollback()
                ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                Exit Function
            End If
        End If



        ' =================================================
        ' Codigo para Generar el cargo en CXP del flete al Acreedor
        ' =================================================
        If Not response.ErrorFound Then
            If Me.chkFleteEnFactura.Checked = False Then
                If IsNumeric(Me.lkpAcreedor.EditValue) = True Then
                    If Me.lkpAcreedor.EditValue > 0 Then
                        response = Me.GeneraCargoAcreedores()
                        If response.ErrorFound Then
                            TINApp.Connection.Rollback()
                            ShowMessage(MessageType.MsgInformation, response.Ex.Message)
                            Exit Function
                        End If
                    End If
                End If

            End If
        End If

        ' =================================================


        'TERMINA LA TRANSACCION
        TINApp.Connection.Commit()

        bReciboImpreso = False

        'DAM 25/05/2007 - Se Manda a Imprimir la Entrada al Almacen
        ImprimirReporte()

        ''DAM 22/05/2007
        ''Si la Entrada viene de una OC sobrepedido se manda  a imprimir las facturas que se estan surtiendo
        'If Not response.ErrorFound And Not bReciboImpreso Then
        '    ImprimirReporteFacturasSurtidasSobrepedido(Me.clcFolio.Value)
        'End If


    End Function
    Private Function Prorratear_costo_articulo(ByVal costo_articulo As Double) As Double
        Dim factor_flete As Double
        Dim costo_prorrateado As Double


        'DAM 10-SEPT-07 ,MODIFICADO PARA CORREGIR EL CALCULO DE PRORRATEO, 

        'If clcTotalCostoUnitario.Value > 0 Then
        'DAM - 23/MAY/2007 - SE CAMBIO PARA QUE EL COSTO DEL FLETE SEA SIN EL IVA PARA EL PRORRATEO
        'factor_flete = Me.clcTotalFlete.Value / clcTotalCostoUnitario.Value
        'factor_flete = Me.clcSubtotalFlete.Value / clcTotalCostoUnitario.Value
        factor_flete = Me.clcSubtotalFlete.Value / clcSubTotal.Value
        costo_prorrateado = (costo_articulo * factor_flete) + costo_articulo
        'Else
        'factor_flete = 0
        'costo_prorrateado = 0
        'End If



        Return costo_prorrateado
    End Function
    Private Function ExisteArticulo(ByVal Indice As Integer) As Boolean
        Dim AuxArticulosPrecios As ArticulosPreciosCostos
        For i As Integer = 0 To EstructuraPrecios.Count - 1
            AuxArticulosPrecios = EstructuraPrecios(i)
            'revisa si existe el articulo
            If AuxArticulosPrecios.Indice = Indice Then
                ExisteArticulo = True
                Exit Function
            End If
        Next
    End Function
    Private Function IndexArticulo(ByVal Indice As Integer) As Integer
        Dim AuxExamenes As ArticulosPreciosCostos
        For i As Integer = 0 To EstructuraPrecios.Count - 1
            AuxExamenes = EstructuraPrecios(i)
            'revisa si existe la pregunta
            If AuxExamenes.Indice = Indice Then
                IndexArticulo = i
                Exit Function
            End If
        Next
    End Function
    Private Function DespliegaPrecios(ByVal Data As DataSet) As Events
        Dim odataset As DataSet
        Dim response As Events
        Dim AuxPrecios As ArticulosPreciosCostos
        EstructuraPrecios.Clear()

        odataset = Data
        Me.tmaEntradas.DataSource = odataset

        Me.tmaEntradas.MoveFirst()

        Do While Not Me.tmaEntradas.EOF
            AuxPrecios = New ArticulosPreciosCostos

            If bdespliega Then
                response = Me.oEntradasDetalleprecios.DespliegaDatos(Me.clcFolio.Value, tmaEntradas.Item("partida"), tmaEntradas.Item("articulo"))
            Else
                response = oArticulosPrecios.Listado(tmaEntradas.Item("articulo"))
            End If
            If Not response.ErrorFound Then
                odataset = response.Value
                AuxPrecios.Indice = tmaEntradas.Item("partida")
                AuxPrecios.Precios = odataset
                Me.EstructuraPrecios.Add(AuxPrecios)
            End If
            Me.tmaEntradas.MoveNext()
        Loop

        Return response
    End Function
    Private Function VerificaPrecios(ByVal articulo As Long, ByVal clave_precio As Long, ByVal utilidad As Double, ByVal precio_venta As Double) As Events
        Dim response As New Events
        response = oArticulosPrecios.Actualizar(articulo, clave_precio, utilidad, precio_venta)
        GuardaHistorial(response, articulo, clave_precio, precio_venta)
        Return response
    End Function
    Private Function GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String)
        If Guardado = False Then

            Select Case accion
                Case Actions.Insert, Actions.Delete
                    response = oMovimientos.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
                Case Actions.Update
                    response = oMovimientos.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
            End Select
        End If
    End Function
    Private Function FechaconHoras() As DateTime
        dteFecha.DateTime = dteFecha.DateTime.Date
        Me.dteFecha.DateTime = DateAdd(DateInterval.Hour, Now.Hour, dteFecha.DateTime)
        dteFecha.EditValue = DateAdd(DateInterval.Minute, Now.Minute, dteFecha.EditValue)
        dteFecha.EditValue = DateAdd(DateInterval.Second, Now.Second, dteFecha.EditValue)
        Return Me.dteFecha.DateTime
    End Function
    Private Function CalculaSaldoHistorico(ByVal cantidad_anterior As Long, ByVal Cantidad_Nueva As Long, ByVal saldo_historico As Long) As Long
        Dim Resultado As Long
        Resultado = (saldo_historico - cantidad_anterior) + Cantidad_Nueva
        Return Resultado
    End Function
    Private Function ImprimirReporte()
        Dim response As New Events
        response = oReportes.EntradasAlmacenProceso(Me.clcFolio.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Entrada no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                If clcTotalFlete.EditValue > 0 Then

                    Dim oReport As New rptEntradaAlmacenProceso

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    oReport.lblFirmaComprador.Visible = chkCosteado.Checked
                    oReport.lblFirmaRecibido.Visible = chkCosteado.Checked
                    oReport.LineaComprador.Visible = chkCosteado.Checked
                    oReport.LineaRecibido.Visible = chkCosteado.Checked


                    TINApp.ShowReport(Me.MdiParent, "Entradas", oReport, , AddressOf ImprimeFacturasSurtidas)

                    oReport = Nothing


                Else
                    Dim oReport As New rptEntradaAlmacenProcesoSinFlete

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    oReport.lblFirmaComprador.Visible = chkCosteado.Checked
                    oReport.lblFirmaRecibido.Visible = chkCosteado.Checked
                    oReport.LineaComprador.Visible = chkCosteado.Checked
                    oReport.LineaRecibido.Visible = chkCosteado.Checked


                    TINApp.ShowReport(Me.MdiParent, "Entradas", oReport, , AddressOf ImprimeFacturasSurtidas)

                    oReport = Nothing

                End If




                oDataSet = Nothing
               
            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            End If
        End If

    End Function

    Private Sub ImprimirReporteFacturasSurtidasSobrepedido(ByVal entrada As Long)
        Dim response As New Events
        response = oReportes.FacturasSurtidasSobrepedido(entrada)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Facturas Surtidas Sobre Pedido no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptFacturasSurtidasSobrepedido

                oDataSet = response.Value

                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Facturas Surtidas Sobre Pedido", oReport, , AddressOf ImprimePoliza)

                oDataSet = Nothing
                oReport = Nothing


            Else
                ImprimePoliza()
            End If
        End If
    End Sub
    Private Sub ImprimeFacturasSurtidas()
        ImprimirReporteFacturasSurtidasSobrepedido(Me.clcFolio.Value)

    End Sub
    Private Sub ImprimePoliza()
        Me.ImprimePolizaFlete(Me.clcFlete.Value)

    End Sub
    Private Sub ImprimePolizaFlete(ByVal flete As Long)
        ' DAM 30/nov/07 - si no hay flete se sale de la funcion
        If flete <= 0 Then Exit Sub

        Dim response As Events
        response = oReportes.EjecutarPolizaFlete(Me.clcFolio.Value, Me.dteFecha.DateTime, True, Comunes.Common.Sucursal_Actual)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptPolizaFlete

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Pólizas", oReport)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            End If
        End If
    End Sub

    'Private Function ObtenerFolioPolizaCheque(ByRef response As Dipros.Utils.Events) As Long
    '    response = Me.oPolizasCheques.ObtenerSiguienteFolio
    '    If Not response.ErrorFound Then
    '        Me.Folio_Poliza_Cheque = response.Value + 1
    '    End If
    'End Function

#End Region


    Private Sub frmEntradas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MENUOPTION.NAME, "FECHA_ENTRADAS") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub

End Class