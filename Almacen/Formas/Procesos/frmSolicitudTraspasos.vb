Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports Comunes.clsUtilerias

Public Class frmSolicitudTraspasos
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblSolicitud As System.Windows.Forms.Label
		Friend WithEvents clcSolicitud As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Friend WithEvents lblSucursal_Entrada As System.Windows.Forms.Label

		Friend WithEvents lblBodega_Entrada As System.Windows.Forms.Label
    Friend WithEvents lblSolicita As System.Windows.Forms.Label

		Friend WithEvents lblCapturo As System.Windows.Forms.Label

		Friend WithEvents lblFecha_Solicitud As System.Windows.Forms.Label
		Friend WithEvents dteFecha_Solicitud As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblEstatus As System.Windows.Forms.Label
    Friend WithEvents grSolicitudTraspasos As DevExpress.XtraGrid.GridControl
		Friend WithEvents grvSolicitudTraspasos As DevExpress.XtraGrid.Views.Grid.GridView
		Friend WithEvents tmaSolicitudTraspasos As Dipros.Windows.TINMaster
    Friend WithEvents lkpSolicita As Dipros.Editors.TINMultiLookup
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtCapturo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblSucursalOrigen As System.Windows.Forms.Label
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursalSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursalEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpAutorizo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones_Salida As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolicitudTraspasos))
        Me.lblSolicitud = New System.Windows.Forms.Label
        Me.clcSolicitud = New Dipros.Editors.TINCalcEdit
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.lblSucursal_Entrada = New System.Windows.Forms.Label
        Me.lblBodega_Entrada = New System.Windows.Forms.Label
        Me.lblSolicita = New System.Windows.Forms.Label
        Me.lblCapturo = New System.Windows.Forms.Label
        Me.lblFecha_Solicitud = New System.Windows.Forms.Label
        Me.dteFecha_Solicitud = New DevExpress.XtraEditors.DateEdit
        Me.lblEstatus = New System.Windows.Forms.Label
        Me.grSolicitudTraspasos = New DevExpress.XtraGrid.GridControl
        Me.grvSolicitudTraspasos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaSolicitudTraspasos = New Dipros.Windows.TINMaster
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaEntrada = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursalSalida = New Dipros.Editors.TINMultiLookup
        Me.lkpSolicita = New Dipros.Editors.TINMultiLookup
        Me.txtCapturo = New DevExpress.XtraEditors.TextEdit
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.lkpSucursalEntrada = New Dipros.Editors.TINMultiLookup
        Me.lblSucursalOrigen = New System.Windows.Forms.Label
        Me.lkpAutorizo = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblObservaciones_Salida = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcSolicitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Solicitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSolicitudTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSolicitudTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(946, 28)
        '
        'lblSolicitud
        '
        Me.lblSolicitud.AutoSize = True
        Me.lblSolicitud.Location = New System.Drawing.Point(63, 40)
        Me.lblSolicitud.Name = "lblSolicitud"
        Me.lblSolicitud.Size = New System.Drawing.Size(56, 16)
        Me.lblSolicitud.TabIndex = 0
        Me.lblSolicitud.Tag = ""
        Me.lblSolicitud.Text = "&Solicitud:"
        Me.lblSolicitud.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSolicitud
        '
        Me.clcSolicitud.EditValue = "0"
        Me.clcSolicitud.Location = New System.Drawing.Point(128, 39)
        Me.clcSolicitud.MaxValue = 0
        Me.clcSolicitud.MinValue = 0
        Me.clcSolicitud.Name = "clcSolicitud"
        '
        'clcSolicitud.Properties
        '
        Me.clcSolicitud.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcSolicitud.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSolicitud.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcSolicitud.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSolicitud.Properties.Enabled = False
        Me.clcSolicitud.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSolicitud.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSolicitud.Size = New System.Drawing.Size(70, 19)
        Me.clcSolicitud.TabIndex = 1
        Me.clcSolicitud.Tag = "solicitud"
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(28, 138)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(91, 16)
        Me.lblBodega_Salida.TabIndex = 8
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "Bodega &Origen:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal_Entrada
        '
        Me.lblSucursal_Entrada.AutoSize = True
        Me.lblSucursal_Entrada.Location = New System.Drawing.Point(17, 67)
        Me.lblSucursal_Entrada.Name = "lblSucursal_Entrada"
        Me.lblSucursal_Entrada.Size = New System.Drawing.Size(102, 16)
        Me.lblSucursal_Entrada.TabIndex = 2
        Me.lblSucursal_Entrada.Tag = ""
        Me.lblSucursal_Entrada.Text = "S&ucursal Destino:"
        Me.lblSucursal_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega_Entrada
        '
        Me.lblBodega_Entrada.AutoSize = True
        Me.lblBodega_Entrada.Location = New System.Drawing.Point(23, 91)
        Me.lblBodega_Entrada.Name = "lblBodega_Entrada"
        Me.lblBodega_Entrada.Size = New System.Drawing.Size(96, 16)
        Me.lblBodega_Entrada.TabIndex = 4
        Me.lblBodega_Entrada.Tag = ""
        Me.lblBodega_Entrada.Text = "Bodega &Destino:"
        Me.lblBodega_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSolicita
        '
        Me.lblSolicita.AutoSize = True
        Me.lblSolicita.Location = New System.Drawing.Point(70, 163)
        Me.lblSolicita.Name = "lblSolicita"
        Me.lblSolicita.Size = New System.Drawing.Size(49, 16)
        Me.lblSolicita.TabIndex = 10
        Me.lblSolicita.Tag = ""
        Me.lblSolicita.Text = "So&licita:"
        Me.lblSolicita.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCapturo
        '
        Me.lblCapturo.AutoSize = True
        Me.lblCapturo.Location = New System.Drawing.Point(66, 188)
        Me.lblCapturo.Name = "lblCapturo"
        Me.lblCapturo.Size = New System.Drawing.Size(53, 16)
        Me.lblCapturo.TabIndex = 12
        Me.lblCapturo.Tag = ""
        Me.lblCapturo.Text = "Cap&turó:"
        Me.lblCapturo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha_Solicitud
        '
        Me.lblFecha_Solicitud.AutoSize = True
        Me.lblFecha_Solicitud.Location = New System.Drawing.Point(248, 40)
        Me.lblFecha_Solicitud.Name = "lblFecha_Solicitud"
        Me.lblFecha_Solicitud.Size = New System.Drawing.Size(109, 16)
        Me.lblFecha_Solicitud.TabIndex = 2
        Me.lblFecha_Solicitud.Tag = ""
        Me.lblFecha_Solicitud.Text = "&Fecha de Solicitud:"
        Me.lblFecha_Solicitud.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Solicitud
        '
        Me.dteFecha_Solicitud.EditValue = New Date(2006, 6, 23, 0, 0, 0, 0)
        Me.dteFecha_Solicitud.Location = New System.Drawing.Point(360, 38)
        Me.dteFecha_Solicitud.Name = "dteFecha_Solicitud"
        '
        'dteFecha_Solicitud.Properties
        '
        Me.dteFecha_Solicitud.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Solicitud.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Solicitud.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Solicitud.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Solicitud.TabIndex = 3
        Me.dteFecha_Solicitud.Tag = "fecha_solicitud"
        '
        'lblEstatus
        '
        Me.lblEstatus.AutoSize = True
        Me.lblEstatus.Location = New System.Drawing.Point(69, 232)
        Me.lblEstatus.Name = "lblEstatus"
        Me.lblEstatus.Size = New System.Drawing.Size(50, 16)
        Me.lblEstatus.TabIndex = 16
        Me.lblEstatus.Tag = ""
        Me.lblEstatus.Text = "&Estatus:"
        Me.lblEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grSolicitudTraspasos
        '
        '
        'grSolicitudTraspasos.EmbeddedNavigator
        '
        Me.grSolicitudTraspasos.EmbeddedNavigator.Name = ""
        Me.grSolicitudTraspasos.Location = New System.Drawing.Point(16, 345)
        Me.grSolicitudTraspasos.MainView = Me.grvSolicitudTraspasos
        Me.grSolicitudTraspasos.Name = "grSolicitudTraspasos"
        Me.grSolicitudTraspasos.Size = New System.Drawing.Size(440, 112)
        Me.grSolicitudTraspasos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grSolicitudTraspasos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSolicitudTraspasos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSolicitudTraspasos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSolicitudTraspasos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSolicitudTraspasos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSolicitudTraspasos.TabIndex = 19
        Me.grSolicitudTraspasos.Text = "SolicitudTraspasos"
        '
        'grvSolicitudTraspasos
        '
        Me.grvSolicitudTraspasos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcCantidad, Me.grcDepartamento, Me.grcGrupo})
        Me.grvSolicitudTraspasos.GridControl = Me.grSolicitudTraspasos
        Me.grvSolicitudTraspasos.Name = "grvSolicitudTraspasos"
        Me.grvSolicitudTraspasos.OptionsBehavior.Editable = False
        Me.grvSolicitudTraspasos.OptionsCustomization.AllowFilter = False
        Me.grvSolicitudTraspasos.OptionsCustomization.AllowGroup = False
        Me.grvSolicitudTraspasos.OptionsCustomization.AllowSort = False
        Me.grvSolicitudTraspasos.OptionsView.ShowGroupPanel = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Artículo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Width = 67
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.VisibleIndex = 0
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripción"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.VisibleIndex = 1
        Me.grcDescripcion.Width = 291
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.VisibleIndex = 2
        Me.grcCantidad.Width = 68
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        '
        'tmaSolicitudTraspasos
        '
        Me.tmaSolicitudTraspasos.BackColor = System.Drawing.Color.White
        Me.tmaSolicitudTraspasos.CanDelete = True
        Me.tmaSolicitudTraspasos.CanInsert = True
        Me.tmaSolicitudTraspasos.CanUpdate = True
        Me.tmaSolicitudTraspasos.Grid = Me.grSolicitudTraspasos
        Me.tmaSolicitudTraspasos.Location = New System.Drawing.Point(16, 321)
        Me.tmaSolicitudTraspasos.Name = "tmaSolicitudTraspasos"
        Me.tmaSolicitudTraspasos.Size = New System.Drawing.Size(440, 23)
        Me.tmaSolicitudTraspasos.TabIndex = 18
        Me.tmaSolicitudTraspasos.Title = ""
        Me.tmaSolicitudTraspasos.UpdateTitle = "un Registro"
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = True
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(128, 136)
        Me.lkpBodegaSalida.MultiSelect = False
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = ""
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = False
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(328, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 9
        Me.lkpBodegaSalida.Tag = ""
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lkpBodegaEntrada
        '
        Me.lkpBodegaEntrada.AllowAdd = False
        Me.lkpBodegaEntrada.AutoReaload = True
        Me.lkpBodegaEntrada.DataSource = Nothing
        Me.lkpBodegaEntrada.DefaultSearchField = ""
        Me.lkpBodegaEntrada.DisplayMember = "Descripcion"
        Me.lkpBodegaEntrada.EditValue = Nothing
        Me.lkpBodegaEntrada.Filtered = False
        Me.lkpBodegaEntrada.InitValue = Nothing
        Me.lkpBodegaEntrada.Location = New System.Drawing.Point(128, 88)
        Me.lkpBodegaEntrada.MultiSelect = False
        Me.lkpBodegaEntrada.Name = "lkpBodegaEntrada"
        Me.lkpBodegaEntrada.NullText = ""
        Me.lkpBodegaEntrada.PopupWidth = CType(400, Long)
        Me.lkpBodegaEntrada.ReadOnlyControl = False
        Me.lkpBodegaEntrada.Required = False
        Me.lkpBodegaEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaEntrada.SearchMember = ""
        Me.lkpBodegaEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaEntrada.SelectAll = False
        Me.lkpBodegaEntrada.Size = New System.Drawing.Size(328, 20)
        Me.lkpBodegaEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaEntrada.TabIndex = 5
        Me.lkpBodegaEntrada.Tag = ""
        Me.lkpBodegaEntrada.ToolTip = Nothing
        Me.lkpBodegaEntrada.ValueMember = "Bodega"
        '
        'lkpSucursalSalida
        '
        Me.lkpSucursalSalida.AllowAdd = False
        Me.lkpSucursalSalida.AutoReaload = False
        Me.lkpSucursalSalida.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursalSalida.DataSource = Nothing
        Me.lkpSucursalSalida.DefaultSearchField = ""
        Me.lkpSucursalSalida.DisplayMember = "nombre"
        Me.lkpSucursalSalida.EditValue = Nothing
        Me.lkpSucursalSalida.Enabled = False
        Me.lkpSucursalSalida.Filtered = False
        Me.lkpSucursalSalida.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursalSalida.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursalSalida.InitValue = Nothing
        Me.lkpSucursalSalida.Location = New System.Drawing.Point(128, 112)
        Me.lkpSucursalSalida.MultiSelect = False
        Me.lkpSucursalSalida.Name = "lkpSucursalSalida"
        Me.lkpSucursalSalida.NullText = ""
        Me.lkpSucursalSalida.PopupWidth = CType(400, Long)
        Me.lkpSucursalSalida.ReadOnlyControl = False
        Me.lkpSucursalSalida.Required = False
        Me.lkpSucursalSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursalSalida.SearchMember = ""
        Me.lkpSucursalSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursalSalida.SelectAll = False
        Me.lkpSucursalSalida.Size = New System.Drawing.Size(328, 20)
        Me.lkpSucursalSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursalSalida.TabIndex = 7
        Me.lkpSucursalSalida.Tag = ""
        Me.lkpSucursalSalida.ToolTip = Nothing
        Me.lkpSucursalSalida.ValueMember = "Sucursal"
        '
        'lkpSolicita
        '
        Me.lkpSolicita.AllowAdd = False
        Me.lkpSolicita.AutoReaload = False
        Me.lkpSolicita.DataSource = Nothing
        Me.lkpSolicita.DefaultSearchField = ""
        Me.lkpSolicita.DisplayMember = "nombre"
        Me.lkpSolicita.EditValue = Nothing
        Me.lkpSolicita.Filtered = False
        Me.lkpSolicita.InitValue = Nothing
        Me.lkpSolicita.Location = New System.Drawing.Point(128, 160)
        Me.lkpSolicita.MultiSelect = False
        Me.lkpSolicita.Name = "lkpSolicita"
        Me.lkpSolicita.NullText = ""
        Me.lkpSolicita.PopupWidth = CType(400, Long)
        Me.lkpSolicita.ReadOnlyControl = False
        Me.lkpSolicita.Required = False
        Me.lkpSolicita.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSolicita.SearchMember = ""
        Me.lkpSolicita.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSolicita.SelectAll = False
        Me.lkpSolicita.Size = New System.Drawing.Size(328, 20)
        Me.lkpSolicita.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSolicita.TabIndex = 11
        Me.lkpSolicita.Tag = "solicita"
        Me.lkpSolicita.ToolTip = Nothing
        Me.lkpSolicita.ValueMember = "empleado"
        '
        'txtCapturo
        '
        Me.txtCapturo.EditValue = ""
        Me.txtCapturo.Location = New System.Drawing.Point(128, 184)
        Me.txtCapturo.Name = "txtCapturo"
        '
        'txtCapturo.Properties
        '
        Me.txtCapturo.Properties.Enabled = False
        Me.txtCapturo.Properties.MaxLength = 50
        Me.txtCapturo.Size = New System.Drawing.Size(328, 20)
        Me.txtCapturo.TabIndex = 13
        Me.txtCapturo.Tag = "capturo"
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "PS"
        Me.cboStatus.Location = New System.Drawing.Point(128, 232)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Por Surtir", "PS", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Surtida", "SU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "CA", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(150, 20)
        Me.cboStatus.TabIndex = 17
        Me.cboStatus.Tag = "estatus"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 6
        Me.ToolBarButton1.Text = "Reimprimir"
        '
        'lkpSucursalEntrada
        '
        Me.lkpSucursalEntrada.AllowAdd = False
        Me.lkpSucursalEntrada.AutoReaload = False
        Me.lkpSucursalEntrada.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursalEntrada.DataSource = Nothing
        Me.lkpSucursalEntrada.DefaultSearchField = ""
        Me.lkpSucursalEntrada.DisplayMember = "nombre"
        Me.lkpSucursalEntrada.EditValue = Nothing
        Me.lkpSucursalEntrada.Enabled = False
        Me.lkpSucursalEntrada.Filtered = False
        Me.lkpSucursalEntrada.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursalEntrada.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursalEntrada.InitValue = Nothing
        Me.lkpSucursalEntrada.Location = New System.Drawing.Point(128, 64)
        Me.lkpSucursalEntrada.MultiSelect = False
        Me.lkpSucursalEntrada.Name = "lkpSucursalEntrada"
        Me.lkpSucursalEntrada.NullText = ""
        Me.lkpSucursalEntrada.PopupWidth = CType(400, Long)
        Me.lkpSucursalEntrada.ReadOnlyControl = False
        Me.lkpSucursalEntrada.Required = False
        Me.lkpSucursalEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursalEntrada.SearchMember = ""
        Me.lkpSucursalEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursalEntrada.SelectAll = False
        Me.lkpSucursalEntrada.Size = New System.Drawing.Size(328, 20)
        Me.lkpSucursalEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursalEntrada.TabIndex = 3
        Me.lkpSucursalEntrada.Tag = "sucursal_entrada"
        Me.lkpSucursalEntrada.ToolTip = Nothing
        Me.lkpSucursalEntrada.ValueMember = "Sucursal"
        '
        'lblSucursalOrigen
        '
        Me.lblSucursalOrigen.AutoSize = True
        Me.lblSucursalOrigen.Location = New System.Drawing.Point(22, 115)
        Me.lblSucursalOrigen.Name = "lblSucursalOrigen"
        Me.lblSucursalOrigen.Size = New System.Drawing.Size(97, 16)
        Me.lblSucursalOrigen.TabIndex = 6
        Me.lblSucursalOrigen.Tag = ""
        Me.lblSucursalOrigen.Text = "Sucursal Ori&gen:"
        Me.lblSucursalOrigen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAutorizo
        '
        Me.lkpAutorizo.AllowAdd = False
        Me.lkpAutorizo.AutoReaload = False
        Me.lkpAutorizo.DataSource = Nothing
        Me.lkpAutorizo.DefaultSearchField = ""
        Me.lkpAutorizo.DisplayMember = "nombre"
        Me.lkpAutorizo.EditValue = Nothing
        Me.lkpAutorizo.Filtered = False
        Me.lkpAutorizo.InitValue = Nothing
        Me.lkpAutorizo.Location = New System.Drawing.Point(128, 208)
        Me.lkpAutorizo.MultiSelect = False
        Me.lkpAutorizo.Name = "lkpAutorizo"
        Me.lkpAutorizo.NullText = ""
        Me.lkpAutorizo.PopupWidth = CType(400, Long)
        Me.lkpAutorizo.ReadOnlyControl = False
        Me.lkpAutorizo.Required = False
        Me.lkpAutorizo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAutorizo.SearchMember = ""
        Me.lkpAutorizo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAutorizo.SelectAll = False
        Me.lkpAutorizo.Size = New System.Drawing.Size(328, 20)
        Me.lkpAutorizo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAutorizo.TabIndex = 15
        Me.lkpAutorizo.Tag = "Autorizo"
        Me.lkpAutorizo.ToolTip = Nothing
        Me.lkpAutorizo.ValueMember = "empleado"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 210)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Tag = ""
        Me.Label2.Text = "Autori&zo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones_Salida
        '
        Me.lblObservaciones_Salida.AutoSize = True
        Me.lblObservaciones_Salida.Location = New System.Drawing.Point(32, 256)
        Me.lblObservaciones_Salida.Name = "lblObservaciones_Salida"
        Me.lblObservaciones_Salida.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones_Salida.TabIndex = 59
        Me.lblObservaciones_Salida.Tag = ""
        Me.lblObservaciones_Salida.Text = "&Observaciones:"
        Me.lblObservaciones_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(128, 256)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(328, 56)
        Me.txtObservaciones.TabIndex = 60
        Me.txtObservaciones.Tag = "observaciones"
        '
        'frmSolicitudTraspasos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(466, 464)
        Me.Controls.Add(Me.lblObservaciones_Salida)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lkpAutorizo)
        Me.Controls.Add(Me.lkpSucursalEntrada)
        Me.Controls.Add(Me.lblSucursalOrigen)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.txtCapturo)
        Me.Controls.Add(Me.lkpSolicita)
        Me.Controls.Add(Me.lkpSucursalSalida)
        Me.Controls.Add(Me.lkpBodegaEntrada)
        Me.Controls.Add(Me.lkpBodegaSalida)
        Me.Controls.Add(Me.lblSolicitud)
        Me.Controls.Add(Me.clcSolicitud)
        Me.Controls.Add(Me.lblBodega_Salida)
        Me.Controls.Add(Me.lblSucursal_Entrada)
        Me.Controls.Add(Me.lblBodega_Entrada)
        Me.Controls.Add(Me.lblSolicita)
        Me.Controls.Add(Me.lblCapturo)
        Me.Controls.Add(Me.lblFecha_Solicitud)
        Me.Controls.Add(Me.dteFecha_Solicitud)
        Me.Controls.Add(Me.lblEstatus)
        Me.Controls.Add(Me.grSolicitudTraspasos)
        Me.Controls.Add(Me.tmaSolicitudTraspasos)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmSolicitudTraspasos"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.tmaSolicitudTraspasos, 0)
        Me.Controls.SetChildIndex(Me.grSolicitudTraspasos, 0)
        Me.Controls.SetChildIndex(Me.lblEstatus, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Solicitud, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Solicitud, 0)
        Me.Controls.SetChildIndex(Me.lblCapturo, 0)
        Me.Controls.SetChildIndex(Me.lblSolicita, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Salida, 0)
        Me.Controls.SetChildIndex(Me.clcSolicitud, 0)
        Me.Controls.SetChildIndex(Me.lblSolicitud, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaSalida, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaEntrada, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursalSalida, 0)
        Me.Controls.SetChildIndex(Me.lkpSolicita, 0)
        Me.Controls.SetChildIndex(Me.txtCapturo, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.lblSucursalOrigen, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursalEntrada, 0)
        Me.Controls.SetChildIndex(Me.lkpAutorizo, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones_Salida, 0)
        CType(Me.clcSolicitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Solicitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSolicitudTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSolicitudTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSolicitudTraspasos As VillarrealBusiness.clsSolicitudTraspasos
    Friend oSolicitudTraspasosDetalle As VillarrealBusiness.clsSolicitudTraspasosDetalle
    Private oEmpleados As New VillarrealBusiness.clsEmpleados
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oSucursales As New VillarrealBusiness.clsSucursales

    Private ReadOnly Property BodegaSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get
    End Property
    Private ReadOnly Property BodegaEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaEntrada)
        End Get
    End Property
    Private ReadOnly Property Solicita() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSolicita)
        End Get
    End Property
    Private ReadOnly Property Autorizo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAutorizo)
        End Get
    End Property
    Private ReadOnly Property sucursal_origen() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(lkpSucursalSalida)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmSolicitudTraspasos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmSolicitudTraspasos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmSolicitudTraspasos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        If Action = Actions.Insert Then
            ImprimeSolicitudTraspaso(Me.clcSolicitud.EditValue)
        End If
    End Sub

    Private Sub frmSolicitudTraspasos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oSolicitudTraspasos.Insertar(Me.DataSource, BodegaEntrada, BodegaSalida)

            Case Actions.Update
                Response = oSolicitudTraspasos.Actualizar(Me.DataSource, BodegaEntrada, BodegaSalida)

            Case Actions.Delete
                Response = oSolicitudTraspasos.Eliminar(clcSolicitud.Value)

        End Select
    End Sub
    Private Sub frmSolicitudTraspasos_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaSolicitudTraspasos
            .MoveFirst()

            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        If Not Response.ErrorFound Then
                            Response = oSolicitudTraspasosDetalle.Insertar(.SelectedRow, Me.clcSolicitud.EditValue)
                        End If

                    Case Actions.Update
                        If Not Response.ErrorFound Then oSolicitudTraspasosDetalle.Actualizar(.SelectedRow, Me.clcSolicitud.EditValue)

                    Case Actions.Delete
                        If Action = Actions.Update Then
                            If Not Response.ErrorFound Then oSolicitudTraspasosDetalle.Eliminar(Me.clcSolicitud.EditValue, .Item("articulo"))
                        End If
                End Select

                .MoveNext()
            Loop
        End With

    End Sub
    Private Sub frmSolicitudTraspasos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        Dim oDataSet As DataSet
        Response = oSolicitudTraspasos.DespliegaDatos(OwnerForm.Value("solicitud"))

        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            Me.lkpBodegaEntrada.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_entrada")
            Me.lkpBodegaSalida.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_salida")
            Me.txtObservaciones.Text = oDataSet.Tables(0).Rows(0).Item("Observaciones")
        End If

        If Not Response.ErrorFound Then Response = oSolicitudTraspasosDetalle.Listado(OwnerForm.Value("solicitud"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaSolicitudTraspasos.DataSource = oDataSet
        End If


    End Sub
    Private Sub frmSolicitudTraspasos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSolicitudTraspasos = New VillarrealBusiness.clsSolicitudTraspasos
        oSolicitudTraspasosDetalle = New VillarrealBusiness.clsSolicitudTraspasosDetalle

        With Me.tmaSolicitudTraspasos
            .UpdateTitle = "un Artículo"
            .UpdateForm = New frmSolicitudTraspasosDetalle
            .AddColumn("articulo")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("departamento", "System.Int32")
            .AddColumn("grupo", "System.Int32")
            .AddColumn("modelo")
        End With

        Select Case Action
            Case Actions.Insert
                Me.dteFecha_Solicitud.EditValue = CDate(TINApp.FechaServidor)
                Me.txtCapturo.Text = CStr(TINApp.Connection.UserName)
                Me.lkpBodegaEntrada.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(2).Visible = False
            Case Actions.Update
                If CStr(OwnerForm.Value("estatus")) <> "PS" Then
                    Me.tbrTools.Buttons(0).Enabled = False
                    Me.tbrTools.Buttons(0).Visible = False
                    Me.tbrTools.Buttons(2).Enabled = False
                    Me.tbrTools.Buttons(2).Visible = False
                    Me.EnabledEdit(False)
                End If
            Case Actions.Delete
                If CStr(OwnerForm.Value("estatus")) <> "PS" Then
                    Me.tbrTools.Buttons(0).Enabled = False
                    Me.tbrTools.Buttons(0).Visible = False
                    Me.tbrTools.Buttons(2).Enabled = False
                    Me.tbrTools.Buttons(2).Visible = False

                    MsgBox("No se puede eliminar: Su estatus es " + CStr(OwnerForm.Value("estatus_vista")))
                End If
        End Select
    End Sub
    Private Sub frmSolicitudTraspasos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oSolicitudTraspasos.Validacion(Action, Me.dteFecha_Solicitud.Text, BodegaSalida, BodegaEntrada, Solicita, CType(Me.grSolicitudTraspasos.DataSource, DataView), Autorizo)
    End Sub

    Private Sub frmSolicitudTraspasos_Localize() Handles MyBase.Localize
        Find("solicitud", CType(Me.clcSolicitud.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup(sucursal_origen, False)
        'Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged
        If Me.lkpBodegaSalida.EditValue Is Nothing Then Exit Sub

        'Me.lkpBodegaEntrada.EditValue = Nothing
        CargarBodegasOrigen()
        Me.lkpSucursalSalida.EditValue = Me.lkpBodegaSalida.GetValue("sucursal")
    End Sub

    Private Sub lkpBodegaEntrada_Format() Handles lkpBodegaEntrada.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaEntrada)
    End Sub
    Private Sub lkpBodegaEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaEntrada.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaEntrada_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpBodegaEntrada.EditValueChanged
        If Me.lkpBodegaEntrada.EditValue Is Nothing Then Exit Sub

        'Me.lkpBodegaSalida.EditValue = Nothing
        'CargarBodegasOrigen()
        Me.lkpSucursalEntrada.EditValue = Me.lkpBodegaEntrada.GetValue("sucursal")
    End Sub

    Private Sub lkpSucursalEntrada_Format() Handles lkpSucursalSalida.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursalEntrada)
    End Sub
    Private Sub lkpSucursalEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpSucursalEntrada.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursalEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursalSalida_Format() Handles lkpSucursalEntrada.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursalSalida)
    End Sub
    Private Sub lkpSucursalSalida_LoadData(ByVal Initialize As Boolean) Handles lkpSucursalSalida.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursalSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub



    Private Sub lkpSolicita_Format() Handles lkpSolicita.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpSolicita)
    End Sub
    Private Sub lkpSolicita_LoadData(ByVal Initialize As Boolean) Handles lkpSolicita.LoadData
        Dim response As Events
        response = oEmpleados.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSolicita.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Reimprimir" Then
            ImprimeSolicitudTraspaso(Me.clcSolicitud.EditValue)
        End If
    End Sub


    Private Sub lkpRecibe_Format() Handles lkpAutorizo.Format
        Comunes.clsFormato.for_empleados_grl(Me.lkpAutorizo)
    End Sub
    Private Sub lkpRecibe_LoadData(ByVal Initialize As Boolean) Handles lkpAutorizo.LoadData
        Dim response As Events
        response = oEmpleados.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpAutorizo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarBodegasOrigen()
        Dim Response As New Events
        Response = oBodegas.BodegasDestino(BodegaEntrada)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub ImprimeSolicitudTraspaso(ByVal solicitud As Long)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.SolicitudTraspasos(solicitud)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La Solicitud de Traspaso No Puede Mostrarse")
            Else
                Dim oDataSet As DataSet
                Dim oReport As New rptSolicitudTraspasos

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Solicitud de Traspaso", oReport, , , , True)
            End If

        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

#End Region

   
    Private Sub frmSolicitudTraspasos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MENUOPTION.NAME, "FECHA_SOL_TRA") Then
            Me.dteFecha_Solicitud.Enabled = True
        Else
            Me.dteFecha_Solicitud.Enabled = False
        End If
    End Sub
End Class
