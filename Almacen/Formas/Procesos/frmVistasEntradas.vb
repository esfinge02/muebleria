Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias


Public Class frmVistasEntradas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Vista_Entrada As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Vista_Entrada As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFolio_Vista_Salida As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPersona_Devuelve As System.Windows.Forms.Label
    Friend WithEvents txtPersona_Devuelve As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPersona_Captura As System.Windows.Forms.Label
    Friend WithEvents txtPersona_Captura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grVistasEntradas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVistasEntradas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lkpFolioVistaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptRepChkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rpLkpBodegas As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcManejaSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbrImprimir As System.Windows.Forms.ToolBarButton

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVistasEntradas))
        Me.lblFolio_Vista_Entrada = New System.Windows.Forms.Label
        Me.clcFolio_Vista_Entrada = New Dipros.Editors.TINCalcEdit
        Me.lblFolio_Vista_Salida = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblPersona_Devuelve = New System.Windows.Forms.Label
        Me.txtPersona_Devuelve = New DevExpress.XtraEditors.TextEdit
        Me.lblPersona_Captura = New System.Windows.Forms.Label
        Me.txtPersona_Captura = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grVistasEntradas = New DevExpress.XtraGrid.GridControl
        Me.grvVistasEntradas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptRepChkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rpLkpBodegas = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcManejaSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lkpFolioVistaSalida = New Dipros.Editors.TINMultiLookup
        Me.tbrImprimir = New System.Windows.Forms.ToolBarButton
        CType(Me.clcFolio_Vista_Entrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersona_Devuelve.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersona_Captura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grVistasEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVistasEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptRepChkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rpLkpBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrImprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4136, 28)
        '
        'lblFolio_Vista_Entrada
        '
        Me.lblFolio_Vista_Entrada.AutoSize = True
        Me.lblFolio_Vista_Entrada.Location = New System.Drawing.Point(58, 40)
        Me.lblFolio_Vista_Entrada.Name = "lblFolio_Vista_Entrada"
        Me.lblFolio_Vista_Entrada.Size = New System.Drawing.Size(82, 16)
        Me.lblFolio_Vista_Entrada.TabIndex = 0
        Me.lblFolio_Vista_Entrada.Tag = ""
        Me.lblFolio_Vista_Entrada.Text = "Folio Entrada:"
        Me.lblFolio_Vista_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Vista_Entrada
        '
        Me.clcFolio_Vista_Entrada.EditValue = "0"
        Me.clcFolio_Vista_Entrada.Location = New System.Drawing.Point(144, 40)
        Me.clcFolio_Vista_Entrada.MaxValue = 0
        Me.clcFolio_Vista_Entrada.MinValue = 0
        Me.clcFolio_Vista_Entrada.Name = "clcFolio_Vista_Entrada"
        '
        'clcFolio_Vista_Entrada.Properties
        '
        Me.clcFolio_Vista_Entrada.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Vista_Entrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Vista_Entrada.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Vista_Entrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Vista_Entrada.Properties.Enabled = False
        Me.clcFolio_Vista_Entrada.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Vista_Entrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Vista_Entrada.Size = New System.Drawing.Size(80, 19)
        Me.clcFolio_Vista_Entrada.TabIndex = 1
        Me.clcFolio_Vista_Entrada.Tag = "folio_vista_entrada"
        '
        'lblFolio_Vista_Salida
        '
        Me.lblFolio_Vista_Salida.AutoSize = True
        Me.lblFolio_Vista_Salida.Location = New System.Drawing.Point(68, 63)
        Me.lblFolio_Vista_Salida.Name = "lblFolio_Vista_Salida"
        Me.lblFolio_Vista_Salida.Size = New System.Drawing.Size(72, 16)
        Me.lblFolio_Vista_Salida.TabIndex = 2
        Me.lblFolio_Vista_Salida.Tag = ""
        Me.lblFolio_Vista_Salida.Text = "&Folio Salida:"
        Me.lblFolio_Vista_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(84, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(144, 87)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(300, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(496, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 14
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "27/12/2007"
        Me.dteFecha.Location = New System.Drawing.Point(544, 39)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 15
        Me.dteFecha.Tag = "fecha"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(93, 112)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 6
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(144, 111)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(300, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 7
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblPersona_Devuelve
        '
        Me.lblPersona_Devuelve.AutoSize = True
        Me.lblPersona_Devuelve.Location = New System.Drawing.Point(32, 136)
        Me.lblPersona_Devuelve.Name = "lblPersona_Devuelve"
        Me.lblPersona_Devuelve.Size = New System.Drawing.Size(108, 16)
        Me.lblPersona_Devuelve.TabIndex = 8
        Me.lblPersona_Devuelve.Tag = ""
        Me.lblPersona_Devuelve.Text = "&Persona Devuelve:"
        Me.lblPersona_Devuelve.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersona_Devuelve
        '
        Me.txtPersona_Devuelve.EditValue = ""
        Me.txtPersona_Devuelve.Location = New System.Drawing.Point(144, 135)
        Me.txtPersona_Devuelve.Name = "txtPersona_Devuelve"
        '
        'txtPersona_Devuelve.Properties
        '
        Me.txtPersona_Devuelve.Properties.MaxLength = 50
        Me.txtPersona_Devuelve.Size = New System.Drawing.Size(300, 20)
        Me.txtPersona_Devuelve.TabIndex = 9
        Me.txtPersona_Devuelve.Tag = "persona_devuelve"
        '
        'lblPersona_Captura
        '
        Me.lblPersona_Captura.AutoSize = True
        Me.lblPersona_Captura.Location = New System.Drawing.Point(40, 160)
        Me.lblPersona_Captura.Name = "lblPersona_Captura"
        Me.lblPersona_Captura.TabIndex = 10
        Me.lblPersona_Captura.Tag = ""
        Me.lblPersona_Captura.Text = "&Persona Captura:"
        Me.lblPersona_Captura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersona_Captura
        '
        Me.txtPersona_Captura.EditValue = ""
        Me.txtPersona_Captura.Location = New System.Drawing.Point(144, 159)
        Me.txtPersona_Captura.Name = "txtPersona_Captura"
        '
        'txtPersona_Captura.Properties
        '
        Me.txtPersona_Captura.Properties.Enabled = False
        Me.txtPersona_Captura.Properties.MaxLength = 15
        Me.txtPersona_Captura.Size = New System.Drawing.Size(90, 20)
        Me.txtPersona_Captura.TabIndex = 11
        Me.txtPersona_Captura.Tag = "persona_captura"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(51, 184)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 12
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(144, 183)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(304, 38)
        Me.txtObservaciones.TabIndex = 13
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grVistasEntradas
        '
        '
        'grVistasEntradas.EmbeddedNavigator
        '
        Me.grVistasEntradas.EmbeddedNavigator.Name = ""
        Me.grVistasEntradas.Location = New System.Drawing.Point(8, 232)
        Me.grVistasEntradas.MainView = Me.grvVistasEntradas
        Me.grVistasEntradas.Name = "grVistasEntradas"
        Me.grVistasEntradas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptRepChkSeleccionar, Me.rpLkpBodegas})
        Me.grVistasEntradas.Size = New System.Drawing.Size(664, 232)
        Me.grVistasEntradas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grVistasEntradas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasEntradas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasEntradas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasEntradas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasEntradas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVistasEntradas.TabIndex = 16
        Me.grVistasEntradas.Text = "VistasEntradas"
        '
        'grvVistasEntradas
        '
        Me.grvVistasEntradas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcPartida, Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcNumeroSerie, Me.grcBodega, Me.grcManejaSerie, Me.grcCosto})
        Me.grvVistasEntradas.GridControl = Me.grVistasEntradas
        Me.grvVistasEntradas.Name = "grvVistasEntradas"
        Me.grvVistasEntradas.OptionsCustomization.AllowFilter = False
        Me.grvVistasEntradas.OptionsCustomization.AllowGroup = False
        Me.grvVistasEntradas.OptionsCustomization.AllowSort = False
        Me.grvVistasEntradas.OptionsView.ShowGroupPanel = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.rptRepChkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 92
        '
        'rptRepChkSeleccionar
        '
        Me.rptRepChkSeleccionar.AutoHeight = False
        Me.rptRepChkSeleccionar.Name = "rptRepChkSeleccionar"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.VisibleIndex = 1
        Me.grcPartida.Width = 65
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 2
        Me.grcArticulo.Width = 97
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 3
        Me.grcModelo.Width = 97
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 4
        Me.grcDescripcion.Width = 97
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "Num. Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroSerie.VisibleIndex = 5
        Me.grcNumeroSerie.Width = 97
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.ColumnEdit = Me.rpLkpBodegas
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.VisibleIndex = 6
        Me.grcBodega.Width = 105
        '
        'rpLkpBodegas
        '
        Me.rpLkpBodegas.AutoHeight = False
        Me.rpLkpBodegas.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rpLkpBodegas.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bodega", "Bodega", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripci�n", 50)})
        Me.rpLkpBodegas.DisplayMember = "descripcion"
        Me.rpLkpBodegas.Name = "rpLkpBodegas"
        Me.rpLkpBodegas.PopupWidth = 250
        Me.rpLkpBodegas.ValueMember = "bodega"
        '
        'grcManejaSerie
        '
        Me.grcManejaSerie.Caption = "Maneja Serie"
        Me.grcManejaSerie.ColumnEdit = Me.rptRepChkSeleccionar
        Me.grcManejaSerie.FieldName = "maneja_series"
        Me.grcManejaSerie.Name = "grcManejaSerie"
        Me.grcManejaSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcManejaSerie.VisibleIndex = 7
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto.VisibleIndex = 8
        '
        'lkpFolioVistaSalida
        '
        Me.lkpFolioVistaSalida.AllowAdd = False
        Me.lkpFolioVistaSalida.AutoReaload = False
        Me.lkpFolioVistaSalida.DataSource = Nothing
        Me.lkpFolioVistaSalida.DefaultSearchField = ""
        Me.lkpFolioVistaSalida.DisplayMember = "folio_vista_salida"
        Me.lkpFolioVistaSalida.EditValue = Nothing
        Me.lkpFolioVistaSalida.Filtered = False
        Me.lkpFolioVistaSalida.InitValue = Nothing
        Me.lkpFolioVistaSalida.Location = New System.Drawing.Point(144, 63)
        Me.lkpFolioVistaSalida.MultiSelect = False
        Me.lkpFolioVistaSalida.Name = "lkpFolioVistaSalida"
        Me.lkpFolioVistaSalida.NullText = ""
        Me.lkpFolioVistaSalida.PopupWidth = CType(400, Long)
        Me.lkpFolioVistaSalida.ReadOnlyControl = False
        Me.lkpFolioVistaSalida.Required = False
        Me.lkpFolioVistaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioVistaSalida.SearchMember = ""
        Me.lkpFolioVistaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioVistaSalida.SelectAll = False
        Me.lkpFolioVistaSalida.Size = New System.Drawing.Size(96, 20)
        Me.lkpFolioVistaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolioVistaSalida.TabIndex = 3
        Me.lkpFolioVistaSalida.Tag = "folio_vista_salida"
        Me.lkpFolioVistaSalida.ToolTip = Nothing
        Me.lkpFolioVistaSalida.ValueMember = "folio_vista_salida"
        '
        'tbrImprimir
        '
        Me.tbrImprimir.Text = "Imprimir"
        Me.tbrImprimir.ToolTipText = "Imprime la Entrada por Vista"
        '
        'frmVistasEntradas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(682, 488)
        Me.Controls.Add(Me.lkpFolioVistaSalida)
        Me.Controls.Add(Me.lblFolio_Vista_Entrada)
        Me.Controls.Add(Me.clcFolio_Vista_Entrada)
        Me.Controls.Add(Me.lblFolio_Vista_Salida)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblPersona_Devuelve)
        Me.Controls.Add(Me.txtPersona_Devuelve)
        Me.Controls.Add(Me.lblPersona_Captura)
        Me.Controls.Add(Me.txtPersona_Captura)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.grVistasEntradas)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Name = "frmVistasEntradas"
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.grVistasEntradas, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtPersona_Captura, 0)
        Me.Controls.SetChildIndex(Me.lblPersona_Captura, 0)
        Me.Controls.SetChildIndex(Me.txtPersona_Devuelve, 0)
        Me.Controls.SetChildIndex(Me.lblPersona_Devuelve, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Vista_Salida, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Vista_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Vista_Entrada, 0)
        Me.Controls.SetChildIndex(Me.lkpFolioVistaSalida, 0)
        CType(Me.clcFolio_Vista_Entrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersona_Devuelve.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersona_Captura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grVistasEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVistasEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptRepChkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rpLkpBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oVistasEntradas As VillarrealBusiness.clsVistasEntradas
    Private oVistasEntradasDetalle As VillarrealBusiness.clsVistasEntradasDetalle

    Private oVistasSalidas As VillarrealBusiness.clsVistasSalidas
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oCliente As VillarrealBusiness.clsClientes
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oVariables As VillarrealBusiness.clsVariables

    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oReportes As VillarrealBusiness.Reportes

    Private oHistoricoCostos As VillarrealBusiness.clsHisCostos


    ReadOnly Property FolioSalida() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolioVistaSalida)
        End Get
    End Property
    ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property ConceptoVistasEntradas() As String
        Get
            Return oVariables.TraeDatos("concepto_vistas_entrada", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Private folio_movimiento As Long = 0
    Private Guardado As Boolean = False
    Private bdespliega As Boolean = False

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmVistasEntradas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = Guardar()
    End Sub

    Private Sub frmVistasEntradas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        bdespliega = True

        Response = oVistasEntradas.DespliegaDatos(OwnerForm.Value("folio_vista_entrada"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet



            Response = oVistasEntradasDetalle.DespliegaDatos(OwnerForm.Value("folio_vista_entrada"))
            oDataSet = Response.Value
            Me.grVistasEntradas.DataSource = oDataSet.Tables(0)
            Me.grVistasEntradas.Enabled = False
        End If

        ' SI ES UNA ACCION DE ACTUALIZAR.. SE INHABILITA EL BOTON PARA SOLO VER DATOS...
        Me.tbrTools.Buttons(0).Enabled = False

        bdespliega = False
    End Sub

    Private Sub frmVistasEntradas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVistasEntradas = New VillarrealBusiness.clsVistasEntradas
        oVistasEntradasDetalle = New VillarrealBusiness.clsVistasEntradasDetalle
        oVistasSalidas = New VillarrealBusiness.clsVistasSalidas
        oSucursales = New VillarrealBusiness.clsSucursales
        oCliente = New VillarrealBusiness.clsClientes
        oBodegas = New VillarrealBusiness.clsBodegas
        oVariables = New VillarrealBusiness.clsVariables
        oReportes = New VillarrealBusiness.Reportes
        oHistoricoCostos = New VillarrealBusiness.clsHisCostos

        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries


        Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        Me.txtPersona_Captura.Text = TINApp.Connection.User
        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)


        Dim oDataSet As DataSet
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.rpLkpBodegas.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If


        Select Case Action
            Case Actions.Insert
                Me.tbrImprimir.Enabled = False
                Me.tbrImprimir.Visible = False
            Case Actions.Update
                Me.lkpFolioVistaSalida.Enabled = False
                Me.tbrImprimir.Enabled = True
                Me.tbrImprimir.Visible = True
            Case Actions.Delete
                Me.lkpFolioVistaSalida.Enabled = False
                Me.tbrImprimir.Enabled = False
                Me.tbrImprimir.Visible = False
        End Select
    End Sub

    Private Sub frmVistasEntradas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.grvVistasEntradas.CloseEditor()
        Me.grvVistasEntradas.UpdateCurrentRow()
        Response = oVistasEntradas.Validacion(Action, Me.FolioSalida, Me.txtPersona_Devuelve.Text, CType(Me.grvVistasEntradas.DataSource, DataView).Table)
    End Sub

    Private Sub frmVistasEntradas_Localize() Handles MyBase.Localize
        Find("folio_vista_entrada", Me.clcFolio_Vista_Entrada.Value)

    End Sub

    Private Sub frmVistasEntradas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oCliente.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub lkpFolio_Vista_Salida_LoadData(ByVal Initialize As Boolean) Handles lkpFolioVistaSalida.LoadData
        Dim Response As New Events
        Response = oVistasSalidas.Lookup(True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFolioVistaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFolio_Vista_Salida_Format() Handles lkpFolioVistaSalida.Format
        Comunes.clsFormato.for_vistas_salidas_grl(Me.lkpFolioVistaSalida)
    End Sub
    Private Sub lkpFolioVistaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolioVistaSalida.EditValueChanged
        If FolioSalida <= 0 Then Exit Sub
        Me.lkpCliente.EditValue = Me.lkpFolioVistaSalida.GetValue("cliente")

        'DAM - 02-01-08 Se Valida Si aun no ha sido generada la entrada 
        If bdespliega = False Then
            CargarArticulosVistasSinEntregar(FolioSalida)
            Me.grVistasEntradas.Enabled = True
        End If


    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbrImprimir Then
            ImprimirReporte()
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarArticulosVistasSinEntregar(ByVal folio_vista_salida As Long)
        Dim response As Events

        response = oVistasEntradasDetalle.ArticulosSinEntregarFacturarVistas(folio_vista_salida)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grVistasEntradas.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

    End Sub
    Private Function Guardar() As Events
        Dim response As Events

        Me.grvVistasEntradas.CloseEditor()
        Me.grvVistasEntradas.UpdateCurrentRow()

        TINApp.Connection.Begin()

        Select Case Action
            Case Actions.Insert
                Response = oVistasEntradas.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oVistasEntradas.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oVistasEntradas.Eliminar(clcFolio_Vista_Entrada.Value)

        End Select


        Dim i As Long = 0
        Dim partida As Long
        Dim articulo As Long
        Dim numero_serie As String
        Dim bodega_entrada As String
        Dim maneja_series As Boolean
        Dim costo As Double
        Dim folio_movimiento_detalle_consecutivo As Long


        For i = 0 To Me.grvVistasEntradas.RowCount - 1
            If Me.grvVistasEntradas.GetRowCellValue(i, Me.grcSeleccionar) = True Then
                Guardado = False
                partida = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcPartida)
                articulo = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcArticulo)
                numero_serie = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcNumeroSerie)
                bodega_entrada = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcBodega)
                maneja_series = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcManejaSerie)
                costo = Me.grvVistasEntradas.GetRowCellValue(i, Me.grcCosto)

                Dim folio_his_costos As Long = -1

                Select Case Action
                    Case Actions.Insert

                        'DAM 27/dic/07 Primero Valido Existencias y genero los Movimientos al Inventario
                        response = oMovimientosInventarioDetalle.ValidacantidadArticulos(partida, articulo, 1, bodega_entrada, ConceptoVistasEntradas, folio_movimiento, "I", "Entrada Vistas")

                        If Not response.ErrorFound Then
                            '    'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                            GeneraMovimientoInventario(Action, response, Sucursal, bodega_entrada, Me.ConceptoVistasEntradas, folio_movimiento, Me.dteFecha.DateTime, -1, "", Me.clcFolio_Vista_Entrada.Value, "Movimiento generado desde Salida a Vistas")

                            '==============================================================

                          

                            'response = oHistoricoCostos.Insertar(folio_his_costos, CDate(TINApp.FechaServidor), articulo, bodega_entrada, 1, costo, 1)
                            'If response.ErrorFound Then
                            '    response.Message = "Error al Actualizar el Historico de Costos"

                            'End If

                            folio_his_costos = -1

                            'Validacion para saber si el folio de la capa corresponde con el articulo
                            If folio_his_costos <> -1 Then
                                response = oHistoricoCostos.ExisteFolioArticulo(folio_his_costos, articulo)

                                If Not response.ErrorFound Then

                                    Dim filas As Int32
                                    filas = CType(response.Value, Integer)

                                    If filas <= 0 Then
                                        response.Message = "La capa " + folio_his_costos.ToString + " no corresponde con el articulo: " + articulo.ToString

                                    End If

                                End If
                            End If


                            '==============================================================


                            If Not response.ErrorFound Then response = oMovimientosInventarioDetalle.Insertar(Sucursal, bodega_entrada, Me.ConceptoVistasEntradas, folio_movimiento, dteFecha.EditValue, partida, articulo, 1, costo, 1 * costo, folio_his_costos, folio_movimiento_detalle_consecutivo)

                            If maneja_series And Comunes.clsUtilerias.UsarSeries Then
                                'AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES
                                If Not response.ErrorFound Then response = oMovimientosInventarioDetalleSeries.Insertar(Sucursal, bodega_entrada, Me.ConceptoVistasEntradas, folio_movimiento, partida, numero_serie, articulo)

                                ' SE MANDA LA SERIE A LA BODEGA DE ENTRADA
                                If Not response.ErrorFound Then response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(articulo, numero_serie, bodega_entrada)
                            End If

                            If Not response.ErrorFound Then response = Me.oVistasEntradasDetalle.Insertar(Me.clcFolio_Vista_Entrada.Value, partida, articulo, numero_serie, bodega_entrada, Me.ConceptoVistasEntradas, folio_movimiento)
                        End If


                        If response.ErrorFound Then
                            TINApp.Connection.Rollback()
                            Return response
                        End If
                End Select
            End If
        Next



        TINApp.Connection.Commit()

        If Action = Actions.Insert Then ImprimirReporte()

    End Function
    Private Function GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String)
        If Guardado = False Then
            Select Case accion
                Case Actions.Insert, Actions.Delete
                    response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
                Case Actions.Update
                    response = oMovimientosInventario.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
            End Select

        End If
    End Function
    Private Function ImprimirReporte()
        Dim response As New Events
        response = oReportes.EntradaVistas(Me.clcFolio_Vista_Entrada.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Entradas por Vista no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptEntradaVistas

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Salida por Vistas", oReport, , , , True)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Function
#End Region


    Private Sub frmVistasEntradas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.OwnerForm.MENUOPTION.NAME, "FECHA_VIS_ENT") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If

    End Sub

End Class
