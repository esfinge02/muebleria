Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias


Public Class frmEntradasSalidasReparacion
    Inherits Dipros.Windows.frmTINForm

    Private oEntradasSalidasReparacion As New VillarrealBusiness.clsEntradasSalidasReparacion
    Private oMovimientoInventario As New VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientoInventarioDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oVariables As New VillarrealBusiness.clsVariables
    Private oBodegueros As New VillarrealBusiness.clsBodegueros



    Private folio_movimiento_salida As Long = 0
    Private folio_movimiento_entrada As Long = 0
    Private Articulo As Long = 0
    Private Ultimo_Costo As Double = 0.0
    Private Bodega_Orden As String = ""
    Private usuario_salida_reparacion As String = ""
    Private usuario_entrada_reparacion As String = ""


    Private ReadOnly Property BodegueroSalida() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBodegueroSalida)
        End Get
    End Property
    Private ReadOnly Property BodegueroEntrada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBodegueroEntrada)
        End Get
    End Property

    Private ReadOnly Property ConceptoSalidaReparacion() As String
        Get
            Return oVariables.TraeDatos("concepto_salida_reparacion", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property ConceptoEntradaReparacion() As String
        Get
            Return oVariables.TraeDatos("concepto_entrada_reparacion", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblOrden_Servicio As System.Windows.Forms.Label
    Friend WithEvents clcOrden_Servicio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTipo_Servicio As System.Windows.Forms.Label
    Friend WithEvents cboTipo_Servicio As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionCorta As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblNumero_Serie As System.Windows.Forms.Label
    Friend WithEvents txtNumero_Serie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblArticuloValor As System.Windows.Forms.Label
    Friend WithEvents chkSalidaReparacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkEntradaReparacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dteFechaEntradaReparacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtUsuarioEntradaReparacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUsuarioSalidaReparacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dteFechaSalidaReparacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtObservacionesEntrada As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtObservacionesSalida As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblBodegueroEntrada As System.Windows.Forms.Label
    Friend WithEvents lkpBodegueroEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lkpBodegueroSalida As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEntradasSalidasReparacion))
        Me.lblOrden_Servicio = New System.Windows.Forms.Label
        Me.clcOrden_Servicio = New Dipros.Editors.TINCalcEdit
        Me.lblTipo_Servicio = New System.Windows.Forms.Label
        Me.cboTipo_Servicio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblDescripcionCorta = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblNumero_Serie = New System.Windows.Forms.Label
        Me.txtNumero_Serie = New DevExpress.XtraEditors.TextEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblArticuloValor = New System.Windows.Forms.Label
        Me.chkSalidaReparacion = New DevExpress.XtraEditors.CheckEdit
        Me.chkEntradaReparacion = New DevExpress.XtraEditors.CheckEdit
        Me.txtUsuarioEntradaReparacion = New DevExpress.XtraEditors.TextEdit
        Me.txtUsuarioSalidaReparacion = New DevExpress.XtraEditors.TextEdit
        Me.dteFechaSalidaReparacion = New DevExpress.XtraEditors.DateEdit
        Me.dteFechaEntradaReparacion = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtObservacionesSalida = New DevExpress.XtraEditors.MemoEdit
        Me.txtObservacionesEntrada = New DevExpress.XtraEditors.MemoEdit
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.lkpBodegueroSalida = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.lblBodegueroEntrada = New System.Windows.Forms.Label
        Me.lkpBodegueroEntrada = New Dipros.Editors.TINMultiLookup
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        CType(Me.clcOrden_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumero_Serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.chkSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesSalida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3251, 28)
        '
        'lblOrden_Servicio
        '
        Me.lblOrden_Servicio.AutoSize = True
        Me.lblOrden_Servicio.Location = New System.Drawing.Point(11, 40)
        Me.lblOrden_Servicio.Name = "lblOrden_Servicio"
        Me.lblOrden_Servicio.Size = New System.Drawing.Size(106, 16)
        Me.lblOrden_Servicio.TabIndex = 0
        Me.lblOrden_Servicio.Tag = ""
        Me.lblOrden_Servicio.Text = "&Orden de servicio:"
        Me.lblOrden_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOrden_Servicio
        '
        Me.clcOrden_Servicio.EditValue = "0"
        Me.clcOrden_Servicio.Location = New System.Drawing.Point(125, 40)
        Me.clcOrden_Servicio.MaxValue = 0
        Me.clcOrden_Servicio.MinValue = 0
        Me.clcOrden_Servicio.Name = "clcOrden_Servicio"
        '
        'clcOrden_Servicio.Properties
        '
        Me.clcOrden_Servicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden_Servicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden_Servicio.Properties.Enabled = False
        Me.clcOrden_Servicio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOrden_Servicio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcOrden_Servicio.Size = New System.Drawing.Size(72, 19)
        Me.clcOrden_Servicio.TabIndex = 1
        Me.clcOrden_Servicio.Tag = "orden_servicio"
        '
        'lblTipo_Servicio
        '
        Me.lblTipo_Servicio.AutoSize = True
        Me.lblTipo_Servicio.Location = New System.Drawing.Point(8, 16)
        Me.lblTipo_Servicio.Name = "lblTipo_Servicio"
        Me.lblTipo_Servicio.Size = New System.Drawing.Size(95, 16)
        Me.lblTipo_Servicio.TabIndex = 59
        Me.lblTipo_Servicio.Tag = ""
        Me.lblTipo_Servicio.Text = "&Tipo de servicio:"
        Me.lblTipo_Servicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Servicio
        '
        Me.cboTipo_Servicio.EditValue = "G"
        Me.cboTipo_Servicio.Location = New System.Drawing.Point(104, 16)
        Me.cboTipo_Servicio.Name = "cboTipo_Servicio"
        '
        'cboTipo_Servicio.Properties
        '
        Me.cboTipo_Servicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Servicio.Properties.Enabled = False
        Me.cboTipo_Servicio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Garant�a", "G", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Armado", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Instalaci�n", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Reparaci�n", "R", -1)})
        Me.cboTipo_Servicio.Size = New System.Drawing.Size(120, 20)
        Me.cboTipo_Servicio.TabIndex = 60
        Me.cboTipo_Servicio.Tag = "tipo_servicio"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(552, 18)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 63
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "31/05/2006"
        Me.dteFecha.Location = New System.Drawing.Point(600, 16)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 64
        Me.dteFecha.Tag = "fecha"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 69
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Descripci�n:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcionCorta
        '
        Me.lblDescripcionCorta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDescripcionCorta.Enabled = False
        Me.lblDescripcionCorta.Location = New System.Drawing.Point(96, 37)
        Me.lblDescripcionCorta.Name = "lblDescripcionCorta"
        Me.lblDescripcionCorta.Size = New System.Drawing.Size(600, 20)
        Me.lblDescripcionCorta.TabIndex = 70
        Me.lblDescripcionCorta.Tag = "descripcion_corta"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(40, 13)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 65
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Art�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumero_Serie
        '
        Me.lblNumero_Serie.AutoSize = True
        Me.lblNumero_Serie.Location = New System.Drawing.Point(32, 61)
        Me.lblNumero_Serie.Name = "lblNumero_Serie"
        Me.lblNumero_Serie.Size = New System.Drawing.Size(60, 16)
        Me.lblNumero_Serie.TabIndex = 67
        Me.lblNumero_Serie.Tag = ""
        Me.lblNumero_Serie.Text = "&No. Serie:"
        Me.lblNumero_Serie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumero_Serie
        '
        Me.txtNumero_Serie.EditValue = ""
        Me.txtNumero_Serie.Location = New System.Drawing.Point(96, 62)
        Me.txtNumero_Serie.Name = "txtNumero_Serie"
        '
        'txtNumero_Serie.Properties
        '
        Me.txtNumero_Serie.Properties.Enabled = False
        Me.txtNumero_Serie.Properties.MaxLength = 30
        Me.txtNumero_Serie.Size = New System.Drawing.Size(152, 20)
        Me.txtNumero_Serie.TabIndex = 68
        Me.txtNumero_Serie.Tag = "numero_serie"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cboEstatus)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.cboTipo_Servicio)
        Me.GroupBox1.Controls.Add(Me.lblTipo_Servicio)
        Me.GroupBox1.Controls.Add(Me.dteFecha)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(712, 48)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(272, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 16)
        Me.Label9.TabIndex = 65
        Me.Label9.Tag = ""
        Me.Label9.Text = "Estatus:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "En Proceso"
        Me.cboEstatus.Location = New System.Drawing.Point(328, 16)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Enabled = False
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Proceso", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Terminada", "T", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(150, 20)
        Me.cboEstatus.TabIndex = 66
        Me.cboEstatus.Tag = "estatus"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.lblArticuloValor)
        Me.GroupBox2.Controls.Add(Me.lblNumero_Serie)
        Me.GroupBox2.Controls.Add(Me.lblDescripcionCorta)
        Me.GroupBox2.Controls.Add(Me.lblArticulo)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtNumero_Serie)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 112)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(712, 88)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(272, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 16)
        Me.Label11.TabIndex = 73
        Me.Label11.Tag = ""
        Me.Label11.Text = "Modelo:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Enabled = False
        Me.Label10.Location = New System.Drawing.Point(328, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(152, 20)
        Me.Label10.TabIndex = 72
        Me.Label10.Tag = "modelo"
        '
        'lblArticuloValor
        '
        Me.lblArticuloValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblArticuloValor.Enabled = False
        Me.lblArticuloValor.Location = New System.Drawing.Point(96, 11)
        Me.lblArticuloValor.Name = "lblArticuloValor"
        Me.lblArticuloValor.Size = New System.Drawing.Size(152, 20)
        Me.lblArticuloValor.TabIndex = 71
        Me.lblArticuloValor.Tag = "articulo_modelo"
        '
        'chkSalidaReparacion
        '
        Me.chkSalidaReparacion.Location = New System.Drawing.Point(32, 210)
        Me.chkSalidaReparacion.Name = "chkSalidaReparacion"
        '
        'chkSalidaReparacion.Properties
        '
        Me.chkSalidaReparacion.Properties.Caption = "Salida"
        Me.chkSalidaReparacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSalidaReparacion.Size = New System.Drawing.Size(56, 19)
        Me.chkSalidaReparacion.TabIndex = 6
        Me.chkSalidaReparacion.Tag = ""
        '
        'chkEntradaReparacion
        '
        Me.chkEntradaReparacion.Location = New System.Drawing.Point(32, 361)
        Me.chkEntradaReparacion.Name = "chkEntradaReparacion"
        '
        'chkEntradaReparacion.Properties
        '
        Me.chkEntradaReparacion.Properties.Caption = "Entrada"
        Me.chkEntradaReparacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkEntradaReparacion.Size = New System.Drawing.Size(64, 19)
        Me.chkEntradaReparacion.TabIndex = 8
        Me.chkEntradaReparacion.Tag = ""
        '
        'txtUsuarioEntradaReparacion
        '
        Me.txtUsuarioEntradaReparacion.EditValue = ""
        Me.txtUsuarioEntradaReparacion.Location = New System.Drawing.Point(104, 24)
        Me.txtUsuarioEntradaReparacion.Name = "txtUsuarioEntradaReparacion"
        '
        'txtUsuarioEntradaReparacion.Properties
        '
        Me.txtUsuarioEntradaReparacion.Properties.Enabled = False
        Me.txtUsuarioEntradaReparacion.Properties.MaxLength = 30
        Me.txtUsuarioEntradaReparacion.Size = New System.Drawing.Size(288, 20)
        Me.txtUsuarioEntradaReparacion.TabIndex = 1
        Me.txtUsuarioEntradaReparacion.Tag = "usuario_entrada_reparacion"
        '
        'txtUsuarioSalidaReparacion
        '
        Me.txtUsuarioSalidaReparacion.EditValue = ""
        Me.txtUsuarioSalidaReparacion.Location = New System.Drawing.Point(104, 24)
        Me.txtUsuarioSalidaReparacion.Name = "txtUsuarioSalidaReparacion"
        '
        'txtUsuarioSalidaReparacion.Properties
        '
        Me.txtUsuarioSalidaReparacion.Properties.Enabled = False
        Me.txtUsuarioSalidaReparacion.Properties.MaxLength = 30
        Me.txtUsuarioSalidaReparacion.Size = New System.Drawing.Size(288, 20)
        Me.txtUsuarioSalidaReparacion.TabIndex = 1
        Me.txtUsuarioSalidaReparacion.Tag = "usuario_salida_reparacion"
        '
        'dteFechaSalidaReparacion
        '
        Me.dteFechaSalidaReparacion.EditValue = "31/05/2006"
        Me.dteFechaSalidaReparacion.Location = New System.Drawing.Point(464, 24)
        Me.dteFechaSalidaReparacion.Name = "dteFechaSalidaReparacion"
        '
        'dteFechaSalidaReparacion.Properties
        '
        Me.dteFechaSalidaReparacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaSalidaReparacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaSalidaReparacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaSalidaReparacion.Properties.Enabled = False
        Me.dteFechaSalidaReparacion.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaSalidaReparacion.TabIndex = 3
        Me.dteFechaSalidaReparacion.Tag = "fecha_salida_reparacion"
        '
        'dteFechaEntradaReparacion
        '
        Me.dteFechaEntradaReparacion.EditValue = "31/05/2006"
        Me.dteFechaEntradaReparacion.Location = New System.Drawing.Point(466, 24)
        Me.dteFechaEntradaReparacion.Name = "dteFechaEntradaReparacion"
        '
        'dteFechaEntradaReparacion.Properties
        '
        Me.dteFechaEntradaReparacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaEntradaReparacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaEntradaReparacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaEntradaReparacion.Properties.Enabled = False
        Me.dteFechaEntradaReparacion.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaEntradaReparacion.TabIndex = 3
        Me.dteFechaEntradaReparacion.Tag = "fecha_entrada_reparacion"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Tag = ""
        Me.Label3.Text = "Observaciones:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservacionesSalida
        '
        Me.txtObservacionesSalida.EditValue = ""
        Me.txtObservacionesSalida.Location = New System.Drawing.Point(104, 72)
        Me.txtObservacionesSalida.Name = "txtObservacionesSalida"
        '
        'txtObservacionesSalida.Properties
        '
        Me.txtObservacionesSalida.Properties.Enabled = False
        Me.txtObservacionesSalida.Properties.MaxLength = 1000
        Me.txtObservacionesSalida.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservacionesSalida.Size = New System.Drawing.Size(592, 56)
        Me.txtObservacionesSalida.TabIndex = 7
        Me.txtObservacionesSalida.Tag = "observaciones_salida_reparacion"
        '
        'txtObservacionesEntrada
        '
        Me.txtObservacionesEntrada.EditValue = ""
        Me.txtObservacionesEntrada.Location = New System.Drawing.Point(104, 72)
        Me.txtObservacionesEntrada.Name = "txtObservacionesEntrada"
        '
        'txtObservacionesEntrada.Properties
        '
        Me.txtObservacionesEntrada.Properties.Enabled = False
        Me.txtObservacionesEntrada.Properties.MaxLength = 1000
        Me.txtObservacionesEntrada.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservacionesEntrada.Size = New System.Drawing.Size(592, 56)
        Me.txtObservacionesEntrada.TabIndex = 7
        Me.txtObservacionesEntrada.Tag = "observaciones_entrada_reparacion"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.lkpBodegueroSalida)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.txtUsuarioSalidaReparacion)
        Me.GroupBox3.Controls.Add(Me.dteFechaSalidaReparacion)
        Me.GroupBox3.Controls.Add(Me.txtObservacionesSalida)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 216)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(712, 136)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(32, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 16)
        Me.Label14.TabIndex = 4
        Me.Label14.Tag = ""
        Me.Label14.Text = "&Bodeguero:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodegueroSalida
        '
        Me.lkpBodegueroSalida.AllowAdd = False
        Me.lkpBodegueroSalida.AutoReaload = False
        Me.lkpBodegueroSalida.DataSource = Nothing
        Me.lkpBodegueroSalida.DefaultSearchField = ""
        Me.lkpBodegueroSalida.DisplayMember = "nombre"
        Me.lkpBodegueroSalida.EditValue = Nothing
        Me.lkpBodegueroSalida.Enabled = False
        Me.lkpBodegueroSalida.Filtered = False
        Me.lkpBodegueroSalida.InitValue = Nothing
        Me.lkpBodegueroSalida.Location = New System.Drawing.Point(104, 48)
        Me.lkpBodegueroSalida.MultiSelect = False
        Me.lkpBodegueroSalida.Name = "lkpBodegueroSalida"
        Me.lkpBodegueroSalida.NullText = ""
        Me.lkpBodegueroSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegueroSalida.ReadOnlyControl = False
        Me.lkpBodegueroSalida.Required = False
        Me.lkpBodegueroSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegueroSalida.SearchMember = ""
        Me.lkpBodegueroSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegueroSalida.SelectAll = False
        Me.lkpBodegueroSalida.Size = New System.Drawing.Size(288, 20)
        Me.lkpBodegueroSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegueroSalida.TabIndex = 5
        Me.lkpBodegueroSalida.Tag = "bodeguero_salida"
        Me.lkpBodegueroSalida.ToolTip = Nothing
        Me.lkpBodegueroSalida.ValueMember = "Bodeguero"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(416, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Tag = ""
        Me.Label5.Text = "Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(48, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Tag = ""
        Me.Label6.Text = "Usuario"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblBodegueroEntrada)
        Me.GroupBox4.Controls.Add(Me.lkpBodegueroEntrada)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.dteFechaEntradaReparacion)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.txtUsuarioEntradaReparacion)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.txtObservacionesEntrada)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 367)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(712, 136)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        '
        'lblBodegueroEntrada
        '
        Me.lblBodegueroEntrada.AutoSize = True
        Me.lblBodegueroEntrada.Location = New System.Drawing.Point(32, 48)
        Me.lblBodegueroEntrada.Name = "lblBodegueroEntrada"
        Me.lblBodegueroEntrada.Size = New System.Drawing.Size(68, 16)
        Me.lblBodegueroEntrada.TabIndex = 4
        Me.lblBodegueroEntrada.Tag = ""
        Me.lblBodegueroEntrada.Text = "&Bodeguero:"
        Me.lblBodegueroEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodegueroEntrada
        '
        Me.lkpBodegueroEntrada.AllowAdd = False
        Me.lkpBodegueroEntrada.AutoReaload = False
        Me.lkpBodegueroEntrada.DataSource = Nothing
        Me.lkpBodegueroEntrada.DefaultSearchField = ""
        Me.lkpBodegueroEntrada.DisplayMember = "nombre"
        Me.lkpBodegueroEntrada.EditValue = Nothing
        Me.lkpBodegueroEntrada.Enabled = False
        Me.lkpBodegueroEntrada.Filtered = False
        Me.lkpBodegueroEntrada.InitValue = Nothing
        Me.lkpBodegueroEntrada.Location = New System.Drawing.Point(104, 48)
        Me.lkpBodegueroEntrada.MultiSelect = False
        Me.lkpBodegueroEntrada.Name = "lkpBodegueroEntrada"
        Me.lkpBodegueroEntrada.NullText = ""
        Me.lkpBodegueroEntrada.PopupWidth = CType(400, Long)
        Me.lkpBodegueroEntrada.ReadOnlyControl = False
        Me.lkpBodegueroEntrada.Required = False
        Me.lkpBodegueroEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegueroEntrada.SearchMember = ""
        Me.lkpBodegueroEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegueroEntrada.SelectAll = False
        Me.lkpBodegueroEntrada.Size = New System.Drawing.Size(288, 20)
        Me.lkpBodegueroEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegueroEntrada.TabIndex = 5
        Me.lkpBodegueroEntrada.Tag = "bodeguero_entrada"
        Me.lkpBodegueroEntrada.ToolTip = Nothing
        Me.lkpBodegueroEntrada.ValueMember = "Bodeguero"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Tag = ""
        Me.Label7.Text = "Observaciones:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(418, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(48, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Tag = ""
        Me.Label8.Text = "Usuario"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(280, 40)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(50, 16)
        Me.Label12.TabIndex = 2
        Me.Label12.Tag = ""
        Me.Label12.Text = "Bodega:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Enabled = False
        Me.Label13.Location = New System.Drawing.Point(336, 40)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 20)
        Me.Label13.TabIndex = 3
        Me.Label13.Tag = "Bodega"
        '
        'frmEntradasSalidasReparacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(730, 512)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.chkSalidaReparacion)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.chkEntradaReparacion)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.lblOrden_Servicio)
        Me.Controls.Add(Me.clcOrden_Servicio)
        Me.Name = "frmEntradasSalidasReparacion"
        Me.Text = "frmEntradasSalidasReparacion"
        Me.Controls.SetChildIndex(Me.clcOrden_Servicio, 0)
        Me.Controls.SetChildIndex(Me.lblOrden_Servicio, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.chkEntradaReparacion, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.chkSalidaReparacion, 0)
        Me.Controls.SetChildIndex(Me.Label13, 0)
        Me.Controls.SetChildIndex(Me.Label12, 0)
        CType(Me.clcOrden_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Servicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumero_Serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.chkSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaSalidaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaEntradaReparacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesSalida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Eventos de la Forma"

    Private Sub frmEntradasSalidasReparacion_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmEntradasSalidasReparacion_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmEntradasSalidasReparacion_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmEntradasSalidasReparacion_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept


        'Si no folio de salida y esta marcado el check de salida
        'Se genera la salida y se almancena el folio
        'If Me.chkSalidaReparacion.Checked And folio_movimiento_salida = 0 Then
        '    Response = Me.oMovimientoInventario.Insertar(Comunes.Common.Sucursal_Actual, Bodega_Orden, Me.ConceptoSalidaReparacion, Me.folio_movimiento_salida, TINApp.FechaServidor(), 0, "", Me.clcOrden_Servicio.Value, Me.txtObservacionesSalida.Text)
        '    If Not Response.ErrorFound Then
        '        Response = Me.oMovimientoInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega_Orden, Me.ConceptoSalidaReparacion, folio_movimiento_salida, TINApp.FechaServidor(), 1, Articulo, 1, Ultimo_Costo, Ultimo_Costo, -1, 0)

        '        If Not Response.ErrorFound Then
        '            Response = Me.oEntradasSalidasReparacion.ActualizaSalidaReparacion(Me.clcOrden_Servicio.EditValue, folio_movimiento_salida, TINApp.FechaServidor(), Me.txtObservacionesSalida.Text)
        '        End If
        '        ' se termina la parte de actualizacion de la salida
        '        Exit Sub
        '    End If
        'End If
        If Me.chkSalidaReparacion.Checked Then
            Response = Me.oEntradasSalidasReparacion.ActualizaSalidaReparacion(Me.clcOrden_Servicio.EditValue, TINApp.FechaServidor(), Me.txtObservacionesSalida.Text, Me.BodegueroSalida)
        End If


        ' si ya tiene una salida y no tiene una entrada y marco el check de entrada
        ' se genera la entrada y se almacena
        'If Me.chkEntradaReparacion.Checked And folio_movimiento_salida > 0 And folio_movimiento_entrada = 0 Then
        '    Response = Me.oMovimientoInventario.Insertar(Comunes.Common.Sucursal_Actual, Me.Bodega_Orden, Me.ConceptoEntradaReparacion, Me.folio_movimiento_entrada, TINApp.FechaServidor, 0, "", Me.clcOrden_Servicio.Value, Me.txtObservacionesEntrada.Text)
        '    If Not Response.ErrorFound Then
        '        Response = Me.oMovimientoInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, Bodega_Orden, Me.ConceptoEntradaReparacion, Me.folio_movimiento_entrada, TINApp.FechaServidor, 1, Me.Articulo, 1, Ultimo_Costo, Ultimo_Costo, -1, 0)
        '        If Not Response.ErrorFound Then
        '            Response = Me.oEntradasSalidasReparacion.ActualizaEntradaReparacion(Me.clcOrden_Servicio.EditValue, Me.folio_movimiento_entrada, TINApp.FechaServidor, Me.txtObservacionesEntrada.Text)
        '        End If
        '        Exit Sub
        '    End If

        'End If

        If Me.chkEntradaReparacion.Checked Then
            Response = Me.oEntradasSalidasReparacion.ActualizaEntradaReparacion(Me.clcOrden_Servicio.EditValue, TINApp.FechaServidor, Me.txtObservacionesEntrada.Text, Me.BodegueroEntrada)
        End If



    End Sub

    Private Sub frmEntradasSalidasReparacion_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

    End Sub

    Private Sub frmEntradasSalidasReparacion_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oEntradasSalidasReparacion.DespliegaDatos(OwnerForm.Value("orden_servicio"))

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            folio_movimiento_salida = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_salida"))
            folio_movimiento_entrada = CInt(oDataSet.Tables(0).Rows(0).Item("folio_movimiento_entrada"))
            Articulo = CInt(oDataSet.Tables(0).Rows(0).Item("articulo"))
            Ultimo_Costo = CDbl(oDataSet.Tables(0).Rows(0).Item("ultimo_costo"))
            Bodega_Orden = CStr(oDataSet.Tables(0).Rows(0).Item("clave_bodega"))
            usuario_salida_reparacion = CStr(oDataSet.Tables(0).Rows(0).Item("usuario_salida_reparacion"))
            usuario_entrada_reparacion = CStr(oDataSet.Tables(0).Rows(0).Item("usuario_entrada_reparacion"))

            If Me.cboEstatus.EditValue = "C" Or Me.cboEstatus.EditValue = "T" Then
                Me.chkSalidaReparacion.Enabled = False
                Me.chkEntradaReparacion.Enabled = False
                Me.tbrTools.Buttons(0).Enabled = False
            Else
                ' ya esta todo generado no realiza nada
                If Len(usuario_salida_reparacion) > 0 And Len(Me.usuario_entrada_reparacion) > 0 Then
                    Me.chkSalidaReparacion.Enabled = False
                    Me.chkEntradaReparacion.Enabled = False
                    Me.tbrTools.Buttons(0).Enabled = False
                Else

                    If Len(usuario_salida_reparacion) > 0 Then
                        Me.chkSalidaReparacion.Enabled = False
                    End If

                    If Len(usuario_entrada_reparacion) > 0 Or Len(usuario_salida_reparacion) = 0 Then
                        Me.chkEntradaReparacion.Enabled = False
                    End If

                End If


            End If
        End If
    End Sub

    Private Sub frmEntradasSalidasReparacion_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Me.chkSalidaReparacion.Checked And Me.chkSalidaReparacion.Enabled Then
            If Me.BodegueroSalida = 0 Then
                Response.Message = "El Bodeguero de la Salida es Requerido"
            End If
        End If

        If Me.chkEntradaReparacion.Checked And Me.chkEntradaReparacion.Enabled Then
            If Me.BodegueroEntrada = 0 Then
                Response.Message = "El Bodeguero de la Entrada es Requerido"
            End If

        End If
    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub chkCambioAutorizado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSalidaReparacion.CheckedChanged
        Me.txtObservacionesSalida.Enabled = Me.chkSalidaReparacion.Checked
        Me.lkpBodegueroSalida.Enabled = Me.chkSalidaReparacion.Checked
    End Sub

    Private Sub chkEntradaReparacion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEntradaReparacion.CheckedChanged
        Me.txtObservacionesEntrada.Enabled = Me.chkEntradaReparacion.Checked
        Me.lkpBodegueroEntrada.Enabled = Me.chkEntradaReparacion.Checked
    End Sub

    Private Sub lkpBodegueroSalida_Format() Handles lkpBodegueroSalida.Format
        Comunes.clsFormato.for_bodegueros_grl(Me.lkpBodegueroSalida)
    End Sub
    Private Sub lkpBodegueroSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegueroSalida.LoadData
        Dim response As Events
        response = oBodegueros.Lookup(Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodegueroSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpBodegueroEntrada_Format() Handles lkpBodegueroEntrada.Format
        Comunes.clsFormato.for_bodegueros_grl(Me.lkpBodegueroEntrada)
    End Sub
    Private Sub lkpBodegueroEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpBodegueroEntrada.LoadData
        Dim response As Events
        response = oBodegueros.Lookup(Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodegueroEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

#End Region

#Region "Funcionalidad"

#End Region






End Class
