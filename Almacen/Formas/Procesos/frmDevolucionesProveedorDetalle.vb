Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmDevolucionesProveedorDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Devolucion As Long
    Private cantidad_entrada As Long = 0
    Private folio_historico_costo_entrada As Long = -1

    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblunidad As System.Windows.Forms.Label
    Friend WithEvents lbldescripcion_corta As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblFolioHistorial As System.Windows.Forms.Label
    Friend WithEvents lkpEntradas As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents chkDevolucionGuardada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcCostoFlete As Dipros.Editors.TINCalcEdit
    Friend WithEvents lkpOrdenServicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDevolucionesProveedorDetalle))
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.lblEntrada = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.chkManejaSeries = New DevExpress.XtraEditors.CheckEdit
        Me.lblunidad = New System.Windows.Forms.Label
        Me.lbldescripcion_corta = New System.Windows.Forms.Label
        Me.lblImporte = New System.Windows.Forms.Label
        Me.lblFolioHistorial = New System.Windows.Forms.Label
        Me.lkpEntradas = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.chkDevolucionGuardada = New DevExpress.XtraEditors.CheckEdit
        Me.clcCostoFlete = New Dipros.Editors.TINCalcEdit
        Me.lkpOrdenServicio = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDevolucionGuardada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCostoFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(254, 28)
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(50, 42)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 0
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "Par&tida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(104, 40)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(58, 19)
        Me.clcPartida.TabIndex = 1
        Me.clcPartida.Tag = "partida"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(47, 139)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 8
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art&�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(40, 186)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 12
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Ca&ntidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(104, 184)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(58, 19)
        Me.clcCantidad.TabIndex = 13
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblCosto
        '
        Me.lblCosto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCosto.Location = New System.Drawing.Point(320, 304)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 6
        Me.lblCosto.Tag = "costo"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCosto.Visible = False
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Location = New System.Drawing.Point(248, 40)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(52, 16)
        Me.lblEntrada.TabIndex = 2
        Me.lblEntrada.Tag = ""
        Me.lblEntrada.Text = "&Entrada:"
        Me.lblEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = ""
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Enabled = False
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 136)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(460, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(108, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 9
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(144, 304)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.MaxLength = 30
        Me.txtSerie.Size = New System.Drawing.Size(148, 20)
        Me.txtSerie.TabIndex = 9
        Me.txtSerie.Tag = "numero_serie"
        Me.txtSerie.Visible = False
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.Location = New System.Drawing.Point(384, 304)
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'chkManejaSeries.Properties
        '
        Me.chkManejaSeries.Properties.Caption = "ManejaSeries"
        Me.chkManejaSeries.Size = New System.Drawing.Size(96, 19)
        Me.chkManejaSeries.TabIndex = 74
        Me.chkManejaSeries.Tag = "maneja_series"
        Me.chkManejaSeries.Visible = False
        '
        'lblunidad
        '
        Me.lblunidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblunidad.Location = New System.Drawing.Point(408, 352)
        Me.lblunidad.Name = "lblunidad"
        Me.lblunidad.Size = New System.Drawing.Size(88, 16)
        Me.lblunidad.TabIndex = 73
        Me.lblunidad.Tag = "nombre_unidad"
        Me.lblunidad.Visible = False
        '
        'lbldescripcion_corta
        '
        Me.lbldescripcion_corta.Location = New System.Drawing.Point(104, 164)
        Me.lbldescripcion_corta.Name = "lbldescripcion_corta"
        Me.lbldescripcion_corta.Size = New System.Drawing.Size(312, 15)
        Me.lbldescripcion_corta.TabIndex = 11
        Me.lbldescripcion_corta.Tag = "descripcion_corta"
        Me.lbldescripcion_corta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImporte
        '
        Me.lblImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblImporte.Location = New System.Drawing.Point(320, 328)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(80, 16)
        Me.lblImporte.TabIndex = 70
        Me.lblImporte.Tag = "importe"
        Me.lblImporte.Visible = False
        '
        'lblFolioHistorial
        '
        Me.lblFolioHistorial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFolioHistorial.Location = New System.Drawing.Point(336, 184)
        Me.lblFolioHistorial.Name = "lblFolioHistorial"
        Me.lblFolioHistorial.Size = New System.Drawing.Size(80, 16)
        Me.lblFolioHistorial.TabIndex = 71
        Me.lblFolioHistorial.Tag = "folio_historico_costo"
        Me.lblFolioHistorial.Text = "-1"
        '
        'lkpEntradas
        '
        Me.lkpEntradas.AllowAdd = False
        Me.lkpEntradas.AutoReaload = False
        Me.lkpEntradas.DataSource = Nothing
        Me.lkpEntradas.DefaultSearchField = ""
        Me.lkpEntradas.DisplayMember = "entrada"
        Me.lkpEntradas.EditValue = Nothing
        Me.lkpEntradas.Enabled = False
        Me.lkpEntradas.Filtered = False
        Me.lkpEntradas.InitValue = Nothing
        Me.lkpEntradas.Location = New System.Drawing.Point(304, 40)
        Me.lkpEntradas.MultiSelect = False
        Me.lkpEntradas.Name = "lkpEntradas"
        Me.lkpEntradas.NullText = ""
        Me.lkpEntradas.PopupWidth = CType(170, Long)
        Me.lkpEntradas.ReadOnlyControl = False
        Me.lkpEntradas.Required = False
        Me.lkpEntradas.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEntradas.SearchMember = ""
        Me.lkpEntradas.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEntradas.SelectAll = False
        Me.lkpEntradas.Size = New System.Drawing.Size(108, 20)
        Me.lkpEntradas.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEntradas.TabIndex = 3
        Me.lkpEntradas.Tag = "entrada"
        Me.lkpEntradas.ToolTip = Nothing
        Me.lkpEntradas.ValueMember = "entrada"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 163)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Enabled = False
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 112)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(312, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 7
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(55, 115)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 6
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Enabled = False
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 88)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(312, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 5
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "Departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(10, 91)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 4
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkDevolucionGuardada
        '
        Me.chkDevolucionGuardada.Location = New System.Drawing.Point(144, 336)
        Me.chkDevolucionGuardada.Name = "chkDevolucionGuardada"
        '
        'chkDevolucionGuardada.Properties
        '
        Me.chkDevolucionGuardada.Properties.Caption = "Devoluci�n guardada"
        Me.chkDevolucionGuardada.Properties.Enabled = False
        Me.chkDevolucionGuardada.Size = New System.Drawing.Size(136, 19)
        Me.chkDevolucionGuardada.TabIndex = 76
        Me.chkDevolucionGuardada.Tag = "devolucion_guardada"
        Me.chkDevolucionGuardada.Visible = False
        '
        'clcCostoFlete
        '
        Me.clcCostoFlete.EditValue = "0"
        Me.clcCostoFlete.Location = New System.Drawing.Point(144, 360)
        Me.clcCostoFlete.MaxValue = 0
        Me.clcCostoFlete.MinValue = 0
        Me.clcCostoFlete.Name = "clcCostoFlete"
        '
        'clcCostoFlete.Properties
        '
        Me.clcCostoFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoFlete.Properties.MaskData.EditMask = "########0.00"
        Me.clcCostoFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCostoFlete.Size = New System.Drawing.Size(58, 19)
        Me.clcCostoFlete.TabIndex = 77
        Me.clcCostoFlete.Tag = "costo_flete"
        Me.clcCostoFlete.Visible = False
        '
        'lkpOrdenServicio
        '
        Me.lkpOrdenServicio.AllowAdd = False
        Me.lkpOrdenServicio.AutoReaload = False
        Me.lkpOrdenServicio.DataSource = Nothing
        Me.lkpOrdenServicio.DefaultSearchField = ""
        Me.lkpOrdenServicio.DisplayMember = "orden_servicio"
        Me.lkpOrdenServicio.EditValue = Nothing
        Me.lkpOrdenServicio.Filtered = False
        Me.lkpOrdenServicio.InitValue = Nothing
        Me.lkpOrdenServicio.Location = New System.Drawing.Point(104, 64)
        Me.lkpOrdenServicio.MultiSelect = False
        Me.lkpOrdenServicio.Name = "lkpOrdenServicio"
        Me.lkpOrdenServicio.NullText = ""
        Me.lkpOrdenServicio.PopupWidth = CType(650, Long)
        Me.lkpOrdenServicio.ReadOnlyControl = False
        Me.lkpOrdenServicio.Required = False
        Me.lkpOrdenServicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpOrdenServicio.SearchMember = ""
        Me.lkpOrdenServicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpOrdenServicio.SelectAll = False
        Me.lkpOrdenServicio.Size = New System.Drawing.Size(108, 20)
        Me.lkpOrdenServicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpOrdenServicio.TabIndex = 78
        Me.lkpOrdenServicio.Tag = "orden_servicio"
        Me.lkpOrdenServicio.ToolTip = Nothing
        Me.lkpOrdenServicio.ValueMember = "orden_servicio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 16)
        Me.Label3.TabIndex = 79
        Me.Label3.Tag = ""
        Me.Label3.Text = "&O. Servicio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmDevolucionesProveedorDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(426, 208)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpOrdenServicio)
        Me.Controls.Add(Me.clcCostoFlete)
        Me.Controls.Add(Me.chkDevolucionGuardada)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpEntradas)
        Me.Controls.Add(Me.chkManejaSeries)
        Me.Controls.Add(Me.lblunidad)
        Me.Controls.Add(Me.lbldescripcion_corta)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblFolioHistorial)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.lblEntrada)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.clcCantidad)
        Me.Name = "frmDevolucionesProveedorDetalle"
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblEntrada, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.lblFolioHistorial, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion_corta, 0)
        Me.Controls.SetChildIndex(Me.lblunidad, 0)
        Me.Controls.SetChildIndex(Me.chkManejaSeries, 0)
        Me.Controls.SetChildIndex(Me.lkpEntradas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.chkDevolucionGuardada, 0)
        Me.Controls.SetChildIndex(Me.clcCostoFlete, 0)
        Me.Controls.SetChildIndex(Me.lkpOrdenServicio, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDevolucionGuardada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCostoFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Public oDevolucionesProveedordetalle As New VillarrealBusiness.clsDevolucionesProveedorDetalle
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oHistorialCostos As New VillarrealBusiness.clsHisCostos
    Private oEntradas As New VillarrealBusiness.clsEntradas
    Private oOrdenesServicio As VillarrealBusiness.clsOrdenesServicio
    Private oOrdenesServicioDetalle As VillarrealBusiness.clsOrdenesServicioDetalle

    Private oEntradasProveedoresDescuentos As VillarrealBusiness.clsEntradasProveedorDescuentos


    ReadOnly Property AfectaCostos() As Boolean
        Get
            Return OwnerForm.AfectaCostos
        End Get
    End Property
    Private ReadOnly Property Entrada() As Long
        Get
            Return PreparaValorLookup(Me.lkpEntradas)
        End Get
    End Property
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property Maneja_series() As Boolean
        Get
            If Articulo = -1 Then
                Return False
            Else
                Return Me.lkpArticulo.GetValue("maneja_series")
            End If
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return OwnerForm.Bodega
        End Get
    End Property
    Private ReadOnly Property OrdenServicio() As Long
        Get
            Return PreparaValorLookup(Me.lkpOrdenServicio)
        End Get
    End Property

    Private serie_seleccionada As String = ""
    Private CostoSinFlete As Double = 0

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDevolucionesProveedorDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    CargarCostosArticulos()
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    CargarCostosArticulos()
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
        OwnerForm.CalculaTotales()
        OwnerForm.ActivaLookup()

    End Sub

    Private Sub frmDevolucionesProveedorDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.txtSerie.Text = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("numero_serie")
        'asignarSerie(Me.txtSerie.Text)
        Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)

        If Me.chkManejaSeries.EditValue = True Then
            Me.lkpEntradas.Enabled = False
            Me.lkpDepartamento.Enabled = False
            Me.lkpGrupo.Enabled = False
            Me.lkpArticulo.Enabled = False
            Me.clcCantidad.Enabled = False
            'Me.grSeries.Enabled = False

        End If

        If Me.Action = Actions.Delete Then
            Me.lkpEntradas.Enabled = False
            Me.lkpDepartamento.Enabled = False
            Me.lkpGrupo.Enabled = False
            Me.lkpArticulo.Enabled = False
            Me.clcCantidad.Enabled = False

        End If
    End Sub

    Private Sub frmDevolucionesProveedorDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGrupos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos
        oEntradasProveedoresDescuentos = New VillarrealBusiness.clsEntradasProveedorDescuentos
        oOrdenesServicio = New VillarrealBusiness.clsOrdenesServicio

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
                Me.clcCantidad.Enabled = False
        End Select

    End Sub

    Private Sub frmDevolucionesProveedorDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = oDevolucionesProveedordetalle.Validacion(Entrada, Departamento, Grupo, Articulo, Me.clcCantidad.EditValue, OrdenServicio, Me.Action)

        If Response.ErrorFound Then Exit Sub

        Dim sBodega As String = CType(OwnerForm, frmDevolucionesProveedor).Bodega
        Dim bInsertarSeries As Boolean = Comunes.clsUtilerias.uti_hisSeries(Articulo, Me.txtSerie.Text, "S", sBodega)

        Response = CType(Me.OwnerForm, frmDevolucionesProveedor).oDevolucionesProveedordetalle.Validacion(Action, _
                                CType(Articulo, String), _
                                Me.clcCantidad.Value, _
                                chkManejaSeries.Checked, _
                                Me.txtSerie.Text, _
                                bInsertarSeries)

        If Response.ErrorFound Then Exit Sub
        If Me.Entrada > 0 Then Response = CType(Me.OwnerForm, frmDevolucionesProveedor).oDevolucionesProveedordetalle.ValidaCantidadDevolucion(Me.clcCantidad.Value, Me.cantidad_entrada)

        If Me.Maneja_series Then
            Response = oDevolucionesProveedordetalle.Validacion(Me.txtSerie.Text)
            If Not Response.ErrorFound Then Response = CType(Me.OwnerForm, frmDevolucionesProveedor).oMovimientosInventarioDetalle.ValidaExisteSerie(serie_seleccionada, "numero_serie", CType(Me.OwnerForm, frmDevolucionesProveedor).tmaDevolucionesProveedor.DataSource)
        End If

        'Dim articulos_de_la_entrada As Long
        'articulos_de_la_entrada = ValidaArticulosEntradaActual(Me.lkpEntradas.EditValue, Me.lkpArticulo.EditValue)

        Dim i As Long
        Dim j As Long
        Dim cantidad_articulo_en_grv As Long
        Dim serie_ocupada As Boolean
        serie_ocupada = False
        j = 0
        cantidad_articulo_en_grv = 0

        With OwnerForm.grvDevolucionesProveedor
            While i <= .RowCount - 1

                If .GetRowCellValue(i, OwnerForm.gdcArticulo) = Articulo Then
                    cantidad_articulo_en_grv = cantidad_articulo_en_grv + 1
                End If

                'If .getrowcellValue(i, OwnerForm.gdcManejaSeries) = True Then
                '    While j <= Me.grvSeries.RowCount - 1
                '        If .GetRowCellValue(i, OwnerForm.gdcArticulo) = Articulo And .GetRowCellValue(i, OwnerForm.gdcSerie) = Me.grvSeries.GetRowCellValue(j, Me.grcSerie) And Me.grvSeries.GetRowCellValue(j, Me.grcSeleccionar) = True Then
                'serie_ocupada = True
                '        End If
                '        j = j + 1
                '    End While
                'End If

                i = i + 1
            End While
        End With



        'Response = oDevolucionesProveedordetalle.ValidaExistenciasArticulos(articulos_de_la_entrada, cantidad_articulo_en_grv, serie_ocupada)
        'If Response.ErrorFound Then Exit Sub


        'If articulos_de_la_entrada <= 0 Then
        '    ShowMessage(MessageType.MsgInformation, "N� hay Existencias del art�culo")
        'End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpEntradas_LoadData(ByVal Initialize As Boolean) Handles lkpEntradas.LoadData
        Dim Response As New Events
        Response = oEntradas.Lookup(OwnerForm.bodega, True, OwnerForm.Proveedor)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEntradas.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpEntradas_Format() Handles lkpEntradas.Format
        Comunes.clsFormato.for_entradas_grl(Me.lkpEntradas)
    End Sub
    Private Sub lkpEntradas_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEntradas.EditValueChanged

        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpArticulo.EditValue = Nothing
        Me.lbldescripcion_corta.Text = " "
        Me.lkpDepartamento_LoadData(True)
        Me.lkpGrupo_LoadData(True)
        Me.lkpArticulo_LoadData(True)
        'If Entrada > 0 Then

        '    Me.lkpArticulo_FilterData(-1)
        '    Me.lkpDepartamento_LoadData(True)
        '    Me.lkpGrupo_FilterData(-1)
        '    Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        '    Me.lkpGrupo.EditValue = Me.lkpGrupo.GetValue("grupo")
        'Else
        '    Me.lkpDepartamento.EditValue = 0
        '    Me.lkpGrupo.EditValue = 0
        '    Me.lkpArticulo.EditValue = 0
        'End If
    End Sub

    'Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
    '    Dim Response As New Events

    '    If Entrada > 0 Then
    '        Response = oEntradas.DespliegaDatosDepartamentoArticulos(Entrada)
    '    Else
    '        Response = oDepartamentos.Lookup
    '    End If
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
    '        Me.lkpDepartamento.EditValue = oDataSet.Tables(0).Rows(0).Item(0)
    '    End If
    'End Sub
    'Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
    '    Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    'End Sub
    'Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
    '    Me.lkpGrupo.EditValue = 0
    '    Me.lkpArticulo.EditValue = 0
    '    Me.lbldescripcion_corta.Text = " "

    '    'If Me.lkpEntradas.EditValue > 0 Then

    '    '    '    Me.lkpGrupo_FilterData(Departamento)
    '    '    '    Me.lkpGrupo_InitData(Departamento)

    '    'Else
    '    '    Me.lkpGrupo.EditValue = 0 'Nothing
    '    '    Me.lkpGrupo_FilterData(-1)
    '    '    Me.lkpGrupo_InitData(-1)
    '    '    Me.lbldescripcion_corta.Text = ""
    '    'End If
    'End Sub

    'Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
    '    Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    'End Sub
    'Private Sub lkpGrupo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged
    '    Me.lkpArticulo.EditValue = -1
    '    Me.lbldescripcion_corta.Text = " "
    '    'If Entrada > 0 Then
    '    'Else
    '    '    Me.lkpArticulo.EditValue = 0 'Nothing
    '    '    Me.lkpArticulo_FilterData(-1)
    '    '    Me.lkpArticulo_InitData(-1)
    '    '    Me.lbldescripcion_corta.Text = ""
    '    'End If
    'End Sub
    '' Private Sub lkpGrupo_FilterData(ByVal Value As Object) Handles lkpGrupo.FilterData
    '    '    Dim Response As New Events
    '    '    If Entrada > 0 Then
    '    '        Response = oEntradas.DespliegaDatosGrupoArticulos(Entrada)
    '    '    Else
    '    '        Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
    '    '    End If
    '    '    If Not Response.ErrorFound Then
    '    '        Dim oDataSet As DataSet
    '    '        oDataSet = Response.Value
    '    '        Me.lkpGrupo.DataSource = oDataSet.Tables(0)
    '    '        Me.lkpGrupo.EditValue = oDataSet.Tables(0).Rows(0).Item(0)
    '    '    End If
    '' End Sub
    ''Private Sub lkpGrupo_InitData(ByVal Value As Object) Handles lkpGrupo.InitData
    ''    Dim Response As New Events

    ''    Response = oGrupos.Lookup(Departamento)
    ''    If Not Response.ErrorFound Then
    ''        Dim oDataSet As DataSet
    ''        oDataSet = Response.Value
    ''        If oDataSet.Tables(0).Rows.Count > 0 Then
    ''            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
    ''        End If

    ''    End If
    ''End Sub
    ''Private Sub lkpArticulo_InitData(ByVal Value As Object) Handles lkpArticulo.InitData
    ''    Dim Response As New Events

    ''    Response = oArticulos.Lookup(Departamento, Grupo, 0, , Value)
    ''    If Not Response.ErrorFound Then
    ''        Dim oDataSet As DataSet
    ''        oDataSet = Response.Value
    ''        If oDataSet.Tables(0).Rows.Count > 0 Then
    ''            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    ''        End If

    ''    End If
    ''End Sub
    ''Private Sub lkpArticulo_FilterData(ByVal Value As Object) Handles lkpArticulo.FilterData
    '    'Dim Response As New Events
    '    'If Entrada > 0 Then
    '    '    Response = oEntradas.DespliegaDatosArticulos(Entrada)
    '    'Else
    '    '    Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue, 0, Value)
    '    'End If
    '    'If Not Response.ErrorFound Then
    '    '    Dim oDataSet As DataSet
    '    '    oDataSet = Response.Value
    '    '    Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    '    '    Me.lkpArticulo.EditValue = oDataSet.Tables(0).Rows(0).Item(0)
    '    'End If
    '    ' End Sub
    'Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
    '    Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    'End Sub
    ''Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
    ''    Dim Response As New Events
    ''    If Entrada > 0 Then
    ''        Response = oEntradas.DespliegaDatosArticulos(Entrada)
    ''    Else
    ''        Response = oArticulos.Lookup
    ''    End If

    ''    If Not Response.ErrorFound Then
    ''        Dim oDataSet As DataSet
    ''        oDataSet = Response.Value
    ''        Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    ''    End If
    ''End Sub

    'Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
    '    lbldescripcion_corta.Text = lkpArticulo.GetValue("descripcion_corta")
    '    lblunidad.Text = lkpArticulo.GetValue("nombre_unidad")
    '    chkManejaSeries.Checked = lkpArticulo.GetValue("maneja_series")

    '    If Entrada > 0 Then
    '        cantidad_entrada = lkpArticulo.GetValue("cantidad")
    '        folio_historico_costo_entrada = lkpArticulo.GetValue("folio_historico_costo")
    '    End If

    '    Select Case AfectaCostos
    '        Case True
    '            clcCantidad.Value = 1
    '            clcCantidad.Enabled = False
    '            If lkpArticulo.GetValue("maneja_series") Then
    '                Me.CargaSeries(Articulo)
    '                Me.grSeries.Visible = True
    '                Me.Size = New Size(432, 336)
    '            Else
    '                Me.grSeries.Visible = False
    '                Me.Size = New Size(432, 240)
    '            End If
    '        Case False
    '            If lkpArticulo.GetValue("maneja_series") Then
    '                clcCantidad.Value = 1
    '                clcCantidad.Enabled = False
    '                Me.CargaSeries(Articulo)
    '                Me.grSeries.Visible = True
    '                Me.Size = New Size(432, 336)
    '            Else
    '                Me.grSeries.Visible = False
    '                Me.Size = New Size(432, 240)
    '                clcCantidad.Enabled = True
    '            End If
    '    End Select


    'End Sub

    Private Sub lkpOrdenServicio_LoadData(ByVal Initialize As Boolean) Handles lkpOrdenServicio.LoadData
        Dim Response As New Events
        Response = Me.oOrdenesServicio.Lookup(True, "", "T", Me.Bodega)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpOrdenServicio.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpOrdenServicio_Format() Handles lkpOrdenServicio.Format
        Comunes.clsFormato.for_ordenes_servicio_grl(Me.lkpOrdenServicio)
    End Sub
    Private Sub lkpOrdenServicio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpOrdenServicio.EditValueChanged
        If Not Me.lkpOrdenServicio.DataSource Is Nothing Then
            Me.lkpDepartamento.EditValue = Me.lkpOrdenServicio.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpOrdenServicio.GetValue("grupo")
            Me.lkpArticulo.EditValue = Me.lkpOrdenServicio.GetValue("articulo")
            Me.txtSerie.Text = Me.lkpOrdenServicio.GetValue("numero_serie")
        End If
    End Sub

#Region "Lookup Articulo"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events

        If Entrada > 0 Then
            Response = oEntradas.DespliegaDatosDepartamentoArticulos(Entrada)
        Else
            Response = oDepartamentos.Lookup
        End If
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            'Me.lkpDepartamento.EditValue = oDataSet.Tables(0).Rows(0).Item(0)
        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpArticulo.EditValue = Nothing
        Me.lbldescripcion_corta.Text = " "
        Me.lkpGrupo_LoadData(True)
        Me.lkpArticulo_LoadData(True)
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged
        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing
        Me.lbldescripcion_corta.Text = " "
        Me.lkpArticulo_LoadData(True)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        If Entrada > 0 Then
            Response = oEntradas.DespliegaDatosGrupoArticulos(Entrada, Departamento)
        Else
            Response = oGrupos.Lookup(Me.lkpDepartamento.EditValue)
        End If
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            'Me.lkpGrupo.EditValue = oDataSet.Tables(0).Rows(0).Item(0)
        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        If Entrada > 0 Then
            Response = oEntradas.DespliegaDatosArticulos(Entrada, Departamento, Grupo, Bodega)
        Else
            Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue, )
        End If
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        lbldescripcion_corta.Text = lkpArticulo.GetValue("descripcion_corta")
        lblunidad.Text = lkpArticulo.GetValue("nombre_unidad")
        chkManejaSeries.Checked = lkpArticulo.GetValue("maneja_series")

        Me.clcCostoFlete.Value = lkpArticulo.GetValue("costo_flete")
        Me.CostoSinFlete = lkpArticulo.GetValue("costo")

        If Entrada > 0 Then
            cantidad_entrada = lkpArticulo.GetValue("cantidad")
            folio_historico_costo_entrada = lkpArticulo.GetValue("folio_historico_costo")
        End If

        Select Case AfectaCostos
            Case True
                clcCantidad.Value = 1
                clcCantidad.Enabled = False
                If lkpArticulo.GetValue("maneja_series") Then
                    'Me.CargaSeries(Articulo)
                    'Me.grSeries.Visible = True
                    'Me.Size = New Size(432, 336)
                    'Else
                    'Me.grSeries.Visible = False
                    'Me.Size = New Size(432, 240)
                End If
            Case False
                If lkpArticulo.GetValue("maneja_series") Then
                    clcCantidad.Value = 1
                    clcCantidad.Enabled = False
                    'Me.CargaSeries(Articulo)
                    'Me.grSeries.Visible = True
                    'Me.Size = New Size(432, 336)
                Else
                    'Me.grSeries.Visible = False
                    'Me.Size = New Size(432, 240)
                    clcCantidad.Enabled = True
                End If
        End Select

    End Sub


#End Region


    'Private Sub grvSeries_CellValueChanging(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs)
    '    Dim j As Integer = Me.grvSeries.FocusedRowHandle
    '    Dim i As Integer = 0
    '    For i = 0 To Me.grvSeries.RowCount - 1
    '        If i <> j Then
    '            Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
    '        Else
    '            serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
    '            Me.txtSerie.Text = serie_seleccionada
    '            Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
    '        End If
    '    Next
    '    grvSeries.UpdateCurrentRow()
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Public Sub CargarCostosArticulos()
        '    Dim Response As Events
        '    Dim oDataSet As DataSet
        'Dim costo_normal As Double


        lblCosto.Text = CostoSinFlete - (CostoSinFlete * DescuentoPorProntoPagoDeEntrada())
        lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value

        '    If AfectaCostos Then
        '        ' Traer costo del historial de costos
        '        Response = oHistorialCostos.UltimoCostoArticulo(Me.lkpArticulo.GetValue("articulo"))
        '        If Not Response.ErrorFound Then
        '            oDataSet = Response.Value
        '            If oDataSet.Tables(0).Rows.Count > 0 Then
        '                costo_normal = oDataSet.Tables(0).Rows(0).Item("costo")

        '                lblCosto.Text = costo_normal - (costo_normal * DescuentoPorProntoPagoDeEntrada())

        '                lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
        '                lblFolioHistorial.Text = oDataSet.Tables(0).Rows(0).Item("folio")
        '            End If
        '        End If
        '    Else
        '        'Traer ultimo costo de Articulos
        '        Response = oArticulos.DespliegaDatos(Me.lkpArticulo.GetValue("articulo"))
        '        If Not Response.ErrorFound Then
        '            oDataSet = Response.Value
        '            If oDataSet.Tables(0).Rows.Count > 0 Then
        '                costo_normal = oDataSet.Tables(0).Rows(0).Item("costo")

        '                lblCosto.Text = costo_normal - (costo_normal * DescuentoPorProntoPagoDeEntrada())
        '                'lblCosto.Text = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
        '                lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
        '                lblFolioHistorial.Text = folio_historico_costo_entrada
        '            End If
        '        End If
        '    End If

        '    oDataSet = Nothing
    End Sub
    'Private Sub CargaSeries(ByVal articulo As Long)
    '    Dim response As Events

    '    'Traer las series de tipo entrada
    '    response = oArticulos.SeriesArticulos(Articulo, "E", CType(OwnerForm, frmDevolucionesProveedor).Bodega, Entrada)
    '    If response.ErrorFound Then
    '        response.ShowMessage()
    '    Else
    '        Dim odataset As DataSet
    '        odataset = response.Value
    '        Me.grSeries.DataSource = odataset.Tables(0)
    '    End If

    'End Sub
    'Private Sub asignarSerie(ByVal serie As String)
    '    Dim i As Long
    '    For i = 0 To Me.grvSeries.RowCount - 1

    '        If Me.grvSeries.GetRowCellValue(i, Me.grcSerie) = serie Then
    '            Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
    '            Exit For
    '        End If
    '        'If  Then
    '        '    Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, False)
    '        'Else
    '        '    serie_seleccionada = Me.grvSeries.GetRowCellValue(i, Me.grcSerie)
    '        '    Me.txtSerie.Text = serie_seleccionada
    '        '    Me.grvSeries.SetRowCellValue(i, Me.grcSeleccionar, True)
    '        'End If
    '    Next


    'End Sub
    'Private Function ValidaArticulosEntradaActual(ByVal entrada, ByVal articulo) As Long
    '    '    ValidaArticulosEntradaActual = oDevolucionesProveedordetalle.ArticuloExistenciasEntradaActual(entrada, articulo)
    '    'End Function
    Private Function DescuentoPorProntoPagoDeEntrada() As Double
        Dim response As Dipros.Utils.Events
        Dim oDataset As DataSet
        Dim Descuento As Double = 0
        Dim i As Integer
        Try
            response = oEntradasProveedoresDescuentos.DespliegaDatos(Me.lkpEntradas.EditValue)
            If response.ErrorFound = False Then
                oDataset = response.Value
            End If
            For i = 0 To oDataset.Tables(0).Rows.Count - 1
                If oDataset.Tables(0).Rows(i).Item("pronto_pago") = True Then
                    Descuento = oDataset.Tables(0).Rows(i).Item("porcentaje")
                    Descuento = Descuento / 100
                    Exit For
                End If
            Next
        Catch ex As Exception

        End Try


        Return Descuento
    End Function
#End Region




   
End Class
