Imports Dipros.Windows.Forms

Module modFormatos

#Region "ConceptosInventario"

    Public Sub for_conceptos_inventario_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo", "tipo", -1, DevExpress.Utils.FormatType.None, "", 80)

        AddColumns(oGrid, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Tipo", "tipo_concepto", 2, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Unidades"

    Public Sub for_unidades_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Unidad", "unidad", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub


#End Region

#Region "Proveedores"

    'Public Sub for_proveedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
    '    AddColumns(oGrid, "Clave", "proveedor", 0, DevExpress.Utils.FormatType.None, "", 100)
    '    AddColumns(oGrid, "Proveedor", "nombre", 1, DevExpress.Utils.FormatType.None, "", 280)
    '    AddColumns(oGrid, "Cuenta", "cuenta", 2, DevExpress.Utils.FormatType.None, "", 100)
    '    AddColumns(oGrid, "RFC", "rfc", 3, DevExpress.Utils.FormatType.None, "", 120)
    '    AddColumns(oGrid, "Domicilio", "domicilio", 4, DevExpress.Utils.FormatType.None, "", 120)
    '    AddColumns(oGrid, "Colonia", "colonia", 5, DevExpress.Utils.FormatType.None, "", 120)
    '    AddColumns(oGrid, "Ciudad", "ciudad", 6, DevExpress.Utils.FormatType.None, "", 120)
    '    AddColumns(oGrid, "Estado", "estado", 7, DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oGrid, "Telefonos", "telefonos", -1, DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oGrid, "Notas", "notas", -1, DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oGrid, "Contactos", "contactos", -1, DevExpress.Utils.FormatType.None, "")
    'End Sub

    'Public Sub for_proveedores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
    '    AddColumns(oLookup, "Proveedor", "proveedor", 1, DevExpress.Utils.FormatType.None, "", 80)
    '    AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 200)
    '    AddColumns(oLookup, "RFC", "rfc", 3, DevExpress.Utils.FormatType.None, "", 90)

    'End Sub

#End Region

#Region "Entradas"

    Public Sub for_entradas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)

        AddColumns(oGrid, "Entrada", "entrada", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Orden de Compra", "orden", 1, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Proveedor", "proveedor", 3, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Referencia", "referencia", 4, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Bodega", "bodega", 6, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Entrada Costeada", "costeado", 5, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Usuario", "nombre_usuario", 7, DevExpress.Utils.FormatType.None, "", 120)

    End Sub

    Public Sub for_entradas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Entrada", "entrada", 0, DevExpress.Utils.FormatType.None, "", 75)
        AddColumns(oLookup, "Orden", "orden", 1, DevExpress.Utils.FormatType.None, "", 75)
        AddColumns(oLookup, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oLookup, "Proveedor", "nombre_proveedor", 3, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Referencia", "referencia", 4, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Bodega", "nombre_bodega", 5, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#Region "EntradasProveedorDescuentos"

    Public Sub for_entradas_proveedor_descuentos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Entrada", "entrada", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descuento", "descuento", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Porcentaje", "porcentaje", 3, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#End Region

#Region "OrdenesCompra"
    Public Sub for_ordenes_compra_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)

        AddColumns(oGrid, "Orden", "orden", 0, DevExpress.Utils.FormatType.None, "", 50)
        AddColumns(oGrid, "Sucursal", "sucursal_nombre", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Fecha Promesa", "fecha_promesa", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 95)
        AddColumns(oGrid, "Bodega", "bodega_descripcion", 4, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Proveedor", "proveedor_nombre", 5, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Folio De Proveedor", "folio_proveedor", 6, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Condiciones", "condiciones", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Agente", "agente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Observaciones", "observaciones", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "status_vista", 7, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Usuario", "nombre_usuario", 8, DevExpress.Utils.FormatType.None, "", 120)
    End Sub

    Public Sub for_ordenes_compra_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Orden", "orden", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oLookup, "Folio de Proveedor", "folio_proveedor", 6, DevExpress.Utils.FormatType.None, "", 120)
    End Sub

#End Region

#Region "OrdenesCompraDetalle"
    Public Sub for_ordenes_compra_detalle_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Orden", "orden", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Partida", "partida", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Art�culo", "articulo", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cantidad", "cantidad", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Precio", "precio", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Importe", "importe", 5, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "Sucursales"
    Public Sub for_sucursales_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "sucursal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Direcci�n", "direccion", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Colonia", "colonia", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cp", "cp", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Ciudad", "ciudad", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Estado", "estado", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Tel�fono", "telefono", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Gerente", "gerente", 8, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region

#Region "DevolucionesProveedor"

    Public Sub for_devoluciones_proveedor_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "status", "status", -1, DevExpress.Utils.FormatType.None, "", 80)

        AddColumns(oGrid, "Devoluci�n", "devolucion", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Fecha", "fecha", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Proveedor", "n_proveedor", 2, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Conducto", "conducto", 3, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Usuario", "nombre_usuario", 4, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Observaciones", "observaciones", 5, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Nota Aplicada", "nota_aplicada", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Aplicado Inv.", "aplicado_inventario", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Enviado al Proveedor", "enviado_proveedor", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "tipo_status", -1, DevExpress.Utils.FormatType.None, "", 80)
    End Sub

#End Region

#Region "Traspasos"

    Public Sub for_traspasos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Traspaso", "traspaso", 0, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oGrid, "Fecha", "fecha", 1, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Sucursal Origen", "sucursal_salida_vista", -1, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oGrid, "Bodega Origen", "bodega_salida_vista", 2, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Concepto", "concepto_salida", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio_salida", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Usuario", "nombre_usuario", 5, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Observaciones De Origen", "observaciones_salida", 6, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Status", "status_vista", 7, DevExpress.Utils.FormatType.None, "")


    End Sub

#End Region

#Region "Ubicaciones"

    Public Sub for_ubicaciones_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Bodega", "bodega", 0, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Ubicaci�n", "ubicacion", 1, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 410)

    End Sub

#End Region

#Region "InventarioFisico"

    Public Sub for_inventario_fisico_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Inventario", "inventario", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Bodega", "bodega", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega", "n_bodega", 1, DevExpress.Utils.FormatType.None, "", 160)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "status", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "n_status", 3, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Observaciones", "observaciones", 4, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Tipo Inventario", "tipo_inventario", -1, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region

#Region "proveeedor_ordenes_compra"
    Public Sub for_folio_proveeedor_ordenes_compra_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Folio de Proveedor", "folio_proveedor", 0, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oLookup, "Orden", "orden", 1, DevExpress.Utils.FormatType.None, "", 70)

    End Sub
#End Region

#Region "SolicitudTraspasos"

    Public Sub for_solicitud_traspasos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Solicitud", "solicitud", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal Destino", "nombre_sucursal_entrada", -1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Bodega Destino", "sucursal_bodega_entrada", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Bodega Origen", "sucursal_bodega_salida", 3, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Solicita", "nombre_solicita", 4, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Captur�", "capturo", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha De Solicitud", "fecha_solicitud", 6, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 150)
        AddColumns(oGrid, "Estatus", "estatus_vista", 7, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Folio De Traspaso", "folio_traspaso", 8, DevExpress.Utils.FormatType.None, "", 150)
    End Sub
#End Region

#Region "Precios"

    Public Sub for_precios_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Precio", "precio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "Tipos de Llamadas"

    Public Sub for_tipos_llamadas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo de llamada", "tipo_llamada", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "DescuentosProveedor"

    Public Sub for_descuentos_proveedor_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Descuento", "descuento_proveedor", 0, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
    End Sub

#End Region



End Module
