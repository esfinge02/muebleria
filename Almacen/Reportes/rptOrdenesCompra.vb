Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptOrdenesCompra
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOrden As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblOrden As DataDynamics.ActiveReports.Label = Nothing
	Private txtAgente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblAgente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaPromesa As DataDynamics.ActiveReports.Label = Nothing
	Private txtFechaPromesa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblCondiciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCondiciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtDireccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private txtCiudad As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTelefonos As DataDynamics.ActiveReports.Label = Nothing
	Private txtTelefonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblRfc As DataDynamics.ActiveReports.Label = Nothing
	Private txtRfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private Line11 As DataDynamics.ActiveReports.Line = Nothing
	Private Line12 As DataDynamics.ActiveReports.Line = Nothing
	Private Line13 As DataDynamics.ActiveReports.Line = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private domicilio_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private ciudad_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Public logo_status As DataDynamics.ActiveReports.Picture = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private Line7 As DataDynamics.ActiveReports.Line = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private txtSubtotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape3 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtIva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUsuario As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSubtotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblIva As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblAtentamente As DataDynamics.ActiveReports.Label = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptOrdenesCompra.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape4 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape1 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.Shape7 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Shape)
		Me.lblFecha = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtOrden = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblOrden = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.txtAgente = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblAgente = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFechaPromesa = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtFechaPromesa = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtFolioProveedor = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Shape5 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Shape)
		Me.lblProveedor = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblCondiciones = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtCondiciones = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.txtBodega = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.txtDireccion = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.lblCiudad = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.Label)
		Me.txtCiudad = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.lblTelefonos = CType(Me.PageHeader.Controls(29),DataDynamics.ActiveReports.Label)
		Me.txtTelefonos = CType(Me.PageHeader.Controls(30),DataDynamics.ActiveReports.TextBox)
		Me.lblRfc = CType(Me.PageHeader.Controls(31),DataDynamics.ActiveReports.Label)
		Me.txtRfc = CType(Me.PageHeader.Controls(32),DataDynamics.ActiveReports.TextBox)
		Me.Line10 = CType(Me.PageHeader.Controls(33),DataDynamics.ActiveReports.Line)
		Me.Line11 = CType(Me.PageHeader.Controls(34),DataDynamics.ActiveReports.Line)
		Me.Line12 = CType(Me.PageHeader.Controls(35),DataDynamics.ActiveReports.Line)
		Me.Line13 = CType(Me.PageHeader.Controls(36),DataDynamics.ActiveReports.Line)
		Me.picLogotipo = CType(Me.PageHeader.Controls(37),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(38),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(39),DataDynamics.ActiveReports.Label)
		Me.domicilio_proveedor1 = CType(Me.PageHeader.Controls(40),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.PageHeader.Controls(41),DataDynamics.ActiveReports.Label)
		Me.ciudad_proveedor1 = CType(Me.PageHeader.Controls(42),DataDynamics.ActiveReports.TextBox)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(43),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(44),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(45),DataDynamics.ActiveReports.TextBox)
		Me.logo_status = CType(Me.PageHeader.Controls(46),DataDynamics.ActiveReports.Picture)
		Me.Label26 = CType(Me.PageHeader.Controls(47),DataDynamics.ActiveReports.Label)
		Me.articulo1 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.importe = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line3 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Line4 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.Line5 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Line)
		Me.Line6 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Line)
		Me.Line7 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Line)
		Me.Line14 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Line)
		Me.Line15 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Line)
		Me.txtSubtotal = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Shape3 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.txtIva = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtUsuario = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblSubtotal = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblIva = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblAtentamente = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Line9 = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.Line)
		Me.txtObservaciones = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.GroupFooter1.Controls(11),DataDynamics.ActiveReports.Label)
		Me.cantidad1 = CType(Me.GroupFooter1.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Shape2 = CType(Me.GroupFooter1.Controls(13),DataDynamics.ActiveReports.Shape)
		Me.Label27 = CType(Me.GroupFooter1.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptOrdenesCompra_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
