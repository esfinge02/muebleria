Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSeriesArticulos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private txtClaveArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptSeriesArticulos.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupo = CType(Me.Sections("gphGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphProveedor = CType(Me.Sections("gphProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghArticulo = CType(Me.Sections("ghArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfArticulo = CType(Me.Sections("gfArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfProveedor = CType(Me.Sections("gpfProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfGrupo = CType(Me.Sections("gpfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblConcepto = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreDepartamento = CType(Me.gphDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.gphGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreGrupo = CType(Me.gphGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtProveedor = CType(Me.gphProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.gphProveedor.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.ghArticulo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblFecha = CType(Me.ghArticulo.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.ghArticulo.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label10 = CType(Me.ghArticulo.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.ghArticulo.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.ghArticulo.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtModelo = CType(Me.ghArticulo.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label18 = CType(Me.ghArticulo.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.ghArticulo.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtClaveArticulo = CType(Me.ghArticulo.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSerie = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region


    Private Sub rptSeriesArticulos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
