Imports System
Imports Dipros.Utils.Common
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document



Public Class rptExistenciasMultiple

    Inherits ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeReport()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphBodega As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupoArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents pghGrupoProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfGrupoProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfGrupoArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfBodega As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_1 As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_2 As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_3 As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_4 As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_5 As DataDynamics.ActiveReports.Label = Nothing
	Private lbln_col_7 As DataDynamics.ActiveReports.Label = Nothing
	Private lblDepartamento As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_departamento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_grupo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtv_col_8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lbln_col_8 As DataDynamics.ActiveReports.Label = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptExistenciasMultiple.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphSucursal = CType(Me.Sections("gphSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphBodega = CType(Me.Sections("gphBodega"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupoArticulo = CType(Me.Sections("gphGrupoArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.pghGrupoProveedor = CType(Me.Sections("pghGrupoProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfGrupoProveedor = CType(Me.Sections("gpfGrupoProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfGrupoArticulo = CType(Me.Sections("gpfGrupoArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfBodega = CType(Me.Sections("gpfBodega"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfSucursal = CType(Me.Sections("gpfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lbln_col_1 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lbln_col_2 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lbln_col_3 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lbln_col_4 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lbln_col_5 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lbln_col_7 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblDepartamento = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.nombre_departamento1 = CType(Me.gphSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.nombre_grupo1 = CType(Me.gphBodega.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDepartamento = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.gphGrupoArticulo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.proveedor1 = CType(Me.gphGrupoArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtProveedor = CType(Me.pghGrupoProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_1 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_2 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_3 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_4 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_5 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_7 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtv_col_8 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lbln_col_8 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtModelo = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

#End Region

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

    End Sub

    Private Sub PageHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Format

    End Sub

    Private Sub rptExistenciasMultiple_ReportStart(ByVal Response As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        Response = oReportes.DatosEmpresa()

        If Response.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.txtEmpresa.Value = Response.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = Response.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = Response.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(Response.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
