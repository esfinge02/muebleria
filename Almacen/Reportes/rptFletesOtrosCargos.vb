Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptFletesOtrosCargos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GH_Todas As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GH_Sucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GF_Sucusal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GF_Todas As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblTituloIncluido As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblIVARetenido As DataDynamics.ActiveReports.Label = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEntrada As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaEntrada As DataDynamics.ActiveReports.Label = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporteFlete As DataDynamics.ActiveReports.Label = Nothing
	Private lblOtrosGastos As DataDynamics.ActiveReports.Label = Nothing
	Private lblIVA As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtIVARetenido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtProveedorNombre As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteFlete As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOtrosGastos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIVA As DataDynamics.ActiveReports.TextBox = Nothing
	Private acreedor_nombre As DataDynamics.ActiveReports.TextBox = Nothing
	Private folio_factura As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSucursalTotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteFleteSubTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOtrosGastosSubTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIVASubTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIVARetenidoSubTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblTodasTotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtIVARetenidoTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIVATotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteFleteTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOtrosGastosTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptFletesOtrosCargos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GH_Todas = CType(Me.Sections("GH_Todas"),DataDynamics.ActiveReports.GroupHeader)
		Me.GH_Sucursal = CType(Me.Sections("GH_Sucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GF_Sucusal = CType(Me.Sections("GF_Sucusal"),DataDynamics.ActiveReports.GroupFooter)
		Me.GF_Todas = CType(Me.Sections("GF_Todas"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblTituloIncluido = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.GH_Sucursal.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.lblIVARetenido = CType(Me.GH_Sucursal.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblSucursal = CType(Me.GH_Sucursal.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtSucursal = CType(Me.GH_Sucursal.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblEntrada = CType(Me.GH_Sucursal.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechaEntrada = CType(Me.GH_Sucursal.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblProveedor = CType(Me.GH_Sucursal.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblImporteFlete = CType(Me.GH_Sucursal.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblOtrosGastos = CType(Me.GH_Sucursal.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblIVA = CType(Me.GH_Sucursal.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.GH_Sucursal.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.GH_Sucursal.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtIVARetenido = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtEntrada = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtProveedorNombre = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaEntrada = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteFlete = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtOtrosGastos = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtIVA = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.acreedor_nombre = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.folio_factura = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lblSucursalTotal = CType(Me.GF_Sucusal.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteFleteSubTotal = CType(Me.GF_Sucusal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtOtrosGastosSubTotal = CType(Me.GF_Sucusal.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtIVASubTotal = CType(Me.GF_Sucusal.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtIVARetenidoSubTotal = CType(Me.GF_Sucusal.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.GF_Sucusal.Controls(5),DataDynamics.ActiveReports.Line)
		Me.lblTodasTotal = CType(Me.GF_Todas.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtIVARetenidoTotal = CType(Me.GF_Todas.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtIVATotal = CType(Me.GF_Todas.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteFleteTotal = CType(Me.GF_Todas.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtOtrosGastosTotal = CType(Me.GF_Todas.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.GF_Todas.Controls(5),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptFletesOtrosCargos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Drawing.Image
        sender = oReportes.DatosEmpresa()
        If CType(CType(sender, Dipros.Utils.Events).Value, DataSet).Tables(0).Rows.Count > 0 Then
            Dim oData As DataRow = CType(CType(sender, Dipros.Utils.Events).Value, DataSet).Tables(0).Rows(0)
            With oData
                Me.txtEmpresa.Value = .Item("nombre_empresa")
                Me.txtDireccionEmpresa.Value = .Item("direccion_empresa")
                Me.txtTelefonosEmpresa.Value = .Item("telefonos_empresa")
                dibujo = Dipros.Utils.Draw.ByteToImage(.Item("logotipo_empresa"))
                Me.picLogotipo.Image = dibujo
            End With
            dibujo = Nothing
            oData = Nothing
        End If
    End Sub

    Private Sub PageHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Format

    End Sub
End Class
