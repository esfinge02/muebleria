Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptExistencias
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupoArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfGrupoArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblUnidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblPorSurtir As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega3 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega4 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega5 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega6 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega7 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblDepartamento As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private nom_bodega_8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ultimo_costo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private por_surtir As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private fisica_8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private t_fisica1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ultimo_costo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptExistencias.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupoArticulo = CType(Me.Sections("gphGrupoArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfGrupoArticulo = CType(Me.Sections("gpfGrupoArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblUnidad = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblPorSurtir = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblBodega1 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblBodega2 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblBodega3 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblBodega4 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblBodega5 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblBodega6 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblBodega7 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblDepartamento = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblModelo = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.nom_bodega_8 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.txtDepartamento = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.gphGrupoArticulo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.proveedor1 = CType(Me.gphGrupoArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.ultimo_costo2 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.por_surtir = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fisica_1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fisica_2 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.fisica_3 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.fisica_4 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.fisica_5 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.fisica_6 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.fisica_7 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.fisica_8 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.t_fisica1 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.ultimo_costo1 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptExistencias_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
