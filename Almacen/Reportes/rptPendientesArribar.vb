Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptPendientesArribar
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GrHProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GrHOrden As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GrFOrden As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GrFProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblOrden As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaPromesa As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblSurtida As DataDynamics.ActiveReports.Label = Nothing
	Private lblPendiente As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaPromesa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtStatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOrden As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line13 As DataDynamics.ActiveReports.Line = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPedida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPendiente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSurtida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteOrden As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporte_Proveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptPendientesArribar.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GrHProveedor = CType(Me.Sections("GrHProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.GrHOrden = CType(Me.Sections("GrHOrden"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GrFOrden = CType(Me.Sections("GrFOrden"),DataDynamics.ActiveReports.GroupFooter)
		Me.GrFProveedor = CType(Me.Sections("GrFProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape8 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblOrden = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFechaPromesa = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblSurtida = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblPendiente = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblModelo = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Line14 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Line)
		Me.Label26 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.GrHProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaPromesa = CType(Me.GrHOrden.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtStatus = CType(Me.GrHOrden.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.GrHOrden.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtOrden = CType(Me.GrHOrden.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioProveedor = CType(Me.GrHOrden.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line13 = CType(Me.GrHOrden.Controls(5),DataDynamics.ActiveReports.Line)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPedida = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtPendiente = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSurtida = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteOrden = CType(Me.GrFOrden.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.GrFProveedor.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporte_Proveedor = CType(Me.GrFProveedor.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label5 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptPendientesArribar_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
