Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptTraspasosEntradasSalidasDesglosado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphTraspasos As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTraspasos As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private txtcostosalida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodegaOrigen As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioOrigen As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaOrigen As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcostoentrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodegaDestino As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioDestino As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaDestino As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private costo_salida1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private costo_entrada1 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptTraspasosEntradasSalidasDesglosado.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphTraspasos = CType(Me.Sections("gphTraspasos"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTraspasos = CType(Me.Sections("gpfTraspasos"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label8 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Line)
		Me.Label29 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Line)
		Me.Label34 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.txtcostosalida = CType(Me.gphTraspasos.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtBodegaOrigen = CType(Me.gphTraspasos.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioOrigen = CType(Me.gphTraspasos.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaOrigen = CType(Me.gphTraspasos.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtcostoentrada = CType(Me.gphTraspasos.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtBodegaDestino = CType(Me.gphTraspasos.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioDestino = CType(Me.gphTraspasos.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaDestino = CType(Me.gphTraspasos.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtGrupo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSerie = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.costo_salida1 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label35 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo_entrada1 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptTraspasosEntradasSalidasDesglosado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub gphTraspasos_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles gphTraspasos.Format
        Me.costo_salida1.Value = Me.costo_salida1.Value + Me.txtcostosalida.Value
        Me.costo_entrada1.Value = Me.costo_entrada1.Value + Me.txtcostoentrada.Value
    End Sub
End Class
