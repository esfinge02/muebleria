Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSalidaVistas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private txtTraspaso As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTraspaso As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblUsuario As DataDynamics.ActiveReports.Label = Nothing
	Private txtUsuario As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private sucursal_bodega_salida1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private txtmodelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtbodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private Line16 As DataDynamics.ActiveReports.Line = Nothing
	Private Line17 As DataDynamics.ActiveReports.Line = Nothing
	Private Line18 As DataDynamics.ActiveReports.Line = Nothing
	Private Line21 As DataDynamics.ActiveReports.Line = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private Line22 As DataDynamics.ActiveReports.Line = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNombreUsuario As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtChofer As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line20 As DataDynamics.ActiveReports.Line = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_bodeguero1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptSalidaVistas.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.lblFecha = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTraspaso = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblTraspaso = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblArticulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblSerie = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblObservaciones = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtObservaciones = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lblUsuario = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtUsuario = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.Label28 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.sucursal_bodega_salida1 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.Label30 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.txtmodelo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtbodega = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSerie = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line14 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line15 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Line16 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.Line17 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Line)
		Me.Line18 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Line)
		Me.Line21 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Line)
		Me.Line5 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Line)
		Me.Line22 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Line9 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Line)
		Me.txtNombreUsuario = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Line10 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.Line)
		Me.Label3 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtChofer = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line20 = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Label27 = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.Label)
		Me.nombre_bodeguero1 = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Line)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptSalidaVistas_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim response As New Dipros.Utils.Events
        Dim dibujo As Image
        Dim oDataSet As DataSet
        response = oReportes.DatosEmpresa()
        oDataSet = response.Value

        If oDataSet.Tables(0).Rows.Count > 0 Then

            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
