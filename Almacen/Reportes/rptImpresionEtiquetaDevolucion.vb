Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptImpresionEtiquetaDevolucion
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVendido As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_devolucion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptImpresionEtiquetaDevolucion.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblFactura = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtFactura = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtVendido = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtDepartamento = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.fecha1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label6 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.proveedor1 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label8 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.articulo1 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Label)
		Me.fecha_devolucion1 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
