Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptEntradaAlmacenProceso
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEntrada As DataDynamics.ActiveReports.Label = Nothing
	Private txtAgente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblAgente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechamportacion As DataDynamics.ActiveReports.Label = Nothing
	Private txtFechaimportaci�n As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblCondiciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCondiciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtDireccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private txtCiudad As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTelefonos As DataDynamics.ActiveReports.Label = Nothing
	Private txtTelefonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblRfc As DataDynamics.ActiveReports.Label = Nothing
	Private txtRfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private Line11 As DataDynamics.ActiveReports.Line = Nothing
	Private Line12 As DataDynamics.ActiveReports.Line = Nothing
	Private Line13 As DataDynamics.ActiveReports.Line = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private domicilio_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private estado_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private txtAduana As DataDynamics.ActiveReports.TextBox = Nothing
	Private folio_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private cuenta_contable As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private lblImpSinFlete As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioOC As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteConFlete As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_sin_flete As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_con_flete1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line35 As DataDynamics.ActiveReports.Line = Nothing
	Private importe_con_flete2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line7 As DataDynamics.ActiveReports.Line = Nothing
	Private txtDescripcion_Corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line18 As DataDynamics.ActiveReports.Line = Nothing
	Private Line20 As DataDynamics.ActiveReports.Line = Nothing
	Private Line27 As DataDynamics.ActiveReports.Line = Nothing
	Private Line32 As DataDynamics.ActiveReports.Line = Nothing
	Private Line34 As DataDynamics.ActiveReports.Line = Nothing
	Private Line36 As DataDynamics.ActiveReports.Line = Nothing
	Private Line37 As DataDynamics.ActiveReports.Line = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtSubtotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblSubtotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblIva As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private condiciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private subtotal_sin_flete As DataDynamics.ActiveReports.TextBox = Nothing
	Private iva_sin_flete As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_sin_flete As DataDynamics.ActiveReports.TextBox = Nothing
	Public lblFirmaRecibido As DataDynamics.ActiveReports.Label = Nothing
	Public lblFirmaComprador As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public LineaComprador As DataDynamics.ActiveReports.Line = Nothing
	Public LineaRecibido As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptEntradaAlmacenProceso.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape7 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.lblCosto = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Shape4 = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Shape)
		Me.Shape1 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.lblFecha = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtEntrada = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblEntrada = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtAgente = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblAgente = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblFechamportacion = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtFechaimportaci�n = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtFolioProveedor = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Shape5 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Shape)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.lblProveedor = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblCondiciones = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtCondiciones = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.txtBodega = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.txtDireccion = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.lblCiudad = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.Label)
		Me.txtCiudad = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.lblTelefonos = CType(Me.PageHeader.Controls(29),DataDynamics.ActiveReports.Label)
		Me.txtTelefonos = CType(Me.PageHeader.Controls(30),DataDynamics.ActiveReports.TextBox)
		Me.lblRfc = CType(Me.PageHeader.Controls(31),DataDynamics.ActiveReports.Label)
		Me.txtRfc = CType(Me.PageHeader.Controls(32),DataDynamics.ActiveReports.TextBox)
		Me.Line10 = CType(Me.PageHeader.Controls(33),DataDynamics.ActiveReports.Line)
		Me.Line11 = CType(Me.PageHeader.Controls(34),DataDynamics.ActiveReports.Line)
		Me.Line12 = CType(Me.PageHeader.Controls(35),DataDynamics.ActiveReports.Line)
		Me.Line13 = CType(Me.PageHeader.Controls(36),DataDynamics.ActiveReports.Line)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(37),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(38),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(39),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(40),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(41),DataDynamics.ActiveReports.Label)
		Me.domicilio_proveedor1 = CType(Me.PageHeader.Controls(42),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.PageHeader.Controls(43),DataDynamics.ActiveReports.Label)
		Me.estado_proveedor1 = CType(Me.PageHeader.Controls(44),DataDynamics.ActiveReports.TextBox)
		Me.Line14 = CType(Me.PageHeader.Controls(45),DataDynamics.ActiveReports.Line)
		Me.Label16 = CType(Me.PageHeader.Controls(46),DataDynamics.ActiveReports.Label)
		Me.txtAduana = CType(Me.PageHeader.Controls(47),DataDynamics.ActiveReports.TextBox)
		Me.folio_proveedor1 = CType(Me.PageHeader.Controls(48),DataDynamics.ActiveReports.TextBox)
		Me.lblModelo = CType(Me.PageHeader.Controls(49),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.PageHeader.Controls(50),DataDynamics.ActiveReports.Label)
		Me.cuenta_contable = CType(Me.PageHeader.Controls(51),DataDynamics.ActiveReports.TextBox)
		Me.Label22 = CType(Me.PageHeader.Controls(52),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(53),DataDynamics.ActiveReports.Label)
		Me.lblImpSinFlete = CType(Me.PageHeader.Controls(54),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageHeader.Controls(55),DataDynamics.ActiveReports.Label)
		Me.txtFolioOC = CType(Me.PageHeader.Controls(56),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteConFlete = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe_sin_flete = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe_con_flete1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line35 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Line)
		Me.importe_con_flete2 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Line7 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.txtDescripcion_Corta = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Line18 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Line)
		Me.Line20 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Line)
		Me.Line27 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Line)
		Me.Line32 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Line34 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Line)
		Me.Line36 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Line)
		Me.Line37 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Line)
		Me.Shape8 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.txtSubtotal = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtIva = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtObservaciones = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSubtotal = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblIva = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Line15 = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.Line)
		Me.cantidad1 = CType(Me.GroupFooter1.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.Label21 = CType(Me.GroupFooter1.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.GroupFooter1.Controls(13),DataDynamics.ActiveReports.Label)
		Me.condiciones = CType(Me.GroupFooter1.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.subtotal_sin_flete = CType(Me.GroupFooter1.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.iva_sin_flete = CType(Me.GroupFooter1.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.total_sin_flete = CType(Me.GroupFooter1.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.lblFirmaRecibido = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblFirmaComprador = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(7),DataDynamics.ActiveReports.Line)
		Me.LineaComprador = CType(Me.PageFooter.Controls(8),DataDynamics.ActiveReports.Line)
		Me.LineaRecibido = CType(Me.PageFooter.Controls(9),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptEntradaAlmacenProceso_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

    End Sub
End Class
