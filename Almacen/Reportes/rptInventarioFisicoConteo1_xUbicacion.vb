Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptInventarioFisicoConteo1_xUbicacion
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphHoja As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfpGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfHoja As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lbl3 As DataDynamics.ActiveReports.Label = Nothing
	Private lbl4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private txtContador As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private txtUbicacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private txtSupervisor As DataDynamics.ActiveReports.TextBox = Nothing
	Private lbl5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConteo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalArticulos As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalArticulosDepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGranTotalArticulos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptInventarioFisicoConteo1_xUbicacion.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphHoja = CType(Me.Sections("gphHoja"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupo = CType(Me.Sections("gphGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfpGrupo = CType(Me.Sections("gfpGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfHoja = CType(Me.Sections("gpfHoja"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.txtBodega = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Shape8 = CType(Me.gphHoja.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.lbl3 = CType(Me.gphHoja.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lbl4 = CType(Me.gphHoja.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.gphHoja.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtContador = CType(Me.gphHoja.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.gphHoja.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtUbicacion = CType(Me.gphHoja.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label17 = CType(Me.gphHoja.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtSupervisor = CType(Me.gphHoja.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lbl5 = CType(Me.gphHoja.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.gphHoja.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.gphHoja.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtNDepartamento = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNGrupo = CType(Me.gphGrupo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtConteo = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.gfpGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalArticulos = CType(Me.gfpGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label20 = CType(Me.gpfDepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalArticulosDepto = CType(Me.gpfDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.gpfHoja.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtGranTotalArticulos = CType(Me.gpfHoja.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptInventarioFisicoConteo1_xUbicacion_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
