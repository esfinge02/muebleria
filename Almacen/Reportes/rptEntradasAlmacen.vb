Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptEntradasAlmacen
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphBodega As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphEntrada As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfEntrada As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfBodega As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblRecibida As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private lblEntrada As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblOrden As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOrden As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPedida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSurtida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPartida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteOrden As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptEntradasAlmacen.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphBodega = CType(Me.Sections("gphBodega"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupo = CType(Me.Sections("gphGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphProveedor = CType(Me.Sections("gphProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphEntrada = CType(Me.Sections("gphEntrada"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfEntrada = CType(Me.Sections("gpfEntrada"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfProveedor = CType(Me.Sections("gpfProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfGrupo = CType(Me.Sections("gpfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfBodega = CType(Me.Sections("gpfBodega"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblRecibida = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblEntrada = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblOrden = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblModelo = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Shape)
		Me.txtBodega = CType(Me.gphBodega.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblBodega = CType(Me.gphBodega.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblConcepto = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreDepartamento = CType(Me.gphDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.gphGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreGrupo = CType(Me.gphGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.gphGrupo.Controls(2),DataDynamics.ActiveReports.Line)
		Me.txtProveedor = CType(Me.gphProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtEntrada = CType(Me.gphEntrada.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtOrden = CType(Me.gphEntrada.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioProveedor = CType(Me.gphEntrada.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.gphEntrada.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPedida = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtSurtida = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtPartida = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteOrden = CType(Me.gpfEntrada.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.gpfEntrada.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.gpfProveedor.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteProveedor = CType(Me.gpfProveedor.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gpfGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteGrupo = CType(Me.gpfGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.gpfDepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteDepartamento = CType(Me.gpfDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.gpfBodega.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteBodega = CType(Me.gpfBodega.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label6 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptEntradasAlmacen_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim response As Dipros.Utils.Events

        Dim oDataSet As DataSet
        Dim dibujo As Image
        response = oReportes.DatosEmpresa()

        oDataSet = CType(response.Value, DataSet)

        If oDataSet.Tables(0).Rows.Count > 0 Then


            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
