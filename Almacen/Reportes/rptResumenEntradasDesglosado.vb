Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenEntradasDesglosado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphConcepto As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphFolio As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private lblGrupo As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTituloConceptos As DataDynamics.ActiveReports.Label = Nothing
	Private txtOrdenado As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtConcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSubTotalConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteConcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTotalGrupo As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptResumenEntradasDesglosado.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphConcepto = CType(Me.Sections("gphConcepto"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphFolio = CType(Me.Sections("gphFolio"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label8 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblGrupo = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Label30 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label34 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label35 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblTituloConceptos = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.txtOrdenado = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblConcepto = CType(Me.gphConcepto.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtConcepto = CType(Me.gphConcepto.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.gphFolio.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.gphFolio.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.gphFolio.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtBodega = CType(Me.gphFolio.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSucursal = CType(Me.gphFolio.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtProveedor = CType(Me.gphFolio.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.gphFolio.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtGrupo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.costo1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSerie = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblSubTotalConcepto = CType(Me.GroupFooter2.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteConcepto = CType(Me.GroupFooter2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblTotalGrupo = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteProveedor = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label29 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptResumenEntradasDesglosado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
