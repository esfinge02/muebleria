Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptTraspasosEntradas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphTraspaso As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTraspaso As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblSubtitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodegaSalida As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodegaEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private txtTraspaso As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTraspaso As DataDynamics.ActiveReports.Label = Nothing
	Private lblUsuario As DataDynamics.ActiveReports.Label = Nothing
	Private txtUsuario As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private Line16 As DataDynamics.ActiveReports.Line = Nothing
	Private Line17 As DataDynamics.ActiveReports.Line = Nothing
	Private Line18 As DataDynamics.ActiveReports.Line = Nothing
	Private Line20 As DataDynamics.ActiveReports.Line = Nothing
	Private articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private txtChofer As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreUsuario As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line19 As DataDynamics.ActiveReports.Line = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptTraspasosEntradas.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphTraspaso = CType(Me.Sections("gphTraspaso"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTraspaso = CType(Me.Sections("gpfTraspaso"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.gphTraspaso.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.gphTraspaso.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.gphTraspaso.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.gphTraspaso.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.gphTraspaso.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblSubtitulo = CType(Me.gphTraspaso.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtBodegaSalida = CType(Me.gphTraspaso.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.gphTraspaso.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtBodegaEntrada = CType(Me.gphTraspaso.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lblFecha = CType(Me.gphTraspaso.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtTraspaso = CType(Me.gphTraspaso.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblTraspaso = CType(Me.gphTraspaso.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblUsuario = CType(Me.gphTraspaso.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtUsuario = CType(Me.gphTraspaso.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.gphTraspaso.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.gphTraspaso.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtObservaciones = CType(Me.gphTraspaso.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.gphTraspaso.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.gphTraspaso.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblSerie = CType(Me.gphTraspaso.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.gphTraspaso.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.gphTraspaso.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtSerie = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line5 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Line)
		Me.Line14 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Line15 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line16 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Line17 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.Line18 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Line)
		Me.Line20 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Line)
		Me.articulo1 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.Line9 = CType(Me.gpfTraspaso.Controls(0),DataDynamics.ActiveReports.Line)
		Me.Line10 = CType(Me.gpfTraspaso.Controls(1),DataDynamics.ActiveReports.Line)
		Me.Label26 = CType(Me.gpfTraspaso.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtChofer = CType(Me.gpfTraspaso.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.gpfTraspaso.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtNombreUsuario = CType(Me.gpfTraspaso.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line19 = CType(Me.gpfTraspaso.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Label27 = CType(Me.gpfTraspaso.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Line)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region


    Private Sub rptTraspasosEntradas_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
