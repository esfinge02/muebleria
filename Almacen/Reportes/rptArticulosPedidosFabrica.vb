Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptArticulosPedidosFabrica
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghdepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblSubtitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioUnitario As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolioReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private lblFechaFactura As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtnombre_concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_grupo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_proveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConcepto_referencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioUnitario As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private orden_compra As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptArticulosPedidosFabrica.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghdepartamento = CType(Me.Sections("ghdepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghGrupo = CType(Me.Sections("ghGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghProveedor = CType(Me.Sections("ghProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfProveedor = CType(Me.Sections("gfProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfGrupo = CType(Me.Sections("gfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfDepartamento = CType(Me.Sections("gfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Label10 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSubtitulo = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblPrecioUnitario = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFolioReferencia = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.lblFechaFactura = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Line)
		Me.lblConcepto = CType(Me.ghdepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtnombre_concepto = CType(Me.ghdepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.nombre_grupo1 = CType(Me.ghGrupo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.nombre_proveedor = CType(Me.ghProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtConcepto_referencia = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.nombre_concepto1 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaFactura = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioUnitario = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.orden_compra = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptArticulosPedidosFabrica_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
