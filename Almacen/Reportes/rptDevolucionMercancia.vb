Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptDevolucionMercancia
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape6 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private txtnombre_concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolioReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtbodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtDireccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private txtCiudad As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private conducto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private conducto2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private proveedor2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private devolucion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private domicilio_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ciudad_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private txtcuenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConcepto_referencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUnitario As DataDynamics.ActiveReports.TextBox = Nothing
	Private iva As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_iva_incluido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private costototal1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private txtiva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txttotal_iva_incluido As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptDevolucionMercancia.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape7 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape6 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Picture)
		Me.lblArticulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblConcepto = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtnombre_concepto = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblFolioReferencia = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtbodega = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Shape5 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Shape)
		Me.lblProveedor = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtDireccion = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.lblCiudad = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtCiudad = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.proveedor1 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.conducto1 = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.conducto2 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.proveedor2 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.Label)
		Me.devolucion1 = CType(Me.PageHeader.Controls(29),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.PageHeader.Controls(30),DataDynamics.ActiveReports.Label)
		Me.domicilio_proveedor1 = CType(Me.PageHeader.Controls(31),DataDynamics.ActiveReports.TextBox)
		Me.ciudad_proveedor1 = CType(Me.PageHeader.Controls(32),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.PageHeader.Controls(33),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(34),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(35),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(36),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.PageHeader.Controls(37),DataDynamics.ActiveReports.Label)
		Me.txtcuenta = CType(Me.PageHeader.Controls(38),DataDynamics.ActiveReports.TextBox)
		Me.nombre_concepto1 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtConcepto_referencia = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtUnitario = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.iva = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.total_iva_incluido = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label15 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.costototal1 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.cantidad1 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label6 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.ReportFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.ReportFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.ReportFooter.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.ReportFooter.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtiva = CType(Me.ReportFooter.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txttotal_iva_incluido = CType(Me.ReportFooter.Controls(9),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptDevolucionMercancia_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
