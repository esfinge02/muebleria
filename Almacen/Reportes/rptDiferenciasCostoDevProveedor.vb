Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptDiferenciasCostoDevProveedor
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghAgrupado As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfAgrupado As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Public lblTipoOrdenContrario As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtnombre_corte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcosto_devolucion As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_capa As DataDynamics.ActiveReports.TextBox = Nothing
	Private diferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descripcion_corta1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descripcion_corta2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descripcion_corta3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_devolucion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_capa1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private diferencia1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private costo_devolucion2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_capa2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private diferencia2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptDiferenciasCostoDevProveedor.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghAgrupado = CType(Me.Sections("ghAgrupado"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfAgrupado = CType(Me.Sections("gfAgrupado"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape7 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblTipoOrdenContrario = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtnombre_corte = CType(Me.ghAgrupado.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtcosto_devolucion = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.costo_capa = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.diferencia = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.nombre_concepto1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.descripcion_corta1 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.descripcion_corta2 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.descripcion_corta3 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.costo_devolucion1 = CType(Me.gfAgrupado.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.costo_capa1 = CType(Me.gfAgrupado.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.diferencia1 = CType(Me.gfAgrupado.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gfAgrupado.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label15 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label25 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.costo_devolucion2 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.costo_capa2 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.diferencia2 = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptDiferenciasCostoDevProveedor_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
