Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptInventarioFisicoDiferencias
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lbl3 As DataDynamics.ActiveReports.Label = Nothing
	Private lbl4 As DataDynamics.ActiveReports.Label = Nothing
	Private lbl5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private txtInventario As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUnidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConteo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtExistencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalArticulos As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptInventarioFisicoDiferencias.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.txtBodega = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Shape8 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Shape)
		Me.lbl3 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lbl4 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lbl5 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtInventario = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.Label20 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDepartamento = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtGrupo = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtUnidad = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtConteo = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtExistencia = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtDiferencia = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label12 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalArticulos = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptInventarioFisicoDiferencias_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
