Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSalidasPorTienda
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptSalidasPorTienda.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphSucursal = CType(Me.Sections("gphSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfSucursal = CType(Me.Sections("gpfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblReferencia = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Line)
		Me.lblSucursal = CType(Me.gphSucursal.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtSucursal = CType(Me.gphSucursal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.articulo1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtReferencia = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioVenta = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label6 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptSalidasPorTienda_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
