Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptCosteoInventarioDesglosadoGrupo
    Inherits ActiveReport

    Public oData As DataSet
    Public blnConcentrado As Boolean
    'Private dTotalCostoArticuloBodega As Double = 0
    'Private dTotalCostoDepto As Double = 0
    Private dTotalCostoGrupo As Double = 0
    Private dTotalCostoTotal As Double = 0

	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lbltipo_reporte As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcion As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidadHistorico As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private n_articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private tipo_articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioHistorico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaHistorico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidadHistorico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoHistorico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCostoHistorico As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCostoGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCostoTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptCosteoInventarioDesglosadoGrupo.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghGrupo = CType(Me.Sections("ghGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghArticulo = CType(Me.Sections("ghArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfArticulo = CType(Me.Sections("gfArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfGrupo = CType(Me.Sections("gfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.ReportHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.ReportHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.ReportHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.ReportHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lbltipo_reporte = CType(Me.ReportHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.ReportHeader.Controls(5),DataDynamics.ActiveReports.Picture)
		Me.Shape2 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape1 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.lblArticulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblDescripcion = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblCantidadHistorico = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Line)
		Me.lblModelo = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Line)
		Me.Label20 = CType(Me.ghGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreGrupo = CType(Me.ghGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.ghArticulo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.n_articulo1 = CType(Me.ghArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.cantidad1 = CType(Me.ghArticulo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.tipo_articulo1 = CType(Me.ghArticulo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.nombre_proveedor1 = CType(Me.ghArticulo.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.costo1 = CType(Me.ghArticulo.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSerie = CType(Me.ghArticulo.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.ghArticulo.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioHistorico = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaHistorico = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidadHistorico = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoHistorico = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCostoHistorico = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.gfGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtCostoGrupo = CType(Me.gfGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line4 = CType(Me.gfGrupo.Controls(2),DataDynamics.ActiveReports.Line)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label15 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label28 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtCostoTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line5 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Line)
	End Sub

#End Region


    Private Sub rptCosteoInventarioDesglosadoGrupo_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If

    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        Me.fecha_actual1.Value = Now.Date
    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format
        Me.txtCostoTotal.Value = dTotalCostoTotal
    End Sub
    Private Sub ghArticulo_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghArticulo.Format
        'dTotalCostoArticuloBodega = dTotalCostoArticuloBodega + Me.costo1.Value
        dTotalCostoGrupo = dTotalCostoGrupo + Me.costo1.Value
    End Sub

    Private Sub gfGrupo_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles gfGrupo.Format
        Me.txtCostoGrupo.Value = dTotalCostogrupo
        dTotalCostoTotal = dTotalCostoTotal + dTotalCostoGrupo

    End Sub

    Private Sub ghGrupo_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghGrupo.Format
        dTotalCostoGrupo = 0
    End Sub


End Class
