Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptPolizaFlete
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_sucursal As DataDynamics.ActiveReports.Label = Nothing
	Private lbCuentaContable As DataDynamics.ActiveReports.Label = Nothing
	Private lblHaber As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcion As DataDynamics.ActiveReports.Label = Nothing
	Private lblDebe As DataDynamics.ActiveReports.Label = Nothing
	Private NombreFletero As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_fletero1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtdebe As DataDynamics.ActiveReports.TextBox = Nothing
	Private txthaber As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCuentaContable As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalDebe As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private txtTotalHaber As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtFechaActual As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptPolizaFlete.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape8 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.nombre_sucursal = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lbCuentaContable = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblHaber = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblDescripcion = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblDebe = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.NombreFletero = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.nombre_fletero1 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtdebe = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txthaber = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCuentaContable = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalDebe = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtTotalHaber = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Line4 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.Line)
		Me.Label28 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label19 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.TextBox2 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtFechaActual = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

   
    Private Sub rptPolizaFlete_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
