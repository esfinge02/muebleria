Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenMovimientos
    Inherits ActiveReport

    Private ultimo As Long
    Private tot_ultimo_bodega As Long
    Private tot_ultimo_grantot As Long
    Public oData As DataSet

	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghBodega As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfBodega As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private txtDescripcionArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInicialArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUnidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private txtMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInicial As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSalida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFinal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotArtSal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotArtEnt As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotArtFin As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private subConceptos As DataDynamics.ActiveReports.SubReport = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Almacen.rptResumenMovimientos.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghBodega = CType(Me.Sections("ghBodega"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghArticulo = CType(Me.Sections("ghArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfArticulo = CType(Me.Sections("gfArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfBodega = CType(Me.Sections("gfBodega"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Line)
		Me.Label29 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblModelo = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.ghBodega.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtNombreBodega = CType(Me.ghBodega.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.ghArticulo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.ghArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreGrupo = CType(Me.ghArticulo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label18 = CType(Me.ghArticulo.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.ghArticulo.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtDescripcionArticulo = CType(Me.ghArticulo.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtInicialArticulo = CType(Me.ghArticulo.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtUnidad = CType(Me.ghArticulo.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.ghArticulo.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Line4 = CType(Me.ghArticulo.Controls(9),DataDynamics.ActiveReports.Line)
		Me.txtMovimiento = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtInicial = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtReferencia = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtEntrada = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSalida = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFinal = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gfArticulo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotArtSal = CType(Me.gfArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotArtEnt = CType(Me.gfArticulo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotArtFin = CType(Me.gfArticulo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line5 = CType(Me.gfArticulo.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.subConceptos = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.SubReport)
	End Sub

	#End Region

    Private Sub Detail_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.AfterPrint
        ultimo = CType(Me.txtFinal.Value, Long)
    End Sub

    Private Sub gfArticulo_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles gfArticulo.BeforePrint
        txtTotArtFin.Value = ultimo
        tot_ultimo_bodega = tot_ultimo_bodega + ultimo
    End Sub

    Private Sub gfBodega_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles gfBodega.BeforePrint
        tot_ultimo_grantot = tot_ultimo_grantot + tot_ultimo_bodega
        tot_ultimo_bodega = 0
    End Sub

    Private Sub rptResumenMovimientos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If

        tot_ultimo_bodega = 0
        tot_ultimo_grantot = 0

    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format
        Me.subConceptos.Report = New subResumenMovimientos_TotalesXConcepto
        CType(subConceptos.Report, subResumenMovimientos_TotalesXConcepto).DataSource = oData.Tables(1)
    End Sub
End Class
