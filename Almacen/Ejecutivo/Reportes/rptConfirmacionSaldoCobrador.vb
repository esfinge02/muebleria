Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptConfirmacionSaldoCobrador
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox10 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptConfirmacionSaldoCobrador.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label10 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.Line)
		Me.Line2 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.Line)
		Me.TextBox1 = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.Detail.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.Detail.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.TextBox5 = CType(Me.Detail.Controls(25),DataDynamics.ActiveReports.TextBox)
		Me.TextBox6 = CType(Me.Detail.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.TextBox7 = CType(Me.Detail.Controls(27),DataDynamics.ActiveReports.TextBox)
		Me.TextBox8 = CType(Me.Detail.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.TextBox9 = CType(Me.Detail.Controls(29),DataDynamics.ActiveReports.TextBox)
		Me.TextBox10 = CType(Me.Detail.Controls(30),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

   
End Class
