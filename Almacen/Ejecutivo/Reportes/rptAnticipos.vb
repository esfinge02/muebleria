Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAnticipos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghFolio As DataDynamics.ActiveReports.GroupHeader = Nothing
    Public WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Public lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolioReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioLista As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblDiferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private txtfolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtserie_factura_aplicado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfolio_factura_aplicado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha_factura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtimporte_aplicado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptAnticipos.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghFolio = CType(Me.Sections("ghFolio"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Label1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFolioReferencia = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblPrecioLista = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblDiferencia = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtfolio = CType(Me.ghFolio.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.ghFolio.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.ghFolio.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.fecha = CType(Me.ghFolio.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente = CType(Me.ghFolio.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.cliente = CType(Me.ghFolio.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.saldo = CType(Me.ghFolio.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtSucursal = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtserie_factura_aplicado = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtfolio_factura_aplicado = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha_factura = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtimporte_aplicado = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtObservaciones = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.importe1 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.saldo1 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptAnticipos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
