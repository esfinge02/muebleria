Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptUtilidadVentasGrupo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioLista As DataDynamics.ActiveReports.Label = Nothing
	Private costo2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Public lblFiltros As DataDynamics.ActiveReports.Label = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtprecioventa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUtilidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPorcentaje As DataDynamics.ActiveReports.TextBox = Nothing
	Private precio_venta1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private utilidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private porcentaje1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private precio_venta3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcostoDepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private utilidad3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtporcentajeDepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptUtilidadVentasGrupo.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghDepartamento = CType(Me.Sections("ghDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghGrupo = CType(Me.Sections("ghGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfGrupo = CType(Me.Sections("gfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfDepartamento = CType(Me.Sections("gfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblPrecioLista = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.costo2 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblFiltros = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtDepartamento = CType(Me.ghDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_concepto = CType(Me.ghGrupo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtprecioventa = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtcosto = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtUtilidad = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion_corta = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtPorcentaje = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.precio_venta1 = CType(Me.gfGrupo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.costo3 = CType(Me.gfGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.utilidad1 = CType(Me.gfGrupo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.porcentaje1 = CType(Me.gfGrupo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.gfGrupo.Controls(4),DataDynamics.ActiveReports.Label)
		Me.precio_venta3 = CType(Me.gfDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtcostoDepto = CType(Me.gfDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.utilidad3 = CType(Me.gfDepartamento.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtporcentajeDepto = CType(Me.gfDepartamento.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label18 = CType(Me.gfDepartamento.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptUtilidadVentasGrupo_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

 
    

  
End Class
