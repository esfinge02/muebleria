Imports Dipros.Windows.Forms

Module modFormatos

#Region "OrdenesCompra"
    Public Sub for_ordenes_compra_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Orden", "orden", 0, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "", 120)
        'AddColumns(oLookup, "Fecha_Promesa", "fecha_promesa", 3, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Bodega", "bodega", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Proveedor", "proveedor", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio de Proveedor", "folio_proveedor", 6, DevExpress.Utils.FormatType.None, "", 120)
        'AddColumns(oLookup, "Condiciones", "condiciones", 7, DevExpress.Utils.FormatType.None, "")
        ' AddColumns(oLookup, "Agente", "agente", 8, DevExpress.Utils.FormatType.None, "")
        ' AddColumns(oLookup, "Observaciones", "observaciones", 9, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Status", "status", 10, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Anticipo", "anticipo", 11, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region

#Region "Ventas"

    Public Sub for_ventas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "n_sucursal", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Serie", "serie", 1, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oGrid, "Folio", "folio", 2, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Vendedor", "vendedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Vendedor", "n_vendedor", 4, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oGrid, "Cliente", "cliente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cliente", "n_cliente", 5, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oGrid, "Domicilio", "domicilio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "colonia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ciudad", "ciudad", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono 1", "telefono1", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono 2", "telefono2", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fax", "fax", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Curp", "curp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "RFC", "rfc", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo de Venta", "tipoventa", 6, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Ivadesglosado", "ivadesglosado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Importe", "importe", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Menos", "menos", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Enganche", "enganche", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Interes", "interes", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Subtotal", "subtotal", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Impuesto", "impuesto", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Total", "total", 7, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00", 90)
        AddColumns(oGrid, "Cancelada", "cancelada", 8, DevExpress.Utils.FormatType.None, "", 90)
        'AddColumns(oGrid, "Plan", "plan", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Plan", "n_plan", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha_Primer_Documento", "fecha_primer_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Numero_Documentos", "numero_documentos", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Importe_Documentos", "importe_documentos", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Importe_Ultimo_Documento", "importe_ultimo_documento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Enganche en Documento", "enganche_en_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha_Enganche_Documento", "fecha_enganche_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Importe_Enganche_Documento", "importe_enganche_documento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Liquida_Vencimiento", "liquida_vencimiento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Monto_Liquida_Vencimiento", "monto_liquida_vencimiento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Pedimento", "pedimento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Aduana", "aduana", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha de Importaci�n", "fecha_importacion", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Reparto", "reparto", -1, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

    '#Region "Vendedores"

    '    Public Sub for_vendedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
    '        AddColumns(oGrid, "Vendedor", "vendedor", 0, DevExpress.Utils.FormatType.None, "")
    '        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
    '        AddColumns(oGrid, "Sueldo", "sueldo", 3, DevExpress.Utils.FormatType.Numeric, "c2", 100)
    '        AddColumns(oGrid, "Meta", "meta", 4, DevExpress.Utils.FormatType.Numeric, "c2", 100)
    '        AddColumns(oGrid, "Saldo", "saldo", 5, DevExpress.Utils.FormatType.Numeric, "c2", 100)
    '        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 2, DevExpress.Utils.FormatType.None, "c2", 130)

    '    End Sub

    '#End Region


#Region "Opciones de Cancelacion"

    Public Sub for_opciones_cancelacion_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Opci�n", "opcion", 0, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 400)
        AddColumns(oGrid, "Activo", "activo", 2, DevExpress.Utils.FormatType.None, "", 60)
    End Sub

#End Region


#Region "Tipos de Vales"

    Public Sub for_tipos_vales_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo de Vale", "tipo", 0, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oGrid, "Descripci�n", "nombre", 1, DevExpress.Utils.FormatType.None, "", 400)
        AddColumns(oGrid, "Concepto", "concepto", 2, DevExpress.Utils.FormatType.None, "", 60)
        AddColumns(oGrid, "Forma", "forma", 2, DevExpress.Utils.FormatType.None, "", 60)
    End Sub

#End Region


#Region "PlanesCredito"

    Public Sub for_planes_credito_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Plan De Cr�dito", "plan_credito", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Plazo", "plazo", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Inter�s", "interes", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Precio", "precio_venta", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo De Venta", "tipo_venta_vista", 4, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Tipo De Plazo", "tipo_plazo_vista", 5, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Plan Dependencia", "plan_dependencia", 6, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Enganche", "enganche", 7, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Vigente", "vigente", 8, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "FONACOT", "fonacot", 8, DevExpress.Utils.FormatType.None, "", 90)

    End Sub

#End Region

#Region "Descuentos"

    Public Sub for_descuentos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Descuento", "descuento", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Porcentaje", "porcentaje", 2, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "AutorizacionesVentasPedidoFabrica"

    Public Sub for_autorizaciones_ventas_pedido_fabrica_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio Autorizaci�n", "folio_autorizacion", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha_autorizacion", 1, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyy")
        AddColumns(oGrid, "Cliente", "cliente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cliente", "nombre", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Vendedor", "vendedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Vendedor", "nombre_vendedor", 3, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Articulo", "articulo", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Art�culo", "descripcion_corta", 4, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Costo", "costo", 5, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Utilizada", "utilizada", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal Factura", "sucursal_factura", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Serie Factura", "serie_factura", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Factura", "folio_factura", -1, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Cotizaciones"

    Public Sub for_cotizaciones_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Cotizaci�n", "cotizacion", 0, DevExpress.Utils.FormatType.Numeric, "n0", 80)
        AddColumns(oGrid, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Nombre Cliente", "nombre", 4, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Utilizada", "cotizacion_utilizada", 5, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Direcci�n", "direccion", -1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub
#End Region

#Region "Anticipos"

    Public Sub for_anticipos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Anticipo", "folio", 0, DevExpress.Utils.FormatType.Numeric, "n0", 80)
        AddColumns(oGrid, "Tipo Venta", "tipo", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Nombre Cliente", "nombre_cliente", 4, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Concepto", "concepto", 5, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Importe", "importe", 6, DevExpress.Utils.FormatType.Numeric, "c2", 100)

    End Sub
#End Region



#Region "AutorizacionesBonificacionesInteres"

    Public Sub for_autorizaciones_bonificaciones_interes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.Numeric, "n0")
        AddColumns(oGrid, "Cliente", "cliente", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Caja", "caja", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Importe", "importe", 4, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Utilizada", "utilizada", 5, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region


#Region "DescuentosEspecialesClientes"
    Public Sub for_descuentos_especiales_clientes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio_descuento", 0, DevExpress.Utils.FormatType.Numeric, "n0", 80)
        AddColumns(oGrid, "Cliente", "nombre_cliente", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Monto", "monto", 2, DevExpress.Utils.FormatType.None, "c2")
        AddColumns(oGrid, "Descuento", "porcentaje_descuento", 3, DevExpress.Utils.FormatType.None, "n2")
        AddColumns(oGrid, "Plan Cr�dito", "plan_credito", 4, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Utilizada", "utilizada", 5, DevExpress.Utils.FormatType.DateTime, "")

        AddColumns(oGrid, "Usuario Autoriza", "usuario_autoriza", 6, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Usuario Captura", "usuario_captura", 7, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha Captura", "fecha_captura", 8, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 100)
        AddColumns(oGrid, "Serie Factura", "serie_factura", 9, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Folio Factura", "folio_factura", 10, DevExpress.Utils.FormatType.None, "n0", 100)


    End Sub

#End Region

End Module
