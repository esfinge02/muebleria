Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports DevExpress.XtraExport
Imports DevExpress.XtraGrid.Export

Public Class frmReporteEstadistico
    Inherits Dipros.Windows.frmTINForm

    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oReportes As New VillarrealBusiness.Reportes
    Private oProveedores As New VillarrealBusiness.clsProveedores

    Enum TipoColumna
        Compras = 1
        Ventas = 2
        Entradas = 3
        Salidas = 4
    End Enum

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents btnExportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcProveedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcInicial As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcComprasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcComprasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcVentasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand3 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcEntradasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcEntradasDevCTE As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcEntradasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand4 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand5 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcEntradasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcSalidasDevPro As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcSalidasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcSalidasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcSalidasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcExistencias As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcValor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand6 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents BrvArticulos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents btnVerCompras As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVerVentas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVerEntradas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVerSalidas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grGrupos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grClasificacion As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridBand7 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoInicial As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoComprasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoVentasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoEntradasDevCte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoEntradasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoEntradasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoEntradasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoSalidasDevPro As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoSalidasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoSalidasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoSalidasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoExistencias As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcGrupoValor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand8 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand9 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand10 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand11 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand12 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcClasificacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionInicial As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionComprasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasEne As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasFeb As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasMar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasAbr As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasMay As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasJun As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasJul As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasAgo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasSep As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasOct As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasNov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasDic As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionVentasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand14 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand15 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand16 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand17 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridBand18 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcClasificacionEntradasDevCte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionEntradasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionEntradasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionEntradasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionSalidasDevPro As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionSalidasMovEsp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionSalidasAjustes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionSalidasTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionExistencias As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcClasificacionValor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand19 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents progressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents BrvGrupos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents BrvClasificacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents GridBand13 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents grcdias_inventario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents grcpor_surtir As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents chkSoloExistencias As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReporteEstadistico))
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.BrvArticulos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcArticulo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcProveedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcInicial = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcComprasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcComprasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand4 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcEntradasDevCTE = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcEntradasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcEntradasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcEntradasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand3 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcVentasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcVentasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand5 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcSalidasDevPro = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcSalidasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcSalidasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcSalidasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand6 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcExistencias = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcValor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand13 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcdias_inventario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcpor_surtir = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.grGrupos = New DevExpress.XtraGrid.GridControl
        Me.BrvGrupos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridBand7 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoInicial = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand11 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupoComprasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoComprasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand9 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupoEntradasDevCte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoEntradasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoEntradasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoEntradasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand8 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupoVentasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoVentasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand10 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupoSalidasDevPro = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoSalidasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoSalidasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoSalidasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand12 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcGrupoExistencias = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcGrupoValor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.grClasificacion = New DevExpress.XtraGrid.GridControl
        Me.BrvClasificacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridBand19 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionInicial = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand14 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacionComprasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionComprasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand15 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacionEntradasDevCte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionEntradasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionEntradasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionEntradasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand16 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacionVentasEne = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasFeb = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasMar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasAbr = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasMay = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasJul = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasJun = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasAgo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasSep = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasOct = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasNov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasDic = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionVentasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand17 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacionSalidasDevPro = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionSalidasMovEsp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionSalidasAjustes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionSalidasTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand18 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.grcClasificacionExistencias = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.grcClasificacionValor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.btnExportar = New DevExpress.XtraEditors.SimpleButton
        Me.btnVerCompras = New DevExpress.XtraEditors.SimpleButton
        Me.btnVerVentas = New DevExpress.XtraEditors.SimpleButton
        Me.btnVerEntradas = New DevExpress.XtraEditors.SimpleButton
        Me.btnVerSalidas = New DevExpress.XtraEditors.SimpleButton
        Me.progressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl
        Me.chkSoloExistencias = New DevExpress.XtraEditors.CheckEdit
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BrvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grGrupos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BrvGrupos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.grClasificacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BrvClasificacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.progressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSoloExistencias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4867, 28)
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(104, 40)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(420, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(232, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 1
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(32, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Pr&oveedor:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "D&epartamento: "
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(232, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(232, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(56, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "&Grupo:"
        '
        'grArticulos
        '
        Me.grArticulos.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Location = New System.Drawing.Point(0, 0)
        Me.grArticulos.MainView = Me.BrvArticulos
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1})
        Me.grArticulos.Size = New System.Drawing.Size(975, 451)
        Me.grArticulos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.TabIndex = 65
        Me.grArticulos.Text = "GridControl1"
        '
        'BrvArticulos
        '
        Me.BrvArticulos.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.GridBand2, Me.GridBand4, Me.GridBand3, Me.GridBand5, Me.GridBand6, Me.GridBand13})
        Me.BrvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcProveedor, Me.grcInicial, Me.grcComprasEne, Me.grcComprasFeb, Me.grcComprasMar, Me.grcComprasAbr, Me.grcComprasMay, Me.grcComprasJun, Me.grcComprasJul, Me.grcComprasAgo, Me.grcComprasSep, Me.grcComprasOct, Me.grcComprasNov, Me.grcComprasDic, Me.grcComprasTotal, Me.grcVentasEne, Me.grcVentasFeb, Me.grcVentasMar, Me.grcVentasAbr, Me.grcVentasMay, Me.grcVentasJun, Me.grcVentasJul, Me.grcVentasAgo, Me.grcVentasSep, Me.grcVentasOct, Me.grcVentasNov, Me.grcVentasDic, Me.grcVentasTotal, Me.grcEntradasDevCTE, Me.grcEntradasMovEsp, Me.grcEntradasAjustes, Me.grcEntradasTotal, Me.grcSalidasDevPro, Me.grcSalidasMovEsp, Me.grcSalidasAjustes, Me.grcSalidasTotal, Me.grcExistencias, Me.grcValor, Me.grcdias_inventario, Me.grcpor_surtir})
        Me.BrvArticulos.GridControl = Me.grArticulos
        Me.BrvArticulos.Name = "BrvArticulos"
        Me.BrvArticulos.OptionsBehavior.Editable = False
        Me.BrvArticulos.OptionsView.ShowGroupedColumns = False
        Me.BrvArticulos.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "Articulos"
        Me.GridBand1.Columns.Add(Me.grcArticulo)
        Me.GridBand1.Columns.Add(Me.grcModelo)
        Me.GridBand1.Columns.Add(Me.grcDescripcion)
        Me.GridBand1.Columns.Add(Me.grcProveedor)
        Me.GridBand1.Columns.Add(Me.grcInicial)
        Me.GridBand1.HeaderStyleName = "Style1"
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Width = 327
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Articulo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.SortIndex = 0
        Me.grcArticulo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcArticulo.Visible = True
        Me.grcArticulo.Width = 69
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Visible = True
        Me.grcModelo.Width = 69
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Visible = True
        Me.grcDescripcion.Width = 60
        '
        'grcProveedor
        '
        Me.grcProveedor.Caption = "Proveedor"
        Me.grcProveedor.FieldName = "proveedor"
        Me.grcProveedor.Name = "grcProveedor"
        Me.grcProveedor.Visible = True
        Me.grcProveedor.Width = 60
        '
        'grcInicial
        '
        Me.grcInicial.Caption = "Inicial"
        Me.grcInicial.FieldName = "inicial"
        Me.grcInicial.Name = "grcInicial"
        Me.grcInicial.Visible = True
        Me.grcInicial.Width = 69
        '
        'GridBand2
        '
        Me.GridBand2.Caption = "Compras"
        Me.GridBand2.Columns.Add(Me.grcComprasEne)
        Me.GridBand2.Columns.Add(Me.grcComprasFeb)
        Me.GridBand2.Columns.Add(Me.grcComprasMar)
        Me.GridBand2.Columns.Add(Me.grcComprasAbr)
        Me.GridBand2.Columns.Add(Me.grcComprasMay)
        Me.GridBand2.Columns.Add(Me.grcComprasJun)
        Me.GridBand2.Columns.Add(Me.grcComprasJul)
        Me.GridBand2.Columns.Add(Me.grcComprasAgo)
        Me.GridBand2.Columns.Add(Me.grcComprasSep)
        Me.GridBand2.Columns.Add(Me.grcComprasOct)
        Me.GridBand2.Columns.Add(Me.grcComprasNov)
        Me.GridBand2.Columns.Add(Me.grcComprasDic)
        Me.GridBand2.Columns.Add(Me.grcComprasTotal)
        Me.GridBand2.HeaderStyleName = "Style1"
        Me.GridBand2.Name = "GridBand2"
        Me.GridBand2.Width = 61
        '
        'grcComprasEne
        '
        Me.grcComprasEne.Caption = "ENE"
        Me.grcComprasEne.FieldName = "c_ene"
        Me.grcComprasEne.Name = "grcComprasEne"
        Me.grcComprasEne.Width = 20
        '
        'grcComprasFeb
        '
        Me.grcComprasFeb.Caption = "FEB"
        Me.grcComprasFeb.FieldName = "c_feb"
        Me.grcComprasFeb.Name = "grcComprasFeb"
        Me.grcComprasFeb.Width = 20
        '
        'grcComprasMar
        '
        Me.grcComprasMar.Caption = "MAR"
        Me.grcComprasMar.FieldName = "c_mar"
        Me.grcComprasMar.Name = "grcComprasMar"
        Me.grcComprasMar.Width = 20
        '
        'grcComprasAbr
        '
        Me.grcComprasAbr.Caption = "ABR"
        Me.grcComprasAbr.FieldName = "c_abr"
        Me.grcComprasAbr.Name = "grcComprasAbr"
        Me.grcComprasAbr.Width = 20
        '
        'grcComprasMay
        '
        Me.grcComprasMay.Caption = "MAY"
        Me.grcComprasMay.FieldName = "c_may"
        Me.grcComprasMay.Name = "grcComprasMay"
        Me.grcComprasMay.Width = 20
        '
        'grcComprasJun
        '
        Me.grcComprasJun.Caption = "JUN"
        Me.grcComprasJun.FieldName = "c_jun"
        Me.grcComprasJun.Name = "grcComprasJun"
        Me.grcComprasJun.Width = 20
        '
        'grcComprasJul
        '
        Me.grcComprasJul.Caption = "JUL"
        Me.grcComprasJul.FieldName = "c_jul"
        Me.grcComprasJul.Name = "grcComprasJul"
        Me.grcComprasJul.Width = 23
        '
        'grcComprasAgo
        '
        Me.grcComprasAgo.Caption = "AGO"
        Me.grcComprasAgo.FieldName = "c_ago"
        Me.grcComprasAgo.Name = "grcComprasAgo"
        Me.grcComprasAgo.Width = 26
        '
        'grcComprasSep
        '
        Me.grcComprasSep.Caption = "SEP"
        Me.grcComprasSep.FieldName = "c_sep"
        Me.grcComprasSep.Name = "grcComprasSep"
        Me.grcComprasSep.Width = 28
        '
        'grcComprasOct
        '
        Me.grcComprasOct.Caption = "OCT"
        Me.grcComprasOct.FieldName = "c_oct"
        Me.grcComprasOct.Name = "grcComprasOct"
        Me.grcComprasOct.Width = 20
        '
        'grcComprasNov
        '
        Me.grcComprasNov.Caption = "NOV"
        Me.grcComprasNov.FieldName = "c_nov"
        Me.grcComprasNov.Name = "grcComprasNov"
        Me.grcComprasNov.Width = 21
        '
        'grcComprasDic
        '
        Me.grcComprasDic.Caption = "DIC"
        Me.grcComprasDic.FieldName = "c_dic"
        Me.grcComprasDic.Name = "grcComprasDic"
        Me.grcComprasDic.Width = 22
        '
        'grcComprasTotal
        '
        Me.grcComprasTotal.Caption = "TOTAL"
        Me.grcComprasTotal.FieldName = "compras"
        Me.grcComprasTotal.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.grcComprasTotal.Name = "grcComprasTotal"
        Me.grcComprasTotal.Visible = True
        Me.grcComprasTotal.Width = 61
        '
        'GridBand4
        '
        Me.GridBand4.Caption = "Entradas"
        Me.GridBand4.Columns.Add(Me.grcEntradasDevCTE)
        Me.GridBand4.Columns.Add(Me.grcEntradasMovEsp)
        Me.GridBand4.Columns.Add(Me.grcEntradasAjustes)
        Me.GridBand4.Columns.Add(Me.grcEntradasTotal)
        Me.GridBand4.HeaderStyleName = "Style1"
        Me.GridBand4.Name = "GridBand4"
        Me.GridBand4.Width = 69
        '
        'grcEntradasDevCTE
        '
        Me.grcEntradasDevCTE.Caption = "DEV CTE"
        Me.grcEntradasDevCTE.FieldName = "e_dev_cte"
        Me.grcEntradasDevCTE.Name = "grcEntradasDevCTE"
        Me.grcEntradasDevCTE.Width = 63
        '
        'grcEntradasMovEsp
        '
        Me.grcEntradasMovEsp.Caption = "Mov Esp"
        Me.grcEntradasMovEsp.FieldName = "e_mov_esp"
        Me.grcEntradasMovEsp.Name = "grcEntradasMovEsp"
        Me.grcEntradasMovEsp.Width = 42
        '
        'grcEntradasAjustes
        '
        Me.grcEntradasAjustes.Caption = "Ajustes"
        Me.grcEntradasAjustes.FieldName = "e_ajustes"
        Me.grcEntradasAjustes.Name = "grcEntradasAjustes"
        Me.grcEntradasAjustes.Width = 21
        '
        'grcEntradasTotal
        '
        Me.grcEntradasTotal.Caption = "TOTAL"
        Me.grcEntradasTotal.FieldName = "entradas"
        Me.grcEntradasTotal.Name = "grcEntradasTotal"
        Me.grcEntradasTotal.Visible = True
        Me.grcEntradasTotal.Width = 69
        '
        'GridBand3
        '
        Me.GridBand3.Caption = "Ventas"
        Me.GridBand3.Columns.Add(Me.grcVentasEne)
        Me.GridBand3.Columns.Add(Me.grcVentasFeb)
        Me.GridBand3.Columns.Add(Me.grcVentasMar)
        Me.GridBand3.Columns.Add(Me.grcVentasAbr)
        Me.GridBand3.Columns.Add(Me.grcVentasMay)
        Me.GridBand3.Columns.Add(Me.grcVentasJun)
        Me.GridBand3.Columns.Add(Me.grcVentasJul)
        Me.GridBand3.Columns.Add(Me.grcVentasAgo)
        Me.GridBand3.Columns.Add(Me.grcVentasSep)
        Me.GridBand3.Columns.Add(Me.grcVentasOct)
        Me.GridBand3.Columns.Add(Me.grcVentasNov)
        Me.GridBand3.Columns.Add(Me.grcVentasDic)
        Me.GridBand3.Columns.Add(Me.grcVentasTotal)
        Me.GridBand3.HeaderStyleName = "Style1"
        Me.GridBand3.Name = "GridBand3"
        Me.GridBand3.Width = 61
        '
        'grcVentasEne
        '
        Me.grcVentasEne.Caption = "ENE"
        Me.grcVentasEne.FieldName = "v_ene"
        Me.grcVentasEne.Name = "grcVentasEne"
        Me.grcVentasEne.Width = 49
        '
        'grcVentasFeb
        '
        Me.grcVentasFeb.Caption = "FEB"
        Me.grcVentasFeb.FieldName = "v_feb"
        Me.grcVentasFeb.Name = "grcVentasFeb"
        Me.grcVentasFeb.Width = 31
        '
        'grcVentasMar
        '
        Me.grcVentasMar.Caption = "MAR"
        Me.grcVentasMar.FieldName = "v_mar"
        Me.grcVentasMar.Name = "grcVentasMar"
        Me.grcVentasMar.Width = 20
        '
        'grcVentasAbr
        '
        Me.grcVentasAbr.Caption = "ABR"
        Me.grcVentasAbr.FieldName = "v_abr"
        Me.grcVentasAbr.Name = "grcVentasAbr"
        Me.grcVentasAbr.Width = 20
        '
        'grcVentasMay
        '
        Me.grcVentasMay.Caption = "MAY"
        Me.grcVentasMay.FieldName = "v_may"
        Me.grcVentasMay.Name = "grcVentasMay"
        Me.grcVentasMay.Width = 20
        '
        'grcVentasJun
        '
        Me.grcVentasJun.Caption = "JUN"
        Me.grcVentasJun.FieldName = "v_jun"
        Me.grcVentasJun.Name = "grcVentasJun"
        Me.grcVentasJun.Width = 20
        '
        'grcVentasJul
        '
        Me.grcVentasJul.Caption = "JUL"
        Me.grcVentasJul.FieldName = "v_jul"
        Me.grcVentasJul.Name = "grcVentasJul"
        Me.grcVentasJul.Width = 20
        '
        'grcVentasAgo
        '
        Me.grcVentasAgo.Caption = "AGO"
        Me.grcVentasAgo.FieldName = "v_ago"
        Me.grcVentasAgo.Name = "grcVentasAgo"
        Me.grcVentasAgo.Width = 20
        '
        'grcVentasSep
        '
        Me.grcVentasSep.Caption = "SEP"
        Me.grcVentasSep.FieldName = "v_sep"
        Me.grcVentasSep.Name = "grcVentasSep"
        Me.grcVentasSep.Width = 20
        '
        'grcVentasOct
        '
        Me.grcVentasOct.Caption = "OCT"
        Me.grcVentasOct.FieldName = "v_oct"
        Me.grcVentasOct.Name = "grcVentasOct"
        Me.grcVentasOct.Width = 20
        '
        'grcVentasNov
        '
        Me.grcVentasNov.Caption = "NOV"
        Me.grcVentasNov.FieldName = "v_nov"
        Me.grcVentasNov.Name = "grcVentasNov"
        Me.grcVentasNov.Width = 20
        '
        'grcVentasDic
        '
        Me.grcVentasDic.Caption = "DIC"
        Me.grcVentasDic.FieldName = "v_dic"
        Me.grcVentasDic.Name = "grcVentasDic"
        Me.grcVentasDic.Width = 20
        '
        'grcVentasTotal
        '
        Me.grcVentasTotal.Caption = "TOTAL"
        Me.grcVentasTotal.FieldName = "ventas"
        Me.grcVentasTotal.Name = "grcVentasTotal"
        Me.grcVentasTotal.Visible = True
        Me.grcVentasTotal.Width = 61
        '
        'GridBand5
        '
        Me.GridBand5.Caption = "Salidas"
        Me.GridBand5.Columns.Add(Me.grcSalidasDevPro)
        Me.GridBand5.Columns.Add(Me.grcSalidasMovEsp)
        Me.GridBand5.Columns.Add(Me.grcSalidasAjustes)
        Me.GridBand5.Columns.Add(Me.grcSalidasTotal)
        Me.GridBand5.HeaderStyleName = "Style1"
        Me.GridBand5.Name = "GridBand5"
        Me.GridBand5.Width = 61
        '
        'grcSalidasDevPro
        '
        Me.grcSalidasDevPro.Caption = "Dev Pro"
        Me.grcSalidasDevPro.FieldName = "s_dev_prov"
        Me.grcSalidasDevPro.Name = "grcSalidasDevPro"
        Me.grcSalidasDevPro.Width = 31
        '
        'grcSalidasMovEsp
        '
        Me.grcSalidasMovEsp.Caption = "Mov Esp"
        Me.grcSalidasMovEsp.FieldName = "s_mov_esp"
        Me.grcSalidasMovEsp.Name = "grcSalidasMovEsp"
        Me.grcSalidasMovEsp.Width = 20
        '
        'grcSalidasAjustes
        '
        Me.grcSalidasAjustes.Caption = "Ajustes"
        Me.grcSalidasAjustes.FieldName = "s_ajustes"
        Me.grcSalidasAjustes.Name = "grcSalidasAjustes"
        Me.grcSalidasAjustes.Width = 20
        '
        'grcSalidasTotal
        '
        Me.grcSalidasTotal.Caption = "TOTAL"
        Me.grcSalidasTotal.FieldName = "salidas"
        Me.grcSalidasTotal.Name = "grcSalidasTotal"
        Me.grcSalidasTotal.Visible = True
        Me.grcSalidasTotal.Width = 61
        '
        'GridBand6
        '
        Me.GridBand6.Caption = "Existencias"
        Me.GridBand6.Columns.Add(Me.grcExistencias)
        Me.GridBand6.Columns.Add(Me.grcValor)
        Me.GridBand6.HeaderStyleName = "Style1"
        Me.GridBand6.Name = "GridBand6"
        Me.GridBand6.Width = 123
        '
        'grcExistencias
        '
        Me.grcExistencias.Caption = "Existencias"
        Me.grcExistencias.FieldName = "exist"
        Me.grcExistencias.Name = "grcExistencias"
        Me.grcExistencias.Visible = True
        Me.grcExistencias.Width = 61
        '
        'grcValor
        '
        Me.grcValor.Caption = "Valor"
        Me.grcValor.DisplayFormat.FormatString = "c2"
        Me.grcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcValor.FieldName = "valor"
        Me.grcValor.Name = "grcValor"
        Me.grcValor.Visible = True
        Me.grcValor.Width = 62
        '
        'GridBand13
        '
        Me.GridBand13.Columns.Add(Me.grcdias_inventario)
        Me.GridBand13.Columns.Add(Me.grcpor_surtir)
        Me.GridBand13.Name = "GridBand13"
        Me.GridBand13.Width = 154
        '
        'grcdias_inventario
        '
        Me.grcdias_inventario.Caption = "D�as Inventario"
        Me.grcdias_inventario.FieldName = "dias_inventario"
        Me.grcdias_inventario.Name = "grcdias_inventario"
        Me.grcdias_inventario.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcdias_inventario.Visible = True
        Me.grcdias_inventario.Width = 83
        '
        'grcpor_surtir
        '
        Me.grcpor_surtir.Caption = "Por Surtir"
        Me.grcpor_surtir.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcpor_surtir.FieldName = "por_surtir"
        Me.grcpor_surtir.Name = "grcpor_surtir"
        Me.grcpor_surtir.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcpor_surtir.Visible = True
        Me.grcpor_surtir.Width = 71
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(7, 128)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(983, 477)
        Me.TabControl1.TabIndex = 6
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grArticulos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(975, 451)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Articulos"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grGrupos)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(975, 451)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = " Grupos"
        '
        'grGrupos
        '
        Me.grGrupos.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grGrupos.EmbeddedNavigator
        '
        Me.grGrupos.EmbeddedNavigator.Name = ""
        Me.grGrupos.Location = New System.Drawing.Point(0, 0)
        Me.grGrupos.MainView = Me.BrvGrupos
        Me.grGrupos.Name = "grGrupos"
        Me.grGrupos.Size = New System.Drawing.Size(975, 451)
        Me.grGrupos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grGrupos.TabIndex = 66
        Me.grGrupos.Text = "GridControl2"
        '
        'BrvGrupos
        '
        Me.BrvGrupos.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand7, Me.GridBand11, Me.GridBand9, Me.GridBand8, Me.GridBand10, Me.GridBand12})
        Me.BrvGrupos.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.grcGrupo, Me.grcGrupoInicial, Me.grcGrupoComprasEne, Me.grcGrupoComprasFeb, Me.grcGrupoComprasMar, Me.grcGrupoComprasAbr, Me.grcGrupoComprasMay, Me.grcGrupoComprasJun, Me.grcGrupoComprasJul, Me.grcGrupoComprasAgo, Me.grcGrupoComprasSep, Me.grcGrupoComprasOct, Me.grcGrupoComprasNov, Me.grcGrupoComprasDic, Me.grcGrupoComprasTotal, Me.grcGrupoVentasEne, Me.grcGrupoVentasFeb, Me.grcGrupoVentasMar, Me.grcGrupoVentasAbr, Me.grcGrupoVentasMay, Me.grcGrupoVentasJun, Me.grcGrupoVentasJul, Me.grcGrupoVentasAgo, Me.grcGrupoVentasSep, Me.grcGrupoVentasOct, Me.grcGrupoVentasNov, Me.grcGrupoVentasDic, Me.grcGrupoVentasTotal, Me.grcGrupoEntradasDevCte, Me.grcGrupoEntradasMovEsp, Me.grcGrupoEntradasAjustes, Me.grcGrupoEntradasTotal, Me.grcGrupoSalidasDevPro, Me.grcGrupoSalidasMovEsp, Me.grcGrupoSalidasAjustes, Me.grcGrupoSalidasTotal, Me.grcGrupoExistencias, Me.grcGrupoValor})
        Me.BrvGrupos.GridControl = Me.grGrupos
        Me.BrvGrupos.Name = "BrvGrupos"
        Me.BrvGrupos.OptionsBehavior.Editable = False
        Me.BrvGrupos.OptionsView.ShowGroupedColumns = False
        Me.BrvGrupos.OptionsView.ShowGroupPanel = False
        '
        'GridBand7
        '
        Me.GridBand7.Caption = "Grupos"
        Me.GridBand7.Columns.Add(Me.grcGrupo)
        Me.GridBand7.Columns.Add(Me.grcGrupoInicial)
        Me.GridBand7.HeaderStyleName = "Style1"
        Me.GridBand7.Name = "GridBand7"
        Me.GridBand7.Width = 69
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        Me.grcGrupo.SortIndex = 0
        Me.grcGrupo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcGrupo.Visible = True
        Me.grcGrupo.Width = 34
        '
        'grcGrupoInicial
        '
        Me.grcGrupoInicial.Caption = "Inicial"
        Me.grcGrupoInicial.FieldName = "inicial"
        Me.grcGrupoInicial.Name = "grcGrupoInicial"
        Me.grcGrupoInicial.Visible = True
        Me.grcGrupoInicial.Width = 35
        '
        'GridBand11
        '
        Me.GridBand11.Caption = "Compras"
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasEne)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasFeb)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasMar)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasAbr)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasMay)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasJun)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasJul)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasAgo)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasSep)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasOct)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasNov)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasDic)
        Me.GridBand11.Columns.Add(Me.grcGrupoComprasTotal)
        Me.GridBand11.HeaderStyleName = "Style1"
        Me.GridBand11.Name = "GridBand11"
        Me.GridBand11.Width = 20
        '
        'grcGrupoComprasEne
        '
        Me.grcGrupoComprasEne.Caption = "ENE"
        Me.grcGrupoComprasEne.FieldName = "c_ene"
        Me.grcGrupoComprasEne.Name = "grcGrupoComprasEne"
        Me.grcGrupoComprasEne.Width = 24
        '
        'grcGrupoComprasFeb
        '
        Me.grcGrupoComprasFeb.Caption = "FEB"
        Me.grcGrupoComprasFeb.FieldName = "c_feb"
        Me.grcGrupoComprasFeb.Name = "grcGrupoComprasFeb"
        Me.grcGrupoComprasFeb.Width = 20
        '
        'grcGrupoComprasMar
        '
        Me.grcGrupoComprasMar.Caption = "MAR"
        Me.grcGrupoComprasMar.FieldName = "c_mar"
        Me.grcGrupoComprasMar.Name = "grcGrupoComprasMar"
        Me.grcGrupoComprasMar.Width = 20
        '
        'grcGrupoComprasAbr
        '
        Me.grcGrupoComprasAbr.Caption = "ABR"
        Me.grcGrupoComprasAbr.FieldName = "c_abr"
        Me.grcGrupoComprasAbr.Name = "grcGrupoComprasAbr"
        Me.grcGrupoComprasAbr.Width = 20
        '
        'grcGrupoComprasMay
        '
        Me.grcGrupoComprasMay.Caption = "MAY"
        Me.grcGrupoComprasMay.FieldName = "c_may"
        Me.grcGrupoComprasMay.Name = "grcGrupoComprasMay"
        Me.grcGrupoComprasMay.Width = 20
        '
        'grcGrupoComprasJun
        '
        Me.grcGrupoComprasJun.Caption = "JUN"
        Me.grcGrupoComprasJun.FieldName = "c_jun"
        Me.grcGrupoComprasJun.Name = "grcGrupoComprasJun"
        Me.grcGrupoComprasJun.Width = 20
        '
        'grcGrupoComprasJul
        '
        Me.grcGrupoComprasJul.Caption = "JUL"
        Me.grcGrupoComprasJul.FieldName = "c_jul"
        Me.grcGrupoComprasJul.Name = "grcGrupoComprasJul"
        Me.grcGrupoComprasJul.Width = 20
        '
        'grcGrupoComprasAgo
        '
        Me.grcGrupoComprasAgo.Caption = "AGO"
        Me.grcGrupoComprasAgo.FieldName = "c_ago"
        Me.grcGrupoComprasAgo.Name = "grcGrupoComprasAgo"
        Me.grcGrupoComprasAgo.Width = 20
        '
        'grcGrupoComprasSep
        '
        Me.grcGrupoComprasSep.Caption = "SEP"
        Me.grcGrupoComprasSep.FieldName = "c_sep"
        Me.grcGrupoComprasSep.Name = "grcGrupoComprasSep"
        Me.grcGrupoComprasSep.Width = 20
        '
        'grcGrupoComprasOct
        '
        Me.grcGrupoComprasOct.Caption = "OCT"
        Me.grcGrupoComprasOct.FieldName = "c_oct"
        Me.grcGrupoComprasOct.Name = "grcGrupoComprasOct"
        Me.grcGrupoComprasOct.Width = 20
        '
        'grcGrupoComprasNov
        '
        Me.grcGrupoComprasNov.Caption = "NOV"
        Me.grcGrupoComprasNov.FieldName = "c_nov"
        Me.grcGrupoComprasNov.Name = "grcGrupoComprasNov"
        Me.grcGrupoComprasNov.Width = 20
        '
        'grcGrupoComprasDic
        '
        Me.grcGrupoComprasDic.Caption = "DIC"
        Me.grcGrupoComprasDic.FieldName = "c_dic"
        Me.grcGrupoComprasDic.Name = "grcGrupoComprasDic"
        Me.grcGrupoComprasDic.Width = 20
        '
        'grcGrupoComprasTotal
        '
        Me.grcGrupoComprasTotal.Caption = "TOTAL"
        Me.grcGrupoComprasTotal.FieldName = "compras"
        Me.grcGrupoComprasTotal.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.grcGrupoComprasTotal.Name = "grcGrupoComprasTotal"
        Me.grcGrupoComprasTotal.Visible = True
        Me.grcGrupoComprasTotal.Width = 20
        '
        'GridBand9
        '
        Me.GridBand9.Caption = "Entradas"
        Me.GridBand9.Columns.Add(Me.grcGrupoEntradasDevCte)
        Me.GridBand9.Columns.Add(Me.grcGrupoEntradasMovEsp)
        Me.GridBand9.Columns.Add(Me.grcGrupoEntradasAjustes)
        Me.GridBand9.Columns.Add(Me.grcGrupoEntradasTotal)
        Me.GridBand9.HeaderStyleName = "Style1"
        Me.GridBand9.Name = "GridBand9"
        Me.GridBand9.Width = 20
        '
        'grcGrupoEntradasDevCte
        '
        Me.grcGrupoEntradasDevCte.Caption = "DEV CTE"
        Me.grcGrupoEntradasDevCte.FieldName = "e_dev_cte"
        Me.grcGrupoEntradasDevCte.Name = "grcGrupoEntradasDevCte"
        Me.grcGrupoEntradasDevCte.Width = 40
        '
        'grcGrupoEntradasMovEsp
        '
        Me.grcGrupoEntradasMovEsp.Caption = "Mov Esp"
        Me.grcGrupoEntradasMovEsp.FieldName = "e_mov_esp"
        Me.grcGrupoEntradasMovEsp.Name = "grcGrupoEntradasMovEsp"
        Me.grcGrupoEntradasMovEsp.Width = 20
        '
        'grcGrupoEntradasAjustes
        '
        Me.grcGrupoEntradasAjustes.Caption = "Ajustes"
        Me.grcGrupoEntradasAjustes.FieldName = "e_ajustes"
        Me.grcGrupoEntradasAjustes.Name = "grcGrupoEntradasAjustes"
        Me.grcGrupoEntradasAjustes.Width = 20
        '
        'grcGrupoEntradasTotal
        '
        Me.grcGrupoEntradasTotal.Caption = "TOTAL"
        Me.grcGrupoEntradasTotal.FieldName = "entradas"
        Me.grcGrupoEntradasTotal.Name = "grcGrupoEntradasTotal"
        Me.grcGrupoEntradasTotal.Visible = True
        Me.grcGrupoEntradasTotal.Width = 20
        '
        'GridBand8
        '
        Me.GridBand8.Caption = "Ventas"
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasEne)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasFeb)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasMar)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasAbr)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasMay)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasJun)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasJul)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasAgo)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasSep)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasOct)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasNov)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasDic)
        Me.GridBand8.Columns.Add(Me.grcGrupoVentasTotal)
        Me.GridBand8.HeaderStyleName = "Style1"
        Me.GridBand8.Name = "GridBand8"
        Me.GridBand8.Width = 22
        '
        'grcGrupoVentasEne
        '
        Me.grcGrupoVentasEne.Caption = "ENE"
        Me.grcGrupoVentasEne.FieldName = "v_ene"
        Me.grcGrupoVentasEne.Name = "grcGrupoVentasEne"
        Me.grcGrupoVentasEne.Width = 34
        '
        'grcGrupoVentasFeb
        '
        Me.grcGrupoVentasFeb.Caption = "FEB"
        Me.grcGrupoVentasFeb.FieldName = "v_feb"
        Me.grcGrupoVentasFeb.Name = "grcGrupoVentasFeb"
        Me.grcGrupoVentasFeb.Width = 20
        '
        'grcGrupoVentasMar
        '
        Me.grcGrupoVentasMar.Caption = "MAR"
        Me.grcGrupoVentasMar.FieldName = "v_mar"
        Me.grcGrupoVentasMar.Name = "grcGrupoVentasMar"
        Me.grcGrupoVentasMar.Width = 20
        '
        'grcGrupoVentasAbr
        '
        Me.grcGrupoVentasAbr.Caption = "ABR"
        Me.grcGrupoVentasAbr.FieldName = "v_abr"
        Me.grcGrupoVentasAbr.Name = "grcGrupoVentasAbr"
        Me.grcGrupoVentasAbr.Width = 20
        '
        'grcGrupoVentasMay
        '
        Me.grcGrupoVentasMay.Caption = "MAY"
        Me.grcGrupoVentasMay.FieldName = "v_may"
        Me.grcGrupoVentasMay.Name = "grcGrupoVentasMay"
        Me.grcGrupoVentasMay.Width = 20
        '
        'grcGrupoVentasJun
        '
        Me.grcGrupoVentasJun.Caption = "JUN"
        Me.grcGrupoVentasJun.FieldName = "v_jun"
        Me.grcGrupoVentasJun.Name = "grcGrupoVentasJun"
        Me.grcGrupoVentasJun.Width = 20
        '
        'grcGrupoVentasJul
        '
        Me.grcGrupoVentasJul.Caption = "JUL"
        Me.grcGrupoVentasJul.FieldName = "v_jul"
        Me.grcGrupoVentasJul.Name = "grcGrupoVentasJul"
        Me.grcGrupoVentasJul.Width = 20
        '
        'grcGrupoVentasAgo
        '
        Me.grcGrupoVentasAgo.Caption = "AGO"
        Me.grcGrupoVentasAgo.FieldName = "v_ago"
        Me.grcGrupoVentasAgo.Name = "grcGrupoVentasAgo"
        Me.grcGrupoVentasAgo.Width = 20
        '
        'grcGrupoVentasSep
        '
        Me.grcGrupoVentasSep.Caption = "SEP"
        Me.grcGrupoVentasSep.FieldName = "v_sep"
        Me.grcGrupoVentasSep.Name = "grcGrupoVentasSep"
        Me.grcGrupoVentasSep.Width = 20
        '
        'grcGrupoVentasOct
        '
        Me.grcGrupoVentasOct.Caption = "OCT"
        Me.grcGrupoVentasOct.FieldName = "v_oct"
        Me.grcGrupoVentasOct.Name = "grcGrupoVentasOct"
        Me.grcGrupoVentasOct.Width = 20
        '
        'grcGrupoVentasNov
        '
        Me.grcGrupoVentasNov.Caption = "NOV"
        Me.grcGrupoVentasNov.FieldName = "v_nov"
        Me.grcGrupoVentasNov.Name = "grcGrupoVentasNov"
        Me.grcGrupoVentasNov.Width = 20
        '
        'grcGrupoVentasDic
        '
        Me.grcGrupoVentasDic.Caption = "DIC"
        Me.grcGrupoVentasDic.FieldName = "v_dic"
        Me.grcGrupoVentasDic.Name = "grcGrupoVentasDic"
        Me.grcGrupoVentasDic.Width = 20
        '
        'grcGrupoVentasTotal
        '
        Me.grcGrupoVentasTotal.Caption = "TOTAL"
        Me.grcGrupoVentasTotal.FieldName = "ventas"
        Me.grcGrupoVentasTotal.Name = "grcGrupoVentasTotal"
        Me.grcGrupoVentasTotal.Visible = True
        Me.grcGrupoVentasTotal.Width = 22
        '
        'GridBand10
        '
        Me.GridBand10.Caption = "Salidas"
        Me.GridBand10.Columns.Add(Me.grcGrupoSalidasDevPro)
        Me.GridBand10.Columns.Add(Me.grcGrupoSalidasMovEsp)
        Me.GridBand10.Columns.Add(Me.grcGrupoSalidasAjustes)
        Me.GridBand10.Columns.Add(Me.grcGrupoSalidasTotal)
        Me.GridBand10.HeaderStyleName = "Style1"
        Me.GridBand10.Name = "GridBand10"
        Me.GridBand10.Width = 24
        '
        'grcGrupoSalidasDevPro
        '
        Me.grcGrupoSalidasDevPro.Caption = "Dev Pro"
        Me.grcGrupoSalidasDevPro.FieldName = "s_dev_prov"
        Me.grcGrupoSalidasDevPro.Name = "grcGrupoSalidasDevPro"
        Me.grcGrupoSalidasDevPro.Width = 34
        '
        'grcGrupoSalidasMovEsp
        '
        Me.grcGrupoSalidasMovEsp.Caption = "Mov Esp"
        Me.grcGrupoSalidasMovEsp.FieldName = "s_mov_esp"
        Me.grcGrupoSalidasMovEsp.Name = "grcGrupoSalidasMovEsp"
        Me.grcGrupoSalidasMovEsp.Width = 23
        '
        'grcGrupoSalidasAjustes
        '
        Me.grcGrupoSalidasAjustes.Caption = "Ajustes"
        Me.grcGrupoSalidasAjustes.FieldName = "s_ajustes"
        Me.grcGrupoSalidasAjustes.Name = "grcGrupoSalidasAjustes"
        Me.grcGrupoSalidasAjustes.Width = 24
        '
        'grcGrupoSalidasTotal
        '
        Me.grcGrupoSalidasTotal.Caption = "TOTAL"
        Me.grcGrupoSalidasTotal.FieldName = "salidas"
        Me.grcGrupoSalidasTotal.Name = "grcGrupoSalidasTotal"
        Me.grcGrupoSalidasTotal.Visible = True
        Me.grcGrupoSalidasTotal.Width = 24
        '
        'GridBand12
        '
        Me.GridBand12.Caption = "Existencias"
        Me.GridBand12.Columns.Add(Me.grcGrupoExistencias)
        Me.GridBand12.Columns.Add(Me.grcGrupoValor)
        Me.GridBand12.HeaderStyleName = "Style1"
        Me.GridBand12.Name = "GridBand12"
        Me.GridBand12.Width = 42
        '
        'grcGrupoExistencias
        '
        Me.grcGrupoExistencias.Caption = "Existencias"
        Me.grcGrupoExistencias.FieldName = "exist"
        Me.grcGrupoExistencias.Name = "grcGrupoExistencias"
        Me.grcGrupoExistencias.Visible = True
        Me.grcGrupoExistencias.Width = 21
        '
        'grcGrupoValor
        '
        Me.grcGrupoValor.Caption = "Valor"
        Me.grcGrupoValor.DisplayFormat.FormatString = "c2"
        Me.grcGrupoValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGrupoValor.FieldName = "valor"
        Me.grcGrupoValor.Name = "grcGrupoValor"
        Me.grcGrupoValor.Visible = True
        Me.grcGrupoValor.Width = 21
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.grClasificacion)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(975, 451)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Clasificacion"
        '
        'grClasificacion
        '
        Me.grClasificacion.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grClasificacion.EmbeddedNavigator
        '
        Me.grClasificacion.EmbeddedNavigator.Name = ""
        Me.grClasificacion.Location = New System.Drawing.Point(0, 0)
        Me.grClasificacion.MainView = Me.BrvClasificacion
        Me.grClasificacion.Name = "grClasificacion"
        Me.grClasificacion.Size = New System.Drawing.Size(975, 451)
        Me.grClasificacion.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClasificacion.TabIndex = 66
        Me.grClasificacion.Text = "GridControl3"
        '
        'BrvClasificacion
        '
        Me.BrvClasificacion.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand19, Me.GridBand14, Me.GridBand15, Me.GridBand16, Me.GridBand17, Me.GridBand18})
        Me.BrvClasificacion.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.grcClasificacion, Me.grcClasificacionInicial, Me.grcClasificacionComprasEne, Me.grcClasificacionComprasFeb, Me.grcClasificacionComprasMar, Me.grcClasificacionComprasAbr, Me.grcClasificacionComprasMay, Me.grcClasificacionComprasJun, Me.grcClasificacionComprasJul, Me.grcClasificacionComprasAgo, Me.grcClasificacionComprasSep, Me.grcClasificacionComprasOct, Me.grcClasificacionComprasNov, Me.grcClasificacionComprasDic, Me.grcClasificacionComprasTotal, Me.grcClasificacionVentasEne, Me.grcClasificacionVentasFeb, Me.grcClasificacionVentasMar, Me.grcClasificacionVentasAbr, Me.grcClasificacionVentasMay, Me.grcClasificacionVentasJun, Me.grcClasificacionVentasJul, Me.grcClasificacionVentasAgo, Me.grcClasificacionVentasSep, Me.grcClasificacionVentasOct, Me.grcClasificacionVentasNov, Me.grcClasificacionVentasDic, Me.grcClasificacionVentasTotal, Me.grcClasificacionEntradasDevCte, Me.grcClasificacionEntradasMovEsp, Me.grcClasificacionEntradasAjustes, Me.grcClasificacionEntradasTotal, Me.grcClasificacionSalidasDevPro, Me.grcClasificacionSalidasMovEsp, Me.grcClasificacionSalidasAjustes, Me.grcClasificacionSalidasTotal, Me.grcClasificacionExistencias, Me.grcClasificacionValor})
        Me.BrvClasificacion.GridControl = Me.grClasificacion
        Me.BrvClasificacion.Name = "BrvClasificacion"
        Me.BrvClasificacion.OptionsBehavior.Editable = False
        Me.BrvClasificacion.OptionsView.ShowGroupedColumns = False
        Me.BrvClasificacion.OptionsView.ShowGroupPanel = False
        '
        'GridBand19
        '
        Me.GridBand19.Caption = "Clasificacion"
        Me.GridBand19.Columns.Add(Me.grcClasificacion)
        Me.GridBand19.Columns.Add(Me.grcClasificacionInicial)
        Me.GridBand19.HeaderStyleName = "Style1"
        Me.GridBand19.Name = "GridBand19"
        Me.GridBand19.Width = 40
        '
        'grcClasificacion
        '
        Me.grcClasificacion.Caption = "Clasificaci�n"
        Me.grcClasificacion.FieldName = "clasificacion"
        Me.grcClasificacion.Name = "grcClasificacion"
        Me.grcClasificacion.Visible = True
        Me.grcClasificacion.Width = 20
        '
        'grcClasificacionInicial
        '
        Me.grcClasificacionInicial.Caption = "Inicial"
        Me.grcClasificacionInicial.FieldName = "inicial"
        Me.grcClasificacionInicial.Name = "grcClasificacionInicial"
        Me.grcClasificacionInicial.Visible = True
        Me.grcClasificacionInicial.Width = 20
        '
        'GridBand14
        '
        Me.GridBand14.Caption = "Compras"
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasEne)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasFeb)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasMar)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasAbr)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasMay)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasJun)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasJul)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasAgo)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasSep)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasOct)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasNov)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasDic)
        Me.GridBand14.Columns.Add(Me.grcClasificacionComprasTotal)
        Me.GridBand14.HeaderStyleName = "Style1"
        Me.GridBand14.Name = "GridBand14"
        Me.GridBand14.Width = 20
        '
        'grcClasificacionComprasEne
        '
        Me.grcClasificacionComprasEne.Caption = "ENE"
        Me.grcClasificacionComprasEne.FieldName = "c_ene"
        Me.grcClasificacionComprasEne.Name = "grcClasificacionComprasEne"
        Me.grcClasificacionComprasEne.Width = 20
        '
        'grcClasificacionComprasFeb
        '
        Me.grcClasificacionComprasFeb.Caption = "FEB"
        Me.grcClasificacionComprasFeb.FieldName = "c_feb"
        Me.grcClasificacionComprasFeb.Name = "grcClasificacionComprasFeb"
        Me.grcClasificacionComprasFeb.Width = 20
        '
        'grcClasificacionComprasMar
        '
        Me.grcClasificacionComprasMar.Caption = "MAR"
        Me.grcClasificacionComprasMar.FieldName = "c_mar"
        Me.grcClasificacionComprasMar.Name = "grcClasificacionComprasMar"
        Me.grcClasificacionComprasMar.Width = 20
        '
        'grcClasificacionComprasAbr
        '
        Me.grcClasificacionComprasAbr.Caption = "ABR"
        Me.grcClasificacionComprasAbr.FieldName = "c_abr"
        Me.grcClasificacionComprasAbr.Name = "grcClasificacionComprasAbr"
        Me.grcClasificacionComprasAbr.Width = 20
        '
        'grcClasificacionComprasMay
        '
        Me.grcClasificacionComprasMay.Caption = "MAY"
        Me.grcClasificacionComprasMay.FieldName = "c_may"
        Me.grcClasificacionComprasMay.Name = "grcClasificacionComprasMay"
        Me.grcClasificacionComprasMay.Width = 20
        '
        'grcClasificacionComprasJun
        '
        Me.grcClasificacionComprasJun.Caption = "JUN"
        Me.grcClasificacionComprasJun.FieldName = "c_jun"
        Me.grcClasificacionComprasJun.Name = "grcClasificacionComprasJun"
        Me.grcClasificacionComprasJun.Width = 20
        '
        'grcClasificacionComprasJul
        '
        Me.grcClasificacionComprasJul.Caption = "JUL"
        Me.grcClasificacionComprasJul.FieldName = "c_jul"
        Me.grcClasificacionComprasJul.Name = "grcClasificacionComprasJul"
        Me.grcClasificacionComprasJul.Width = 20
        '
        'grcClasificacionComprasAgo
        '
        Me.grcClasificacionComprasAgo.Caption = "AGO"
        Me.grcClasificacionComprasAgo.FieldName = "c_ago"
        Me.grcClasificacionComprasAgo.Name = "grcClasificacionComprasAgo"
        Me.grcClasificacionComprasAgo.Width = 20
        '
        'grcClasificacionComprasSep
        '
        Me.grcClasificacionComprasSep.Caption = "SEP"
        Me.grcClasificacionComprasSep.FieldName = "c_sep"
        Me.grcClasificacionComprasSep.Name = "grcClasificacionComprasSep"
        Me.grcClasificacionComprasSep.Width = 20
        '
        'grcClasificacionComprasOct
        '
        Me.grcClasificacionComprasOct.Caption = "OCT"
        Me.grcClasificacionComprasOct.FieldName = "c_oct"
        Me.grcClasificacionComprasOct.Name = "grcClasificacionComprasOct"
        Me.grcClasificacionComprasOct.Width = 20
        '
        'grcClasificacionComprasNov
        '
        Me.grcClasificacionComprasNov.Caption = "NOV"
        Me.grcClasificacionComprasNov.FieldName = "c_nov"
        Me.grcClasificacionComprasNov.Name = "grcClasificacionComprasNov"
        Me.grcClasificacionComprasNov.Width = 20
        '
        'grcClasificacionComprasDic
        '
        Me.grcClasificacionComprasDic.Caption = "DIC"
        Me.grcClasificacionComprasDic.FieldName = "c_dic"
        Me.grcClasificacionComprasDic.Name = "grcClasificacionComprasDic"
        Me.grcClasificacionComprasDic.Width = 20
        '
        'grcClasificacionComprasTotal
        '
        Me.grcClasificacionComprasTotal.Caption = "TOTAL"
        Me.grcClasificacionComprasTotal.FieldName = "compras"
        Me.grcClasificacionComprasTotal.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.grcClasificacionComprasTotal.Name = "grcClasificacionComprasTotal"
        Me.grcClasificacionComprasTotal.Visible = True
        Me.grcClasificacionComprasTotal.Width = 20
        '
        'GridBand15
        '
        Me.GridBand15.Caption = "Entradas"
        Me.GridBand15.Columns.Add(Me.grcClasificacionEntradasDevCte)
        Me.GridBand15.Columns.Add(Me.grcClasificacionEntradasMovEsp)
        Me.GridBand15.Columns.Add(Me.grcClasificacionEntradasAjustes)
        Me.GridBand15.Columns.Add(Me.grcClasificacionEntradasTotal)
        Me.GridBand15.HeaderStyleName = "Style1"
        Me.GridBand15.Name = "GridBand15"
        Me.GridBand15.Width = 22
        '
        'grcClasificacionEntradasDevCte
        '
        Me.grcClasificacionEntradasDevCte.Caption = "DEV CTE"
        Me.grcClasificacionEntradasDevCte.FieldName = "e_dev_cte"
        Me.grcClasificacionEntradasDevCte.Name = "grcClasificacionEntradasDevCte"
        Me.grcClasificacionEntradasDevCte.Width = 29
        '
        'grcClasificacionEntradasMovEsp
        '
        Me.grcClasificacionEntradasMovEsp.Caption = "Mov Esp"
        Me.grcClasificacionEntradasMovEsp.FieldName = "e_mov_esp"
        Me.grcClasificacionEntradasMovEsp.Name = "grcClasificacionEntradasMovEsp"
        Me.grcClasificacionEntradasMovEsp.Width = 24
        '
        'grcClasificacionEntradasAjustes
        '
        Me.grcClasificacionEntradasAjustes.Caption = "Ajustes"
        Me.grcClasificacionEntradasAjustes.FieldName = "e_ajustes"
        Me.grcClasificacionEntradasAjustes.Name = "grcClasificacionEntradasAjustes"
        Me.grcClasificacionEntradasAjustes.Width = 20
        '
        'grcClasificacionEntradasTotal
        '
        Me.grcClasificacionEntradasTotal.Caption = "TOTAL"
        Me.grcClasificacionEntradasTotal.FieldName = "entradas"
        Me.grcClasificacionEntradasTotal.Name = "grcClasificacionEntradasTotal"
        Me.grcClasificacionEntradasTotal.Visible = True
        Me.grcClasificacionEntradasTotal.Width = 22
        '
        'GridBand16
        '
        Me.GridBand16.Caption = "Ventas"
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasEne)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasFeb)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasMar)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasAbr)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasMay)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasJul)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasJun)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasAgo)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasSep)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasOct)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasNov)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasDic)
        Me.GridBand16.Columns.Add(Me.grcClasificacionVentasTotal)
        Me.GridBand16.HeaderStyleName = "Style1"
        Me.GridBand16.Name = "GridBand16"
        Me.GridBand16.Width = 20
        '
        'grcClasificacionVentasEne
        '
        Me.grcClasificacionVentasEne.Caption = "ENE"
        Me.grcClasificacionVentasEne.FieldName = "v_ene"
        Me.grcClasificacionVentasEne.Name = "grcClasificacionVentasEne"
        Me.grcClasificacionVentasEne.Width = 69
        '
        'grcClasificacionVentasFeb
        '
        Me.grcClasificacionVentasFeb.Caption = "FEB"
        Me.grcClasificacionVentasFeb.FieldName = "v_feb"
        Me.grcClasificacionVentasFeb.Name = "grcClasificacionVentasFeb"
        Me.grcClasificacionVentasFeb.Width = 49
        '
        'grcClasificacionVentasMar
        '
        Me.grcClasificacionVentasMar.Caption = "MAR"
        Me.grcClasificacionVentasMar.FieldName = "v_mar"
        Me.grcClasificacionVentasMar.Name = "grcClasificacionVentasMar"
        Me.grcClasificacionVentasMar.Width = 31
        '
        'grcClasificacionVentasAbr
        '
        Me.grcClasificacionVentasAbr.Caption = "ABR"
        Me.grcClasificacionVentasAbr.FieldName = "v_abr"
        Me.grcClasificacionVentasAbr.Name = "grcClasificacionVentasAbr"
        Me.grcClasificacionVentasAbr.Width = 34
        '
        'grcClasificacionVentasMay
        '
        Me.grcClasificacionVentasMay.Caption = "MAY"
        Me.grcClasificacionVentasMay.FieldName = "v_may"
        Me.grcClasificacionVentasMay.Name = "grcClasificacionVentasMay"
        Me.grcClasificacionVentasMay.Width = 20
        '
        'grcClasificacionVentasJul
        '
        Me.grcClasificacionVentasJul.Caption = "JUL"
        Me.grcClasificacionVentasJul.FieldName = "v_jul"
        Me.grcClasificacionVentasJul.Name = "grcClasificacionVentasJul"
        Me.grcClasificacionVentasJul.Width = 20
        '
        'grcClasificacionVentasJun
        '
        Me.grcClasificacionVentasJun.Caption = "JUN"
        Me.grcClasificacionVentasJun.FieldName = "v_jun"
        Me.grcClasificacionVentasJun.Name = "grcClasificacionVentasJun"
        Me.grcClasificacionVentasJun.Width = 20
        '
        'grcClasificacionVentasAgo
        '
        Me.grcClasificacionVentasAgo.Caption = "AGO"
        Me.grcClasificacionVentasAgo.FieldName = "v_ago"
        Me.grcClasificacionVentasAgo.Name = "grcClasificacionVentasAgo"
        Me.grcClasificacionVentasAgo.Width = 20
        '
        'grcClasificacionVentasSep
        '
        Me.grcClasificacionVentasSep.Caption = "SEP"
        Me.grcClasificacionVentasSep.FieldName = "v_sep"
        Me.grcClasificacionVentasSep.Name = "grcClasificacionVentasSep"
        Me.grcClasificacionVentasSep.Width = 20
        '
        'grcClasificacionVentasOct
        '
        Me.grcClasificacionVentasOct.Caption = "OCT"
        Me.grcClasificacionVentasOct.FieldName = "v_oct"
        Me.grcClasificacionVentasOct.Name = "grcClasificacionVentasOct"
        Me.grcClasificacionVentasOct.Width = 20
        '
        'grcClasificacionVentasNov
        '
        Me.grcClasificacionVentasNov.Caption = "NOV"
        Me.grcClasificacionVentasNov.FieldName = "v_nov"
        Me.grcClasificacionVentasNov.Name = "grcClasificacionVentasNov"
        Me.grcClasificacionVentasNov.Width = 20
        '
        'grcClasificacionVentasDic
        '
        Me.grcClasificacionVentasDic.Caption = "DIC "
        Me.grcClasificacionVentasDic.FieldName = "v_dic"
        Me.grcClasificacionVentasDic.Name = "grcClasificacionVentasDic"
        Me.grcClasificacionVentasDic.Width = 20
        '
        'grcClasificacionVentasTotal
        '
        Me.grcClasificacionVentasTotal.Caption = "TOTAL"
        Me.grcClasificacionVentasTotal.FieldName = "ventas"
        Me.grcClasificacionVentasTotal.Name = "grcClasificacionVentasTotal"
        Me.grcClasificacionVentasTotal.Visible = True
        Me.grcClasificacionVentasTotal.Width = 20
        '
        'GridBand17
        '
        Me.GridBand17.Caption = "Salidas"
        Me.GridBand17.Columns.Add(Me.grcClasificacionSalidasDevPro)
        Me.GridBand17.Columns.Add(Me.grcClasificacionSalidasMovEsp)
        Me.GridBand17.Columns.Add(Me.grcClasificacionSalidasAjustes)
        Me.GridBand17.Columns.Add(Me.grcClasificacionSalidasTotal)
        Me.GridBand17.HeaderStyleName = "Style1"
        Me.GridBand17.Name = "GridBand17"
        Me.GridBand17.Width = 20
        '
        'grcClasificacionSalidasDevPro
        '
        Me.grcClasificacionSalidasDevPro.Caption = "Dev Pro"
        Me.grcClasificacionSalidasDevPro.FieldName = "s_dev_prov"
        Me.grcClasificacionSalidasDevPro.Name = "grcClasificacionSalidasDevPro"
        Me.grcClasificacionSalidasDevPro.SortIndex = 0
        Me.grcClasificacionSalidasDevPro.SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        Me.grcClasificacionSalidasDevPro.Width = 20
        '
        'grcClasificacionSalidasMovEsp
        '
        Me.grcClasificacionSalidasMovEsp.Caption = "Mov Esp"
        Me.grcClasificacionSalidasMovEsp.FieldName = "s_mov_esp"
        Me.grcClasificacionSalidasMovEsp.Name = "grcClasificacionSalidasMovEsp"
        Me.grcClasificacionSalidasMovEsp.Width = 20
        '
        'grcClasificacionSalidasAjustes
        '
        Me.grcClasificacionSalidasAjustes.Caption = "Ajustes"
        Me.grcClasificacionSalidasAjustes.FieldName = "s_ajustes"
        Me.grcClasificacionSalidasAjustes.Name = "grcClasificacionSalidasAjustes"
        Me.grcClasificacionSalidasAjustes.Width = 20
        '
        'grcClasificacionSalidasTotal
        '
        Me.grcClasificacionSalidasTotal.Caption = "TOTAL"
        Me.grcClasificacionSalidasTotal.FieldName = "salidas"
        Me.grcClasificacionSalidasTotal.Name = "grcClasificacionSalidasTotal"
        Me.grcClasificacionSalidasTotal.Visible = True
        Me.grcClasificacionSalidasTotal.Width = 20
        '
        'GridBand18
        '
        Me.GridBand18.Caption = "Existencias"
        Me.GridBand18.Columns.Add(Me.grcClasificacionExistencias)
        Me.GridBand18.Columns.Add(Me.grcClasificacionValor)
        Me.GridBand18.HeaderStyleName = "Style1"
        Me.GridBand18.Name = "GridBand18"
        Me.GridBand18.Width = 40
        '
        'grcClasificacionExistencias
        '
        Me.grcClasificacionExistencias.Caption = "Existencias"
        Me.grcClasificacionExistencias.FieldName = "exist"
        Me.grcClasificacionExistencias.Name = "grcClasificacionExistencias"
        Me.grcClasificacionExistencias.Visible = True
        Me.grcClasificacionExistencias.Width = 20
        '
        'grcClasificacionValor
        '
        Me.grcClasificacionValor.Caption = "Valor"
        Me.grcClasificacionValor.DisplayFormat.FormatString = "c2"
        Me.grcClasificacionValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcClasificacionValor.FieldName = "valor"
        Me.grcClasificacionValor.Name = "grcClasificacionValor"
        Me.grcClasificacionValor.Visible = True
        Me.grcClasificacionValor.Width = 20
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(888, 80)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(88, 32)
        Me.btnExportar.TabIndex = 7
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.ToolTip = "Exporta la informacion mostrada en Excel"
        '
        'btnVerCompras
        '
        Me.btnVerCompras.Location = New System.Drawing.Point(536, 80)
        Me.btnVerCompras.Name = "btnVerCompras"
        Me.btnVerCompras.Size = New System.Drawing.Size(88, 32)
        Me.btnVerCompras.TabIndex = 7
        Me.btnVerCompras.Text = "Ver Compras"
        Me.btnVerCompras.ToolTip = "Muestra u Oculta Detalle de las Compras"
        '
        'btnVerVentas
        '
        Me.btnVerVentas.Location = New System.Drawing.Point(712, 80)
        Me.btnVerVentas.Name = "btnVerVentas"
        Me.btnVerVentas.Size = New System.Drawing.Size(88, 32)
        Me.btnVerVentas.TabIndex = 7
        Me.btnVerVentas.Text = "Ver Ventas"
        Me.btnVerVentas.ToolTip = "Muestra u Oculta Detalle de las Ventas"
        '
        'btnVerEntradas
        '
        Me.btnVerEntradas.Location = New System.Drawing.Point(624, 80)
        Me.btnVerEntradas.Name = "btnVerEntradas"
        Me.btnVerEntradas.Size = New System.Drawing.Size(88, 32)
        Me.btnVerEntradas.TabIndex = 7
        Me.btnVerEntradas.Text = "Ver Entradas"
        Me.btnVerEntradas.ToolTip = "Muestra u Oculta Detalle de las Entradas"
        '
        'btnVerSalidas
        '
        Me.btnVerSalidas.Location = New System.Drawing.Point(800, 80)
        Me.btnVerSalidas.Name = "btnVerSalidas"
        Me.btnVerSalidas.Size = New System.Drawing.Size(88, 32)
        Me.btnVerSalidas.TabIndex = 7
        Me.btnVerSalidas.Text = "Ver Salidas"
        Me.btnVerSalidas.ToolTip = "Muestra u Oculta Detalle de las Salidas"
        '
        'progressBarControl1
        '
        Me.progressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progressBarControl1.Location = New System.Drawing.Point(8, 112)
        Me.progressBarControl1.Name = "progressBarControl1"
        Me.progressBarControl1.Size = New System.Drawing.Size(978, 16)
        Me.progressBarControl1.TabIndex = 59
        Me.progressBarControl1.TabStop = False
        '
        'chkSoloExistencias
        '
        Me.chkSoloExistencias.Location = New System.Drawing.Point(347, 88)
        Me.chkSoloExistencias.Name = "chkSoloExistencias"
        '
        'chkSoloExistencias.Properties
        '
        Me.chkSoloExistencias.Properties.Caption = "Solo con Existencias"
        Me.chkSoloExistencias.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSoloExistencias.Size = New System.Drawing.Size(144, 20)
        Me.chkSoloExistencias.TabIndex = 60
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "n0"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.EditFormat.FormatString = "n0"
        Me.RepositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'frmReporteEstadistico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(996, 610)
        Me.Controls.Add(Me.chkSoloExistencias)
        Me.Controls.Add(Me.progressBarControl1)
        Me.Controls.Add(Me.btnExportar)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnVerCompras)
        Me.Controls.Add(Me.btnVerVentas)
        Me.Controls.Add(Me.btnVerEntradas)
        Me.Controls.Add(Me.btnVerSalidas)
        Me.Name = "frmReporteEstadistico"
        Me.Text = "frmReporteEstadistico"
        Me.Controls.SetChildIndex(Me.btnVerSalidas, 0)
        Me.Controls.SetChildIndex(Me.btnVerEntradas, 0)
        Me.Controls.SetChildIndex(Me.btnVerVentas, 0)
        Me.Controls.SetChildIndex(Me.btnVerCompras, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.btnExportar, 0)
        Me.Controls.SetChildIndex(Me.progressBarControl1, 0)
        Me.Controls.SetChildIndex(Me.chkSoloExistencias, 0)
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BrvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grGrupos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BrvGrupos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.grClasificacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BrvClasificacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.progressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSoloExistencias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(lkpProveedor)
        End Get
    End Property

#Region "Eventos de la Forma"
    Private Sub frmReporteEstadistico_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = Visualizar()
    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        lkpGrupo_LoadData(True)
    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGrupos.MultiLookup(Me.lkpDepartamento.ToXML)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub chkSoloExistencias_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSoloExistencias.CheckedChanged
        Dim response As Events

        response = Visualizar()
        If response.ErrorFound Then
            response.Message = "Error al Desplegar la informaci�n"
        End If
    End Sub



    Private Sub btnVerCompras_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerCompras.Click
        MostrarOcultarColumnas(TipoColumna.Compras)
    End Sub
    Private Sub btnVerVentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerVentas.Click
        MostrarOcultarColumnas(TipoColumna.Ventas)
    End Sub
    Private Sub btnVerEntradas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerEntradas.Click
        MostrarOcultarColumnas(TipoColumna.Entradas)
    End Sub
    Private Sub btnVerSalidas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerSalidas.Click
        MostrarOcultarColumnas(TipoColumna.Salidas)
    End Sub
    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        If ShowMessage(MessageType.MsgQuestion, "Deseas Exportar los Articulos", , , False) = Answer.MsgYes Then
            ExportarArticulos()
        End If

        If ShowMessage(MessageType.MsgQuestion, "Deseas Exportar los Grupos", , , False) = Answer.MsgYes Then
            ExportarGrupos()
        End If

        If ShowMessage(MessageType.MsgQuestion, "Deseas Exportar las Clasificaciones", , , False) = Answer.MsgYes Then
            ExportarClasificacion()
        End If


    End Sub
#End Region

#Region "Funcionalidad"



#Region "EXPORTACIONES"

    Private Sub ExportarArticulos()
        Dim fileName As String = ShowSaveFileDialog("Microsoft Excel Document", "Microsoft Excel|*.xls")
        If fileName <> "" Then
            ExportTo(New ExportXlsProvider(fileName), Me.BrvArticulos)
            OpenFile(fileName)
        End If
    End Sub
    Private Sub ExportarGrupos()
        Dim fileName As String = ShowSaveFileDialog("Microsoft Excel Document", "Microsoft Excel|*.xls")
        If fileName <> "" Then
            ExportTo(New ExportXlsProvider(fileName), Me.BrvGrupos)
            OpenFile(fileName)
        End If
    End Sub
    Private Sub ExportarClasificacion()
        Dim fileName As String = ShowSaveFileDialog("Microsoft Excel Document", "Microsoft Excel|*.xls")
        If fileName <> "" Then
            ExportTo(New ExportXlsProvider(fileName), Me.BrvClasificacion)
            OpenFile(fileName)
        End If
    End Sub


    Private Sub ExportTo(ByVal provider As IExportProvider, ByVal vista As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView)
        Dim currentCursor As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        Dim link As BaseExportLink = vista.CreateExportLink(provider)
        CType(link, GridViewExportLink).ExpandAll = False
        AddHandler link.Progress, New DevExpress.XtraGrid.Export.ProgressEventHandler(AddressOf Export_Progerss)
        link.ExportTo(True)

        Cursor.Current = currentCursor
    End Sub
    Private Sub OpenFile(ByVal fileName As String)
        If MessageBox.Show("Deseas Abrir el Archivo?", "Exportar A...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Try
                Dim process As New System.Diagnostics.Process
                process.StartInfo.FileName = fileName
                process.StartInfo.Verb = "Open"
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
                process.Start()
            Catch
                MessageBox.Show(Me, "Cannot find an application on your system suitable for openning the file with exported data.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
        progressBarControl1.Position = 0
    End Sub
    Private Function ShowSaveFileDialog(ByVal title As String, ByVal filter As String) As String
        Dim dlg As New SaveFileDialog
        Dim name As String = Application.ProductName
        Dim n As Integer = name.LastIndexOf(".") + 1
        If n > 0 Then name = name.Substring(n, name.Length - n)
        dlg.Title = "Export To " + title
        dlg.FileName = name
        dlg.Filter = filter
        If dlg.ShowDialog() = DialogResult.OK Then Return dlg.FileName
        Return ""
    End Function
    Private Sub Export_Progerss(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Export.ProgressEventArgs)
        If e.Phase = DevExpress.XtraGrid.Export.ExportPhase.Link Then
            progressBarControl1.Position = e.Position
            Me.Update()
        End If
    End Sub
#End Region

#Region "MOSTRAR OCULTAR COLUMNAS"


    Private Sub MostrarOcultarColumnas(ByVal Tipo As TipoColumna)
        Select Case Tipo
            Case TipoColumna.Compras
                MostrarOcultarColumnasComprasArticulos()
                MostrarOcultarColumnasComprasGrupos()
                MostrarOcultarColumnasComprasClasificacion()
            Case TipoColumna.Ventas
                MostrarOcultarColumnasVentasArticulos()
                MostrarOcultarColumnasVentasGrupos()
                MostrarOcultarColumnasVentasClasificacion()
            Case TipoColumna.Entradas
                MostrarOcultarColumnasEntradasArticulos()
                MostrarOcultarColumnasEntradasGrupos()
                MostrarOcultarColumnasEntradasClasificacion()

            Case TipoColumna.Salidas
                MostrarOcultarColumnasSalidasArticulos()
                MostrarOcultarColumnasSalidasGrupos()
                MostrarOcultarColumnasSalidasClasificacion()

        End Select
    End Sub

    Private Sub MostrarOcultarColumnasComprasArticulos()
        Me.grcComprasEne.Visible = Not Me.grcComprasEne.Visible
        Me.grcComprasFeb.Visible = Not Me.grcComprasFeb.Visible
        Me.grcComprasMar.Visible = Not Me.grcComprasMar.Visible
        Me.grcComprasAbr.Visible = Not Me.grcComprasAbr.Visible
        Me.grcComprasMay.Visible = Not Me.grcComprasMay.Visible
        Me.grcComprasJun.Visible = Not Me.grcComprasJun.Visible
        Me.grcComprasJul.Visible = Not Me.grcComprasJul.Visible
        Me.grcComprasAgo.Visible = Not Me.grcComprasAgo.Visible
        Me.grcComprasSep.Visible = Not Me.grcComprasSep.Visible
        Me.grcComprasOct.Visible = Not Me.grcComprasOct.Visible
        Me.grcComprasNov.Visible = Not Me.grcComprasNov.Visible
        Me.grcComprasDic.Visible = Not Me.grcComprasDic.Visible
    End Sub
    Private Sub MostrarOcultarColumnasComprasGrupos()
        Me.grcGrupoComprasEne.Visible = Not Me.grcGrupoComprasEne.Visible
        Me.grcGrupoComprasFeb.Visible = Not Me.grcGrupoComprasFeb.Visible
        Me.grcGrupoComprasMar.Visible = Not Me.grcGrupoComprasMar.Visible
        Me.grcGrupoComprasAbr.Visible = Not Me.grcGrupoComprasAbr.Visible
        Me.grcGrupoComprasMay.Visible = Not Me.grcGrupoComprasMay.Visible
        Me.grcGrupoComprasJun.Visible = Not Me.grcGrupoComprasJun.Visible
        Me.grcGrupoComprasJul.Visible = Not Me.grcGrupoComprasJul.Visible
        Me.grcGrupoComprasAgo.Visible = Not Me.grcGrupoComprasAgo.Visible
        Me.grcGrupoComprasSep.Visible = Not Me.grcGrupoComprasSep.Visible
        Me.grcGrupoComprasOct.Visible = Not Me.grcGrupoComprasOct.Visible
        Me.grcGrupoComprasNov.Visible = Not Me.grcGrupoComprasNov.Visible
        Me.grcGrupoComprasDic.Visible = Not Me.grcGrupoComprasDic.Visible
    End Sub
    Private Sub MostrarOcultarColumnasComprasClasificacion()
        Me.grcClasificacionComprasEne.Visible = Not Me.grcClasificacionComprasEne.Visible
        Me.grcClasificacionComprasFeb.Visible = Not Me.grcClasificacionComprasFeb.Visible
        Me.grcClasificacionComprasMar.Visible = Not Me.grcClasificacionComprasMar.Visible
        Me.grcClasificacionComprasAbr.Visible = Not Me.grcClasificacionComprasAbr.Visible
        Me.grcClasificacionComprasMay.Visible = Not Me.grcClasificacionComprasMay.Visible
        Me.grcClasificacionComprasJun.Visible = Not Me.grcClasificacionComprasJun.Visible
        Me.grcClasificacionComprasJul.Visible = Not Me.grcClasificacionComprasJul.Visible
        Me.grcClasificacionComprasAgo.Visible = Not Me.grcClasificacionComprasAgo.Visible
        Me.grcClasificacionComprasSep.Visible = Not Me.grcClasificacionComprasSep.Visible
        Me.grcClasificacionComprasOct.Visible = Not Me.grcClasificacionComprasOct.Visible
        Me.grcClasificacionComprasNov.Visible = Not Me.grcClasificacionComprasNov.Visible
        Me.grcClasificacionComprasDic.Visible = Not Me.grcClasificacionComprasDic.Visible
    End Sub

    Private Sub MostrarOcultarColumnasVentasArticulos()
        Me.grcVentasEne.Visible = Not Me.grcVentasEne.Visible
        Me.grcVentasFeb.Visible = Not Me.grcVentasFeb.Visible
        Me.grcVentasMar.Visible = Not Me.grcVentasMar.Visible
        Me.grcVentasAbr.Visible = Not Me.grcVentasAbr.Visible
        Me.grcVentasMay.Visible = Not Me.grcVentasMay.Visible
        Me.grcVentasJun.Visible = Not Me.grcVentasJun.Visible
        Me.grcVentasJul.Visible = Not Me.grcVentasJul.Visible
        Me.grcVentasAgo.Visible = Not Me.grcVentasAgo.Visible
        Me.grcVentasSep.Visible = Not Me.grcVentasSep.Visible
        Me.grcVentasOct.Visible = Not Me.grcVentasOct.Visible
        Me.grcVentasNov.Visible = Not Me.grcVentasNov.Visible
        Me.grcVentasDic.Visible = Not Me.grcVentasDic.Visible
    End Sub
    Private Sub MostrarOcultarColumnasVentasGrupos()
        Me.grcGrupoVentasEne.Visible = Not Me.grcGrupoVentasEne.Visible
        Me.grcGrupoVentasFeb.Visible = Not Me.grcGrupoVentasFeb.Visible
        Me.grcGrupoVentasMar.Visible = Not Me.grcGrupoVentasMar.Visible
        Me.grcGrupoVentasAbr.Visible = Not Me.grcGrupoVentasAbr.Visible
        Me.grcGrupoVentasMay.Visible = Not Me.grcGrupoVentasMay.Visible
        Me.grcGrupoVentasJun.Visible = Not Me.grcGrupoVentasJun.Visible
        Me.grcGrupoVentasJul.Visible = Not Me.grcGrupoVentasJul.Visible
        Me.grcGrupoVentasAgo.Visible = Not Me.grcGrupoVentasAgo.Visible
        Me.grcGrupoVentasSep.Visible = Not Me.grcGrupoVentasSep.Visible
        Me.grcGrupoVentasOct.Visible = Not Me.grcGrupoVentasOct.Visible
        Me.grcGrupoVentasNov.Visible = Not Me.grcGrupoVentasNov.Visible
        Me.grcGrupoVentasDic.Visible = Not Me.grcGrupoVentasDic.Visible
    End Sub
    Private Sub MostrarOcultarColumnasVentasClasificacion()
        Me.grcClasificacionVentasEne.Visible = Not Me.grcClasificacionVentasEne.Visible
        Me.grcClasificacionVentasFeb.Visible = Not Me.grcClasificacionVentasFeb.Visible
        Me.grcClasificacionVentasMar.Visible = Not Me.grcClasificacionVentasMar.Visible
        Me.grcClasificacionVentasAbr.Visible = Not Me.grcClasificacionVentasAbr.Visible
        Me.grcClasificacionVentasMay.Visible = Not Me.grcClasificacionVentasMay.Visible
        Me.grcClasificacionVentasJun.Visible = Not Me.grcClasificacionVentasJun.Visible
        Me.grcClasificacionVentasJul.Visible = Not Me.grcClasificacionVentasJul.Visible
        Me.grcClasificacionVentasAgo.Visible = Not Me.grcClasificacionVentasAgo.Visible
        Me.grcClasificacionVentasSep.Visible = Not Me.grcClasificacionVentasSep.Visible
        Me.grcClasificacionVentasOct.Visible = Not Me.grcClasificacionVentasOct.Visible
        Me.grcClasificacionVentasNov.Visible = Not Me.grcClasificacionVentasNov.Visible
        Me.grcClasificacionVentasDic.Visible = Not Me.grcClasificacionVentasDic.Visible
    End Sub

    Private Sub MostrarOcultarColumnasEntradasArticulos()
        grcEntradasDevCTE.Visible = Not grcEntradasDevCTE.Visible
        grcEntradasMovEsp.Visible = Not grcEntradasMovEsp.Visible
        grcEntradasAjustes.Visible = Not grcEntradasAjustes.Visible
    End Sub
    Private Sub MostrarOcultarColumnasEntradasGrupos()
        grcGrupoEntradasDevCte.Visible = Not grcGrupoEntradasDevCte.Visible
        grcGrupoEntradasMovEsp.Visible = Not grcGrupoEntradasMovEsp.Visible
        grcGrupoEntradasAjustes.Visible = Not grcGrupoEntradasAjustes.Visible
    End Sub
    Private Sub MostrarOcultarColumnasEntradasClasificacion()
        grcClasificacionEntradasDevCte.Visible = Not grcClasificacionEntradasDevCte.Visible
        grcClasificacionEntradasMovEsp.Visible = Not grcClasificacionEntradasMovEsp.Visible
        grcClasificacionEntradasAjustes.Visible = Not grcClasificacionEntradasAjustes.Visible
    End Sub

    Private Sub MostrarOcultarColumnasSalidasArticulos()
        grcSalidasDevPro.Visible = Not grcSalidasDevPro.Visible
        grcSalidasMovEsp.Visible = Not grcSalidasMovEsp.Visible
        grcSalidasAjustes.Visible = Not grcSalidasAjustes.Visible

    End Sub
    Private Sub MostrarOcultarColumnasSalidasGrupos()
        grcGrupoSalidasDevPro.Visible = Not grcGrupoSalidasDevPro.Visible
        grcGrupoSalidasMovEsp.Visible = Not grcGrupoSalidasMovEsp.Visible
        grcGrupoSalidasAjustes.Visible = Not grcGrupoSalidasAjustes.Visible

    End Sub
    Private Sub MostrarOcultarColumnasSalidasClasificacion()
        grcClasificacionSalidasDevPro.Visible = Not grcClasificacionSalidasDevPro.Visible
        grcClasificacionSalidasMovEsp.Visible = Not grcClasificacionSalidasMovEsp.Visible
        grcClasificacionSalidasAjustes.Visible = Not grcClasificacionSalidasAjustes.Visible

    End Sub

#End Region


    Private Function Visualizar() As Events
        Dim Response As New Events
        Dim currentCursor As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        Response = oReportes.ReporteEstadistico(Departamento, Grupo, Proveedor, Me.chkSoloExistencias.Checked)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.grArticulos.DataSource = oDataSet.Tables(0)
            Me.grGrupos.DataSource = oDataSet.Tables(1)
            Me.grClasificacion.DataSource = oDataSet.Tables(2)
            oDataSet = Nothing
        End If

        Cursor.Current = currentCursor

        Return Response
    End Function


#End Region
End Class
