Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmVentasDiarias
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents spnA�o As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents cboMes As DevExpress.XtraEditors.ImageComboBoxEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasDiarias))
        Me.spnA�o = New DevExpress.XtraEditors.SpinEdit
        Me.cboMes = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.spnA�o.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(281, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'spnA�o
        '
        Me.spnA�o.EditValue = New Decimal(New Integer() {1980, 0, 0, 0})
        Me.spnA�o.Location = New System.Drawing.Point(216, 46)
        Me.spnA�o.Name = "spnA�o"
        '
        'spnA�o.Properties
        '
        Me.spnA�o.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spnA�o.Properties.MaxValue = New Decimal(New Integer() {2050, 0, 0, 0})
        Me.spnA�o.Properties.MinValue = New Decimal(New Integer() {1980, 0, 0, 0})
        Me.spnA�o.Properties.UseCtrlIncrement = False
        Me.spnA�o.Size = New System.Drawing.Size(75, 20)
        Me.spnA�o.TabIndex = 60
        '
        'cboMes
        '
        Me.cboMes.EditValue = 1
        Me.cboMes.Location = New System.Drawing.Point(61, 46)
        Me.cboMes.Name = "cboMes"
        '
        'cboMes.Properties
        '
        Me.cboMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboMes.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enero", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Febrero", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Marzo", 3, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Abril", 4, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mayo", 5, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Junio", 6, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Julio", 7, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Agosto", 8, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Septiembre", 9, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Octubre", 10, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Noviembre", 11, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diciembre", 12, -1)})
        Me.cboMes.Size = New System.Drawing.Size(88, 20)
        Me.cboMes.TabIndex = 61
        '
        'chkDesglosado
        '
        Me.chkDesglosado.EditValue = True
        Me.chkDesglosado.Location = New System.Drawing.Point(61, 79)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Desglosado"
        Me.chkDesglosado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(96, 22)
        Me.chkDesglosado.TabIndex = 62
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 16)
        Me.Label2.TabIndex = 63
        Me.Label2.Text = "Mes:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(184, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 16)
        Me.Label3.TabIndex = 64
        Me.Label3.Text = "A�o:"
        '
        'frmVentasDiarias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(322, 112)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.cboMes)
        Me.Controls.Add(Me.spnA�o)
        Me.Name = "frmVentasDiarias"
        Me.Text = "frmVentasDiarias"
        Me.Controls.SetChildIndex(Me.spnA�o, 0)
        Me.Controls.SetChildIndex(Me.cboMes, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.spnA�o.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
#End Region


    Private Sub frmVentasDiarias_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim titulo As String = "Ventas Diarias del Mes: "
        titulo = titulo + Me.cboMes.Text + " del " + Me.spnA�o.Value.ToString
        Response = oReportes.VentasDiarias(Me.cboMes.Value.ToString, Me.spnA�o.Value.ToString, Me.chkDesglosado.Checked)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                If Me.chkDesglosado.Checked Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptVentasDiariasDesglosado
                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, titulo, oReport)
                    oDataSet = Nothing
                    oReport = Nothing
                Else
                    Dim oDataSet As DataSet
                    Dim oReport As New rptVentasDiarias
                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, titulo, oReport)
                    oDataSet = Nothing
                    oReport = Nothing
                End If
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub

    Private Sub frmVentasDiarias_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.cboMes.SelectedIndex = CDate(TINApp.FechaServidor).Month - 1
        Me.spnA�o.Value = CDate(TINApp.FechaServidor).Year
    End Sub

End Class
