Imports Dipros.Utils.Common

Public Class frmDescuentos
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblDescuento As System.Windows.Forms.Label
		Friend WithEvents clcDescuento As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
		Friend WithEvents clcPorcentaje As Dipros.Editors.TINCalcEdit 
    Friend WithEvents txtNombre As DevExpress.XtraEditors.MemoEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDescuentos))
        Me.lblDescuento = New System.Windows.Forms.Label
        Me.clcDescuento = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.lblPorcentaje = New System.Windows.Forms.Label
        Me.clcPorcentaje = New Dipros.Editors.TINCalcEdit
        Me.txtNombre = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(600, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(11, 40)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(67, 16)
        Me.lblDescuento.TabIndex = 0
        Me.lblDescuento.Tag = ""
        Me.lblDescuento.Text = "&Descuento:"
        Me.lblDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDescuento
        '
        Me.clcDescuento.EditValue = "0"
        Me.clcDescuento.Location = New System.Drawing.Point(86, 39)
        Me.clcDescuento.MaxValue = 0
        Me.clcDescuento.MinValue = 0
        Me.clcDescuento.Name = "clcDescuento"
        '
        'clcDescuento.Properties
        '
        Me.clcDescuento.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcDescuento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDescuento.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcDescuento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDescuento.Properties.Enabled = False
        Me.clcDescuento.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcDescuento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDescuento.Size = New System.Drawing.Size(54, 19)
        Me.clcDescuento.TabIndex = 1
        Me.clcDescuento.Tag = "descuento"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(25, 64)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(10, 136)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(68, 16)
        Me.lblPorcentaje.TabIndex = 4
        Me.lblPorcentaje.Tag = ""
        Me.lblPorcentaje.Text = "&Porcentaje:"
        Me.lblPorcentaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentaje
        '
        Me.clcPorcentaje.EditValue = "0"
        Me.clcPorcentaje.Location = New System.Drawing.Point(86, 135)
        Me.clcPorcentaje.MaxValue = 0
        Me.clcPorcentaje.MinValue = 0
        Me.clcPorcentaje.Name = "clcPorcentaje"
        '
        'clcPorcentaje.Properties
        '
        Me.clcPorcentaje.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.MaskData.EditMask = "########0.00"
        Me.clcPorcentaje.Properties.Precision = 18
        Me.clcPorcentaje.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPorcentaje.Size = New System.Drawing.Size(54, 19)
        Me.clcPorcentaje.TabIndex = 5
        Me.clcPorcentaje.Tag = "porcentaje"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(86, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNombre.Size = New System.Drawing.Size(290, 64)
        Me.txtNombre.TabIndex = 2
        Me.txtNombre.Tag = "nombre"
        '
        'frmDescuentos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 168)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblDescuento)
        Me.Controls.Add(Me.clcDescuento)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.lblPorcentaje)
        Me.Controls.Add(Me.clcPorcentaje)
        Me.Name = "frmDescuentos"
        Me.Controls.SetChildIndex(Me.clcPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.lblPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcDescuento, 0)
        Me.Controls.SetChildIndex(Me.lblDescuento, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        CType(Me.clcDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

    '#Region "DIPROS Systems, Declaraciones"
    '    Private oDescuentos As VillarrealBusiness.clsDescuentos
    '#End Region

    '#Region "DIPROS Systems, Eventos de la Forma"
    '	Private Sub frmDescuentos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
    '		Select Case Action
    '		Case Actions.Insert
    '						Response = oDescuentos.Insertar(Me.DataSource)

    '		Case Actions.Update
    '						Response = oDescuentos.Actualizar(Me.DataSource)

    '		Case Actions.Delete
    '						Response = oDescuentos.Eliminar(clcDescuento.value)

    '		End Select
    '	End Sub

    '	Private Sub frmDescuentos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
    '		Response = oDescuentos.DespliegaDatos(OwnerForm.Value("descuento"))
    '		If Not Response.ErrorFound Then
    '			Dim oDataSet As DataSet
    '			oDataSet = Response.Value
    '			Me.DataSource = oDataSet
    '		End If

    '	End Sub

    '	Private Sub frmDescuentos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
    '		oDescuentos = New VillarrealBusiness.clsDescuentos

    '		Select Case Action
    '			Case Actions.Insert
    '			Case Actions.Update
    '			Case Actions.Delete
    '		End Select
    '	End Sub

    '	Private Sub frmDescuentos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
    '		Response = oDescuentos.Validacion(Action, txtNombre.Text, clcPorcentaje.value)
    '	End Sub

    '	Private Sub frmDescuentos_Localize() Handles MyBase.Localize
    '		Find("Unknow", CType("Replace by a control", Object))

    '	End Sub

    '#End Region

    '#Region "DIPROS Systems, Eventos de Controles"
    '    Private Sub txtNombre_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '        Dim oEvent As Dipros.Utils.Events
    '        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '        oEvent = oDescuentos.ValidaNombre(txtNombre.Text)
    '        If oEvent.ErrorFound Then
    '            oEvent.ShowError()
    '            e.Cancel = True
    '        End If
    '    End Sub
    '    Private Sub clcPorcentaje_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcPorcentaje.Validating
    '        Dim oEvent As Dipros.Utils.Events
    '        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '        oEvent = oDescuentos.ValidaPorcentaje(clcPorcentaje.Value)
    '        If oEvent.ErrorFound Then
    '            oEvent.ShowError()
    '            e.Cancel = True
    '        End If
    '    End Sub

    '#End Region

    '#Region "DIPROS Systems, Funcionalidad"
    '#End Region

End Class
