Imports Dipros.Utils.Common
Imports Dipros.Utils


Public Class frmPlanesCredito
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblPlan_Credito As System.Windows.Forms.Label
		Friend WithEvents clcPlan_Credito As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblPlazo As System.Windows.Forms.Label
		Friend WithEvents clcPlazo As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblInteres As System.Windows.Forms.Label
		Friend WithEvents clcInteres As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblTipo_Venta As System.Windows.Forms.Label
		Friend WithEvents cboTipo_Venta As DevExpress.XtraEditors.ImageComboBoxEdit
		Friend WithEvents lblTipo_Plazo As System.Windows.Forms.Label
		Friend WithEvents cboTipo_Plazo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboPrecios As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkPlanDependencia As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblEnganche As System.Windows.Forms.Label
    Friend WithEvents clcEnganche As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkUrgente As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkplan_fonacot As DevExpress.XtraEditors.CheckEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPlanesCredito))
        Me.lblPlan_Credito = New System.Windows.Forms.Label
        Me.clcPlan_Credito = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblPlazo = New System.Windows.Forms.Label
        Me.clcPlazo = New Dipros.Editors.TINCalcEdit
        Me.lblInteres = New System.Windows.Forms.Label
        Me.clcInteres = New Dipros.Editors.TINCalcEdit
        Me.lblTipo_Venta = New System.Windows.Forms.Label
        Me.cboTipo_Venta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblTipo_Plazo = New System.Windows.Forms.Label
        Me.cboTipo_Plazo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboPrecios = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkPlanDependencia = New DevExpress.XtraEditors.CheckEdit
        Me.lblEnganche = New System.Windows.Forms.Label
        Me.clcEnganche = New Dipros.Editors.TINCalcEdit
        Me.chkUrgente = New DevExpress.XtraEditors.CheckEdit
        Me.chkplan_fonacot = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcPlan_Credito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcInteres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Venta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Plazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPlanDependencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEnganche.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUrgente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkplan_fonacot.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(286, 28)
        '
        'lblPlan_Credito
        '
        Me.lblPlan_Credito.AutoSize = True
        Me.lblPlan_Credito.Location = New System.Drawing.Point(13, 40)
        Me.lblPlan_Credito.Name = "lblPlan_Credito"
        Me.lblPlan_Credito.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan_Credito.TabIndex = 0
        Me.lblPlan_Credito.Tag = ""
        Me.lblPlan_Credito.Text = "P&lan de crédito:"
        Me.lblPlan_Credito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlan_Credito
        '
        Me.clcPlan_Credito.EditValue = "0"
        Me.clcPlan_Credito.Location = New System.Drawing.Point(112, 40)
        Me.clcPlan_Credito.MaxValue = 0
        Me.clcPlan_Credito.MinValue = 0
        Me.clcPlan_Credito.Name = "clcPlan_Credito"
        '
        'clcPlan_Credito.Properties
        '
        Me.clcPlan_Credito.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPlan_Credito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlan_Credito.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPlan_Credito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlan_Credito.Properties.Enabled = False
        Me.clcPlan_Credito.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlan_Credito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlan_Credito.Size = New System.Drawing.Size(54, 19)
        Me.clcPlan_Credito.TabIndex = 1
        Me.clcPlan_Credito.Tag = "plan_credito"
        Me.clcPlan_Credito.ToolTip = "plan de crédito"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(32, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(112, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(300, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripción"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(66, 86)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(38, 16)
        Me.lblPlazo.TabIndex = 4
        Me.lblPlazo.Tag = ""
        Me.lblPlazo.Text = "Pla&zo:"
        Me.lblPlazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo
        '
        Me.clcPlazo.EditValue = "0"
        Me.clcPlazo.Location = New System.Drawing.Point(112, 85)
        Me.clcPlazo.MaxValue = 0
        Me.clcPlazo.MinValue = 0
        Me.clcPlazo.Name = "clcPlazo"
        '
        'clcPlazo.Properties
        '
        Me.clcPlazo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPlazo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.EditFormat.FormatString = "########0"
        Me.clcPlazo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.MaskData.EditMask = "########0"
        Me.clcPlazo.Properties.MaxLength = 2
        Me.clcPlazo.Properties.Precision = 18
        Me.clcPlazo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo.Size = New System.Drawing.Size(54, 19)
        Me.clcPlazo.TabIndex = 5
        Me.clcPlazo.Tag = "plazo"
        Me.clcPlazo.ToolTip = "plazo"
        '
        'lblInteres
        '
        Me.lblInteres.AutoSize = True
        Me.lblInteres.Location = New System.Drawing.Point(56, 109)
        Me.lblInteres.Name = "lblInteres"
        Me.lblInteres.Size = New System.Drawing.Size(48, 16)
        Me.lblInteres.TabIndex = 6
        Me.lblInteres.Tag = ""
        Me.lblInteres.Text = "&Interés:"
        Me.lblInteres.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcInteres
        '
        Me.clcInteres.EditValue = "0"
        Me.clcInteres.Location = New System.Drawing.Point(112, 107)
        Me.clcInteres.MaxValue = 0
        Me.clcInteres.MinValue = 0
        Me.clcInteres.Name = "clcInteres"
        '
        'clcInteres.Properties
        '
        Me.clcInteres.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcInteres.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcInteres.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcInteres.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcInteres.Properties.MaskData.EditMask = "########0.00"
        Me.clcInteres.Properties.MaxLength = 4
        Me.clcInteres.Properties.Precision = 18
        Me.clcInteres.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcInteres.Size = New System.Drawing.Size(54, 19)
        Me.clcInteres.TabIndex = 7
        Me.clcInteres.Tag = "interes"
        Me.clcInteres.ToolTip = "interes"
        '
        'lblTipo_Venta
        '
        Me.lblTipo_Venta.AutoSize = True
        Me.lblTipo_Venta.Location = New System.Drawing.Point(20, 132)
        Me.lblTipo_Venta.Name = "lblTipo_Venta"
        Me.lblTipo_Venta.Size = New System.Drawing.Size(84, 16)
        Me.lblTipo_Venta.TabIndex = 8
        Me.lblTipo_Venta.Tag = ""
        Me.lblTipo_Venta.Text = "Tip&o de venta:"
        Me.lblTipo_Venta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Venta
        '
        Me.cboTipo_Venta.EditValue = 0
        Me.cboTipo_Venta.Location = New System.Drawing.Point(112, 129)
        Me.cboTipo_Venta.Name = "cboTipo_Venta"
        '
        'cboTipo_Venta.Properties
        '
        Me.cboTipo_Venta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Venta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Abonos", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normal", 1, -1)})
        Me.cboTipo_Venta.Size = New System.Drawing.Size(150, 20)
        Me.cboTipo_Venta.TabIndex = 9
        Me.cboTipo_Venta.Tag = ""
        Me.cboTipo_Venta.ToolTip = "tipo de venta"
        '
        'lblTipo_Plazo
        '
        Me.lblTipo_Plazo.AutoSize = True
        Me.lblTipo_Plazo.Location = New System.Drawing.Point(22, 155)
        Me.lblTipo_Plazo.Name = "lblTipo_Plazo"
        Me.lblTipo_Plazo.Size = New System.Drawing.Size(82, 16)
        Me.lblTipo_Plazo.TabIndex = 10
        Me.lblTipo_Plazo.Tag = ""
        Me.lblTipo_Plazo.Text = "&Tipo de plazo:"
        Me.lblTipo_Plazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Plazo
        '
        Me.cboTipo_Plazo.EditValue = 0
        Me.cboTipo_Plazo.Location = New System.Drawing.Point(112, 152)
        Me.cboTipo_Plazo.Name = "cboTipo_Plazo"
        '
        'cboTipo_Plazo.Properties
        '
        Me.cboTipo_Plazo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Plazo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Semanal", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Quincenal", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mensual", 2, -1)})
        Me.cboTipo_Plazo.Size = New System.Drawing.Size(150, 20)
        Me.cboTipo_Plazo.TabIndex = 11
        Me.cboTipo_Plazo.Tag = ""
        Me.cboTipo_Plazo.ToolTip = "tipo de plazo"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 16)
        Me.Label7.TabIndex = 12
        Me.Label7.Tag = ""
        Me.Label7.Text = "Precio de Ve&nta:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPrecios
        '
        Me.cboPrecios.Location = New System.Drawing.Point(112, 175)
        Me.cboPrecios.Name = "cboPrecios"
        '
        'cboPrecios.Properties
        '
        Me.cboPrecios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPrecios.Size = New System.Drawing.Size(150, 20)
        Me.cboPrecios.TabIndex = 13
        Me.cboPrecios.Tag = "precio_venta"
        Me.cboPrecios.ToolTip = "precio de venta"
        '
        'chkPlanDependencia
        '
        Me.chkPlanDependencia.Location = New System.Drawing.Point(112, 224)
        Me.chkPlanDependencia.Name = "chkPlanDependencia"
        '
        'chkPlanDependencia.Properties
        '
        Me.chkPlanDependencia.Properties.Caption = "Plan de D&ependencia"
        Me.chkPlanDependencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkPlanDependencia.Size = New System.Drawing.Size(152, 19)
        Me.chkPlanDependencia.TabIndex = 16
        Me.chkPlanDependencia.Tag = "plan_dependencia"
        Me.chkPlanDependencia.ToolTip = "plan de dependencia"
        '
        'lblEnganche
        '
        Me.lblEnganche.AutoSize = True
        Me.lblEnganche.Location = New System.Drawing.Point(41, 200)
        Me.lblEnganche.Name = "lblEnganche"
        Me.lblEnganche.Size = New System.Drawing.Size(63, 16)
        Me.lblEnganche.TabIndex = 14
        Me.lblEnganche.Tag = ""
        Me.lblEnganche.Text = "Enganc&he:"
        Me.lblEnganche.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEnganche
        '
        Me.clcEnganche.EditValue = "0"
        Me.clcEnganche.Location = New System.Drawing.Point(112, 199)
        Me.clcEnganche.MaxValue = 0
        Me.clcEnganche.MinValue = 0
        Me.clcEnganche.Name = "clcEnganche"
        '
        'clcEnganche.Properties
        '
        Me.clcEnganche.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcEnganche.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcEnganche.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche.Properties.MaskData.EditMask = "########0.00"
        Me.clcEnganche.Properties.MaxLength = 4
        Me.clcEnganche.Properties.Precision = 18
        Me.clcEnganche.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEnganche.Size = New System.Drawing.Size(54, 19)
        Me.clcEnganche.TabIndex = 15
        Me.clcEnganche.Tag = "enganche"
        Me.clcEnganche.ToolTip = "enganche"
        '
        'chkUrgente
        '
        Me.chkUrgente.Location = New System.Drawing.Point(348, 248)
        Me.chkUrgente.Name = "chkUrgente"
        '
        'chkUrgente.Properties
        '
        Me.chkUrgente.Properties.Caption = "Vi&gente"
        Me.chkUrgente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkUrgente.Size = New System.Drawing.Size(64, 19)
        Me.chkUrgente.TabIndex = 18
        Me.chkUrgente.Tag = "vigente"
        Me.chkUrgente.ToolTip = "vigente"
        '
        'chkplan_fonacot
        '
        Me.chkplan_fonacot.Location = New System.Drawing.Point(112, 248)
        Me.chkplan_fonacot.Name = "chkplan_fonacot"
        '
        'chkplan_fonacot.Properties
        '
        Me.chkplan_fonacot.Properties.Caption = "Plan de FONACOT"
        Me.chkplan_fonacot.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkplan_fonacot.Size = New System.Drawing.Size(152, 19)
        Me.chkplan_fonacot.TabIndex = 17
        Me.chkplan_fonacot.Tag = "fonacot"
        Me.chkplan_fonacot.ToolTip = "Plan FONACOT"
        '
        'frmPlanesCredito
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(426, 272)
        Me.Controls.Add(Me.chkplan_fonacot)
        Me.Controls.Add(Me.chkUrgente)
        Me.Controls.Add(Me.chkPlanDependencia)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cboPrecios)
        Me.Controls.Add(Me.lblPlan_Credito)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblPlazo)
        Me.Controls.Add(Me.lblInteres)
        Me.Controls.Add(Me.lblTipo_Venta)
        Me.Controls.Add(Me.lblTipo_Plazo)
        Me.Controls.Add(Me.clcPlan_Credito)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.clcPlazo)
        Me.Controls.Add(Me.clcInteres)
        Me.Controls.Add(Me.cboTipo_Venta)
        Me.Controls.Add(Me.cboTipo_Plazo)
        Me.Controls.Add(Me.lblEnganche)
        Me.Controls.Add(Me.clcEnganche)
        Me.Name = "frmPlanesCredito"
        Me.Controls.SetChildIndex(Me.clcEnganche, 0)
        Me.Controls.SetChildIndex(Me.lblEnganche, 0)
        Me.Controls.SetChildIndex(Me.cboTipo_Plazo, 0)
        Me.Controls.SetChildIndex(Me.cboTipo_Venta, 0)
        Me.Controls.SetChildIndex(Me.clcInteres, 0)
        Me.Controls.SetChildIndex(Me.clcPlazo, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcPlan_Credito, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Plazo, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Venta, 0)
        Me.Controls.SetChildIndex(Me.lblInteres, 0)
        Me.Controls.SetChildIndex(Me.lblPlazo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblPlan_Credito, 0)
        Me.Controls.SetChildIndex(Me.cboPrecios, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.chkPlanDependencia, 0)
        Me.Controls.SetChildIndex(Me.chkUrgente, 0)
        Me.Controls.SetChildIndex(Me.chkplan_fonacot, 0)
        CType(Me.clcPlan_Credito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcInteres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Venta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Plazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPlanDependencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEnganche.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUrgente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkplan_fonacot.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oPlanesCredito As VillarrealBusiness.clsPlanesCredito
    Private oPreciosVenta As VillarrealBusiness.clsPrecios

    Public Property Tipo_Venta() As Char
        Get
            Select Case Me.cboTipo_Venta.SelectedIndex
                Case 0
                    Return "A"
                Case 1
                    Return "N"
            End Select

        End Get
        Set(ByVal Value As Char)
            Select Case Value
                Case "A"
                    Me.cboTipo_Venta.SelectedIndex = 0
                Case "N"
                    Me.cboTipo_Venta.SelectedIndex = 1
            End Select
        End Set
    End Property

    Public Property Tipo_Plazo() As Char
        Get
            Select Case Me.cboTipo_Plazo.SelectedIndex
                Case 0
                    Return "S"
                Case 1
                    Return "Q"
                Case 2
                    Return "M"
            End Select

        End Get
        Set(ByVal Value As Char)
            Select Case Value
                Case "S"
                    Me.cboTipo_Plazo.SelectedIndex = 0
                Case "Q"
                    Me.cboTipo_Plazo.SelectedIndex = 1
                Case "M"
                    Me.cboTipo_Plazo.SelectedIndex = 2
            End Select
        End Set
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmPlanesCredito_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oPlanesCredito.Insertar(Me.DataSource, Tipo_Venta, Tipo_Plazo)

            Case Actions.Update
                Response = oPlanesCredito.Actualizar(Me.DataSource, Tipo_Venta, Tipo_Plazo)

            Case Actions.Delete
                Response = oPlanesCredito.Eliminar(clcPlan_Credito.Value)

        End Select
    End Sub

    Private Sub frmPlanesCredito_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oPlanesCredito.DespliegaDatos(OwnerForm.Value("plan_credito"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            Me.Tipo_Venta = oDataSet.Tables(0).Rows(0).Item("tipo_venta")
            Me.Tipo_Plazo = oDataSet.Tables(0).Rows(0).Item("tipo_plazo")
        End If

    End Sub

    Private Sub frmPlanesCredito_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oPlanesCredito = New VillarrealBusiness.clsPlanesCredito
        oPreciosVenta = New VillarrealBusiness.clsPrecios
        Me.cboTipo_Plazo.Value = 2

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select

        LLenarPrecios()
    End Sub

    Private Sub frmPlanesCredito_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oPlanesCredito.Validacion(Action, txtDescripcion.Text, clcPlazo.Value, clcInteres.Value)
    End Sub

    Private Sub frmPlanesCredito_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPlanesCredito.ValidaDescripcion(txtDescripcion.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub clcPlazo_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcPlazo.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPlanesCredito.ValidaPlazo(clcPlazo.Value)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub clcInteres_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcInteres.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPlanesCredito.ValidaInteres(clcInteres.Value)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub LLenarPrecios()
        Dim Response As New Events
        Response = oPreciosVenta.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            Dim item As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
            item.Description = "Precio Lista"
            item.Value = 0
            Me.cboPrecios.Properties.Items.Add(item)

            Dim i As Long
            For i = 1 To oDataSet.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = oDataSet.Tables(0).Rows(i - 1).Item("nombre")
                item_for.Value = oDataSet.Tables(0).Rows(i - 1).Item("precio")
                Me.cboPrecios.Properties.Items.Add(item_for)
            Next

            Me.cboPrecios.SelectedIndex = 0
            oDataSet = Nothing
        End If
    End Sub
#End Region


End Class
