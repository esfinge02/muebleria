Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports System.Windows.Forms

Public Class frmAbrirOrdenCompra
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpOrden As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblFechaPromesa As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAbrirOrdenCompra))
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpOrden = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblBodega = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblFechaPromesa = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(147, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(16, 48)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(116, 16)
        Me.lblOrden.TabIndex = 0
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "Orde&n de Compra:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpOrden
        '
        Me.lkpOrden.AutoReaload = False
        Me.lkpOrden.DataSource = Nothing
        Me.lkpOrden.DefaultSearchField = ""
        Me.lkpOrden.DisplayMember = "orden"
        Me.lkpOrden.EditValue = Nothing
        Me.lkpOrden.InitValue = Nothing
        Me.lkpOrden.Location = New System.Drawing.Point(144, 48)
        Me.lkpOrden.MultiSelect = False
        Me.lkpOrden.Name = "lkpOrden"
        Me.lkpOrden.NullText = ""
        Me.lkpOrden.PopupWidth = CType(350, Long)
        Me.lkpOrden.Required = True
        Me.lkpOrden.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpOrden.SearchMember = ""
        Me.lkpOrden.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpOrden.SelectAll = False
        Me.lkpOrden.Size = New System.Drawing.Size(136, 20)
        Me.lkpOrden.TabIndex = 1
        Me.lkpOrden.Tag = ""
        Me.lkpOrden.ToolTip = Nothing
        Me.lkpOrden.ValueMember = "Orden"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(61, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 16)
        Me.Label2.TabIndex = 59
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sucursal:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.Location = New System.Drawing.Point(134, 136)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(296, 16)
        Me.lblSucursal.TabIndex = 59
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(68, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 16)
        Me.Label3.TabIndex = 59
        Me.Label3.Tag = ""
        Me.Label3.Text = "Bodega:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega
        '
        Me.lblBodega.Location = New System.Drawing.Point(134, 160)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(296, 16)
        Me.lblBodega.TabIndex = 59
        Me.lblBodega.Tag = ""
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(50, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 16)
        Me.Label5.TabIndex = 59
        Me.Label5.Tag = ""
        Me.Label5.Text = "Proveedor:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblProveedor
        '
        Me.lblProveedor.Location = New System.Drawing.Point(134, 184)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(296, 16)
        Me.lblProveedor.TabIndex = 59
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(77, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 16)
        Me.Label4.TabIndex = 59
        Me.Label4.Tag = ""
        Me.Label4.Text = "Fecha:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.Location = New System.Drawing.Point(134, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(96, 16)
        Me.lblFecha.TabIndex = 59
        Me.lblFecha.Tag = ""
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(20, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(102, 16)
        Me.Label7.TabIndex = 59
        Me.Label7.Tag = ""
        Me.Label7.Text = "Fecha Promesa:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFechaPromesa
        '
        Me.lblFechaPromesa.Location = New System.Drawing.Point(134, 112)
        Me.lblFechaPromesa.Name = "lblFechaPromesa"
        Me.lblFechaPromesa.Size = New System.Drawing.Size(96, 16)
        Me.lblFechaPromesa.TabIndex = 59
        Me.lblFechaPromesa.Tag = ""
        Me.lblFechaPromesa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblObservaciones
        '
        Me.lblObservaciones.Location = New System.Drawing.Point(134, 208)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(296, 48)
        Me.lblObservaciones.TabIndex = 59
        Me.lblObservaciones.Tag = ""
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(23, 208)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 16)
        Me.Label10.TabIndex = 59
        Me.Label10.Tag = ""
        Me.Label10.Text = "Observaciones:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAbrirOrdenCompra
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(306, 80)
        Me.Controls.Add(Me.lblOrden)
        Me.Controls.Add(Me.lkpOrden)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblFechaPromesa)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.Label10)
        Me.Name = "frmAbrirOrdenCompra"
        Me.Text = "frmAbrirOrdenCompra"
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblFechaPromesa, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpOrden, 0)
        Me.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oOrdenesCompra As VillarrealBusiness.clsOrdenesCompra

    Private ReadOnly Property Orden() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpOrden)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmAbrirOrdenCompra_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oOrdenesCompra = New VillarrealBusiness.clsOrdenesCompra
    End Sub

    Private Sub frmAbrirOrdenCompra_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If ShowMessage(MessageType.MsgQuestion, "�Est� seguro de Abrir la Orden de Compra?", "Mensaje del Sistema") = Answer.MsgYes Then
            Response = oOrdenesCompra.AbrirOrden(Orden)
        Else
            Response.Message = "Datos no Guardados"
        End If

    End Sub

    Private Sub frmAbrirOrdenCompra_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oOrdenesCompra.Validacion(Action, Orden)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub frmAbrirOrdenCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub lkpOrden_LoadData(ByVal Initialize As Boolean) Handles lkpOrden.LoadData
        Dim response As Events
        response = oOrdenesCompra.LookupOrdenesCerradas()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpOrden.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpOrden_Format() Handles lkpOrden.Format
        modFormatos.for_ordenes_compra_grl(Me.lkpOrden)
    End Sub
    Private Sub lkpOrden_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpOrden.EditValueChanged
        Me.lblFecha.Text = CDate(Me.lkpOrden.GetValue("fecha")).ToShortDateString
        Me.lblFechaPromesa.Text = CDate(Me.lkpOrden.GetValue("fecha_promesa")).ToShortDateString
        Me.lblProveedor.Text = Me.lkpOrden.GetValue("proveedor")
        Me.lblSucursal.Text = Me.lkpOrden.GetValue("sucursal")
        Me.lblBodega.Text = Me.lkpOrden.GetValue("bodega")
        Me.lblObservaciones.Text = Me.lkpOrden.GetValue("observaciones")
        Me.Width = 455
        Me.Height = 296
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region

End Class