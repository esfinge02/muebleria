Imports Dipros.Utils.Common
Imports Dipros.Utils
Public Class frmCambiaBodegaSalidaVenta
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnLlenar As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnVaciar As System.Windows.Forms.ToolBarButton
    Friend WithEvents tblImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpFolio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEstatus2 As System.Windows.Forms.Label
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodegaAnterior As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodegaNueva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblFecha2 As System.Windows.Forms.Label
    Friend WithEvents lblCliente2 As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents rlkpBodegaNueva As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents grcNombreBodegaAnterior As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tlbArticulos As System.Windows.Forms.ToolBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambiaBodegaSalidaVenta))
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEstatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreBodegaAnterior = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodegaAnterior = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodegaNueva = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rlkpBodegaNueva = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblFecha2 = New System.Windows.Forms.Label
        Me.tlbArticulos = New System.Windows.Forms.ToolBar
        Me.btnLlenar = New System.Windows.Forms.ToolBarButton
        Me.btnVaciar = New System.Windows.Forms.ToolBarButton
        Me.tblImprimir = New System.Windows.Forms.ToolBarButton
        Me.lblCliente2 = New System.Windows.Forms.Label
        Me.lblCliente = New System.Windows.Forms.Label
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpFolio = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.lblEstatus2 = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rlkpBodegaNueva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFactura.SuspendLayout()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblImprimir})
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2628, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 160)
        Me.grFacturas.MainView = Me.grvArticulos
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar, Me.rlkpBodegaNueva})
        Me.grFacturas.Size = New System.Drawing.Size(728, 312)
        Me.grFacturas.TabIndex = 8
        Me.grFacturas.Text = "Articulos"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcPartida, Me.grcEstatus, Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcNombreBodegaAnterior, Me.grcBodegaAnterior, Me.grcBodegaNueva, Me.grcCliente, Me.grcFolioFactura, Me.grcFactura})
        Me.grvArticulos.GridControl = Me.grFacturas
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsCustomization.AllowFilter = False
        Me.grvArticulos.OptionsCustomization.AllowGroup = False
        Me.grvArticulos.OptionsCustomization.AllowSort = False
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        Me.grvArticulos.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 39
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        Me.chkSeleccionar.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Inactive
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.VisibleIndex = 1
        Me.grcPartida.Width = 55
        '
        'grcEstatus
        '
        Me.grcEstatus.Caption = "Estatus"
        Me.grcEstatus.FieldName = "estatus"
        Me.grcEstatus.Name = "grcEstatus"
        Me.grcEstatus.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEstatus.VisibleIndex = 2
        Me.grcEstatus.Width = 84
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 3
        Me.grcArticulo.Width = 56
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 4
        Me.grcModelo.Width = 72
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 5
        Me.grcDescripcion.Width = 212
        '
        'grcNombreBodegaAnterior
        '
        Me.grcNombreBodegaAnterior.Caption = "Bodega Anterior"
        Me.grcNombreBodegaAnterior.FieldName = "nombre_bodega_anterior"
        Me.grcNombreBodegaAnterior.Name = "grcNombreBodegaAnterior"
        Me.grcNombreBodegaAnterior.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreBodegaAnterior.VisibleIndex = 6
        Me.grcNombreBodegaAnterior.Width = 99
        '
        'grcBodegaAnterior
        '
        Me.grcBodegaAnterior.Caption = "Bodega Anterior"
        Me.grcBodegaAnterior.FieldName = "bodega_anterior"
        Me.grcBodegaAnterior.Name = "grcBodegaAnterior"
        Me.grcBodegaAnterior.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodegaAnterior.Width = 90
        '
        'grcBodegaNueva
        '
        Me.grcBodegaNueva.Caption = "Bodega Nueva"
        Me.grcBodegaNueva.ColumnEdit = Me.rlkpBodegaNueva
        Me.grcBodegaNueva.FieldName = "bodega_nueva"
        Me.grcBodegaNueva.Name = "grcBodegaNueva"
        Me.grcBodegaNueva.VisibleIndex = 7
        Me.grcBodegaNueva.Width = 109
        '
        'rlkpBodegaNueva
        '
        Me.rlkpBodegaNueva.AutoHeight = False
        Me.rlkpBodegaNueva.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rlkpBodegaNueva.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bodega_nueva", "Bodega", 30), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripci�n", 200)})
        Me.rlkpBodegaNueva.DisplayMember = "descripcion"
        Me.rlkpBodegaNueva.Name = "rlkpBodegaNueva"
        Me.rlkpBodegaNueva.NullText = ""
        Me.rlkpBodegaNueva.PopupWidth = 250
        Me.rlkpBodegaNueva.ValueMember = "bodega_nueva"
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.Width = 189
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "Folio Factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        Me.grcFolioFactura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "clave_compuesta"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.Width = 72
        '
        'lblFecha2
        '
        Me.lblFecha2.AutoSize = True
        Me.lblFecha2.Location = New System.Drawing.Point(336, 98)
        Me.lblFecha2.Name = "lblFecha2"
        Me.lblFecha2.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha2.TabIndex = 3
        Me.lblFecha2.Tag = ""
        Me.lblFecha2.Text = "&Fecha:"
        Me.lblFecha2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlbArticulos
        '
        Me.tlbArticulos.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tlbArticulos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tlbArticulos.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnLlenar, Me.btnVaciar})
        Me.tlbArticulos.ButtonSize = New System.Drawing.Size(77, 22)
        Me.tlbArticulos.Dock = System.Windows.Forms.DockStyle.None
        Me.tlbArticulos.DropDownArrows = True
        Me.tlbArticulos.ImageList = Me.ilsToolbar
        Me.tlbArticulos.Location = New System.Drawing.Point(8, 124)
        Me.tlbArticulos.Name = "tlbArticulos"
        Me.tlbArticulos.ShowToolTips = True
        Me.tlbArticulos.Size = New System.Drawing.Size(728, 29)
        Me.tlbArticulos.TabIndex = 7
        Me.tlbArticulos.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'btnLlenar
        '
        Me.btnLlenar.ImageIndex = 4
        Me.btnLlenar.Text = "Todos"
        '
        'btnVaciar
        '
        Me.btnVaciar.ImageIndex = 1
        Me.btnVaciar.Text = "Ninguno"
        '
        'tblImprimir
        '
        Me.tblImprimir.Text = "Imprimir"
        Me.tblImprimir.ToolTipText = "Impresion del Reporte de Notas de Cargo"
        Me.tblImprimir.Visible = False
        '
        'lblCliente2
        '
        Me.lblCliente2.AutoSize = True
        Me.lblCliente2.Location = New System.Drawing.Point(8, 98)
        Me.lblCliente2.Name = "lblCliente2"
        Me.lblCliente2.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente2.TabIndex = 1
        Me.lblCliente2.Tag = ""
        Me.lblCliente2.Text = "C&liente:"
        Me.lblCliente2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCliente
        '
        Me.lblCliente.Location = New System.Drawing.Point(64, 94)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(256, 24)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Text = "Label2"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label5)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpFolio)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal)
        Me.gpbFactura.Controls.Add(Me.lkpSerie)
        Me.gpbFactura.Location = New System.Drawing.Point(8, 40)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(728, 48)
        Me.gpbFactura.TabIndex = 0
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Text = "Factura:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(560, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "F&olio:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(328, 16)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolio
        '
        Me.lkpFolio.AllowAdd = False
        Me.lkpFolio.AutoReaload = False
        Me.lkpFolio.DataSource = Nothing
        Me.lkpFolio.DefaultSearchField = ""
        Me.lkpFolio.DisplayMember = "folio"
        Me.lkpFolio.EditValue = Nothing
        Me.lkpFolio.Filtered = False
        Me.lkpFolio.InitValue = Nothing
        Me.lkpFolio.Location = New System.Drawing.Point(600, 14)
        Me.lkpFolio.MultiSelect = False
        Me.lkpFolio.Name = "lkpFolio"
        Me.lkpFolio.NullText = ""
        Me.lkpFolio.PopupWidth = CType(350, Long)
        Me.lkpFolio.Required = False
        Me.lkpFolio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio.SearchMember = ""
        Me.lkpFolio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio.SelectAll = False
        Me.lkpFolio.Size = New System.Drawing.Size(112, 20)
        Me.lkpFolio.TabIndex = 5
        Me.lkpFolio.Tag = "folio_factura"
        Me.lkpFolio.ToolTip = Nothing
        Me.lkpFolio.ValueMember = "folio"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 14)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(168, 20)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(376, 14)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(112, 20)
        Me.lkpSerie.TabIndex = 3
        Me.lkpSerie.Tag = "serie_factura"
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'lblEstatus2
        '
        Me.lblEstatus2.AutoSize = True
        Me.lblEstatus2.Location = New System.Drawing.Point(552, 98)
        Me.lblEstatus2.Name = "lblEstatus2"
        Me.lblEstatus2.Size = New System.Drawing.Size(50, 16)
        Me.lblEstatus2.TabIndex = 5
        Me.lblEstatus2.Tag = ""
        Me.lblEstatus2.Text = "&Estatus:"
        Me.lblEstatus2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.Location = New System.Drawing.Point(384, 94)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(112, 24)
        Me.lblFecha.TabIndex = 9
        Me.lblFecha.Text = "Label2"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = ""
        Me.cboStatus.Location = New System.Drawing.Point(608, 96)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Entregada", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("No Entregada", "NE", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(112, 20)
        Me.cboStatus.TabIndex = 11
        Me.cboStatus.Tag = "estatus"
        '
        'frmCambiaBodegaSalidaVenta
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(746, 480)
        Me.Controls.Add(Me.lblEstatus2)
        Me.Controls.Add(Me.lblCliente2)
        Me.Controls.Add(Me.lblFecha2)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.gpbFactura)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.tlbArticulos)
        Me.Name = "frmCambiaBodegaSalidaVenta"
        Me.Text = "Generaci�n de Notas de Cargo por Descuentos Anticipados"
        Me.Controls.SetChildIndex(Me.tlbArticulos, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.gpbFactura, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.lblFecha2, 0)
        Me.Controls.SetChildIndex(Me.lblCliente2, 0)
        Me.Controls.SetChildIndex(Me.lblEstatus2, 0)
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rlkpBodegaNueva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFactura.ResumeLayout(False)
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oBodegas As VillarrealBusiness.clsBodegas
    Public oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oArticulosExistencias As VillarrealBusiness.clsArticulosExistencias
    Private oVariables As VillarrealBusiness.clsVariables
    Private intCajero As Long

    Private sConceptoCargoPorDescuentosAnticipados As String = ""

    'Private sConceptoDescuentosAnticipados As String = ""
    Private sConceptoFacturaCredito As String = ""
    Private sucursal_dependencia As Boolean

    'Private ReadOnly Property Sucursal() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
    '    End Get
    'End Property
    'Private ReadOnly Property cliente() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
    '    End Get
    'End Property
    'Private ReadOnly Property Cajero() As Long
    '    Get
    '        If Comunes.clsUtilerias.uti_Usuariocajero(CStr(TINApp.Connection.User), intCajero) = True Then
    '            Return intCajero
    '        Else
    '            Return -1
    '        End If
    '    End Get
    'End Property
    'Private ReadOnly Property Concepto_Cargo_Por_Descuentos_Anticipados() As String
    '    Get
    '        Return oVariables.TraeDatos("concepto_cxc_nota_cargo_descuento_anticipado", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
    '    End Get
    'End Property

    Private ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property folio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Private ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property





    Private ReadOnly Property Concepto_Descuentos_Anticipados() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_descuentos_anticipados", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Private ReadOnly Property Concepto_Factura_Credito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property





    Private folio_movimiento As Long = 0


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCambiaBodegaSalidaVenta_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCambiaBodegaSalidaVenta_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCambiaBodegaSalidaVenta_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmCambiaBodegaSalidaVenta_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oBodegas = New VillarrealBusiness.clsBodegas
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oVariables = New VillarrealBusiness.clsVariables
        oArticulosExistencias = New VillarrealBusiness.clsArticulosExistencias

        sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)

        Me.lblFecha.Text = "" 'Format(CDate(TINApp.FechaServidor), "dd/MMM/yyyy")
        Me.lblCliente.Text = ""


        'If Concepto_Descuentos_Anticipados.Length = 0 Then
        '    Me.tbrTools.Buttons.Item(0).Visible = False
        '    Me.tbrTools.Buttons.Item(0).Enabled = False
        '    ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Descuentos Anticipados no esta definido", "Variables del Sistema", Nothing, False)
        'End If
    End Sub
    Private Sub frmCambiaBodegaSalidaVenta_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Me.grvArticulos.CloseEditor()
        'Dim impuesto As Double
        'Dim subtotal As Double
        'Dim iva As Double
        'Dim dImporte_proporcional_costo As Double
        'Dim dIVa As Double


        'sConceptoCargoPorDescuentosAnticipados = Me.Concepto_Cargo_Por_Descuentos_Anticipados

        ''sConceptoDescuentosAnticipados = Concepto_Descuentos_Anticipados
        'sConceptoFacturaCredito = Concepto_Factura_Credito
        'impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        Dim orow As DataRow
        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
            If orow("seleccionar") = True Then
                oVentasDetalle.ActualizarBodegaVenta(Me.sucursal, Me.serie, Me.folio, orow("partida"), orow("bodega_anterior"), orow("bodega_nueva"))
            End If
        Next
        Me.CargarArticulos()
     

    End Sub
    Private Sub frmCambiaBodegaSalidaVenta_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaVenta()
        If Not Response.ErrorFound Then
            Response = ValidaGrid(True)
            'If Response.ErrorFound Then
            'End If

        End If
    End Sub
    Private Sub frmCambiaBodegaSalidaVenta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
#Region "DIPROS Systems, LookUps"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        ''Select Case Action
        ''    Case Actions.Insert

        lkpSerie_LoadData(True)
        lkpSerie.EditValue = ""
        lkpFolio.Text = Nothing

        ''End Select
    End Sub

    'Private Sub lkpSucursalElabora_Format()
    '    Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursalElabora)
    'End Sub
    'Private Sub lkpSucursalElabora_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    Response = oVentas.LookupSucursalVentas
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpSucursalElabora.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If
    '    Response = Nothing
    'End Sub
    'Private Sub lkpSucursalElabora_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    ''lkpSerie_LoadData(True)
    'End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        ''Select Case Action
        ''    Case Actions.Insert
        lkpFolio_LoadData(True)
        Me.lkpFolio.EditValue = -1
        Me.lkpFolio.Text = Nothing

        ''End Select
    End Sub

    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal, serie, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio)
    End Sub
    Private Sub lkpFolio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolio.EditValueChanged
        ''Select Case Action
        ''    Case Actions.Insert
        If Me.lkpFolio.EditValue = -1 Or Me.lkpFolio.EditValue = Nothing Then
            Me.lkpFolio.Text.Remove(0, Me.lkpFolio.Text.Length - 1)
        End If
        CargaDatosVenta()
        CargarArticulos()
        ''End Select
    End Sub

   
#End Region

    Private Sub tlbArticulos_ButtonClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tlbArticulos.ButtonClick
        If e.Button Is btnLlenar Then
            Dim oRow As DataRow
            For Each oRow In CType(Me.grvArticulos.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = True

            Next
        ElseIf e.Button Is btnVaciar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvArticulos.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = False
            Next
        End If

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargaDatosVenta()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.DespliegaDatos(sucursal, serie, folio)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lblFecha.Text = CType(Format(CDate(oDataSet.Tables(0).Rows(0).Item("fecha")), "dd/MMM/yyyy"), String)
                Me.lblCliente.Text = CType(oDataSet.Tables(0).Rows(0).Item("cliente_nombre"), String)
                Me.cboStatus.EditValue = oDataSet.Tables(0).Rows(0).Item("estatus")
                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub CargarArticulos()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentasDetalle.Listado(sucursal, serie, folio)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)
                oDataSet = Nothing
            End If


        Catch ex As Exception

        End Try

    End Sub
    Private Sub LlenarLkpBodegaNueva(ByVal bodega As String, ByVal articulo As Long)
        Dim response As Events
        Dim oDataSet As DataSet

        response = oBodegas.LookupSinBodegaActual(bodega, sucursal_dependencia, articulo)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.rlkpBodegaNueva.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Function ValidaGrid(ByVal AlGuardar As Boolean) As Events
        Me.grvArticulos.CloseEditor()
        Dim orow As DataRow
        Dim Response As New Dipros.Utils.Events
        Dim odata As DataSet
        Dim fila As Long


        fila = Me.grvArticulos.FocusedRowHandle() + 1

        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
            If (orow("seleccionar") = True And AlGuardar = True) Or (AlGuardar = False And fila = orow("partida")) Then
                Response = oArticulosExistencias.ArticulosBodegaExistencias(orow("bodega_nueva"), orow("articulo"))
                If Not Response.ErrorFound Then
                    odata = Response.Value
                    If odata.Tables(0).Rows(0).Item(0) <= 0 Then
                        Response.Ex = Nothing
                        Response.Message = "Partida " + CType(orow("partida"), String) + ". La bodega " + CType(orow("bodega_nueva"), String).Trim + " no tiene existencias del art�culo " + CType(orow("articulo"), String).Trim + "."
                        Return Response
                    End If
                Else
                    Response.Ex = Nothing
                    Response.Message = "Ocurri� un error al validar las existencia de los art�culos"
                    Return Response
                End If
            End If


            If CType(orow("estatus"), String) = "Repartiendo" Then
                Response.Ex = Nothing
                Response.Message = "Partida " + CType(orow("partida"), String) + ".  No se puede cambiar la bodega por que el art�culo " + CType(orow("articulo"), String).Trim + " esta por repartirse."
                Return Response
            End If
        Next
        Return Response
    End Function
    Private Function ValidaVenta() As Events
        Dim Response As New Dipros.Utils.Events
        If CType(Me.cboStatus.EditValue, String).Trim.ToUpper = "C" Or CType(Me.cboStatus.EditValue, String).Trim.ToUpper = "E" Then
            Select Case CType(Me.cboStatus.EditValue, String).Trim.ToUpper
                Case "C"
                    Response.Ex = Nothing
                    Response.Message = "No se puede cambiar la bodega por que la venta fu� cancelada."
                    Return Response
                Case "E"
                    Response.Ex = Nothing
                    Response.Message = "No se puede cambiar la bodega por que la venta fu� entregada."
                    Return Response
            End Select
            Return Response
        End If
        Return Response
    End Function

    Private Sub rlkpBodegaNueva_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rlkpBodegaNueva.EditValueChanged
        Me.grvArticulos.CloseEditor()
        If Me.grvArticulos.RowCount > 0 Then
            Dim response As Dipros.Utils.Events
            response = Me.ValidaGrid(False)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, response.Message)
            End If
        End If
    End Sub
    Private Sub grvArticulos_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvArticulos.FocusedRowChanged
        If Me.grvArticulos.FocusedColumn Is Me.grcBodegaNueva Then
            Dim bodega As String
            Dim articulo_grid As Long

            bodega = Me.grvArticulos.GetRowCellValue(e.FocusedRowHandle, grcBodegaAnterior)
            articulo_grid = Me.grvArticulos.GetRowCellValue(e.FocusedRowHandle, Me.grcArticulo)

            LlenarLkpBodegaNueva(bodega, articulo_grid)
        End If


        If CType(Me.grvArticulos.GetRowCellValue(e.FocusedRowHandle, grcEstatus), String).ToUpper = "ENTREGADO" Or CType(Me.grvArticulos.GetRowCellValue(e.FocusedRowHandle, grcEstatus), String).ToUpper = "REPARTIENDO" Then
            grcSeleccionar.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
            grcBodegaNueva.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Else
            grcSeleccionar.Options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
            Me.grcBodegaNueva.Options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        End If

    End Sub
    Private Sub grvArticulos_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles grvArticulos.FocusedColumnChanged

        Dim fila As Integer = Me.grvArticulos.FocusedRowHandle
        If e.FocusedColumn Is Me.grcBodegaNueva Then
            Dim bodega As String
            Dim articulo_grid As Long

            bodega = Me.grvArticulos.GetRowCellValue(fila, grcBodegaAnterior)
            articulo_grid = Me.grvArticulos.GetRowCellValue(fila, Me.grcArticulo)
            LlenarLkpBodegaNueva(bodega, articulo_grid)
        End If

    End Sub

    'Private Function ValidaGrid() As Events
    '    Dim oevent As New Events

    '    Me.grvFacturas.CloseEditor()
    '    Me.grvFacturas.UpdateCurrentRow()

    '    If CType(Me.grFacturas.DataSource, DataTable).Select("seleccionar = 1").Length <= 0 Then
    '        oevent.Ex = Nothing
    '        oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"
    '    End If
    '    Return oevent
    'End Function

    'Private Function ValidaBonificar() As Events
    '    Dim oevent As New Events
    '    Dim orow As DataRow
    '    'Me.grvFacturas.CloseEditor()
    '    'Me.grvFacturas.UpdateCurrentRow()

    '    For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows

    '        If Not Isnumeric(orow("bonificar")) Then
    '            oevent.Ex = Nothing
    '            oevent.Message = "El importe a bonificar debe ser un valor num�rico"
    '            Return oevent
    '        End If
    '    Next
    '    Return oevent
    'End Function


    'Private Function ValidaCajero() As Events
    '    Dim oevent As New Events
    '    If Cajero <= 0 Then
    '        oevent.Ex = Nothing
    '        oevent.Message = "El Usuario No Tiene un Cajero Asignado"
    '    End If
    '    Return oevent
    'End Function
    'Private Sub ImprimeListadoNotasCargo()
    '    'Dim Response As Events
    '    'Dim oReportes As New VillarrealBusiness.Reportes
    '    'Try
    '    '    'Response = oReportes.ListadoNotasDeCredito(Sucursal, dteFecha.EditValue)
    '    '    Response = oReportes.ListadoNotasDeCargo(Sucursal, dteFecha.EditValue)

    '    '    If Response.ErrorFound Then
    '    '        ShowMessage(MessageType.MsgInformation, "La Impresi�n de las Notas de Cargo no pueden Mostrarse")
    '    '    Else
    '    '        Dim oDataSet As DataSet
    '    '        oDataSet = Response.Value

    '    '        If oDataSet.Tables(0).Rows.Count > 0 Then
    '    '            'Dim oReport As New rptNotasCredito
    '    '            Dim oReport As New rptNotasCargo
    '    '            oReport.DataSource = oDataSet.Tables(0)

    '    '            TINApp.ShowReport(Me.MdiParent, "Impresi�n de Listado de Notas de Cargo", oReport)
    '    '            oReport = Nothing
    '    '        Else
    '    '            ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
    '    '        End If

    '    '        oDataSet = Nothing
    '    '    End If
    '    'Catch ex As Exception
    '    '    ShowMessage(MessageType.MsgError, ex.ToString, )
    '    'End Try

    'End Sub
    'Private Sub ImprimeNotaDeCargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long)
    '    Dim Response As Events
    '    Dim oReportes As New VillarrealBusiness.Reportes

    '    Try
    '        Response = oReportes.ImprimeNotaDeCargo(sucursal, concepto, serie, folio, cliente, True)

    '        If Response.ErrorFound Then
    '            ShowMessage(MessageType.MsgInformation, "Las Notas de Cargo no pueden Mostrarse")
    '        Else
    '            Dim oDataSet As DataSet
    '            oDataSet = Response.Value

    '            If oDataSet.Tables(0).Rows.Count > 0 Then
    '                Dim oReport As New rptNotaCargo
    '                oReport.DataSource = oDataSet.Tables(0)

    '                TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo " & CType(folio, String), oReport)
    '                oReport = Nothing
    '            Else
    '                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
    '            End If

    '            oDataSet = Nothing
    '        End If
    '    Catch ex As Exception
    '        ShowMessage(MessageType.MsgError, ex.ToString, )
    '    End Try
    'End Sub

#End Region

End Class


