Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmVales
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grcsucursal_recibo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcconcepto_recibo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfolio_recibo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcimporte As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Public WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcImporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcSaldo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents dteVigencia As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents grValesDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvValesDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents repClcFolio_Recibo As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents clcFolioFactura As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtSerieFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tblAutorizarCanjeValePersonaDistinta As System.Windows.Forms.ToolBarButton
    Friend WithEvents tblReimprimirVale As System.Windows.Forms.ToolBarButton
    Friend WithEvents lkpTipoVale As Dipros.Editors.TINMultiLookup
    Public WithEvents lblTipoVale As System.Windows.Forms.Label
    Friend WithEvents txtNombreTitular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNombreTitular As System.Windows.Forms.Label
    Friend WithEvents gpbConvenio As System.Windows.Forms.GroupBox
    Friend WithEvents clcAnioConvenio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblAnioconvenio As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVales))
        Me.grValesDetalle = New DevExpress.XtraGrid.GridControl
        Me.grvValesDetalle = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcsucursal_recibo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcconcepto_recibo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfolio_recibo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.repClcFolio_Recibo = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcfecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcimporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.clcFolioFactura = New DevExpress.XtraEditors.CalcEdit
        Me.txtSerieFactura = New DevExpress.XtraEditors.TextEdit
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcImporte = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcSaldo = New DevExpress.XtraEditors.CalcEdit
        Me.dteVigencia = New DevExpress.XtraEditors.DateEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tblAutorizarCanjeValePersonaDistinta = New System.Windows.Forms.ToolBarButton
        Me.tblReimprimirVale = New System.Windows.Forms.ToolBarButton
        Me.lkpTipoVale = New Dipros.Editors.TINMultiLookup
        Me.lblTipoVale = New System.Windows.Forms.Label
        Me.txtNombreTitular = New DevExpress.XtraEditors.TextEdit
        Me.lblNombreTitular = New System.Windows.Forms.Label
        Me.gpbConvenio = New System.Windows.Forms.GroupBox
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblAnioconvenio = New System.Windows.Forms.Label
        Me.clcAnioConvenio = New DevExpress.XtraEditors.CalcEdit
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        CType(Me.grValesDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvValesDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repClcFolio_Recibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFactura.SuspendLayout()
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerieFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteVigencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtNombreTitular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbConvenio.SuspendLayout()
        CType(Me.clcAnioConvenio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblReimprimirVale, Me.tblAutorizarCanjeValePersonaDistinta})
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5250, 28)
        '
        'grValesDetalle
        '
        '
        'grValesDetalle.EmbeddedNavigator
        '
        Me.grValesDetalle.EmbeddedNavigator.Name = ""
        Me.grValesDetalle.Location = New System.Drawing.Point(8, 376)
        Me.grValesDetalle.MainView = Me.grvValesDetalle
        Me.grValesDetalle.Name = "grValesDetalle"
        Me.grValesDetalle.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemDateEdit1, Me.repClcFolio_Recibo})
        Me.grValesDetalle.Size = New System.Drawing.Size(619, 216)
        Me.grValesDetalle.TabIndex = 20
        Me.grValesDetalle.Text = "GridControl1"
        '
        'grvValesDetalle
        '
        Me.grvValesDetalle.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcsucursal_recibo, Me.grcconcepto_recibo, Me.grcfolio_recibo, Me.grcfecha, Me.grcimporte, Me.grcFactura})
        Me.grvValesDetalle.GridControl = Me.grValesDetalle
        Me.grvValesDetalle.Name = "grvValesDetalle"
        Me.grvValesDetalle.OptionsView.ShowGroupPanel = False
        '
        'grcsucursal_recibo
        '
        Me.grcsucursal_recibo.Caption = "Sucursal Recibo"
        Me.grcsucursal_recibo.FieldName = "sucursal_recibo"
        Me.grcsucursal_recibo.Name = "grcsucursal_recibo"
        Me.grcsucursal_recibo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcsucursal_recibo.VisibleIndex = 0
        Me.grcsucursal_recibo.Width = 129
        '
        'grcconcepto_recibo
        '
        Me.grcconcepto_recibo.Caption = "Concepto Recibo"
        Me.grcconcepto_recibo.FieldName = "concepto_recibo"
        Me.grcconcepto_recibo.Name = "grcconcepto_recibo"
        Me.grcconcepto_recibo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcconcepto_recibo.VisibleIndex = 1
        Me.grcconcepto_recibo.Width = 99
        '
        'grcfolio_recibo
        '
        Me.grcfolio_recibo.Caption = "Folio Recibo"
        Me.grcfolio_recibo.ColumnEdit = Me.repClcFolio_Recibo
        Me.grcfolio_recibo.FieldName = "Recibo"
        Me.grcfolio_recibo.Name = "grcfolio_recibo"
        Me.grcfolio_recibo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfolio_recibo.VisibleIndex = 3
        Me.grcfolio_recibo.Width = 86
        '
        'repClcFolio_Recibo
        '
        Me.repClcFolio_Recibo.AutoHeight = False
        Me.repClcFolio_Recibo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repClcFolio_Recibo.DisplayFormat.FormatString = "n0"
        Me.repClcFolio_Recibo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.repClcFolio_Recibo.Name = "repClcFolio_Recibo"
        '
        'grcfecha
        '
        Me.grcfecha.Caption = "Fecha"
        Me.grcfecha.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.grcfecha.FieldName = "fecha"
        Me.grcfecha.Name = "grcfecha"
        Me.grcfecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfecha.VisibleIndex = 4
        Me.grcfecha.Width = 105
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'grcimporte
        '
        Me.grcimporte.Caption = "Importe"
        Me.grcimporte.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcimporte.DisplayFormat.FormatString = "c2"
        Me.grcimporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcimporte.FieldName = "importe"
        Me.grcimporte.Name = "grcimporte"
        Me.grcimporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcimporte.VisibleIndex = 5
        Me.grcimporte.Width = 99
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "Factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 2
        Me.grcFactura.Width = 87
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "10/04/2006"
        Me.dteFecha.Location = New System.Drawing.Point(529, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = "fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(483, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label5)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal)
        Me.gpbFactura.Controls.Add(Me.clcFolioFactura)
        Me.gpbFactura.Controls.Add(Me.txtSerieFactura)
        Me.gpbFactura.Enabled = False
        Me.gpbFactura.Location = New System.Drawing.Point(8, 141)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(291, 96)
        Me.gpbFactura.TabIndex = 14
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Text = "Factura:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(58, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Folio:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(38, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(55, 40)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(99, 14)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(173, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'clcFolioFactura
        '
        Me.clcFolioFactura.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioFactura.Location = New System.Drawing.Point(99, 64)
        Me.clcFolioFactura.Name = "clcFolioFactura"
        '
        'clcFolioFactura.Properties
        '
        Me.clcFolioFactura.Properties.Enabled = False
        Me.clcFolioFactura.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioFactura.Size = New System.Drawing.Size(80, 20)
        Me.clcFolioFactura.TabIndex = 5
        Me.clcFolioFactura.Tag = "folio_factura"
        '
        'txtSerieFactura
        '
        Me.txtSerieFactura.EditValue = ""
        Me.txtSerieFactura.Location = New System.Drawing.Point(99, 39)
        Me.txtSerieFactura.Name = "txtSerieFactura"
        '
        'txtSerieFactura.Properties
        '
        Me.txtSerieFactura.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieFactura.Properties.Enabled = False
        Me.txtSerieFactura.Properties.MaxLength = 50
        Me.txtSerieFactura.Size = New System.Drawing.Size(80, 20)
        Me.txtSerieFactura.TabIndex = 3
        Me.txtSerieFactura.Tag = "serie_factura"
        Me.txtSerieFactura.ToolTip = "descripci�n"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(69, 88)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(352, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 11
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(19, 88)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(69, 40)
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(80, 20)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "folio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporte.Location = New System.Drawing.Point(120, 40)
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "c2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporte.Size = New System.Drawing.Size(96, 20)
        Me.clcImporte.TabIndex = 3
        Me.clcImporte.Tag = "importe"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(61, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Tag = ""
        Me.Label4.Text = "Importe:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(74, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Tag = ""
        Me.Label6.Text = "Saldo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(120, 64)
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldo.Size = New System.Drawing.Size(96, 20)
        Me.clcSaldo.TabIndex = 5
        Me.clcSaldo.Tag = "saldo"
        '
        'dteVigencia
        '
        Me.dteVigencia.EditValue = "10/04/2006"
        Me.dteVigencia.Location = New System.Drawing.Point(120, 15)
        Me.dteVigencia.Name = "dteVigencia"
        '
        'dteVigencia.Properties
        '
        Me.dteVigencia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteVigencia.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteVigencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteVigencia.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteVigencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteVigencia.Properties.Enabled = False
        Me.dteVigencia.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteVigencia.Size = New System.Drawing.Size(95, 23)
        Me.dteVigencia.TabIndex = 1
        Me.dteVigencia.Tag = "vigencia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(59, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Tag = ""
        Me.Label7.Text = "Vigencia:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(197, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(95, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Tag = ""
        Me.Label8.Text = "Usuario Genero:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(293, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 16)
        Me.Label9.TabIndex = 3
        Me.Label9.Tag = "usuario_genera"
        Me.Label9.Text = "Usuario_Genera"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(443, 68)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(50, 16)
        Me.lblTipoventa.TabIndex = 8
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "Estatus:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "A"
        Me.cboEstatus.Location = New System.Drawing.Point(497, 68)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Activo", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Canjeado", "J", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cambiado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", "C", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(127, 23)
        Me.cboEstatus.TabIndex = 9
        Me.cboEstatus.Tag = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dteVigencia)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.clcImporte)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.clcSaldo)
        Me.GroupBox1.Location = New System.Drawing.Point(336, 141)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(291, 96)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(8, 352)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(258, 22)
        Me.Label10.TabIndex = 19
        Me.Label10.Tag = ""
        Me.Label10.Text = "Utilizado en los Documentos:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblAutorizarCanjeValePersonaDistinta
        '
        Me.tblAutorizarCanjeValePersonaDistinta.Enabled = False
        Me.tblAutorizarCanjeValePersonaDistinta.Tag = "BTNAUTOCANJEPERSDIST"
        Me.tblAutorizarCanjeValePersonaDistinta.Text = "Autorizar Canje Persona Distinta"
        Me.tblAutorizarCanjeValePersonaDistinta.ToolTipText = "Autoriza Cajear el vale a otra persona distinta"
        '
        'tblReimprimirVale
        '
        Me.tblReimprimirVale.Text = "Reimprimir"
        '
        'lkpTipoVale
        '
        Me.lkpTipoVale.AllowAdd = False
        Me.lkpTipoVale.AutoReaload = False
        Me.lkpTipoVale.DataSource = Nothing
        Me.lkpTipoVale.DefaultSearchField = ""
        Me.lkpTipoVale.DisplayMember = "nombre"
        Me.lkpTipoVale.EditValue = Nothing
        Me.lkpTipoVale.Filtered = False
        Me.lkpTipoVale.InitValue = Nothing
        Me.lkpTipoVale.Location = New System.Drawing.Point(69, 64)
        Me.lkpTipoVale.MultiSelect = False
        Me.lkpTipoVale.Name = "lkpTipoVale"
        Me.lkpTipoVale.NullText = ""
        Me.lkpTipoVale.PopupWidth = CType(520, Long)
        Me.lkpTipoVale.ReadOnlyControl = False
        Me.lkpTipoVale.Required = False
        Me.lkpTipoVale.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTipoVale.SearchMember = ""
        Me.lkpTipoVale.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTipoVale.SelectAll = False
        Me.lkpTipoVale.Size = New System.Drawing.Size(352, 20)
        Me.lkpTipoVale.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTipoVale.TabIndex = 7
        Me.lkpTipoVale.Tag = "tipo_vale"
        Me.lkpTipoVale.ToolTip = Nothing
        Me.lkpTipoVale.ValueMember = "tipo"
        '
        'lblTipoVale
        '
        Me.lblTipoVale.AutoSize = True
        Me.lblTipoVale.Location = New System.Drawing.Point(6, 64)
        Me.lblTipoVale.Name = "lblTipoVale"
        Me.lblTipoVale.Size = New System.Drawing.Size(60, 16)
        Me.lblTipoVale.TabIndex = 6
        Me.lblTipoVale.Tag = ""
        Me.lblTipoVale.Text = "Tipo Vale:"
        Me.lblTipoVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombreTitular
        '
        Me.txtNombreTitular.EditValue = ""
        Me.txtNombreTitular.Location = New System.Drawing.Point(69, 112)
        Me.txtNombreTitular.Name = "txtNombreTitular"
        '
        'txtNombreTitular.Properties
        '
        Me.txtNombreTitular.Properties.Enabled = False
        Me.txtNombreTitular.Properties.MaxLength = 90
        Me.txtNombreTitular.Size = New System.Drawing.Size(555, 20)
        Me.txtNombreTitular.TabIndex = 13
        Me.txtNombreTitular.Tag = "nombre_titular"
        Me.txtNombreTitular.ToolTip = "Nombre del Titular del Vale"
        '
        'lblNombreTitular
        '
        Me.lblNombreTitular.AutoSize = True
        Me.lblNombreTitular.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreTitular.Location = New System.Drawing.Point(20, 112)
        Me.lblNombreTitular.Name = "lblNombreTitular"
        Me.lblNombreTitular.Size = New System.Drawing.Size(46, 17)
        Me.lblNombreTitular.TabIndex = 12
        Me.lblNombreTitular.Tag = ""
        Me.lblNombreTitular.Text = "Titular:"
        Me.lblNombreTitular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbConvenio
        '
        Me.gpbConvenio.Controls.Add(Me.lkpConvenio)
        Me.gpbConvenio.Controls.Add(Me.Label12)
        Me.gpbConvenio.Controls.Add(Me.lblAnioconvenio)
        Me.gpbConvenio.Controls.Add(Me.clcAnioConvenio)
        Me.gpbConvenio.Enabled = False
        Me.gpbConvenio.Location = New System.Drawing.Point(8, 243)
        Me.gpbConvenio.Name = "gpbConvenio"
        Me.gpbConvenio.Size = New System.Drawing.Size(619, 48)
        Me.gpbConvenio.TabIndex = 16
        Me.gpbConvenio.TabStop = False
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Enabled = False
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(100, 14)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = "(Seleccione un Convenio)"
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(236, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 1
        Me.lkpConvenio.Tag = "convenio"
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(36, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(62, 17)
        Me.Label12.TabIndex = 0
        Me.Label12.Tag = ""
        Me.Label12.Text = "Convenio:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAnioconvenio
        '
        Me.lblAnioconvenio.AutoSize = True
        Me.lblAnioconvenio.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnioconvenio.Location = New System.Drawing.Point(408, 14)
        Me.lblAnioconvenio.Name = "lblAnioconvenio"
        Me.lblAnioconvenio.Size = New System.Drawing.Size(31, 17)
        Me.lblAnioconvenio.TabIndex = 2
        Me.lblAnioconvenio.Tag = ""
        Me.lblAnioconvenio.Text = "A�o:"
        Me.lblAnioconvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnioConvenio
        '
        Me.clcAnioConvenio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcAnioConvenio.Location = New System.Drawing.Point(448, 14)
        Me.clcAnioConvenio.Name = "clcAnioConvenio"
        '
        'clcAnioConvenio.Properties
        '
        Me.clcAnioConvenio.Properties.Enabled = False
        Me.clcAnioConvenio.Properties.MaxLength = 4
        Me.clcAnioConvenio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcAnioConvenio.Size = New System.Drawing.Size(80, 20)
        Me.clcAnioConvenio.TabIndex = 3
        Me.clcAnioConvenio.Tag = "anio_convenio"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(112, 296)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.MaxLength = 100
        Me.txtObservaciones.Size = New System.Drawing.Size(512, 48)
        Me.txtObservaciones.TabIndex = 18
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservaciones.Location = New System.Drawing.Point(16, 300)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(92, 17)
        Me.lblObservaciones.TabIndex = 17
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmVales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(634, 596)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.gpbConvenio)
        Me.Controls.Add(Me.lblNombreTitular)
        Me.Controls.Add(Me.txtNombreTitular)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboEstatus)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grValesDetalle)
        Me.Controls.Add(Me.gpbFactura)
        Me.Controls.Add(Me.lkpTipoVale)
        Me.Controls.Add(Me.lblTipoVale)
        Me.Name = "frmVales"
        Me.Text = "frmVales"
        Me.Controls.SetChildIndex(Me.lblTipoVale, 0)
        Me.Controls.SetChildIndex(Me.lkpTipoVale, 0)
        Me.Controls.SetChildIndex(Me.gpbFactura, 0)
        Me.Controls.SetChildIndex(Me.grValesDetalle, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.txtNombreTitular, 0)
        Me.Controls.SetChildIndex(Me.lblNombreTitular, 0)
        Me.Controls.SetChildIndex(Me.gpbConvenio, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        CType(Me.grValesDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvValesDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repClcFolio_Recibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFactura.ResumeLayout(False)
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerieFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteVigencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtNombreTitular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbConvenio.ResumeLayout(False)
        CType(Me.clcAnioConvenio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oVales As VillarrealBusiness.clsVales
    Private oValesDetalle As VillarrealBusiness.clsValesDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oTiposVales As VillarrealBusiness.clsTiposVales
    Private oConvenios As VillarrealBusiness.clsConvenios


    Private CantidadValesDetalleAbonos As Long = 0
    Private nombre_autoriza_canjear As String = ""
    Private ConceptoVale As String = ""
    Private EsInsert As Boolean = True
    Private ClienteConvenioMVC As Long = 27028

 

    Public ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Public ReadOnly Property FolioVenta() As Long
        Get
            Return Me.clcFolioFactura.Value
            'Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Public ReadOnly Property serie() As String
        Get
            Return Me.txtSerieFactura.Text
            'Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
    Public ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Public ReadOnly Property TipoVale() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTipoVale)
        End Get
    End Property
    Public WriteOnly Property NombreAutorizaCanjear() As String
        Set(ByVal Value As String)
            nombre_autoriza_canjear = Value
        End Set
    End Property
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property

#End Region

#Region "Eventos de la Forma"

    Private Sub frmVales_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmVales_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        'If Me.nombre_autoriza_canjear.Length > 0 Then
        Me.ImprimirVale(Me.clcFolio.EditValue)
        'End If
    End Sub

    Private Sub frmVales_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub


    Private Sub frmVales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oVales.Insertar(Me.clcFolio.Value, Me.dteFecha.DateTime, Me.Cliente, Me.sucursal, Me.serie, Me.FolioVenta, Me.clcImporte.Value, Me.clcSaldo.Value, Me.cboEstatus.Value, Me.dteVigencia.DateTime, Me.TipoVale, Me.Convenio, Me.clcAnioConvenio.EditValue, Me.txtObservaciones.Text, Me.txtNombreTitular.Text, Me.nombre_autoriza_canjear)

                If Not Response.ErrorFound Then
                    GenerarNotaCredito(Response)
                End If

            Case Actions.Update
                    Response = oVales.Actualizar(Me.clcFolio.Value, Me.dteFecha.DateTime, Me.Cliente, Me.sucursal, Me.serie, Me.FolioVenta, Me.clcImporte.Value, Me.clcSaldo.Value, Me.cboEstatus.Value, Me.dteVigencia.DateTime, Me.TipoVale, Me.Convenio, Me.clcAnioConvenio.EditValue, Me.txtObservaciones.Text, Me.txtNombreTitular.Text, Me.nombre_autoriza_canjear)
        End Select

    End Sub

    Private Sub frmVales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVales = New VillarrealBusiness.clsVales
        oValesDetalle = New VillarrealBusiness.clsValesDetalle
        oClientes = New VillarrealBusiness.clsClientes
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oTiposVales = New VillarrealBusiness.clsTiposVales
        oConvenios = New VillarrealBusiness.clsConvenios

        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
        Me.dteVigencia.DateTime = DateAdd(DateInterval.Year, 1, Me.dteFecha.DateTime)

        Select Case Action
            Case Actions.Insert
                Me.clcImporte.Enabled = True
                Me.cboEstatus.Enabled = False
                Me.tblAutorizarCanjeValePersonaDistinta.Enabled = False
                Me.tblAutorizarCanjeValePersonaDistinta.Visible = False

                Me.tblReimprimirVale.Enabled = False
                Me.tblReimprimirVale.Visible = False

            Case Actions.Update
                Me.lkpCliente.Enabled = False

        End Select

    End Sub

    Private Sub frmVales_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oVales.Validacion(Action, Cliente, TipoVale, ConceptoVale, Me.txtNombreTitular.Text, Me.sucursal, Me.serie, Me.FolioVenta, Me.Convenio, Me.clcAnioConvenio.EditValue, Me.cboEstatus.EditValue, CantidadValesDetalleAbonos, TINApp.Connection.User.ToUpper(), Me.clcImporte.EditValue)
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tblReimprimirVale Then
            ImprimirVale(clcFolio.Value)
        End If

        If e.Button Is Me.tblAutorizarCanjeValePersonaDistinta Then
            Dim oForm As New frmValesAutorizarCanjePersonaDistinta
            oForm.OwnerForm = Me
            oForm.NombreAutorizaCanje = Me.nombre_autoriza_canjear
            oForm.Show()
        End If
    End Sub

    Private Sub frmVales_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        EsInsert = False
        lkpTipoVale_LoadData(False)

        Response = oVales.DespliegaDatos(OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            Me.cboEstatus.EditValue = oDataSet.Tables(0).Rows(0).Item("estatus")
            TraerDatosDetalle()


            If Not Comunes.Common.Identificadores Is Nothing Then
                If Comunes.Common.Identificadores.Length > 0 Then
                    Dim Validaciones As Boolean() = VerificaPermisosExtendidos(Me.tbrTools.Buttons, Comunes.Common.Identificadores)
                End If
            End If

            If Me.cboEstatus.EditValue = "J" Or Me.cboEstatus.EditValue = "C" Then
                Me.txtObservaciones.Enabled = False
                Me.tbrTools.Buttons(0).Enabled = False
                Me.tbrTools.Buttons(0).Visible = False
                Me.tblAutorizarCanjeValePersonaDistinta.Enabled = False
            End If


            Me.lkpTipoVale.Enabled = False
            cboEstatus.Enabled = False
            Me.clcSaldo.Enabled = False
            Me.txtNombreTitular.Enabled = False
            Me.lkpConvenio.Enabled = False
            Me.clcAnioConvenio.Enabled = False

            If TINApp.Connection.User.ToUpper = "SUPER" Then
                Me.clcSaldo.Enabled = True
                Me.cboEstatus.Enabled = True
            End If

        End If

    End Sub

    Private Sub frmVales_Localize() Handles MyBase.Localize
        Find("folio", Me.clcFolio.EditValue)
    End Sub

#End Region

#Region "Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        'lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If
        Response = Nothing

    End Sub


    Private Sub lkpTipoVale_LoadData(ByVal Initialize As Boolean) Handles lkpTipoVale.LoadData
        Dim Response As New Events
        If EsInsert Then
            Response = oTiposVales.Lookup("M")
        Else
            Response = oTiposVales.Lookup()
        End If


        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTipoVale.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpTipoVale_Format() Handles lkpTipoVale.Format
        Comunes.clsFormato.for_tipos_vales_grl(Me.lkpTipoVale)
    End Sub
    Private Sub lkpTipoVale_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTipoVale.EditValueChanged
        If Me.TipoVale > 0 Then
            ConceptoVale = Me.lkpTipoVale.GetValue("concepto")

            Select Case ConceptoVale.ToUpper()
                Case "CA"

                Case "DE"  'Descuento
                    Me.txtNombreTitular.Enabled = True
                    Me.CamposDescuento(True)
                    CamposConvenio(False)
                Case "CO"  ' Convenio
                    Me.txtNombreTitular.Enabled = True
                    Me.CamposDescuento(False)
                    CamposConvenio(True)
                    Me.lkpCliente.EditValue = Me.ClienteConvenioMVC
                Case "VE"  ' Venta
                    Me.txtNombreTitular.Enabled = True
                    Me.CamposDescuento(True)
                    CamposConvenio(False)

            End Select
        End If
    End Sub


    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

#End Region

#Region "Funcionalidad"

    Private Sub TraerDatosDetalle()
        Dim response As Events
        Dim odataset As DataSet

        response = oValesDetalle.DespliegaDatos(clcFolio.Value)
        If Not response.ErrorFound Then
            odataset = response.Value
            CantidadValesDetalleAbonos = IIf(odataset.Tables.Count > 0, odataset.Tables(0).Rows.Count, 0)
            Me.grValesDetalle.DataSource = odataset.Tables(0)

        End If
    End Sub
    Private Sub ImprimirVale(ByVal folio As Long)
        Dim response As New Events

        response = oReportes.ImprimeVale(folio)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Vale no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptValeFormato

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Impresi�n del Vale ", oReport, , , , True)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Function GenerarNotaCredito(ByRef response As Events) As Events
        Dim oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar

        response = oMovimientosCobrar.LlenaNotaCreditoValeCFDI(Comunes.Common.Sucursal_Actual, Me.clcFolio.EditValue)


    End Function
    Private Sub CamposDescuento(ByVal Habilitar As Boolean)
        Me.gpbFactura.Enabled = Habilitar
        Me.lkpSucursal.Enabled = Habilitar
        Me.txtSerieFactura.Enabled = Habilitar
        Me.clcFolioFactura.Enabled = Habilitar
        Me.lkpCliente.Enabled = Habilitar
    End Sub
    Private Sub CamposConvenio(ByVal Habilitar As Boolean)
        Me.gpbConvenio.Enabled = Habilitar
        Me.lkpConvenio.Enabled = Habilitar
        Me.clcAnioConvenio.Enabled = Habilitar
    End Sub
#End Region



End Class
