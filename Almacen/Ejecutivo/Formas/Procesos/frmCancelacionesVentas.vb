Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmCancelacionesVentas
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpFolio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    Friend WithEvents grVentas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVentas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkReparto As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSurtido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSurtido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPrecioMinimo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRegalo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioAutorizacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartidaVistas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Public WithEvents lbl_eFecha As System.Windows.Forms.Label
    Friend WithEvents dteFechaVenta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFechaCancelacion As DevExpress.XtraEditors.DateEdit
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnConsultarVenta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtObservacionesCancelacion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents tmaVentas As Dipros.Windows.TINMaster
    Friend WithEvents clcImporteGastosAdministrativos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporteGastosAdministrativos As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gbxGastosAdministrativos As System.Windows.Forms.GroupBox
    Friend WithEvents txtObservacionesAdministrativas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkNoValeReembolso As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lkpOpcionCancelacion As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCancelacionesVentas))
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpFolio = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.grVentas = New DevExpress.XtraGrid.GridControl
        Me.grvVentas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkReparto = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSurtido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSurtido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPrecioMinimo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRegalo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioAutorizacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartidaVistas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnConsultarVenta = New DevExpress.XtraEditors.SimpleButton
        Me.dteFechaVenta = New DevExpress.XtraEditors.DateEdit
        Me.lbl_eFecha = New System.Windows.Forms.Label
        Me.dteFechaCancelacion = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtObservacionesCancelacion = New DevExpress.XtraEditors.MemoEdit
        Me.tmaVentas = New Dipros.Windows.TINMaster
        Me.lblImporteGastosAdministrativos = New System.Windows.Forms.Label
        Me.clcImporteGastosAdministrativos = New Dipros.Editors.TINCalcEdit
        Me.gbxGastosAdministrativos = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtObservacionesAdministrativas = New DevExpress.XtraEditors.MemoEdit
        Me.chkNoValeReembolso = New DevExpress.XtraEditors.CheckEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpOpcionCancelacion = New Dipros.Editors.TINMultiLookup
        Me.gpbFactura.SuspendLayout()
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFechaVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteGastosAdministrativos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGastosAdministrativos.SuspendLayout()
        CType(Me.txtObservacionesAdministrativas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoValeReembolso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4250, 28)
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label5)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpFolio)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal)
        Me.gpbFactura.Controls.Add(Me.lkpSerie)
        Me.gpbFactura.Location = New System.Drawing.Point(8, 64)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(640, 48)
        Me.gpbFactura.TabIndex = 3
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Text = "Factura:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(472, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Folio:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(40, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(296, 16)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolio
        '
        Me.lkpFolio.AllowAdd = False
        Me.lkpFolio.AutoReaload = False
        Me.lkpFolio.DataSource = Nothing
        Me.lkpFolio.DefaultSearchField = ""
        Me.lkpFolio.DisplayMember = "folio"
        Me.lkpFolio.EditValue = Nothing
        Me.lkpFolio.Filtered = False
        Me.lkpFolio.InitValue = Nothing
        Me.lkpFolio.Location = New System.Drawing.Point(520, 14)
        Me.lkpFolio.MultiSelect = False
        Me.lkpFolio.Name = "lkpFolio"
        Me.lkpFolio.NullText = ""
        Me.lkpFolio.PopupWidth = CType(350, Long)
        Me.lkpFolio.ReadOnlyControl = False
        Me.lkpFolio.Required = False
        Me.lkpFolio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio.SearchMember = ""
        Me.lkpFolio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio.SelectAll = False
        Me.lkpFolio.Size = New System.Drawing.Size(112, 20)
        Me.lkpFolio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolio.TabIndex = 5
        Me.lkpFolio.Tag = "folio_factura"
        Me.lkpFolio.ToolTip = Nothing
        Me.lkpFolio.ValueMember = "folio"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(104, 14)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(168, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(336, 14)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.ReadOnlyControl = False
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(112, 20)
        Me.lkpSerie.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerie.TabIndex = 3
        Me.lkpSerie.Tag = "serie_factura"
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'grVentas
        '
        '
        'grVentas.EmbeddedNavigator
        '
        Me.grVentas.EmbeddedNavigator.Name = ""
        Me.grVentas.Location = New System.Drawing.Point(8, 352)
        Me.grVentas.MainView = Me.grvVentas
        Me.grVentas.Name = "grVentas"
        Me.grVentas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSobrePedido, Me.grchkReparto, Me.grchkSurtido})
        Me.grVentas.Size = New System.Drawing.Size(640, 120)
        Me.grVentas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grVentas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.TabIndex = 10
        Me.grVentas.TabStop = False
        Me.grVentas.Text = "Ventas"
        '
        'grvVentas
        '
        Me.grvVentas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcNArticulo, Me.grcArticulo, Me.grcNBodega, Me.grcBodega, Me.grcSobrePedido, Me.grcReparto, Me.grcCantidad, Me.grcPrecioUnitario, Me.grcTotal, Me.grcCosto, Me.grcSurtido, Me.grcPrecioMinimo, Me.grcRegalo, Me.grcContado, Me.grcModelo, Me.grcFolioAutorizacion, Me.grcPartidaVistas, Me.grcNumeroSerie})
        Me.grvVentas.GridControl = Me.grVentas
        Me.grvVentas.Name = "grvVentas"
        Me.grvVentas.OptionsCustomization.AllowFilter = False
        Me.grvVentas.OptionsCustomization.AllowGroup = False
        Me.grvVentas.OptionsCustomization.AllowSort = False
        Me.grvVentas.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.Width = 52
        '
        'grcNArticulo
        '
        Me.grcNArticulo.Caption = "Art�culo"
        Me.grcNArticulo.FieldName = "n_articulo"
        Me.grcNArticulo.Name = "grcNArticulo"
        Me.grcNArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNArticulo.VisibleIndex = 1
        Me.grcNArticulo.Width = 167
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNBodega
        '
        Me.grcNBodega.Caption = "Bodega"
        Me.grcNBodega.FieldName = "n_bodega"
        Me.grcNBodega.Name = "grcNBodega"
        Me.grcNBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNBodega.VisibleIndex = 2
        Me.grcNBodega.Width = 80
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSobrePedido
        '
        Me.grcSobrePedido.Caption = "Ped. a F�brica"
        Me.grcSobrePedido.ColumnEdit = Me.grchkSobrePedido
        Me.grcSobrePedido.FieldName = "sobrepedido"
        Me.grcSobrePedido.Name = "grcSobrePedido"
        Me.grcSobrePedido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSobrePedido.VisibleIndex = 4
        Me.grcSobrePedido.Width = 83
        '
        'grchkSobrePedido
        '
        Me.grchkSobrePedido.AutoHeight = False
        Me.grchkSobrePedido.Name = "grchkSobrePedido"
        Me.grchkSobrePedido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.ColumnEdit = Me.grchkReparto
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.Width = 68
        '
        'grchkReparto
        '
        Me.grchkReparto.AutoHeight = False
        Me.grchkReparto.Name = "grchkReparto"
        Me.grchkReparto.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 5
        Me.grcCantidad.Width = 55
        '
        'grcPrecioUnitario
        '
        Me.grcPrecioUnitario.Caption = "Precio"
        Me.grcPrecioUnitario.DisplayFormat.FormatString = "c2"
        Me.grcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioUnitario.FieldName = "preciounitario"
        Me.grcPrecioUnitario.Name = "grcPrecioUnitario"
        Me.grcPrecioUnitario.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioUnitario.VisibleIndex = 6
        Me.grcPrecioUnitario.Width = 63
        '
        'grcTotal
        '
        Me.grcTotal.Caption = "Total"
        Me.grcTotal.DisplayFormat.FormatString = "c2"
        Me.grcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTotal.FieldName = "total"
        Me.grcTotal.Name = "grcTotal"
        Me.grcTotal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTotal.VisibleIndex = 8
        Me.grcTotal.Width = 98
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSurtido
        '
        Me.grcSurtido.Caption = "Surtido"
        Me.grcSurtido.ColumnEdit = Me.grchkSurtido
        Me.grcSurtido.FieldName = "surtido"
        Me.grcSurtido.Name = "grcSurtido"
        Me.grcSurtido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grchkSurtido
        '
        Me.grchkSurtido.AutoHeight = False
        Me.grchkSurtido.Name = "grchkSurtido"
        Me.grchkSurtido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcPrecioMinimo
        '
        Me.grcPrecioMinimo.Caption = "Precio Minimo"
        Me.grcPrecioMinimo.FieldName = "precio_minimo"
        Me.grcPrecioMinimo.Name = "grcPrecioMinimo"
        Me.grcPrecioMinimo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcRegalo
        '
        Me.grcRegalo.Caption = "Regalo"
        Me.grcRegalo.ColumnEdit = Me.grchkSurtido
        Me.grcRegalo.FieldName = "articulo_regalo"
        Me.grcRegalo.Name = "grcRegalo"
        Me.grcRegalo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRegalo.VisibleIndex = 3
        Me.grcRegalo.Width = 50
        '
        'grcContado
        '
        Me.grcContado.Caption = "P. Contado"
        Me.grcContado.DisplayFormat.FormatString = "c2"
        Me.grcContado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcContado.FieldName = "precio_contado"
        Me.grcContado.Name = "grcContado"
        Me.grcContado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcContado.VisibleIndex = 7
        Me.grcContado.Width = 72
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 0
        Me.grcModelo.Width = 78
        '
        'grcFolioAutorizacion
        '
        Me.grcFolioAutorizacion.Caption = "Folio Aut."
        Me.grcFolioAutorizacion.FieldName = "folio_autorizacion"
        Me.grcFolioAutorizacion.Name = "grcFolioAutorizacion"
        Me.grcFolioAutorizacion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPartidaVistas
        '
        Me.grcPartidaVistas.Caption = "Partida Vistas"
        Me.grcPartidaVistas.FieldName = "partida_vistas"
        Me.grcPartidaVistas.Name = "grcPartidaVistas"
        Me.grcPartidaVistas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "N�mero Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(16, 44)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(84, 16)
        Me.lblTipoventa.TabIndex = 2
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "&Tipo de venta:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "D"
        Me.cboTipoventa.Location = New System.Drawing.Point(104, 44)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Enabled = False
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contado", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cr�dito", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoventa.TabIndex = 3
        Me.cboTipoventa.Tag = "tipoventa"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 20)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 20)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(360, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnConsultarVenta)
        Me.GroupBox1.Controls.Add(Me.dteFechaVenta)
        Me.GroupBox1.Controls.Add(Me.lbl_eFecha)
        Me.GroupBox1.Controls.Add(Me.lblTipoventa)
        Me.GroupBox1.Controls.Add(Me.lkpCliente)
        Me.GroupBox1.Controls.Add(Me.cboTipoventa)
        Me.GroupBox1.Controls.Add(Me.lblCliente)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 120)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(640, 100)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Factura"
        '
        'btnConsultarVenta
        '
        Me.btnConsultarVenta.Enabled = False
        Me.btnConsultarVenta.Location = New System.Drawing.Point(528, 56)
        Me.btnConsultarVenta.Name = "btnConsultarVenta"
        Me.btnConsultarVenta.Size = New System.Drawing.Size(104, 32)
        Me.btnConsultarVenta.TabIndex = 7
        Me.btnConsultarVenta.Text = "Consultar Venta"
        '
        'dteFechaVenta
        '
        Me.dteFechaVenta.EditValue = "10/04/2006"
        Me.dteFechaVenta.Location = New System.Drawing.Point(104, 70)
        Me.dteFechaVenta.Name = "dteFechaVenta"
        '
        'dteFechaVenta.Properties
        '
        Me.dteFechaVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaVenta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaVenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaVenta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaVenta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaVenta.Properties.Enabled = False
        Me.dteFechaVenta.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaVenta.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaVenta.TabIndex = 6
        Me.dteFechaVenta.Tag = "fecha"
        '
        'lbl_eFecha
        '
        Me.lbl_eFecha.AutoSize = True
        Me.lbl_eFecha.Location = New System.Drawing.Point(23, 70)
        Me.lbl_eFecha.Name = "lbl_eFecha"
        Me.lbl_eFecha.Size = New System.Drawing.Size(77, 16)
        Me.lbl_eFecha.TabIndex = 5
        Me.lbl_eFecha.Tag = ""
        Me.lbl_eFecha.Text = "F&echa Venta:"
        Me.lbl_eFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCancelacion
        '
        Me.dteFechaCancelacion.EditValue = "10/04/2006"
        Me.dteFechaCancelacion.Location = New System.Drawing.Point(544, 34)
        Me.dteFechaCancelacion.Name = "dteFechaCancelacion"
        '
        'dteFechaCancelacion.Properties
        '
        Me.dteFechaCancelacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCancelacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCancelacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCancelacion.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCancelacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCancelacion.Properties.Enabled = False
        Me.dteFechaCancelacion.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaCancelacion.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaCancelacion.TabIndex = 2
        Me.dteFechaCancelacion.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(424, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Tag = ""
        Me.Label2.Text = "F&echa Cancelaci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 254)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Tag = ""
        Me.Label3.Text = "Observaci&ones:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservacionesCancelacion
        '
        Me.txtObservacionesCancelacion.EditValue = ""
        Me.txtObservacionesCancelacion.Location = New System.Drawing.Point(112, 254)
        Me.txtObservacionesCancelacion.Name = "txtObservacionesCancelacion"
        Me.txtObservacionesCancelacion.Size = New System.Drawing.Size(536, 32)
        Me.txtObservacionesCancelacion.TabIndex = 8
        Me.txtObservacionesCancelacion.Tag = ""
        '
        'tmaVentas
        '
        Me.tmaVentas.BackColor = System.Drawing.Color.White
        Me.tmaVentas.CanDelete = False
        Me.tmaVentas.CanInsert = False
        Me.tmaVentas.CanUpdate = False
        Me.tmaVentas.Grid = Me.grVentas
        Me.tmaVentas.Location = New System.Drawing.Point(8, 352)
        Me.tmaVentas.Name = "tmaVentas"
        Me.tmaVentas.Size = New System.Drawing.Size(640, 23)
        Me.tmaVentas.TabIndex = 63
        Me.tmaVentas.TabStop = False
        Me.tmaVentas.Title = "Art�culos"
        Me.tmaVentas.UpdateTitle = "un Art�culo"
        '
        'lblImporteGastosAdministrativos
        '
        Me.lblImporteGastosAdministrativos.AutoSize = True
        Me.lblImporteGastosAdministrativos.Location = New System.Drawing.Point(13, 16)
        Me.lblImporteGastosAdministrativos.Name = "lblImporteGastosAdministrativos"
        Me.lblImporteGastosAdministrativos.Size = New System.Drawing.Size(201, 16)
        Me.lblImporteGastosAdministrativos.TabIndex = 6
        Me.lblImporteGastosAdministrativos.Tag = ""
        Me.lblImporteGastosAdministrativos.Text = "Importe por Gastos Administrativos"
        Me.lblImporteGastosAdministrativos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporteGastosAdministrativos
        '
        Me.clcImporteGastosAdministrativos.EditValue = "0"
        Me.clcImporteGastosAdministrativos.Location = New System.Drawing.Point(221, 13)
        Me.clcImporteGastosAdministrativos.MaxValue = 0
        Me.clcImporteGastosAdministrativos.MinValue = 0
        Me.clcImporteGastosAdministrativos.Name = "clcImporteGastosAdministrativos"
        '
        'clcImporteGastosAdministrativos.Properties
        '
        Me.clcImporteGastosAdministrativos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteGastosAdministrativos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteGastosAdministrativos.Properties.Enabled = False
        Me.clcImporteGastosAdministrativos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporteGastosAdministrativos.Properties.Precision = 2
        Me.clcImporteGastosAdministrativos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteGastosAdministrativos.Size = New System.Drawing.Size(75, 19)
        Me.clcImporteGastosAdministrativos.TabIndex = 7
        Me.clcImporteGastosAdministrativos.Tag = "importe"
        '
        'gbxGastosAdministrativos
        '
        Me.gbxGastosAdministrativos.Controls.Add(Me.clcImporteGastosAdministrativos)
        Me.gbxGastosAdministrativos.Controls.Add(Me.lblImporteGastosAdministrativos)
        Me.gbxGastosAdministrativos.Controls.Add(Me.Label4)
        Me.gbxGastosAdministrativos.Controls.Add(Me.txtObservacionesAdministrativas)
        Me.gbxGastosAdministrativos.Location = New System.Drawing.Point(8, 286)
        Me.gbxGastosAdministrativos.Name = "gbxGastosAdministrativos"
        Me.gbxGastosAdministrativos.Size = New System.Drawing.Size(640, 58)
        Me.gbxGastosAdministrativos.TabIndex = 9
        Me.gbxGastosAdministrativos.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(312, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 16)
        Me.Label4.TabIndex = 65
        Me.Label4.Tag = ""
        Me.Label4.Text = "Observaci&ones:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservacionesAdministrativas
        '
        Me.txtObservacionesAdministrativas.EditValue = ""
        Me.txtObservacionesAdministrativas.Location = New System.Drawing.Point(408, 9)
        Me.txtObservacionesAdministrativas.Name = "txtObservacionesAdministrativas"
        '
        'txtObservacionesAdministrativas.Properties
        '
        Me.txtObservacionesAdministrativas.Properties.Enabled = False
        Me.txtObservacionesAdministrativas.Size = New System.Drawing.Size(224, 40)
        Me.txtObservacionesAdministrativas.TabIndex = 66
        Me.txtObservacionesAdministrativas.Tag = ""
        '
        'chkNoValeReembolso
        '
        Me.chkNoValeReembolso.Location = New System.Drawing.Point(200, 34)
        Me.chkNoValeReembolso.Name = "chkNoValeReembolso"
        '
        'chkNoValeReembolso.Properties
        '
        Me.chkNoValeReembolso.Properties.Caption = "No Generar Vale de Reembolso"
        Me.chkNoValeReembolso.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoValeReembolso.Size = New System.Drawing.Size(192, 19)
        Me.chkNoValeReembolso.TabIndex = 0
        Me.chkNoValeReembolso.Tag = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 230)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Tag = ""
        Me.Label6.Text = "Cancelado por:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpOpcionCancelacion
        '
        Me.lkpOpcionCancelacion.AllowAdd = False
        Me.lkpOpcionCancelacion.AutoReaload = False
        Me.lkpOpcionCancelacion.DataSource = Nothing
        Me.lkpOpcionCancelacion.DefaultSearchField = ""
        Me.lkpOpcionCancelacion.DisplayMember = "descripcion"
        Me.lkpOpcionCancelacion.EditValue = Nothing
        Me.lkpOpcionCancelacion.Filtered = False
        Me.lkpOpcionCancelacion.InitValue = Nothing
        Me.lkpOpcionCancelacion.Location = New System.Drawing.Point(112, 230)
        Me.lkpOpcionCancelacion.MultiSelect = False
        Me.lkpOpcionCancelacion.Name = "lkpOpcionCancelacion"
        Me.lkpOpcionCancelacion.NullText = ""
        Me.lkpOpcionCancelacion.PopupWidth = CType(400, Long)
        Me.lkpOpcionCancelacion.ReadOnlyControl = False
        Me.lkpOpcionCancelacion.Required = False
        Me.lkpOpcionCancelacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpOpcionCancelacion.SearchMember = ""
        Me.lkpOpcionCancelacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpOpcionCancelacion.SelectAll = False
        Me.lkpOpcionCancelacion.Size = New System.Drawing.Size(536, 20)
        Me.lkpOpcionCancelacion.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpOpcionCancelacion.TabIndex = 6
        Me.lkpOpcionCancelacion.Tag = "opcion"
        Me.lkpOpcionCancelacion.ToolTip = Nothing
        Me.lkpOpcionCancelacion.ValueMember = "opcion"
        '
        'frmCancelacionesVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 476)
        Me.Controls.Add(Me.lkpOpcionCancelacion)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.chkNoValeReembolso)
        Me.Controls.Add(Me.txtObservacionesCancelacion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dteFechaCancelacion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grVentas)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gpbFactura)
        Me.Controls.Add(Me.tmaVentas)
        Me.Controls.Add(Me.gbxGastosAdministrativos)
        Me.Name = "frmCancelacionesVentas"
        Me.Text = "frmCancelacionesVentas"
        Me.Controls.SetChildIndex(Me.gbxGastosAdministrativos, 0)
        Me.Controls.SetChildIndex(Me.tmaVentas, 0)
        Me.Controls.SetChildIndex(Me.gpbFactura, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.grVentas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.dteFechaCancelacion, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.txtObservacionesCancelacion, 0)
        Me.Controls.SetChildIndex(Me.chkNoValeReembolso, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lkpOpcionCancelacion, 0)
        Me.gpbFactura.ResumeLayout(False)
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFechaVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteGastosAdministrativos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGastosAdministrativos.ResumeLayout(False)
        CType(Me.txtObservacionesAdministrativas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoValeReembolso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oHistoricoCostos As VillarrealBusiness.clsHisCostos
    Private oReportes As VillarrealBusiness.Reportes
    Private oVales As VillarrealBusiness.clsVales
    Private oValesDetalle As VillarrealBusiness.clsValesDetalle
    Private oOpcionesCancelacion As VillarrealBusiness.clsOpcionesCancelacion


    Public ConRegistro As Boolean
    Private GeneroValeGastosAdministrativos As Boolean = False
    Private GeneroNotaCargoGastosAdministrativos As Boolean = False

    Private folio_movimiento As Long = 0
    Private folio_movimiento_detalle As Long = 0
    Private arrBodegas As New ArrayList
    Private arrFoliosMovimientos As New ArrayList
    Private dFactor_Enajenacion As Double = 0
    Private dsDocumentosCobrar As DataSet

    Private ConceptoNCR As String
    Private ConceptoCancelacionVenta As String
    Private ConceptoFactura As String
    Private ConceptoFacturaCredito As String
    Private folio_cobrar As Long = 0
    Private folio_notacredito_abonos As Long = 0

    Private ConceptoGastosAdministrativos As String = ""
    Private ImporteGastosAdministrativos As Double = 0


    Private Cadena_Error_Cancelacion As String = ""

    Private banConceptoCancelacionVenta As Boolean = False
    Private banConceptoFactura As Boolean = False
    Private banConceptoFacturaCredito As Boolean = False
    Private banConceptoNCR As Boolean = False
    Private banConceptoNCGastosAdministrativos As Boolean = False
    Private banImporteGastosAdministrativos As Boolean = False

    Private factura_electronica As Boolean = False
    Private SerieNC As String = ""
    Private SerieNCargo As String = ""


    Private articulos_factura As DataTable

    Private folio_nc_gastos_administrativos As Long = 0
    Private folio_vale_gastos_administrativos As Long = 0
    Private folio_vale_factura As Long = 0
    Private tipo_vale_cancelacion As Long = 0
    Private folio_factura_electronica As Long = 0


    Public ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Public ReadOnly Property FolioVenta() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Public ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
    Public ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property SerieNotacargo() As String
        Get
            Return Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)
        End Get
    End Property

    Private ReadOnly Property TipoValeCancelacion() As Long
        Get
            Return Comunes.clsUtilerias.uti_ObtenerTipoValeCancelacion()
        End Get
    End Property
    Public ReadOnly Property OpcionCancelacion() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpOpcionCancelacion)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"

    Private Sub frmCancelacionesVentas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCancelacionesVentas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCancelacionesVentas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        If ConRegistro = True Then
            Dim response As New Events

            If Me.factura_electronica = False Then
                ImprimeNotaDeCredito(Comunes.Common.Sucursal_Actual, 52, Me.SerieNC, folio_cobrar, Cliente)
                System.Threading.Thread.Sleep(10000)
            End If
           
            ImprimirReporteMovimiento()

            If GeneroValeGastosAdministrativos And Me.factura_electronica = False Then
                System.Threading.Thread.Sleep(10000)
                ImprimirNotaCargo(ConceptoGastosAdministrativos, Me.SerieNCargo, 0, folio_nc_gastos_administrativos)
            End If

            
            If GeneroValeGastosAdministrativos Then
                System.Threading.Thread.Sleep(10000)
                ImprimirVale(folio_vale_gastos_administrativos)
            End If

            If folio_vale_factura > 0 Then
                System.Threading.Thread.Sleep(10000)
                ImprimirVale(folio_vale_factura)
            End If

            If folio_notacredito_abonos > 0 And Me.factura_electronica = False Then
                System.Threading.Thread.Sleep(10000)
                ImprimeNotaDeCredito(Comunes.Common.Sucursal_Actual, 63, Me.SerieNC, folio_notacredito_abonos, Cliente)
            End If
        End If


        Dipros.Utils.Common.ShowMessage(MessageType.MsgInformation, "La Venta Fue Cancelada Correctamente", Me.Title)
    End Sub

    Private Sub frmCancelacionesVentas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oClientes = New VillarrealBusiness.clsClientes
        oVariables = New VillarrealBusiness.clsVariables
        oHistoricoCostos = New VillarrealBusiness.clsHisCostos
        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oOpcionesCancelacion = New VillarrealBusiness.clsOpcionesCancelacion

        ' =========  codigo para gastos administrativos ==================
        Me.lblImporteGastosAdministrativos.Visible = ConRegistro
        Me.clcImporteGastosAdministrativos.Enabled = ConRegistro
        Me.clcImporteGastosAdministrativos.Visible = ConRegistro
        Me.gbxGastosAdministrativos.Enabled = ConRegistro
        Me.gbxGastosAdministrativos.Visible = ConRegistro
        Me.chkNoValeReembolso.Enabled = ConRegistro
        Me.chkNoValeReembolso.Visible = ConRegistro


        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet
        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieNC = CType(oDataSet.Tables(0).Rows(0).Item("serie_nota_credito_electronica"), String)
        SerieNCargo = CType(oDataSet.Tables(0).Rows(0).Item("serie_nota_cargo_electronica"), String)
        '-----------------------------


        If ConRegistro Then
            Me.txtObservacionesCancelacion.Size = New Size(536, 48)
            TraeImporteGastosAdministrativos()
            Me.clcImporteGastosAdministrativos.EditValue = Me.ImporteGastosAdministrativos

        Else
            Me.txtObservacionesCancelacion.Size = New Size(536, 99)
        End If

        ' ==================================================


        Me.dteFechaCancelacion.DateTime = CDate(TINApp.FechaServidor)

        ConfiguraMaster()

        TraeConceptoFactura()        'Contado
        TraeConceptoFacturaCredito() 'Credito

        tipo_vale_cancelacion = Me.TipoValeCancelacion()
    End Sub
    Private Sub frmCancelacionesVentas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If ConRegistro = False Then
            Response = Me.EliminarVenta(Me.txtObservacionesCancelacion.Text)
        Else
            Response = Me.CancelarVenta(Me.txtObservacionesCancelacion.Text)
        End If
    End Sub
    Private Sub frmCancelacionesVentas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        Response = oVentas.ValidaCancelacionVenta(sucursal, serie, FolioVenta, ConRegistro, Me.dteFechaVenta.DateTime, Me.dteFechaCancelacion.DateTime, Me.OpcionCancelacion, tipo_vale_cancelacion)

        If Response.ErrorFound Then Exit Sub

        If Me.ConRegistro = False Then
            If Not ValidoRecuperacionesRepartos() Then
                Response.Message = "Hay Mercancia en Reparto o Aun no se Termina su Recuperaci�n"
                Exit Sub
            End If
        Else
            If (Me.clcImporteGastosAdministrativos.EditValue < Me.ImporteGastosAdministrativos) And Me.txtObservacionesAdministrativas.Text.Trim.Length = 0 Then
                Response.Message = "Las observaciones de la nota de cargo son requeridas"
            End If
        End If

    End Sub
#End Region

#Region "Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        'Select Case Action
        '    Case Actions.Insert
        lkpFolio_LoadData(True)
        'End Select
    End Sub

    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal, serie, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio)
    End Sub
    Private Sub lkpFolio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolio.EditValueChanged
        If Me.FolioVenta > 0 Then
            Me.dteFechaVenta.DateTime = Me.lkpFolio.GetValue("fecha")
            Me.lkpCliente.EditValue = Me.lkpFolio.GetValue("cliente")
            Me.cboTipoventa.Value = Me.lkpFolio.GetValue("tipoventa")
            Me.folio_factura_electronica = Me.lkpFolio.GetValue("folio_factura_electronica")
            If CType(Me.lkpFolio.GetValue("enajenacion"), Boolean) = True Then
                dFactor_Enajenacion = CType(Me.lkpFolio.GetValue("factor_enajenacion"), Double)
            End If
            Me.btnConsultarVenta.Enabled = True
        Else
            Me.lkpCliente.EditValue = Nothing
            Me.btnConsultarVenta.Enabled = False
        End If
        TraerDatosDetalle()
    End Sub

    Private Sub lkpOpcionCancelacion_LoadData(ByVal Initialize As Boolean) Handles lkpOpcionCancelacion.LoadData
        Dim response As Events
        response = oOpcionesCancelacion.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpOpcionCancelacion.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpOpcionCancelacion_Format() Handles lkpOpcionCancelacion.Format
        Comunes.clsFormato.for_opciones_cancelacion_grl(Me.lkpOpcionCancelacion)
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupCliente() '(sucursal_dependencia)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If
        Response = Nothing

    End Sub
    Private Sub btnConsultarVenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultarVenta.Click
        Dim oForm As New Comunes.frmVentas
        With oForm
            .Title = "Consulta de Ventas"
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Action = Actions.Update
            .lSucursal = Me.sucursal
            .sSerie = Me.serie
            .lFolio = Me.FolioVenta
            .Show()
            Me.Enabled = False
        End With
    End Sub
    Private Sub clcImporteGastosAdministrativos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporteGastosAdministrativos.Validated
        If Me.clcImporteGastosAdministrativos.IsLoading Then Exit Sub

        If Not IsNumeric(Me.clcImporteGastosAdministrativos.EditValue) Then Exit Sub


        If (Me.clcImporteGastosAdministrativos.EditValue < Me.ImporteGastosAdministrativos) Then
            Me.txtObservacionesAdministrativas.Enabled = True

        Else
            Me.txtObservacionesAdministrativas.Enabled = False
            Me.txtObservacionesAdministrativas.Text = ""
        End If
    End Sub
#End Region

#Region "Funcionalidad"
    Private Sub ConfiguraMaster()

        With Me.tmaVentas
            .UpdateTitle = "un Art�culo"
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("preciounitario", "System.Double")
            .AddColumn("total", "System.Double")
            .AddColumn("sobrepedido", "System.Boolean")
            .AddColumn("costo", "System.Double")    'oculto
            .AddColumn("surtido", "System.Boolean") 'oculto
            .AddColumn("reparto", "System.Boolean")
            .AddColumn("bodega")
            .AddColumn("n_bodega")
            .AddColumn("precio_minimo", "System.Double")    'oculto
            .AddColumn("folio_historico_costo", "System.Int32")
            .AddColumn("articulo_regalo", "System.Boolean")
            .AddColumn("precio_contado", "System.Double")
            .AddColumn("modelo")
            .AddColumn("folio_autorizacion", "System.Int32")
            .AddColumn("partida_vistas", "System.Int32")
            .AddColumn("numero_serie")
        End With


    End Sub
    Private Sub TraerDatosDetalle()
        Dim response As Events
        Dim odataset As DataSet

        response = oVentasDetalle.Listado(sucursal, serie, FolioVenta)
        If Not response.ErrorFound Then
            odataset = response.Value
            articulos_factura = odataset.Tables(0)
            Me.tmaVentas.DataSource = odataset
        End If
    End Sub
    Public Function ValidoRecuperacionesRepartos() As Boolean
        Dim response As Events
        Dim odataset As DataSet
        Dim respuesta As Boolean = True

        response = oVentas.ValidaRecuperacionesRepartos(sucursal, serie, FolioVenta)
        If Not response.ErrorFound Then
            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then respuesta = odataset.Tables(0).Rows(0)(0)
        End If
        odataset = Nothing
        ValidoRecuperacionesRepartos = respuesta
    End Function

    Private Sub TraeConceptoGastosAdministrativos()

        ConceptoGastosAdministrativos = CType(oVariables.TraeDatos("concepto_gastos_administrativos", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoGastosAdministrativos <> "" Then

            banConceptoNCGastosAdministrativos = True

        Else
            Cadena_Error_Cancelacion = "El Concepto de Gastos Administrativos no esta definido"
        End If

    End Sub
    Private Sub TraeImporteGastosAdministrativos()

        ImporteGastosAdministrativos = CType(oVariables.TraeDatos("importe_gastos_administrativos", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
        If ImporteGastosAdministrativos <> 0 Then

            banImporteGastosAdministrativos = True

        Else
            Cadena_Error_Cancelacion = "El Importe de Gastos Administrativos no esta definido"
        End If

    End Sub


    Private Sub TraeConceptoNCR()

        ConceptoNCR = CType(oVariables.TraeDatos("concepto_cxc_nota_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoNCR <> "" Then

            banConceptoNCR = True

        Else

            Cadena_Error_Cancelacion = "El Concepto de Cancelaci�n de la Venta no esta definido"

        End If

    End Sub
    Private Sub TraeConceptoCancelacionVenta()

        ConceptoCancelacionVenta = CType(oVariables.TraeDatos("concepto_cancelacion_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoCancelacionVenta <> "" Then

            banConceptoCancelacionVenta = True

        Else

            Cadena_Error_Cancelacion = "El Concepto de Cancelaci�n de la Venta no esta definido"

        End If

    End Sub
   
    Private Sub TraeConceptoFactura()

        ConceptoFactura = CType(oVariables.TraeDatos("concepto_cxc_factura_contado", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoFactura <> "" Then

            banConceptoFactura = True

        Else

            ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Contado no esta definido", "Variables del Sistema", Nothing, False)

        End If

    End Sub
    Private Sub TraeConceptoFacturaCredito()

        ConceptoFacturaCredito = CType(oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoFacturaCredito <> "" Then

            banConceptoFacturaCredito = True

        Else

            ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)

        End If

    End Sub
    Private Sub GeneraListaBodegas()

        arrBodegas.Clear()
        Dim i As Integer

        For i = 0 To Me.grvVentas.RowCount - 1

            If arrBodegas.Contains(Me.grvVentas.GetRowCellValue(i, Me.grcBodega)) = False Then

                arrBodegas.Add(Me.grvVentas.GetRowCellValue(i, Me.grcBodega))

            End If

        Next

        arrBodegas.Sort()

    End Sub
    Private Function GeneraMovimientoInventarioCancelacion(ByRef response As Events, ByRef folio As Long, ByVal concepto As String, ByVal bodega As String)

        response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, bodega, concepto, folio, _
                    Me.dteFechaCancelacion.DateTime, sucursal, serie, FolioVenta, "Movimiento generado desde el proceso de Cancelaci�n de Ventas")

    End Function


    Private Function NumeroDocumentos(ByVal importe As Double) As Long

        Dim i As Long
        Dim importe_documento As Double
        For i = 0 To dsDocumentosCobrar.Tables(0).Rows.Count - 1

            If importe > dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo") Then

                importe_documento = dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo")

            Else

                importe_documento = importe

            End If

            importe = importe - importe_documento
            If importe = 0 Then Exit For

        Next

        Return i + 1

    End Function
    Private Function EliminarVenta(ByVal observaciones As String) As Events

        Dim response_accept As New Events

        Try


            TraeConceptoCancelacionVenta()
            If banConceptoCancelacionVenta Then

                Dim response As New Events
                Dim concepto_fac As String
                If Me.cboTipoventa.Value = "C" Then 'Credito

                    concepto_fac = ConceptoFacturaCredito
                Else    'Contado

                    concepto_fac = ConceptoFactura

                End If

                'cancela venta y movto_cobrar
                response = oVentas.EliminarVenta(sucursal, serie, FolioVenta, Cliente, concepto_fac, observaciones)

                If Not response.ErrorFound() Then

                    'GUARDA ENCABEZADOS DE CANCELACION EN MOVIMIENTOS
                    folio_movimiento = 0
                    arrFoliosMovimientos.Clear()
                    GeneraListaBodegas()
                    Dim i As Integer = 0
                    For i = 0 To arrBodegas.Count - 1

                        GeneraMovimientoInventarioCancelacion(response, folio_movimiento, ConceptoCancelacionVenta, arrBodegas(i))
                        If response.ErrorFound Then

                            Cadena_Error_Cancelacion = "Error al Generar el Movimiento al Inventario por la Cancelaci�n"
                            Throw New Exception(Cadena_Error_Cancelacion)

                        End If

                        arrFoliosMovimientos.Add(folio_movimiento)
                        folio_movimiento = 0

                    Next

                    'GUARDA DETALLES DE CANCELACION EN MOVIMIENTOS
                    Dim folio_mvto As Long
                    Dim folio_his_costos As Long = -1
                    i = 0

                    With Me.tmaVentas
                        .MoveFirst()
                        Do While Not .EOF

                            For i = 0 To arrBodegas.Count - 1

                                If arrBodegas(i) = .Item("bodega") Then

                                    folio_mvto = arrFoliosMovimientos(i)
                                    Exit For

                                End If

                            Next

                            folio_his_costos = -1

                            'actualiza historico de costos      CType(.Item("costo"), Double) > 0 And
                            'si el costo del articulo a devolver es mayor a 0 Y NO ES SOBREPEDIDO y el folio historico de costos es MAYOR a 0 (no tiene) , se inserta una nueva capa en his_costos
                            If CType(.Item("sobrepedido"), Boolean) = False Or (CType(.Item("sobrepedido"), Boolean) = True And CType(.Item("folio_historico_costo"), Long) > 0) Then
                                'If CType(.Item("costo"), Double) > 0 Then
                                response = oHistoricoCostos.Insertar(folio_his_costos, CDate(TINApp.FechaServidor), CType(.Item("articulo"), Long), CType(.Item("bodega"), String), CType(.Item("cantidad"), Double), CType(.Item("costo"), Double), CType(.Item("cantidad"), Double))
                                If response.ErrorFound Then
                                    Cadena_Error_Cancelacion = "Error al Actualizar el Historico de Costos"
                                    Throw New Exception(Cadena_Error_Cancelacion)
                                End If
                            End If

                            'Validacion para saber si el folio de la capa corresponde con el articulo
                            If folio_his_costos <> -1 Then
                                response = oHistoricoCostos.ExisteFolioArticulo(folio_his_costos, .Item("articulo"))

                                If Not response.ErrorFound Then

                                    Dim filas As Int32
                                    filas = CType(response.Value, Integer)

                                    If filas <= 0 Then
                                        Cadena_Error_Cancelacion = "La capa " + folio_his_costos.ToString + " no corresponde con el articulo: " + CType(.Item("articulo"), String)
                                        Throw New Exception(Cadena_Error_Cancelacion)
                                    End If

                                End If
                            End If



                            response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoCancelacionVenta, folio_mvto, CType(TINApp.FechaServidor, DateTime), .Item("partida"), .Item("articulo"), .Item("cantidad"), .Item("costo"), .Item("total"), folio_his_costos, folio_movimiento_detalle)

                            If Not response.ErrorFound Then
                                response = oMovimientosInventarioDetalle.RecalculaExistenciasCancelacion(.Item("articulo"))
                            End If


                            If response.ErrorFound Then

                                Cadena_Error_Cancelacion = "Error al Generar el Detalle del Movimiento al Inventario por la Cancelaci�n"
                                Throw New Exception(Cadena_Error_Cancelacion)

                            End If



                            folio_movimiento_detalle = 0
                            .MoveNext()
                        Loop

                    End With

                    If Not response.ErrorFound Then
                        response = Me.oMovimientosCobrar.CancelarTimbrado(serie, FolioVenta, folio_factura_electronica)

                        If response.ErrorFound Then

                            Cadena_Error_Cancelacion = "Error cancelar el timbrado"
                            Throw New Exception(Cadena_Error_Cancelacion)

                        End If
                    End If



                Else

                    Throw New Exception("Error al Eliminar la Venta")

                End If

            Else

                Throw New Exception(Cadena_Error_Cancelacion)

            End If


        Catch ex As Exception

            response_accept.Ex = ex
            response_accept.Message = ex.Message

        End Try
        Return response_accept
    End Function
    Private Function CancelarVenta(ByVal observaciones As String) As Events

        Dim response_accept As New Events
        oVales = New VillarrealBusiness.clsVales
        oValesDetalle = New VillarrealBusiness.clsValesDetalle

        Try

            TraeConceptoCancelacionVenta()
            TraeConceptoGastosAdministrativos()


            If banConceptoCancelacionVenta And banConceptoNCGastosAdministrativos Then

                Dim response As New Events

                Dim concepto_fac As String
                Dim subtotal_cobrar As Double
                Dim iva_cobrar As Double
                Dim impuesto_cobrar As Double

                Dim ImporteGastosAdministrativosCapturado As Double = Me.clcImporteGastosAdministrativos.EditValue


                If Not factura_electronica Then
                    SerieNC = Comunes.clsUtilerias.uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
                    SerieNCargo = SerieNotacargo
                End If



                impuesto_cobrar = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)


                If Me.cboTipoventa.Value = "C" Then 'Credito
                    concepto_fac = ConceptoFacturaCredito
                Else    'Contado
                    concepto_fac = ConceptoFactura
                End If

                '------------------------NC GASTOS ADMINISTRATIVOS ------------------------

                ' Si el importe capturado es menor al importe configurado de la nota de cargo
                ' se inserta un registro en la tabla doc_ventas_cancelaciones
                If (ImporteGastosAdministrativosCapturado < Me.ImporteGastosAdministrativos) Then


                    response = oVentas.VentasCancelaciones(Me.dteFechaCancelacion.DateTime, sucursal, serie, FolioVenta, Me.txtObservacionesAdministrativas.Text, ImporteGastosAdministrativosCapturado)
                    If response.ErrorFound Then

                        Throw New Exception(response.Ex.Message)
                        Exit Function
                    End If

                End If


                ' si el importe capturado del cargo administrativo es mayor a 0 se genera el movimiento
                If ImporteGastosAdministrativosCapturado > 0 Then


                    ' se inserta un movimiento por cobrar para la nota de cargo por gastos administrativos
                    impuesto_cobrar = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
                    If impuesto_cobrar <> -1 Then

                        subtotal_cobrar = ImporteGastosAdministrativosCapturado / (1 + (impuesto_cobrar / 100))
                        iva_cobrar = ImporteGastosAdministrativosCapturado - subtotal_cobrar
                    Else
                        ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
                        Exit Function
                    End If

                    response = oMovimientosCobrar.InsertarCajas(sucursal, Me.ConceptoGastosAdministrativos, Me.SerieNCargo, Cliente, 0, Me.dteFechaCancelacion.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, ImporteGastosAdministrativosCapturado, 0, subtotal_cobrar, iva_cobrar, ImporteGastosAdministrativosCapturado, ImporteGastosAdministrativosCapturado, System.DBNull.Value, "Nota de Cargo por Gastos Administrativos", "", sucursal, folio_nc_gastos_administrativos, False)
                    If response.ErrorFound Then

                        Throw New Exception(response.Ex.Message)
                        Exit Function
                    End If


                    response = oMovimientosCobrarDetalle.Insertar(sucursal, ConceptoGastosAdministrativos, SerieNCargo, folio_nc_gastos_administrativos, Cliente, 1, 0, Me.dteFechaCancelacion.DateTime.Date, ImporteGastosAdministrativosCapturado, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, 0, Me.dteFechaCancelacion.DateTime.Date, ImporteGastosAdministrativosCapturado, "", observaciones, 0)
                    If response.ErrorFound Then

                        Throw New Exception(response.Ex.Message)
                        Exit Function
                    End If

                    Me.GeneroNotaCargoGastosAdministrativos = True
                End If

                '--------------- Vale por NC Gastos Administrativos y de Abonos de la Factura --------------
                Dim ImporteAbonosFactura As Double = 0



                response = oVentas.ObtenerAbonosFactura(sucursal, serie, FolioVenta)

                If Not response.ErrorFound Then
                    ImporteAbonosFactura = CType(response.Value, DataSet).Tables(0).Rows(0).Item("Importe")
                Else
                    Throw New Exception(response.Message)
                    Exit Function
                End If


                If ImporteAbonosFactura >= ImporteGastosAdministrativosCapturado Then
                    If ImporteGastosAdministrativosCapturado > 0 Then
                        folio_vale_gastos_administrativos = GeneraValeGastoAdministrativo(ImporteGastosAdministrativosCapturado)
                        GeneroValeGastosAdministrativos = True
                    Else
                        GeneroValeGastosAdministrativos = False
                        folio_vale_gastos_administrativos = 0
                    End If
                Else
                    folio_vale_gastos_administrativos = GeneraValeGastoAdministrativo(ImporteAbonosFactura)
                End If

                Dim DiferenciaImporte As Double = ImporteAbonosFactura - ImporteGastosAdministrativosCapturado

                If DiferenciaImporte > 0 And Me.chkNoValeReembolso.Checked = False Then
                    folio_vale_factura = GeneraValeAbonosFactura(DiferenciaImporte)
                End If




                '----------------------------------NCR-------------------------------------
                'obtengo concepto de ncr
                TraeConceptoNCR()
                If Not banConceptoNCR = True Then
                    Throw New Exception(Cadena_Error_Cancelacion)
                End If

                'obtengo el saldo , subtotal e iva de la factura
                Dim saldo As Double
                Dim importe_ncr As Double
                'Dim folio_cobrar As Long = 0
                Dim numero_documentos As Long
                Dim sObservaciones As String

                subtotal_cobrar = 0
                iva_cobrar = 0


                response = oMovimientosCobrar.DespliegaDatos(sucursal, concepto_fac, serie, FolioVenta, Cliente)
                saldo = CType(CType(response.Value, DataSet).Tables(0).Rows(0).Item("saldo"), Double)
                importe_ncr = saldo


                If impuesto_cobrar <> -1 Then
                    iva_cobrar = saldo * (impuesto_cobrar / 100)
                Else
                    Cadena_Error_Cancelacion = "El Porcentaje de Impuesto no esta definido"
                    Throw New Exception(Cadena_Error_Cancelacion)
                End If

                subtotal_cobrar = saldo - iva_cobrar

                'obtengo documentos del movto cobrar
                If Not response.ErrorFound Then response = oMovimientosCobrarDetalle.DocumentosMovimiento(sucursal, concepto_fac, serie, FolioVenta, Cliente)
                If Not response.ErrorFound Then
                    dsDocumentosCobrar = CType(response.Value, DataSet)
                    numero_documentos = NumeroDocumentos(saldo)
                End If

                'genero ncr
                If Not response.ErrorFound Then response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, 52, Me.SerieNC, Cliente, 0, _
                                                        CDate(TINApp.FechaServidor), Comunes.Common.Caja_Actual, -1, System.DBNull.Value, numero_documentos, 0, saldo, subtotal_cobrar, iva_cobrar, _
                                                         saldo, 0, System.DBNull.Value, "Cancelaci�n de Venta", "", Comunes.Common.Sucursal_Actual, folio_cobrar)


                'genero documentos de ncr
                If Not response.ErrorFound Then
                    Dim i As Integer = 0
                    Dim importe_documento As Double
                    Dim dImporte_proporcional_costo As Double = 0
                    Dim dIva As Double = 1 + (impuesto_cobrar / 100)


                    For i = 0 To dsDocumentosCobrar.Tables(0).Rows.Count - 1
                        If saldo > dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo") Then
                            importe_documento = dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo")
                        Else
                            importe_documento = saldo
                        End If


                        'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                        dImporte_proporcional_costo = (importe_documento / dIva) * dFactor_Enajenacion
                        'Se multiplica por -1 cuando el tipo de movimiento es un Abono
                        dImporte_proporcional_costo = dImporte_proporcional_costo * -1

                        'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
                        sObservaciones = "Cancelaci�n de la Venta " + SerieNC + "-" + folio_cobrar.ToString
                        If Not response.ErrorFound Then      ' ConceptoNCR antes se ponia este concepto ahora estara fijo el 52
                            response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, 52, SerieNC, folio_cobrar, Cliente, _
                                                             i + 1, 0, CDate(TINApp.FechaServidor), importe_documento, sucursal, concepto_fac, serie, FolioVenta, Cliente, dsDocumentosCobrar.Tables(0).Rows(i).Item("documento"), 0, System.DBNull.Value, 0, "", sObservaciones, dImporte_proporcional_costo)

                            If response.ErrorFound Then
                                Cadena_Error_Cancelacion = "Error  Cancelacion de Nota de credito"
                                Throw New Exception(Cadena_Error_Cancelacion)
                            End If

                        Else
                            Cadena_Error_Cancelacion = "Error al Generar el Detalle de la Nota de Credito"
                            Throw New Exception(Cadena_Error_Cancelacion)
                        End If
                        saldo -= importe_documento
                        If saldo = 0 Then Exit For
                    Next
                Else
                    Cadena_Error_Cancelacion = "Error al Generar la Nota de Credito"
                    Throw New Exception(Cadena_Error_Cancelacion)
                End If
                '--------------------------------------------------------------------------

                observaciones = observaciones + " (" + Comunes.Common.Sucursal_Actual.ToString + "-" + ConceptoNCR.ToString + "-" + SerieNC + "-" + folio_cobrar.ToString + ")"

                'cancelo venta
                If Not response.ErrorFound Then response = oVentas.CancelarVenta(sucursal, serie, FolioVenta, Cliente, concepto_fac, observaciones, importe_ncr, Me.OpcionCancelacion)

                If Not response.ErrorFound() Then
                    'GUARDA ENCABEZADOS DE CANCELACION EN MOVIMIENTOS
                    folio_movimiento = 0
                    arrFoliosMovimientos.Clear()
                    GeneraListaBodegas()
                    Dim i As Integer = 0
                    For i = 0 To arrBodegas.Count - 1
                        GeneraMovimientoInventarioCancelacion(response, folio_movimiento, ConceptoCancelacionVenta, arrBodegas(i))
                        If response.ErrorFound Then
                            Cadena_Error_Cancelacion = "Error al Generar el Movimiento al Inventario por la Cancelaci�n"
                            Throw New Exception(Cadena_Error_Cancelacion)
                        End If
                        arrFoliosMovimientos.Add(folio_movimiento)
                        folio_movimiento = 0
                    Next

                    'GUARDA DETALLES DE CANCELACION EN MOVIMIENTOS
                    Dim folio_mvto As Long
                    Dim folio_his_costos As Long = -1
                    i = 0
                    With Me.tmaVentas
                        .MoveFirst()
                        Do While Not .EOF
                            For i = 0 To arrBodegas.Count - 1
                                If arrBodegas(i) = .Item("bodega") Then
                                    folio_mvto = arrFoliosMovimientos(i)
                                    Exit For
                                End If
                            Next

                            folio_his_costos = -1
                            'actualiza historico de costos CType(.Item("costo"), Double) > 0 And 
                            'si el costo del articulo a devolver es mayor a 0 Y NO ES SOBREPEDIDO y el folio historico de costos es MAYOR a 0 (no tiene) , se inserta una nueva capa en his_costos
                            If CType(.Item("sobrepedido"), Boolean) = False Or (CType(.Item("sobrepedido"), Boolean) = True And CType(.Item("folio_historico_costo"), Long) > 0) Then
                                response = oHistoricoCostos.Insertar(folio_his_costos, CDate(TINApp.FechaServidor), CType(.Item("articulo"), Long), CType(.Item("bodega"), String), CType(.Item("cantidad"), Double), CType(.Item("costo"), Double), CType(.Item("cantidad"), Double))
                                If response.ErrorFound Then
                                    Cadena_Error_Cancelacion = "Error al Actualizar el Historico de Costos"
                                    Throw New Exception(Cadena_Error_Cancelacion)
                                End If
                            End If

                            'Validacion para saber si el folio de la capa corresponde con el articulo
                            If folio_his_costos <> -1 Then
                                response = oHistoricoCostos.ExisteFolioArticulo(folio_his_costos, .Item("articulo"))

                                If Not response.ErrorFound Then

                                    Dim filas As Int32
                                    filas = CType(response.Value, Integer)

                                    If filas <= 0 Then
                                        Cadena_Error_Cancelacion = "La capa " + folio_his_costos.ToString + " no corresponde con el articulo: " + CType(.Item("articulo"), String)
                                        Throw New Exception(Cadena_Error_Cancelacion)
                                    End If

                                End If
                            End If



                            response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoCancelacionVenta, folio_mvto, CType(TINApp.FechaServidor, DateTime), .Item("partida"), .Item("articulo"), .Item("cantidad"), .Item("costo"), .Item("total"), folio_his_costos, folio_movimiento_detalle)

                            If Not response.ErrorFound Then
                                response = oMovimientosInventarioDetalle.RecalculaExistenciasCancelacion(.Item("articulo"))
                            End If

                            If response.ErrorFound Then
                                Cadena_Error_Cancelacion = "Error al Generar el Detalle del Movimiento al Inventario por la Cancelaci�n"
                                Throw New Exception(Cadena_Error_Cancelacion)
                            End If


                            folio_movimiento_detalle = 0
                            .MoveNext()
                        Loop
                    End With


                    If Not response.ErrorFound Then

                        response = GeneraMovimiento63(concepto_fac, SerieNC, impuesto_cobrar, ImporteAbonosFactura, 0)
                        If response.ErrorFound Then
                            Cadena_Error_Cancelacion = "Error al Generar la Nota de Cr�dito 63 por la Cancelaci�n"
                            Throw New Exception(Cadena_Error_Cancelacion)
                        Else
                            response = Timbrados(Me.GeneroNotaCargoGastosAdministrativos, Me.ConceptoGastosAdministrativos, Me.folio_nc_gastos_administrativos, folio_cobrar, folio_notacredito_abonos)

                        End If


                    End If

                Else
                    Throw New Exception("Error al Cancelar la Venta")
                End If


            Else
                Throw New Exception(Cadena_Error_Cancelacion)

            End If


        Catch ex As Exception

            response_accept.Ex = ex
            response_accept.Message = ex.Message

        End Try

        Return response_accept

    End Function
    Private Function GeneraMovimiento63(ByVal concepto_fac As String, ByVal serie_nota_credito As String, ByVal impuesto_cobrar As Double, ByVal Total_Abonos As Double, ByVal dImporte_proporcional_costo As Double) As Events
        Dim response As New Events

        Dim iva_abonos As Double = 0
        Dim subtotal_abonos As Double = 0
        Dim observaciones As String = "CREDITO POR REEMBOLSO DE CANCELACION FACTURA "

        observaciones = observaciones + Me.serie + "-" + Me.FolioVenta.ToString()


        iva_abonos = Total_Abonos * (impuesto_cobrar / 100)
        subtotal_abonos = Total_Abonos - iva_abonos

        response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, "63", serie_nota_credito, Cliente, 1, Me.dteFechaCancelacion.DateTime, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, 0, Total_Abonos, subtotal_abonos, iva_abonos, Total_Abonos, 0, System.DBNull.Value, observaciones, "", Comunes.Common.Sucursal_Actual, folio_notacredito_abonos)

        If Not response.ErrorFound Then
            response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, "63", serie_nota_credito, folio_notacredito_abonos, Cliente, 1, 1, Me.dteFechaCancelacion.DateTime, Total_Abonos, sucursal, concepto_fac, Me.serie, Me.FolioVenta, -1, -1, 0, System.DBNull.Value, 0, "", "Nota Credito por Abonos Cancelados", dImporte_proporcional_costo)
        End If

        Return response
    End Function
    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New Comunes.rptNotaDeCredito

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                    TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito " & CType(folio, String), oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Function ImprimirReporteMovimiento()
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        response = oReportes.MovimientoInventarioCancelacionVentas(Comunes.Common.Sucursal_Actual, ConceptoCancelacionVenta, sucursal, serie, FolioVenta)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Movimiento al Inventario no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptEntradaAlmacenCancelacionVenta

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Movimiento al Inventario", oReport, True, , , True)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Function
    Private Sub ImprimirNotaCargo(ByVal conceptointereses As String, ByVal serieinteres As String, ByVal saldo_intereses As Double, ByVal foliointeres As Long)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        response = oReportes.ImprimeNotaDeCargo(sucursal, conceptointereses, serieinteres, foliointeres, Cliente, True)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Nota de Cargo no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptNotaCargoDescuentosAnticipados   'Clientes.rptNotaCargo

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo ", oReport)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Function GeneraValeGastoAdministrativo(ByVal dImporteGastosAdministrativos As Double) As Long
        Dim response As New Events
        Dim folio_vale_gastos_administrativos As Long = 0

        response = oVales.Insertar(folio_vale_gastos_administrativos, Me.dteFechaCancelacion.DateTime, Cliente, sucursal, serie, FolioVenta, dImporteGastosAdministrativos, dImporteGastosAdministrativos, "A", DateAdd(DateInterval.Year, 1, Me.dteFechaCancelacion.DateTime), tipo_vale_cancelacion, -1, 0, "Vale por Gastos Administrativos generado por Cancelaci�n", "", "")
        If response.ErrorFound Then

            Throw New Exception(response.Message)
            Exit Function
        End If

        Return folio_vale_gastos_administrativos

    End Function
    Private Function GeneraValeAbonosFactura(ByVal dImporteAbonosFactura As Double) As Long
        Dim response As New Events
        Dim folio_vale_factura As Long = 0


        response = oVales.Insertar(folio_vale_factura, Me.dteFechaCancelacion.DateTime, Cliente, sucursal, serie, FolioVenta, dImporteAbonosFactura, dImporteAbonosFactura, "A", DateAdd(DateInterval.Year, 1, Me.dteFechaCancelacion.DateTime), tipo_vale_cancelacion, -1, 0, "Vale por Abonos de Factura generado por Cancelaci�n", "", "")
        If response.ErrorFound Then
            Throw New Exception(response.Message)
        End If

        Return folio_vale_factura

    End Function
    Private Sub ImprimirVale(ByVal folio As Long)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        response = oReportes.ImprimeVale(folio)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Vale no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptValeFormato

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Impresi�n del Vale ", oReport)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub



#End Region

    Private Function Timbrados(ByVal bGeneroNotaCargoGastosAdministrativos As Boolean, ByVal sConceptoGastosAdministrativos As String, ByVal FolioNotaCargoGastosAdministrativos As Long, ByVal FolioNotaCredito52 As Long, ByVal FolionotaCredito63 As Long) As Events
        Dim response As New Events
        If bGeneroNotaCargoGastosAdministrativos Then
            response = Me.oMovimientosCobrar.LllenarNotaCargoCFDI(Comunes.Common.Sucursal_Actual, sConceptoGastosAdministrativos, Me.SerieNCargo, FolioNotaCargoGastosAdministrativos, Cliente, True, Comunes.Common.Sucursal_Actual)
        End If

        If Not response.ErrorFound Then
            response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Comunes.Common.Sucursal_Actual, 52, Me.SerieNC, FolioNotaCredito52, Cliente, True, Comunes.Common.Sucursal_Actual, True)
        End If

        If Not response.ErrorFound Then
            response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Comunes.Common.Sucursal_Actual, 63, Me.SerieNC, FolionotaCredito63, Cliente, True, Comunes.Common.Sucursal_Actual, True)
        End If

        Return response
    End Function


   
End Class
