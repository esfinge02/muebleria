Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmValesAutorizarCanjePersonaDistinta
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents txtNombre_Autoriza_canjear As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmValesAutorizarCanjePersonaDistinta))
        Me.lblOrden = New System.Windows.Forms.Label
        Me.txtNombre_Autoriza_canjear = New DevExpress.XtraEditors.MemoEdit
        CType(Me.txtNombre_Autoriza_canjear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(202, 28)
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(16, 36)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(301, 17)
        Me.lblOrden.TabIndex = 61
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "Nombre de la Persona Autorizada para Canjear Vale"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre_Autoriza_canjear
        '
        Me.txtNombre_Autoriza_canjear.EditValue = ""
        Me.txtNombre_Autoriza_canjear.Location = New System.Drawing.Point(8, 60)
        Me.txtNombre_Autoriza_canjear.Name = "txtNombre_Autoriza_canjear"
        '
        'txtNombre_Autoriza_canjear.Properties
        '
        Me.txtNombre_Autoriza_canjear.Properties.MaxLength = 100
        Me.txtNombre_Autoriza_canjear.Size = New System.Drawing.Size(328, 60)
        Me.txtNombre_Autoriza_canjear.TabIndex = 63
        Me.txtNombre_Autoriza_canjear.Tag = "nombre_autoriza_canjear"
        '
        'frmValesAutorizarCanjePersonaDistinta
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(346, 132)
        Me.Controls.Add(Me.txtNombre_Autoriza_canjear)
        Me.Controls.Add(Me.lblOrden)
        Me.Name = "frmValesAutorizarCanjePersonaDistinta"
        Me.Text = "Canje de Vale a Persona Distinta"
        Me.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.Controls.SetChildIndex(Me.txtNombre_Autoriza_canjear, 0)
        CType(Me.txtNombre_Autoriza_canjear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public WriteOnly Property NombreAutorizaCanje() As String
        Set(ByVal Value As String)
            Me.txtNombre_Autoriza_canjear.Text = Value
        End Set
    End Property


    Private Sub frmValesAutorizarCanjePersonaDistinta_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Me.OwnerForm.NombreAutorizaCanjear = Me.txtNombre_Autoriza_canjear.Text
    End Sub

    Private Sub frmValesAutorizarCanjePersonaDistinta_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

    End Sub

    Private Sub frmValesAutorizarCanjePersonaDistinta_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = New Events
        If Me.txtNombre_Autoriza_canjear.Text.Trim.Length = 0 Then
            Response.Message = "El nombre de la persona autorizada es requerido"
        End If
    End Sub
End Class
