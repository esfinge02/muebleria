Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptRepartos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphReparto As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfReparto As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lblfolio As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblChofer As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtChofer As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodeguero As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtstatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_reparto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private nombre_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Reparto.rptRepartos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphReparto = CType(Me.Sections("gphReparto"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfReparto = CType(Me.Sections("gpfReparto"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Label29 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Line)
		Me.lblfolio = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Picture)
		Me.Label1 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblChofer = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.gphReparto.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.gphReparto.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.gphReparto.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.gphReparto.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtChofer = CType(Me.gphReparto.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtBodeguero = CType(Me.gphReparto.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtstatus = CType(Me.gphReparto.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.fecha_reparto1 = CType(Me.gphReparto.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Line)
		Me.nombre_cliente1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente2 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente3 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

   
    Private Sub rptRepartos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
