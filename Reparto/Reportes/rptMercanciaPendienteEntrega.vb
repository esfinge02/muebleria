Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptMercanciaPendienteEntrega
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghFolio As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfFolio As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
    Public nombre_empresa As DataDynamics.ActiveReports.TextBox = Nothing
    Public txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Public txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lblfolio As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblFechaVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblDepartamento As DataDynamics.ActiveReports.Label = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private lblGrupo As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioReparto As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha_venta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_bodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private lbldescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblnombre_grupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblNombreDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Reparto.rptMercanciaPendienteEntrega.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghFolio = CType(Me.Sections("ghFolio"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfFolio = CType(Me.Sections("gfFolio"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.lblCliente = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.nombre_empresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Line)
		Me.lblfolio = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Picture)
		Me.lblFechaVenta = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblDepartamento = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblSucursal = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblGrupo = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtFolioReparto = CType(Me.ghFolio.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente = CType(Me.ghFolio.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha_venta = CType(Me.ghFolio.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_bodega = CType(Me.ghFolio.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_sucursal = CType(Me.ghFolio.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lbldescripcion_corta = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblnombre_grupo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblNombreDepartamento = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptMercanciaPendienteEntrega_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.nombre_empresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
