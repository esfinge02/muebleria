Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptRepartoVale
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private fecha_factura As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtSerieFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSerieFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtReparto As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtDireccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtColoniaCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private txtCiudadCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEstadoCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtEstadoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTelefonoCliente As DataDynamics.ActiveReports.Label = Nothing
	Private txtTelefonoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private txtRepartir As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtbodega_entrega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblreparto As DataDynamics.ActiveReports.Label = Nothing
	Private txtsucusal_bodeguero As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private Line7 As DataDynamics.ActiveReports.Line = Nothing
	Private Line8 As DataDynamics.ActiveReports.Line = Nothing
	Private nombre_bodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblChofer As DataDynamics.ActiveReports.Label = Nothing
	Private lineCliente As DataDynamics.ActiveReports.Line = Nothing
	Private txtChofer As DataDynamics.ActiveReports.TextBox = Nothing
	Public lineChofer As DataDynamics.ActiveReports.Line = Nothing
	Private txtBodeguero As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblNombreFirmaCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Reparto.rptRepartoVale.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.fecha_factura = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label28 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Shape7 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Shape)
		Me.txtFactura = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtSerieFactura = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblSerieFactura = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSucursal = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtSucursal = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Shape4 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Shape)
		Me.txtFecha = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblFecha = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Shape)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtReparto = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.lblCliente = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.txtDireccion = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.txtColoniaCliente = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.lblCiudad = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.txtCiudadCliente = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.TextBox)
		Me.lblEstadoCliente = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Label)
		Me.txtEstadoCliente = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.TextBox)
		Me.lblTelefonoCliente = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.Label)
		Me.txtTelefonoCliente = CType(Me.PageHeader.Controls(29),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.PageHeader.Controls(30),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.PageHeader.Controls(31),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.PageHeader.Controls(32),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.PageHeader.Controls(33),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(34),DataDynamics.ActiveReports.Picture)
		Me.Label26 = CType(Me.PageHeader.Controls(35),DataDynamics.ActiveReports.Label)
		Me.txtRepartir = CType(Me.PageHeader.Controls(36),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageHeader.Controls(37),DataDynamics.ActiveReports.Label)
		Me.txtbodega_entrega = CType(Me.PageHeader.Controls(38),DataDynamics.ActiveReports.TextBox)
		Me.lblreparto = CType(Me.PageHeader.Controls(39),DataDynamics.ActiveReports.Label)
		Me.txtsucusal_bodeguero = CType(Me.PageHeader.Controls(40),DataDynamics.ActiveReports.TextBox)
		Me.txtSerie = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Line)
		Me.Line4 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Line5 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line6 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Line7 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.Line8 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Line)
		Me.nombre_bodega = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtObservaciones = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblChofer = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lineCliente = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.Line)
		Me.txtChofer = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lineChofer = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.Line)
		Me.txtBodeguero = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Line10 = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.Line)
		Me.txtNombreCliente = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblNombreFirmaCliente = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptRepartoVale_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
