Imports System.Globalization
Imports System.Threading

Module modInicio
    Public TINApp As New Dipros.Windows.Application

    'DESCRIPCION: Funci�n de inicio del sistema
    'DESARROLLO: DIPROS SYSTEMS
    'FECHA: 18/02/2006 00:00:00
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Reparto"
        TINApp.Prefix = "REP"

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        'Paso de conexion y frmMain a Comunes
        Comunes.Common.Aplicacion = TINApp

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------
        'Inicia la aplicaci�n
        Dim oMain As New frmMain

        'Checa si existe el archivo ini de la sucursal actual
        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Or Comunes.clsUtilerias.AbrirArchivoBodega(Comunes.Common.Bodega_Actual) = False Then

            'Si no existe el archivo checa si es el usuario Super para que pueda configurar la sucursal
            If TINApp.Connection.User = "SUPER" Then

                Dim oForm As New Comunes.frmSucursalActual
                Dim oForm2 As New Comunes.frmBodegaActual
                'oMain.Enabled = False
                Application.Run(oForm)

                If oForm.EntrarMain = True Then
                    If Comunes.clsUtilerias.AbrirArchivoBodega(Comunes.Common.Bodega_Actual) = False Then
                        If TINApp.Connection.User = "SUPER" Then
                            'Dim oForm As New Comunes.frmBodegaActual
                            Application.Run(oForm2)
                            oForm2.Sucursal = Comunes.Common.Sucursal_Actual
                            oForm2.lkpBodega_LoadData(True)
                            If oForm2.EntrarMain = True Then
                                Application.Run(oMain)
                            Else
                                MsgBox("No hay una Bodega Definida", MsgBoxStyle.Information, "Mensaje del Sistema")
                            End If
                        Else
                            MsgBox("No hay una Bodega Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                        End If
                    Else
                        Application.Run(oMain)
                    End If
                    'Application.Run(oMain)
                Else
                    MsgBox("No hay una Sucursal Definida", MsgBoxStyle.Information, "Mensaje del Sistema")
                End If
            Else
                MsgBox("No hay una Sucursal Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
            End If
        Else

            Application.Run(oMain)

        End If

    End Sub
End Module
