Imports Dipros.Windows.Forms

Module ModFormatos
    
#Region "Bodegueros"

    Public Sub for_bodegueros_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Clave", "bodeguero", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodeguero", "nombre", 1, DevExpress.Utils.FormatType.None, "", 350)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "", 130)
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 2, DevExpress.Utils.FormatType.None, "", 350)

    End Sub
    

#End Region

#Region "Choferes"

    Public Sub for_choferes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Chofer", "chofer", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 230)
        AddColumns(oGrid, "Vehículo", "vehiculo", 2, DevExpress.Utils.FormatType.None, "", 230)
        AddColumns(oGrid, "Placas", "placas", 3, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Activo", "activo", 4, DevExpress.Utils.FormatType.None, "", 50)

    End Sub
    Public Sub for_choferes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Chofer", "chofer", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oLookup, "Vehículo", "vehiculo", 2, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oLookup, "Placas", "placas", 3, DevExpress.Utils.FormatType.None, "", 80)

    End Sub

#End Region

#Region "Grupos Eventualidades"
    Public Sub for_grupos_eventualidades_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Grupo", "grupo", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 350)

    End Sub

    Public Sub for_grupos_eventualidades_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Grupo", "grupo", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 320)

    End Sub
#End Region

#Region "Eventualidades"

    Public Sub for_eventualidades_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Grupo", "grupo", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Grupo", "nombre_grupo", 0, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Eventualidad", "eventualidad", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Descripción", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 400)
    End Sub
    Public Sub for_eventualidades_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        'AddColumns(oLookup, "Grupo", "grupo", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Grupo", "descripcion_grupo", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Eventualidad", "eventualidad", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripción", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 400)
    End Sub


#End Region

#Region "Repartos"

    Public Sub for_repartos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal_nombre", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Reparto", "reparto", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Sucursal_Factura", "sucursal_factura", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Factura", "folio_factura", 4, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Serie_Factura", "serie_factura", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Repartir", "repartir_vista", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Chofer", "chofer_nombre", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodeguero", "bodeguero_nombre", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Observaciones", "observaciones", 9, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region



End Module
