Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmEventualidades
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblGrupo As System.Windows.Forms.Label
		Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
		Friend WithEvents lblEventualidad As System.Windows.Forms.Label
		Friend WithEvents clcEventualidad As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEventualidades))
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblEventualidad = New System.Windows.Forms.Label
        Me.clcEventualidad = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcEventualidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(540, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(53, 88)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 4
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 86)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.Required = True
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(360, 20)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.Tag = "Grupo"
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "Grupo"
        '
        'lblEventualidad
        '
        Me.lblEventualidad.AutoSize = True
        Me.lblEventualidad.Location = New System.Drawing.Point(16, 40)
        Me.lblEventualidad.Name = "lblEventualidad"
        Me.lblEventualidad.Size = New System.Drawing.Size(80, 16)
        Me.lblEventualidad.TabIndex = 0
        Me.lblEventualidad.Tag = ""
        Me.lblEventualidad.Text = "&Eventualidad:"
        Me.lblEventualidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEventualidad
        '
        Me.clcEventualidad.EditValue = "0"
        Me.clcEventualidad.Location = New System.Drawing.Point(104, 39)
        Me.clcEventualidad.MaxValue = 0
        Me.clcEventualidad.MinValue = 0
        Me.clcEventualidad.Name = "clcEventualidad"
        '
        'clcEventualidad.Properties
        '
        Me.clcEventualidad.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcEventualidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEventualidad.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcEventualidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEventualidad.Properties.Enabled = False
        Me.clcEventualidad.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEventualidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEventualidad.Size = New System.Drawing.Size(64, 19)
        Me.clcEventualidad.TabIndex = 1
        Me.clcEventualidad.Tag = "eventualidad"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(24, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripcion:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(104, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 100
        Me.txtDescripcion.Size = New System.Drawing.Size(360, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmEventualidades
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(482, 120)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lblEventualidad)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.clcEventualidad)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmEventualidades"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcEventualidad, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblEventualidad, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        CType(Me.clcEventualidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oEventualidades As VillarrealBusiness.clsEventualidades
    Private oGruposEventualidades As VillarrealBusiness.clsGruposEventualidades

    Private ReadOnly Property GrupoEventualidad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmEventualidades_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
		Case Actions.Insert
						Response = oEventualidades.Insertar(Me.DataSource)

		Case Actions.Update
						Response = oEventualidades.Actualizar(Me.DataSource)

		Case Actions.Delete
						Response = oEventualidades.Eliminar(clcEventualidad.value)

		End Select
	End Sub

	Private Sub frmEventualidades_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
		Response = oEventualidades.DespliegaDatos(OwnerForm.Value("eventualidad"))
		If Not Response.ErrorFound Then
			Dim oDataSet As DataSet
			oDataSet = Response.Value
			Me.DataSource = oDataSet
		End If

	End Sub

	Private Sub frmEventualidades_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEventualidades = New VillarrealBusiness.clsEventualidades
        oGruposEventualidades = New VillarrealBusiness.clsGruposEventualidades

		Select Case Action
			Case Actions.Insert
			Case Actions.Update
			Case Actions.Delete
		End Select
	End Sub

	Private Sub frmEventualidades_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oEventualidades.Validacion(Action, GrupoEventualidad, txtDescripcion.Text)
	End Sub

	Private Sub frmEventualidades_Localize() Handles MyBase.Localize
		Find("Unknow", CType("Replace by a control", Object))

	End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oEventualidades.ValidaDescripcion(txtDescripcion.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        for_grupos_eventualidades_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim response As Events
        response = oGruposEventualidades.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If
        response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region

End Class
