Imports Dipros.Utils.Common
Imports Dipros.Utils


Public Class frmChoferes
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents clcChofer As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblVehiculo As System.Windows.Forms.Label
    Friend WithEvents txtVehiculo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPlacas As System.Windows.Forms.Label
    Friend WithEvents txtPlacas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmChoferes))
        Me.lblChofer = New System.Windows.Forms.Label
        Me.clcChofer = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblVehiculo = New System.Windows.Forms.Label
        Me.txtVehiculo = New DevExpress.XtraEditors.TextEdit
        Me.lblPlacas = New System.Windows.Forms.Label
        Me.txtPlacas = New DevExpress.XtraEditors.TextEdit
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.clcChofer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVehiculo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlacas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(383, 28)
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(22, 40)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(45, 16)
        Me.lblChofer.TabIndex = 0
        Me.lblChofer.Tag = ""
        Me.lblChofer.Text = "&Chofer:"
        Me.lblChofer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcChofer
        '
        Me.clcChofer.EditValue = "0"
        Me.clcChofer.Location = New System.Drawing.Point(75, 40)
        Me.clcChofer.MaxValue = 0
        Me.clcChofer.MinValue = 0
        Me.clcChofer.Name = "clcChofer"
        '
        'clcChofer.Properties
        '
        Me.clcChofer.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcChofer.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcChofer.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcChofer.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcChofer.Properties.Enabled = False
        Me.clcChofer.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcChofer.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcChofer.Size = New System.Drawing.Size(38, 19)
        Me.clcChofer.TabIndex = 1
        Me.clcChofer.Tag = "chofer"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(14, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(75, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 60
        Me.txtNombre.Size = New System.Drawing.Size(360, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'lblVehiculo
        '
        Me.lblVehiculo.AutoSize = True
        Me.lblVehiculo.Location = New System.Drawing.Point(12, 86)
        Me.lblVehiculo.Name = "lblVehiculo"
        Me.lblVehiculo.Size = New System.Drawing.Size(55, 16)
        Me.lblVehiculo.TabIndex = 4
        Me.lblVehiculo.Tag = ""
        Me.lblVehiculo.Text = "&Vehiculo:"
        Me.lblVehiculo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtVehiculo
        '
        Me.txtVehiculo.EditValue = ""
        Me.txtVehiculo.Location = New System.Drawing.Point(75, 86)
        Me.txtVehiculo.Name = "txtVehiculo"
        '
        'txtVehiculo.Properties
        '
        Me.txtVehiculo.Properties.MaxLength = 50
        Me.txtVehiculo.Size = New System.Drawing.Size(300, 20)
        Me.txtVehiculo.TabIndex = 5
        Me.txtVehiculo.Tag = "vehiculo"
        '
        'lblPlacas
        '
        Me.lblPlacas.AutoSize = True
        Me.lblPlacas.Location = New System.Drawing.Point(24, 109)
        Me.lblPlacas.Name = "lblPlacas"
        Me.lblPlacas.Size = New System.Drawing.Size(43, 16)
        Me.lblPlacas.TabIndex = 6
        Me.lblPlacas.Tag = ""
        Me.lblPlacas.Text = "&Placas:"
        Me.lblPlacas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPlacas
        '
        Me.txtPlacas.EditValue = ""
        Me.txtPlacas.Location = New System.Drawing.Point(75, 109)
        Me.txtPlacas.Name = "txtPlacas"
        '
        'txtPlacas.Properties
        '
        Me.txtPlacas.Properties.MaxLength = 15
        Me.txtPlacas.Size = New System.Drawing.Size(90, 20)
        Me.txtPlacas.TabIndex = 7
        Me.txtPlacas.Tag = "placas"
        '
        'chkActivo
        '
        Me.chkActivo.EditValue = True
        Me.chkActivo.Location = New System.Drawing.Point(74, 159)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Size = New System.Drawing.Size(104, 19)
        Me.chkActivo.TabIndex = 10
        Me.chkActivo.Tag = "activo"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(75, 132)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(358, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 9
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 134)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sucursal:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmChoferes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(444, 184)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.chkActivo)
        Me.Controls.Add(Me.lblChofer)
        Me.Controls.Add(Me.clcChofer)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblVehiculo)
        Me.Controls.Add(Me.txtVehiculo)
        Me.Controls.Add(Me.lblPlacas)
        Me.Controls.Add(Me.txtPlacas)
        Me.Name = "frmChoferes"
        Me.Controls.SetChildIndex(Me.txtPlacas, 0)
        Me.Controls.SetChildIndex(Me.lblPlacas, 0)
        Me.Controls.SetChildIndex(Me.txtVehiculo, 0)
        Me.Controls.SetChildIndex(Me.lblVehiculo, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcChofer, 0)
        Me.Controls.SetChildIndex(Me.lblChofer, 0)
        Me.Controls.SetChildIndex(Me.chkActivo, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.clcChofer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVehiculo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlacas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oChoferes As VillarrealBusiness.clsChoferes
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmChoferes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oChoferes.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oChoferes.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oChoferes.Eliminar(clcChofer.value)

        End Select
    End Sub

    Private Sub frmChoferes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oChoferes.DespliegaDatos(OwnerForm.Value("chofer"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmChoferes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oChoferes = New VillarrealBusiness.clsChoferes
        oSucursales = New VillarrealBusiness.clsSucursales

        Me.lkpSucursal.EditValue = CLng(Comunes.Common.Sucursal_Actual)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmChoferes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oChoferes.Validacion(Action, txtNombre.Text, txtVehiculo.Text, Sucursal)
    End Sub

    Private Sub frmChoferes_Localize() Handles MyBase.Localize
        Find("chofer", Me.clcChofer.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    'Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oChoferes.ValidaNombre(txtNombre.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub
    'Private Sub txtVehiculo_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtVehiculo.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oChoferes.ValidaVehiculo(txtVehiculo.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
