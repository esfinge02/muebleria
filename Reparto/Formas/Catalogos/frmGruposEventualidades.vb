Imports Dipros.Utils.Common

Public Class frmGruposEventualidades
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblGrupo As System.Windows.Forms.Label
		Friend WithEvents clcGrupo As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGruposEventualidades))
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.clcGrupo = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(361, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(46, 40)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 0
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcGrupo
        '
        Me.clcGrupo.EditValue = "0"
        Me.clcGrupo.Location = New System.Drawing.Point(92, 40)
        Me.clcGrupo.MaxValue = 0
        Me.clcGrupo.MinValue = 0
        Me.clcGrupo.Name = "clcGrupo"
        '
        'clcGrupo.Properties
        '
        Me.clcGrupo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcGrupo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGrupo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcGrupo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGrupo.Properties.Enabled = False
        Me.clcGrupo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcGrupo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcGrupo.Size = New System.Drawing.Size(60, 19)
        Me.clcGrupo.TabIndex = 1
        Me.clcGrupo.Tag = "grupo"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(17, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(300, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmGruposEventualidades
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 96)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.clcGrupo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmGruposEventualidades"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcGrupo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        CType(Me.clcGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oGruposEventualidades As VillarrealBusiness.clsGruposEventualidades
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmGruposEventualidades_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
		Case Actions.Insert
						Response = oGruposEventualidades.Insertar(Me.DataSource)

		Case Actions.Update
						Response = oGruposEventualidades.Actualizar(Me.DataSource)

		Case Actions.Delete
						Response = oGruposEventualidades.Eliminar(clcGrupo.value)

		End Select
	End Sub

	Private Sub frmGruposEventualidades_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
		Response = oGruposEventualidades.DespliegaDatos(OwnerForm.Value("grupo"))
		If Not Response.ErrorFound Then
			Dim oDataSet As DataSet
			oDataSet = Response.Value
			Me.DataSource = oDataSet
		End If

	End Sub

	Private Sub frmGruposEventualidades_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
		oGruposEventualidades = New VillarrealBusiness.clsGruposEventualidades

		Select Case Action
			Case Actions.Insert
			Case Actions.Update
			Case Actions.Delete
		End Select
	End Sub

	Private Sub frmGruposEventualidades_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oGruposEventualidades.Validacion(Action, txtDescripcion.Text)
	End Sub

	Private Sub frmGruposEventualidades_Localize() Handles MyBase.Localize
		Find("Unknow", CType("Replace by a control", Object))

	End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    'Private Sub _Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles .Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oGruposEventualidades.ValidaGrupo()
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub
	Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oGruposEventualidades.ValidaDescripcion(txtDescripcion.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
