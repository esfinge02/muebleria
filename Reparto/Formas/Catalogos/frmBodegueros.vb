Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmBodegueros
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBodeguero As System.Windows.Forms.Label
    Friend WithEvents clcBodeguero As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBodegueros))
        Me.lblBodeguero = New System.Windows.Forms.Label
        Me.clcBodeguero = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        CType(Me.clcBodeguero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(411, 28)
        '
        'lblBodeguero
        '
        Me.lblBodeguero.AutoSize = True
        Me.lblBodeguero.Location = New System.Drawing.Point(10, 40)
        Me.lblBodeguero.Name = "lblBodeguero"
        Me.lblBodeguero.Size = New System.Drawing.Size(68, 16)
        Me.lblBodeguero.TabIndex = 0
        Me.lblBodeguero.Tag = ""
        Me.lblBodeguero.Text = "&Bodeguero:"
        Me.lblBodeguero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBodeguero
        '
        Me.clcBodeguero.EditValue = "0"
        Me.clcBodeguero.Location = New System.Drawing.Point(80, 40)
        Me.clcBodeguero.MaxValue = 0
        Me.clcBodeguero.MinValue = 0
        Me.clcBodeguero.Name = "clcBodeguero"
        '
        'clcBodeguero.Properties
        '
        Me.clcBodeguero.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcBodeguero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBodeguero.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcBodeguero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBodeguero.Properties.Enabled = False
        Me.clcBodeguero.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBodeguero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBodeguero.Size = New System.Drawing.Size(24, 21)
        Me.clcBodeguero.TabIndex = 1
        Me.clcBodeguero.Tag = "bodeguero"
        Me.clcBodeguero.ToolTip = "bodeguero"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(25, 64)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(80, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 60
        Me.txtNombre.Size = New System.Drawing.Size(360, 22)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.ToolTip = "nombre"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(22, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 89)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(132, 22)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'frmBodegueros
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 127)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblBodeguero)
        Me.Controls.Add(Me.clcBodeguero)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "frmBodegueros"
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcBodeguero, 0)
        Me.Controls.SetChildIndex(Me.lblBodeguero, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        CType(Me.clcBodeguero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oBodegueros As VillarrealBusiness.clsBodegueros
    Private oSucursales As New VillarrealBusiness.clsSucursales
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmBodegueros_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oBodegueros.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oBodegueros.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oBodegueros.Eliminar(clcBodeguero.value)

        End Select
    End Sub

    Private Sub frmBodegueros_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oBodegueros.DespliegaDatos(OwnerForm.Value("bodeguero"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmBodegueros_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oBodegueros = New VillarrealBusiness.clsBodegueros

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmBodegueros_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oBodegueros.Validacion(Action, txtNombre.Text, Me.lkpSucursal.EditValue)
    End Sub

    Private Sub frmBodegueros_Localize() Handles MyBase.Localize
        Find("bodeguero", Me.clcBodeguero.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    'Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oBodegueros.ValidaNombre(txtNombre.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
