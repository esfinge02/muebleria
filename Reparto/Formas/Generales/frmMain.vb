Imports Dipros.Utils.Common

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatBodegueros As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatChoferes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatGruposEventualidades As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEventualidades As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProConsultaRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUtiVariables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepMercanciaPendienteEntrega As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCierreRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEnvioRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuproModificacionDatosReparto As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatBodegueros = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatChoferes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatGruposEventualidades = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEventualidades = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProConsultaRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuUtiVariables = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepMercanciaPendienteEntrega = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCierreRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEnvioRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuproModificacionDatosReparto = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatBodegueros, Me.mnuCatChoferes, Me.mnuCatGruposEventualidades, Me.mnuCatEventualidades, Me.mnuProRepartos, Me.mnuProConsultaRepartos, Me.mnuRepRepartos, Me.mnuUtiVariables, Me.BarStaticItem1, Me.mnuRepMercanciaPendienteEntrega, Me.mnuProCierreRepartos, Me.mnuRepEnvioRepartos, Me.mnuproModificacionDatosReparto})
        Me.BarManager.MaxItemId = 121
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiVariables, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatBodegueros), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatChoferes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatGruposEventualidades), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEventualidades)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProRepartos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProConsultaRepartos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCierreRepartos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuproModificacionDatosReparto)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepRepartos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMercanciaPendienteEntrega), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEnvioRepartos)})
        '
        'mnuCatBodegueros
        '
        Me.mnuCatBodegueros.Caption = "&Bodegueros"
        Me.mnuCatBodegueros.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatBodegueros.Id = 106
        Me.mnuCatBodegueros.ImageIndex = 1
        Me.mnuCatBodegueros.Name = "mnuCatBodegueros"
        '
        'mnuCatChoferes
        '
        Me.mnuCatChoferes.Caption = "C&hoferes"
        Me.mnuCatChoferes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatChoferes.Id = 107
        Me.mnuCatChoferes.ImageIndex = 2
        Me.mnuCatChoferes.Name = "mnuCatChoferes"
        '
        'mnuCatGruposEventualidades
        '
        Me.mnuCatGruposEventualidades.Caption = "&Grupos de Eventualidades"
        Me.mnuCatGruposEventualidades.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatGruposEventualidades.Id = 109
        Me.mnuCatGruposEventualidades.ImageIndex = 6
        Me.mnuCatGruposEventualidades.Name = "mnuCatGruposEventualidades"
        '
        'mnuCatEventualidades
        '
        Me.mnuCatEventualidades.Caption = "&Eventualidades"
        Me.mnuCatEventualidades.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEventualidades.Id = 110
        Me.mnuCatEventualidades.ImageIndex = 7
        Me.mnuCatEventualidades.Name = "mnuCatEventualidades"
        '
        'mnuProRepartos
        '
        Me.mnuProRepartos.Caption = "Re&partos"
        Me.mnuProRepartos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProRepartos.Id = 111
        Me.mnuProRepartos.ImageIndex = 3
        Me.mnuProRepartos.Name = "mnuProRepartos"
        '
        'mnuProConsultaRepartos
        '
        Me.mnuProConsultaRepartos.Caption = "C&onsultar Repartos"
        Me.mnuProConsultaRepartos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProConsultaRepartos.Id = 113
        Me.mnuProConsultaRepartos.ImageIndex = 4
        Me.mnuProConsultaRepartos.Name = "mnuProConsultaRepartos"
        '
        'mnuRepRepartos
        '
        Me.mnuRepRepartos.Caption = "Repartos"
        Me.mnuRepRepartos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepRepartos.Id = 114
        Me.mnuRepRepartos.ImageIndex = 5
        Me.mnuRepRepartos.Name = "mnuRepRepartos"
        '
        'mnuUtiVariables
        '
        Me.mnuUtiVariables.Caption = "Variables del Sistema"
        Me.mnuUtiVariables.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuUtiVariables.Id = 115
        Me.mnuUtiVariables.Name = "mnuUtiVariables"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(46, 474)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 11
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 116
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuRepMercanciaPendienteEntrega
        '
        Me.mnuRepMercanciaPendienteEntrega.Caption = "&Mercancia Pendiente de Entrega"
        Me.mnuRepMercanciaPendienteEntrega.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMercanciaPendienteEntrega.Id = 117
        Me.mnuRepMercanciaPendienteEntrega.ImageIndex = 8
        Me.mnuRepMercanciaPendienteEntrega.Name = "mnuRepMercanciaPendienteEntrega"
        '
        'mnuProCierreRepartos
        '
        Me.mnuProCierreRepartos.Caption = "Cierre de Repartos"
        Me.mnuProCierreRepartos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCierreRepartos.Id = 118
        Me.mnuProCierreRepartos.Name = "mnuProCierreRepartos"
        '
        'mnuRepEnvioRepartos
        '
        Me.mnuRepEnvioRepartos.Caption = "Env�o de Repartos"
        Me.mnuRepEnvioRepartos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEnvioRepartos.Id = 119
        Me.mnuRepEnvioRepartos.Name = "mnuRepEnvioRepartos"
        '
        'mnuproModificacionDatosReparto
        '
        Me.mnuproModificacionDatosReparto.Caption = "Modificacion de datos de reparto"
        Me.mnuproModificacionDatosReparto.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuproModificacionDatosReparto.Id = 120
        Me.mnuproModificacionDatosReparto.Name = "mnuproModificacionDatosReparto"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim obodegas As New VillarrealBusiness.clsBodegas
        Dim response As Dipros.Utils.Events
        Dim response2 As Dipros.Utils.Events
        Dim odataset As DataSet
        Dim odataset2 As DataSet

        Try
            response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
            If Not response.ErrorFound Then
                odataset = response.Value
            End If

            response2 = obodegas.DespliegaDatos(Comunes.Common.Bodega_Actual)
            If Not response2.ErrorFound Then
                odataset2 = response2.Value
            End If

            If Not response.ErrorFound And Not response2.ErrorFound Then
                Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre") + "          Bodega Actual: " + odataset2.Tables(0).Rows(0).Item("descripcion")
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, "Ocurr�o un error al buscar la sucursal y bodega actual")
        End Try
    End Sub

#End Region

#Region "Catalogos"
    Public Sub mnuCatBodegueros_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatBodegueros.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oBodegueros As New VillarrealBusiness.clsBodegueros
        With oGrid
            .Title = "Bodegueros"
            .UpdateTitle = "Bodegueros"
            .DataSource = AddressOf oBodegueros.Listado
            .UpdateForm = New frmBodegueros
            .Format = AddressOf for_bodegueros_grs
            .MdiParent = Me
            .Show()
        End With
        oBodegueros = Nothing
    End Sub
    Public Sub mnuCatChoferes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatChoferes.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oChoferes As New VillarrealBusiness.clsChoferes
        With oGrid
            .Title = "Choferes"
            .UpdateTitle = "Choferes"
            .DataSource = AddressOf oChoferes.Listado
            .UpdateForm = New frmChoferes
            .Format = AddressOf for_choferes_grs
            .MdiParent = Me
            .Show()
        End With
        oChoferes = Nothing
    End Sub
    Public Sub mnuCatGruposEventualidades_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatGruposEventualidades.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oGruposEventualidades As New VillarrealBusiness.clsGruposEventualidades
        With oGrid
            .Title = "Grupos de Eventualidades"
            .UpdateTitle = "Grupos de Eventualidades"
            .DataSource = AddressOf oGruposEventualidades.Listado
            .UpdateForm = New frmGruposEventualidades
            .Format = AddressOf for_grupos_eventualidades_grs
            .MdiParent = Me
            .Show()
        End With
        oGruposEventualidades = Nothing
    End Sub
    Public Sub mnuCatEventualidades_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEventualidades.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEventualidades As New VillarrealBusiness.clsEventualidades
        With oGrid
            .Title = "Eventualidades"
            .UpdateTitle = "Eventualidades"
            .DataSource = AddressOf oEventualidades.Listado
            .UpdateForm = New frmEventualidades
            .Format = AddressOf for_eventualidades_grs
            .MdiParent = Me
            .Show()
        End With
        oEventualidades = Nothing
    End Sub
#End Region

#Region "Procesos"
    Public Sub mnuProRepartos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProRepartos.ItemClick
        Dim oForm As New frmRepartos

        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Registro de Repartos"
            .Show()
        End With

    End Sub

    Public Sub mnuProConsultaRepartos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProConsultaRepartos.ItemClick
        Dim oform As New frmConsultarCerrarReparto
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Seguimiento de Repartos"
        oform.tbrTools.Buttons(0).Enabled = False
        oform.tbrTools.Buttons(0).Visible = False
        oform.Show()
    End Sub

    Private Sub mnuProCierreRepartos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCierreRepartos.ItemClick
        Dim oform As New frmCierreRepartos
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Cierre de Repartos"        
        oform.Show()

    End Sub


    Public Sub mnuproModificacionDatosReparto_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuproModificacionDatosReparto.ItemClick
        Dim oForm As New frmModificarReparto

        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Modificar Datos de Reparto"
        oForm.Show()

    End Sub

#End Region

#Region "Reportes"
    Public Sub mnuRepRepartos_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepRepartos.ItemClick
        Dim oform As New frmRepartosReportes
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Repartos"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub

    Public Sub mnuRepMercanciaPendienteEntrega_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMercanciaPendienteEntrega.ItemClick
        Dim oForm As New frmRepMercanciaPendienteEntrega
        oForm.MdiParent = Me
        oForm.Title = "Mercancia Pendiente de Entrega"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepEnvioRepartos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEnvioRepartos.ItemClick
        Dim oform As New frmEnvioRepartos
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Env�o de Repartos"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub
#End Region

#Region "Herramientas"

    Public Sub mnuUtiVariables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUtiVariables.ItemClick
        Dim oForm As New Comunes.frmVariables
        oForm.MdiParent = Me
        oForm.Show()
    End Sub
#End Region


    
End Class
