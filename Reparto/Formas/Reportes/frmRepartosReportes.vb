Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepartosReportes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents optChofer As System.Windows.Forms.RadioButton
    Friend WithEvents optTodos As System.Windows.Forms.RadioButton
    Friend WithEvents lkpChofer As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents optCliente As System.Windows.Forms.RadioButton
    Friend WithEvents gpbChofer As System.Windows.Forms.GroupBox
    Friend WithEvents gpbCliente As System.Windows.Forms.GroupBox
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboRepartir As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblRepartir As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkChoferesSucursal As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepartosReportes))
        Me.optChofer = New System.Windows.Forms.RadioButton
        Me.optTodos = New System.Windows.Forms.RadioButton
        Me.gpbChofer = New System.Windows.Forms.GroupBox
        Me.lkpChofer = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.gpbCliente = New System.Windows.Forms.GroupBox
        Me.optCliente = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboRepartir = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblRepartir = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.chkChoferesSucursal = New DevExpress.XtraEditors.CheckEdit
        Me.gpbChofer.SuspendLayout()
        Me.gpbCliente.SuspendLayout()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChoferesSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(79, 50)
        '
        'optChofer
        '
        Me.optChofer.Location = New System.Drawing.Point(24, -5)
        Me.optChofer.Name = "optChofer"
        Me.optChofer.Size = New System.Drawing.Size(64, 24)
        Me.optChofer.TabIndex = 0
        Me.optChofer.Text = "Ch&ofer"
        '
        'optTodos
        '
        Me.optTodos.Checked = True
        Me.optTodos.Location = New System.Drawing.Point(40, 96)
        Me.optTodos.Name = "optTodos"
        Me.optTodos.TabIndex = 4
        Me.optTodos.TabStop = True
        Me.optTodos.Text = "&Todos"
        '
        'gpbChofer
        '
        Me.gpbChofer.Controls.Add(Me.lkpChofer)
        Me.gpbChofer.Controls.Add(Me.Label6)
        Me.gpbChofer.Controls.Add(Me.optChofer)
        Me.gpbChofer.Location = New System.Drawing.Point(16, 136)
        Me.gpbChofer.Name = "gpbChofer"
        Me.gpbChofer.Size = New System.Drawing.Size(376, 56)
        Me.gpbChofer.TabIndex = 6
        Me.gpbChofer.TabStop = False
        '
        'lkpChofer
        '
        Me.lkpChofer.AllowAdd = False
        Me.lkpChofer.AutoReaload = False
        Me.lkpChofer.DataSource = Nothing
        Me.lkpChofer.DefaultSearchField = ""
        Me.lkpChofer.DisplayMember = "nombre"
        Me.lkpChofer.EditValue = Nothing
        Me.lkpChofer.Enabled = False
        Me.lkpChofer.Filtered = False
        Me.lkpChofer.InitValue = Nothing
        Me.lkpChofer.Location = New System.Drawing.Point(66, 24)
        Me.lkpChofer.MultiSelect = False
        Me.lkpChofer.Name = "lkpChofer"
        Me.lkpChofer.NullText = ""
        Me.lkpChofer.PopupWidth = CType(400, Long)
        Me.lkpChofer.ReadOnlyControl = False
        Me.lkpChofer.Required = False
        Me.lkpChofer.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChofer.SearchMember = ""
        Me.lkpChofer.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChofer.SelectAll = True
        Me.lkpChofer.Size = New System.Drawing.Size(280, 20)
        Me.lkpChofer.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpChofer.TabIndex = 3
        Me.lkpChofer.ToolTip = "Chofer"
        Me.lkpChofer.ValueMember = "chofer"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Chofer:"
        '
        'gpbCliente
        '
        Me.gpbCliente.Controls.Add(Me.optCliente)
        Me.gpbCliente.Controls.Add(Me.Label2)
        Me.gpbCliente.Controls.Add(Me.lkpCliente)
        Me.gpbCliente.Location = New System.Drawing.Point(16, 200)
        Me.gpbCliente.Name = "gpbCliente"
        Me.gpbCliente.Size = New System.Drawing.Size(376, 56)
        Me.gpbCliente.TabIndex = 7
        Me.gpbCliente.TabStop = False
        '
        'optCliente
        '
        Me.optCliente.Location = New System.Drawing.Point(24, -5)
        Me.optCliente.Name = "optCliente"
        Me.optCliente.Size = New System.Drawing.Size(64, 24)
        Me.optCliente.TabIndex = 0
        Me.optCliente.Text = "C&liente"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Cliente:"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(64, 24)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(280, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.ToolTip = "Cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(16, 264)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(376, 56)
        Me.gpbFechas.TabIndex = 8
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(240, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Fin.TabIndex = 14
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(33, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(198, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "&Hasta: "
        '
        'cboRepartir
        '
        Me.cboRepartir.EditValue = 1
        Me.cboRepartir.Location = New System.Drawing.Point(72, 64)
        Me.cboRepartir.Name = "cboRepartir"
        '
        'cboRepartir.Properties
        '
        Me.cboRepartir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboRepartir.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ma�ana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ambos", 3, -1)})
        Me.cboRepartir.Size = New System.Drawing.Size(96, 20)
        Me.cboRepartir.TabIndex = 3
        Me.cboRepartir.Tag = "repartir"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRepartir
        '
        Me.lblRepartir.AutoSize = True
        Me.lblRepartir.Location = New System.Drawing.Point(31, 64)
        Me.lblRepartir.Name = "lblRepartir"
        Me.lblRepartir.Size = New System.Drawing.Size(41, 16)
        Me.lblRepartir.TabIndex = 2
        Me.lblRepartir.Tag = ""
        Me.lblRepartir.Text = "Turno:"
        Me.lblRepartir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(72, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todas)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(296, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'chkChoferesSucursal
        '
        Me.chkChoferesSucursal.Location = New System.Drawing.Point(168, 98)
        Me.chkChoferesSucursal.Name = "chkChoferesSucursal"
        '
        'chkChoferesSucursal.Properties
        '
        Me.chkChoferesSucursal.Properties.Caption = "Solo Choferes de la Sucursal "
        Me.chkChoferesSucursal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkChoferesSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkChoferesSucursal.Size = New System.Drawing.Size(176, 22)
        Me.chkChoferesSucursal.TabIndex = 5
        '
        'frmRepartosReportes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 328)
        Me.Controls.Add(Me.chkChoferesSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.cboRepartir)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblRepartir)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.gpbChofer)
        Me.Controls.Add(Me.optTodos)
        Me.Controls.Add(Me.gpbCliente)
        Me.Name = "frmRepartosReportes"
        Me.Text = "frmRepartosReportes"
        Me.Controls.SetChildIndex(Me.gpbCliente, 0)
        Me.Controls.SetChildIndex(Me.optTodos, 0)
        Me.Controls.SetChildIndex(Me.gpbChofer, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lblRepartir, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboRepartir, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkChoferesSucursal, 0)
        Me.gpbChofer.ResumeLayout(False)
        Me.gpbCliente.ResumeLayout(False)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChoferesSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oChoferes As VillarrealBusiness.clsChoferes
    Private oClientes As VillarrealBusiness.clsClientes
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales


    Private ReadOnly Property TipoReporte() As Long
        Get
            If Me.optTodos.Checked = True Then Return 1
            If Me.optChofer.Checked = True Then Return 2
            If Me.optCliente.Checked = True Then Return 3
        End Get
    End Property

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Cliente() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpCliente.ToXML, "Cliente")
        End Get
    End Property

    Private ReadOnly Property Chofer() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpChofer.ToXML, "Chofer")
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepartosReportes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.optChofer.Checked = True And Me.lkpChofer.EditValue = 0 Then
            ShowMessage(MessageType.MsgError, "Debes de Introducir Un Chofer V�lido Para Mostrar Sus Repartos")
            Exit Sub
        End If

        If Me.optCliente.Checked = True And Me.lkpCliente.EditValue = 0 Then
            ShowMessage(MessageType.MsgError, "Debes de Introducir Un Cliente V�lido Para Mostrar Sus Repartos")
            Exit Sub
        End If

        If Me.dteFecha_Fin.EditValue < Me.dteFecha_Ini.EditValue Then
            ShowMessage(MessageType.MsgError, "La Fecha de Inicio Debe De Ser Antes Que La Final")
            Exit Sub
        End If

        Response = oReportes.Repartos(Me.lkpChofer.ToXML, Me.lkpCliente.ToXML, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, TipoReporte, Sucursal, Me.cboRepartir.Value, Me.chkChoferesSucursal.Checked)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Repartos no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptRepartos
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Repartos", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Sub frmRepartosReportes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oChoferes = New VillarrealBusiness.clsChoferes
        oClientes = New VillarrealBusiness.clsClientes
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales

        optChofer.Checked = False
        optCliente.Checked = False
        optTodos.Checked = True

        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)

    End Sub

    Private Sub frmRepartosReportes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Select Case TipoReporte
            Case 1
                Response = oReportes.Validacion(dteFecha_Ini.Text, dteFecha_Fin.Text)
            Case 2
                Response = oReportes.Validacion(Chofer, dteFecha_Ini.Text, dteFecha_Fin.Text)
            Case 3
                Response = oReportes.Validacion(Cliente, dteFecha_Ini.Text, dteFecha_Fin.Text)
        End Select

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpChofer_Format() Handles lkpChofer.Format
        for_choferes_grl(Me.lkpChofer)
    End Sub
    Private Sub lkpChofer_LoadData(ByVal Initialize As Boolean) Handles lkpChofer.LoadData
        Dim Response As New Events
        Response = oChoferes.Lookup(-1)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpChofer.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupRepartoClienteExcluido
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub optTodos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optTodos.CheckedChanged
        If optTodos.Checked = True Then
            Me.optChofer.Checked = False
            Me.optCliente.Checked = False
            Me.lkpChofer.SelectAll = True
         
        End If
    End Sub
    Private Sub optChofer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optChofer.CheckedChanged
        If optChofer.Checked = True Then
            Me.optTodos.Checked = False
            Me.optCliente.Checked = False
            Me.lkpChofer.Enabled = True
        Else
            Me.lkpChofer.Enabled = False
        End If
    End Sub
    Private Sub optCliente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optCliente.CheckedChanged
        If optCliente.Checked = True Then
            Me.optTodos.Checked = False
            Me.optChofer.Checked = False
            Me.lkpCliente.Enabled = True
        Else
            Me.lkpCliente.Enabled = False
        End If
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region


End Class
