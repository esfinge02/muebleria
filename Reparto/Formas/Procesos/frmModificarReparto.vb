Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmModificarReparto
    Inherits Dipros.Windows.frmTINForm

    Dim oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents grpInformacion As System.Windows.Forms.GroupBox
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents lblDObservaciones As System.Windows.Forms.Label
    Friend WithEvents lblDTelefono As System.Windows.Forms.Label
    Friend WithEvents lblDEstado As System.Windows.Forms.Label
    Friend WithEvents lblDCiudad As System.Windows.Forms.Label
    Friend WithEvents lblDColonia As System.Windows.Forms.Label
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents lblDDomicilio As System.Windows.Forms.Label
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblHora As System.Windows.Forms.Label
    Public WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFechaReparto As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblObservacionesC As System.Windows.Forms.Label
    Friend WithEvents lblDomicilioC As System.Windows.Forms.Label
    Friend WithEvents lblColoniaC As System.Windows.Forms.Label
    Friend WithEvents lblCiudadC As System.Windows.Forms.Label
    Friend WithEvents lblEstadoC As System.Windows.Forms.Label
    Friend WithEvents lblTelefonoC As System.Windows.Forms.Label
    Friend WithEvents lblHorarioC As System.Windows.Forms.Label
    Public WithEvents lblFechaC As System.Windows.Forms.Label
    Friend WithEvents dteFechaNueva As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grpInformacionNueva As System.Windows.Forms.GroupBox
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnBuscar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cboHorarioOriginal As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents cboHorarioNuevo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmModificarReparto))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.grpInformacion = New System.Windows.Forms.GroupBox
        Me.cboHorarioOriginal = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.lblDObservaciones = New System.Windows.Forms.Label
        Me.lblDTelefono = New System.Windows.Forms.Label
        Me.lblDEstado = New System.Windows.Forms.Label
        Me.lblDCiudad = New System.Windows.Forms.Label
        Me.lblDColonia = New System.Windows.Forms.Label
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.lblDDomicilio = New System.Windows.Forms.Label
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lblTelefono = New System.Windows.Forms.Label
        Me.lblHora = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFechaReparto = New DevExpress.XtraEditors.DateEdit
        Me.grpInformacionNueva = New System.Windows.Forms.GroupBox
        Me.cboHorarioNuevo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.txtTelefono = New DevExpress.XtraEditors.TextEdit
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilioC = New System.Windows.Forms.Label
        Me.lblColoniaC = New System.Windows.Forms.Label
        Me.lblCiudadC = New System.Windows.Forms.Label
        Me.lblEstadoC = New System.Windows.Forms.Label
        Me.lblTelefonoC = New System.Windows.Forms.Label
        Me.lblHorarioC = New System.Windows.Forms.Label
        Me.lblFechaC = New System.Windows.Forms.Label
        Me.dteFechaNueva = New DevExpress.XtraEditors.DateEdit
        Me.lblObservacionesC = New System.Windows.Forms.Label
        Me.btnBuscar = New DevExpress.XtraEditors.SimpleButton
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpInformacion.SuspendLayout()
        CType(Me.cboHorarioOriginal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpInformacionNueva.SuspendLayout()
        CType(Me.cboHorarioNuevo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaNueva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(16, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3448, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(19, 42)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(480, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(210, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(352, 40)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.MaxLength = 3
        Me.txtSerie.Size = New System.Drawing.Size(90, 20)
        Me.txtSerie.TabIndex = 3
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(504, 40)
        Me.clcFolio.Name = "clcFolio"
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(90, 20)
        Me.clcFolio.TabIndex = 5
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(312, 42)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 2
        Me.lblSerie.Text = "Serie:"
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(464, 42)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 4
        Me.lblFolio.Text = "Folio:"
        '
        'grpInformacion
        '
        Me.grpInformacion.Controls.Add(Me.cboHorarioOriginal)
        Me.grpInformacion.Controls.Add(Me.lblObservaciones)
        Me.grpInformacion.Controls.Add(Me.lblDObservaciones)
        Me.grpInformacion.Controls.Add(Me.lblDTelefono)
        Me.grpInformacion.Controls.Add(Me.lblDEstado)
        Me.grpInformacion.Controls.Add(Me.lblDCiudad)
        Me.grpInformacion.Controls.Add(Me.lblDColonia)
        Me.grpInformacion.Controls.Add(Me.lblDomicilio)
        Me.grpInformacion.Controls.Add(Me.lblDDomicilio)
        Me.grpInformacion.Controls.Add(Me.lblColonia)
        Me.grpInformacion.Controls.Add(Me.lblCiudad)
        Me.grpInformacion.Controls.Add(Me.lblEstado)
        Me.grpInformacion.Controls.Add(Me.lblTelefono)
        Me.grpInformacion.Controls.Add(Me.lblHora)
        Me.grpInformacion.Controls.Add(Me.lblFecha)
        Me.grpInformacion.Controls.Add(Me.dteFechaReparto)
        Me.grpInformacion.Location = New System.Drawing.Point(10, 72)
        Me.grpInformacion.Name = "grpInformacion"
        Me.grpInformacion.Size = New System.Drawing.Size(702, 224)
        Me.grpInformacion.TabIndex = 81
        Me.grpInformacion.TabStop = False
        Me.grpInformacion.Text = " Informaci�n Original "
        '
        'cboHorarioOriginal
        '
        Me.cboHorarioOriginal.EditValue = 1
        Me.cboHorarioOriginal.Location = New System.Drawing.Point(96, 48)
        Me.cboHorarioOriginal.Name = "cboHorarioOriginal"
        '
        'cboHorarioOriginal.Properties
        '
        Me.cboHorarioOriginal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboHorarioOriginal.Properties.Enabled = False
        Me.cboHorarioOriginal.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ma�ana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1)})
        Me.cboHorarioOriginal.Size = New System.Drawing.Size(96, 23)
        Me.cboHorarioOriginal.TabIndex = 115
        Me.cboHorarioOriginal.Tag = "horario_reparto"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.Location = New System.Drawing.Point(8, 200)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(96, 16)
        Me.lblObservaciones.TabIndex = 114
        Me.lblObservaciones.Text = "Observaciones :"
        '
        'lblDObservaciones
        '
        Me.lblDObservaciones.Location = New System.Drawing.Point(112, 200)
        Me.lblDObservaciones.Name = "lblDObservaciones"
        Me.lblDObservaciones.Size = New System.Drawing.Size(432, 16)
        Me.lblDObservaciones.TabIndex = 113
        '
        'lblDTelefono
        '
        Me.lblDTelefono.Location = New System.Drawing.Point(96, 176)
        Me.lblDTelefono.Name = "lblDTelefono"
        Me.lblDTelefono.Size = New System.Drawing.Size(154, 16)
        Me.lblDTelefono.TabIndex = 112
        '
        'lblDEstado
        '
        Me.lblDEstado.Location = New System.Drawing.Point(96, 152)
        Me.lblDEstado.Name = "lblDEstado"
        Me.lblDEstado.Size = New System.Drawing.Size(163, 16)
        Me.lblDEstado.TabIndex = 111
        '
        'lblDCiudad
        '
        Me.lblDCiudad.Location = New System.Drawing.Point(96, 128)
        Me.lblDCiudad.Name = "lblDCiudad"
        Me.lblDCiudad.Size = New System.Drawing.Size(240, 16)
        Me.lblDCiudad.TabIndex = 110
        '
        'lblDColonia
        '
        Me.lblDColonia.Location = New System.Drawing.Point(96, 104)
        Me.lblDColonia.Name = "lblDColonia"
        Me.lblDColonia.Size = New System.Drawing.Size(230, 16)
        Me.lblDColonia.TabIndex = 109
        '
        'lblDomicilio
        '
        Me.lblDomicilio.Location = New System.Drawing.Point(24, 80)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(64, 16)
        Me.lblDomicilio.TabIndex = 108
        Me.lblDomicilio.Text = "Domicilio :"
        '
        'lblDDomicilio
        '
        Me.lblDDomicilio.Location = New System.Drawing.Point(96, 80)
        Me.lblDDomicilio.Name = "lblDDomicilio"
        Me.lblDDomicilio.Size = New System.Drawing.Size(584, 16)
        Me.lblDDomicilio.TabIndex = 107
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(32, 104)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(53, 16)
        Me.lblColonia.TabIndex = 106
        Me.lblColonia.Text = "Colonia :"
        '
        'lblCiudad
        '
        Me.lblCiudad.Location = New System.Drawing.Point(32, 128)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(56, 16)
        Me.lblCiudad.TabIndex = 105
        Me.lblCiudad.Text = "Ciudad  :"
        '
        'lblEstado
        '
        Me.lblEstado.Location = New System.Drawing.Point(32, 152)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(56, 16)
        Me.lblEstado.TabIndex = 104
        Me.lblEstado.Text = "Estado  :"
        '
        'lblTelefono
        '
        Me.lblTelefono.Location = New System.Drawing.Point(24, 176)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(64, 16)
        Me.lblTelefono.TabIndex = 103
        Me.lblTelefono.Text = "Telefono :"
        '
        'lblHora
        '
        Me.lblHora.Location = New System.Drawing.Point(32, 56)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(56, 16)
        Me.lblHora.TabIndex = 101
        Me.lblHora.Text = "Horario :"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(48, 32)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 100
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "F&echa:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaReparto
        '
        Me.dteFechaReparto.EditValue = "10/04/2006"
        Me.dteFechaReparto.Location = New System.Drawing.Point(96, 24)
        Me.dteFechaReparto.Name = "dteFechaReparto"
        '
        'dteFechaReparto.Properties
        '
        Me.dteFechaReparto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaReparto.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaReparto.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaReparto.Properties.Enabled = False
        Me.dteFechaReparto.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaReparto.Size = New System.Drawing.Size(96, 23)
        Me.dteFechaReparto.TabIndex = 99
        Me.dteFechaReparto.Tag = "fecha"
        '
        'grpInformacionNueva
        '
        Me.grpInformacionNueva.Controls.Add(Me.cboHorarioNuevo)
        Me.grpInformacionNueva.Controls.Add(Me.txtColonia)
        Me.grpInformacionNueva.Controls.Add(Me.txtTelefono)
        Me.grpInformacionNueva.Controls.Add(Me.txtEstado)
        Me.grpInformacionNueva.Controls.Add(Me.txtCiudad)
        Me.grpInformacionNueva.Controls.Add(Me.txtDomicilio)
        Me.grpInformacionNueva.Controls.Add(Me.lblDomicilioC)
        Me.grpInformacionNueva.Controls.Add(Me.lblColoniaC)
        Me.grpInformacionNueva.Controls.Add(Me.lblCiudadC)
        Me.grpInformacionNueva.Controls.Add(Me.lblEstadoC)
        Me.grpInformacionNueva.Controls.Add(Me.lblTelefonoC)
        Me.grpInformacionNueva.Controls.Add(Me.lblHorarioC)
        Me.grpInformacionNueva.Controls.Add(Me.lblFechaC)
        Me.grpInformacionNueva.Controls.Add(Me.dteFechaNueva)
        Me.grpInformacionNueva.Location = New System.Drawing.Point(10, 304)
        Me.grpInformacionNueva.Name = "grpInformacionNueva"
        Me.grpInformacionNueva.Size = New System.Drawing.Size(700, 200)
        Me.grpInformacionNueva.TabIndex = 82
        Me.grpInformacionNueva.TabStop = False
        Me.grpInformacionNueva.Text = " Informaci�n Nueva "
        '
        'cboHorarioNuevo
        '
        Me.cboHorarioNuevo.EditValue = 1
        Me.cboHorarioNuevo.Location = New System.Drawing.Point(96, 48)
        Me.cboHorarioNuevo.Name = "cboHorarioNuevo"
        '
        'cboHorarioNuevo.Properties
        '
        Me.cboHorarioNuevo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboHorarioNuevo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ma�ana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1)})
        Me.cboHorarioNuevo.Size = New System.Drawing.Size(96, 23)
        Me.cboHorarioNuevo.TabIndex = 8
        Me.cboHorarioNuevo.Tag = "horario_reparto_nuevo"
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(96, 96)
        Me.txtColonia.Name = "txtColonia"
        '
        'txtColonia.Properties
        '
        Me.txtColonia.Properties.MaxLength = 60
        Me.txtColonia.Size = New System.Drawing.Size(504, 20)
        Me.txtColonia.TabIndex = 10
        Me.txtColonia.Tag = "colonia_nuevo"
        '
        'txtTelefono
        '
        Me.txtTelefono.EditValue = ""
        Me.txtTelefono.Location = New System.Drawing.Point(96, 168)
        Me.txtTelefono.Name = "txtTelefono"
        '
        'txtTelefono.Properties
        '
        Me.txtTelefono.Properties.MaxLength = 60
        Me.txtTelefono.Size = New System.Drawing.Size(202, 20)
        Me.txtTelefono.TabIndex = 13
        Me.txtTelefono.Tag = "telefono_nuevo"
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(96, 144)
        Me.txtEstado.Name = "txtEstado"
        '
        'txtEstado.Properties
        '
        Me.txtEstado.Properties.MaxLength = 60
        Me.txtEstado.Size = New System.Drawing.Size(504, 20)
        Me.txtEstado.TabIndex = 12
        Me.txtEstado.Tag = "estado_nuevo"
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(96, 120)
        Me.txtCiudad.Name = "txtCiudad"
        '
        'txtCiudad.Properties
        '
        Me.txtCiudad.Properties.MaxLength = 60
        Me.txtCiudad.Size = New System.Drawing.Size(504, 20)
        Me.txtCiudad.TabIndex = 11
        Me.txtCiudad.Tag = "ciudad_nuevo"
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(96, 72)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.MaxLength = 60
        Me.txtDomicilio.Size = New System.Drawing.Size(504, 20)
        Me.txtDomicilio.TabIndex = 9
        Me.txtDomicilio.Tag = "domicilio_nuevo"
        '
        'lblDomicilioC
        '
        Me.lblDomicilioC.AutoSize = True
        Me.lblDomicilioC.Location = New System.Drawing.Point(22, 80)
        Me.lblDomicilioC.Name = "lblDomicilioC"
        Me.lblDomicilioC.Size = New System.Drawing.Size(63, 16)
        Me.lblDomicilioC.TabIndex = 123
        Me.lblDomicilioC.Text = "Domicilio :"
        '
        'lblColoniaC
        '
        Me.lblColoniaC.AutoSize = True
        Me.lblColoniaC.Location = New System.Drawing.Point(32, 104)
        Me.lblColoniaC.Name = "lblColoniaC"
        Me.lblColoniaC.Size = New System.Drawing.Size(53, 16)
        Me.lblColoniaC.TabIndex = 122
        Me.lblColoniaC.Text = "Colonia :"
        '
        'lblCiudadC
        '
        Me.lblCiudadC.AutoSize = True
        Me.lblCiudadC.Location = New System.Drawing.Point(34, 128)
        Me.lblCiudadC.Name = "lblCiudadC"
        Me.lblCiudadC.Size = New System.Drawing.Size(51, 16)
        Me.lblCiudadC.TabIndex = 121
        Me.lblCiudadC.Text = "Ciudad :"
        '
        'lblEstadoC
        '
        Me.lblEstadoC.AutoSize = True
        Me.lblEstadoC.Location = New System.Drawing.Point(35, 152)
        Me.lblEstadoC.Name = "lblEstadoC"
        Me.lblEstadoC.Size = New System.Drawing.Size(50, 16)
        Me.lblEstadoC.TabIndex = 120
        Me.lblEstadoC.Text = "Estado :"
        '
        'lblTelefonoC
        '
        Me.lblTelefonoC.AutoSize = True
        Me.lblTelefonoC.Location = New System.Drawing.Point(25, 176)
        Me.lblTelefonoC.Name = "lblTelefonoC"
        Me.lblTelefonoC.Size = New System.Drawing.Size(60, 16)
        Me.lblTelefonoC.TabIndex = 119
        Me.lblTelefonoC.Text = "Telefono :"
        '
        'lblHorarioC
        '
        Me.lblHorarioC.AutoSize = True
        Me.lblHorarioC.Location = New System.Drawing.Point(32, 56)
        Me.lblHorarioC.Name = "lblHorarioC"
        Me.lblHorarioC.Size = New System.Drawing.Size(53, 16)
        Me.lblHorarioC.TabIndex = 117
        Me.lblHorarioC.Text = "Horario :"
        '
        'lblFechaC
        '
        Me.lblFechaC.AutoSize = True
        Me.lblFechaC.Location = New System.Drawing.Point(44, 32)
        Me.lblFechaC.Name = "lblFechaC"
        Me.lblFechaC.Size = New System.Drawing.Size(41, 16)
        Me.lblFechaC.TabIndex = 116
        Me.lblFechaC.Tag = ""
        Me.lblFechaC.Text = "F&echa:"
        Me.lblFechaC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaNueva
        '
        Me.dteFechaNueva.EditValue = "10/04/2006"
        Me.dteFechaNueva.Location = New System.Drawing.Point(96, 24)
        Me.dteFechaNueva.Name = "dteFechaNueva"
        '
        'dteFechaNueva.Properties
        '
        Me.dteFechaNueva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaNueva.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaNueva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaNueva.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaNueva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaNueva.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaNueva.Size = New System.Drawing.Size(96, 23)
        Me.dteFechaNueva.TabIndex = 7
        Me.dteFechaNueva.Tag = "fecha_nuevo"
        '
        'lblObservacionesC
        '
        Me.lblObservacionesC.Location = New System.Drawing.Point(16, 512)
        Me.lblObservacionesC.Name = "lblObservacionesC"
        Me.lblObservacionesC.Size = New System.Drawing.Size(160, 16)
        Me.lblObservacionesC.TabIndex = 124
        Me.lblObservacionesC.Text = "Observaciones de Reparto :"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(616, 40)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(96, 20)
        Me.btnBuscar.TabIndex = 6
        Me.btnBuscar.Text = "&Buscar Factura"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(176, 512)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(536, 72)
        Me.txtObservaciones.TabIndex = 14
        Me.txtObservaciones.Tag = "observaciones_cambio"
        '
        'frmModificarReparto
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(730, 596)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.grpInformacionNueva)
        Me.Controls.Add(Me.grpInformacion)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblObservacionesC)
        Me.Name = "frmModificarReparto"
        Me.Text = "Modificar Reparto"
        Me.Controls.SetChildIndex(Me.lblObservacionesC, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.grpInformacion, 0)
        Me.Controls.SetChildIndex(Me.grpInformacionNueva, 0)
        Me.Controls.SetChildIndex(Me.btnBuscar, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpInformacion.ResumeLayout(False)
        CType(Me.cboHorarioOriginal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpInformacionNueva.ResumeLayout(False)
        CType(Me.cboHorarioNuevo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaNueva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        VaciarDatos()
    End Sub

    Private Sub VaciarDatos()
        Dim Llenar As New VillarrealBusiness.clsVentasCambioReparto
        Dim dataset As New DataSet
        Dim oEvents As New Events
        Dim Columna As DataRow

        oEvents = Llenar.ObtenerDatosReparto(Sucursal, Me.txtSerie.Text, Me.clcFolio.Value)
        If Not oEvents.ErrorFound Then

            dataset = oEvents.Value

            If dataset.Tables(0).Rows.Count > 0 Then
                Columna = dataset.Tables(0).Rows(0)
                dataset = Nothing

                Me.dteFechaReparto.EditValue = Columna("fecha_reparto")
                Me.cboHorarioOriginal.EditValue = Columna("horario_reparto")
                Me.lblDDomicilio.Text = Columna("domicilio_reparto")
                Me.lblDColonia.Text = Columna("colonia_reparto")
                Me.lblDCiudad.Text = Columna("ciudad_reparto")
                Me.lblDEstado.Text = Columna("estado_reparto")
                Me.lblDTelefono.Text = Columna("telefono_reparto")
                Me.lblDObservaciones.Text = Columna("observaciones_reparto")

                Me.dteFechaNueva.EditValue = Columna("fecha_reparto")
                Me.cboHorarioNuevo.EditValue = Columna("horario_reparto")
                Me.txtDomicilio.Text = Columna("domicilio_reparto")
                Me.txtColonia.Text = Columna("colonia_reparto")
                Me.txtCiudad.Text = Columna("ciudad_reparto")
                Me.txtEstado.Text = Columna("estado_reparto")
                Me.txtTelefono.Text = Columna("telefono_reparto")
                Me.txtObservaciones.Text = Columna("observaciones_reparto")
            Else
                ShowMessage(MessageType.MsgInformation, "Factura no encontrada", "Modificar datos de reparto")
            End If
        Else
            ShowMessage(MessageType.MsgError, "Error al buscar la factura", "Modificar datos de reparto", oEvents.Ex)
        End If

    End Sub

    Private Sub frmModificarReparto_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim Guarda As New VillarrealBusiness.clsVentasCambioReparto
       
        Response = Guarda.Insertar(Sucursal, Me.txtSerie.Text, Me.clcFolio.Value, Me.dteFechaReparto.DateTime, CType(Me.cboHorarioOriginal.EditValue, String), Me.lblDDomicilio.Text, Me.lblDColonia.Text, Me.lblDCiudad.Text, Me.lblDEstado.Text, Me.lblDTelefono.Text, Me.lblDObservaciones.Text)

        If Not Response.ErrorFound Then
            Response = Guarda.ActualizarVenta(Sucursal, Me.txtSerie.Text, Me.clcFolio.Value, Me.dteFechaNueva.DateTime, CType(Me.cboHorarioNuevo.EditValue, String), Me.txtDomicilio.Text, Me.txtColonia.Text, Me.txtCiudad.Text, Me.txtEstado.Text, Me.txtTelefono.Text, txtObservaciones.Text)
        End If

    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub frmModificarReparto_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales

    End Sub

    Private Sub lblEstado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblEstado.Click

    End Sub

    Private Sub frmModificarReparto_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmModificarReparto_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmModificarReparto_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
End Class
