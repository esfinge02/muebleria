
Public Class frmConsultarCerrarRepartoVerProductos
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grvProductos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblProductos As System.Windows.Forms.Label
    Friend WithEvents lblReparto As System.Windows.Forms.Label
    Friend WithEvents grcBodegaEntrega As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultarCerrarRepartoVerProductos))
        Me.grProductos = New DevExpress.XtraGrid.GridControl
        Me.grvProductos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.Ar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblProductos = New System.Windows.Forms.Label
        Me.lblReparto = New System.Windows.Forms.Label
        Me.grcBodegaEntrega = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.grProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1800, 28)
        '
        'grProductos
        '
        '
        'grProductos.EmbeddedNavigator
        '
        Me.grProductos.EmbeddedNavigator.Name = ""
        Me.grProductos.Location = New System.Drawing.Point(6, 54)
        Me.grProductos.MainView = Me.grvProductos
        Me.grProductos.Name = "grProductos"
        Me.grProductos.Size = New System.Drawing.Size(783, 248)
        Me.grProductos.TabIndex = 59
        Me.grProductos.Text = "GridControl1"
        '
        'grvProductos
        '
        Me.grvProductos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Ar, Me.grcArticulo, Me.grcNumeroSerie, Me.grcCantidad, Me.grcBodega, Me.grcBodegaEntrega})
        Me.grvProductos.GridControl = Me.grProductos
        Me.grvProductos.Name = "grvProductos"
        Me.grvProductos.OptionsView.ColumnAutoWidth = False
        Me.grvProductos.OptionsView.ShowGroupPanel = False
        '
        'Ar
        '
        Me.Ar.Caption = "Art�culo"
        Me.Ar.FieldName = "articulo"
        Me.Ar.Name = "Ar"
        Me.Ar.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Ar.VisibleIndex = 0
        Me.Ar.Width = 100
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Descripci�n"
        Me.grcArticulo.FieldName = "descripcion_corta"
        Me.grcArticulo.MinWidth = 50
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 1
        Me.grcArticulo.Width = 200
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "Numero de Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroSerie.VisibleIndex = 2
        Me.grcNumeroSerie.Width = 98
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 62
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "descripcion"
        Me.grcBodega.MinWidth = 50
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodega.VisibleIndex = 4
        Me.grcBodega.Width = 154
        '
        'lblProductos
        '
        Me.lblProductos.AutoSize = True
        Me.lblProductos.Location = New System.Drawing.Point(16, 34)
        Me.lblProductos.Name = "lblProductos"
        Me.lblProductos.Size = New System.Drawing.Size(128, 16)
        Me.lblProductos.TabIndex = 60
        Me.lblProductos.Text = "Productos del reparto:"
        '
        'lblReparto
        '
        Me.lblReparto.AutoSize = True
        Me.lblReparto.Location = New System.Drawing.Point(152, 34)
        Me.lblReparto.Name = "lblReparto"
        Me.lblReparto.Size = New System.Drawing.Size(44, 16)
        Me.lblReparto.TabIndex = 61
        Me.lblReparto.Text = "reparto"
        '
        'grcBodegaEntrega
        '
        Me.grcBodegaEntrega.Caption = "Bodega Entrega"
        Me.grcBodegaEntrega.FieldName = "bodega_entrega"
        Me.grcBodegaEntrega.Name = "grcBodegaEntrega"
        Me.grcBodegaEntrega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodegaEntrega.VisibleIndex = 5
        Me.grcBodegaEntrega.Width = 153
        '
        'frmConsultarCerrarRepartoVerProductos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(794, 312)
        Me.Controls.Add(Me.lblReparto)
        Me.Controls.Add(Me.lblProductos)
        Me.Controls.Add(Me.grProductos)
        Me.Name = "frmConsultarCerrarRepartoVerProductos"
        Me.Text = "frmVerProductos"
        Me.Controls.SetChildIndex(Me.grProductos, 0)
        Me.Controls.SetChildIndex(Me.lblProductos, 0)
        Me.Controls.SetChildIndex(Me.lblReparto, 0)
        CType(Me.grProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Public FormaPadre As Windows.Forms.Form
    Public intReparto As Integer
    Private oRepartosDetalle As New VillarrealBusiness.clsRepartosDetalle
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmVerProductos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        tbrTools.Buttons(0).Visible = False
        FormaPadre.Enabled = False
        Response = oRepartosDetalle.MostrarProductos(intReparto)
        If Response.ErrorFound Then
            Response.ShowError()
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            grProductos.DataSource = oDataSet.Tables(0)
            lblReparto.Text = intReparto
            oDataSet = Nothing
        End If
    End Sub

    Private Sub frmVerProductos_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Disposed
        FormaPadre.Enabled = True
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region

End Class
