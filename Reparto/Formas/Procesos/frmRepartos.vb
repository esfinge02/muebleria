Imports Dipros.Utils.Common
Imports Dipros.Utils


Public Class frmRepartos
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Private Factura_Entregada As Boolean
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblSucursal As System.Windows.Forms.Label
		Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
		Friend WithEvents lblReparto As System.Windows.Forms.Label
		Friend WithEvents clcReparto As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblFecha As System.Windows.Forms.Label
		Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblSucursal_Factura As System.Windows.Forms.Label

		Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label

		Friend WithEvents lblSerie_Factura As System.Windows.Forms.Label
		Friend WithEvents txtSerie_Factura As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblRepartir As System.Windows.Forms.Label

		Friend WithEvents lblChofer As System.Windows.Forms.Label

		Friend WithEvents lblBodeguero As System.Windows.Forms.Label

		Friend WithEvents lblObservaciones As System.Windows.Forms.Label
		Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpChofer As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodeguero As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFechaFactura As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dteFechaFactura As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cboRepartir As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcRepartir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRepartir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpFactura As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcPartidaFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSucursal_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcBodegaNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkChofer As System.Windows.Forms.CheckBox
    Friend WithEvents grcManejaSeries As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtNumeroSerie As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblNombreVendedor As System.Windows.Forms.Label
    Friend WithEvents grcSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcbodega_entrada As DevExpress.XtraGrid.Columns.GridColumn

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepartos))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblReparto = New System.Windows.Forms.Label
        Me.clcReparto = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblSucursal_Factura = New System.Windows.Forms.Label
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.lblSerie_Factura = New System.Windows.Forms.Label
        Me.txtSerie_Factura = New DevExpress.XtraEditors.TextEdit
        Me.lblRepartir = New System.Windows.Forms.Label
        Me.lblChofer = New System.Windows.Forms.Label
        Me.lblBodeguero = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lkpChofer = New Dipros.Editors.TINMultiLookup
        Me.lkpBodeguero = New Dipros.Editors.TINMultiLookup
        Me.lblFechaFactura = New System.Windows.Forms.Label
        Me.dteFechaFactura = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit
        Me.cboRepartir = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcRepartir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRepartir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodegaNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartidaFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcManejaSeries = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkManejaSeries = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcbodega_entrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtNumeroSerie = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.lkpFactura = New Dipros.Editors.TINMultiLookup
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtTelefono = New DevExpress.XtraEditors.TextEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtSucursal_Factura = New DevExpress.XtraEditors.TextEdit
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkChofer = New System.Windows.Forms.CheckBox
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblNombreVendedor = New System.Windows.Forms.Label
        CType(Me.clcReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRepartir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroSerie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSucursal_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(16019, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(40, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(288, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblReparto
        '
        Me.lblReparto.AutoSize = True
        Me.lblReparto.Location = New System.Drawing.Point(44, 64)
        Me.lblReparto.Name = "lblReparto"
        Me.lblReparto.Size = New System.Drawing.Size(52, 16)
        Me.lblReparto.TabIndex = 4
        Me.lblReparto.Tag = ""
        Me.lblReparto.Text = "&Reparto:"
        Me.lblReparto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcReparto
        '
        Me.clcReparto.EditValue = "0"
        Me.clcReparto.Location = New System.Drawing.Point(96, 64)
        Me.clcReparto.MaxValue = 0
        Me.clcReparto.MinValue = 0
        Me.clcReparto.Name = "clcReparto"
        '
        'clcReparto.Properties
        '
        Me.clcReparto.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcReparto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcReparto.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcReparto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcReparto.Properties.Enabled = False
        Me.clcReparto.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcReparto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcReparto.Size = New System.Drawing.Size(95, 19)
        Me.clcReparto.TabIndex = 5
        Me.clcReparto.Tag = "reparto"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(576, 35)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(624, 35)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy HH:mm"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblSucursal_Factura
        '
        Me.lblSucursal_Factura.AutoSize = True
        Me.lblSucursal_Factura.Location = New System.Drawing.Point(6, 46)
        Me.lblSucursal_Factura.Name = "lblSucursal_Factura"
        Me.lblSucursal_Factura.Size = New System.Drawing.Size(116, 16)
        Me.lblSucursal_Factura.TabIndex = 2
        Me.lblSucursal_Factura.Tag = ""
        Me.lblSucursal_Factura.Text = "&Sucursal de factura:"
        Me.lblSucursal_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(72, 22)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(50, 16)
        Me.lblFolio_Factura.TabIndex = 0
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Factura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerie_Factura
        '
        Me.lblSerie_Factura.AutoSize = True
        Me.lblSerie_Factura.Location = New System.Drawing.Point(25, 70)
        Me.lblSerie_Factura.Name = "lblSerie_Factura"
        Me.lblSerie_Factura.Size = New System.Drawing.Size(97, 16)
        Me.lblSerie_Factura.TabIndex = 4
        Me.lblSerie_Factura.Tag = ""
        Me.lblSerie_Factura.Text = "&Serie de factura:"
        Me.lblSerie_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie_Factura
        '
        Me.txtSerie_Factura.EditValue = ""
        Me.txtSerie_Factura.Location = New System.Drawing.Point(128, 70)
        Me.txtSerie_Factura.Name = "txtSerie_Factura"
        '
        'txtSerie_Factura.Properties
        '
        Me.txtSerie_Factura.Properties.Enabled = False
        Me.txtSerie_Factura.Properties.MaxLength = 3
        Me.txtSerie_Factura.Size = New System.Drawing.Size(96, 20)
        Me.txtSerie_Factura.TabIndex = 5
        Me.txtSerie_Factura.Tag = "serie_factura"
        '
        'lblRepartir
        '
        Me.lblRepartir.AutoSize = True
        Me.lblRepartir.Location = New System.Drawing.Point(45, 240)
        Me.lblRepartir.Name = "lblRepartir"
        Me.lblRepartir.Size = New System.Drawing.Size(53, 16)
        Me.lblRepartir.TabIndex = 7
        Me.lblRepartir.Tag = ""
        Me.lblRepartir.Text = "&Repartir:"
        Me.lblRepartir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(8, 19)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(45, 16)
        Me.lblChofer.TabIndex = 1
        Me.lblChofer.Tag = ""
        Me.lblChofer.Text = "&Chofer:"
        Me.lblChofer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodeguero
        '
        Me.lblBodeguero.AutoSize = True
        Me.lblBodeguero.Location = New System.Drawing.Point(30, 264)
        Me.lblBodeguero.Name = "lblBodeguero"
        Me.lblBodeguero.Size = New System.Drawing.Size(68, 16)
        Me.lblBodeguero.TabIndex = 9
        Me.lblBodeguero.Tag = ""
        Me.lblBodeguero.Text = "&Bodeguero:"
        Me.lblBodeguero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(9, 312)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 13
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 312)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(616, 32)
        Me.txtObservaciones.TabIndex = 14
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lkpChofer
        '
        Me.lkpChofer.AllowAdd = False
        Me.lkpChofer.AutoReaload = False
        Me.lkpChofer.DataSource = Nothing
        Me.lkpChofer.DefaultSearchField = ""
        Me.lkpChofer.DisplayMember = "nombre"
        Me.lkpChofer.EditValue = Nothing
        Me.lkpChofer.Enabled = False
        Me.lkpChofer.Filtered = False
        Me.lkpChofer.InitValue = Nothing
        Me.lkpChofer.Location = New System.Drawing.Point(56, 19)
        Me.lkpChofer.MultiSelect = False
        Me.lkpChofer.Name = "lkpChofer"
        Me.lkpChofer.NullText = ""
        Me.lkpChofer.PopupWidth = CType(400, Long)
        Me.lkpChofer.ReadOnlyControl = False
        Me.lkpChofer.Required = True
        Me.lkpChofer.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChofer.SearchMember = ""
        Me.lkpChofer.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChofer.SelectAll = False
        Me.lkpChofer.Size = New System.Drawing.Size(256, 20)
        Me.lkpChofer.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpChofer.TabIndex = 2
        Me.lkpChofer.Tag = "chofer"
        Me.lkpChofer.ToolTip = Nothing
        Me.lkpChofer.ValueMember = "chofer"
        '
        'lkpBodeguero
        '
        Me.lkpBodeguero.AllowAdd = False
        Me.lkpBodeguero.AutoReaload = True
        Me.lkpBodeguero.DataSource = Nothing
        Me.lkpBodeguero.DefaultSearchField = ""
        Me.lkpBodeguero.DisplayMember = "nombre"
        Me.lkpBodeguero.EditValue = Nothing
        Me.lkpBodeguero.Filtered = False
        Me.lkpBodeguero.InitValue = Nothing
        Me.lkpBodeguero.Location = New System.Drawing.Point(104, 264)
        Me.lkpBodeguero.MultiSelect = False
        Me.lkpBodeguero.Name = "lkpBodeguero"
        Me.lkpBodeguero.NullText = ""
        Me.lkpBodeguero.PopupWidth = CType(400, Long)
        Me.lkpBodeguero.ReadOnlyControl = False
        Me.lkpBodeguero.Required = False
        Me.lkpBodeguero.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodeguero.SearchMember = ""
        Me.lkpBodeguero.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodeguero.SelectAll = False
        Me.lkpBodeguero.Size = New System.Drawing.Size(288, 20)
        Me.lkpBodeguero.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodeguero.TabIndex = 10
        Me.lkpBodeguero.Tag = "bodeguero"
        Me.lkpBodeguero.ToolTip = Nothing
        Me.lkpBodeguero.ValueMember = "bodeguero"
        '
        'lblFechaFactura
        '
        Me.lblFechaFactura.AutoSize = True
        Me.lblFechaFactura.Location = New System.Drawing.Point(16, 94)
        Me.lblFechaFactura.Name = "lblFechaFactura"
        Me.lblFechaFactura.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaFactura.TabIndex = 6
        Me.lblFechaFactura.Tag = ""
        Me.lblFechaFactura.Text = "&Fecha de Factura:"
        Me.lblFechaFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaFactura
        '
        Me.dteFechaFactura.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaFactura.Location = New System.Drawing.Point(128, 94)
        Me.dteFechaFactura.Name = "dteFechaFactura"
        '
        'dteFechaFactura.Properties
        '
        Me.dteFechaFactura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFactura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.Enabled = False
        Me.dteFechaFactura.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaFactura.TabIndex = 7
        Me.dteFechaFactura.Tag = "fecha_factura"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(16, 16)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 14
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCliente
        '
        Me.txtCliente.EditValue = ""
        Me.txtCliente.Location = New System.Drawing.Point(64, 16)
        Me.txtCliente.Name = "txtCliente"
        '
        'txtCliente.Properties
        '
        Me.txtCliente.Properties.Enabled = False
        Me.txtCliente.Size = New System.Drawing.Size(248, 20)
        Me.txtCliente.TabIndex = 15
        '
        'cboRepartir
        '
        Me.cboRepartir.EditValue = 1
        Me.cboRepartir.Location = New System.Drawing.Point(104, 240)
        Me.cboRepartir.Name = "cboRepartir"
        '
        'cboRepartir.Properties
        '
        Me.cboRepartir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboRepartir.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mañana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1)})
        Me.cboRepartir.Size = New System.Drawing.Size(96, 23)
        Me.cboRepartir.TabIndex = 8
        Me.cboRepartir.Tag = "repartir"
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(64, 40)
        Me.txtDomicilio.Name = "txtDomicilio"
        Me.txtDomicilio.Size = New System.Drawing.Size(248, 20)
        Me.txtDomicilio.TabIndex = 17
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(4, 40)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 16
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(64, 64)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(248, 20)
        Me.txtColonia.TabIndex = 19
        Me.txtColonia.Tag = "colonia"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(13, 64)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 18
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "&Colonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grArticulos
        '
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Location = New System.Drawing.Point(16, 352)
        Me.grArticulos.MainView = Me.grvArticulos
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRepartir, Me.chkManejaSeries, Me.txtNumeroSerie, Me.chkSobrePedido})
        Me.grArticulos.Size = New System.Drawing.Size(704, 168)
        Me.grArticulos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Control, System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.TabIndex = 17
        Me.grArticulos.Text = "Articulos"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcRepartir, Me.grcPartida, Me.grcArticulo, Me.grcModelo, Me.grcDescripcion, Me.grcCantidad, Me.grcBodegaNombre, Me.grcBodega, Me.grcPartidaFactura, Me.grcManejaSeries, Me.grcNumeroSerie, Me.GridColumn1, Me.GridColumn2, Me.GridColumn4, Me.grcSobrePedido, Me.grcEntrada, Me.grcbodega_entrada})
        Me.grvArticulos.GridControl = Me.grArticulos
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsCustomization.AllowFilter = False
        Me.grvArticulos.OptionsCustomization.AllowGroup = False
        Me.grvArticulos.OptionsCustomization.AllowSort = False
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        Me.grvArticulos.OptionsView.ShowIndicator = False
        '
        'grcRepartir
        '
        Me.grcRepartir.Caption = "Repartir"
        Me.grcRepartir.ColumnEdit = Me.chkRepartir
        Me.grcRepartir.FieldName = "repartir"
        Me.grcRepartir.Name = "grcRepartir"
        Me.grcRepartir.VisibleIndex = 0
        Me.grcRepartir.Width = 51
        '
        'chkRepartir
        '
        Me.chkRepartir.AutoHeight = False
        Me.chkRepartir.Name = "chkRepartir"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Artículo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.Width = 72
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 137
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripción"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 2
        Me.grcDescripcion.Width = 215
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cant"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 38
        '
        'grcBodegaNombre
        '
        Me.grcBodegaNombre.Caption = "Bodega"
        Me.grcBodegaNombre.FieldName = "nombre_bodega"
        Me.grcBodegaNombre.Name = "grcBodegaNombre"
        Me.grcBodegaNombre.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodegaNombre.VisibleIndex = 4
        Me.grcBodegaNombre.Width = 150
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodega.Width = 238
        '
        'grcPartidaFactura
        '
        Me.grcPartidaFactura.Caption = "Partida en la Factura"
        Me.grcPartidaFactura.FieldName = "partida_factura"
        Me.grcPartidaFactura.Name = "grcPartidaFactura"
        Me.grcPartidaFactura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcManejaSeries
        '
        Me.grcManejaSeries.Caption = "Maneja Series"
        Me.grcManejaSeries.ColumnEdit = Me.chkManejaSeries
        Me.grcManejaSeries.FieldName = "maneja_series"
        Me.grcManejaSeries.Name = "grcManejaSeries"
        Me.grcManejaSeries.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.AutoHeight = False
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "Numero de Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.VisibleIndex = 5
        Me.grcNumeroSerie.Width = 111
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Requiere Instalación"
        Me.GridColumn1.FieldName = "requiere_instalacion"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.Width = 59
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Requiere Armado"
        Me.GridColumn2.FieldName = "requiere_armado"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.Width = 72
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Partida_factura"
        Me.GridColumn4.FieldName = "partida_factura"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSobrePedido
        '
        Me.grcSobrePedido.Caption = "PF"
        Me.grcSobrePedido.ColumnEdit = Me.chkSobrePedido
        Me.grcSobrePedido.FieldName = "sobrepedido"
        Me.grcSobrePedido.Name = "grcSobrePedido"
        Me.grcSobrePedido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSobrePedido.VisibleIndex = 6
        '
        'chkSobrePedido
        '
        Me.chkSobrePedido.AutoHeight = False
        Me.chkSobrePedido.Name = "chkSobrePedido"
        '
        'grcEntrada
        '
        Me.grcEntrada.Caption = "Entrada"
        Me.grcEntrada.FieldName = "entrada"
        Me.grcEntrada.Name = "grcEntrada"
        Me.grcEntrada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntrada.VisibleIndex = 7
        '
        'grcbodega_entrada
        '
        Me.grcbodega_entrada.Caption = "Bod. Ent."
        Me.grcbodega_entrada.FieldName = "bodega_entrada"
        Me.grcbodega_entrada.Name = "grcbodega_entrada"
        Me.grcbodega_entrada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcbodega_entrada.VisibleIndex = 8
        '
        'txtNumeroSerie
        '
        Me.txtNumeroSerie.AutoHeight = False
        Me.txtNumeroSerie.Name = "txtNumeroSerie"
        '
        'lkpFactura
        '
        Me.lkpFactura.AllowAdd = False
        Me.lkpFactura.AutoReaload = False
        Me.lkpFactura.DataSource = Nothing
        Me.lkpFactura.DefaultSearchField = ""
        Me.lkpFactura.DisplayMember = "clave_compuesta"
        Me.lkpFactura.EditValue = Nothing
        Me.lkpFactura.Filtered = True
        Me.lkpFactura.InitValue = Nothing
        Me.lkpFactura.Location = New System.Drawing.Point(128, 22)
        Me.lkpFactura.MultiSelect = False
        Me.lkpFactura.Name = "lkpFactura"
        Me.lkpFactura.NullText = ""
        Me.lkpFactura.PopupWidth = CType(400, Long)
        Me.lkpFactura.ReadOnlyControl = False
        Me.lkpFactura.Required = False
        Me.lkpFactura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFactura.SearchMember = ""
        Me.lkpFactura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFactura.SelectAll = False
        Me.lkpFactura.Size = New System.Drawing.Size(216, 20)
        Me.lkpFactura.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFactura.TabIndex = 1
        Me.lkpFactura.Tag = "folio_factura"
        Me.lkpFactura.ToolTip = Nothing
        Me.lkpFactura.ValueMember = "clave_compuesta"
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(64, 88)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(248, 20)
        Me.txtCiudad.TabIndex = 21
        Me.txtCiudad.Tag = "ciudad"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Ciudad:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(64, 112)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(248, 20)
        Me.txtEstado.TabIndex = 23
        Me.txtEstado.Tag = "estado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 16)
        Me.Label3.TabIndex = 22
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Estado:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 24
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Teléfono:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono
        '
        Me.txtTelefono.EditValue = ""
        Me.txtTelefono.Location = New System.Drawing.Point(64, 136)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(248, 20)
        Me.txtTelefono.TabIndex = 25
        Me.txtTelefono.Tag = "telefono"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtDomicilio)
        Me.GroupBox1.Controls.Add(Me.lblDomicilio)
        Me.GroupBox1.Controls.Add(Me.txtColonia)
        Me.GroupBox1.Controls.Add(Me.lblColonia)
        Me.GroupBox1.Controls.Add(Me.txtEstado)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Me.txtCiudad)
        Me.GroupBox1.Controls.Add(Me.lblCliente)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(400, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(320, 168)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSucursal_Factura)
        Me.GroupBox2.Controls.Add(Me.lblFolio_Factura)
        Me.GroupBox2.Controls.Add(Me.lblSerie_Factura)
        Me.GroupBox2.Controls.Add(Me.txtSerie_Factura)
        Me.GroupBox2.Controls.Add(Me.lblFechaFactura)
        Me.GroupBox2.Controls.Add(Me.lkpFactura)
        Me.GroupBox2.Controls.Add(Me.dteFechaFactura)
        Me.GroupBox2.Controls.Add(Me.txtSucursal_Factura)
        Me.GroupBox2.Location = New System.Drawing.Point(32, 96)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(352, 128)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Factura"
        '
        'txtSucursal_Factura
        '
        Me.txtSucursal_Factura.EditValue = ""
        Me.txtSucursal_Factura.Location = New System.Drawing.Point(128, 46)
        Me.txtSucursal_Factura.Name = "txtSucursal_Factura"
        '
        'txtSucursal_Factura.Properties
        '
        Me.txtSucursal_Factura.Properties.Enabled = False
        Me.txtSucursal_Factura.Properties.MaxLength = 3
        Me.txtSucursal_Factura.Size = New System.Drawing.Size(216, 20)
        Me.txtSucursal_Factura.TabIndex = 3
        Me.txtSucursal_Factura.Tag = ""
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkChofer)
        Me.GroupBox3.Controls.Add(Me.lkpChofer)
        Me.GroupBox3.Controls.Add(Me.lblChofer)
        Me.GroupBox3.Location = New System.Drawing.Point(400, 236)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(320, 48)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        '
        'chkChofer
        '
        Me.chkChofer.Location = New System.Drawing.Point(16, -2)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(64, 16)
        Me.chkChofer.TabIndex = 0
        Me.chkChofer.Text = "Ch&ofer"
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(104, 288)
        Me.lkpBodegaSalida.MultiSelect = False
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = ""
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = True
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(288, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 12
        Me.lkpBodegaSalida.Tag = "bodega_entrega"
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(1, 288)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(97, 16)
        Me.lblBodega_Salida.TabIndex = 11
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "&Bodega Entrega:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(408, 288)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 17)
        Me.Label5.TabIndex = 59
        Me.Label5.Tag = ""
        Me.Label5.Text = "Vendedor:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNombreVendedor
        '
        Me.lblNombreVendedor.AutoSize = True
        Me.lblNombreVendedor.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreVendedor.ForeColor = System.Drawing.Color.Blue
        Me.lblNombreVendedor.Location = New System.Drawing.Point(488, 288)
        Me.lblNombreVendedor.Name = "lblNombreVendedor"
        Me.lblNombreVendedor.Size = New System.Drawing.Size(28, 17)
        Me.lblNombreVendedor.TabIndex = 60
        Me.lblNombreVendedor.Tag = ""
        Me.lblNombreVendedor.Text = "Ven"
        Me.lblNombreVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmRepartos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(730, 528)
        Me.Controls.Add(Me.lblNombreVendedor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lkpBodegaSalida)
        Me.Controls.Add(Me.lblBodega_Salida)
        Me.Controls.Add(Me.grArticulos)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboRepartir)
        Me.Controls.Add(Me.lkpBodeguero)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblReparto)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblRepartir)
        Me.Controls.Add(Me.lblBodeguero)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.clcReparto)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.txtObservaciones)
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmRepartos"
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcReparto, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblBodeguero, 0)
        Me.Controls.SetChildIndex(Me.lblRepartir, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblReparto, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpBodeguero, 0)
        Me.Controls.SetChildIndex(Me.cboRepartir, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.grArticulos, 0)
        Me.Controls.SetChildIndex(Me.lblBodega_Salida, 0)
        Me.Controls.SetChildIndex(Me.lkpBodegaSalida, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblNombreVendedor, 0)
        CType(Me.clcReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRepartir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroSerie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSucursal_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oRepartos As VillarrealBusiness.clsRepartos
    Private oRepartosDetalles As VillarrealBusiness.clsRepartosDetalle
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oChoferes As VillarrealBusiness.clsChoferes
    Private oBodegueros As VillarrealBusiness.clsBodegueros
    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oMovimientosInventariosDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oarticulosexistencias As VillarrealBusiness.clsArticulosExistencias

    Private ParcheSinSeries As Boolean = True ' su valor default debe ser False pero por motivos fuera de lo comun sera TRUE

    Private lCliente As Long = 0
    Private lsucursal As Long = 0
    Private lfolioFactura As Long = 0
    Private lfecha_factura As DateTime
    Private entro_Accept As Boolean = False
    Private abortar_transaccion As Boolean = False

    Private ReadOnly Property Chofer() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpChofer)
        End Get
    End Property
    Private ReadOnly Property Bodeguero() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBodeguero)
        End Get
    End Property
    Private ReadOnly Property SucursalFactura() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property SerieFactura() As String
        Get
            Return CStr(Me.lkpFactura.GetValue("serie"))
        End Get
    End Property
    Private ReadOnly Property Factura() As Long
        Get
            Return CLng(Me.lkpFactura.GetValue("folio"))
        End Get
    End Property
    Private ReadOnly Property ConceptoVenta() As String
        Get
            Return oVariables.TraeDatos("concepto_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property BodegaEntrega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepartos_AbortUpdate() Handles MyBase.AbortUpdate
        If Not abortar_transaccion Then TINApp.Connection.Rollback()
    End Sub
    Private Sub frmRepartos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmRepartos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        ImprimeValeReparto()

        Comunes.Common.Bodeguero_Repartos = Me.Bodeguero
    End Sub

    Private Sub frmRepartos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        ' SE INSERTA EL ENCABEZADO DEL REPARTO 
        Dim orow As DataRow
        entro_Accept = True

        Response = oRepartos.Insertar(Me.DataSource, Me.dteFecha.Text, Me.clcReparto.EditValue, Comunes.Common.Sucursal_Actual, "RE", SucursalFactura, Factura)

        ' SE INSERTA EL DETALLE DEL REPARTO
        For Each orow In CType(Me.grArticulos.DataSource, DataTable).Rows
            If Not Response.ErrorFound And orow("repartir") = True Then

                Response = oRepartosDetalles.Insertar(SucursalFactura, Me.clcReparto.EditValue, orow("partida"), orow("articulo"), orow("cantidad"), orow("bodega"), SerieFactura, Factura, orow("partida_factura"), orow("numero_serie"))

                '@ACH-21/06/07: Se eliminó el proceso de Generar una Orden de Servicio al generar un Reparto.
                'If orow("requiere_armado") Then
                '    ' SI TIENE UN ARMADO  SE GENERA LA ORDEN DE SERVICIO POR ARTICULO
                '    GeneraOrdenServicio(Response, "A", Me.dteFecha.DateTime, orow("articulo"), orow("numero_serie"), lCliente, Me.txtDomicilio.Text, SucursalFactura, Me.SerieFactura, Me.lfolioFactura, orow("partida_factura"), lfecha_factura)
                'End If

                'If orow("requiere_instalacion") Then
                '    ' SI TIENE UNA INSTALACION SE GENERA LA ORDEN DE SERVICIO POR ARTICULO
                '    GeneraOrdenServicio(Response, "I", Me.dteFecha.DateTime, orow("articulo"), orow("numero_serie"), lCliente, Me.txtDomicilio.Text, SucursalFactura, Me.SerieFactura, Me.lfolioFactura, orow("partida_factura"), lfecha_factura)
                'End If

                '/@ACH-21/06/07

               
                'SE INSERTA EL NUMERO DE SERIE EN MOVIMIENTOS_INVENTARIO_DETALLE_SERIES SI EL ARTICULO MANEJA SERIES
                If Not Response.ErrorFound And orow("maneja_series") = True _
                And orow("validanumeroserie") = True Then

                    If Not Response.ErrorFound And orow("entro_pantalla_reparto_validaserie") = 1 Then

                        Response = Me.oMovimientosInventariosDetalleSeries.EliminarSerieMVC(orow("articulo"), orow("numero_serie"))

                        ' CODIGO PARA MOSTRAR UN MESAJE PERSONALIZADO SI HUBO UN ERROR 
                        If Response.ErrorFound Then
                            If CType(CType(CType(Response.Ex, System.Exception).InnerException, System.Exception), System.Data.SqlClient.SqlException).Number = 515 Then
                                abortar_transaccion = True
                                Response.Message = "NO HAY UNA SERIE VALIDA DEL INVENTARIO INICIAL PARA EL ARTICULO:" & CType(orow("ARTICULO"), String)
                            End If
                        End If

                    End If

                    If Not Response.ErrorFound Then Response = oMovimientosInventariosDetalleSeries.InsertarSerieArticuloReparto(SucursalFactura, SerieFactura, Factura, orow("numero_serie"), orow("articulo"))
                Else
                    If ParcheSinSeries = True Then
                        If Not Response.ErrorFound Then Response = oMovimientosInventariosDetalleSeries.InsertarSerieArticuloReparto(SucursalFactura, SerieFactura, Factura, orow("numero_serie"), orow("articulo"))
                    End If
                End If
            End If
        Next

        entro_Accept = False
    End Sub
    Private Sub frmRepartos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oRepartos.DespliegaDatos(OwnerForm.Value("reparto"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub
    Private Sub frmRepartos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oRepartos = New VillarrealBusiness.clsRepartos
        oRepartosDetalles = New VillarrealBusiness.clsRepartosDetalle
        oSucursales = New VillarrealBusiness.clsSucursales
        oChoferes = New VillarrealBusiness.clsChoferes
        oBodegueros = New VillarrealBusiness.clsBodegueros
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oVariables = New VillarrealBusiness.clsVariables
        oBodegas = New VillarrealBusiness.clsBodegas
        oMovimientosInventariosDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries
        oarticulosexistencias = New VillarrealBusiness.clsArticulosExistencias

        Me.dteFecha.DateTime = Comunes.clsUtilerias.FechaServidor

        If Me.dteFecha.DateTime.TimeOfDay.Hours > 13 Then
            Me.cboRepartir.SelectedIndex = 1
        End If

        If Comunes.Common.Bodeguero_Repartos > 0 Then
            Me.lkpBodeguero.EditValue = Comunes.Common.Bodeguero_Repartos
        End If

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmRepartos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateCurrentRow()

        If Me.chkChofer.Checked = True Then
            Response = oRepartos.Validacion(Action, SucursalFactura, dteFecha.Text, Me.lkpFactura.EditValue, Me.cboRepartir.EditValue, Chofer, Bodeguero, grArticulos.DataSource, BodegaEntrega)
        Else
            Response = oRepartos.Validacion(Action, SucursalFactura, dteFecha.Text, Me.lkpFactura.EditValue, Me.cboRepartir.EditValue, Bodeguero, grArticulos.DataSource, BodegaEntrega)
        End If


        If Not Response.ErrorFound Then
            Dim arrRows() As DataRow
            Dim oRowHijo As DataRow

            arrRows = CType(Me.grArticulos.DataSource, DataTable).Select("repartir = true")

            For Each oRowHijo In arrRows

                If Me.BodegaEntrega <> oRowHijo("bodega") Then
                    If Not (CType(oRowHijo("sobrepedido"), Boolean) = True And CType(oRowHijo("entrada"), Long) > 0 And CType(oRowHijo("bodega_entrada"), String) = Me.BodegaEntrega) Then
                        Response = oarticulosexistencias.ValidaExistenciaArticuloBodega(oRowHijo("articulo"), Me.BodegaEntrega, oRowHijo("cantidad"))
                    End If
                Else

                    'Si son bodegas iguales, si es un pedido a fabrica y no tiene una entrada 
                    ' Se valida las existencias
                    If CType(oRowHijo("sobrepedido"), Boolean) = True And CType(oRowHijo("entrada"), Long) = 0 Then
                        Response = oarticulosexistencias.ValidaExistenciaArticuloBodega(oRowHijo("articulo"), Me.BodegaEntrega, oRowHijo("cantidad"))
                    End If

                End If


                If oRowHijo("maneja_series") And Not Response.ErrorFound Then
                    Response = oRepartosDetalles.Validacion(oRowHijo("maneja_series"), oRowHijo("numero_serie"), oRowHijo("articulo"), oRowHijo("validanumeroserie"))
                End If
            Next
        End If

    End Sub

    Private Sub frmRepartos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpChofer_Format() Handles lkpChofer.Format
        for_choferes_grl(Me.lkpChofer)
    End Sub
    Private Sub lkpChofer_LoadData(ByVal Initialize As Boolean) Handles lkpChofer.LoadData
        Dim response As Events
        response = oChoferes.Lookup(1)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChofer.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged

        Me.lkpFactura.EditValue = Nothing
        Me.lkpFactura_LoadData(True)
    End Sub

    Private Sub lkpBodeguero_Format() Handles lkpBodeguero.Format
        Comunes.clsFormato.for_bodegueros_grl(Me.lkpBodeguero)
    End Sub
    Private Sub lkpBodeguero_LoadData(ByVal Initialize As Boolean) Handles lkpBodeguero.LoadData
        Dim response As Events
        response = oBodegueros.Lookup(Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodeguero.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

    Private Sub lkpFactura_Format() Handles lkpFactura.Format
        Comunes.clsFormato.for_ventas_facturas_grl(Me.lkpFactura)
    End Sub
    Private Sub lkpFactura_LoadData(ByVal Initialize As Boolean) Handles lkpFactura.LoadData
        Dim response As Events
        response = oVentas.LookupFacturas(SucursalFactura)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFactura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpFactura_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFactura.EditValueChanged
        '@ACH-21/06/07: Se modificó el txtCliente para que mostrará ambas: la clave y el cliente.
        ' Me.txtCliente.Text = Me.lkpFactura.GetValue("nombre_cliente")
        Me.txtCliente.Text = CType(Me.lkpFactura.GetValue("cliente"), String) + " - " + Me.lkpFactura.GetValue("nombre_cliente")
        '/@ACH-21/06/07
        Me.txtSerie_Factura.Text = Me.lkpFactura.GetValue("serie")
        Me.dteFechaFactura.EditValue = Me.lkpFactura.GetValue("fecha")
        Me.txtSucursal_Factura.Text = Me.lkpFactura.GetValue("nombre_sucursal")
        Me.lkpSucursal.EditValue = Me.lkpFactura.GetValue("sucursal")
        Me.txtDomicilio.Text = Me.lkpFactura.GetValue("domicilio_reparto")
        Me.txtColonia.Text = Me.lkpFactura.GetValue("colonia_reparto")
        Me.txtCiudad.Text = Me.lkpFactura.GetValue("ciudad_reparto")
        Me.txtEstado.Text = Me.lkpFactura.GetValue("estado_reparto")
        Me.txtTelefono.Text = Me.lkpFactura.GetValue("telefono_reparto")
        Me.txtObservaciones.Text = Me.lkpFactura.GetValue("observaciones_reparto")
        Me.lblNombreVendedor.Text = Me.lkpFactura.GetValue("vendedor")

        Factura_Entregada = Me.lkpFactura.GetValue("entregada")
        lCliente = Me.lkpFactura.GetValue("cliente")
        lsucursal = Me.lkpFactura.GetValue("sucursal")
        lfolioFactura = Me.lkpFactura.GetValue("folio")
        lfecha_factura = Me.lkpFactura.GetValue("fecha_factura")

        CargarArticulos(Factura_Entregada)

        Me.txtObservaciones.Focus()
    End Sub

    Private Sub chkChofer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChofer.CheckedChanged
        If Me.chkChofer.Checked = True Then
            Me.lkpChofer.Enabled = True
        Else
            Me.lkpChofer.Enabled = False
            Me.lkpChofer.EditValue = Nothing
        End If
    End Sub

    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged

        CargarArticulos(Factura_Entregada)

    End Sub
    Private Sub lkpBodegaSalida_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles lkpBodegaSalida.Validating
        If lkpBodegaSalida.DisplayText = Nothing Then
            lkpBodegaSalida.EditValue = Nothing
        End If
    End Sub

    Private Sub grvArticulos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvArticulos.CellValueChanging


        If e.Column.FieldName = "repartir" Then
            If lkpBodegaSalida.EditValue = Nothing Then
                grvArticulos.CloseEditor()
                grvArticulos.UpdateCurrentRow()
                MsgBox("Debe seleccionar una Bodega Entrega", MsgBoxStyle.Information)
                CType(grvArticulos.DataSource, DataView).Item(grvArticulos.FocusedRowHandle).Item("repartir") = False
            End If
        End If


    End Sub
    Private Sub grvArticulos_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvArticulos.CellValueChanged
        RevisarNumeroSerie(e.Column.FieldName, e.RowHandle)
    End Sub
    Private Sub grvArticulos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvArticulos.KeyDown
        Dim FILA As Long = Me.grvArticulos.FocusedRowHandle()
        CType(Me.grArticulos.DataSource, DataTable).Rows(FILA).Item("validanumeroserie") = False

        'If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
        '    RevisarNumeroSerie("numero_serie", e.Handled)
        'End If
    End Sub
    Private Sub grvArticulos_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvArticulos.FocusedRowChanged
        'If CType(Me.grArticulos.DataSource, DataTable).Rows(e.FocusedRowHandle).Item("maneja_series") = False Then
        'Comunes.clsUtilerias.InhabilitaColumnas(Me.grcNumeroSerie)
        'Else
        Comunes.clsUtilerias.HablilitaColumnas(Me.grcNumeroSerie)
        'End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarArticulos(ByVal Factura_Entregada As Boolean)
        Dim Response As Dipros.Utils.Events

        Response = oVentasDetalle.ListadoPendientesRepartir2(SucursalFactura, Me.txtSerie_Factura.Text, Factura)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            'If oDataSet.Tables(0).Rows.Count = 0 And Factura_Entregada = True Then
            '    ShowMessage(MessageType.MsgInformation, "Los Articulos ya fueron Entregado", "Repartos")
            'End If

            'If oDataSet.Tables(0).Rows.Count = 0 And Factura_Entregada = False Then
            '    ShowMessage(MessageType.MsgInformation, "No hay Articulos a Repartir", "Repartos")
            'End If

            Me.grArticulos.DataSource = oDataSet.Tables(0)

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.grvArticulos.FocusedRowHandle = 0
            End If

            oDataSet = Nothing
        End If
    End Sub
    Private Sub ImprimeValeReparto()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeDeReparto(Me.clcReparto.EditValue, Me.chkChofer.Checked)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Vale de Reparto no puede Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptRepartoVale

                    If Me.chkChofer.Checked = False Then
                        oReport.lineChofer.Visible = False
                    End If


                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                    TINApp.ShowReport(Me.MdiParent, "Vale de Reparto", oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If

            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub
    Public Sub LLenarNumeroSerieValido(ByVal fila As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal validanumeroserie As Boolean)

        CType(Me.grArticulos.DataSource, DataTable).Rows(fila).Item("validanumeroserie") = validanumeroserie
        If validanumeroserie Then
            grvArticulos.SetRowCellValue(fila, Me.grcNumeroSerie, numero_serie)
            CType(Me.grArticulos.DataSource, DataTable).Rows(fila).Item("entro_pantalla_reparto_validaserie") = 1
        End If
        Me.grvArticulos.RefreshEditor(True)

    End Sub
    '@ACH-21/06/07: Se eliminó el proceso de Generar una Orden de Servicio al generar un Reparto.
    'Private Sub GeneraOrdenServicio(ByRef response As Events, ByVal tipo_servicio As Char, ByVal fecha As DateTime, ByVal articulo As Long, _
    'ByVal numero_serie As String, ByVal cliente As Long, ByVal direccion_cliente As String, ByVal sucursal_factura As Long, _
    'ByVal serie_factura As String, ByVal folio_factura As Long, ByVal partida_factura As Long, ByVal fecha_factura As DateTime)
    '    Dim oservicio As New VillarrealBusiness.clsOrdenesServicio
    '    Dim orden_servicio As Long = 0
    '    response = oservicio.InsertarOSReparto(orden_servicio, Comunes.Common.Sucursal_Actual, tipo_servicio, fecha, _
    '    articulo, numero_serie, cliente, direccion_cliente, sucursal_factura, serie_factura, folio_factura, partida_factura, fecha_factura)
    'End Sub
    '/@ACH-21/06/07
    Private Sub RevisarNumeroSerie(ByVal fieldname As String, ByVal rowhandle As Long)

        If fieldname = "numero_serie" And entro_Accept = False Then
            Dim response As Events
            Dim oRowHijo As DataRow

            oRowHijo = CType(Me.grArticulos.DataSource, DataTable).Rows(rowhandle)

            If oRowHijo("maneja_series") And oRowHijo("validanumeroserie") = False Then
                response = oRepartosDetalles.Validacion(Action, oRowHijo("maneja_series"), oRowHijo("numero_serie"), oRowHijo("articulo"), Comunes.clsUtilerias.uti_hisSeries(oRowHijo("articulo"), oRowHijo("numero_serie"), "S", oRowHijo("bodega")))

                If response.ErrorFound Then
                    response.ShowMessage()
                    Dim OFORM As New Comunes.frmRepartosValidaNumeroSerie
                    OFORM.Articulo = oRowHijo("articulo")
                    OFORM.modelo = oRowHijo("modelo")
                    OFORM.Fila = rowhandle

                    OFORM.OwnerForm = Me
                    OFORM.MdiParent = Me.MdiParent
                    Me.Enabled = False
                    OFORM.Show()
                Else
                    'MARCO COMO VALIDO SI EL NUMERO DE SERIE PASO LA REVISION EN LA TABLA
                    CType(Me.grArticulos.DataSource, DataTable).Rows(rowhandle).Item("validanumeroserie") = True
                End If

            End If

        End If
    End Sub

#End Region

 
End Class
