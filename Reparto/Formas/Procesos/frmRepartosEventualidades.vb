Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepartosEventualidades
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys

    ReadOnly Property Eventualidad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEventualidad)
        End Get
    End Property
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
    Friend WithEvents lblEventualidad As System.Windows.Forms.Label

		Friend WithEvents lblFecha As System.Windows.Forms.Label
		Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblCapturo As System.Windows.Forms.Label
		Friend WithEvents txtCapturo As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblObservaciones As System.Windows.Forms.Label
		Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpEventualidad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionEventualidad As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tmEditFecha As DevExpress.XtraEditors.TimeEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepartosEventualidades))
        Me.lblEventualidad = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblCapturo = New System.Windows.Forms.Label
        Me.txtCapturo = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lkpEventualidad = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lblDescripcionEventualidad = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.tmEditFecha = New DevExpress.XtraEditors.TimeEdit
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEditFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(229, 28)
        '
        'lblEventualidad
        '
        Me.lblEventualidad.AutoSize = True
        Me.lblEventualidad.Location = New System.Drawing.Point(19, 40)
        Me.lblEventualidad.Name = "lblEventualidad"
        Me.lblEventualidad.Size = New System.Drawing.Size(80, 16)
        Me.lblEventualidad.TabIndex = 0
        Me.lblEventualidad.Tag = ""
        Me.lblEventualidad.Text = "&Eventualidad:"
        Me.lblEventualidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(58, 64)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(104, 64)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(84, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblCapturo
        '
        Me.lblCapturo.AutoSize = True
        Me.lblCapturo.Location = New System.Drawing.Point(46, 88)
        Me.lblCapturo.Name = "lblCapturo"
        Me.lblCapturo.Size = New System.Drawing.Size(53, 16)
        Me.lblCapturo.TabIndex = 6
        Me.lblCapturo.Tag = ""
        Me.lblCapturo.Text = "&Capturó:"
        Me.lblCapturo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCapturo
        '
        Me.txtCapturo.EditValue = ""
        Me.txtCapturo.Location = New System.Drawing.Point(104, 88)
        Me.txtCapturo.Name = "txtCapturo"
        '
        'txtCapturo.Properties
        '
        Me.txtCapturo.Properties.Enabled = False
        Me.txtCapturo.Properties.MaxLength = 15
        Me.txtCapturo.Properties.ReadOnly = True
        Me.txtCapturo.Size = New System.Drawing.Size(224, 20)
        Me.txtCapturo.TabIndex = 7
        Me.txtCapturo.Tag = "capturo"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(10, 112)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 8
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 112)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(224, 38)
        Me.txtObservaciones.TabIndex = 9
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lkpEventualidad
        '
        Me.lkpEventualidad.AllowAdd = False
        Me.lkpEventualidad.AutoReaload = False
        Me.lkpEventualidad.DataSource = Nothing
        Me.lkpEventualidad.DefaultSearchField = ""
        Me.lkpEventualidad.DisplayMember = "descripcion"
        Me.lkpEventualidad.EditValue = Nothing
        Me.lkpEventualidad.Filtered = False
        Me.lkpEventualidad.InitValue = Nothing
        Me.lkpEventualidad.Location = New System.Drawing.Point(104, 40)
        Me.lkpEventualidad.MultiSelect = False
        Me.lkpEventualidad.Name = "lkpEventualidad"
        Me.lkpEventualidad.NullText = ""
        Me.lkpEventualidad.PopupWidth = CType(400, Long)
        Me.lkpEventualidad.Required = False
        Me.lkpEventualidad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEventualidad.SearchMember = ""
        Me.lkpEventualidad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEventualidad.SelectAll = False
        Me.lkpEventualidad.Size = New System.Drawing.Size(224, 20)
        Me.lkpEventualidad.TabIndex = 1
        Me.lkpEventualidad.Tag = "eventualidad"
        Me.lkpEventualidad.ToolTip = Nothing
        Me.lkpEventualidad.ValueMember = "eventualidad"
        '
        'lblGrupo
        '
        Me.lblGrupo.Location = New System.Drawing.Point(40, 168)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.TabIndex = 59
        Me.lblGrupo.Tag = "grupo"
        '
        'lblDescripcionEventualidad
        '
        Me.lblDescripcionEventualidad.Location = New System.Drawing.Point(176, 168)
        Me.lblDescripcionEventualidad.Name = "lblDescripcionEventualidad"
        Me.lblDescripcionEventualidad.TabIndex = 60
        Me.lblDescripcionEventualidad.Tag = "descripcion"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(201, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "Hora:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label5.Visible = False
        '
        'tmEditFecha
        '
        Me.tmEditFecha.EditValue = New Date(2006, 4, 12, 0, 0, 0, 0)
        Me.tmEditFecha.Location = New System.Drawing.Point(240, 64)
        Me.tmEditFecha.Name = "tmEditFecha"
        '
        'tmEditFecha.Properties
        '
        Me.tmEditFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.tmEditFecha.Properties.UseCtrlIncrement = False
        Me.tmEditFecha.Size = New System.Drawing.Size(88, 20)
        Me.tmEditFecha.TabIndex = 5
        '
        'frmRepartosEventualidades
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(346, 160)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tmEditFecha)
        Me.Controls.Add(Me.lblDescripcionEventualidad)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpEventualidad)
        Me.Controls.Add(Me.lblEventualidad)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCapturo)
        Me.Controls.Add(Me.txtCapturo)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Name = "frmRepartosEventualidades"
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtCapturo, 0)
        Me.Controls.SetChildIndex(Me.lblCapturo, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblEventualidad, 0)
        Me.Controls.SetChildIndex(Me.lkpEventualidad, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionEventualidad, 0)
        Me.Controls.SetChildIndex(Me.tmEditFecha, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCapturo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEditFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oEventualidades As VillarrealBusiness.clsEventualidades
    Private oRepartosEventualidades As New VillarrealBusiness.clsRepartosEventualidades
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmRepartosEventualidades_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    Me.lblGrupo.Text = Me.lkpEventualidad.GetValue("grupo")
                    Me.lblDescripcionEventualidad.Text = Me.lkpEventualidad.GetValue("descripcion")
                    Dim a As String
                    a = Me.dteFecha.DateTime.Day.ToString + "-" + Me.dteFecha.DateTime.Month.ToString + "-" + Me.dteFecha.DateTime.Year.ToString + "  "
                    If Me.tmEditFecha.Time.Hour > 0 Or Me.tmEditFecha.Time.Minute > 0 Or Me.tmEditFecha.Time.Second > 0 Then
                        a = a + Me.tmEditFecha.Time.Hour.ToString + ":" + Me.tmEditFecha.Time.Minute.ToString + ":" + Me.tmEditFecha.Time.Second.ToString
                    End If


                    Me.dteFecha.DateTime = CType(a, Date)

                    .AddRow(Me.DataSource)
                Case Actions.Update
                        .UpdateRow(Me.DataSource)
                Case Actions.Delete
                        .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmRepartosEventualidades_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.tmEditFecha.Time = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("fecha")
    End Sub

    Private Sub frmRepartosEventualidades_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oEventualidades = New VillarrealBusiness.clsEventualidades

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
        Me.tmEditFecha.EditValue = CDate(TINApp.FechaServidor)
        Me.txtCapturo.Text = CStr(TINApp.Connection.User)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpEventualidad.Enabled = False
                Me.tmEditFecha.Enabled = False
                Me.dteFecha.Enabled = False
                Me.txtObservaciones.Enabled = False

            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmRepartosEventualidades_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = Me.oRepartosEventualidades.Validacion(Action, Eventualidad, Me.txtObservaciones.Text)
    End Sub



#End Region

#Region "DIPROS Systems, Eventos de Controles"
    'Private Sub clcEventualidad_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcEventualidad.Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oRepartosEventualidades.ValidaEventualidad(clcEventualidad.value)
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub
    'Private Sub dteFecha_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dteFecha.Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oRepartosEventualidades.ValidaFecha(dteFecha.Datetime)
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub
    'Private Sub _Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles .Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oRepartosEventualidades.ValidaCapturo()
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub
    'Private Sub txtObservaciones_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oRepartosEventualidades.ValidaObservaciones(txtObservaciones.Text)
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub

    Private Sub lkpEventualidad_Format() Handles lkpEventualidad.Format
        for_eventualidades_grl(Me.lkpEventualidad)
    End Sub
    Private Sub lkpEventualidad_LoadData(ByVal Initialize As Boolean) Handles lkpEventualidad.LoadData
        Dim response As Events
        response = oEventualidades.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpEventualidad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If
        response = Nothing

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
