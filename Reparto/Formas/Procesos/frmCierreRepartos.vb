Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCierreRepartos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grRepartos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvRepartos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcRepartir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRepartir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkManejaSeries As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents chkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents txtNumeroSerie As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursalFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerieFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tblRefrescar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteFechaEntrega As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCierreRepartos))
        Me.grRepartos = New DevExpress.XtraGrid.GridControl
        Me.grvRepartos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcRepartir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRepartir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerieFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkManejaSeries = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.txtNumeroSerie = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.chkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteFechaEntrega = New DevExpress.XtraEditors.DateEdit
        Me.tblRefrescar = New System.Windows.Forms.ToolBarButton
        CType(Me.grRepartos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRepartos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRepartir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejaSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroSerie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFechaEntrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblRefrescar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1998, 28)
        '
        'grRepartos
        '
        '
        'grRepartos.EmbeddedNavigator
        '
        Me.grRepartos.EmbeddedNavigator.Name = ""
        Me.grRepartos.Location = New System.Drawing.Point(8, 120)
        Me.grRepartos.MainView = Me.grvRepartos
        Me.grRepartos.Name = "grRepartos"
        Me.grRepartos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRepartir, Me.chkManejaSeries, Me.txtNumeroSerie, Me.chkSobrePedido})
        Me.grRepartos.Size = New System.Drawing.Size(720, 336)
        Me.grRepartos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Control, System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.TabIndex = 59
        Me.grRepartos.Text = "Articulos"
        '
        'grvRepartos
        '
        Me.grvRepartos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcRepartir, Me.grcReparto, Me.grcFecha, Me.grcSucursalFactura, Me.grcSerieFactura, Me.grcFolioFactura, Me.grcNombreCliente})
        Me.grvRepartos.GridControl = Me.grRepartos
        Me.grvRepartos.Name = "grvRepartos"
        Me.grvRepartos.OptionsCustomization.AllowFilter = False
        Me.grvRepartos.OptionsCustomization.AllowGroup = False
        Me.grvRepartos.OptionsCustomization.AllowSort = False
        Me.grvRepartos.OptionsView.ShowGroupPanel = False
        Me.grvRepartos.OptionsView.ShowIndicator = False
        '
        'grcRepartir
        '
        Me.grcRepartir.Caption = "Repartir"
        Me.grcRepartir.ColumnEdit = Me.chkRepartir
        Me.grcRepartir.FieldName = "repartir"
        Me.grcRepartir.Name = "grcRepartir"
        Me.grcRepartir.VisibleIndex = 0
        Me.grcRepartir.Width = 72
        '
        'chkRepartir
        '
        Me.chkRepartir.AutoHeight = False
        Me.chkRepartir.Name = "chkRepartir"
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.VisibleIndex = 1
        Me.grcReparto.Width = 64
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 81
        '
        'grcSucursalFactura
        '
        Me.grcSucursalFactura.Caption = "Sucursal Factura"
        Me.grcSucursalFactura.FieldName = "nombre_sucursal_factura"
        Me.grcSucursalFactura.Name = "grcSucursalFactura"
        Me.grcSucursalFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursalFactura.VisibleIndex = 4
        Me.grcSucursalFactura.Width = 99
        '
        'grcSerieFactura
        '
        Me.grcSerieFactura.Caption = "Serie Factura"
        Me.grcSerieFactura.FieldName = "serie_factura"
        Me.grcSerieFactura.Name = "grcSerieFactura"
        Me.grcSerieFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerieFactura.VisibleIndex = 5
        Me.grcSerieFactura.Width = 82
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "folio_factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        Me.grcFolioFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolioFactura.VisibleIndex = 6
        Me.grcFolioFactura.Width = 82
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Cliente"
        Me.grcNombreCliente.FieldName = "nombre_cliente"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        Me.grcNombreCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreCliente.VisibleIndex = 2
        Me.grcNombreCliente.Width = 238
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.AutoHeight = False
        Me.chkManejaSeries.Name = "chkManejaSeries"
        '
        'txtNumeroSerie
        '
        Me.txtNumeroSerie.AutoHeight = False
        Me.txtNumeroSerie.Name = "txtNumeroSerie"
        '
        'chkSobrePedido
        '
        Me.chkSobrePedido.AutoHeight = False
        Me.chkSobrePedido.Name = "chkSobrePedido"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(32, 24)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 64
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 24)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(320, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 65
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(47, 48)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 66
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(96, 48)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 67
        Me.dteFecha.Tag = "fecha"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lkpSucursal)
        Me.GroupBox1.Controls.Add(Me.dteFecha)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dteFechaEntrega)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(720, 80)
        Me.GroupBox1.TabIndex = 68
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtros"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(223, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 69
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Fecha Entrega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaEntrega
        '
        Me.dteFechaEntrega.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaEntrega.Location = New System.Drawing.Point(319, 48)
        Me.dteFechaEntrega.Name = "dteFechaEntrega"
        '
        'dteFechaEntrega.Properties
        '
        Me.dteFechaEntrega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaEntrega.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaEntrega.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaEntrega.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaEntrega.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaEntrega.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaEntrega.TabIndex = 70
        Me.dteFechaEntrega.Tag = ""
        '
        'tblRefrescar
        '
        Me.tblRefrescar.Text = "Obtener Repartos"
        Me.tblRefrescar.ToolTipText = "Obtiene los Repartos para Cerrar de la Sucursal y Fecha Seleccionados"
        '
        'frmCierreRepartos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(738, 464)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grRepartos)
        Me.Name = "frmCierreRepartos"
        Me.Text = "frmCierreRepartos"
        Me.Controls.SetChildIndex(Me.grRepartos, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.grRepartos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRepartos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRepartir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejaSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroSerie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFechaEntrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oRepartos As VillarrealBusiness.clsRepartos
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private FechaEntrega As DateTime

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCierreRepartos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oRepartos = New VillarrealBusiness.clsRepartos
        oSucursales = New VillarrealBusiness.clsSucursales

        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
        Me.dteFechaEntrega.DateTime = dteFecha.DateTime

    End Sub
    Private Sub frmCierreRepartos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If ShowMessage(MessageType.MsgQuestion, "�Desea Cerrar los Repartos Seleccionados?", , , False) = Answer.MsgYes Then
            Dim orow As DataRow
            Response = New Events

            Me.grvRepartos.CloseEditor()
            Me.grvRepartos.UpdateCurrentRow()

            FechaEntrega = Me.dteFechaEntrega.DateTime

            For Each orow In CType(Me.grRepartos.DataSource, DataTable).Rows
                If Not Response.ErrorFound And orow("repartir") = True Then
                    Response = CerrarReparto(orow("reparto"), FechaEntrega)
                End If
            Next

        End If
    End Sub
    Private Sub frmCierreRepartos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = New Events

        If Me.dteFechaEntrega.Text.Trim.Length = 0 Or IsDate(Me.dteFechaEntrega.Text) = False Then
            Response.Message = "La Fecha de Entrega es Requerida"
        Else
            If CDate(Me.dteFechaEntrega.Text).Year < 1753 Or CDate(Me.dteFechaEntrega.Text).Year > 9999 Then
                Response.Message = "La Fecha de Entrega est� fuera del rango v�lido"
            Else
                If CDate(Me.dteFechaEntrega.Text) < Me.dteFecha.DateTime Then
                    Response.Message = "La Fecha de Entrega es Menor a la Fecha del Reparto"
                End If
            End If
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tblRefrescar Then
            ObtenerRepartos()
        End If

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Function CerrarReparto(ByVal Folio_Reparto As Long, ByVal dFechaEntrega As Date) As Events

        Dim Response As New Events
        Response = oRepartos.CerrarReparto(Folio_Reparto, dFechaEntrega, "CLIENTE CR", Comunes.Common.Sucursal_Actual)

        Return Response

    End Function
    Private Sub ObtenerRepartos()
        Dim response As New Events
        response = oRepartos.CierreRepartos(Me.Sucursal, Me.dteFecha.DateTime)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value

            Me.grRepartos.DataSource = odataset.Tables(0)
            Me.dteFechaEntrega.DateTime = dteFecha.DateTime
        End If
    End Sub

#End Region


End Class
