Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Dipros.Windows

Public Class frmConsultarCerrarReparto
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Factura As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents grvRepartos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grClave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grCapturo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaRepartos As Dipros.Windows.TINMaster
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteFechaEntrega As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtRecibio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpReparto As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtchofer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBodeguero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tmEditEntrega As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents picEntregado As System.Windows.Forms.PictureBox
    Friend WithEvents picEliminado As System.Windows.Forms.PictureBox
    Friend WithEvents tblImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents tblCerrarReparto As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbEliminarReparto As System.Windows.Forms.ToolBarButton
    Friend WithEvents grRepartos As DevExpress.XtraGrid.GridControl
    Friend WithEvents btnProductos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTipoBusqueda As System.Windows.Forms.Label
    Friend WithEvents cboTipoBusqueda As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblFechaReparto As System.Windows.Forms.Label
    Friend WithEvents lblFechaFactura As System.Windows.Forms.Label
    Friend WithEvents dteFechaFactura As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFechaReparto As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkAnoActual As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultarCerrarReparto))
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.clcFolio_Factura = New Dipros.Editors.TINCalcEdit
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.grRepartos = New DevExpress.XtraGrid.GridControl
        Me.grvRepartos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grClave = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grCapturo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaRepartos = New Dipros.Windows.TINMaster
        Me.lkpReparto = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteFechaEntrega = New DevExpress.XtraEditors.DateEdit
        Me.txtRecibio = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tmEditEntrega = New DevExpress.XtraEditors.TimeEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtchofer = New DevExpress.XtraEditors.TextEdit
        Me.txtBodeguero = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.tblCerrarReparto = New System.Windows.Forms.ToolBarButton
        Me.tbEliminarReparto = New System.Windows.Forms.ToolBarButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.picEntregado = New System.Windows.Forms.PictureBox
        Me.picEliminado = New System.Windows.Forms.PictureBox
        Me.tblImprimir = New System.Windows.Forms.ToolBarButton
        Me.btnProductos = New DevExpress.XtraEditors.SimpleButton
        Me.cboTipoBusqueda = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblTipoBusqueda = New System.Windows.Forms.Label
        Me.lblFechaReparto = New System.Windows.Forms.Label
        Me.lblFechaFactura = New System.Windows.Forms.Label
        Me.dteFechaFactura = New DevExpress.XtraEditors.DateEdit
        Me.dteFechaReparto = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.chkAnoActual = New DevExpress.XtraEditors.CheckEdit
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grRepartos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRepartos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaEntrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecibio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEditEntrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtchofer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBodeguero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAnoActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblCerrarReparto, Me.tbEliminarReparto, Me.tblImprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(16791, 28)
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(120, 160)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.Enabled = False
        Me.txtDomicilio.Size = New System.Drawing.Size(224, 20)
        Me.txtDomicilio.TabIndex = 11
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(56, 160)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 10
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(120, 184)
        Me.txtColonia.Name = "txtColonia"
        '
        'txtColonia.Properties
        '
        Me.txtColonia.Properties.Enabled = False
        Me.txtColonia.Size = New System.Drawing.Size(224, 20)
        Me.txtColonia.TabIndex = 13
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(64, 184)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 12
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "&Colonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(24, 368)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 25
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(120, 368)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.Enabled = False
        Me.txtObservaciones.Size = New System.Drawing.Size(344, 49)
        Me.txtObservaciones.TabIndex = 26
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(64, 112)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(50, 16)
        Me.lblFolio_Factura.TabIndex = 4
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Factura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Factura
        '
        Me.clcFolio_Factura.EditValue = "0"
        Me.clcFolio_Factura.Location = New System.Drawing.Point(120, 112)
        Me.clcFolio_Factura.MaxValue = 0
        Me.clcFolio_Factura.MinValue = 0
        Me.clcFolio_Factura.Name = "clcFolio_Factura"
        '
        'clcFolio_Factura.Properties
        '
        Me.clcFolio_Factura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Factura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Factura.Properties.Enabled = False
        Me.clcFolio_Factura.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Factura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Factura.Size = New System.Drawing.Size(70, 19)
        Me.clcFolio_Factura.TabIndex = 5
        Me.clcFolio_Factura.Tag = "folio_factura"
        '
        'txtCliente
        '
        Me.txtCliente.EditValue = ""
        Me.txtCliente.Location = New System.Drawing.Point(120, 136)
        Me.txtCliente.Name = "txtCliente"
        '
        'txtCliente.Properties
        '
        Me.txtCliente.Properties.Enabled = False
        Me.txtCliente.Size = New System.Drawing.Size(344, 20)
        Me.txtCliente.TabIndex = 9
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(64, 136)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 8
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grRepartos
        '
        '
        'grRepartos.EmbeddedNavigator
        '
        Me.grRepartos.EmbeddedNavigator.Name = ""
        Me.grRepartos.Location = New System.Drawing.Point(480, 64)
        Me.grRepartos.MainView = Me.grvRepartos
        Me.grRepartos.Name = "grRepartos"
        Me.grRepartos.Size = New System.Drawing.Size(376, 352)
        Me.grRepartos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grRepartos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRepartos.TabIndex = 28
        Me.grRepartos.TabStop = False
        Me.grRepartos.Text = "Repartos"
        '
        'grvRepartos
        '
        Me.grvRepartos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grGrupo, Me.grClave, Me.grDescripcion, Me.grFecha, Me.grCapturo, Me.grObservaciones})
        Me.grvRepartos.GridControl = Me.grRepartos
        Me.grvRepartos.Name = "grvRepartos"
        Me.grvRepartos.OptionsCustomization.AllowFilter = False
        Me.grvRepartos.OptionsCustomization.AllowGroup = False
        Me.grvRepartos.OptionsCustomization.AllowSort = False
        Me.grvRepartos.OptionsView.ShowGroupPanel = False
        '
        'grGrupo
        '
        Me.grGrupo.Caption = "Grupo"
        Me.grGrupo.FieldName = "grupo"
        Me.grGrupo.Name = "grGrupo"
        Me.grGrupo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grClave
        '
        Me.grClave.Caption = "Clave"
        Me.grClave.FieldName = "eventualidad"
        Me.grClave.Name = "grClave"
        Me.grClave.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grDescripcion
        '
        Me.grDescripcion.Caption = "Descripci�n"
        Me.grDescripcion.FieldName = "descripcion"
        Me.grDescripcion.Name = "grDescripcion"
        Me.grDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grDescripcion.VisibleIndex = 0
        '
        'grFecha
        '
        Me.grFecha.Caption = "Fecha"
        Me.grFecha.FieldName = "fecha"
        Me.grFecha.Name = "grFecha"
        Me.grFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grFecha.VisibleIndex = 1
        '
        'grCapturo
        '
        Me.grCapturo.Caption = "Captur�"
        Me.grCapturo.FieldName = "capturo"
        Me.grCapturo.Name = "grCapturo"
        Me.grCapturo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grCapturo.VisibleIndex = 2
        '
        'grObservaciones
        '
        Me.grObservaciones.Caption = "Observaciones"
        Me.grObservaciones.FieldName = "observaciones"
        Me.grObservaciones.Name = "grObservaciones"
        Me.grObservaciones.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grObservaciones.VisibleIndex = 3
        '
        'tmaRepartos
        '
        Me.tmaRepartos.BackColor = System.Drawing.Color.White
        Me.tmaRepartos.CanDelete = True
        Me.tmaRepartos.CanInsert = True
        Me.tmaRepartos.CanUpdate = True
        Me.tmaRepartos.Grid = Me.grRepartos
        Me.tmaRepartos.Location = New System.Drawing.Point(480, 40)
        Me.tmaRepartos.Name = "tmaRepartos"
        Me.tmaRepartos.Size = New System.Drawing.Size(376, 23)
        Me.tmaRepartos.TabIndex = 27
        Me.tmaRepartos.Title = "Eventualidades"
        Me.tmaRepartos.UpdateTitle = "un Registro"
        '
        'lkpReparto
        '
        Me.lkpReparto.AllowAdd = False
        Me.lkpReparto.AutoReaload = False
        Me.lkpReparto.DataSource = Nothing
        Me.lkpReparto.DefaultSearchField = ""
        Me.lkpReparto.DisplayMember = "reparto"
        Me.lkpReparto.EditValue = Nothing
        Me.lkpReparto.Enabled = False
        Me.lkpReparto.Filtered = False
        Me.lkpReparto.InitValue = Nothing
        Me.lkpReparto.Location = New System.Drawing.Point(120, 88)
        Me.lkpReparto.MultiSelect = False
        Me.lkpReparto.Name = "lkpReparto"
        Me.lkpReparto.NullText = ""
        Me.lkpReparto.PopupWidth = CType(400, Long)
        Me.lkpReparto.ReadOnlyControl = False
        Me.lkpReparto.Required = False
        Me.lkpReparto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpReparto.SearchMember = ""
        Me.lkpReparto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpReparto.SelectAll = False
        Me.lkpReparto.Size = New System.Drawing.Size(73, 20)
        Me.lkpReparto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpReparto.TabIndex = 1
        Me.lkpReparto.Tag = ""
        Me.lkpReparto.ToolTip = "Folio del Reparto"
        Me.lkpReparto.ValueMember = "reparto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 320)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 18
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Fecha Entrega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaEntrega
        '
        Me.dteFechaEntrega.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaEntrega.Location = New System.Drawing.Point(120, 320)
        Me.dteFechaEntrega.Name = "dteFechaEntrega"
        '
        'dteFechaEntrega.Properties
        '
        Me.dteFechaEntrega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaEntrega.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaEntrega.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaEntrega.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaEntrega.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaEntrega.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaEntrega.TabIndex = 19
        Me.dteFechaEntrega.Tag = ""
        '
        'txtRecibio
        '
        Me.txtRecibio.EditValue = ""
        Me.txtRecibio.Location = New System.Drawing.Point(120, 344)
        Me.txtRecibio.Name = "txtRecibio"
        '
        'txtRecibio.Properties
        '
        Me.txtRecibio.Properties.MaxLength = 50
        Me.txtRecibio.Size = New System.Drawing.Size(344, 20)
        Me.txtRecibio.TabIndex = 24
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(62, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Reparto:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(64, 344)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 23
        Me.Label4.Tag = ""
        Me.Label4.Text = "Recibio:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmEditEntrega
        '
        Me.tmEditEntrega.EditValue = New Date(2006, 4, 12, 0, 0, 0, 0)
        Me.tmEditEntrega.Location = New System.Drawing.Point(376, 320)
        Me.tmEditEntrega.Name = "tmEditEntrega"
        '
        'tmEditEntrega.Properties
        '
        Me.tmEditEntrega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.tmEditEntrega.Properties.UseCtrlIncrement = False
        Me.tmEditEntrega.Size = New System.Drawing.Size(88, 20)
        Me.tmEditEntrega.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(288, 320)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 16)
        Me.Label5.TabIndex = 20
        Me.Label5.Tag = ""
        Me.Label5.Text = "Hora Entrega:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtchofer
        '
        Me.txtchofer.EditValue = ""
        Me.txtchofer.Location = New System.Drawing.Point(120, 264)
        Me.txtchofer.Name = "txtchofer"
        '
        'txtchofer.Properties
        '
        Me.txtchofer.Properties.Enabled = False
        Me.txtchofer.Properties.MaxLength = 80
        Me.txtchofer.Size = New System.Drawing.Size(344, 20)
        Me.txtchofer.TabIndex = 15
        '
        'txtBodeguero
        '
        Me.txtBodeguero.EditValue = ""
        Me.txtBodeguero.Location = New System.Drawing.Point(120, 288)
        Me.txtBodeguero.Name = "txtBodeguero"
        '
        'txtBodeguero.Properties
        '
        Me.txtBodeguero.Properties.Enabled = False
        Me.txtBodeguero.Properties.MaxLength = 80
        Me.txtBodeguero.Size = New System.Drawing.Size(344, 20)
        Me.txtBodeguero.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(72, 264)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 16)
        Me.Label6.TabIndex = 14
        Me.Label6.Tag = ""
        Me.Label6.Text = "Chofer:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(48, 288)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 16)
        Me.Label7.TabIndex = 16
        Me.Label7.Tag = ""
        Me.Label7.Text = "Bodeguero:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tblCerrarReparto
        '
        Me.tblCerrarReparto.ImageIndex = 7
        Me.tblCerrarReparto.Text = "Cerrar Reparto"
        '
        'tbEliminarReparto
        '
        Me.tbEliminarReparto.ImageIndex = 8
        Me.tbEliminarReparto.Text = "Eliminar Reparto"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(64, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Cliente:"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(120, 64)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(224, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.ToolTip = "Cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'picEntregado
        '
        Me.picEntregado.Image = CType(resources.GetObject("picEntregado.Image"), System.Drawing.Image)
        Me.picEntregado.Location = New System.Drawing.Point(368, 32)
        Me.picEntregado.Name = "picEntregado"
        Me.picEntregado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEntregado.TabIndex = 59
        Me.picEntregado.TabStop = False
        Me.picEntregado.Visible = False
        '
        'picEliminado
        '
        Me.picEliminado.Image = CType(resources.GetObject("picEliminado.Image"), System.Drawing.Image)
        Me.picEliminado.Location = New System.Drawing.Point(368, 32)
        Me.picEliminado.Name = "picEliminado"
        Me.picEliminado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEliminado.TabIndex = 60
        Me.picEliminado.TabStop = False
        Me.picEliminado.Visible = False
        '
        'tblImprimir
        '
        Me.tblImprimir.ImageIndex = 6
        Me.tblImprimir.Text = "Imprimir"
        Me.tblImprimir.ToolTipText = "Imprimir Reparto"
        '
        'btnProductos
        '
        Me.btnProductos.Enabled = False
        Me.btnProductos.Location = New System.Drawing.Point(360, 160)
        Me.btnProductos.Name = "btnProductos"
        Me.btnProductos.Size = New System.Drawing.Size(104, 23)
        Me.btnProductos.TabIndex = 62
        Me.btnProductos.Text = "Ver Productos"
        '
        'cboTipoBusqueda
        '
        Me.cboTipoBusqueda.EditValue = "ImageComboBoxEdit1"
        Me.cboTipoBusqueda.Location = New System.Drawing.Point(120, 40)
        Me.cboTipoBusqueda.Name = "cboTipoBusqueda"
        '
        'cboTipoBusqueda.Properties
        '
        Me.cboTipoBusqueda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoBusqueda.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cliente", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Reparto", "R", -1)})
        Me.cboTipoBusqueda.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoBusqueda.TabIndex = 63
        '
        'lblTipoBusqueda
        '
        Me.lblTipoBusqueda.AutoSize = True
        Me.lblTipoBusqueda.Location = New System.Drawing.Point(9, 42)
        Me.lblTipoBusqueda.Name = "lblTipoBusqueda"
        Me.lblTipoBusqueda.Size = New System.Drawing.Size(107, 16)
        Me.lblTipoBusqueda.TabIndex = 65
        Me.lblTipoBusqueda.Text = "Tipo de b�squeda:"
        '
        'lblFechaReparto
        '
        Me.lblFechaReparto.AutoSize = True
        Me.lblFechaReparto.Location = New System.Drawing.Point(256, 90)
        Me.lblFechaReparto.Name = "lblFechaReparto"
        Me.lblFechaReparto.Size = New System.Drawing.Size(106, 16)
        Me.lblFechaReparto.TabIndex = 66
        Me.lblFechaReparto.Text = "Fecha de Reparto:"
        '
        'lblFechaFactura
        '
        Me.lblFechaFactura.AutoSize = True
        Me.lblFechaFactura.Location = New System.Drawing.Point(256, 114)
        Me.lblFechaFactura.Name = "lblFechaFactura"
        Me.lblFechaFactura.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaFactura.TabIndex = 67
        Me.lblFechaFactura.Text = "Fecha de Factura:"
        '
        'dteFechaFactura
        '
        Me.dteFechaFactura.EditValue = Nothing
        Me.dteFechaFactura.Location = New System.Drawing.Point(368, 112)
        Me.dteFechaFactura.Name = "dteFechaFactura"
        '
        'dteFechaFactura.Properties
        '
        Me.dteFechaFactura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFactura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaFactura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaFactura.Properties.Enabled = False
        Me.dteFechaFactura.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaFactura.TabIndex = 68
        Me.dteFechaFactura.Tag = ""
        '
        'dteFechaReparto
        '
        Me.dteFechaReparto.EditValue = Nothing
        Me.dteFechaReparto.Location = New System.Drawing.Point(368, 88)
        Me.dteFechaReparto.Name = "dteFechaReparto"
        '
        'dteFechaReparto.Properties
        '
        Me.dteFechaReparto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaReparto.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaReparto.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaReparto.Properties.Enabled = False
        Me.dteFechaReparto.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaReparto.TabIndex = 69
        Me.dteFechaReparto.Tag = ""
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.BackColor = System.Drawing.SystemColors.Window
        Me.lblFecha.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.ForeColor = System.Drawing.Color.Black
        Me.lblFecha.Location = New System.Drawing.Point(8, 384)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblFecha.Visible = False
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(8, 400)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = "fecha"
        Me.dteFecha.Visible = False
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(120, 208)
        Me.txtCiudad.Name = "txtCiudad"
        '
        'txtCiudad.Properties
        '
        Me.txtCiudad.Properties.Enabled = False
        Me.txtCiudad.Size = New System.Drawing.Size(224, 20)
        Me.txtCiudad.TabIndex = 70
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(120, 232)
        Me.txtEstado.Name = "txtEstado"
        '
        'txtEstado.Properties
        '
        Me.txtEstado.Properties.Enabled = False
        Me.txtEstado.Size = New System.Drawing.Size(224, 20)
        Me.txtEstado.TabIndex = 71
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(64, 208)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(47, 16)
        Me.lblCiudad.TabIndex = 72
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "C&iudad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(64, 232)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 73
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkAnoActual
        '
        Me.chkAnoActual.EditValue = True
        Me.chkAnoActual.Location = New System.Drawing.Point(240, 44)
        Me.chkAnoActual.Name = "chkAnoActual"
        '
        'chkAnoActual.Properties
        '
        Me.chkAnoActual.Properties.Caption = "A�o Actual"
        Me.chkAnoActual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkAnoActual.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkAnoActual.Size = New System.Drawing.Size(104, 19)
        Me.chkAnoActual.TabIndex = 74
        Me.chkAnoActual.Tag = ""
        '
        'frmConsultarCerrarReparto
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 424)
        Me.Controls.Add(Me.chkAnoActual)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.txtEstado)
        Me.Controls.Add(Me.txtCiudad)
        Me.Controls.Add(Me.dteFechaReparto)
        Me.Controls.Add(Me.dteFechaFactura)
        Me.Controls.Add(Me.lblFechaFactura)
        Me.Controls.Add(Me.lblFechaReparto)
        Me.Controls.Add(Me.lblTipoBusqueda)
        Me.Controls.Add(Me.cboTipoBusqueda)
        Me.Controls.Add(Me.btnProductos)
        Me.Controls.Add(Me.picEliminado)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtBodeguero)
        Me.Controls.Add(Me.txtchofer)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tmEditEntrega)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtRecibio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grRepartos)
        Me.Controls.Add(Me.dteFechaEntrega)
        Me.Controls.Add(Me.lkpReparto)
        Me.Controls.Add(Me.tmaRepartos)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lblFolio_Factura)
        Me.Controls.Add(Me.clcFolio_Factura)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblDomicilio)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.txtColonia)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.picEntregado)
        Me.Name = "frmConsultarCerrarReparto"
        Me.Text = "frmCerrarReparto"
        Me.Controls.SetChildIndex(Me.picEntregado, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.txtColonia, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblDomicilio, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Factura, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Factura, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.txtCliente, 0)
        Me.Controls.SetChildIndex(Me.tmaRepartos, 0)
        Me.Controls.SetChildIndex(Me.lkpReparto, 0)
        Me.Controls.SetChildIndex(Me.dteFechaEntrega, 0)
        Me.Controls.SetChildIndex(Me.grRepartos, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtRecibio, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.tmEditEntrega, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.txtchofer, 0)
        Me.Controls.SetChildIndex(Me.txtBodeguero, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.picEliminado, 0)
        Me.Controls.SetChildIndex(Me.btnProductos, 0)
        Me.Controls.SetChildIndex(Me.cboTipoBusqueda, 0)
        Me.Controls.SetChildIndex(Me.lblTipoBusqueda, 0)
        Me.Controls.SetChildIndex(Me.lblFechaReparto, 0)
        Me.Controls.SetChildIndex(Me.lblFechaFactura, 0)
        Me.Controls.SetChildIndex(Me.dteFechaFactura, 0)
        Me.Controls.SetChildIndex(Me.dteFechaReparto, 0)
        Me.Controls.SetChildIndex(Me.txtCiudad, 0)
        Me.Controls.SetChildIndex(Me.txtEstado, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.chkAnoActual, 0)
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grRepartos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRepartos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaEntrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecibio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEditEntrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtchofer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBodeguero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAnoActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private oRepartos As New VillarrealBusiness.clsRepartos
    Private oRepartosEventualidades As New VillarrealBusiness.clsRepartosEventualidades
    Private oClientes As New VillarrealBusiness.clsClientes
    '@ACH-22/06/07: Agregu� la clase RepartosDetalle para obtener el m�todo RepartosServicio
    Private oRepartosDetalle As New VillarrealBusiness.clsRepartosDetalle
    '/@ACH-22/06/07
    Private lTiene_Chofer As Boolean
    Private Estatus As String


    ReadOnly Property Folio_Reparto() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpReparto)
        End Get
    End Property


    ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    ReadOnly Property FechaEntrega() As String
        Get
            Return Me.dteFechaEntrega.Text + " " + Me.tmEditEntrega.Text
        End Get
    End Property





#End Region

#Region "Eventos de la Forma"

    Private Sub frmCerrarReparto_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCerrarReparto_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmCerrarReparto_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCerrarReparto_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)

        Me.tbrTools.Buttons(0).Visible = True

        Me.tbrTools.Buttons(2).Enabled = False
        Me.tbrTools.Buttons(3).Enabled = False
        Me.tbrTools.Buttons(4).Enabled = False

        Me.dteFechaEntrega.DateTime = CDate(TINApp.FechaServidor)
        Me.tmEditEntrega.EditValue = CDate(TINApp.FechaServidor)

        With Me.tmaRepartos
            .UpdateTitle = "una Eventualidad"
            .UpdateForm = New frmRepartosEventualidades
            .AddColumn("grupo")
            .AddColumn("eventualidad")
            .AddColumn("descripcion")
            .AddColumn("fecha")
            .AddColumn("capturo")
            .AddColumn("observaciones")
        End With

    End Sub
    Private Sub frmCerrarReparto_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaRepartos
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oRepartosEventualidades.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, Folio_Reparto)
                    Case Actions.Update
                        Response = oRepartosEventualidades.Actualizar(.SelectedRow, Comunes.Common.Sucursal_Actual, Folio_Reparto)
                    Case Actions.Delete
                        Response = oRepartosEventualidades.Eliminar(Folio_Reparto, .Item("eventualidad"))
                End Select
                .MoveNext()
            Loop
        End With
    End Sub
    Private Sub frmConsultarCerrarReparto_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If ValidaModificacionesPendientesGrabar = True Then
            If ShowMessage(MessageType.MsgQuestion, "Ha realizado modificaciones.�Desea salir sin guardar?", "Mensaje del Sistema") = Answer.MsgYes Then
            Else
                e.Cancel = True
            End If


        End If

    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If Folio_Reparto <= 0 Then Exit Sub

        If e.Button Is Me.tblImprimir Then
            Me.txtRecibio.Focus()
            ImprimeValeReparto()
            Exit Sub
        End If

        Dim response As New Events
        frmCerrarReparto_BeginUpdate()

        If e.Button Is Me.tblCerrarReparto Then
            'Cerrar Reparto

            If ShowMessage(MessageType.MsgQuestion, "�Desea Cerrar el Reparto?") = Answer.MsgYes Then


                'Me.txtRecibio.Focus()
                If ValidaCamposRequeridos() = True Then
                    response = oRepartos.CerrarReparto(Folio_Reparto, FechaEntrega, Me.txtRecibio.Text, Comunes.Common.Sucursal_Actual)

                    'DAM SE AGREGO PARA QUE TAMBIEN GUARDE LAS EVENTUALIDADES
                    If Not response.ErrorFound Then
                        frmCerrarReparto_Detail(response)
                    End If

                    If response.ErrorFound Then
                        frmCerrarReparto_AbortUpdate()
                        response.ShowMessage()
                        Exit Sub
                    End If

                    Me.Close()

                End If
            End If

        ElseIf e.Button Is Me.tbEliminarReparto Then
            'Eliminar Reparto
            If ShowMessage(MessageType.MsgQuestion, "�Desea Eliminar el Reparto?") = Answer.MsgYes Then
                'Me.txtRecibio.Focus()
                If Me.tmaRepartos.Count > 0 Then
                    response = oRepartos.EliminarReparto(Folio_Reparto)
                    If response.ErrorFound Then
                        frmCerrarReparto_AbortUpdate()
                        response.ShowMessage()
                        Exit Sub
                    End If
                    frmCerrarReparto_Detail(response)

                    Me.Close()

                Else
                    ShowMessage(MessageType.MsgInformation, "Al menos una Eventualidad es requerida", Me.Text)
                End If

            End If
        End If

        response = Nothing
        frmCerrarReparto_EndUpdate()
    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub lkpReparto_LoadData(ByVal Initialize As Boolean) Handles lkpReparto.LoadData
        Dim response As Events
        'Si el lookup va ser filtrado por cliente
        'response = oRepartos.Lookup(, Cliente)
        '-------------------------------------------
        If cboTipoBusqueda.EditValue = "C" Then
            response = oRepartos.Lookup(Comunes.Common.Sucursal_Actual, , lkpCliente.EditValue, Me.chkAnoActual.Checked)
        ElseIf cboTipoBusqueda.EditValue = "R" Then
            response = oRepartos.Lookup(Comunes.Common.Sucursal_Actual, , , Me.chkAnoActual.Checked)
        End If
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpReparto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpReparto_Format() Handles lkpReparto.Format
        Comunes.clsFormato.for_repartos_grl(Me.lkpReparto)
    End Sub
    Private Sub lkpReparto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpReparto.EditValueChanged
        '@ACH-20/06/07 : Agregu� un c�digo para detectar el valor del lookup de Cliente y habilitar/deshabilitar el bot�n de 'Ver Productos'
        If lkpReparto.EditValue = Nothing Then
            btnProductos.Enabled = False
        Else
            btnProductos.Enabled = True
        End If
        '@ACH-20/06/07: Fin del c�digo agregado

        If Me.lkpReparto.DataSource Is Nothing Or Me.lkpReparto.EditValue Is Nothing Then
            If cboTipoBusqueda.EditValue = "R" Then
                Me.lkpCliente.EditValue = Nothing
            End If
            Me.clcFolio_Factura.EditValue = 0
            dteFechaReparto.EditValue = Nothing
            dteFechaFactura.EditValue = Nothing
            Me.txtCliente.Text = Nothing
            Me.txtDomicilio.Text = Nothing
            Me.txtColonia.Text = Nothing

            Me.txtCiudad.Text = Nothing
            Me.txtEstado.Text = Nothing

            Me.txtchofer.Text = Nothing
            Me.txtBodeguero.Text = Nothing
            dteFechaEntrega.EditValue = Today
            tmEditEntrega.EditValue = Today
            Me.txtRecibio.Text = Nothing
            Me.txtObservaciones.Text = Nothing
            Exit Sub
        End If

        Me.clcFolio_Factura.EditValue = Me.lkpReparto.GetValue("folio_factura")
        Me.txtCliente.Text = Me.lkpReparto.GetValue("nombre_cliente")
        Me.txtDomicilio.Text = Me.lkpReparto.GetValue("domicilio")
        Me.txtColonia.Text = Me.lkpReparto.GetValue("colonia")

        Me.txtCiudad.Text = Me.lkpReparto.GetValue("ciudad")
        Me.txtEstado.Text = Me.lkpReparto.GetValue("estado")
       
        Me.dteFechaReparto.EditValue = Me.lkpReparto.GetValue("fecha")
        Me.dteFechaFactura.EditValue = Me.lkpReparto.GetValue("fecha_factura")
      
        Me.txtRecibio.Text = Me.lkpReparto.GetValue("recibio")
        Me.txtchofer.Text = Me.lkpReparto.GetValue("nombre_chofer")
        Me.txtBodeguero.Text = Me.lkpReparto.GetValue("nombre_bodeguero")
        Me.txtObservaciones.Text = Me.lkpReparto.GetValue("observaciones")
        Estatus = Me.lkpReparto.GetValue("status")

        lTiene_Chofer = IIf(Me.lkpReparto.GetValue("tiene_chofer") > 0, True, False)

        Select Case Estatus.ToUpper
            Case "RE" ' Repartiendo
                Me.tbrTools.Buttons(0).Enabled = True
                Me.tbrTools.Buttons(2).Enabled = True
                Me.tbrTools.Buttons(3).Enabled = True
                Me.tbrTools.Buttons(4).Enabled = True

                Me.tmaRepartos.Enabled = True

                Me.picEntregado.Visible = False
                Me.picEliminado.Visible = False

                Me.dteFechaEntrega.Enabled = True
                Me.tmEditEntrega.Enabled = True
                Me.txtRecibio.Enabled = True
            Case "NE" ' No Entregada(eliminada)
                Me.tbrTools.Buttons(0).Enabled = False
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(3).Enabled = False
                Me.tbrTools.Buttons(4).Enabled = False
                Me.tmaRepartos.Enabled = False

                Me.picEliminado.Visible = True
                Me.picEntregado.Visible = False

                Me.dteFechaEntrega.Enabled = False
                Me.tmEditEntrega.Enabled = False
                Me.txtRecibio.Enabled = False
            Case "EN" ' Entregada
                Me.tbrTools.Buttons(0).Enabled = False
                Me.tbrTools.Buttons(2).Enabled = False
                Me.tbrTools.Buttons(3).Enabled = False
                Me.tbrTools.Buttons(4).Enabled = False
                Me.tmaRepartos.Enabled = False

                Me.picEliminado.Visible = False
                Me.picEntregado.Visible = True

                Me.dteFechaEntrega.Enabled = False
                Me.tmEditEntrega.Enabled = False
                Me.txtRecibio.Enabled = False
        End Select

        ObtenerEventualidades(Folio_Reparto)

        If cboTipoBusqueda.EditValue = "R" Then
            Me.lkpCliente.EditValue = Me.lkpReparto.GetValue("cliente")
        End If

        Me.txtRecibio.Focus()
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim response As Events
        response = oClientes.LookupCliente(Comunes.Common.Sucursal_Actual, Me.chkAnoActual.Checked)
        'response = oRepartos.Lookup(-1, -1)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If lkpCliente.EditValue = Nothing And cboTipoBusqueda.EditValue = "C" Then
            lkpReparto.EditValue = Nothing
            lkpReparto.Enabled = False
        ElseIf cboTipoBusqueda.EditValue = "C" Then
            lkpReparto.EditValue = Nothing
            lkpReparto.Enabled = True
        End If
    End Sub

    Private Sub btnProductos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProductos.Click
        Dim oVerProductos As New frmConsultarCerrarRepartoVerProductos

        With oVerProductos
            .Title = "Productos del Reparto"
            .FormaPadre = Me
            .MdiParent = Me.MdiParent
            .intReparto = lkpReparto.EditValue
            .Show()
        End With
    End Sub

    Private Sub cboTipoBusqueda_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoBusqueda.EditValueChanged
        Me.tbrTools.Buttons(0).Enabled = Nothing
        Me.tbrTools.Buttons(2).Enabled = Nothing
        Me.tbrTools.Buttons(3).Enabled = Nothing
        Me.tbrTools.Buttons(4).Enabled = Nothing
        lkpReparto.EditValue = Nothing
        lkpCliente.EditValue = Nothing
        If Not (cboTipoBusqueda.EditValue = Nothing) Then
            If cboTipoBusqueda.EditValue = "C" Then
                lkpReparto.Enabled = False
                lkpCliente.Enabled = True
            ElseIf cboTipoBusqueda.EditValue = "R" Then
                lkpReparto.Enabled = True
                lkpCliente.Enabled = False
            End If
        End If
    End Sub
#End Region

#Region "Funcionalidad"

    Private Sub ObtenerEventualidades(ByVal folio_reparto As Long)
        Dim response As New Events

        response = oRepartosEventualidades.Listado(folio_reparto)
        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value
            Me.tmaRepartos.DataSource = odataset

        Else
            response.ShowMessage()
        End If
    End Sub
    Public Function ValidaCamposRequeridos() As Boolean
        If Me.txtRecibio.Text.Trim.Length <= 0 Then
            ShowMessage(MessageType.MsgInformation, "El campo Recibi� es Requerido")
            Return False
        Else
            If Me.dteFechaEntrega.Text.Trim.Length = 0 Or IsDate(Me.dteFechaEntrega.Text) = False Then
                ShowMessage(MessageType.MsgInformation, "La Fecha de Entrega es Requerida")
                Return False
            Else
                If CDate(Me.dteFechaEntrega.Text).Year < 1753 Or CDate(Me.dteFechaEntrega.Text).Year > 9999 Then
                    ShowMessage(MessageType.MsgInformation, "La Fecha de Entrega est� fuera del rango v�lido")
                    Return False
                Else
                    Return True
                End If
            End If
        End If
    End Function
    Private Sub ImprimeValeReparto()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeDeReparto(Folio_Reparto, lTiene_Chofer)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Vale de Reparto no puede Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptRepartoVale

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                    TINApp.ShowReport(Me.MdiParent, "Vale de Reparto", oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub

    '@ACH-21/06/07: Se agreg� el proceso de Generar una Orden de Servicio al generar un Reparto.
    Private Sub GeneraOrdenServicio(ByRef response As Events, ByVal tipo_servicio As Char, ByVal fecha As DateTime, ByVal articulo As Long, _
    ByVal numero_serie As String, ByVal cliente As Long, ByVal direccion_cliente As String, ByVal sucursal_factura As Long, _
    ByVal serie_factura As String, ByVal folio_factura As Long, ByVal partida_factura As Long, ByVal fecha_factura As DateTime)
        Dim oservicio As New VillarrealBusiness.clsOrdenesServicio
        Dim orden_servicio As Long = 0
        response = oservicio.InsertarOSReparto(orden_servicio, Comunes.Common.Sucursal_Actual, tipo_servicio, fecha, _
        articulo, numero_serie, cliente, direccion_cliente, sucursal_factura, serie_factura, folio_factura, partida_factura, fecha_factura)
    End Sub


    Private Function SucursalFactura(ByRef oSucursalFactura As Object) As Long
        If oSucursalFactura = Nothing Then
            Return -1
        Else
            Return CType(oSucursalFactura, Long)
        End If
    End Function

    Private Function ValidaModificacionesPendientesGrabar() As Boolean
        Dim i As Long = 0
        Dim eventualidades As DataView

        eventualidades = Me.tmaRepartos.DataSource.Tables(0).DefaultView
        eventualidades.RowFilter = "control in (1,2,3)"
        If eventualidades.Count > 0 Then
            ValidaModificacionesPendientesGrabar = True
            Return True
        Else
            ValidaModificacionesPendientesGrabar = False
            Return False
        End If
    End Function
#End Region

  
   

    Private Sub tmaRepartos_ValidateFields(ByRef Cancel As Boolean) Handles tmaRepartos.ValidateFields
        If tmaRepartos.Action = Actions.Insert Or tmaRepartos.Action = Actions.Update Then
            Me.txtRecibio.Focus()
        End If
    End Sub
End Class
