Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "


    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatCobradores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem2 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuProCapturaVisitasClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosCajas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSugerenciaCobroProximasVisitas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepIngresosxCobrador As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSugerenciaCobro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenIngresos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSaldosGenerales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEstadosCuentaClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSaldosVencidos As DevExpress.XtraBars.BarButtonItem


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatCobradores = New DevExpress.XtraBars.BarButtonItem
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem2 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuProCapturaVisitasClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosCajas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSugerenciaCobroProximasVisitas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepIngresosxCobrador = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSugerenciaCobro = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosCliente = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenIngresos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSaldosGenerales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEstadosCuentaClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSaldosVencidos = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatCobradores, Me.BarStaticItem1, Me.BarStaticItem2, Me.mnuProCapturaVisitasClientes, Me.mnuProMovimientosCajas, Me.mnuRepSugerenciaCobroProximasVisitas, Me.mnuRepIngresosxCobrador, Me.mnuRepSugerenciaCobro, Me.mnuProMovimientosCliente, Me.mnuRepResumenIngresos, Me.mnuRepSaldosGenerales, Me.mnuRepEstadosCuentaClientes, Me.mnuRepSaldosVencidos})
        Me.BarManager.MaxItemId = 121
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCobradores)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosCajas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCapturaVisitasClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosCliente)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSugerenciaCobro), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSugerenciaCobroProximasVisitas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepIngresosxCobrador), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenIngresos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosGenerales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEstadosCuentaClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosVencidos)})
        '
        'mnuCatCobradores
        '
        Me.mnuCatCobradores.Caption = "Cobradores"
        Me.mnuCatCobradores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCobradores.Id = 108
        Me.mnuCatCobradores.ImageIndex = 0
        Me.mnuCatCobradores.Name = "mnuCatCobradores"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 109
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'Bar1
        '
        Me.Bar1.BarName = "Custom 3"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(269, 475)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem2)})
        Me.Bar1.Offset = 59
        Me.Bar1.Text = "Custom 3"
        '
        'BarStaticItem2
        '
        Me.BarStaticItem2.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BarStaticItem2.Caption = "BarStaticItem1"
        Me.BarStaticItem2.Id = 110
        Me.BarStaticItem2.Name = "BarStaticItem2"
        Me.BarStaticItem2.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem2.Width = 32
        '
        'mnuProCapturaVisitasClientes
        '
        Me.mnuProCapturaVisitasClientes.Caption = "Captura de Visitas a Clientes"
        Me.mnuProCapturaVisitasClientes.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProCapturaVisitasClientes.Id = 111
        Me.mnuProCapturaVisitasClientes.Name = "mnuProCapturaVisitasClientes"
        '
        'mnuProMovimientosCajas
        '
        Me.mnuProMovimientosCajas.Caption = "Movimientos de Caja"
        Me.mnuProMovimientosCajas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosCajas.Id = 112
        Me.mnuProMovimientosCajas.ImageIndex = 1
        Me.mnuProMovimientosCajas.Name = "mnuProMovimientosCajas"
        '
        'mnuRepSugerenciaCobroProximasVisitas
        '
        Me.mnuRepSugerenciaCobroProximasVisitas.Caption = "Sugerencia de Cobro Proximas Visitas"
        Me.mnuRepSugerenciaCobroProximasVisitas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSugerenciaCobroProximasVisitas.Id = 113
        Me.mnuRepSugerenciaCobroProximasVisitas.Name = "mnuRepSugerenciaCobroProximasVisitas"
        '
        'mnuRepIngresosxCobrador
        '
        Me.mnuRepIngresosxCobrador.Caption = "Ingresos por Cobrador"
        Me.mnuRepIngresosxCobrador.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepIngresosxCobrador.Id = 114
        Me.mnuRepIngresosxCobrador.ImageIndex = 2
        Me.mnuRepIngresosxCobrador.Name = "mnuRepIngresosxCobrador"
        '
        'mnuRepSugerenciaCobro
        '
        Me.mnuRepSugerenciaCobro.Caption = "Sugerencia de Cobro"
        Me.mnuRepSugerenciaCobro.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSugerenciaCobro.Id = 115
        Me.mnuRepSugerenciaCobro.ImageIndex = 3
        Me.mnuRepSugerenciaCobro.Name = "mnuRepSugerenciaCobro"
        '
        'mnuProMovimientosCliente
        '
        Me.mnuProMovimientosCliente.Caption = "Movimientos de Clientes"
        Me.mnuProMovimientosCliente.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosCliente.Id = 116
        Me.mnuProMovimientosCliente.ImageIndex = 4
        Me.mnuProMovimientosCliente.Name = "mnuProMovimientosCliente"
        '
        'mnuRepResumenIngresos
        '
        Me.mnuRepResumenIngresos.Caption = "Resumen de Ingresos"
        Me.mnuRepResumenIngresos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenIngresos.Id = 117
        Me.mnuRepResumenIngresos.ImageIndex = 5
        Me.mnuRepResumenIngresos.Name = "mnuRepResumenIngresos"
        '
        'mnuRepSaldosGenerales
        '
        Me.mnuRepSaldosGenerales.Caption = "Saldos Generales"
        Me.mnuRepSaldosGenerales.Id = 118
        Me.mnuRepSaldosGenerales.Name = "mnuRepSaldosGenerales"
        '
        'mnuRepEstadosCuentaClientes
        '
        Me.mnuRepEstadosCuentaClientes.Caption = "Estado de Cuenta"
        Me.mnuRepEstadosCuentaClientes.Id = 119
        Me.mnuRepEstadosCuentaClientes.Name = "mnuRepEstadosCuentaClientes"
        '
        'mnuRepSaldosVencidos
        '
        Me.mnuRepSaldosVencidos.Caption = "Saldos Vencidos"
        Me.mnuRepSaldosVencidos.Id = 120
        Me.mnuRepSaldosVencidos.Name = "mnuRepSaldosVencidos"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnología .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub frmMain_LoadOptions() Handles MyBase.LoadOptions
        With TINApp.Options.Security
            '---------------------------------------------------------------------------------------------
            .Options("mnuProMovimientosCajas").AddAction("FECHA_COB_CAJA", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

        End With
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem2.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If
    End Sub


    Public Sub mnuCatCobradores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCobradores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCobradores As New VillarrealBusiness.clsCobradores
        With oGrid
            .Title = "Cobradores"
            .UpdateTitle = "Cobradores"
            .DataSource = AddressOf oCobradores.Listado
            .UpdateForm = New Comunes.frmCobradores
            .Format = AddressOf Comunes.clsFormato.for_cobradores_grs
            .MdiParent = Me
            .Show()
            .Width = 680
        End With
        oCobradores = Nothing
        oGrid = Nothing


    End Sub
    Public Sub mnuProMovimientosCajas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosCajas.ItemClick
        If Me.mnuProcesos.Enabled = False Then Exit Sub
        Dim frmmovimientos_cajas As New Comunes.frmMovimientosCaja

        With frmmovimientos_cajas
            .MenuOption = e.Item
            .MdiParent = Me
            .EntroCaja = False
            .Text = "Movimientos de Caja"
            .Show()
        End With

    End Sub
    Public Sub mnuProCapturaVisitasClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCapturaVisitasClientes.ItemClick
        Dim oGrid As New Comunes.brwCapturaVisitasClientes
        With oGrid
            .MenuOption = e.Item
            .Title = "Captura de Visitas de Clientes"
            .UpdateTitle = "Visita de Clientes"
            .UpdateForm = New Comunes.frmCapturaVisitasClientes
            .Format = AddressOf Comunes.clsFormato.for_captura_visitas_clientes_grs
            .MdiParent = Me
            .Show()
        End With


    End Sub
    Public Sub mnuProMovimientosCliente_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosCliente.ItemClick
        Dim oForm As New Comunes.frmMovimientosClientes
        With oForm
            .MenuOption = e.Item
            .Title = "Movimientos del Cliente"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuRepResumenIngresos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenIngresos.ItemClick
        Dim oForm As New Comunes.frmResumenIngresos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Ingresos"
        oForm.Action = Actions.Report
        oForm.Ingresos = True
        oForm.Show()
    End Sub
    Public Sub mnuRepSugerenciaCobroProximasVisitas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSugerenciaCobroProximasVisitas.ItemClick
        Dim oForm As New Comunes.frmReporteCobranzaProximasVisitas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Sugerencia de Cobro Proximas Visitas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepIngresosxCobrador_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepIngresosxCobrador.ItemClick
        Dim oForm As New Comunes.frmIngresosxCobrador
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ingresos por Cobrador"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepSugerenciaCobro_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSugerenciaCobro.ItemClick
        Dim oForm As New Comunes.frmRepSugerenciaCobro
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Sugerencia de Cobro"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepSaldosGenerales_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosGenerales.ItemClick
        Dim oForm As New Comunes.frmRepSaldosGenerales
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Generales"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepEstadosCuentaClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEstadosCuentaClientes.ItemClick
        Dim oForm As New Comunes.frmRepEstadoDeCuenta
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Estado de Cuenta"
            .Action = Actions.Report
            .Show()
        End With
    End Sub

    Public Sub mnuRepSaldosVencidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosVencidos.ItemClick
        Dim oForm As New Comunes.frmRepSaldosVencidos
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Vencidos"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
End Class
