Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuProDescuentosEspecialesProgramados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepCambiosPrecios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDescuentosEspecialesProgramados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProPaquetes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHerrPermisosExtendidos As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.mnuProDescuentosEspecialesProgramados = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepCambiosPrecios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDescuentosEspecialesProgramados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProPaquetes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuHerrPermisosExtendidos = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuProDescuentosEspecialesProgramados, Me.BarStaticItem1, Me.mnuRepCambiosPrecios, Me.mnuRepDescuentosEspecialesProgramados, Me.mnuProPaquetes, Me.mnuHerrPermisosExtendidos})
        Me.BarManager.MaxItemId = 117
        '
        'barMainMenu
        '
        Me.barMainMenu.FloatLocation = New System.Drawing.Point(208, 164)
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerrPermisosExtendidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDescuentosEspecialesProgramados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProPaquetes)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepCambiosPrecios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDescuentosEspecialesProgramados)})
        '
        'mnuProDescuentosEspecialesProgramados
        '
        Me.mnuProDescuentosEspecialesProgramados.Caption = "Descuentos Especiales Programados"
        Me.mnuProDescuentosEspecialesProgramados.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDescuentosEspecialesProgramados.Id = 111
        Me.mnuProDescuentosEspecialesProgramados.Name = "mnuProDescuentosEspecialesProgramados"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(94, 479)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 10
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 112
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuRepCambiosPrecios
        '
        Me.mnuRepCambiosPrecios.Caption = "Cambio de Precios"
        Me.mnuRepCambiosPrecios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepCambiosPrecios.Id = 113
        Me.mnuRepCambiosPrecios.Name = "mnuRepCambiosPrecios"
        '
        'mnuRepDescuentosEspecialesProgramados
        '
        Me.mnuRepDescuentosEspecialesProgramados.Caption = "Descuentos Especiales Programados"
        Me.mnuRepDescuentosEspecialesProgramados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDescuentosEspecialesProgramados.Id = 114
        Me.mnuRepDescuentosEspecialesProgramados.Name = "mnuRepDescuentosEspecialesProgramados"
        '
        'mnuProPaquetes
        '
        Me.mnuProPaquetes.Caption = "Paquetes"
        Me.mnuProPaquetes.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProPaquetes.Id = 115
        Me.mnuProPaquetes.Name = "mnuProPaquetes"
        '
        'mnuHerrPermisosExtendidos
        '
        Me.mnuHerrPermisosExtendidos.Caption = "Permisos Extendidos"
        Me.mnuHerrPermisosExtendidos.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuHerrPermisosExtendidos.Id = 116
        Me.mnuHerrPermisosExtendidos.Name = "mnuHerrPermisosExtendidos"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        Dim oPermisosEspeciales As New VillarrealBusiness.clsIdentificadoresPermisos

        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If

        response = oPermisosEspeciales.ListadoPermisos(TINApp.Connection.User, TINApp.Prefix)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim Row As DataRow
            Dim i As Long = 0

            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                ReDim Comunes.Common.Identificadores(odataset.Tables(0).Rows.Count() - 1)

                For Each Row In odataset.Tables(0).Rows
                    Comunes.Common.Identificadores.SetValue(Row("identificador"), i)
                    i = i + 1
                    'If i = 1 Then
                    '    Cadena_identificadores = Row("identificador")
                    'Else
                    '    Cadena_identificadores = Cadena_identificadores + "," + Row("identificador")
                    'End If
                Next
            Else
                'ShowMessage(MessageType.MsgInformation, "No hay registros de Permisos para este usuario.")
            End If
        End If
    End Sub


#Region "Procesos"
    Public Sub mnuProDescuentosEspecialesProgramados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDescuentosEspecialesProgramados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Descuentos Especiales Programados"
            .DataSource = AddressOf oDescuentosEspecialesProgramados.Listado
            .UpdateTitle = "un Descuento Especial Programado"
            .UpdateForm = New Comunes.frmDescuentosEspecialesProgramados
            .Format = AddressOf Comunes.clsFormato.for_descuentos_especiales_programados_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub mnuProPaquetes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProPaquetes.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPaquetes As New VillarrealBusiness.clsPaquetes
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Paquetes"
            .DataSource = AddressOf oPaquetes.Listado
            .UpdateTitle = "un Paquete"
            .UpdateForm = New Comunes.frmPaquetes
            .Format = AddressOf Comunes.clsFormato.for_paquetes_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub

#End Region

#Region "Reportes"
    Public Sub mnuRepCambiosPrecios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepCambiosPrecios.ItemClick
        Dim oForm As New Comunes.frmCambiosPrecios
        oForm.MdiParent = Me
        oForm.Title = "Cambios de Precios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDescuentosEspecialesProgramados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDescuentosEspecialesProgramados.ItemClick
        Dim oForm As New Comunes.frmRepDescuentosProgramados
        oForm.MdiParent = Me
        oForm.Title = "Descuentos Especiales Programados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
#End Region

#Region "Herramientas"

    Public Sub mnuHerrPermisosExtendidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHerrPermisosExtendidos.ItemClick
        Dim oForm As New Comunes.frmPermisosIdentificadores
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Permisos Extendidos"
        oForm.Action = Actions.None
        oForm.Show()
    End Sub
#End Region

End Class
