Imports Dipros.Utils.Common
Public Class FrmMain
    Inherits Dipros.Windows.frmTINMain

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuProMovimientosPagar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosChequera As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAcreedoresDiversos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents mnuProPagoVariasFacturas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatAbogados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatLlamadasClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatTiposLlamadas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCapturaClientesJuridico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepClientesEnJuridicoPorEstatus As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAbonosClientesJuridico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuDocumentos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents mnuCatCorrespondenciaClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatCamposCorrespondencia As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAsignacionClientesJuridico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepImpresionCorrespondencia As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepComisionesAbogados As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmMain))
        Me.mnuProAcreedoresDiversos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosPagar = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosChequera = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuProPagoVariasFacturas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatAbogados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatLlamadasClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatTiposLlamadas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCapturaClientesJuridico = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepClientesEnJuridicoPorEstatus = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAbonosClientesJuridico = New DevExpress.XtraBars.BarButtonItem
        Me.mnuDocumentos = New DevExpress.XtraBars.BarButtonItem
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.mnuCatCorrespondenciaClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatCamposCorrespondencia = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProAsignacionClientesJuridico = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepImpresionCorrespondencia = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepComisionesAbogados = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatAbogados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatTiposLlamadas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCamposCorrespondencia), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCorrespondenciaClientes)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatLlamadasClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCapturaClientesJuridico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAsignacionClientesJuridico)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAbonosClientesJuridico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepClientesEnJuridicoPorEstatus), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepImpresionCorrespondencia), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepComisionesAbogados)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuProAcreedoresDiversos, Me.mnuProMovimientosPagar, Me.mnuProMovimientosChequera, Me.mnuProPagoVariasFacturas, Me.mnuCatAbogados, Me.mnuCatLlamadasClientes, Me.mnuCatTiposLlamadas, Me.mnuProCapturaClientesJuridico, Me.mnuRepClientesEnJuridicoPorEstatus, Me.mnuRepAbonosClientesJuridico, Me.mnuDocumentos, Me.BarEditItem1, Me.mnuCatCorrespondenciaClientes, Me.mnuCatCamposCorrespondencia, Me.mnuProAsignacionClientesJuridico, Me.BarStaticItem1, Me.mnuRepImpresionCorrespondencia, Me.mnuRepComisionesAbogados})
        Me.BarManager.MaxItemId = 142
        Me.BarManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        '
        'mnuProAcreedoresDiversos
        '
        Me.mnuProAcreedoresDiversos.Caption = "Acreedores &Diversos"
        Me.mnuProAcreedoresDiversos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProAcreedoresDiversos.Id = 110
        Me.mnuProAcreedoresDiversos.ImageIndex = 5
        Me.mnuProAcreedoresDiversos.Name = "mnuProAcreedoresDiversos"
        '
        'mnuProMovimientosPagar
        '
        Me.mnuProMovimientosPagar.Caption = "&Movimientos Pagar"
        Me.mnuProMovimientosPagar.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProMovimientosPagar.Id = 111
        Me.mnuProMovimientosPagar.ImageIndex = 11
        Me.mnuProMovimientosPagar.Name = "mnuProMovimientosPagar"
        '
        'mnuProMovimientosChequera
        '
        Me.mnuProMovimientosChequera.Caption = "Movimientos de C&hequera"
        Me.mnuProMovimientosChequera.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProMovimientosChequera.Id = 113
        Me.mnuProMovimientosChequera.ImageIndex = 4
        Me.mnuProMovimientosChequera.Name = "mnuProMovimientosChequera"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(77, 475)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 3
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Id = 139
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        Me.BarStaticItem1.Width = 32
        '
        'mnuProPagoVariasFacturas
        '
        Me.mnuProPagoVariasFacturas.Caption = "Pago a Varias &Facturas"
        Me.mnuProPagoVariasFacturas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProPagoVariasFacturas.Id = 123
        Me.mnuProPagoVariasFacturas.ImageIndex = 15
        Me.mnuProPagoVariasFacturas.Name = "mnuProPagoVariasFacturas"
        '
        'mnuCatAbogados
        '
        Me.mnuCatAbogados.Caption = "A&bogados"
        Me.mnuCatAbogados.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatAbogados.Id = 126
        Me.mnuCatAbogados.ImageIndex = 3
        Me.mnuCatAbogados.Name = "mnuCatAbogados"
        '
        'mnuCatLlamadasClientes
        '
        Me.mnuCatLlamadasClientes.Caption = "&Llamadas a Clientes"
        Me.mnuCatLlamadasClientes.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuCatLlamadasClientes.Id = 127
        Me.mnuCatLlamadasClientes.ImageIndex = 1
        Me.mnuCatLlamadasClientes.Name = "mnuCatLlamadasClientes"
        '
        'mnuCatTiposLlamadas
        '
        Me.mnuCatTiposLlamadas.Caption = "Tipos de Llamadas"
        Me.mnuCatTiposLlamadas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatTiposLlamadas.Id = 128
        Me.mnuCatTiposLlamadas.ImageIndex = 0
        Me.mnuCatTiposLlamadas.Name = "mnuCatTiposLlamadas"
        '
        'mnuProCapturaClientesJuridico
        '
        Me.mnuProCapturaClientesJuridico.Caption = "Captura de Clientes en Jur�dico"
        Me.mnuProCapturaClientesJuridico.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProCapturaClientesJuridico.Id = 129
        Me.mnuProCapturaClientesJuridico.ImageIndex = 2
        Me.mnuProCapturaClientesJuridico.Name = "mnuProCapturaClientesJuridico"
        '
        'mnuRepClientesEnJuridicoPorEstatus
        '
        Me.mnuRepClientesEnJuridicoPorEstatus.Caption = "&Clientes en Jur�dico por Estatus"
        Me.mnuRepClientesEnJuridicoPorEstatus.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepClientesEnJuridicoPorEstatus.Id = 130
        Me.mnuRepClientesEnJuridicoPorEstatus.ImageIndex = 7
        Me.mnuRepClientesEnJuridicoPorEstatus.Name = "mnuRepClientesEnJuridicoPorEstatus"
        '
        'mnuRepAbonosClientesJuridico
        '
        Me.mnuRepAbonosClientesJuridico.Caption = "Abonos de Clientes en &Jur�dico"
        Me.mnuRepAbonosClientesJuridico.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAbonosClientesJuridico.Id = 131
        Me.mnuRepAbonosClientesJuridico.ImageIndex = 6
        Me.mnuRepAbonosClientesJuridico.Name = "mnuRepAbonosClientesJuridico"
        '
        'mnuDocumentos
        '
        Me.mnuDocumentos.Caption = "Documentos"
        Me.mnuDocumentos.CategoryGuid = New System.Guid("172a4a9e-1131-44d5-9c0d-3977a9609339")
        Me.mnuDocumentos.Id = 132
        Me.mnuDocumentos.Name = "mnuDocumentos"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Caption = "Documentos"
        Me.BarEditItem1.Edit = Me.RepositoryItemTextEdit1
        Me.BarEditItem1.Id = 133
        Me.BarEditItem1.Name = "BarEditItem1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'mnuCatCorrespondenciaClientes
        '
        Me.mnuCatCorrespondenciaClientes.Caption = "C&orrespondencia Clientes"
        Me.mnuCatCorrespondenciaClientes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCorrespondenciaClientes.Id = 136
        Me.mnuCatCorrespondenciaClientes.ImageIndex = 5
        Me.mnuCatCorrespondenciaClientes.Name = "mnuCatCorrespondenciaClientes"
        '
        'mnuCatCamposCorrespondencia
        '
        Me.mnuCatCamposCorrespondencia.Caption = "Ca&mpos Correspondencia"
        Me.mnuCatCamposCorrespondencia.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCamposCorrespondencia.Id = 137
        Me.mnuCatCamposCorrespondencia.ImageIndex = 4
        Me.mnuCatCamposCorrespondencia.Name = "mnuCatCamposCorrespondencia"
        '
        'mnuProAsignacionClientesJuridico
        '
        Me.mnuProAsignacionClientesJuridico.Caption = "Asi&gnaci�n de Clientes a Jur�dico"
        Me.mnuProAsignacionClientesJuridico.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProAsignacionClientesJuridico.Id = 138
        Me.mnuProAsignacionClientesJuridico.ImageIndex = 9
        Me.mnuProAsignacionClientesJuridico.Name = "mnuProAsignacionClientesJuridico"
        '
        'mnuRepImpresionCorrespondencia
        '
        Me.mnuRepImpresionCorrespondencia.Caption = "&Impresi�n de Correspondencia"
        Me.mnuRepImpresionCorrespondencia.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepImpresionCorrespondencia.Id = 140
        Me.mnuRepImpresionCorrespondencia.ImageIndex = 8
        Me.mnuRepImpresionCorrespondencia.Name = "mnuRepImpresionCorrespondencia"
        '
        'mnuRepComisionesAbogados
        '
        Me.mnuRepComisionesAbogados.Caption = "Co&misiones a Abogados"
        Me.mnuRepComisionesAbogados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepComisionesAbogados.Id = 141
        Me.mnuRepComisionesAbogados.ImageIndex = 10
        Me.mnuRepComisionesAbogados.Name = "mnuRepComisionesAbogados"
        '
        'FrmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "FrmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region " Cat�logos "

    Public Sub mnuCatAbogados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatAbogados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAbogados As New VillarrealBusiness.clsAbogados
        With oGrid
            .Title = "Abogados"
            .UpdateTitle = "Abogados"
            .DataSource = AddressOf oAbogados.Listado
            .UpdateForm = New frmAbogados
            .Format = AddressOf for_abogados_grs
            .MdiParent = Me
            .Show()
        End With
        oAbogados = Nothing
    End Sub
    Public Sub mnuCatTiposLlamadas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatTiposLlamadas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTiposLlamadas As New VillarrealBusiness.clsTiposLlamadas
        With oGrid
            .Title = "Tipos de Llamadas"
            .UpdateTitle = "Tipo de Llamada"
            .DataSource = AddressOf oTiposLlamadas.Listado
            .UpdateForm = New Comunes.frmTiposLlamadas
            .Format = AddressOf for_tipos_llamadas_grs
            .MdiParent = Me
            .Show()
        End With
        oTiposLlamadas = Nothing
    End Sub
    Public Sub mnuCatCorrespondenciaClientes_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCorrespondenciaClientes.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCorrespondenciaClientes As New VillarrealBusiness.clsCorrespondenciaClientes
        Dim oFormato As New Comunes.clsFormato
        With oGrid
            .CanInsert() = True
            .CanDelete() = True
            .CanUpdate() = True
            .Title = "Correspondencia de Clientes"
            .UpdateTitle = "Correspondencia de Clientes"
            .DataSource = AddressOf oCorrespondenciaClientes.Listado
            .UpdateForm = New Comunes.frmCorrespondenciaClientes
            .Format = AddressOf for_correspondencia_clientes_grs
            .MdiParent = Me
            .Show()
        End With
        oCorrespondenciaClientes = Nothing
    End Sub
    Public Sub mnuCatCamposCorrespondencia_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCamposCorrespondencia.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCamposCorrespondencia As New VillarrealBusiness.clsCamposCorrespondencia
        With oGrid
            .Title = "Campos Correspondencia"
            .UpdateTitle = "Campo de Correspondencia"
            .DataSource = AddressOf oCamposCorrespondencia.Listado
            .UpdateForm = New frmCamposCorrespondencia
            .Format = AddressOf for_campos_correspondencia_grs
            .MdiParent = Me
            .Show()
        End With
        oCamposCorrespondencia = Nothing

    End Sub

#End Region

#Region " Procesos "

    Public Sub mnuCatLlamadasClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatLlamadasClientes.ItemClick
        Dim oForm As New Comunes.frmLlamadasClientes

        With oForm
            .MenuOption = e.Item
            .Title = "Llamadas a Clientes"
            .MdiParent = Me
            .entro_catalogo_clientes = False
            .StartPosition() = FormStartPosition.Manual
            .Show()

        End With
    End Sub

    Public Sub mnuProCapturaClientesJuridico_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCapturaClientesJuridico.ItemClick
        Dim oGrid As New brwClientesJuridico
        '  Dim oClientesJuridico As New VillarrealBusiness.clsClientesJuridico
        With oGrid
            .MenuOption = e.Item
            .Title = "Captura de Clientes en Jur�dico"
            .UpdateTitle = "Captura de Clientes"
            .UpdateForm = New frmCapturaClientesJuridico
            .Format = AddressOf for_clientes_juridico_grs
            .MdiParent = Me
            .StartPosition() = FormStartPosition.Manual
            .Show()
        End With
        '    oClientesJuridico = Nothing
    End Sub

    Public Sub mnuProAsignacionClientesJuridico_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAsignacionClientesJuridico.ItemClick
        Dim oForm As New frmAsignacionClientesJuridico

        With oForm
            .MenuOption = e.Item
            .Title = "Asignaci�n de Clientes a Jur�dico"
            .MdiParent = Me
            .StartPosition() = FormStartPosition.Manual
            .Show()
        End With

    End Sub

    'Public Sub mnuProDevolucionesProveedor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDevolucionesProveedor.ItemClick
    '    Dim oGrid As New brwDevolucionesProveedor
    '    With oGrid
    '        .MenuOption = e.Item
    '        .Title = "Devoluciones Proveedor"
    '        .UpdateTitle = "Devoluci�n Proveedor"
    '        .UpdateForm = New frmDevolucionesProveedor
    '        .Format = AddressOf for_devoluciones_proveedor_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    'End Sub


#End Region

#Region " Reportes "

    Public Sub mnuRepClientesEnJuridicoPorEstatus_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepClientesEnJuridicoPorEstatus.ItemClick
        Dim oForm As New frmRepClientesEnJuridicoPorEstatus
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Clientes En Jur�dico Por Estatus"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepAbonosClientesJuridico_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAbonosClientesJuridico.ItemClick
        Dim oForm As New frmRepAbonosClientesJuridico
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Abonos de Clientes en Jur�dico"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepImpresionCorrespondencia_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepImpresionCorrespondencia.ItemClick
        Dim oForm As New frmRepImprimeCorrespondencia
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Imprime Correspondencia"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepComisionesAbogados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepComisionesAbogados.ItemClick
        Dim oForm As New frmRepComisionesAbogados
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Comisiones a Abogados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub


#End Region


    
   
End Class
