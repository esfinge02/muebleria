Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class brwClientesJuridico
    Inherits Dipros.Windows.frmTINGridNet

    Private oClientesJuridico As New VillarrealBusiness.clsClientesJuridico
    Private oAbogados As New VillarrealBusiness.clsAbogados

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkTodos As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.chkTodos = New DevExpress.XtraEditors.CheckEdit
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'FooterPanel
        '
        Me.FooterPanel.Name = "FooterPanel"
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.chkTodos)
        Me.FilterPanel.Controls.Add(Me.lblAbogado)
        Me.FilterPanel.Controls.Add(Me.lkpAbogado)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Visible = True
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpAbogado, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.chkTodos, 0)
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(29, 14)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(53, 16)
        Me.lblAbogado.TabIndex = 4
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "&Abogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = True
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(88, 13)
        Me.lkpAbogado.MultiSelect = False
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = ""
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = False
        Me.lkpAbogado.Size = New System.Drawing.Size(360, 20)
        Me.lkpAbogado.TabIndex = 5
        Me.lkpAbogado.Tag = "Abogado"
        Me.lkpAbogado.ToolTip = Nothing
        Me.lkpAbogado.ValueMember = "Abogado"
        '
        'chkTodos
        '
        Me.chkTodos.Location = New System.Drawing.Point(472, 14)
        Me.chkTodos.Name = "chkTodos"
        '
        'chkTodos.Properties
        '
        Me.chkTodos.Properties.Caption = "Todos"
        Me.chkTodos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkTodos.Size = New System.Drawing.Size(104, 19)
        Me.chkTodos.TabIndex = 6
        '
        'brwClientesJuridico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(770, 468)
        Me.Name = "brwClientesJuridico"
        Me.Text = "brwClientesJuridico"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Public Property Abogado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAbogado)
        End Get
        Set(ByVal Value As Long)
            Me.lkpAbogado.EditValue = Value
        End Set
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub brwClientesJuridico_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        If Me.chkTodos.Checked = True Then
            Response = oClientesJuridico.Listado()
            Me.View.Columns.Item("nombre_abogado").VisibleIndex = 1
        Else
            Response = oClientesJuridico.Listado(Me.lkpAbogado.ToXML, False)
            Me.View.Columns.Item("nombre_abogado").VisibleIndex = -1
        End If

    End Sub
    Private Sub brwClientesJuridico_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oClientesJuridico.Validacionbrw(Me.CurrentAction, Abogado)
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim Response As New Events
        Response = oAbogados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpAbogado_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpAbogado.EditValueChanged
        Me.Reload()
    End Sub

    Private Sub chkTodos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTodos.EditValueChanged
        Me.lkpAbogado.Enabled = Not chkTodos.Checked
        Me.Abogado = Nothing
        Me.Reload()

    End Sub

#End Region

End Class


