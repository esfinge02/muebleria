Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmAbogados
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents clcAbogado As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblMunicipio As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblLocalidad As System.Windows.Forms.Label
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCodigo_Postal As System.Windows.Forms.Label
    Friend WithEvents clcCodigo_Postal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTelefono_Casa As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Casa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono_Oficina As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Oficina As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono_Celular As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Celular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblComision As System.Windows.Forms.Label
    Friend WithEvents clcComision As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblpercent As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAbogados))
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.clcAbogado = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lblMunicipio = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblLocalidad = New System.Windows.Forms.Label
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.lblCodigo_Postal = New System.Windows.Forms.Label
        Me.clcCodigo_Postal = New Dipros.Editors.TINCalcEdit
        Me.lblTelefono_Casa = New System.Windows.Forms.Label
        Me.txtTelefono_Casa = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono_Oficina = New System.Windows.Forms.Label
        Me.txtTelefono_Oficina = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono_Celular = New System.Windows.Forms.Label
        Me.txtTelefono_Celular = New DevExpress.XtraEditors.TextEdit
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtEmail = New DevExpress.XtraEditors.TextEdit
        Me.lblComision = New System.Windows.Forms.Label
        Me.clcComision = New Dipros.Editors.TINCalcEdit
        Me.lblpercent = New System.Windows.Forms.Label
        CType(Me.clcAbogado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigo_Postal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Casa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Oficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Celular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2515, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(36, 42)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 0
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "A&bogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAbogado
        '
        Me.clcAbogado.EditValue = "0"
        Me.clcAbogado.Location = New System.Drawing.Point(96, 40)
        Me.clcAbogado.MaxValue = 0
        Me.clcAbogado.MinValue = 0
        Me.clcAbogado.Name = "clcAbogado"
        '
        'clcAbogado.Properties
        '
        Me.clcAbogado.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcAbogado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAbogado.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcAbogado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAbogado.Properties.Enabled = False
        Me.clcAbogado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAbogado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAbogado.Size = New System.Drawing.Size(54, 19)
        Me.clcAbogado.TabIndex = 1
        Me.clcAbogado.Tag = "abogado"
        Me.clcAbogado.ToolTip = "abogado"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(40, 65)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(96, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(528, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.ToolTip = "nombre"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(33, 115)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 6
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "&Direcci�n:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(96, 112)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.MaxLength = 100
        Me.txtDireccion.Size = New System.Drawing.Size(528, 20)
        Me.txtDireccion.TabIndex = 7
        Me.txtDireccion.Tag = "direccion"
        Me.txtDireccion.ToolTip = "direcci�n"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(47, 139)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 8
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "E&stado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(96, 136)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(254, 20)
        Me.lkpEstado.TabIndex = 9
        Me.lkpEstado.Tag = "Estado"
        Me.lkpEstado.ToolTip = "estado"
        Me.lkpEstado.ValueMember = "Estado"
        '
        'lblMunicipio
        '
        Me.lblMunicipio.AutoSize = True
        Me.lblMunicipio.Location = New System.Drawing.Point(33, 163)
        Me.lblMunicipio.Name = "lblMunicipio"
        Me.lblMunicipio.Size = New System.Drawing.Size(60, 16)
        Me.lblMunicipio.TabIndex = 10
        Me.lblMunicipio.Tag = ""
        Me.lblMunicipio.Text = "&Municipio:"
        Me.lblMunicipio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = False
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(96, 160)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(400, Long)
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(254, 20)
        Me.lkpMunicipio.TabIndex = 11
        Me.lkpMunicipio.Tag = "Municipio"
        Me.lkpMunicipio.ToolTip = "municipio"
        Me.lkpMunicipio.ValueMember = "Municipio"
        '
        'lblLocalidad
        '
        Me.lblLocalidad.AutoSize = True
        Me.lblLocalidad.Location = New System.Drawing.Point(33, 187)
        Me.lblLocalidad.Name = "lblLocalidad"
        Me.lblLocalidad.Size = New System.Drawing.Size(60, 16)
        Me.lblLocalidad.TabIndex = 12
        Me.lblLocalidad.Tag = ""
        Me.lblLocalidad.Text = "&Localidad:"
        Me.lblLocalidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = False
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(96, 184)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(400, Long)
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(254, 20)
        Me.lkpCiudad.TabIndex = 13
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = "ciudad"
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(43, 211)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 14
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "Colon&ia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = False
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(96, 208)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(400, Long)
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(254, 20)
        Me.lkpColonia.TabIndex = 15
        Me.lkpColonia.Tag = "Colonia"
        Me.lkpColonia.ToolTip = "colonia"
        Me.lkpColonia.ValueMember = "Colonia"
        '
        'lblCodigo_Postal
        '
        Me.lblCodigo_Postal.AutoSize = True
        Me.lblCodigo_Postal.Location = New System.Drawing.Point(9, 234)
        Me.lblCodigo_Postal.Name = "lblCodigo_Postal"
        Me.lblCodigo_Postal.Size = New System.Drawing.Size(84, 16)
        Me.lblCodigo_Postal.TabIndex = 16
        Me.lblCodigo_Postal.Tag = ""
        Me.lblCodigo_Postal.Text = "C�di&go postal:"
        Me.lblCodigo_Postal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigo_Postal
        '
        Me.clcCodigo_Postal.EditValue = "0"
        Me.clcCodigo_Postal.Location = New System.Drawing.Point(96, 232)
        Me.clcCodigo_Postal.MaxValue = 0
        Me.clcCodigo_Postal.MinValue = 0
        Me.clcCodigo_Postal.Name = "clcCodigo_Postal"
        '
        'clcCodigo_Postal.Properties
        '
        Me.clcCodigo_Postal.Properties.DisplayFormat.FormatString = "########0"
        Me.clcCodigo_Postal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigo_Postal.Properties.EditFormat.FormatString = "########0"
        Me.clcCodigo_Postal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigo_Postal.Properties.MaskData.EditMask = "########0"
        Me.clcCodigo_Postal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigo_Postal.Size = New System.Drawing.Size(78, 19)
        Me.clcCodigo_Postal.TabIndex = 17
        Me.clcCodigo_Postal.Tag = "codigo_postal"
        Me.clcCodigo_Postal.ToolTip = "c�digo postal"
        '
        'lblTelefono_Casa
        '
        Me.lblTelefono_Casa.AutoSize = True
        Me.lblTelefono_Casa.Location = New System.Drawing.Point(385, 163)
        Me.lblTelefono_Casa.Name = "lblTelefono_Casa"
        Me.lblTelefono_Casa.Size = New System.Drawing.Size(85, 16)
        Me.lblTelefono_Casa.TabIndex = 18
        Me.lblTelefono_Casa.Tag = ""
        Me.lblTelefono_Casa.Text = "&Tel�fono casa:"
        Me.lblTelefono_Casa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Casa
        '
        Me.txtTelefono_Casa.EditValue = ""
        Me.txtTelefono_Casa.Location = New System.Drawing.Point(474, 160)
        Me.txtTelefono_Casa.Name = "txtTelefono_Casa"
        '
        'txtTelefono_Casa.Properties
        '
        Me.txtTelefono_Casa.Properties.MaxLength = 25
        Me.txtTelefono_Casa.Size = New System.Drawing.Size(150, 20)
        Me.txtTelefono_Casa.TabIndex = 19
        Me.txtTelefono_Casa.Tag = "telefono_casa"
        Me.txtTelefono_Casa.ToolTip = "tel�fono de casa"
        '
        'lblTelefono_Oficina
        '
        Me.lblTelefono_Oficina.AutoSize = True
        Me.lblTelefono_Oficina.Location = New System.Drawing.Point(374, 187)
        Me.lblTelefono_Oficina.Name = "lblTelefono_Oficina"
        Me.lblTelefono_Oficina.Size = New System.Drawing.Size(96, 16)
        Me.lblTelefono_Oficina.TabIndex = 20
        Me.lblTelefono_Oficina.Tag = ""
        Me.lblTelefono_Oficina.Text = "Tel�&fono oficina:"
        Me.lblTelefono_Oficina.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Oficina
        '
        Me.txtTelefono_Oficina.EditValue = ""
        Me.txtTelefono_Oficina.Location = New System.Drawing.Point(474, 184)
        Me.txtTelefono_Oficina.Name = "txtTelefono_Oficina"
        '
        'txtTelefono_Oficina.Properties
        '
        Me.txtTelefono_Oficina.Properties.MaxLength = 25
        Me.txtTelefono_Oficina.Size = New System.Drawing.Size(150, 20)
        Me.txtTelefono_Oficina.TabIndex = 21
        Me.txtTelefono_Oficina.Tag = "telefono_oficina"
        Me.txtTelefono_Oficina.ToolTip = "tel�fono de oficina"
        '
        'lblTelefono_Celular
        '
        Me.lblTelefono_Celular.AutoSize = True
        Me.lblTelefono_Celular.Location = New System.Drawing.Point(374, 211)
        Me.lblTelefono_Celular.Name = "lblTelefono_Celular"
        Me.lblTelefono_Celular.Size = New System.Drawing.Size(96, 16)
        Me.lblTelefono_Celular.TabIndex = 22
        Me.lblTelefono_Celular.Tag = ""
        Me.lblTelefono_Celular.Text = "Tel�fono cel&ular:"
        Me.lblTelefono_Celular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Celular
        '
        Me.txtTelefono_Celular.EditValue = ""
        Me.txtTelefono_Celular.Location = New System.Drawing.Point(474, 208)
        Me.txtTelefono_Celular.Name = "txtTelefono_Celular"
        '
        'txtTelefono_Celular.Properties
        '
        Me.txtTelefono_Celular.Properties.MaxLength = 25
        Me.txtTelefono_Celular.Size = New System.Drawing.Size(150, 20)
        Me.txtTelefono_Celular.TabIndex = 23
        Me.txtTelefono_Celular.Tag = "telefono_celular"
        Me.txtTelefono_Celular.ToolTip = "tel�fono celular"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(54, 91)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(39, 16)
        Me.lblEmail.TabIndex = 4
        Me.lblEmail.Tag = ""
        Me.lblEmail.Text = "&Email:"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEmail
        '
        Me.txtEmail.EditValue = ""
        Me.txtEmail.Location = New System.Drawing.Point(96, 88)
        Me.txtEmail.Name = "txtEmail"
        '
        'txtEmail.Properties
        '
        Me.txtEmail.Properties.MaxLength = 50
        Me.txtEmail.Size = New System.Drawing.Size(254, 20)
        Me.txtEmail.TabIndex = 5
        Me.txtEmail.Tag = "email"
        Me.txtEmail.ToolTip = "e-mail"
        '
        'lblComision
        '
        Me.lblComision.AutoSize = True
        Me.lblComision.Location = New System.Drawing.Point(411, 234)
        Me.lblComision.Name = "lblComision"
        Me.lblComision.Size = New System.Drawing.Size(59, 16)
        Me.lblComision.TabIndex = 24
        Me.lblComision.Tag = ""
        Me.lblComision.Text = "C&omisi�n:"
        Me.lblComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComision
        '
        Me.clcComision.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcComision.Location = New System.Drawing.Point(474, 232)
        Me.clcComision.MaxValue = 0
        Me.clcComision.MinValue = 0
        Me.clcComision.Name = "clcComision"
        '
        'clcComision.Properties
        '
        Me.clcComision.Properties.DisplayFormat.FormatString = "#0.00"
        Me.clcComision.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.EditFormat.FormatString = "#0.00"
        Me.clcComision.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.MaskData.EditMask = "#0.00"
        Me.clcComision.Properties.MaxLength = 5
        Me.clcComision.Properties.NullText = "00.00"
        Me.clcComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComision.Size = New System.Drawing.Size(46, 19)
        Me.clcComision.TabIndex = 25
        Me.clcComision.Tag = "comision"
        Me.clcComision.ToolTip = "comisi�n"
        '
        'lblpercent
        '
        Me.lblpercent.AutoSize = True
        Me.lblpercent.Location = New System.Drawing.Point(520, 234)
        Me.lblpercent.Name = "lblpercent"
        Me.lblpercent.Size = New System.Drawing.Size(16, 16)
        Me.lblpercent.TabIndex = 26
        Me.lblpercent.Tag = ""
        Me.lblpercent.Text = "%"
        Me.lblpercent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAbogados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(642, 271)
        Me.Controls.Add(Me.lblpercent)
        Me.Controls.Add(Me.lblComision)
        Me.Controls.Add(Me.clcComision)
        Me.Controls.Add(Me.lblAbogado)
        Me.Controls.Add(Me.clcAbogado)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblMunicipio)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblLocalidad)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.lblCodigo_Postal)
        Me.Controls.Add(Me.clcCodigo_Postal)
        Me.Controls.Add(Me.lblTelefono_Casa)
        Me.Controls.Add(Me.txtTelefono_Casa)
        Me.Controls.Add(Me.lblTelefono_Oficina)
        Me.Controls.Add(Me.txtTelefono_Oficina)
        Me.Controls.Add(Me.lblTelefono_Celular)
        Me.Controls.Add(Me.txtTelefono_Celular)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.txtEmail)
        Me.Name = "frmAbogados"
        Me.Controls.SetChildIndex(Me.txtEmail, 0)
        Me.Controls.SetChildIndex(Me.lblEmail, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono_Celular, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono_Celular, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono_Oficina, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono_Oficina, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono_Casa, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono_Casa, 0)
        Me.Controls.SetChildIndex(Me.clcCodigo_Postal, 0)
        Me.Controls.SetChildIndex(Me.lblCodigo_Postal, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblLocalidad, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lblMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.txtDireccion, 0)
        Me.Controls.SetChildIndex(Me.lblDireccion, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.Controls.SetChildIndex(Me.clcComision, 0)
        Me.Controls.SetChildIndex(Me.lblComision, 0)
        Me.Controls.SetChildIndex(Me.lblpercent, 0)
        CType(Me.clcAbogado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigo_Postal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Casa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Oficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Celular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oAbogados As VillarrealBusiness.clsAbogados

    Private ociudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oVariables As New VillarrealBusiness.clsVariables
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private Entro_Despliega As Boolean = False

    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property

    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property

    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property

    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmAbogados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oAbogados.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oAbogados.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oAbogados.Eliminar(clcAbogado.value)

        End Select
    End Sub

    Private Sub frmAbogados_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Entro_Despliega = True

        Response = oAbogados.DespliegaDatos(OwnerForm.Value("abogado"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmAbogados_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oAbogados = New VillarrealBusiness.clsAbogados
        ociudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oEstados = New VillarrealBusiness.clsEstados
        oColonias = New VillarrealBusiness.clsColonias


        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmAbogados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oAbogados.Validacion(Action, Me.txtNombre.EditValue, Me.txtDireccion.EditValue, Estado, Municipio, Ciudad, Colonia, Me.clcComision.EditValue)
    End Sub

    Private Sub frmAbogados_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        'If Me.lkpMunicipio.DataSource Is Nothing Or Me.lkpMunicipio.EditValue Is Nothing Then Exit Sub
        If Entro_Despliega = True Then Exit Sub
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)

    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        'If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
