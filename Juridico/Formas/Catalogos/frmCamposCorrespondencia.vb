Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports ErrP = System.Windows.Forms.ErrorProvider
Imports SendKey = System.Windows.Forms.SendKeys



Public Class frmCamposCorrespondencia
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCampoCorrespondencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents listCamposTabla As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTablas As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtSintaxis As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnComprobarSintaxis As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cboTipoDato As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCamposCorrespondencia))
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.listCamposTabla = New DevExpress.XtraEditors.ListBoxControl
        Me.txtCampoCorrespondencia = New DevExpress.XtraEditors.TextEdit
        Me.cboTablas = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtSintaxis = New DevExpress.XtraEditors.MemoEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnComprobarSintaxis = New DevExpress.XtraEditors.SimpleButton
        Me.cboTipoDato = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label6 = New System.Windows.Forms.Label
        CType(Me.listCamposTabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCampoCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTablas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSintaxis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoDato.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1202, 28)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Campo de Correspondencia:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(144, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tabla:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Campos de la Tabla"
        '
        'listCamposTabla
        '
        Me.listCamposTabla.ItemHeight = 15
        Me.listCamposTabla.Location = New System.Drawing.Point(16, 144)
        Me.listCamposTabla.Name = "listCamposTabla"
        Me.listCamposTabla.Size = New System.Drawing.Size(152, 136)
        Me.listCamposTabla.TabIndex = 5
        Me.listCamposTabla.ToolTip = "Campos Disponibles de la Tabla"
        '
        'txtCampoCorrespondencia
        '
        Me.txtCampoCorrespondencia.EditValue = ""
        Me.txtCampoCorrespondencia.Location = New System.Drawing.Point(192, 40)
        Me.txtCampoCorrespondencia.Name = "txtCampoCorrespondencia"
        '
        'txtCampoCorrespondencia.Properties
        '
        Me.txtCampoCorrespondencia.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCampoCorrespondencia.Properties.MaxLength = 80
        Me.txtCampoCorrespondencia.Size = New System.Drawing.Size(312, 20)
        Me.txtCampoCorrespondencia.TabIndex = 1
        Me.txtCampoCorrespondencia.Tag = "campo_correspondencia"
        '
        'cboTablas
        '
        Me.cboTablas.EditValue = ""
        Me.cboTablas.Location = New System.Drawing.Point(192, 64)
        Me.cboTablas.Name = "cboTablas"
        '
        'cboTablas.Properties
        '
        Me.cboTablas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTablas.Size = New System.Drawing.Size(176, 20)
        Me.cboTablas.TabIndex = 3
        Me.cboTablas.Tag = "clave_tabla"
        Me.cboTablas.ToolTip = "Tablas de la Base de Datos"
        '
        'txtSintaxis
        '
        Me.txtSintaxis.EditValue = ""
        Me.txtSintaxis.Location = New System.Drawing.Point(176, 144)
        Me.txtSintaxis.Name = "txtSintaxis"
        Me.txtSintaxis.Size = New System.Drawing.Size(328, 136)
        Me.txtSintaxis.TabIndex = 7
        Me.txtSintaxis.Tag = "campo_tabla"
        Me.txtSintaxis.ToolTip = "Escriba la Sintaxis del Campo a Crear"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(184, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Sintaxis"
        '
        'btnComprobarSintaxis
        '
        Me.btnComprobarSintaxis.Location = New System.Drawing.Point(176, 288)
        Me.btnComprobarSintaxis.Name = "btnComprobarSintaxis"
        Me.btnComprobarSintaxis.Size = New System.Drawing.Size(144, 23)
        Me.btnComprobarSintaxis.TabIndex = 8
        Me.btnComprobarSintaxis.Text = "Comprobar Sintaxis"
        '
        'cboTipoDato
        '
        Me.cboTipoDato.EditValue = "C"
        Me.cboTipoDato.Location = New System.Drawing.Point(192, 88)
        Me.cboTipoDato.Name = "cboTipoDato"
        '
        'cboTipoDato.Properties
        '
        Me.cboTipoDato.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoDato.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cadena", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Moneda", "M", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Numeric", "N", -1)})
        Me.cboTipoDato.Size = New System.Drawing.Size(112, 20)
        Me.cboTipoDato.TabIndex = 3
        Me.cboTipoDato.Tag = "tipo_dato"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(103, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Tipo de Dato:"
        '
        'frmCamposCorrespondencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(522, 320)
        Me.Controls.Add(Me.btnComprobarSintaxis)
        Me.Controls.Add(Me.txtSintaxis)
        Me.Controls.Add(Me.cboTablas)
        Me.Controls.Add(Me.txtCampoCorrespondencia)
        Me.Controls.Add(Me.listCamposTabla)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboTipoDato)
        Me.Controls.Add(Me.Label6)
        Me.Name = "frmCamposCorrespondencia"
        Me.Text = "frmCamposCorrespondencia"
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.cboTipoDato, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.listCamposTabla, 0)
        Me.Controls.SetChildIndex(Me.txtCampoCorrespondencia, 0)
        Me.Controls.SetChildIndex(Me.cboTablas, 0)
        Me.Controls.SetChildIndex(Me.txtSintaxis, 0)
        Me.Controls.SetChildIndex(Me.btnComprobarSintaxis, 0)
        CType(Me.listCamposTabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCampoCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTablas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSintaxis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoDato.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCamposCorrespondencia As VillarrealBusiness.clsCamposCorrespondencia
    Private oUtilerias As VillarrealBusiness.clsUtilerias
    Private bSintaxisValida As Boolean = False

    Private ReadOnly Property SintaxisValida() As Boolean
        Get
            Return bSintaxisValida
        End Get
    End Property

    Private ReadOnly Property CampoCorrespondencia() As String
        Get
            Return "@" + Me.txtCampoCorrespondencia.Text
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCamposCorrespondencia_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCamposCorrespondencia = New VillarrealBusiness.clsCamposCorrespondencia
        oUtilerias = New VillarrealBusiness.clsUtilerias
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
        LLenaTablas(Response)
    End Sub
    Private Sub frmCamposCorrespondencia_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCamposCorrespondencia.Insertar(CampoCorrespondencia, Me.cboTablas.Text, Me.txtSintaxis.Text.TrimStart, Me.cboTipoDato.Value)

            Case Actions.Update
                Response = oCamposCorrespondencia.Actualizar(CampoCorrespondencia.Remove(0, 1), Me.cboTablas.Text, Me.txtSintaxis.Text, Me.cboTipoDato.Value)

            Case Actions.Delete
                Response = oCamposCorrespondencia.Eliminar(CampoCorrespondencia.Remove(0, 1))

        End Select
    End Sub
    Private Sub frmCamposCorrespondencia_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oCamposCorrespondencia.DespliegaDatos(OwnerForm.Value("campo_correspondencia"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub
    Private Sub frmCamposCorrespondencia_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCamposCorrespondencia.Validacion(Action, CampoCorrespondencia, Me.txtSintaxis.Text, Me.SintaxisValida)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub btnComprobarSintaxis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComprobarSintaxis.Click
        bSintaxisValida = ValidaSintaxis()
    End Sub
    Private Sub listCamposTabla_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listCamposTabla.DoubleClick
        Me.txtSintaxis.Text = Me.txtSintaxis.Text & " " & Me.listCamposTabla.SelectedValue
    End Sub
    Private Sub cboTablas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTablas.SelectedIndexChanged
        txtSintaxis.Text = ""
        LLenaCamposTablas(Me.cboTablas.Value)

    End Sub
    Private Sub txtCampoCorrespondencia_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCampoCorrespondencia.KeyPress
        If e.KeyChar = ChrW(32) Or e.KeyChar = ChrW(64) Then
            e.Handled = True
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LLenaTablas(ByRef response As Events)

        response = oUtilerias.Tablas("Cat")
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value
            Dim i As Long
            For i = 1 To odataset.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = odataset.Tables(0).Rows(i - 1).Item("tabla")
                item_for.Value = odataset.Tables(0).Rows(i - 1).Item("clave_tabla")
                Me.cboTablas.Properties.Items.Add(item_for)
            Next

            Me.cboTablas.SelectedIndex = 0
            odataset = Nothing
        End If

    End Sub
    Private Sub LLenaCamposTablas(ByVal id_tabla As Long)

        Dim response As New Dipros.Utils.Events

        response = oUtilerias.Campos_Tablas(id_tabla)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim i As Long

            odataset = response.Value
            Me.listCamposTabla.Items.Clear()
            For i = 1 To odataset.Tables(0).Rows.Count
                Me.listCamposTabla.Items.Add(odataset.Tables(0).Rows.Item(i - 1).Item("campo"))
            Next
        Else
            ShowMessage(MessageType.MsgError, "Error al Cargar los Campos de la Tabla")
        End If

    End Sub
    Private Function ValidaSintaxis() As Boolean
        Dim sCadena_Evaluar As String

        Try
            TINApp.Connection.Begin()

            sCadena_Evaluar = "SELECT " & Me.txtSintaxis.Text + " FROM " & Me.cboTablas.Text
            TINApp.Connection.GetDataSet(sCadena_Evaluar)

            ValidaSintaxis = True
            ShowMessage(MessageType.MsgInformation, "Sintaxis Valida")

        Catch ex As Exception
            ValidaSintaxis = False
            ShowMessage(MessageType.MsgError, "Error en la Sintaxis", , ex)
        Finally
            TINApp.Connection.Rollback()
        End Try
    End Function

#End Region


   
End Class



