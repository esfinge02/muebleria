Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepClientesEnJuridicoPorEstatus
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepClientesEnJuridicoPorEstatus))
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(348, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(19, 72)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 2
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "A&bogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = False
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(80, 72)
        Me.lkpAbogado.MultiSelect = True
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = "(Todos)"
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = True
        Me.lkpAbogado.Size = New System.Drawing.Size(336, 20)
        Me.lkpAbogado.TabIndex = 3
        Me.lkpAbogado.Tag = "Abogado"
        Me.lkpAbogado.ToolTip = "abogado"
        Me.lkpAbogado.ValueMember = "Abogado"
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "AS"
        Me.cboStatus.Location = New System.Drawing.Point(80, 96)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Asignado", "AS", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Sin Localizar", "SL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Juicio", "EJ", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Terminado", "TE", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 20)
        Me.cboStatus.TabIndex = 5
        Me.cboStatus.Tag = "status"
        Me.cboStatus.ToolTip = "status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(32, 96)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 4
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "S&tatus:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(20, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 48)
        Me.lkpSucursal.MultiSelect = True
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(336, 20)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'frmRepClientesEnJuridicoPorEstatus
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(434, 136)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lblAbogado)
        Me.Controls.Add(Me.lkpAbogado)
        Me.Name = "frmRepClientesEnJuridicoPorEstatus"
        Me.Text = "Clientes En Jur�dico Por Estatus"
        Me.Controls.SetChildIndex(Me.lkpAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oAbogados As New VillarrealBusiness.clsAbogados

#Region "Propiedades"

    Private ReadOnly Property Sucursal() As String
        Get
            Return Me.lkpSucursal.ToXML(Dipros.Editors.Encoding.ISO8859)
        End Get
    End Property
   
    Private ReadOnly Property Abogado() As String
        Get
            Return Me.lkpAbogado.ToXML(Dipros.Editors.Encoding.ISO8859)
        End Get
    End Property

#End Region

#End Region

#Region "Eventos de la Forma"

    Private Sub frmRepClientesEnJuridicoPorEstatus_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.ClientesEnJuridicoPorEstatus(Sucursal, Abogado, Me.cboStatus.EditValue)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Clientes en Juridico por Estatus no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptClientesEnJuridicoPorEstatus
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Clientes En Juridico Por Estatus", oReport)
                oReport = Nothing

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub

    Private Sub frmRepClientesEnJuridicoPorEstatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
    End Sub

    Private Sub frmRepClientesEnJuridicoPorEstatus_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.ValidacionSucursalAbogado(Sucursal, Abogado)
    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim Response As New Events
        Response = oAbogados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged

    End Sub

#End Region

End Class
