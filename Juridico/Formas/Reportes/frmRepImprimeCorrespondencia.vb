Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepImprimeCorrespondencia
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grvClientesCorrespondencia As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDireccion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grClientesCorrespondencia As DevExpress.XtraGrid.GridControl
    Friend WithEvents RepositoryItemCheckSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents btnImpEtiquetas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RepositoryItemLookClientes As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents lkpCorrespondencia As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepImprimeCorrespondencia))
        Me.lkpCorrespondencia = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.grClientesCorrespondencia = New DevExpress.XtraGrid.GridControl
        Me.grvClientesCorrespondencia = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookClientes = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDireccion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnImpEtiquetas = New DevExpress.XtraEditors.SimpleButton
        CType(Me.grClientesCorrespondencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientesCorrespondencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4040, 28)
        '
        'lkpCorrespondencia
        '
        Me.lkpCorrespondencia.AllowAdd = False
        Me.lkpCorrespondencia.AutoReaload = False
        Me.lkpCorrespondencia.DataSource = Nothing
        Me.lkpCorrespondencia.DefaultSearchField = ""
        Me.lkpCorrespondencia.DisplayMember = "descripcion"
        Me.lkpCorrespondencia.EditValue = Nothing
        Me.lkpCorrespondencia.Filtered = False
        Me.lkpCorrespondencia.InitValue = Nothing
        Me.lkpCorrespondencia.Location = New System.Drawing.Point(144, 40)
        Me.lkpCorrespondencia.MultiSelect = False
        Me.lkpCorrespondencia.Name = "lkpCorrespondencia"
        Me.lkpCorrespondencia.NullText = ""
        Me.lkpCorrespondencia.PopupWidth = CType(480, Long)
        Me.lkpCorrespondencia.Required = False
        Me.lkpCorrespondencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCorrespondencia.SearchMember = ""
        Me.lkpCorrespondencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCorrespondencia.SelectAll = False
        Me.lkpCorrespondencia.Size = New System.Drawing.Size(403, 20)
        Me.lkpCorrespondencia.TabIndex = 6
        Me.lkpCorrespondencia.Tag = ""
        Me.lkpCorrespondencia.ToolTip = "correspondencia"
        Me.lkpCorrespondencia.ValueMember = "correspondencia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 16)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Correspondencia:"
        '
        'grClientesCorrespondencia
        '
        '
        'grClientesCorrespondencia.EmbeddedNavigator
        '
        Me.grClientesCorrespondencia.EmbeddedNavigator.Name = ""
        Me.grClientesCorrespondencia.Location = New System.Drawing.Point(9, 72)
        Me.grClientesCorrespondencia.MainView = Me.grvClientesCorrespondencia
        Me.grClientesCorrespondencia.Name = "grClientesCorrespondencia"
        Me.grClientesCorrespondencia.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckSeleccionar, Me.RepositoryItemLookClientes})
        Me.grClientesCorrespondencia.Size = New System.Drawing.Size(879, 344)
        Me.grClientesCorrespondencia.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.Info, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesCorrespondencia.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesCorrespondencia.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesCorrespondencia.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesCorrespondencia.TabIndex = 60
        Me.grClientesCorrespondencia.Text = "GridControl1"
        '
        'grvClientesCorrespondencia
        '
        Me.grvClientesCorrespondencia.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcClaveCliente, Me.grcSucursal, Me.grcDireccion, Me.grcSaldo})
        Me.grvClientesCorrespondencia.GridControl = Me.grClientesCorrespondencia
        Me.grvClientesCorrespondencia.Name = "grvClientesCorrespondencia"
        Me.grvClientesCorrespondencia.OptionsMenu.EnableFooterMenu = False
        Me.grvClientesCorrespondencia.OptionsView.ShowGroupPanel = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.RepositoryItemCheckSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 97
        '
        'RepositoryItemCheckSeleccionar
        '
        Me.RepositoryItemCheckSeleccionar.AutoHeight = False
        Me.RepositoryItemCheckSeleccionar.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.RepositoryItemCheckSeleccionar.Name = "RepositoryItemCheckSeleccionar"
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cuenta"
        Me.grcClaveCliente.ColumnEdit = Me.RepositoryItemLookClientes
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.VisibleIndex = 1
        Me.grcClaveCliente.Width = 264
        '
        'RepositoryItemLookClientes
        '
        Me.RepositoryItemLookClientes.AutoHeight = False
        Me.RepositoryItemLookClientes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookClientes.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("cliente", "Cuenta", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("nombre", "Cliente", 100)})
        Me.RepositoryItemLookClientes.DisplayMember = "nombre"
        Me.RepositoryItemLookClientes.Name = "RepositoryItemLookClientes"
        Me.RepositoryItemLookClientes.NullText = "[Sin Valor]"
        Me.RepositoryItemLookClientes.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.WindowText)
        Me.RepositoryItemLookClientes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.RepositoryItemLookClientes.ValueMember = "cliente"
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursal.VisibleIndex = 2
        Me.grcSucursal.Width = 169
        '
        'grcDireccion
        '
        Me.grcDireccion.Caption = "Direcci�n"
        Me.grcDireccion.FieldName = "direccion"
        Me.grcDireccion.Name = "grcDireccion"
        Me.grcDireccion.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDireccion.VisibleIndex = 3
        Me.grcDireccion.Width = 253
        '
        'grcSaldo
        '
        Me.grcSaldo.Caption = "Saldo"
        Me.grcSaldo.DisplayFormat.FormatString = "c2"
        Me.grcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldo.FieldName = "saldo"
        Me.grcSaldo.HeaderStyleName = "Style1"
        Me.grcSaldo.Name = "grcSaldo"
        Me.grcSaldo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldo.VisibleIndex = 4
        Me.grcSaldo.Width = 107
        '
        'btnImpEtiquetas
        '
        Me.btnImpEtiquetas.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnImpEtiquetas.Location = New System.Drawing.Point(382, 432)
        Me.btnImpEtiquetas.Name = "btnImpEtiquetas"
        Me.btnImpEtiquetas.Size = New System.Drawing.Size(135, 23)
        Me.btnImpEtiquetas.TabIndex = 61
        Me.btnImpEtiquetas.Text = "Imprimir Etiquetas"
        '
        'frmRepImprimeCorrespondencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(898, 464)
        Me.Controls.Add(Me.grClientesCorrespondencia)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpCorrespondencia)
        Me.Controls.Add(Me.btnImpEtiquetas)
        Me.Name = "frmRepImprimeCorrespondencia"
        Me.Text = "frmRepImprimeCorrespondencia"
        Me.Controls.SetChildIndex(Me.btnImpEtiquetas, 0)
        Me.Controls.SetChildIndex(Me.lkpCorrespondencia, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.grClientesCorrespondencia, 0)
        CType(Me.grClientesCorrespondencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientesCorrespondencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Dipros Systems, Declaraciones"
    Private oCorrespondenciaClientes As VillarrealBusiness.clsCorrespondenciaClientes
    Private oCorrespondenciaClientesdetalle As VillarrealBusiness.clsCorrespondenciaClientesDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    Private oUtilierias As Comunes.clsUtilerias
    Private oReportes As VillarrealBusiness.Reportes

    Private clone As DataView

    Public Property Correspondencia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCorrespondencia)
        End Get
        Set(ByVal Value As Long)
            Me.lkpCorrespondencia.EditValue = Value
        End Set
    End Property

    Private sTexto_correspondencia As String



#End Region

#Region "Dipros Systems, Eventos de la Forma"
    Private Sub frmRepImprimeCorrespondencia_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCorrespondenciaClientes = New VillarrealBusiness.clsCorrespondenciaClientes
        oCorrespondenciaClientesdetalle = New VillarrealBusiness.clsCorrespondenciaClientesDetalle
        oClientes = New VillarrealBusiness.clsClientes
        oReportes = New VillarrealBusiness.Reportes

        Dim oDataSet As DataSet
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.RepositoryItemLookClientes.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        InsertaNewRow()

    End Sub
    Private Sub frmRepImprimeCorrespondencia_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim i As Long
        Me.grvClientesCorrespondencia.UpdateCurrentRow()
        Me.grvClientesCorrespondencia.CloseEditor()

        For i = 0 To Me.grvClientesCorrespondencia.RowCount - 1

            If (Me.grvClientesCorrespondencia.GetRowCellValue(i, Me.grcSeleccionar) = True) And (Not Me.grvClientesCorrespondencia.GetRowCellValue(i, Me.grcClaveCliente) Is System.DBNull.Value) Then
                Dim cliente As Long = 0
                cliente = Me.grvClientesCorrespondencia.GetRowCellValue(i, Me.grcClaveCliente)
                ImprimeCorrespondencia(Response, cliente)
            End If
        Next

    End Sub
    Private Sub frmRepImprimeCorrespondencia_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCorrespondenciaClientes.Validacion(Me.Correspondencia)
    End Sub
#End Region

#Region "Dipros Systems, Eventos de los Controles"
    Private Sub lkpCorrespondencia_LoadData(ByVal Initialize As Boolean) Handles lkpCorrespondencia.LoadData
        Dim oresponse As Events
        Dim odataset As DataSet

        oresponse = oCorrespondenciaClientes.Lookup
        If Not oresponse.ErrorFound Then
            odataset = oresponse.Value
            Me.lkpCorrespondencia.DataSource = odataset.Tables(0)
        End If
        odataset = Nothing
        oresponse = Nothing
    End Sub
    Private Sub lkpCorrespondencia_Format() Handles lkpCorrespondencia.Format
        Comunes.clsFormato.for_correspondencia_clientes_grl(Me.lkpCorrespondencia)
    End Sub

    Private Sub grvClientesCorrespondencia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvClientesCorrespondencia.KeyDown
        If e.KeyCode = e.KeyCode.Enter Then

            ' Actualiza los Valores al Cambiar de fila
            Me.grvClientesCorrespondencia.CloseEditor()
            Me.grvClientesCorrespondencia.UpdateCurrentRow()

            ' Valida si no tiene cuenta no deja agregar mas filas al grid
            If Me.grvClientesCorrespondencia.GetRowCellValue(Me.grvClientesCorrespondencia.FocusedRowHandle, Me.grcClaveCliente) Is System.DBNull.Value Then Exit Sub

            'Asigno una nueva fila
            If Me.grvClientesCorrespondencia.RowCount - 1 = Me.grvClientesCorrespondencia.FocusedRowHandle Then
                Me.grvClientesCorrespondencia.AddNewRow()
                Me.grvClientesCorrespondencia.FocusedRowHandle = 0
                Me.grvClientesCorrespondencia.MoveLast()
                Me.grvClientesCorrespondencia.SetRowCellValue(Me.grvClientesCorrespondencia.FocusedRowHandle, Me.grcSeleccionar, True)
            End If
           
        End If

    End Sub
    Private Sub grvArticulos_HiddenEditor(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvClientesCorrespondencia.HiddenEditor
        If Not clone Is Nothing Then
            clone.Dispose()
            clone = Nothing
        End If
    End Sub

    Private Sub RepositoryItemLookClientes_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepositoryItemLookClientes.EditValueChanged
        'Obtengo la Fila selecionado del Repositorio Lookup 
        Dim edit As DevExpress.XtraEditors.LookUpEdit = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        Dim orow As DataRowView = edit.Properties.GetDataSourceRowByKeyValue(edit.EditValue)
        Dim FocusedRowHandle As Integer

        'asigno los valores a la vista del grid con los datos que vienen en la fila
        FocusedRowHandle = Me.grvClientesCorrespondencia.FocusedRowHandle
        Me.grvClientesCorrespondencia.SetRowCellValue(FocusedRowHandle, Me.grcSucursal, orow("nombre_sucursal"))
        Me.grvClientesCorrespondencia.SetRowCellValue(FocusedRowHandle, Me.grcDireccion, orow("direccion"))
        Me.grvClientesCorrespondencia.SetRowCellValue(FocusedRowHandle, Me.grcSaldo, orow("saldo"))
    End Sub
    Private Sub lkpCorrespondencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCorrespondencia.EditValueChanged
        If Me.Correspondencia > 0 Then
            sTexto_correspondencia = Me.lkpCorrespondencia.GetValue("texto_correspondencia")
        End If
    End Sub
#End Region

#Region "Dipros Systems, Funcionalidad"
    Private Function CreaDataset() As DataSet
        Dim odata As New DataSet
        Dim row As DataRow

        odata.Tables.Add("generales")
        odata.Tables("generales").Columns.Add("seleccionar").DataType = System.Type.GetType("System.Boolean")
        odata.Tables("generales").Columns.Add("cliente").DataType = System.Type.GetType("System.Int32")
        odata.Tables("generales").Columns.Add("sucursal")
        odata.Tables("generales").Columns.Add("direccion")
        odata.Tables("generales").Columns.Add("saldo").DataType = System.Type.GetType("System.Double")

        CreaDataset = odata
    End Function
    Private Sub InsertaNewRow()
        Me.grClientesCorrespondencia.DataSource = CreaDataset.Tables(0)

        Me.grvClientesCorrespondencia.AddNewRow()
        Me.grvClientesCorrespondencia.FocusedRowHandle = 0
        Me.grvClientesCorrespondencia.MoveLast()
        Me.grvClientesCorrespondencia.SetRowCellValue(Me.grvClientesCorrespondencia.FocusedRowHandle, Me.grcSeleccionar, True)
    End Sub

    Private Function ObtenerCamposCorrespondenciaTexto() As DataTable
        Dim response As Events
        Dim odatatable As DataTable
        response = oCorrespondenciaClientesdetalle.Listado(Correspondencia)
        If Not response.ErrorFound Then
            odatatable = CType(response.Value, DataSet).Tables(0)
        End If

        Return odatatable
    End Function
    Private Sub ImprimeCorrespondencia(ByRef Response As Events, ByVal cliente As Long)
        Dim oReport As New rptCorrespondencia


        Response = oReportes.ImprimeCorrespondenciaClientes(cliente)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La impresi�n de Correspondencia no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            oReport.Texto_Correspondencia = Me.sTexto_correspondencia
            oReport.Cliente = cliente
            oReport.TablaCamposCorrespondenciaDetalle = Me.ObtenerCamposCorrespondenciaTexto
            oReport.DataSource = oDataSet.Tables(0)

            TINApp.ShowReport(Me.MdiParent, "Correspondencia", oReport, , , , False)
            oReport = Nothing
        End If
    End Sub
#End Region


  
End Class

