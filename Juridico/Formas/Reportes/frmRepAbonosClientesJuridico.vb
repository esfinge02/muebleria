Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepAbonosClientesJuridico
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepAbonosClientesJuridico))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(553, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(15, 72)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 2
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "A&bogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = False
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(76, 72)
        Me.lkpAbogado.MultiSelect = True
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = "(Todos)"
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = True
        Me.lkpAbogado.Size = New System.Drawing.Size(320, 20)
        Me.lkpAbogado.TabIndex = 3
        Me.lkpAbogado.Tag = "Abogado"
        Me.lkpAbogado.ToolTip = "Abogado"
        Me.lkpAbogado.ValueMember = "Abogado"
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(14, 104)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(383, 56)
        Me.gpbFechas.TabIndex = 8
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 8, 30, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(74, 22)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Ini.Size = New System.Drawing.Size(94, 20)
        Me.dteFecha_Ini.TabIndex = 6
        Me.dteFecha_Ini.ToolTip = "Fecha Inicial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 8, 30, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(259, 22)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Fin.Size = New System.Drawing.Size(94, 20)
        Me.dteFecha_Fin.TabIndex = 8
        Me.dteFecha_Fin.ToolTip = "Fecha Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(211, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Ha&sta: "
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(76, 48)
        Me.lkpSucursal.MultiSelect = True
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(320, 20)
        Me.lkpSucursal.TabIndex = 59
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'frmRepAbonosClientesJuridico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(410, 175)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblAbogado)
        Me.Controls.Add(Me.lkpAbogado)
        Me.Name = "frmRepAbonosClientesJuridico"
        Me.Text = "frmRepAbonosClientesJuridico"
        Me.Controls.SetChildIndex(Me.lkpAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oAbogados As New VillarrealBusiness.clsAbogados

#Region "Propiedades"


    Private ReadOnly Property Sucursal() As String
        Get
            Return Me.lkpSucursal.ToXML(Dipros.Editors.Encoding.ISO8859)
        End Get
    End Property
    Private ReadOnly Property Abogado() As String
        Get
            Return Me.lkpAbogado.ToXML(Dipros.Editors.Encoding.ISO8859)
        End Get
    End Property

#End Region

#End Region

#Region "Eventos de la Forma"

    Private Sub frmRepAbonosClientesJuridico_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        oAbogados = New VillarrealBusiness.clsAbogados

        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = Format(CDate(TINApp.FechaServidor), "dd/MMM/yyyy")


    End Sub

    Private Sub frmRepAbonosClientesJuridico_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.AbonosClientesJuridico(Sucursal, Abogado, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Abonos de Clientes en Juridico no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptAbonosClientesJuridico
                oReport.DataSource = oDataSet.Tables(0)

                TINApp.ShowReport(Me.MdiParent, "Abonos de Clientes en Jur�dico", oReport)
                oReport = Nothing

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub

    Private Sub frmRepAbonosClientesJuridico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
    End Sub

    Private Sub frmRepAbonosClientesJuridico_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim Response As New Events
        Response = oAbogados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs)

    End Sub

#End Region



End Class
