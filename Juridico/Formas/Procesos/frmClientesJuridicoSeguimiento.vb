Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesJuridicoSeguimiento
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Folio_Juridico As Long
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblSeguimiento As System.Windows.Forms.Label
    Friend WithEvents txtSeguimiento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkAgendar As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesJuridicoSeguimiento))
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblSeguimiento = New System.Windows.Forms.Label
        Me.txtSeguimiento = New DevExpress.XtraEditors.TextEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.chkAgendar = New DevExpress.XtraEditors.CheckEdit
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSeguimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAgendar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1012, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(64, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "29/01/2007"
        Me.dteFecha.Location = New System.Drawing.Point(110, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm:ss "
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy HH:mm:ss"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(154, 20)
        Me.dteFecha.TabIndex = 1
        Me.dteFecha.Tag = "fecha"
        '
        'lblSeguimiento
        '
        Me.lblSeguimiento.AutoSize = True
        Me.lblSeguimiento.Location = New System.Drawing.Point(28, 63)
        Me.lblSeguimiento.Name = "lblSeguimiento"
        Me.lblSeguimiento.Size = New System.Drawing.Size(78, 16)
        Me.lblSeguimiento.TabIndex = 2
        Me.lblSeguimiento.Tag = ""
        Me.lblSeguimiento.Text = "&Seguimiento:"
        Me.lblSeguimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSeguimiento
        '
        Me.txtSeguimiento.EditValue = ""
        Me.txtSeguimiento.Location = New System.Drawing.Point(110, 63)
        Me.txtSeguimiento.Name = "txtSeguimiento"
        '
        'txtSeguimiento.Properties
        '
        Me.txtSeguimiento.Properties.MaxLength = 80
        Me.txtSeguimiento.Size = New System.Drawing.Size(480, 20)
        Me.txtSeguimiento.TabIndex = 3
        Me.txtSeguimiento.Tag = "seguimiento"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(16, 86)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 4
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(110, 86)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(480, 59)
        Me.txtObservaciones.TabIndex = 5
        Me.txtObservaciones.Tag = "observaciones"
        '
        'chkAgendar
        '
        Me.chkAgendar.Location = New System.Drawing.Point(112, 150)
        Me.chkAgendar.Name = "chkAgendar"
        '
        'chkAgendar.Properties
        '
        Me.chkAgendar.Properties.Caption = "Agendar"
        Me.chkAgendar.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAgendar.Size = New System.Drawing.Size(114, 19)
        Me.chkAgendar.TabIndex = 6
        Me.chkAgendar.Tag = "agendar"
        '
        'frmClientesJuridicoSeguimiento
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(600, 176)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblSeguimiento)
        Me.Controls.Add(Me.txtSeguimiento)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.chkAgendar)
        Me.Name = "frmClientesJuridicoSeguimiento"
        Me.Controls.SetChildIndex(Me.chkAgendar, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtSeguimiento, 0)
        Me.Controls.SetChildIndex(Me.lblSeguimiento, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSeguimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAgendar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oClientesJuridicoSeguimiento As VillarrealBusiness.clsClientesJuridicoSeguimiento


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmClientesJuridicoSeguimiento_Accept(ByRef Response As Events) Handles MyBase.Accept

        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    'Tomo la Fecha actual del servidor para insertar
                    Me.dteFecha.DateTime = TINApp.FechaServidor
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmClientesJuridicoSeguimiento_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmClientesJuridicoSeguimiento_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        oClientesJuridicoSeguimiento = New VillarrealBusiness.clsClientesJuridicoSeguimiento
        Me.dteFecha.Enabled = False

        Select Case Action
            Case Actions.Insert
                Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmClientesJuridicoSeguimiento_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = oClientesJuridicoSeguimiento.Validacion(Action, Me.txtSeguimiento.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
