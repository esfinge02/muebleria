Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesJuridicoEmbargos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Folio_Juridico As Long
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblEmbargo As System.Windows.Forms.Label
    Friend WithEvents clcEmbargo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion_Mercancia As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion_Mercancia As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblValor_Estimado As System.Windows.Forms.Label
    Friend WithEvents clcValor_Estimado As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesJuridicoEmbargos))
        Me.lblEmbargo = New System.Windows.Forms.Label
        Me.clcEmbargo = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion_Mercancia = New System.Windows.Forms.Label
        Me.txtDescripcion_Mercancia = New DevExpress.XtraEditors.MemoEdit
        Me.lblValor_Estimado = New System.Windows.Forms.Label
        Me.clcValor_Estimado = New Dipros.Editors.TINCalcEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        CType(Me.clcEmbargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion_Mercancia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcValor_Estimado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(413, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblEmbargo
        '
        Me.lblEmbargo.AutoSize = True
        Me.lblEmbargo.Location = New System.Drawing.Point(99, 40)
        Me.lblEmbargo.Name = "lblEmbargo"
        Me.lblEmbargo.Size = New System.Drawing.Size(58, 16)
        Me.lblEmbargo.TabIndex = 0
        Me.lblEmbargo.Tag = ""
        Me.lblEmbargo.Text = "&Embargo:"
        Me.lblEmbargo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEmbargo
        '
        Me.clcEmbargo.EditValue = "0"
        Me.clcEmbargo.Location = New System.Drawing.Point(162, 40)
        Me.clcEmbargo.MaxValue = 0
        Me.clcEmbargo.MinValue = 0
        Me.clcEmbargo.Name = "clcEmbargo"
        '
        'clcEmbargo.Properties
        '
        Me.clcEmbargo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcEmbargo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEmbargo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcEmbargo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEmbargo.Properties.Enabled = False
        Me.clcEmbargo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEmbargo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEmbargo.Size = New System.Drawing.Size(72, 19)
        Me.clcEmbargo.TabIndex = 1
        Me.clcEmbargo.Tag = "embargo"
        '
        'lblDescripcion_Mercancia
        '
        Me.lblDescripcion_Mercancia.AutoSize = True
        Me.lblDescripcion_Mercancia.Location = New System.Drawing.Point(7, 63)
        Me.lblDescripcion_Mercancia.Name = "lblDescripcion_Mercancia"
        Me.lblDescripcion_Mercancia.Size = New System.Drawing.Size(150, 16)
        Me.lblDescripcion_Mercancia.TabIndex = 2
        Me.lblDescripcion_Mercancia.Tag = ""
        Me.lblDescripcion_Mercancia.Text = "&Descripci�n de mercanc�a:"
        Me.lblDescripcion_Mercancia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion_Mercancia
        '
        Me.txtDescripcion_Mercancia.EditValue = ""
        Me.txtDescripcion_Mercancia.Location = New System.Drawing.Point(162, 64)
        Me.txtDescripcion_Mercancia.Name = "txtDescripcion_Mercancia"
        Me.txtDescripcion_Mercancia.Size = New System.Drawing.Size(280, 49)
        Me.txtDescripcion_Mercancia.TabIndex = 3
        Me.txtDescripcion_Mercancia.Tag = "descripcion_mercancia"
        '
        'lblValor_Estimado
        '
        Me.lblValor_Estimado.AutoSize = True
        Me.lblValor_Estimado.Location = New System.Drawing.Point(65, 120)
        Me.lblValor_Estimado.Name = "lblValor_Estimado"
        Me.lblValor_Estimado.Size = New System.Drawing.Size(92, 16)
        Me.lblValor_Estimado.TabIndex = 4
        Me.lblValor_Estimado.Tag = ""
        Me.lblValor_Estimado.Text = "Va&lor Estimado:"
        Me.lblValor_Estimado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcValor_Estimado
        '
        Me.clcValor_Estimado.EditValue = "0"
        Me.clcValor_Estimado.Location = New System.Drawing.Point(162, 118)
        Me.clcValor_Estimado.MaxValue = 0
        Me.clcValor_Estimado.MinValue = 0
        Me.clcValor_Estimado.Name = "clcValor_Estimado"
        '
        'clcValor_Estimado.Properties
        '
        Me.clcValor_Estimado.Properties.DisplayFormat.FormatString = "c2"
        Me.clcValor_Estimado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValor_Estimado.Properties.EditFormat.FormatString = "n2"
        Me.clcValor_Estimado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValor_Estimado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcValor_Estimado.Properties.Precision = 2
        Me.clcValor_Estimado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcValor_Estimado.Size = New System.Drawing.Size(120, 19)
        Me.clcValor_Estimado.TabIndex = 5
        Me.clcValor_Estimado.Tag = "valor_estimado"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(113, 144)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 6
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "E"
        Me.cboStatus.Location = New System.Drawing.Point(162, 142)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Embargado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Devuelta al Cliente", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("A la Venta", "V", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(120, 20)
        Me.cboStatus.TabIndex = 7
        Me.cboStatus.Tag = "status"
        '
        'frmClientesJuridicoEmbargos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 176)
        Me.Controls.Add(Me.lblEmbargo)
        Me.Controls.Add(Me.clcEmbargo)
        Me.Controls.Add(Me.lblDescripcion_Mercancia)
        Me.Controls.Add(Me.txtDescripcion_Mercancia)
        Me.Controls.Add(Me.lblValor_Estimado)
        Me.Controls.Add(Me.clcValor_Estimado)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.cboStatus)
        Me.Name = "frmClientesJuridicoEmbargos"
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.clcValor_Estimado, 0)
        Me.Controls.SetChildIndex(Me.lblValor_Estimado, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion_Mercancia, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion_Mercancia, 0)
        Me.Controls.SetChildIndex(Me.clcEmbargo, 0)
        Me.Controls.SetChildIndex(Me.lblEmbargo, 0)
        CType(Me.clcEmbargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion_Mercancia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcValor_Estimado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientesJuridicoEmbargos As VillarrealBusiness.clsClientesJuridicoEmbargos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmClientesJuridicoEmbargos_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmClientesJuridicoEmbargos_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmClientesJuridicoEmbargos_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oClientesJuridicoEmbargos = New VillarrealBusiness.clsClientesJuridicoEmbargos

        Select Case Action
            Case Actions.Insert
                Me.cboStatus.Value = "E"
                Me.cboStatus.Enabled = False
            Case Actions.Update
                Me.cboStatus.Enabled = True
            Case Actions.Delete
                Me.cboStatus.Enabled = True
        End Select
    End Sub

    Private Sub frmClientesJuridicoEmbargos_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = oClientesJuridicoEmbargos.Validacion(Action, Me.txtDescripcion_Mercancia.Text, Me.clcValor_Estimado.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

    Private Sub lblValor_Estimado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblValor_Estimado.Click

    End Sub
    Private Sub lblEmbargo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblEmbargo.Click

    End Sub
    Private Sub lblStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblStatus.Click

    End Sub
End Class
