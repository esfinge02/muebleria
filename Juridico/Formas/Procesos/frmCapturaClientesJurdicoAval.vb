Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCapturaClientesJurdicoAval
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents pnlAval As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents clcA�os_aval As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtParentesco_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtEstado_aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAval As System.Windows.Forms.Label
    Friend WithEvents txtAval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio_Aval As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia_Aval As System.Windows.Forms.Label
    Friend WithEvents txtColonia_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono_Aval As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcCliente As DevExpress.XtraEditors.CalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCapturaClientesJurdicoAval))
        Me.pnlAval = New System.Windows.Forms.Panel
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.clcA�os_aval = New DevExpress.XtraEditors.CalcEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtParentesco_Aval = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtEstado_aval = New DevExpress.XtraEditors.TextEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtCiudad_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblAval = New System.Windows.Forms.Label
        Me.txtAval = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio_Aval = New System.Windows.Forms.Label
        Me.txtDomicilio_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia_Aval = New System.Windows.Forms.Label
        Me.txtColonia_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono_Aval = New System.Windows.Forms.Label
        Me.txtTelefono_Aval = New DevExpress.XtraEditors.TextEdit
        Me.clcCliente = New DevExpress.XtraEditors.CalcEdit
        Me.pnlAval.SuspendLayout()
        CType(Me.clcA�os_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1693, 28)
        '
        'pnlAval
        '
        Me.pnlAval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAval.Controls.Add(Me.clcCliente)
        Me.pnlAval.Controls.Add(Me.Label19)
        Me.pnlAval.Controls.Add(Me.Label18)
        Me.pnlAval.Controls.Add(Me.clcA�os_aval)
        Me.pnlAval.Controls.Add(Me.Label17)
        Me.pnlAval.Controls.Add(Me.txtParentesco_Aval)
        Me.pnlAval.Controls.Add(Me.Label16)
        Me.pnlAval.Controls.Add(Me.txtEstado_aval)
        Me.pnlAval.Controls.Add(Me.Label15)
        Me.pnlAval.Controls.Add(Me.txtCiudad_Aval)
        Me.pnlAval.Controls.Add(Me.lblAval)
        Me.pnlAval.Controls.Add(Me.txtAval)
        Me.pnlAval.Controls.Add(Me.lblDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.txtDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.lblColonia_Aval)
        Me.pnlAval.Controls.Add(Me.txtColonia_Aval)
        Me.pnlAval.Controls.Add(Me.lblTelefono_Aval)
        Me.pnlAval.Controls.Add(Me.txtTelefono_Aval)
        Me.pnlAval.Enabled = False
        Me.pnlAval.Location = New System.Drawing.Point(0, 30)
        Me.pnlAval.Name = "pnlAval"
        Me.pnlAval.Size = New System.Drawing.Size(704, 232)
        Me.pnlAval.TabIndex = 59
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(37, 19)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(48, 16)
        Me.Label19.TabIndex = 150
        Me.Label19.Tag = ""
        Me.Label19.Text = "Cuenta:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(299, 195)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(309, 16)
        Me.Label18.TabIndex = 14
        Me.Label18.Tag = ""
        Me.Label18.Text = "En caso de ser conocido, tiempo de conocerlo en a�os:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcA�os_aval
        '
        Me.clcA�os_aval.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcA�os_aval.Location = New System.Drawing.Point(614, 192)
        Me.clcA�os_aval.Name = "clcA�os_aval"
        Me.clcA�os_aval.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcA�os_aval.Size = New System.Drawing.Size(75, 20)
        Me.clcA�os_aval.TabIndex = 15
        Me.clcA�os_aval.Tag = "anios_aval"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(16, 169)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(69, 16)
        Me.Label17.TabIndex = 10
        Me.Label17.Tag = ""
        Me.Label17.Text = "Parentesco:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentesco_Aval
        '
        Me.txtParentesco_Aval.EditValue = ""
        Me.txtParentesco_Aval.Location = New System.Drawing.Point(89, 166)
        Me.txtParentesco_Aval.Name = "txtParentesco_Aval"
        '
        'txtParentesco_Aval.Properties
        '
        Me.txtParentesco_Aval.Properties.MaxLength = 50
        Me.txtParentesco_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtParentesco_Aval.TabIndex = 11
        Me.txtParentesco_Aval.Tag = "parentesco_aval"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(39, 144)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 16)
        Me.Label16.TabIndex = 8
        Me.Label16.Tag = ""
        Me.Label16.Text = "Estado:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado_aval
        '
        Me.txtEstado_aval.EditValue = ""
        Me.txtEstado_aval.Location = New System.Drawing.Point(89, 141)
        Me.txtEstado_aval.Name = "txtEstado_aval"
        '
        'txtEstado_aval.Properties
        '
        Me.txtEstado_aval.Properties.MaxLength = 50
        Me.txtEstado_aval.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado_aval.TabIndex = 9
        Me.txtEstado_aval.Tag = "estado_aval"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(38, 119)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(47, 16)
        Me.Label15.TabIndex = 6
        Me.Label15.Tag = ""
        Me.Label15.Text = "Ciudad:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad_Aval
        '
        Me.txtCiudad_Aval.EditValue = ""
        Me.txtCiudad_Aval.Location = New System.Drawing.Point(89, 116)
        Me.txtCiudad_Aval.Name = "txtCiudad_Aval"
        '
        'txtCiudad_Aval.Properties
        '
        Me.txtCiudad_Aval.Properties.MaxLength = 50
        Me.txtCiudad_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad_Aval.TabIndex = 7
        Me.txtCiudad_Aval.Tag = "ciudad_aval"
        '
        'lblAval
        '
        Me.lblAval.AutoSize = True
        Me.lblAval.Location = New System.Drawing.Point(32, 44)
        Me.lblAval.Name = "lblAval"
        Me.lblAval.Size = New System.Drawing.Size(53, 16)
        Me.lblAval.TabIndex = 0
        Me.lblAval.Tag = ""
        Me.lblAval.Text = "Nombre:"
        Me.lblAval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAval
        '
        Me.txtAval.EditValue = ""
        Me.txtAval.Location = New System.Drawing.Point(89, 41)
        Me.txtAval.Name = "txtAval"
        '
        'txtAval.Properties
        '
        Me.txtAval.Properties.MaxLength = 100
        Me.txtAval.Size = New System.Drawing.Size(600, 20)
        Me.txtAval.TabIndex = 1
        Me.txtAval.Tag = "aval"
        '
        'lblDomicilio_Aval
        '
        Me.lblDomicilio_Aval.AutoSize = True
        Me.lblDomicilio_Aval.Location = New System.Drawing.Point(26, 69)
        Me.lblDomicilio_Aval.Name = "lblDomicilio_Aval"
        Me.lblDomicilio_Aval.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio_Aval.TabIndex = 2
        Me.lblDomicilio_Aval.Tag = ""
        Me.lblDomicilio_Aval.Text = "Domici&lio:"
        Me.lblDomicilio_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio_Aval
        '
        Me.txtDomicilio_Aval.EditValue = ""
        Me.txtDomicilio_Aval.Location = New System.Drawing.Point(89, 66)
        Me.txtDomicilio_Aval.Name = "txtDomicilio_Aval"
        '
        'txtDomicilio_Aval.Properties
        '
        Me.txtDomicilio_Aval.Properties.MaxLength = 50
        Me.txtDomicilio_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilio_Aval.TabIndex = 3
        Me.txtDomicilio_Aval.Tag = "domicilio_aval"
        '
        'lblColonia_Aval
        '
        Me.lblColonia_Aval.AutoSize = True
        Me.lblColonia_Aval.Location = New System.Drawing.Point(35, 94)
        Me.lblColonia_Aval.Name = "lblColonia_Aval"
        Me.lblColonia_Aval.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia_Aval.TabIndex = 4
        Me.lblColonia_Aval.Tag = ""
        Me.lblColonia_Aval.Text = "Colonia:"
        Me.lblColonia_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia_Aval
        '
        Me.txtColonia_Aval.EditValue = ""
        Me.txtColonia_Aval.Location = New System.Drawing.Point(89, 91)
        Me.txtColonia_Aval.Name = "txtColonia_Aval"
        '
        'txtColonia_Aval.Properties
        '
        Me.txtColonia_Aval.Properties.MaxLength = 50
        Me.txtColonia_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia_Aval.TabIndex = 5
        Me.txtColonia_Aval.Tag = "colonia_aval"
        '
        'lblTelefono_Aval
        '
        Me.lblTelefono_Aval.AutoSize = True
        Me.lblTelefono_Aval.Location = New System.Drawing.Point(525, 69)
        Me.lblTelefono_Aval.Name = "lblTelefono_Aval"
        Me.lblTelefono_Aval.Size = New System.Drawing.Size(56, 16)
        Me.lblTelefono_Aval.TabIndex = 12
        Me.lblTelefono_Aval.Tag = ""
        Me.lblTelefono_Aval.Text = "Tel�fono:"
        Me.lblTelefono_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Aval
        '
        Me.txtTelefono_Aval.EditValue = ""
        Me.txtTelefono_Aval.Location = New System.Drawing.Point(585, 66)
        Me.txtTelefono_Aval.Name = "txtTelefono_Aval"
        '
        'txtTelefono_Aval.Properties
        '
        Me.txtTelefono_Aval.Properties.MaxLength = 13
        Me.txtTelefono_Aval.Size = New System.Drawing.Size(104, 20)
        Me.txtTelefono_Aval.TabIndex = 13
        Me.txtTelefono_Aval.Tag = "telefono_aval"
        '
        'clcCliente
        '
        Me.clcCliente.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCliente.Location = New System.Drawing.Point(88, 16)
        Me.clcCliente.Name = "clcCliente"
        Me.clcCliente.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcCliente.Size = New System.Drawing.Size(120, 20)
        Me.clcCliente.TabIndex = 151
        Me.clcCliente.Tag = "cliente"
        '
        'frmCapturaClientesJurdicoAval
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(706, 264)
        Me.Controls.Add(Me.pnlAval)
        Me.Name = "frmCapturaClientesJurdicoAval"
        Me.Text = "Captura de Aval de Clientes en Jur�dico"
        Me.Controls.SetChildIndex(Me.pnlAval, 0)
        Me.pnlAval.ResumeLayout(False)
        CType(Me.clcA�os_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oClientes As New VillarrealBusiness.clsClientes

    'Public lvalor As Long
    'Public bsucursal_dependencia As Boolean

    'Public WriteOnly Property sucursal_dependencia() As Boolean
    '    Set(ByVal Value As Boolean)
    '        bsucursal_dependencia = Value
    '    End Set
    'End Property

    Public Property ClienteAval() As Long
        Get
            Return Me.clcCliente.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCliente.Value = Value
        End Set
    End Property
    Public WriteOnly Property NombreAval() As String
        Set(ByVal Value As String)
            Me.txtAval.Text = Value
        End Set
    End Property
    Public WriteOnly Property DomicilioAval() As String
        Set(ByVal Value As String)
            Me.txtDomicilio_Aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property ColoniaAval() As String
        Set(ByVal Value As String)
            Me.txtColonia_Aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property TelefonoAval() As String
        Set(ByVal Value As String)
            Me.txtTelefono_Aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property CiudadAval() As String
        Set(ByVal Value As String)
            Me.txtCiudad_Aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property EstadoAval() As String
        Set(ByVal Value As String)
            Me.txtEstado_aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property ParentescoAval() As String
        Set(ByVal Value As String)
            Me.txtParentesco_Aval.Text = Value
        End Set
    End Property
    Public WriteOnly Property AniosAval() As Long
        Set(ByVal Value As Long)
            Me.clcA�os_aval.Value = Value
        End Set
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCapturaClientesJurdicoAval_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(0).Visible = False
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    'Private Sub lkpClienteAval_Format()
    '    Comunes.clsFormato.for_clientes_grl(Me.lkpClienteAval)
    'End Sub
    'Public Sub lkpClienteAval_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    'Response = oClientes.LookupRepartoClienteExcluido(bsucursal_dependencia)
    '    Response = oClientes.LookupCliente()
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpClienteAval.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If

    '    Response = Nothing

    'End Sub
    'Private Sub lkpClienteAval_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    '    AsignarCliente(Me.ClienteAval)
    'End Sub

#End Region

    '#Region "DIPROS Systems, Funcionalidad"

    '    Private Sub AsignarCliente(ByVal Cliente_Aval As Long)
    '        If Cliente_Aval = -1 Then
    '            LimpiaFormulario()
    '        Else
    '            LlenaFormulario(Cliente_Aval)
    '        End If
    '    End Sub

    '    Private Sub LimpiaFormulario()
    '        ' ======================================
    '        ' Funci�n para limpiar los controles del formulario
    '        ' ======================================

    '        Me.txtAval.Enabled = True
    '        Me.txtDomicilio_Aval.Enabled = True
    '        Me.txtColonia_Aval.Enabled = True
    '        Me.txtCiudad_Aval.Enabled = True
    '        Me.txtEstado_aval.Enabled = True
    '        Me.txtTelefono_Aval.Enabled = True

    '        Me.txtAval.Text = ""
    '        Me.txtDomicilio_Aval.Text = ""
    '        Me.txtColonia_Aval.Text = ""
    '        Me.txtCiudad_Aval.Text = ""
    '        Me.txtEstado_aval.Text = ""
    '        Me.txtParentesco_Aval.Text = ""
    '        Me.txtTelefono_Aval.Text = ""
    '        Me.clcA�os_aval.Value = 0

    '    End Sub
    '    Private Sub LlenaFormulario(ByVal lkp As Dipros.Editors.TINMultiLookup)
    '        ' ======================================
    '        ' Funci�n para llenar los controles del formulario
    '        ' ======================================

    '        Me.txtAval.Enabled = False
    '        Me.txtDomicilio_Aval.Enabled = False
    '        Me.txtColonia_Aval.Enabled = False
    '        Me.txtCiudad_Aval.Enabled = False
    '        Me.txtEstado_aval.Enabled = False
    '        Me.txtTelefono_Aval.Enabled = False

    '        Me.txtAval.Text = lkp.GetValue("nombre")
    '        Me.txtDomicilio_Aval.Text = lkp.GetValue("direccion_aval_resultado")
    '        Me.txtColonia_Aval.Text = lkp.GetValue("nombre_colonia")
    '        Me.txtCiudad_Aval.Text = lkp.GetValue("ciudad")
    '        Me.txtEstado_aval.Text = lkp.GetValue("nombre_estado")
    '        Me.txtTelefono_Aval.Text = lkp.GetValue("telefono_aval")
    '        Me.clcA�os_aval.Value = lkp.GetValue("anios_aval")
    '        Me.txtParentesco_Aval.Text = lkp.GetValue("parentesco_aval")
    '    End Sub

    '#End Region

End Class
