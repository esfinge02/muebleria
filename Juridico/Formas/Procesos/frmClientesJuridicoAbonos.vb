Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesJuridicoAbonos
    Inherits Dipros.Windows.frmTINForm

    Public lvalor As Long

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grvAbonos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCaja As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCajero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCobrador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSubtotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImpuesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaCancelacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAbogado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreAbogado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcComisionAbogado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grAbonos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesJuridicoAbonos))
        Me.grAbonos = New DevExpress.XtraGrid.GridControl
        Me.grvAbonos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCaja = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCajero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCobrador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSubtotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImpuesto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaCancelacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEstatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAbogado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreAbogado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcComisionAbogado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.grAbonos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvAbonos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2078, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grAbonos
        '
        '
        'grAbonos.EmbeddedNavigator
        '
        Me.grAbonos.EmbeddedNavigator.Name = ""
        Me.grAbonos.Location = New System.Drawing.Point(16, 56)
        Me.grAbonos.MainView = Me.grvAbonos
        Me.grAbonos.Name = "grAbonos"
        Me.grAbonos.Size = New System.Drawing.Size(688, 312)
        Me.grAbonos.TabIndex = 59
        Me.grAbonos.Text = "grAbonos"
        '
        'grvAbonos
        '
        Me.grvAbonos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcNombreCliente, Me.grcPartida, Me.grcFecha, Me.grcCaja, Me.grcCajero, Me.grcCobrador, Me.grcDocumentos, Me.grcCargo, Me.grcAbono, Me.grcSubtotal, Me.grcImpuesto, Me.grcTotal, Me.grcSaldo, Me.grcFechaCancelacion, Me.grcEstatus, Me.grcAbogado, Me.grcNombreAbogado, Me.grcComisionAbogado, Me.grcNombreSucursal})
        Me.grvAbonos.GridControl = Me.grAbonos
        Me.grvAbonos.GroupPanelText = "Arrastre un encabezado aqui para agrupar por esa columna"
        Me.grvAbonos.Name = "grvAbonos"
        Me.grvAbonos.OptionsView.ShowGroupPanel = False
        Me.grvAbonos.OptionsView.ShowIndicator = False
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.VisibleIndex = 1
        Me.grcConcepto.Width = 73
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.VisibleIndex = 2
        Me.grcSerie.Width = 73
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "F�lio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.VisibleIndex = 3
        Me.grcFolio.Width = 81
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Nombre Cliente"
        Me.grcNombreCliente.FieldName = "nombre_cliente"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.VisibleIndex = 4
        Me.grcFecha.Width = 113
        '
        'grcCaja
        '
        Me.grcCaja.Caption = "Caja"
        Me.grcCaja.FieldName = "caja"
        Me.grcCaja.Name = "grcCaja"
        '
        'grcCajero
        '
        Me.grcCajero.Caption = "Cajero"
        Me.grcCajero.FieldName = "cajero"
        Me.grcCajero.Name = "grcCajero"
        '
        'grcCobrador
        '
        Me.grcCobrador.Caption = "Cobrador"
        Me.grcCobrador.FieldName = "cobrador"
        Me.grcCobrador.Name = "grcCobrador"
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documentos"
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        '
        'grcCargo
        '
        Me.grcCargo.Caption = "Cargo"
        Me.grcCargo.FieldName = "cargo"
        Me.grcCargo.Name = "grcCargo"
        '
        'grcAbono
        '
        Me.grcAbono.Caption = "Abono"
        Me.grcAbono.FieldName = "Abono"
        Me.grcAbono.Name = "grcAbono"
        '
        'grcSubtotal
        '
        Me.grcSubtotal.Caption = "Subtotal"
        Me.grcSubtotal.FieldName = "subtotal"
        Me.grcSubtotal.Name = "grcSubtotal"
        '
        'grcImpuesto
        '
        Me.grcImpuesto.Caption = "Impuesto"
        Me.grcImpuesto.FieldName = "impuesto"
        Me.grcImpuesto.Name = "grcImpuesto"
        '
        'grcTotal
        '
        Me.grcTotal.Caption = "Total"
        Me.grcTotal.DisplayFormat.FormatString = "c2"
        Me.grcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTotal.FieldName = "total"
        Me.grcTotal.Name = "grcTotal"
        Me.grcTotal.VisibleIndex = 5
        Me.grcTotal.Width = 113
        '
        'grcSaldo
        '
        Me.grcSaldo.Caption = "Saldo"
        Me.grcSaldo.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.grcSaldo.FieldName = "saldo"
        Me.grcSaldo.Name = "grcSaldo"
        Me.grcSaldo.Width = 91
        '
        'grcFechaCancelacion
        '
        Me.grcFechaCancelacion.Caption = "Fecha Cancelacion"
        Me.grcFechaCancelacion.FieldName = "fecha_cancelacion"
        Me.grcFechaCancelacion.Name = "grcFechaCancelacion"
        '
        'grcEstatus
        '
        Me.grcEstatus.Caption = "Estatus"
        Me.grcEstatus.Name = "grcEstatus"
        '
        'grcAbogado
        '
        Me.grcAbogado.Caption = "Abogado"
        Me.grcAbogado.FieldName = "abogado"
        Me.grcAbogado.Name = "grcAbogado"
        '
        'grcNombreAbogado
        '
        Me.grcNombreAbogado.Caption = "Nombre Abogado"
        Me.grcNombreAbogado.FieldName = "nombre_abogado"
        Me.grcNombreAbogado.Name = "grcNombreAbogado"
        '
        'grcComisionAbogado
        '
        Me.grcComisionAbogado.Caption = "Comisi�n Abogado"
        Me.grcComisionAbogado.DisplayFormat.FormatString = "c2"
        Me.grcComisionAbogado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcComisionAbogado.FieldName = "comision_abogado"
        Me.grcComisionAbogado.Name = "grcComisionAbogado"
        Me.grcComisionAbogado.VisibleIndex = 6
        Me.grcComisionAbogado.Width = 117
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.VisibleIndex = 0
        Me.grcNombreSucursal.Width = 114
        '
        'frmClientesJuridicoAbonos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(722, 383)
        Me.Controls.Add(Me.grAbonos)
        Me.Name = "frmClientesJuridicoAbonos"
        Me.Text = "Abonos de Cliente en Jur�dico"
        Me.Controls.SetChildIndex(Me.grAbonos, 0)
        CType(Me.grAbonos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvAbonos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientesJuridico As VillarrealBusiness.clsClientesJuridico
    Private oAbogados As New VillarrealBusiness.clsAbogados
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes
    Private oUtilerias As VillarrealBusiness.clsUtilerias
    'Private oCapturaClientesjURIDICO as VillarrealBusiness.clscap

    Public Cliente As Long
    Public Abogado As Long
    Private oTabla As DataTable
    'Private oClientesJuridicoSeguimiento As VillarrealBusiness.clsClientesJuridicoSeguimiento
    'Private oClientesJuridicoGastosCostas As VillarrealBusiness.clsClientesJuridicoGastosCostas
    'Private oClientesJuridicoEmbargos As VillarrealBusiness.clsClientesJuridicoEmbargos

    'Private bsucursal_dependencia As Boolean = False
    'Public lClienteAval As Long
    'Private Property Abogado() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAbogado)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Me.lkpAbogado.EditValue = Value
    '    End Set
    'End Property
    'Private ReadOnly Property Cliente() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
    '    End Get
    'End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmClientesJuridicoAbonos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'Select Case Action
        '    Case Actions.Insert
        '        Response = oClientesJuridico.Insertar(Me.clcFolio_Juridico.EditValue, Abogado, Cliente, Me.clcSaldoAsignar.Value, Me.dteFecha_Asignacion.DateTime, Me.cboStatus.Value, Me.chkNo_Recibir_Abonos.Checked, Me.txtobservaciones.Text, Me.chkClienteReformado.EditValue)
        '    Case Actions.Update
        '        Response = oClientesJuridico.Actualizar(Me.clcFolio_Juridico.EditValue, Abogado, Cliente, Me.clcSaldoAsignar.Value, Me.dteFecha_Asignacion.DateTime, Me.cboStatus.Value, Me.chkNo_Recibir_Abonos.Checked, Me.txtobservaciones.Text, Me.chkClienteReformado.EditValue)
        '    Case Actions.Delete
        '        Response = oClientesJuridico.Eliminar(clcFolio_Juridico.Value)
        'End Select
    End Sub
    Private Sub frmClientesJuridicoAbonos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
    End Sub
    Private Sub frmClientesJuridicoAbonos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientesJuridico = New VillarrealBusiness.clsClientesJuridico
        oAbogados = New VillarrealBusiness.clsAbogados
        oClientes = New VillarrealBusiness.clsClientes
        oLlamadasClientes = New VillarrealBusiness.clsLlamadasClientes
        oUtilerias = New VillarrealBusiness.clsUtilerias

        LlenarGrid()

        'Select Case Action
        '    Case Actions.Insert
        '        Me.Abogado = CType(OwnerForm, brwClientesJuridico).Abogado
        '        Me.lkpAbogado.Enabled = False
        '    Case Actions.Update
        '        Me.Abogado = OwnerForm.Value("abogado")
        '    Case Actions.Delete
        'End Select

    End Sub
    Private Sub frmClientesJuridicoAbonos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ' Response = oClientesJuridico.Validacion(Action, Abogado, Cliente)
    End Sub
    Private Sub frmClientesJuridicoAbonos_Localize() Handles MyBase.Localize
        '    Find("folio_juridico", Me.clcFolio_Juridico.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LlenarGrid()

        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet

        response = oUtilerias.AbonosClientesJuridico(Me.Abogado, Me.Cliente)
        If Not response.ErrorFound Then
            odataset = response.Value
            oTabla = odataset.Tables(0)
            Me.grAbonos.DataSource = oTabla

        End If

        odataset = Nothing

    End Sub

#End Region


    
End Class

