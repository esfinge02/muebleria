Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmAsignacionClientesJuridico
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    Friend WithEvents grClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoActual As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoVencido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaUltimoAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTraerClientes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAsignarAJuridico As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcNombreEmpresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDireccionEmpresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTelefonosEmpresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcLogotipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaActual As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAsignacionClientesJuridico))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.grClientes = New DevExpress.XtraGrid.GridControl
        Me.grvClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoActual = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoVencido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaUltimoAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreEmpresa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDireccionEmpresa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTelefonosEmpresa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcLogotipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaActual = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.btnImprimir = New DevExpress.XtraEditors.SimpleButton
        Me.btnTraerClientes = New DevExpress.XtraEditors.SimpleButton
        Me.btnAsignarAJuridico = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2703, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(36, 51)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 48)
        Me.lkpSucursal.MultiSelect = True
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(336, 20)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(96, 72)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(104, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        Me.dteFecha.ToolTip = "Fecha"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(51, 75)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "&Fecha:"
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(35, 99)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 4
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "A&bogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = False
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(96, 96)
        Me.lkpAbogado.MultiSelect = False
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = ""
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = False
        Me.lkpAbogado.Size = New System.Drawing.Size(336, 20)
        Me.lkpAbogado.TabIndex = 5
        Me.lkpAbogado.Tag = "Abogado"
        Me.lkpAbogado.ToolTip = "abogado"
        Me.lkpAbogado.ValueMember = "Abogado"
        '
        'grClientes
        '
        '
        'grClientes.EmbeddedNavigator
        '
        Me.grClientes.EmbeddedNavigator.Name = ""
        Me.grClientes.Location = New System.Drawing.Point(8, 128)
        Me.grClientes.MainView = Me.grvClientes
        Me.grClientes.Name = "grClientes"
        Me.grClientes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.chkSeleccionar})
        Me.grClientes.Size = New System.Drawing.Size(776, 376)
        Me.grClientes.TabIndex = 6
        Me.grClientes.Text = "grClientes"
        '
        'grvClientes
        '
        Me.grvClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcCliente, Me.grcNombre, Me.grcSaldoActual, Me.grcSaldoVencido, Me.grcFechaUltimoAbono, Me.grcImporteAbono, Me.grcNombreEmpresa, Me.grcDireccionEmpresa, Me.grcTelefonosEmpresa, Me.grcLogotipo, Me.grcFechaActual})
        Me.grvClientes.GridControl = Me.grClientes
        Me.grvClientes.Name = "grvClientes"
        Me.grvClientes.OptionsView.ShowGroupPanel = False
        Me.grvClientes.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 89
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 1
        Me.grcCliente.Width = 69
        '
        'grcNombre
        '
        Me.grcNombre.Caption = "Nombre"
        Me.grcNombre.FieldName = "nombre"
        Me.grcNombre.Name = "grcNombre"
        Me.grcNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombre.VisibleIndex = 2
        Me.grcNombre.Width = 195
        '
        'grcSaldoActual
        '
        Me.grcSaldoActual.Caption = "Saldo Actual"
        Me.grcSaldoActual.DisplayFormat.FormatString = "c2"
        Me.grcSaldoActual.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoActual.FieldName = "saldo_actual"
        Me.grcSaldoActual.Name = "grcSaldoActual"
        Me.grcSaldoActual.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoActual.VisibleIndex = 3
        Me.grcSaldoActual.Width = 95
        '
        'grcSaldoVencido
        '
        Me.grcSaldoVencido.Caption = "Saldo Vencido"
        Me.grcSaldoVencido.DisplayFormat.FormatString = "c2"
        Me.grcSaldoVencido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoVencido.FieldName = "saldo_vencido"
        Me.grcSaldoVencido.Name = "grcSaldoVencido"
        Me.grcSaldoVencido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoVencido.VisibleIndex = 4
        Me.grcSaldoVencido.Width = 95
        '
        'grcFechaUltimoAbono
        '
        Me.grcFechaUltimoAbono.Caption = "Fecha �ltimo Abono"
        Me.grcFechaUltimoAbono.FieldName = "fecha_ultimo_abono"
        Me.grcFechaUltimoAbono.Name = "grcFechaUltimoAbono"
        Me.grcFechaUltimoAbono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaUltimoAbono.VisibleIndex = 5
        Me.grcFechaUltimoAbono.Width = 120
        '
        'grcImporteAbono
        '
        Me.grcImporteAbono.Caption = "Importe Abono"
        Me.grcImporteAbono.DisplayFormat.FormatString = "c2"
        Me.grcImporteAbono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteAbono.FieldName = "importe_abono"
        Me.grcImporteAbono.Name = "grcImporteAbono"
        Me.grcImporteAbono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteAbono.VisibleIndex = 6
        Me.grcImporteAbono.Width = 95
        '
        'grcNombreEmpresa
        '
        Me.grcNombreEmpresa.Caption = "Nombre Empresa"
        Me.grcNombreEmpresa.FieldName = "nombre_empresa"
        Me.grcNombreEmpresa.Name = "grcNombreEmpresa"
        '
        'grcDireccionEmpresa
        '
        Me.grcDireccionEmpresa.Caption = "Direccion Empresa"
        Me.grcDireccionEmpresa.FieldName = "direccion_empresa"
        Me.grcDireccionEmpresa.Name = "grcDireccionEmpresa"
        '
        'grcTelefonosEmpresa
        '
        Me.grcTelefonosEmpresa.Caption = "Telefonos Empresa"
        Me.grcTelefonosEmpresa.FieldName = "telefonos_empresa"
        Me.grcTelefonosEmpresa.Name = "grcTelefonosEmpresa"
        '
        'grcLogotipo
        '
        Me.grcLogotipo.Caption = "Logotipo"
        Me.grcLogotipo.FieldName = "logotipo"
        Me.grcLogotipo.Name = "grcLogotipo"
        '
        'grcFechaActual
        '
        Me.grcFechaActual.Caption = "Fecha Actual"
        Me.grcFechaActual.FieldName = "fecha_actual"
        Me.grcFechaActual.Name = "grcFechaActual"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(192, 528)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(107, 23)
        Me.btnImprimir.TabIndex = 7
        Me.btnImprimir.Text = "&Imprimir"
        '
        'btnTraerClientes
        '
        Me.btnTraerClientes.Location = New System.Drawing.Point(344, 528)
        Me.btnTraerClientes.Name = "btnTraerClientes"
        Me.btnTraerClientes.Size = New System.Drawing.Size(107, 23)
        Me.btnTraerClientes.TabIndex = 8
        Me.btnTraerClientes.Text = "&Traer Clientes"
        '
        'btnAsignarAJuridico
        '
        Me.btnAsignarAJuridico.Location = New System.Drawing.Point(496, 528)
        Me.btnAsignarAJuridico.Name = "btnAsignarAJuridico"
        Me.btnAsignarAJuridico.Size = New System.Drawing.Size(107, 23)
        Me.btnAsignarAJuridico.TabIndex = 9
        Me.btnAsignarAJuridico.Text = "Asignar a &Jur�dico"
        '
        'frmAsignacionClientesJuridico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(794, 575)
        Me.Controls.Add(Me.grClientes)
        Me.Controls.Add(Me.btnAsignarAJuridico)
        Me.Controls.Add(Me.btnTraerClientes)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.lblAbogado)
        Me.Controls.Add(Me.lkpAbogado)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Name = "frmAsignacionClientesJuridico"
        Me.Text = "Asignaci�n de Clientes a Jur�dico"
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.Controls.SetChildIndex(Me.btnImprimir, 0)
        Me.Controls.SetChildIndex(Me.btnTraerClientes, 0)
        Me.Controls.SetChildIndex(Me.btnAsignarAJuridico, 0)
        Me.Controls.SetChildIndex(Me.grClientes, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oAbogados As New VillarrealBusiness.clsAbogados
    Private oUtilerias As New Comunes.clsUtilerias
    Private oClientesJuridico As VillarrealBusiness.clsClientesJuridico
    Dim oTabla As DataTable

    Private ReadOnly Property Sucursal() As String
        Get
            Return Me.lkpSucursal.ToXML(Dipros.Editors.Encoding.ISO8859)
        End Get
    End Property
    Private Property Abogado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAbogado)
        End Get
        Set(ByVal Value As Long)
            Me.lkpAbogado.EditValue = Value
        End Set
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepDocumentosVencer_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If ValidarClientesAJuridico() = True Then
            '    Exit Sub
            'Else
            '    AsignarClientesAJuridico()
        End If

        '05-Nov-09 Se ajusto para que asigne los clientes aun con el mensaje de validacion, 
        'ya que antes si encontraba al menos un cliente se salia de la peticion de asignar
        AsignarClientesAJuridico()


        ' =============================================================


        'Response = oReportes.DocumentosVencer(Me.Sucursal, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.cboTipoVenta.EditValue, Me.Cobrador)

        'If Response.ErrorFound Then
        '    ShowMessage(MessageType.MsgInformation, "El Reporte de Documentos por Vencer no se puede Mostrar")
        'Else
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value

        '    If oDataSet.Tables(0).Rows.Count > 0 Then
        '        Dim oReport As New rptDocumentosVencer
        '        oReport.DataSource = oDataSet.Tables(0)
        '        TINApp.ShowReport(Me.MdiParent, "Documentos por Vencer", oReport)
        '        oReport = Nothing
        '    Else
        '        ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
        '    End If

        '    oDataSet = Nothing
        'End If




    End Sub

    Private Sub frmRepDocumentosVencer_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        Response = Me.oClientesJuridico.Validacion(Action, Me.dteFecha.Text, Abogado)

    End Sub

    Private Sub frmRepDocumentosVencer_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oSucursales = New VillarrealBusiness.clsSucursales
        oAbogados = New VillarrealBusiness.clsAbogados
        oClientesJuridico = New VillarrealBusiness.clsClientesJuridico
        oUtilerias = New Comunes.clsUtilerias

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"

#Region "Lookups"
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim Response As New Events
        Response = oAbogados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
#End Region

#Region "Botones"

    Private Sub btnTraerClientes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTraerClientes.Click
        LlenarGrid()
    End Sub
    Private Sub btnAsignarAJuridico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAsignarAJuridico.Click

        If ValidarClientesAJuridico() = True Then
            Exit Sub
        Else
            AsignarClientesAJuridico()
        End If
        ' LlenarGrid()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        ImprimirPrevio()
    End Sub

#End Region


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LlenarGrid()

        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet
        Try
            response = oClientesJuridico.ClientesParaAsignarAJuridico(Me.Sucursal)
        If Not response.ErrorFound Then
            odataset = response.Value
            oTabla = odataset.Tables(0)
                Me.grClientes.DataSource = oTabla
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, ex.Message)
        End Try
        odataset = Nothing


    End Sub
    Private Sub AsignarClientesAJuridico()

        Dim response As New Dipros.Utils.Events
     
        Dim i As Long = 0
        For i = 0 To Me.oTabla.Rows.Count - 1
            If Me.oTabla.Rows(i).Item("Seleccionar") Then
                response = Me.oClientesJuridico.Asignar(Me.lkpAbogado.EditValue, Me.grvClientes.GetRowCellValue(i, Me.grcCliente), Me.grvClientes.GetRowCellValue(i, Me.grcSaldoActual), Me.dteFecha.EditValue, "AS", False, "Asignado desde el m�dulo de asignaci�n", False)
            End If
        Next

        ImprimirAsignados()

    End Sub
    Private Function ValidarClientesAJuridico() As Boolean

        Dim response As New Dipros.Utils.Events
        Dim bandera As Boolean = False

        Dim i As Long = 0
        For i = 0 To Me.oTabla.Rows.Count - 1
            If CType(Me.oTabla.Rows(i).Item("Seleccionar"), Boolean) = True Then
                If ValidarCliente(CType(Me.grvClientes.GetRowCellValue(i, Me.grcCliente), Integer)) Then
                    ShowMessage(MessageType.MsgInformation, "El cliente " + CType(Me.grvClientes.GetRowCellValue(i, Me.grcCliente), String) + " no se puede asignar,tiene notas de cargo pendientes de generar")
                    bandera = True
                End If
            End If
        Next
        Return bandera
        'ImprimirAsignados()

    End Function
    Private Function ValidarCliente(ByVal cliente As Integer) As Boolean
        Dim response As Dipros.Utils.Events
        Try
            response = oClientesJuridico.NotasDeCargoPendientesClienteExiste(cliente)
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

        If CType(CType(CType((oClientesJuridico.NotasDeCargoPendientesClienteExiste(cliente)).Value.tables(0).rows(0), Object), System.Data.DataRow).ItemArray(0), Integer) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub ImprimirPrevio()
        Dim odatatable As DataTable

        odatatable = CType(Me.grvClientes.DataSource, DataView).DataViewManager.DataSet.Tables(0)

        If odatatable.Rows.Count > 0 Then

            Dim oReport As New rptAsignacionClientesJuridico
            oReport.DataSource = odatatable
            'oReport.AddCode("titulo ='Reporte de Posibles clientes a Juridico'  ")    '= "Reporte de Posibles clientes a Juridico"
            'oReport.Run(True)
            'oReport.DataSource.titulo = "Reporte de Posibles clientes a Juridico"
            TINApp.ShowReport(Me.MdiParent, "Abonos de Clientes en Jur�dico", oReport, , , , False)
            oReport = Nothing

        Else
            ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
        End If

    End Sub
    Private Sub ImprimirAsignados()

        'Dim oEvent As Dipros.Utils.Events
        'Dim oDataTable As DataRow
        'Try

        '    oDataTable = CType(Me.grvClientes.DataSource, DataRow).Select("Seleccionar = TRUE")

        '    Dim oReport As New rptAsignacionClientesJuridico

        '    oReport.DataSource = oDataTable


        '    TINApp.ShowReport(Me.MdiParent, "Reporte de Clientes Asignados a Juridico", oReport)

        'Catch ex As Exception
        '    ShowMessage(MessageType.MsgError, ex.ToString, )
        '    oEvent.Message = ex.ToString
        'End Try

    End Sub

#End Region


End Class
