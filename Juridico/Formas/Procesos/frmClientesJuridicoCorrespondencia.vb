Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesJuridicoCorrespondencia
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Folio_Juridico As Long
    Dim Folio_Juridico_Correspondencia As Long
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblDescripcion_Correspondencia As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion_Correspondencia As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpCorrespondencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents rchContenido As Comunes.TINRichTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcFolioCorrespondencia As DevExpress.XtraEditors.CalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesJuridicoCorrespondencia))
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblDescripcion_Correspondencia = New System.Windows.Forms.Label
        Me.txtDescripcion_Correspondencia = New DevExpress.XtraEditors.MemoEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpCorrespondencia = New Dipros.Editors.TINMultiLookup
        Me.rchContenido = New Comunes.TINRichTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcFolioCorrespondencia = New DevExpress.XtraEditors.CalcEdit
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion_Correspondencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5143, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(135, 62)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "18/01/2007"
        Me.dteFecha.Location = New System.Drawing.Point(184, 62)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 1
        Me.dteFecha.Tag = "fecha"
        '
        'lblDescripcion_Correspondencia
        '
        Me.lblDescripcion_Correspondencia.AutoSize = True
        Me.lblDescripcion_Correspondencia.Location = New System.Drawing.Point(8, 110)
        Me.lblDescripcion_Correspondencia.Name = "lblDescripcion_Correspondencia"
        Me.lblDescripcion_Correspondencia.Size = New System.Drawing.Size(168, 16)
        Me.lblDescripcion_Correspondencia.TabIndex = 4
        Me.lblDescripcion_Correspondencia.Tag = ""
        Me.lblDescripcion_Correspondencia.Text = "&Descripci�n Correspondencia:"
        Me.lblDescripcion_Correspondencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion_Correspondencia
        '
        Me.txtDescripcion_Correspondencia.EditValue = ""
        Me.txtDescripcion_Correspondencia.Location = New System.Drawing.Point(192, 168)
        Me.txtDescripcion_Correspondencia.Name = "txtDescripcion_Correspondencia"
        '
        'txtDescripcion_Correspondencia.Properties
        '
        Me.txtDescripcion_Correspondencia.Properties.Enabled = False
        Me.txtDescripcion_Correspondencia.Size = New System.Drawing.Size(160, 152)
        Me.txtDescripcion_Correspondencia.TabIndex = 5
        Me.txtDescripcion_Correspondencia.TabStop = False
        Me.txtDescripcion_Correspondencia.Tag = "descripcion_correspondencia"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(89, 398)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 6
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(184, 397)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(592, 38)
        Me.txtObservaciones.TabIndex = 7
        Me.txtObservaciones.Tag = "observaciones"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(75, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Corr&espondencia:"
        '
        'lkpCorrespondencia
        '
        Me.lkpCorrespondencia.AllowAdd = False
        Me.lkpCorrespondencia.AutoReaload = False
        Me.lkpCorrespondencia.DataSource = Nothing
        Me.lkpCorrespondencia.DefaultSearchField = ""
        Me.lkpCorrespondencia.DisplayMember = "descripcion"
        Me.lkpCorrespondencia.EditValue = Nothing
        Me.lkpCorrespondencia.Filtered = False
        Me.lkpCorrespondencia.InitValue = Nothing
        Me.lkpCorrespondencia.Location = New System.Drawing.Point(184, 87)
        Me.lkpCorrespondencia.MultiSelect = False
        Me.lkpCorrespondencia.Name = "lkpCorrespondencia"
        Me.lkpCorrespondencia.NullText = ""
        Me.lkpCorrespondencia.PopupWidth = CType(480, Long)
        Me.lkpCorrespondencia.Required = False
        Me.lkpCorrespondencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCorrespondencia.SearchMember = ""
        Me.lkpCorrespondencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCorrespondencia.SelectAll = False
        Me.lkpCorrespondencia.Size = New System.Drawing.Size(394, 20)
        Me.lkpCorrespondencia.TabIndex = 3
        Me.lkpCorrespondencia.Tag = "correspondencia"
        Me.lkpCorrespondencia.ToolTip = "correspondencia"
        Me.lkpCorrespondencia.ValueMember = "correspondencia"
        '
        'rchContenido
        '
        Me.rchContenido.Location = New System.Drawing.Point(184, 112)
        Me.rchContenido.Name = "rchContenido"
        Me.rchContenido.Size = New System.Drawing.Size(592, 280)
        Me.rchContenido.TabIndex = 59
        Me.rchContenido.TextRtf = "{\rtf1\ansi\ansicpg1252\deff0\deflang2058{\fonttbl{\f0\fnil\fcharset0 Times New R" & _
        "oman;}}" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "{\*\generator Riched20 5.40.11.2210;}\viewkind4\uc1\pard\f0\fs24\par" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "}" & _
        "" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(0)
        Me.rchContenido.TextSimple = ""
        Me.rchContenido.VisibleToolbar = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(131, 16)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "Folio Correspondenc&ia:"
        '
        'clcFolioCorrespondencia
        '
        Me.clcFolioCorrespondencia.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioCorrespondencia.Location = New System.Drawing.Point(184, 37)
        Me.clcFolioCorrespondencia.Name = "clcFolioCorrespondencia"
        '
        'clcFolioCorrespondencia.Properties
        '
        Me.clcFolioCorrespondencia.Properties.Enabled = False
        Me.clcFolioCorrespondencia.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioCorrespondencia.Size = New System.Drawing.Size(95, 20)
        Me.clcFolioCorrespondencia.TabIndex = 61
        Me.clcFolioCorrespondencia.Tag = "folio_juridico_correspondencia"
        '
        'frmClientesJuridicoCorrespondencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(786, 448)
        Me.Controls.Add(Me.clcFolioCorrespondencia)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.rchContenido)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpCorrespondencia)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblDescripcion_Correspondencia)
        Me.Controls.Add(Me.txtDescripcion_Correspondencia)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Name = "frmClientesJuridicoCorrespondencia"
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion_Correspondencia, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion_Correspondencia, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpCorrespondencia, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.rchContenido, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcFolioCorrespondencia, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion_Correspondencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCorrespondenciaClientes As VillarrealBusiness.clsCorrespondenciaClientes
    Private oCorrespondenciaClientesdetalle As VillarrealBusiness.clsCorrespondenciaClientesDetalle
    Private oCamposCorrespondecia As VillarrealBusiness.clsCamposCorrespondencia

    Public Property Correspondencia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCorrespondencia)
        End Get
        Set(ByVal Value As Long)
            Me.lkpCorrespondencia.EditValue = Value
        End Set
    End Property

    Private sTexto_correspondencia As String

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmClientesJuridicoCorrespondencia_Accept(ByRef Response As Events) Handles MyBase.Accept
        Me.txtDescripcion_Correspondencia.Text = Me.rchContenido.TextRtf
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub
    Private Sub frmClientesJuridicoCorrespondencia_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.rchContenido.TextRtf = Me.txtDescripcion_Correspondencia.Text
    End Sub
    Private Sub frmClientesJuridicoCorrespondencia_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oCorrespondenciaClientes = New VillarrealBusiness.clsCorrespondenciaClientes
        oCorrespondenciaClientesdetalle = New VillarrealBusiness.clsCorrespondenciaClientesDetalle
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpCorrespondencia.Enabled = False
            Case Actions.Delete
                Me.rchContenido.Enabled = False
        End Select


    End Sub
    Private Sub frmClientesJuridicoCorrespondencia_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields

    End Sub
    
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCorrespondencia_LoadData(ByVal Initialize As Boolean) Handles lkpCorrespondencia.LoadData
        Dim oresponse As Events
        Dim odataset As DataSet

        oresponse = oCorrespondenciaClientes.Lookup
        If Not oresponse.ErrorFound Then
            odataset = oresponse.Value
            Me.lkpCorrespondencia.DataSource = odataset.Tables(0)
        End If
        odataset = Nothing
        oresponse = Nothing
    End Sub
    Private Sub lkpCorrespondencia_Format() Handles lkpCorrespondencia.Format
        Comunes.clsFormato.for_correspondencia_clientes_grl(Me.lkpCorrespondencia)
    End Sub
    Private Sub lkpCorrespondencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCorrespondencia.EditValueChanged
        If Me.Correspondencia > 0 Then
            sTexto_correspondencia = Me.lkpCorrespondencia.GetValue("texto_correspondencia")
            Me.rchContenido.TextRtf = sTexto_correspondencia
            Rutina(ObtenerCamposCorrespondenciaTexto())
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function ObtenerCamposCorrespondenciaTexto() As DataTable
        Dim response As Events
        Dim odatatable As DataTable
        response = oCorrespondenciaClientesdetalle.Listado(Correspondencia)
        If Not response.ErrorFound Then
            odatatable = CType(response.Value, DataSet).Tables(0)
        End If

        Return odatatable
    End Function

    Private Sub Rutina(ByVal odatatable As DataTable)
        Dim response As Dipros.Utils.Events
        Dim i As Long

        oCamposCorrespondecia = New VillarrealBusiness.clsCamposCorrespondencia
        For i = 0 To odatatable.Rows.Count - 1
            'Dim response As Dipros.Utils.Events
            Dim stipo_dato As Char = ""
            Dim sSql As String = ""

            'oCamposCorrespondecia = New VillarrealBusiness.clsCamposCorrespondencia

            response = oCamposCorrespondecia.DespliegaDatos(odatatable.Rows(i).Item("campo_correspondencia"))
            If Not response.ErrorFound Then
                Dim odataset As DataSet
                odataset = response.Value
                If odataset.Tables(0).Rows.Count > 0 Then
                    stipo_dato = odataset.Tables(0).Rows(0).Item("tipo_dato")
                    ' POR EL MOMENTO SE TOMARA POR DEFECTO SOBRE LA TABLA DE CLIENTES
                    'SE NECESITA REALIZAR AJUSTE EN CASO DE QUE SE SELECCIONEN CAMPOS DE OTRAS TABLAS
                    sSql = "SELECT " & odataset.Tables(0).Rows(0).Item("campo_tabla") & " FROM " & odataset.Tables(0).Rows(0).Item("nombre_tabla") & " WHERE cliente= " & CType(Me.OwnerForm, frmCapturaClientesJuridico).Cliente.ToString
                    Dim odataset_campos_correspondencia As DataSet = TINApp.Connection.GetDataSet(sSql)
                    If odataset_campos_correspondencia.Tables(0).Rows.Count > 0 Then
                        Select Case stipo_dato
                            Case "M" ' Moneda
                                rchContenido.TextRtf = Replace(rchContenido.TextRtf, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "$#,###.00"))
                            Case "N" 'Numerico
                                rchContenido.TextRtf = Replace(rchContenido.TextRtf, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "#,###"))
                            Case "C" 'Cadena
                                rchContenido.TextRtf = Replace(rchContenido.TextRtf, odatatable.Rows(i).Item("campo_correspondencia"), IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)))
                            Case "F" 'Fecha
                                rchContenido.TextRtf = Replace(rchContenido.TextRtf, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "Long Date"))

                        End Select
                    Else
                        Me.rchContenido.TextRtf = Replace(Me.rchContenido.TextRtf, odatatable.Rows(i).Item("campo_correspondencia"), "")
                    End If
                End If
            End If
        Next


    End Sub
#End Region

End Class
