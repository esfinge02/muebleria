Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Reflection

Public Class frmCapturaClientesJuridico
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Juridico As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Juridico As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha_Asignacion As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Asignacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents chkNo_Recibir_Abonos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tbDocumentos As System.Windows.Forms.TabPage
    Friend WithEvents tbLlamadas As System.Windows.Forms.TabPage
    Friend WithEvents tbCorrespondencia As System.Windows.Forms.TabPage
    Friend WithEvents TbSeguimiento As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents tbEmbargos As System.Windows.Forms.TabPage
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents clcSaldoAsignar As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtobservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grvDirecciones As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDireccion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcSaldoActual As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents clcSaldoVencido As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcIntereses As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcGastosCostas As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcGastoTotal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcUltimoAbono As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents btnAvalRefencia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDetalleAbonos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dteFechaUltimoAbono As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Folio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grDomicilios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcChecar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkChecar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcInteres As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkClienteReformado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grLlamadasClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvLlamadasClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClienteLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcParentescoPersonaContesta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeDejado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeRecibido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grClientesJuridicoCorrespondencia As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvClientesJuridicoCorrespondencia As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaClientesJuridicoCorrespondencia As Dipros.Windows.TINMaster
    Friend WithEvents tmaClientesJuridicoSeguimiento As Dipros.Windows.TINMaster
    Friend WithEvents grClientesJuridicoSeguimiento As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvClientesJuridicoSeguimiento As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcFechaSeguimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSeguimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAgendarSeguimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckAgendar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemDateFecha As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents grcAgendarAnterior As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaGastosCostas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConceptoGastosCostas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteGastosCostas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateGastosCostas As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents tmaClientesJuridicoGastosCostas As Dipros.Windows.TINMaster
    Friend WithEvents grClientesJuridicoGastosCostas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvClientesJuridicoGastosCostas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grvClientesJuridicoEmbargos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaClientesJuridicoEmbargos As Dipros.Windows.TINMaster
    Friend WithEvents grClientesJuridicoEmbargos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcEmbargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcionMercancia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcValorEstimado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemImageComboStatus As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents grcObservacionesCorrespondencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaCorrespondencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents grcdescripcion_correspondencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioCorrespondencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCorrespondencias As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCapturaClientesJuridico))
        Me.lblFolio_Juridico = New System.Windows.Forms.Label
        Me.clcFolio_Juridico = New Dipros.Editors.TINCalcEdit
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblFecha_Asignacion = New System.Windows.Forms.Label
        Me.dteFecha_Asignacion = New DevExpress.XtraEditors.DateEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.chkNo_Recibir_Abonos = New DevExpress.XtraEditors.CheckEdit
        Me.grClientesJuridicoCorrespondencia = New DevExpress.XtraGrid.GridControl
        Me.grvClientesJuridicoCorrespondencia = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFolioCorrespondencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaCorrespondencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcObservacionesCorrespondencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcdescripcion_correspondencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCorrespondencias = New DevExpress.XtraGrid.Columns.GridColumn
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tbDocumentos = New System.Windows.Forms.TabPage
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcChecar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkChecar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcInteres = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tbCorrespondencia = New System.Windows.Forms.TabPage
        Me.tmaClientesJuridicoCorrespondencia = New Dipros.Windows.TINMaster
        Me.tbLlamadas = New System.Windows.Forms.TabPage
        Me.grLlamadasClientes = New DevExpress.XtraGrid.GridControl
        Me.grvLlamadasClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClienteLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContesto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcParentescoPersonaContesta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeDejado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeRecibido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TbSeguimiento = New System.Windows.Forms.TabPage
        Me.tmaClientesJuridicoSeguimiento = New Dipros.Windows.TINMaster
        Me.grClientesJuridicoSeguimiento = New DevExpress.XtraGrid.GridControl
        Me.grvClientesJuridicoSeguimiento = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFechaSeguimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateFecha = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcSeguimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAgendarSeguimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckAgendar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcAgendarAnterior = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.tmaClientesJuridicoGastosCostas = New Dipros.Windows.TINMaster
        Me.grClientesJuridicoGastosCostas = New DevExpress.XtraGrid.GridControl
        Me.grvClientesJuridicoGastosCostas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFechaGastosCostas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateGastosCostas = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcConceptoGastosCostas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteGastosCostas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tbEmbargos = New System.Windows.Forms.TabPage
        Me.grClientesJuridicoEmbargos = New DevExpress.XtraGrid.GridControl
        Me.grvClientesJuridicoEmbargos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcEmbargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcionMercancia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEstatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemImageComboStatus = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.grcValorEstimado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaClientesJuridicoEmbargos = New Dipros.Windows.TINMaster
        Me.clcSaldoAsignar = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtobservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnDetalleAbonos = New DevExpress.XtraEditors.SimpleButton
        Me.btnAvalRefencia = New DevExpress.XtraEditors.SimpleButton
        Me.Label10 = New System.Windows.Forms.Label
        Me.dteFechaUltimoAbono = New DevExpress.XtraEditors.DateEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcUltimoAbono = New DevExpress.XtraEditors.CalcEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcGastoTotal = New DevExpress.XtraEditors.CalcEdit
        Me.clcGastosCostas = New DevExpress.XtraEditors.CalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.clcIntereses = New DevExpress.XtraEditors.CalcEdit
        Me.clcSaldoVencido = New DevExpress.XtraEditors.CalcEdit
        Me.clcSaldoActual = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.grDomicilios = New DevExpress.XtraGrid.GridControl
        Me.grvDirecciones = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDireccion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Folio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label11 = New System.Windows.Forms.Label
        Me.chkClienteReformado = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcFolio_Juridico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Asignacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNo_Recibir_Abonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grClientesJuridicoCorrespondencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientesJuridicoCorrespondencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tbDocumentos.SuspendLayout()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbCorrespondencia.SuspendLayout()
        Me.tbLlamadas.SuspendLayout()
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TbSeguimiento.SuspendLayout()
        CType(Me.grClientesJuridicoSeguimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientesJuridicoSeguimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckAgendar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.grClientesJuridicoGastosCostas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientesJuridicoGastosCostas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateGastosCostas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbEmbargos.SuspendLayout()
        CType(Me.grClientesJuridicoEmbargos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientesJuridicoEmbargos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemImageComboStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoAsignar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtobservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFechaUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcGastoTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcGastosCostas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoVencido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDomicilios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDirecciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkClienteReformado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(12139, 28)
        '
        'lblFolio_Juridico
        '
        Me.lblFolio_Juridico.AutoSize = True
        Me.lblFolio_Juridico.Location = New System.Drawing.Point(85, 40)
        Me.lblFolio_Juridico.Name = "lblFolio_Juridico"
        Me.lblFolio_Juridico.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Juridico.TabIndex = 0
        Me.lblFolio_Juridico.Tag = ""
        Me.lblFolio_Juridico.Text = "&Folio:"
        Me.lblFolio_Juridico.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Juridico
        '
        Me.clcFolio_Juridico.EditValue = "0"
        Me.clcFolio_Juridico.Location = New System.Drawing.Point(128, 40)
        Me.clcFolio_Juridico.MaxValue = 0
        Me.clcFolio_Juridico.MinValue = 0
        Me.clcFolio_Juridico.Name = "clcFolio_Juridico"
        '
        'clcFolio_Juridico.Properties
        '
        Me.clcFolio_Juridico.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Juridico.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Juridico.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Juridico.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Juridico.Properties.Enabled = False
        Me.clcFolio_Juridico.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Juridico.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Juridico.Size = New System.Drawing.Size(80, 19)
        Me.clcFolio_Juridico.TabIndex = 1
        Me.clcFolio_Juridico.Tag = "folio_juridico"
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(63, 68)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 2
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "A&bogado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = False
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(128, 63)
        Me.lkpAbogado.MultiSelect = False
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = ""
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.ReadOnlyControl = False
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = False
        Me.lkpAbogado.Size = New System.Drawing.Size(336, 20)
        Me.lkpAbogado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAbogado.TabIndex = 3
        Me.lkpAbogado.Tag = "Abogado"
        Me.lkpAbogado.ToolTip = Nothing
        Me.lkpAbogado.ValueMember = "Abogado"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(73, 94)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 4
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "Nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(128, 87)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(336, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 5
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblFecha_Asignacion
        '
        Me.lblFecha_Asignacion.AutoSize = True
        Me.lblFecha_Asignacion.Location = New System.Drawing.Point(17, 112)
        Me.lblFecha_Asignacion.Name = "lblFecha_Asignacion"
        Me.lblFecha_Asignacion.Size = New System.Drawing.Size(103, 16)
        Me.lblFecha_Asignacion.TabIndex = 6
        Me.lblFecha_Asignacion.Tag = ""
        Me.lblFecha_Asignacion.Text = "Fecha asi&gnaci�n:"
        Me.lblFecha_Asignacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Asignacion
        '
        Me.dteFecha_Asignacion.EditValue = "17/01/2007"
        Me.dteFecha_Asignacion.Location = New System.Drawing.Point(128, 111)
        Me.dteFecha_Asignacion.Name = "dteFecha_Asignacion"
        '
        'dteFecha_Asignacion.Properties
        '
        Me.dteFecha_Asignacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Asignacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Asignacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Asignacion.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Asignacion.TabIndex = 7
        Me.dteFecha_Asignacion.Tag = "fecha_asignacion"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(76, 136)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 9
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkNo_Recibir_Abonos
        '
        Me.chkNo_Recibir_Abonos.Location = New System.Drawing.Point(344, 112)
        Me.chkNo_Recibir_Abonos.Name = "chkNo_Recibir_Abonos"
        '
        'chkNo_Recibir_Abonos.Properties
        '
        Me.chkNo_Recibir_Abonos.Properties.Caption = "&No Recibir Abonos"
        Me.chkNo_Recibir_Abonos.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkNo_Recibir_Abonos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkNo_Recibir_Abonos.Size = New System.Drawing.Size(128, 22)
        Me.chkNo_Recibir_Abonos.TabIndex = 8
        Me.chkNo_Recibir_Abonos.Tag = "no_recibir_abonos"
        '
        'grClientesJuridicoCorrespondencia
        '
        Me.grClientesJuridicoCorrespondencia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grClientesJuridicoCorrespondencia.EmbeddedNavigator
        '
        Me.grClientesJuridicoCorrespondencia.EmbeddedNavigator.Name = ""
        Me.grClientesJuridicoCorrespondencia.Location = New System.Drawing.Point(0, 24)
        Me.grClientesJuridicoCorrespondencia.MainView = Me.grvClientesJuridicoCorrespondencia
        Me.grClientesJuridicoCorrespondencia.Name = "grClientesJuridicoCorrespondencia"
        Me.grClientesJuridicoCorrespondencia.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1})
        Me.grClientesJuridicoCorrespondencia.Size = New System.Drawing.Size(786, 192)
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoCorrespondencia.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoCorrespondencia.TabIndex = 14
        Me.grClientesJuridicoCorrespondencia.Text = "ClientesJuridico"
        '
        'grvClientesJuridicoCorrespondencia
        '
        Me.grvClientesJuridicoCorrespondencia.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFolioCorrespondencia, Me.grcFechaCorrespondencia, Me.grcObservacionesCorrespondencia, Me.grcdescripcion_correspondencia, Me.grcCorrespondencias})
        Me.grvClientesJuridicoCorrespondencia.GridControl = Me.grClientesJuridicoCorrespondencia
        Me.grvClientesJuridicoCorrespondencia.Name = "grvClientesJuridicoCorrespondencia"
        Me.grvClientesJuridicoCorrespondencia.OptionsCustomization.AllowFilter = False
        Me.grvClientesJuridicoCorrespondencia.OptionsCustomization.AllowGroup = False
        Me.grvClientesJuridicoCorrespondencia.OptionsCustomization.AllowSort = False
        Me.grvClientesJuridicoCorrespondencia.OptionsView.ShowGroupPanel = False
        '
        'grcFolioCorrespondencia
        '
        Me.grcFolioCorrespondencia.Caption = "Folio"
        Me.grcFolioCorrespondencia.FieldName = "folio_juridico_correspondencia"
        Me.grcFolioCorrespondencia.Name = "grcFolioCorrespondencia"
        Me.grcFolioCorrespondencia.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFechaCorrespondencia
        '
        Me.grcFechaCorrespondencia.Caption = "Fecha"
        Me.grcFechaCorrespondencia.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.grcFechaCorrespondencia.FieldName = "fecha"
        Me.grcFechaCorrespondencia.Name = "grcFechaCorrespondencia"
        Me.grcFechaCorrespondencia.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaCorrespondencia.VisibleIndex = 1
        Me.grcFechaCorrespondencia.Width = 224
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'grcObservacionesCorrespondencia
        '
        Me.grcObservacionesCorrespondencia.Caption = "Observaciones"
        Me.grcObservacionesCorrespondencia.FieldName = "observaciones"
        Me.grcObservacionesCorrespondencia.Name = "grcObservacionesCorrespondencia"
        Me.grcObservacionesCorrespondencia.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcObservacionesCorrespondencia.VisibleIndex = 2
        Me.grcObservacionesCorrespondencia.Width = 429
        '
        'grcdescripcion_correspondencia
        '
        Me.grcdescripcion_correspondencia.Caption = "Descripci�n Correspondencia"
        Me.grcdescripcion_correspondencia.FieldName = "descripcion_correspondencia"
        Me.grcdescripcion_correspondencia.Name = "grcdescripcion_correspondencia"
        Me.grcdescripcion_correspondencia.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcdescripcion_correspondencia.Width = 242
        '
        'grcCorrespondencias
        '
        Me.grcCorrespondencias.Caption = "Correspondencia"
        Me.grcCorrespondencias.FieldName = "correspondencia"
        Me.grcCorrespondencias.Name = "grcCorrespondencias"
        Me.grcCorrespondencias.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCorrespondencias.VisibleIndex = 0
        Me.grcCorrespondencias.Width = 119
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "AS"
        Me.cboStatus.Location = New System.Drawing.Point(128, 135)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Asignado", "AS", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Sin Localizar", "SL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Juicio", "EJ", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Saldo Al Corriente", "SC", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Devuelto a Cr�dito", "DC", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 20)
        Me.cboStatus.TabIndex = 10
        Me.cboStatus.Tag = "status"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbDocumentos)
        Me.TabControl1.Controls.Add(Me.tbCorrespondencia)
        Me.TabControl1.Controls.Add(Me.tbLlamadas)
        Me.TabControl1.Controls.Add(Me.TbSeguimiento)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.tbEmbargos)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TabControl1.Location = New System.Drawing.Point(0, 335)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(794, 240)
        Me.TabControl1.TabIndex = 61
        '
        'tbDocumentos
        '
        Me.tbDocumentos.Controls.Add(Me.grMovimientos)
        Me.tbDocumentos.Location = New System.Drawing.Point(4, 22)
        Me.tbDocumentos.Name = "tbDocumentos"
        Me.tbDocumentos.Size = New System.Drawing.Size(786, 214)
        Me.tbDocumentos.TabIndex = 0
        Me.tbDocumentos.Text = "Documentos"
        '
        'grMovimientos
        '
        Me.grMovimientos.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(0, 0)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkChecar})
        Me.grMovimientos.Size = New System.Drawing.Size(786, 214)
        Me.grMovimientos.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 60
        Me.grMovimientos.TabStop = False
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcChecar, Me.grcFecha, Me.grcDocumento, Me.grcSaldoDocumento, Me.grcSaldoVenta, Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcInteres, Me.grcDocumentos})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.GroupPanelText = "Precios"
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcChecar
        '
        Me.grcChecar.Caption = "Incluir"
        Me.grcChecar.ColumnEdit = Me.chkChecar
        Me.grcChecar.FieldName = "checar"
        Me.grcChecar.Name = "grcChecar"
        Me.grcChecar.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcChecar.Width = 76
        '
        'chkChecar
        '
        Me.chkChecar.AutoHeight = False
        Me.chkChecar.Name = "chkChecar"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_vencimiento"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 116
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.DisplayFormat.FormatString = "n"
        Me.grcDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.Width = 65
        '
        'grcSaldoDocumento
        '
        Me.grcSaldoDocumento.Caption = "Saldo Documento"
        Me.grcSaldoDocumento.DisplayFormat.FormatString = "c2"
        Me.grcSaldoDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoDocumento.FieldName = "saldo_documento"
        Me.grcSaldoDocumento.Name = "grcSaldoDocumento"
        Me.grcSaldoDocumento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoDocumento.VisibleIndex = 5
        Me.grcSaldoDocumento.Width = 147
        '
        'grcSaldoVenta
        '
        Me.grcSaldoVenta.Caption = "Saldo Venta"
        Me.grcSaldoVenta.DisplayFormat.FormatString = "c2"
        Me.grcSaldoVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoVenta.FieldName = "saldo_venta"
        Me.grcSaldoVenta.Name = "grcSaldoVenta"
        Me.grcSaldoVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoVenta.VisibleIndex = 6
        Me.grcSaldoVenta.Width = 111
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal Factura"
        Me.grcSucursal.FieldName = "sucursal_nombre"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursal.VisibleIndex = 0
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto Factura"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.Width = 107
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie Factura"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 1
        Me.grcSerie.Width = 80
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio Factura"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 2
        Me.grcFolio.Width = 80
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcInteres
        '
        Me.grcInteres.Caption = "Inter�s"
        Me.grcInteres.DisplayFormat.FormatString = "c2"
        Me.grcInteres.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcInteres.FieldName = "interes"
        Me.grcInteres.Name = "grcInteres"
        Me.grcInteres.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcInteres.VisibleIndex = 7
        Me.grcInteres.Width = 108
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documento"
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        Me.grcDocumentos.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumentos.VisibleIndex = 4
        Me.grcDocumentos.Width = 106
        '
        'tbCorrespondencia
        '
        Me.tbCorrespondencia.Controls.Add(Me.tmaClientesJuridicoCorrespondencia)
        Me.tbCorrespondencia.Controls.Add(Me.grClientesJuridicoCorrespondencia)
        Me.tbCorrespondencia.Location = New System.Drawing.Point(4, 22)
        Me.tbCorrespondencia.Name = "tbCorrespondencia"
        Me.tbCorrespondencia.Size = New System.Drawing.Size(786, 214)
        Me.tbCorrespondencia.TabIndex = 2
        Me.tbCorrespondencia.Text = "Correspondencia"
        '
        'tmaClientesJuridicoCorrespondencia
        '
        Me.tmaClientesJuridicoCorrespondencia.BackColor = System.Drawing.Color.White
        Me.tmaClientesJuridicoCorrespondencia.CanDelete = True
        Me.tmaClientesJuridicoCorrespondencia.CanInsert = True
        Me.tmaClientesJuridicoCorrespondencia.CanUpdate = True
        Me.tmaClientesJuridicoCorrespondencia.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaClientesJuridicoCorrespondencia.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tmaClientesJuridicoCorrespondencia.ForeColor = System.Drawing.Color.Black
        Me.tmaClientesJuridicoCorrespondencia.Grid = Me.grClientesJuridicoCorrespondencia
        Me.tmaClientesJuridicoCorrespondencia.Location = New System.Drawing.Point(0, 0)
        Me.tmaClientesJuridicoCorrespondencia.Name = "tmaClientesJuridicoCorrespondencia"
        Me.tmaClientesJuridicoCorrespondencia.Size = New System.Drawing.Size(786, 24)
        Me.tmaClientesJuridicoCorrespondencia.TabIndex = 13
        Me.tmaClientesJuridicoCorrespondencia.Title = "Correspondencia"
        Me.tmaClientesJuridicoCorrespondencia.UpdateTitle = "un Registro"
        '
        'tbLlamadas
        '
        Me.tbLlamadas.Controls.Add(Me.grLlamadasClientes)
        Me.tbLlamadas.Location = New System.Drawing.Point(4, 22)
        Me.tbLlamadas.Name = "tbLlamadas"
        Me.tbLlamadas.Size = New System.Drawing.Size(786, 214)
        Me.tbLlamadas.TabIndex = 1
        Me.tbLlamadas.Text = "Llamadas"
        '
        'grLlamadasClientes
        '
        Me.grLlamadasClientes.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grLlamadasClientes.EmbeddedNavigator
        '
        Me.grLlamadasClientes.EmbeddedNavigator.Name = ""
        Me.grLlamadasClientes.Location = New System.Drawing.Point(0, 0)
        Me.grLlamadasClientes.MainView = Me.grvLlamadasClientes
        Me.grLlamadasClientes.Name = "grLlamadasClientes"
        Me.grLlamadasClientes.Size = New System.Drawing.Size(786, 214)
        Me.grLlamadasClientes.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grLlamadasClientes.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.TabIndex = 62
        Me.grLlamadasClientes.TabStop = False
        Me.grLlamadasClientes.Text = "LlamadasClientes"
        '
        'grvLlamadasClientes
        '
        Me.grvLlamadasClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClienteLlamada, Me.grcFechaLlamada, Me.grcTipoLlamada, Me.grcTelefono, Me.grcTipoTelefono, Me.grcContesto, Me.grcParentescoPersonaContesta, Me.grcMensajeDejado, Me.grcMensajeRecibido, Me.grcObservaciones})
        Me.grvLlamadasClientes.GridControl = Me.grLlamadasClientes
        Me.grvLlamadasClientes.Name = "grvLlamadasClientes"
        Me.grvLlamadasClientes.OptionsBehavior.Editable = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowFilter = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowGroup = False
        Me.grvLlamadasClientes.OptionsView.ShowGroupPanel = False
        Me.grvLlamadasClientes.OptionsView.ShowIndicator = False
        '
        'grcClienteLlamada
        '
        Me.grcClienteLlamada.Caption = "Cliente"
        Me.grcClienteLlamada.FieldName = "cliente"
        Me.grcClienteLlamada.Name = "grcClienteLlamada"
        '
        'grcFechaLlamada
        '
        Me.grcFechaLlamada.Caption = "Fecha"
        Me.grcFechaLlamada.DisplayFormat.FormatString = "d"
        Me.grcFechaLlamada.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaLlamada.FieldName = "fecha_hora"
        Me.grcFechaLlamada.Name = "grcFechaLlamada"
        Me.grcFechaLlamada.VisibleIndex = 0
        Me.grcFechaLlamada.Width = 74
        '
        'grcTipoLlamada
        '
        Me.grcTipoLlamada.Caption = "Tipo Llamada"
        Me.grcTipoLlamada.FieldName = "descripcion_tipo_llamada"
        Me.grcTipoLlamada.Name = "grcTipoLlamada"
        Me.grcTipoLlamada.VisibleIndex = 1
        Me.grcTipoLlamada.Width = 84
        '
        'grcTelefono
        '
        Me.grcTelefono.Caption = "Tel�fono"
        Me.grcTelefono.FieldName = "telefono"
        Me.grcTelefono.Name = "grcTelefono"
        Me.grcTelefono.VisibleIndex = 3
        Me.grcTelefono.Width = 79
        '
        'grcTipoTelefono
        '
        Me.grcTipoTelefono.Caption = "Tipo de Tel�fono"
        Me.grcTipoTelefono.FieldName = "descripcion_tipo_telefono"
        Me.grcTipoTelefono.Name = "grcTipoTelefono"
        Me.grcTipoTelefono.VisibleIndex = 2
        Me.grcTipoTelefono.Width = 79
        '
        'grcContesto
        '
        Me.grcContesto.Caption = "Contest�"
        Me.grcContesto.FieldName = "persona_contesta"
        Me.grcContesto.Name = "grcContesto"
        Me.grcContesto.VisibleIndex = 4
        Me.grcContesto.Width = 79
        '
        'grcParentescoPersonaContesta
        '
        Me.grcParentescoPersonaContesta.Caption = "Parentesco Persona Contesta"
        Me.grcParentescoPersonaContesta.FieldName = "parentesco_persona_contesta"
        Me.grcParentescoPersonaContesta.Name = "grcParentescoPersonaContesta"
        Me.grcParentescoPersonaContesta.Width = 79
        '
        'grcMensajeDejado
        '
        Me.grcMensajeDejado.Caption = "Mensaje Dejado"
        Me.grcMensajeDejado.FieldName = "mensaje_dejado"
        Me.grcMensajeDejado.Name = "grcMensajeDejado"
        Me.grcMensajeDejado.Width = 79
        '
        'grcMensajeRecibido
        '
        Me.grcMensajeRecibido.Caption = "Mensaje Recibido"
        Me.grcMensajeRecibido.FieldName = "mensaje_recibido"
        Me.grcMensajeRecibido.Name = "grcMensajeRecibido"
        Me.grcMensajeRecibido.VisibleIndex = 5
        Me.grcMensajeRecibido.Width = 79
        '
        'grcObservaciones
        '
        Me.grcObservaciones.Caption = "Observaciones"
        Me.grcObservaciones.FieldName = "observaciones"
        Me.grcObservaciones.Name = "grcObservaciones"
        Me.grcObservaciones.VisibleIndex = 6
        Me.grcObservaciones.Width = 103
        '
        'TbSeguimiento
        '
        Me.TbSeguimiento.Controls.Add(Me.tmaClientesJuridicoSeguimiento)
        Me.TbSeguimiento.Controls.Add(Me.grClientesJuridicoSeguimiento)
        Me.TbSeguimiento.Location = New System.Drawing.Point(4, 22)
        Me.TbSeguimiento.Name = "TbSeguimiento"
        Me.TbSeguimiento.Size = New System.Drawing.Size(786, 214)
        Me.TbSeguimiento.TabIndex = 3
        Me.TbSeguimiento.Text = "Seguimiento"
        '
        'tmaClientesJuridicoSeguimiento
        '
        Me.tmaClientesJuridicoSeguimiento.BackColor = System.Drawing.Color.White
        Me.tmaClientesJuridicoSeguimiento.CanDelete = True
        Me.tmaClientesJuridicoSeguimiento.CanInsert = True
        Me.tmaClientesJuridicoSeguimiento.CanUpdate = True
        Me.tmaClientesJuridicoSeguimiento.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaClientesJuridicoSeguimiento.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tmaClientesJuridicoSeguimiento.ForeColor = System.Drawing.Color.Black
        Me.tmaClientesJuridicoSeguimiento.Grid = Me.grClientesJuridicoSeguimiento
        Me.tmaClientesJuridicoSeguimiento.Location = New System.Drawing.Point(0, 0)
        Me.tmaClientesJuridicoSeguimiento.Name = "tmaClientesJuridicoSeguimiento"
        Me.tmaClientesJuridicoSeguimiento.Size = New System.Drawing.Size(786, 24)
        Me.tmaClientesJuridicoSeguimiento.TabIndex = 15
        Me.tmaClientesJuridicoSeguimiento.Title = "Seguimientos"
        Me.tmaClientesJuridicoSeguimiento.UpdateTitle = "un Seguimiento"
        '
        'grClientesJuridicoSeguimiento
        '
        Me.grClientesJuridicoSeguimiento.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grClientesJuridicoSeguimiento.EmbeddedNavigator
        '
        Me.grClientesJuridicoSeguimiento.EmbeddedNavigator.Name = ""
        Me.grClientesJuridicoSeguimiento.Location = New System.Drawing.Point(0, 23)
        Me.grClientesJuridicoSeguimiento.MainView = Me.grvClientesJuridicoSeguimiento
        Me.grClientesJuridicoSeguimiento.Name = "grClientesJuridicoSeguimiento"
        Me.grClientesJuridicoSeguimiento.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckAgendar, Me.RepositoryItemDateFecha})
        Me.grClientesJuridicoSeguimiento.Size = New System.Drawing.Size(786, 192)
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoSeguimiento.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoSeguimiento.TabIndex = 16
        Me.grClientesJuridicoSeguimiento.TabStop = False
        Me.grClientesJuridicoSeguimiento.Text = "ClientesJuridico"
        '
        'grvClientesJuridicoSeguimiento
        '
        Me.grvClientesJuridicoSeguimiento.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFechaSeguimiento, Me.grcSeguimiento, Me.grcAgendarSeguimiento, Me.grcAgendarAnterior})
        Me.grvClientesJuridicoSeguimiento.GridControl = Me.grClientesJuridicoSeguimiento
        Me.grvClientesJuridicoSeguimiento.Name = "grvClientesJuridicoSeguimiento"
        Me.grvClientesJuridicoSeguimiento.OptionsCustomization.AllowFilter = False
        Me.grvClientesJuridicoSeguimiento.OptionsCustomization.AllowGroup = False
        Me.grvClientesJuridicoSeguimiento.OptionsCustomization.AllowSort = False
        Me.grvClientesJuridicoSeguimiento.OptionsView.ShowGroupPanel = False
        Me.grvClientesJuridicoSeguimiento.OptionsView.ShowIndicator = False
        '
        'grcFechaSeguimiento
        '
        Me.grcFechaSeguimiento.Caption = "Fecha"
        Me.grcFechaSeguimiento.ColumnEdit = Me.RepositoryItemDateFecha
        Me.grcFechaSeguimiento.FieldName = "fecha"
        Me.grcFechaSeguimiento.Name = "grcFechaSeguimiento"
        Me.grcFechaSeguimiento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaSeguimiento.VisibleIndex = 0
        Me.grcFechaSeguimiento.Width = 136
        '
        'RepositoryItemDateFecha
        '
        Me.RepositoryItemDateFecha.AutoHeight = False
        Me.RepositoryItemDateFecha.DisplayFormat.FormatString = "dd/MMM/yyyy hh:mm:ss tt"
        Me.RepositoryItemDateFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateFecha.Name = "RepositoryItemDateFecha"
        '
        'grcSeguimiento
        '
        Me.grcSeguimiento.Caption = "Seguimiento"
        Me.grcSeguimiento.FieldName = "seguimiento"
        Me.grcSeguimiento.Name = "grcSeguimiento"
        Me.grcSeguimiento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeguimiento.VisibleIndex = 1
        Me.grcSeguimiento.Width = 545
        '
        'grcAgendarSeguimiento
        '
        Me.grcAgendarSeguimiento.Caption = "Agendar"
        Me.grcAgendarSeguimiento.ColumnEdit = Me.RepositoryItemCheckAgendar
        Me.grcAgendarSeguimiento.FieldName = "agendar"
        Me.grcAgendarSeguimiento.Name = "grcAgendarSeguimiento"
        Me.grcAgendarSeguimiento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAgendarSeguimiento.VisibleIndex = 2
        Me.grcAgendarSeguimiento.Width = 91
        '
        'RepositoryItemCheckAgendar
        '
        Me.RepositoryItemCheckAgendar.AutoHeight = False
        Me.RepositoryItemCheckAgendar.Name = "RepositoryItemCheckAgendar"
        '
        'grcAgendarAnterior
        '
        Me.grcAgendarAnterior.Caption = "Agendar_Anterior"
        Me.grcAgendarAnterior.ColumnEdit = Me.RepositoryItemCheckAgendar
        Me.grcAgendarAnterior.FieldName = "agendar_anterior"
        Me.grcAgendarAnterior.Name = "grcAgendarAnterior"
        Me.grcAgendarAnterior.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.tmaClientesJuridicoGastosCostas)
        Me.TabPage1.Controls.Add(Me.grClientesJuridicoGastosCostas)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(786, 214)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Gastos y Costas"
        '
        'tmaClientesJuridicoGastosCostas
        '
        Me.tmaClientesJuridicoGastosCostas.BackColor = System.Drawing.Color.White
        Me.tmaClientesJuridicoGastosCostas.CanDelete = True
        Me.tmaClientesJuridicoGastosCostas.CanInsert = True
        Me.tmaClientesJuridicoGastosCostas.CanUpdate = True
        Me.tmaClientesJuridicoGastosCostas.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaClientesJuridicoGastosCostas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tmaClientesJuridicoGastosCostas.ForeColor = System.Drawing.Color.Black
        Me.tmaClientesJuridicoGastosCostas.Grid = Me.grClientesJuridicoGastosCostas
        Me.tmaClientesJuridicoGastosCostas.Location = New System.Drawing.Point(0, 0)
        Me.tmaClientesJuridicoGastosCostas.Name = "tmaClientesJuridicoGastosCostas"
        Me.tmaClientesJuridicoGastosCostas.Size = New System.Drawing.Size(786, 24)
        Me.tmaClientesJuridicoGastosCostas.TabIndex = 15
        Me.tmaClientesJuridicoGastosCostas.Title = "Gastos y Costas"
        Me.tmaClientesJuridicoGastosCostas.UpdateTitle = "un Gasto"
        '
        'grClientesJuridicoGastosCostas
        '
        Me.grClientesJuridicoGastosCostas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grClientesJuridicoGastosCostas.EmbeddedNavigator
        '
        Me.grClientesJuridicoGastosCostas.EmbeddedNavigator.Name = ""
        Me.grClientesJuridicoGastosCostas.Location = New System.Drawing.Point(0, 24)
        Me.grClientesJuridicoGastosCostas.MainView = Me.grvClientesJuridicoGastosCostas
        Me.grClientesJuridicoGastosCostas.Name = "grClientesJuridicoGastosCostas"
        Me.grClientesJuridicoGastosCostas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateGastosCostas})
        Me.grClientesJuridicoGastosCostas.Size = New System.Drawing.Size(786, 192)
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoGastosCostas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoGastosCostas.TabIndex = 16
        Me.grClientesJuridicoGastosCostas.TabStop = False
        Me.grClientesJuridicoGastosCostas.Text = "ClientesJuridico"
        '
        'grvClientesJuridicoGastosCostas
        '
        Me.grvClientesJuridicoGastosCostas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFechaGastosCostas, Me.grcConceptoGastosCostas, Me.grcImporteGastosCostas})
        Me.grvClientesJuridicoGastosCostas.GridControl = Me.grClientesJuridicoGastosCostas
        Me.grvClientesJuridicoGastosCostas.Name = "grvClientesJuridicoGastosCostas"
        Me.grvClientesJuridicoGastosCostas.OptionsCustomization.AllowFilter = False
        Me.grvClientesJuridicoGastosCostas.OptionsCustomization.AllowGroup = False
        Me.grvClientesJuridicoGastosCostas.OptionsCustomization.AllowSort = False
        Me.grvClientesJuridicoGastosCostas.OptionsView.ShowGroupPanel = False
        Me.grvClientesJuridicoGastosCostas.OptionsView.ShowIndicator = False
        '
        'grcFechaGastosCostas
        '
        Me.grcFechaGastosCostas.Caption = "Fecha"
        Me.grcFechaGastosCostas.ColumnEdit = Me.RepositoryItemDateGastosCostas
        Me.grcFechaGastosCostas.FieldName = "fecha"
        Me.grcFechaGastosCostas.Name = "grcFechaGastosCostas"
        Me.grcFechaGastosCostas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaGastosCostas.VisibleIndex = 0
        Me.grcFechaGastosCostas.Width = 167
        '
        'RepositoryItemDateGastosCostas
        '
        Me.RepositoryItemDateGastosCostas.AutoHeight = False
        Me.RepositoryItemDateGastosCostas.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateGastosCostas.DisplayFormat.FormatString = "dd/MMM/yyyy hh:mm:ss tt"
        Me.RepositoryItemDateGastosCostas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateGastosCostas.Name = "RepositoryItemDateGastosCostas"
        '
        'grcConceptoGastosCostas
        '
        Me.grcConceptoGastosCostas.Caption = "Concepto"
        Me.grcConceptoGastosCostas.FieldName = "concepto"
        Me.grcConceptoGastosCostas.Name = "grcConceptoGastosCostas"
        Me.grcConceptoGastosCostas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConceptoGastosCostas.VisibleIndex = 1
        Me.grcConceptoGastosCostas.Width = 426
        '
        'grcImporteGastosCostas
        '
        Me.grcImporteGastosCostas.Caption = "Importe"
        Me.grcImporteGastosCostas.DisplayFormat.FormatString = "c2"
        Me.grcImporteGastosCostas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteGastosCostas.FieldName = "importe"
        Me.grcImporteGastosCostas.Name = "grcImporteGastosCostas"
        Me.grcImporteGastosCostas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteGastosCostas.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporteGastosCostas.VisibleIndex = 2
        Me.grcImporteGastosCostas.Width = 179
        '
        'tbEmbargos
        '
        Me.tbEmbargos.Controls.Add(Me.grClientesJuridicoEmbargos)
        Me.tbEmbargos.Controls.Add(Me.tmaClientesJuridicoEmbargos)
        Me.tbEmbargos.Location = New System.Drawing.Point(4, 22)
        Me.tbEmbargos.Name = "tbEmbargos"
        Me.tbEmbargos.Size = New System.Drawing.Size(786, 214)
        Me.tbEmbargos.TabIndex = 5
        Me.tbEmbargos.Text = "Embargos"
        '
        'grClientesJuridicoEmbargos
        '
        Me.grClientesJuridicoEmbargos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grClientesJuridicoEmbargos.EmbeddedNavigator
        '
        Me.grClientesJuridicoEmbargos.EmbeddedNavigator.Name = ""
        Me.grClientesJuridicoEmbargos.Location = New System.Drawing.Point(0, 24)
        Me.grClientesJuridicoEmbargos.MainView = Me.grvClientesJuridicoEmbargos
        Me.grClientesJuridicoEmbargos.Name = "grClientesJuridicoEmbargos"
        Me.grClientesJuridicoEmbargos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemImageComboStatus})
        Me.grClientesJuridicoEmbargos.Size = New System.Drawing.Size(786, 192)
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoEmbargos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grClientesJuridicoEmbargos.TabIndex = 17
        Me.grClientesJuridicoEmbargos.TabStop = False
        Me.grClientesJuridicoEmbargos.Text = "ClientesJuridico"
        '
        'grvClientesJuridicoEmbargos
        '
        Me.grvClientesJuridicoEmbargos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcEmbargo, Me.grcDescripcionMercancia, Me.grcEstatus, Me.grcValorEstimado})
        Me.grvClientesJuridicoEmbargos.GridControl = Me.grClientesJuridicoEmbargos
        Me.grvClientesJuridicoEmbargos.Name = "grvClientesJuridicoEmbargos"
        Me.grvClientesJuridicoEmbargos.OptionsCustomization.AllowFilter = False
        Me.grvClientesJuridicoEmbargos.OptionsCustomization.AllowGroup = False
        Me.grvClientesJuridicoEmbargos.OptionsCustomization.AllowSort = False
        Me.grvClientesJuridicoEmbargos.OptionsView.ShowGroupPanel = False
        Me.grvClientesJuridicoEmbargos.OptionsView.ShowIndicator = False
        '
        'grcEmbargo
        '
        Me.grcEmbargo.Caption = "Embargo"
        Me.grcEmbargo.FieldName = "embargo"
        Me.grcEmbargo.Name = "grcEmbargo"
        Me.grcEmbargo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEmbargo.VisibleIndex = 0
        Me.grcEmbargo.Width = 91
        '
        'grcDescripcionMercancia
        '
        Me.grcDescripcionMercancia.Caption = "Descripci�n de Mercanc�a"
        Me.grcDescripcionMercancia.FieldName = "descripcion_mercancia"
        Me.grcDescripcionMercancia.Name = "grcDescripcionMercancia"
        Me.grcDescripcionMercancia.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcionMercancia.VisibleIndex = 1
        Me.grcDescripcionMercancia.Width = 470
        '
        'grcEstatus
        '
        Me.grcEstatus.Caption = "Estatus"
        Me.grcEstatus.ColumnEdit = Me.RepositoryItemImageComboStatus
        Me.grcEstatus.FieldName = "status"
        Me.grcEstatus.Name = "grcEstatus"
        Me.grcEstatus.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEstatus.VisibleIndex = 2
        Me.grcEstatus.Width = 94
        '
        'RepositoryItemImageComboStatus
        '
        Me.RepositoryItemImageComboStatus.AutoHeight = False
        Me.RepositoryItemImageComboStatus.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemImageComboStatus.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Embargado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Devuelto al Cliente", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("A la Venta", "V", -1)})
        Me.RepositoryItemImageComboStatus.Name = "RepositoryItemImageComboStatus"
        '
        'grcValorEstimado
        '
        Me.grcValorEstimado.Caption = "Valor Estimado"
        Me.grcValorEstimado.DisplayFormat.FormatString = "c2"
        Me.grcValorEstimado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcValorEstimado.FieldName = "valor_estimado"
        Me.grcValorEstimado.Name = "grcValorEstimado"
        Me.grcValorEstimado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcValorEstimado.VisibleIndex = 3
        Me.grcValorEstimado.Width = 117
        '
        'tmaClientesJuridicoEmbargos
        '
        Me.tmaClientesJuridicoEmbargos.BackColor = System.Drawing.Color.White
        Me.tmaClientesJuridicoEmbargos.CanDelete = True
        Me.tmaClientesJuridicoEmbargos.CanInsert = True
        Me.tmaClientesJuridicoEmbargos.CanUpdate = True
        Me.tmaClientesJuridicoEmbargos.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaClientesJuridicoEmbargos.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tmaClientesJuridicoEmbargos.ForeColor = System.Drawing.Color.Black
        Me.tmaClientesJuridicoEmbargos.Grid = Me.grClientesJuridicoEmbargos
        Me.tmaClientesJuridicoEmbargos.Location = New System.Drawing.Point(0, 0)
        Me.tmaClientesJuridicoEmbargos.Name = "tmaClientesJuridicoEmbargos"
        Me.tmaClientesJuridicoEmbargos.Size = New System.Drawing.Size(786, 24)
        Me.tmaClientesJuridicoEmbargos.TabIndex = 16
        Me.tmaClientesJuridicoEmbargos.Title = "Embargos"
        Me.tmaClientesJuridicoEmbargos.UpdateTitle = "un Gasto"
        '
        'clcSaldoAsignar
        '
        Me.clcSaldoAsignar.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldoAsignar.Location = New System.Drawing.Point(344, 136)
        Me.clcSaldoAsignar.Name = "clcSaldoAsignar"
        '
        'clcSaldoAsignar.Properties
        '
        Me.clcSaldoAsignar.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldoAsignar.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoAsignar.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldoAsignar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoAsignar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldoAsignar.Size = New System.Drawing.Size(120, 20)
        Me.clcSaldoAsignar.TabIndex = 12
        Me.clcSaldoAsignar.Tag = "saldo_asignar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(240, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 16)
        Me.Label2.TabIndex = 11
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sal&do al Asignar:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 192)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 16)
        Me.Label3.TabIndex = 62
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Observaciones:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtobservaciones
        '
        Me.txtobservaciones.EditValue = ""
        Me.txtobservaciones.Location = New System.Drawing.Point(128, 184)
        Me.txtobservaciones.Name = "txtobservaciones"
        Me.txtobservaciones.Size = New System.Drawing.Size(336, 44)
        Me.txtobservaciones.TabIndex = 63
        Me.txtobservaciones.Tag = "observaciones"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnDetalleAbonos)
        Me.GroupBox1.Controls.Add(Me.btnAvalRefencia)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.dteFechaUltimoAbono)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.clcUltimoAbono)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.clcGastoTotal)
        Me.GroupBox1.Controls.Add(Me.clcGastosCostas)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.clcIntereses)
        Me.GroupBox1.Controls.Add(Me.clcSaldoVencido)
        Me.GroupBox1.Controls.Add(Me.clcSaldoActual)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(480, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(296, 264)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Informaci�n Adicional"
        '
        'btnDetalleAbonos
        '
        Me.btnDetalleAbonos.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnDetalleAbonos.Location = New System.Drawing.Point(156, 232)
        Me.btnDetalleAbonos.Name = "btnDetalleAbonos"
        Me.btnDetalleAbonos.Size = New System.Drawing.Size(112, 23)
        Me.btnDetalleAbonos.TabIndex = 80
        Me.btnDetalleAbonos.Text = "Detalle de Abonos"
        '
        'btnAvalRefencia
        '
        Me.btnAvalRefencia.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnAvalRefencia.Location = New System.Drawing.Point(28, 232)
        Me.btnAvalRefencia.Name = "btnAvalRefencia"
        Me.btnAvalRefencia.Size = New System.Drawing.Size(112, 23)
        Me.btnAvalRefencia.TabIndex = 77
        Me.btnAvalRefencia.Text = "Aval y Referencia"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(36, 160)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(140, 16)
        Me.Label10.TabIndex = 76
        Me.Label10.Tag = ""
        Me.Label10.Text = "Fecha del �ltimo Abono:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaUltimoAbono
        '
        Me.dteFechaUltimoAbono.EditValue = New Date(2007, 1, 19, 0, 0, 0, 0)
        Me.dteFechaUltimoAbono.Location = New System.Drawing.Point(184, 160)
        Me.dteFechaUltimoAbono.Name = "dteFechaUltimoAbono"
        '
        'dteFechaUltimoAbono.Properties
        '
        Me.dteFechaUltimoAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaUltimoAbono.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaUltimoAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaUltimoAbono.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaUltimoAbono.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaUltimoAbono.Properties.Enabled = False
        Me.dteFechaUltimoAbono.Size = New System.Drawing.Size(88, 20)
        Me.dteFechaUltimoAbono.TabIndex = 75
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(24, 184)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(152, 16)
        Me.Label9.TabIndex = 74
        Me.Label9.Tag = ""
        Me.Label9.Text = "Importe del �ltimo Abono:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcUltimoAbono
        '
        Me.clcUltimoAbono.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcUltimoAbono.Location = New System.Drawing.Point(184, 184)
        Me.clcUltimoAbono.Name = "clcUltimoAbono"
        '
        'clcUltimoAbono.Properties
        '
        Me.clcUltimoAbono.Properties.DisplayFormat.FormatString = "c2"
        Me.clcUltimoAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoAbono.Properties.Enabled = False
        Me.clcUltimoAbono.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcUltimoAbono.Size = New System.Drawing.Size(88, 20)
        Me.clcUltimoAbono.TabIndex = 73
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(102, 121)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 16)
        Me.Label8.TabIndex = 72
        Me.Label8.Tag = ""
        Me.Label8.Text = "Saldo Total:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(76, 95)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 16)
        Me.Label7.TabIndex = 71
        Me.Label7.Tag = ""
        Me.Label7.Text = "Gas&tos y Costas:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcGastoTotal
        '
        Me.clcGastoTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcGastoTotal.Location = New System.Drawing.Point(184, 116)
        Me.clcGastoTotal.Name = "clcGastoTotal"
        '
        'clcGastoTotal.Properties
        '
        Me.clcGastoTotal.Properties.DisplayFormat.FormatString = "c2"
        Me.clcGastoTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGastoTotal.Properties.Enabled = False
        Me.clcGastoTotal.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlLightLight, System.Drawing.SystemColors.ActiveCaption)
        Me.clcGastoTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcGastoTotal.Size = New System.Drawing.Size(88, 20)
        Me.clcGastoTotal.TabIndex = 70
        '
        'clcGastosCostas
        '
        Me.clcGastosCostas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcGastosCostas.Location = New System.Drawing.Point(184, 91)
        Me.clcGastosCostas.Name = "clcGastosCostas"
        '
        'clcGastosCostas.Properties
        '
        Me.clcGastosCostas.Properties.DisplayFormat.FormatString = "c2"
        Me.clcGastosCostas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGastosCostas.Properties.Enabled = False
        Me.clcGastosCostas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcGastosCostas.Size = New System.Drawing.Size(88, 20)
        Me.clcGastosCostas.TabIndex = 69
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(87, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 16)
        Me.Label6.TabIndex = 68
        Me.Label6.Tag = ""
        Me.Label6.Text = "Sal&do Vencido:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(113, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 16)
        Me.Label5.TabIndex = 67
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Intereses:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcIntereses.Location = New System.Drawing.Point(184, 66)
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "c2"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.Enabled = False
        Me.clcIntereses.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcIntereses.Size = New System.Drawing.Size(88, 20)
        Me.clcIntereses.TabIndex = 2
        '
        'clcSaldoVencido
        '
        Me.clcSaldoVencido.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldoVencido.Location = New System.Drawing.Point(184, 41)
        Me.clcSaldoVencido.Name = "clcSaldoVencido"
        '
        'clcSaldoVencido.Properties
        '
        Me.clcSaldoVencido.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldoVencido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoVencido.Properties.Enabled = False
        Me.clcSaldoVencido.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldoVencido.Size = New System.Drawing.Size(88, 20)
        Me.clcSaldoVencido.TabIndex = 1
        '
        'clcSaldoActual
        '
        Me.clcSaldoActual.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldoActual.Location = New System.Drawing.Point(184, 16)
        Me.clcSaldoActual.Name = "clcSaldoActual"
        '
        'clcSaldoActual.Properties
        '
        Me.clcSaldoActual.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldoActual.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoActual.Properties.Enabled = False
        Me.clcSaldoActual.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldoActual.Size = New System.Drawing.Size(88, 20)
        Me.clcSaldoActual.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(96, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 16)
        Me.Label4.TabIndex = 66
        Me.Label4.Tag = ""
        Me.Label4.Text = "Saldo Actual:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grDomicilios
        '
        '
        'grDomicilios.EmbeddedNavigator
        '
        Me.grDomicilios.EmbeddedNavigator.Name = ""
        Me.grDomicilios.Location = New System.Drawing.Point(8, 232)
        Me.grDomicilios.MainView = Me.grvDirecciones
        Me.grDomicilios.Name = "grDomicilios"
        Me.grDomicilios.Size = New System.Drawing.Size(464, 96)
        Me.grDomicilios.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDomicilios.TabIndex = 65
        Me.grDomicilios.Text = "GridControl1"
        '
        'grvDirecciones
        '
        Me.grvDirecciones.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFactura, Me.grcDireccion, Me.Folio})
        Me.grvDirecciones.GridControl = Me.grDomicilios
        Me.grvDirecciones.Name = "grvDirecciones"
        Me.grvDirecciones.OptionsCustomization.AllowFilter = False
        Me.grvDirecciones.OptionsView.ShowGroupPanel = False
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura o Reparto"
        Me.grcFactura.FieldName = "tipo"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 103
        '
        'grcDireccion
        '
        Me.grcDireccion.Caption = "Domicilio"
        Me.grcDireccion.FieldName = "domicilio"
        Me.grcDireccion.HeaderStyleName = "Style1"
        Me.grcDireccion.Name = "grcDireccion"
        Me.grcDireccion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDireccion.VisibleIndex = 2
        Me.grcDireccion.Width = 219
        '
        'Folio
        '
        Me.Folio.Caption = "Folio"
        Me.Folio.FieldName = "factura"
        Me.Folio.Name = "Folio"
        Me.Folio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Folio.SortIndex = 0
        Me.Folio.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.Folio.VisibleIndex = 0
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2007, 1, 19, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(376, 39)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 66
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(328, 40)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 16)
        Me.Label11.TabIndex = 67
        Me.Label11.Tag = ""
        Me.Label11.Text = "F&echa:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkClienteReformado
        '
        Me.chkClienteReformado.Location = New System.Drawing.Point(128, 160)
        Me.chkClienteReformado.Name = "chkClienteReformado"
        '
        'chkClienteReformado.Properties
        '
        Me.chkClienteReformado.Properties.Caption = "Cliente Refor&mado"
        Me.chkClienteReformado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkClienteReformado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkClienteReformado.Size = New System.Drawing.Size(128, 22)
        Me.chkClienteReformado.TabIndex = 68
        Me.chkClienteReformado.Tag = "cliente_reformado"
        '
        'frmCapturaClientesJuridico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(794, 575)
        Me.Controls.Add(Me.chkClienteReformado)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.grDomicilios)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtobservaciones)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcSaldoAsignar)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.cboStatus)
        Me.Controls.Add(Me.lblFolio_Juridico)
        Me.Controls.Add(Me.clcFolio_Juridico)
        Me.Controls.Add(Me.lblAbogado)
        Me.Controls.Add(Me.lkpAbogado)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblFecha_Asignacion)
        Me.Controls.Add(Me.dteFecha_Asignacion)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.chkNo_Recibir_Abonos)
        Me.MasterControl = Me.tmaClientesJuridicoCorrespondencia
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmCapturaClientesJuridico"
        Me.Controls.SetChildIndex(Me.chkNo_Recibir_Abonos, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Asignacion, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Asignacion, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpAbogado, 0)
        Me.Controls.SetChildIndex(Me.lblAbogado, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Juridico, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Juridico, 0)
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.clcSaldoAsignar, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.txtobservaciones, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.grDomicilios, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.chkClienteReformado, 0)
        CType(Me.clcFolio_Juridico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Asignacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNo_Recibir_Abonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grClientesJuridicoCorrespondencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientesJuridicoCorrespondencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tbDocumentos.ResumeLayout(False)
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbCorrespondencia.ResumeLayout(False)
        Me.tbLlamadas.ResumeLayout(False)
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TbSeguimiento.ResumeLayout(False)
        CType(Me.grClientesJuridicoSeguimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientesJuridicoSeguimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckAgendar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.grClientesJuridicoGastosCostas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientesJuridicoGastosCostas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateGastosCostas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbEmbargos.ResumeLayout(False)
        CType(Me.grClientesJuridicoEmbargos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientesJuridicoEmbargos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemImageComboStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoAsignar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtobservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFechaUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcGastoTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcGastosCostas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoVencido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDomicilios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDirecciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkClienteReformado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientesJuridico As VillarrealBusiness.clsClientesJuridico
    Private oAbogados As New VillarrealBusiness.clsAbogados
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes
    Private oClientesJuridicoSeguimiento As VillarrealBusiness.clsClientesJuridicoSeguimiento
    Private oClientesJuridicoGastosCostas As VillarrealBusiness.clsClientesJuridicoGastosCostas
    Private oClientesJuridicoEmbargos As VillarrealBusiness.clsClientesJuridicoEmbargos
    Private oClientesJuridicoCorrespondencia As VillarrealBusiness.clsClientesJuridicoCorrespondencia

    Private bsucursal_dependencia As Boolean = False
    Private lClienteAval As Long
    Private NombreAval As String = ""
    Private DomicilioAval As String = ""
    Private ColoniaAval As String = ""
    Private TelefonoAval As String = ""
    Private CiudadAval As String = ""
    Private EstadoAval As String = ""
    Private ParentescoAval As String = ""
    Private AniosAval As Long = 0


    Private Property Abogado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAbogado)
        End Get
        Set(ByVal Value As Long)
            Me.lkpAbogado.EditValue = Value
        End Set
    End Property
    Public ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property



#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCapturaClientesJuridico_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmCapturaClientesJuridico_BeginUpdate() Handles MyBase.BeginUpdate
        Dim RESPONSE As Dipros.Utils.Events

        If ValidarClientesAJuridico(Response) = True Then
        End If

        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCapturaClientesJuridico_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmCapturaClientesJuridico_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert

                'If ValidarClientesAJuridico(Response) = True Then
                'Exit Sub  ---  06- NOV 09 SE QUITO POR EL MOMENTO YA QUE OCASIONA CONFLICTOS CON LA INFO ACTUAL.
                'Else
                'Response = oClientesJuridico.Insertar(Me.clcFolio_Juridico.EditValue, Abogado, Cliente, Me.clcSaldoAsignar.Value, Me.dteFecha_Asignacion.DateTime, Me.cboStatus.Value, Me.chkNo_Recibir_Abonos.Checked, Me.txtobservaciones.Text, Me.chkClienteReformado.EditValue)
                'End If
                Response = oClientesJuridico.Insertar(Me.clcFolio_Juridico.EditValue, Abogado, Cliente, Me.clcSaldoAsignar.Value, Me.dteFecha_Asignacion.DateTime, Me.cboStatus.Value, Me.chkNo_Recibir_Abonos.Checked, Me.txtobservaciones.Text, Me.chkClienteReformado.EditValue)
            Case Actions.Update
                Response = oClientesJuridico.Actualizar(Me.clcFolio_Juridico.EditValue, Abogado, Cliente, Me.clcSaldoAsignar.Value, Me.dteFecha_Asignacion.DateTime, Me.cboStatus.Value, Me.chkNo_Recibir_Abonos.Checked, Me.txtobservaciones.Text, Me.chkClienteReformado.EditValue)

            Case Actions.Delete
                Response = oClientesJuridico.Eliminar(clcFolio_Juridico.Value)

        End Select
    End Sub
    Private Sub frmCapturaClientesJuridico_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim oDataSet As DataSet

        'Se llena el Formulario
        Response = oClientesJuridico.DespliegaDatos(OwnerForm.Value("folio_juridico"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        Else
            Exit Sub
        End If

        'Se llena el Master de los Seguimientos
        Response = oClientesJuridicoSeguimiento.Listado(Me.clcFolio_Juridico.Value)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaClientesJuridicoSeguimiento.DataSource = oDataSet
        End If

        'Se llena el Master de los Gastos y Costas
        Response = oClientesJuridicoGastosCostas.Listado(Me.clcFolio_Juridico.Value)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaClientesJuridicoGastosCostas.DataSource = oDataSet
        End If

        'Se llena el Master de los Embargos
        Response = oClientesJuridicoEmbargos.Listado(Me.clcFolio_Juridico.Value)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaClientesJuridicoEmbargos.DataSource = oDataSet
        End If

        'Se llena el Master de los Correspondencia
        Response = oClientesJuridicoCorrespondencia.Listado(Me.clcFolio_Juridico.Value)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaClientesJuridicoCorrespondencia.DataSource = oDataSet
        End If


        ActualizaGastosCostas(0)

    End Sub
    Private Sub frmCapturaClientesJuridico_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientesJuridico = New VillarrealBusiness.clsClientesJuridico
        oLlamadasClientes = New VillarrealBusiness.clsLlamadasClientes
        oClientesJuridicoSeguimiento = New VillarrealBusiness.clsClientesJuridicoSeguimiento
        oClientesJuridicoGastosCostas = New VillarrealBusiness.clsClientesJuridicoGastosCostas
        oClientesJuridicoEmbargos = New VillarrealBusiness.clsClientesJuridicoEmbargos
        oClientesJuridicoCorrespondencia = New VillarrealBusiness.clsClientesJuridicoCorrespondencia

        'Asigno las Fechas del Servidor
        Me.dteFecha.DateTime = TINApp.FechaServidor
        Me.dteFecha_Asignacion.DateTime = TINApp.FechaServidor

        'Inicializa los Master de la Forma
        InicializaMasterSeguimiento()
        InicializaMasterGastosCostas()
        InicializaMasterEmbargos()
        InicializaMasterCorrespondencias()

        Select Case Action
            Case Actions.Insert
                Me.Abogado = CType(OwnerForm, brwClientesJuridico).Abogado
                Me.lkpAbogado.Enabled = False
            Case Actions.Update
                Me.Abogado = OwnerForm.Value("abogado")
            Case Actions.Delete
        End Select

    End Sub
    Private Sub frmCapturaClientesJuridico_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oClientesJuridico.Validacion(Action, Abogado, Cliente)
    End Sub
    Private Sub frmCapturaClientesJuridico_Localize() Handles MyBase.Localize
        Find("folio_juridico", Me.clcFolio_Juridico.Value)

    End Sub
    Private Sub frmCapturaClientesJuridico_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        'Realiza el Action Correspondiente a las filas de los seguimientos
        ' =================================================================
        With Me.tmaClientesJuridicoSeguimiento
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = Me.oClientesJuridicoSeguimiento.Insertar(.SelectedRow, Me.clcFolio_Juridico.Value)
                        If Not Response.ErrorFound And .Item("agendar") = True Then CreaCitaOutlook(Response, .Item("fecha"), .Item("seguimiento"), .Item("observaciones"))
                    Case Actions.Update
                        Response = Me.oClientesJuridicoSeguimiento.Actualizar(.SelectedRow, Me.clcFolio_Juridico.Value)

                        If Not Response.ErrorFound Then
                            'Si ya tenia agendado un evento se elimina primero
                            If .Item("agendar_anterior") = True Then
                                Me.EliminCitaOutlook(Response, .Item("seguimiento"))
                            End If

                            'Si tiene agendado de nuevo se da de alta con los nuevos datos
                            If .Item("agendar") = True Then
                                CreaCitaOutlook(Response, .Item("fecha"), .Item("seguimiento"), .Item("observaciones"))
                            End If

                        End If
                    Case Actions.Delete
                        Response = Me.oClientesJuridicoSeguimiento.Eliminar(Me.clcFolio_Juridico.Value, .Item("fecha"))
                        If Not Response.ErrorFound Then
                            'Si ya tenia agendado un evento se elimina primero
                            If .Item("agendar_anterior") = True Then
                                Me.EliminCitaOutlook(Response, .Item("seguimiento"))
                            End If
                        End If
                End Select
                .MoveNext()
            Loop
        End With

        If Response.ErrorFound Then Exit Sub

        'Realiza el Action Correspondiente a las filas de los Gastos
        ' =================================================================
        With Me.tmaClientesJuridicoGastosCostas
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = Me.oClientesJuridicoGastosCostas.Insertar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Update
                        Response = Me.oClientesJuridicoGastosCostas.Actualizar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Delete
                        Response = Me.oClientesJuridicoGastosCostas.Eliminar(Me.clcFolio_Juridico.Value, .Item("fecha"))
                End Select
                .MoveNext()
            Loop
        End With

        If Response.ErrorFound Then Exit Sub

        'Realiza el Action Correspondiente a las filas de los seguimientos
        ' =================================================================
        With Me.tmaClientesJuridicoEmbargos
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = Me.oClientesJuridicoEmbargos.Insertar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Update
                        Response = Me.oClientesJuridicoEmbargos.Actualizar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Delete
                        Response = Me.oClientesJuridicoEmbargos.Eliminar(Me.clcFolio_Juridico.Value, .Item("embargo"))
                End Select
                .MoveNext()
            Loop
        End With

        If Response.ErrorFound Then Exit Sub

        'Realiza el Action Correspondiente a las filas de las correspondencias
        ' =================================================================
        With Me.tmaClientesJuridicoCorrespondencia
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = Me.oClientesJuridicoCorrespondencia.Insertar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Update
                        Response = Me.oClientesJuridicoCorrespondencia.Actualizar(.SelectedRow, Me.clcFolio_Juridico.Value)
                    Case Actions.Delete
                        Response = Me.oClientesJuridicoCorrespondencia.Eliminar(Me.clcFolio_Juridico.Value, .Item("folio_juridico_correspondencia"))
                End Select
                .MoveNext()
            Loop
        End With

    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim Response As New Events
        Response = oAbogados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupDisponiblesJuridicos()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Cliente > 0 Then
            'Si la clave del cliente es valida o mayor a 0 lleno los datos del cliente
            Me.lClienteAval = Me.lkpCliente.GetValue("cliente_aval")
            NombreAval = Me.lkpCliente.GetValue("aval")
            DomicilioAval = Me.lkpCliente.GetValue("domicilio_aval")
            ColoniaAval = Me.lkpCliente.GetValue("colonia_aval")
            TelefonoAval = Me.lkpCliente.GetValue("telefono_aval")
            CiudadAval = Me.lkpCliente.GetValue("ciudad_aval")
            EstadoAval = Me.lkpCliente.GetValue("estado_aval")

            Me.ParentescoAval = Me.lkpCliente.GetValue("parentesco_aval")
            Me.AniosAval = Me.lkpCliente.GetValue("anios_aval")




            CargaDatos_Saldos_Documentos()
            CargarDatos_Domicilios()
            LlenarGridLlamadas()
        End If
    End Sub

    Private Sub btnAvalRefencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvalRefencia.Click
        Dim oform As New frmCapturaClientesJurdicoAval
        oform.MdiParent = Me.MdiParent

        oform.ClienteAval = lClienteAval
        oform.NombreAval = NombreAval
        oform.DomicilioAval = DomicilioAval
        oform.ColoniaAval = ColoniaAval
        oform.TelefonoAval = TelefonoAval
        oform.CiudadAval = CiudadAval
        oform.EstadoAval = EstadoAval
        oform.ParentescoAval = Me.ParentescoAval
        oform.AniosAval = Me.AniosAval

        'oform.sucursal_dependencia = bsucursal_dependencia
        oform.Show()
    End Sub
    Private Sub btnDetalleAbonos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetalleAbonos.Click
        If Me.Cliente <= 0 Then
            ShowMessage(MessageType.MsgInformation, "El Cliente es Requerido")
            Exit Sub
        End If
        If Me.Abogado <= 0 Then
            ShowMessage(MessageType.MsgInformation, "El Abogado es Requerido")
            Exit Sub
        End If

        Dim response As New Events
        Dim oData As DataSet

        Dim frmModal As New frmClientesJuridicoAbonos
        With frmModal
            .Cliente = Cliente
            .Abogado = Abogado
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Show()
            Me.Enabled = False
        End With

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    'Funciones varias
    Private Sub CargaDatos_Saldos_Documentos()
        ' ======================================
        ' Funci�n para llenar los documentos vencidos del cliente no importando la factura
        ' ======================================

        Dim response As Events
        Dim odataset As DataSet
        Dim otableSaldos As DataTable
        Dim otableDocumentos As DataTable

        response = Comunes.clsUtilerias.obtenerSaldosVencidosInteresesCliente(Cliente, Me.dteFecha.DateTime)
        If Not response.ErrorFound Then
            odataset = CType(response.Value, DataSet)
            otableSaldos = odataset.Tables(0)
            otableDocumentos = odataset.Tables(1)
            If otableSaldos.Rows.Count > 0 Then
                With otableSaldos.Rows(0)
                    Me.clcSaldoActual.EditValue = .Item("saldo_actual")
                    Me.clcSaldoVencido.EditValue = .Item("saldo_vencido")
                    Me.clcIntereses.EditValue = .Item("intereses")
                    Me.clcGastoTotal.EditValue = .Item("saldo_total")
                    Me.dteFechaUltimoAbono.DateTime = .Item("fecha_ultimo_abono")
                    Me.clcUltimoAbono.EditValue = .Item("importe_ultimo_abono")
                End With
                Me.grMovimientos.DataSource = otableDocumentos
            Else
                Me.clcSaldoActual.EditValue = 0
                Me.clcSaldoVencido.EditValue = 0
                Me.clcIntereses.EditValue = 0
                Me.clcGastoTotal.EditValue = 0
                Me.dteFechaUltimoAbono.EditValue = System.DBNull.Value
                Me.clcUltimoAbono.EditValue = 0
            End If
        End If

        odataset = Nothing
        response = Nothing
    End Sub
    Private Sub CargarDatos_Domicilios()
        ' ======================================
        'Funci�n para llenar el grid con los domicilios del cliente ya sea de repartos o de la factura
        ' ======================================

        Dim response As Events
        Dim odataset As DataSet
        response = Comunes.clsUtilerias.Obtener_Domicilios_Ventas_Repartos_Cliente(Cliente)
        If Not response.ErrorFound Then
            odataset = CType(response.Value, DataSet)
            Me.grDomicilios.DataSource = odataset.Tables(0)
        End If

    End Sub
    Private Sub LlenarGridLlamadas()
        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet


        response = oLlamadasClientes.DespliegaDatos(Cliente)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.grLlamadasClientes.DataSource = odataset.Tables(0)
        End If

        odataset = Nothing

    End Sub
    Public Sub ActualizaGastosCostas(ByVal valor_anterior As Double)
        Me.clcGastosCostas.Value = Me.grcImporteGastosCostas.SummaryItem.SummaryValue
        Me.clcGastoTotal.Value = (Me.clcGastoTotal.Value + Me.clcGastosCostas.Value) - valor_anterior
    End Sub

    'Funciones de Outlook de eventos
    Private Sub CreaCitaOutlook(ByRef response As Events, ByVal fecha As DateTime, ByVal Seguimiento As String, ByVal Observaciones As String)
        '======================================================================
        ' Codigo para Agregar un Evento o Cita en el Outlook
        '======================================================================
        ' Create an Outlook application.
        Dim oApp As Outlook.Application = New Outlook.Application
        Dim objRecip As Outlook.Recipient

        ' Get Mapi NameSpace and Logon.
        Dim oNS As Outlook.NameSpace = oApp.GetNamespace("mapi")

        Try


            oNS.Logon("YourValidProfile", Missing.Value, False, True)             ' TODO:

            ' Create an AppointmentItem.
            Dim oAppt As Outlook._AppointmentItem = oApp.CreateItem(Outlook.OlItemType.olAppointmentItem)


            ' Change AppointmentItem to a Meeting. 
            oAppt.MeetingStatus = Outlook.OlMeetingStatus.olMeeting

            ' Set some common properties.
            oAppt.Subject = Seguimiento
            oAppt.Body = Observaciones
            oAppt.Location = "Juzgado"

            oAppt.Start = fecha
            oAppt.End = fecha

            oAppt.ReminderSet = True
            oAppt.ReminderMinutesBeforeStart = 10
            oAppt.ReminderPlaySound = True
            oAppt.BusyStatus = Outlook.OlBusyStatus.olBusy
            oAppt.IsOnlineMeeting = False
            oAppt.AllDayEvent = False
            oAppt.Importance = Outlook.OlImportance.olImportanceHigh
            oAppt.Sensitivity = Outlook.OlSensitivity.olPersonal



            ''Este Codigo es para agregar y enviarles a personas de la lista de contactos 
            ''la cita o reunion programada y asi esten enterados
            ''======================================================================

            ''Add attendees.
            'Dim oRecipts As Outlook.Recipients = oAppt.Recipients

            '' Add required attendee.
            'Dim oRecipt As Outlook.Recipient
            'oRecipt = oRecipts.Add("Donato Almiray Moctezuma")
            'oRecipt.Type = Outlook.OlMeetingRecipientType.olRequired

            '' Add optional attendee.
            'oRecipt = oRecipts.Add("Hugo C. Lumbreras Cuevas")
            'oRecipt.Type = Outlook.OlMeetingRecipientType.olRequired

            'oRecipts.ResolveAll()
            ''======================================================================

            ' Se envia.
            oAppt.Send()

            'Si se desea ver 
            oAppt.Display(True)

            oAppt = Nothing

        Catch ex As System.Runtime.InteropServices.COMException
            Select Case ex.ErrorCode
                Case -2147467260    ' User responded NO to security prompt
                    response.Message = "It won't work unless you say Yes to the security prompt."
                Case -1767636707    ' store not available to GetSharedDefaultFolder
                    response.Message = "Information store for " & objRecip.Name & " not available."
                Case -1664876273    ' no permissions for folder
                    response.Message = "You do not have permission to view the folder."
                Case -632274939     ' read-only permission, cannot write
                    response.Message = "You do not have permission to create a new item in the folder."
                Case Else
                    response.Message = "COMException - " & ex.ErrorCode

            End Select

        Catch ex As Exception
            response.Message = ex.ToString
        End Try

        ' Logoff.
        oNS.Logoff()

        '' Clean up.
        oApp = Nothing
        oNS = Nothing

        'Estos son en dado caso de que se utilicen recipientes para la cita
        'oRecipts = Nothing
        'oRecipt = Nothing
    End Sub
    Private Sub EliminCitaOutlook(ByRef response As Events, ByVal asunto As String)
        'Funcion que elimina un item del calendario si existe en el
        Dim oApp As Outlook.Application = New Outlook.Application
        Dim oNS As Outlook.NameSpace = oApp.GetNamespace("mapi")
        Dim calendar As Outlook.MAPIFolder = oNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar)
        Dim calendarItems As Outlook.Items = calendar.Items

        Try
            Dim item As Outlook.AppointmentItem
            item = CType(calendarItems(asunto), Outlook.AppointmentItem)
            item.Delete()

        Catch ex As System.Runtime.InteropServices.COMException
            response.Message = "No existe ese evento en el calendario"
        Catch ex As Exception
            response.Message = ex.Message
        End Try
    End Sub

    'Funciones de los Masters
    Private Sub InicializaMasterSeguimiento()

        With Me.tmaClientesJuridicoSeguimiento
            .UpdateTitle = "un Seguimiento"
            .UpdateForm = New frmClientesJuridicoSeguimiento
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("seguimiento", "System.String")
            .AddColumn("observaciones", "System.String")
            .AddColumn("agendar", "System.Boolean")
            .AddColumn("agendar_anterior", "System.Boolean")
        End With
    End Sub
    Private Sub InicializaMasterGastosCostas()

        With Me.tmaClientesJuridicoGastosCostas
            .UpdateTitle = "un Gasto"
            .UpdateForm = New frmClientesJuridicoGastosCostas
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("concepto", "System.String")
            .AddColumn("importe", "System.Double")
            .AddColumn("observaciones", "System.String")
        End With
    End Sub
    Private Sub InicializaMasterEmbargos()
        With Me.tmaClientesJuridicoEmbargos
            .UpdateTitle = "un Embargo"
            .UpdateForm = New frmClientesJuridicoEmbargos
            .AddColumn("embargo", "System.Int32")
            .AddColumn("descripcion_mercancia", "System.String")
            .AddColumn("valor_estimado", "System.Double")
            .AddColumn("status", "System.String")
        End With

    End Sub
    Private Sub InicializaMasterCorrespondencias()

        With Me.tmaClientesJuridicoCorrespondencia
            .UpdateTitle = "una Correspondencia"
            .UpdateForm = New frmClientesJuridicoCorrespondencia
            .AddColumn("folio_juridico_correspondencia", "System.Int32")
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("correspondencia")
            .AddColumn("descripcion_correspondencia", "System.String")
            .AddColumn("observaciones", "System.String")
        End With

    End Sub


    Private Function ValidarClientesAJuridico(ByRef response As Events) As Boolean

        ' Dim response As New Dipros.Utils.Events
        Dim bandera As Boolean = False


        If Me.Action <> Actions.Delete Then
            If ValidarCliente(CType(Me.lkpCliente.EditValue, Integer)) Then
                'ShowMessage(MessageType.MsgInformation, "El cliente " + CType(Me.lkpCliente.EditValue, String) + " no se puede asignar, tiene notas de cargo pendientes de generar")
                response = New Events
                response.Message = "El cliente " + CType(Me.lkpCliente.EditValue, String) + " tiene notas de cargo pendientes de generar"
                bandera = True
                response.ShowMessage()
            End If
        End If

        Return bandera
        'ImprimirAsignados()

    End Function
    Private Function ValidarCliente(ByVal cliente As Integer) As Boolean
        Dim response As Dipros.Utils.Events
        Try
            response = oClientesJuridico.NotasDeCargoPendientesClienteExiste(cliente)
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

        If CType(CType(CType((oClientesJuridico.NotasDeCargoPendientesClienteExiste(cliente)).Value.tables(0).rows(0), Object), System.Data.DataRow).ItemArray(0), Integer) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

   
End Class
