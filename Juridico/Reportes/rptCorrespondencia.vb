Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptCorrespondencia
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private rchContenido As DataDynamics.ActiveReports.RichTextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Juridico.rptCorrespondencia.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.rchContenido = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.RichTextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private oCamposCorrespondecia As VillarrealBusiness.clsCamposCorrespondencia

    Private sTexto_Correspondencia As String
    Private lcliente As Long
    Private odatatable As DataTable
    Private i As Long

    Public WriteOnly Property Texto_Correspondencia() As String
        Set(ByVal Value As String)
            sTexto_Correspondencia = Value
            Me.rchContenido.RTF = Value
        End Set
    End Property
    Public WriteOnly Property Cliente() As Long
        Set(ByVal Value As Long)
            lcliente = Value
        End Set
    End Property
    Public WriteOnly Property TablaCamposCorrespondenciaDetalle() As DataTable
        Set(ByVal Value As DataTable)
            odatatable = Value
        End Set
    End Property


    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim response As Dipros.Utils.Events
        oCamposCorrespondecia = New VillarrealBusiness.clsCamposCorrespondencia
        For i = 0 To odatatable.Rows.Count - 1
            'Dim response As Dipros.Utils.Events
            Dim stipo_dato As Char = ""
            Dim sSql As String = ""

            'oCamposCorrespondecia = New VillarrealBusiness.clsCamposCorrespondencia

            response = oCamposCorrespondecia.DespliegaDatos(odatatable.Rows(i).Item("campo_correspondencia"))
            If Not response.ErrorFound Then
                Dim odataset As DataSet
                odataset = response.Value
                If odataset.Tables(0).Rows.Count > 0 Then
                    stipo_dato = odataset.Tables(0).Rows(0).Item("tipo_dato")
                    ' POR EL MOMENTO SE TOMARA POR DEFECTO SOBRE LA TABLA DE CLIENTES
                    'SE NECESITA REALIZAR AJUSTE EN CASO DE QUE SE SELECCIONEN CAMPOS DE OTRAS TABLAS
                    sSql = "SELECT " & odataset.Tables(0).Rows(0).Item("campo_tabla") & " FROM " & odataset.Tables(0).Rows(0).Item("nombre_tabla") & " WHERE cliente= " & lcliente
                    Dim odataset_campos_correspondencia As DataSet = TINApp.Connection.GetDataSet(sSql)
                    If odataset_campos_correspondencia.Tables(0).Rows.Count > 0 Then
                        Select Case stipo_dato
                            Case "M" ' Moneda
                                rchContenido.RTF = Replace(rchContenido.RTF, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "$#,###.00"))
                            Case "N" 'Numerico
                                rchContenido.RTF = Replace(rchContenido.RTF, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "#,###"))
                            Case "C" 'Cadena
                                rchContenido.RTF = Replace(rchContenido.RTF, odatatable.Rows(i).Item("campo_correspondencia"), IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)))
                            Case "F" 'Fecha
                                rchContenido.RTF = Replace(rchContenido.RTF, odatatable.Rows(i).Item("campo_correspondencia"), Format(IIf(odataset_campos_correspondencia.Tables(0).Rows(0).Item(0) Is System.DBNull.Value, 0, odataset_campos_correspondencia.Tables(0).Rows(0).Item(0)), "Long Date"))

                        End Select
                    Else
                        Me.rchContenido.RTF = Replace(Me.rchContenido.RTF, odatatable.Rows(i).Item("campo_correspondencia"), "")
                    End If


                End If
            End If


        Next
    End Sub

    Private Sub rptCorrespondencia_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class


