Imports Dipros.Windows.Forms
Module modFormatos
    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	Abogados
    'DATE:		16/01/2007 00:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------
#Region "Abogados"

    Public Sub for_abogados_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Abogado", "abogado", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Direcci�n", "direccion", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "nombre_estado", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Municipio", "municipio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Municipio", "nombre_municipio", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ciudad", "ciudad", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ciudad", "nombre_ciudad", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "colonia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "nombre_colonia", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "C�digo Postal", "codigo_postal", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono Casa", "telefono_casa", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono Oficina", "telefono_oficina", 9, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono Celular", "telefono_celular", 10, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Email", "email", 11, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Comisi�n", "comision", 12, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	LlamadasClientes
    'DATE:		17/01/2007 00:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------
#Region "LlamadasClientes"

    Public Sub for_llamadas_clientes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Cliente", "cliente", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha Hora", "fecha_hora", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo Llamada", "tipo_llamada", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Telefono", "telefono", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo Telefono", "tipo_telefono", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Persona Contesta", "persona_contesta", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Parentesco Persona_Contesta", "parentesco_persona_contesta", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Mensaje Dejado", "mensaje_dejado", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Mensaje Recibido", "mensaje_recibido", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Usuario Llamada", "usuario_llamada", 9, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Observaciones", "observaciones", 10, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	cat_clientes
    'DATE:		17/01/2007 00:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------
#Region "cat_clientes"

    'Public Sub for_clientes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
    '    AddColumns(oLookup, "Cliente", "cliente", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Nombre", "nombre", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Nombres", "nombres", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Paterno", "paterno", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Materno", "materno", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Rfc", "rfc", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Curp", "curp", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Domicilio", "domicilio", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Numero_Exterior", "numero_exterior", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Numero_Interior", "numero_interior", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Entrecalle", "entrecalle", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Ycalle", "ycalle", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Colonia", "colonia", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Cp", "cp", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Estado", "estado", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Municipio", "municipio", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Ciudad", "ciudad", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Telefono1", "telefono1", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Telefono2", "telefono2", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Fax", "fax", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Ocupacion", "ocupacion", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Ingresos", "ingresos", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Puesto", "puesto", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Departamento", "departamento", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Telefonos_Laboral", "telefonos_laboral", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Antiguedad_Laboral", "antiguedad_laboral", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Notas", "notas", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Cliente_Aval", "cliente_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Aval", "aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Domicilio_Aval", "domicilio_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Colonia_Aval", "colonia_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Telefono_Aval", "telefono_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Ciudad_Aval", "ciudad_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Estado_Aval", "estado_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Anios_Aval", "anios_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Parentesco_Aval", "parentesco_aval", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Tipo_Cobro", "tipo_cobro", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Fecha_Alta", "fecha_alta", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Usuario", "usuario", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Saldo", "saldo", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Persona", "persona", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Sucursal", "sucursal", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "No_Paga_Comision", "no_paga_comision", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Rfc_Completo", "rfc_completo", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Cliente_Dependencias", "cliente_dependencias", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Limite_Credito", "limite_credito", DevExpress.Utils.FormatType.None, "")
    '    AddColumns(oLookup, "Tipo_Captura", "tipo_captura", DevExpress.Utils.FormatType.None, "")

    'End Sub


#End Region

    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	cat_tipos_llamadas
    'DATE:		17/01/2007 00:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------
#Region "cat_tipos_llamadas"
    Public Sub for_tipos_llamadas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tipo_Llamada", "tipo_llamada", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripcion", "descripcion", 1, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region


    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	TiposLlamadas
    'DATE:		17/01/2007 12:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------
#Region "TiposLlamadas"

    Public Sub for_tipos_llamadas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo de Llamada", "tipo_llamada", 0, DevExpress.Utils.FormatType.Numeric, "n0", 115)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 254)

    End Sub

#End Region

#Region "Clientes Juridico"

    Public Sub for_clientes_juridico_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio_juridico", 0, DevExpress.Utils.FormatType.Numeric, "n0", 50)
        AddColumns(oGrid, "Abogado", "nombre_abogado", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Cliente", "nombre_cliente", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Fecha Asignaci�n", "fecha_asignacion", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 200)
        AddColumns(oGrid, "Saldo al Asignar", "saldo_asignar", 4, DevExpress.Utils.FormatType.Numeric, "c2", 100)

    End Sub

#End Region

#Region "CamposCorrespondencia"

    Public Sub for_campos_correspondencia_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Campo_Correspondencia", "campo_correspondencia", 0, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Nombre de Tabla", "nombre_tabla", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Campo de la Tabla", "campo_tabla", 2, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Tipo de Dato", "tipo_dato", 3, DevExpress.Utils.FormatType.None, "", 80)

    End Sub

#End Region


#Region "CorrespondenciaClientes"

    Public Sub for_correspondencia_clientes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Correspondencia", "correspondencia", 0, DevExpress.Utils.FormatType.None, "", 130)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 350)
        AddColumns(oGrid, "Texto Correspondencia", "texto_correspondencia", -1, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

End Module
