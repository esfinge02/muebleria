Imports System.Reflection
Imports System.Globalization
Imports System.Threading

Module modInicio
    Public TINApp As New Dipros.Windows.Application
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Juridico"
        TINApp.Prefix = "JUR"

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        Comunes.Common.Aplicacion = TINApp

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        'Inicia la aplicaci�n
        Dim oMain As New FrmMain
        Application.Run(oMain)

        'Private Sub CreaCitaOutlook(ByVal fecha As DateTime, ByVal Seguimiento As String, ByVal Observaciones As String)
        '        '======================================================================
        '        ' Codigo para Agregar un Evento o Cita en el Outlook
        '        '======================================================================
        '        ' Create an Outlook application.
        '        Dim oApp As Outlook.Application = New Outlook.Application
        '        Dim objRecip As Outlook.Recipient

        '        ' Get Mapi NameSpace and Logon.
        '        Dim oNS As Outlook.NameSpace = oApp.GetNamespace("mapi")

        '        Try


        '            oNS.Logon("YourValidProfile", Missing.Value, False, True)             ' TODO:

        '            ' Create an AppointmentItem.
        '            Dim oAppt As Outlook._AppointmentItem = oApp.CreateItem(Outlook.OlItemType.olAppointmentItem)


        '            ' Change AppointmentItem to a Meeting. 
        '            oAppt.MeetingStatus = Outlook.OlMeetingStatus.olMeeting

        '            ' Set some common properties.
        '            oAppt.Subject = Seguimiento
        '            oAppt.Body = Observaciones
        '            oAppt.Location = "Juzgado"

        '            oAppt.Start = fecha
        '            oAppt.End = fecha

        '            oAppt.ReminderSet = True
        '            oAppt.ReminderMinutesBeforeStart = 10
        '            oAppt.ReminderPlaySound = True
        '            oAppt.BusyStatus = Outlook.OlBusyStatus.olBusy
        '            oAppt.IsOnlineMeeting = False
        '            oAppt.AllDayEvent = False
        '            oAppt.Importance = Outlook.OlImportance.olImportanceHigh
        '            oAppt.Sensitivity = Outlook.OlSensitivity.olPersonal



        '            ''Este Codigo es para agregar y enviarles a personas de la lista de contactos 
        '            ''la cita o reunion programada y asi esten enterados
        '            ''======================================================================

        '            ''Add attendees.
        '            'Dim oRecipts As Outlook.Recipients = oAppt.Recipients

        '            '' Add required attendee.
        '            'Dim oRecipt As Outlook.Recipient
        '            'oRecipt = oRecipts.Add("Donato Almiray Moctezuma")
        '            'oRecipt.Type = Outlook.OlMeetingRecipientType.olRequired

        '            '' Add optional attendee.
        '            'oRecipt = oRecipts.Add("Hugo C. Lumbreras Cuevas")
        '            'oRecipt.Type = Outlook.OlMeetingRecipientType.olRequired

        '            'oRecipts.ResolveAll()
        '            ''======================================================================

        '            ' Se envia.
        '            oAppt.Send()

        '            'Si se desea ver 
        '            oAppt.Display(True)

        '            oAppt = Nothing

        '        Catch ex As System.Runtime.InteropServices.COMException

        '            Select Case ex.ErrorCode
        '                Case -2147467260    ' User responded NO to security prompt
        '                    MessageBox.Show("It won't work unless you say Yes to the security prompt.")
        '                Case -1767636707    ' store not available to GetSharedDefaultFolder
        '                    MessageBox.Show("Information store for " & objRecip.Name & " not available.")
        '                Case -1664876273    ' no permissions for folder
        '                    MessageBox.Show("You do not have permission to view the folder.")
        '                Case -632274939     ' read-only permission, cannot write
        '                    MessageBox.Show("You do not have permission to create a new item in the folder.")
        '                Case Else
        '                    MessageBox.Show("COMException - " & ex.ErrorCode)
        '            End Select


        '        Catch ex As Exception
        '            MessageBox.Show(ex.Message, ex.ToString)
        '        End Try

        '        ' Logoff.
        '        oNS.Logoff()

        '        '' Clean up.
        '        oApp = Nothing
        '        oNS = Nothing

        '        'Estos son en dado caso de que se utilicen recipientes para la cita
        '        'oRecipts = Nothing
        '        'oRecipt = Nothing
        '    End Sub

    End Sub


End Module
